﻿// Decompiled with JetBrains decompiler
// Type: ServerSelector
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using Gsc;

public static class ServerSelector
{
  public static string ApiUrl
  {
    get
    {
      return SDK.Configuration.Env.ServerUrl;
    }
  }

  public static string DlcUrl
  {
    get
    {
      return SDK.Configuration.GetEnv<Gsc.App.Environment>().DlcPath;
    }
  }
}
