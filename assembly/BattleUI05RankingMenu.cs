﻿// Decompiled with JetBrains decompiler
// Type: BattleUI05RankingMenu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class BattleUI05RankingMenu : ResultMenuBase
{
  [SerializeField]
  private GameObject dir_RankingEvent;
  [SerializeField]
  private GameObject dir_StageTitle;
  [SerializeField]
  private UILabel txt_GetPoint;
  [SerializeField]
  private UILabel txt_TotalScoreTitle;
  [SerializeField]
  private UILabel txt_TotalScore;
  [SerializeField]
  private UILabel txt_StageHishScore;
  [SerializeField]
  private GameObject obj_NowHighScore;
  [SerializeField]
  private GameObject obj_TotalHighScore;
  [SerializeField]
  private GameObject obj_TotalScoreTitle;
  [SerializeField]
  private GameObject obj_TotalScore;
  [SerializeField]
  private GameObject baseObj;
  [SerializeField]
  private GameObject nextBtnObj;
  [SerializeField]
  private NGxScroll2 scroll;
  private GameObject breakDownPrefab;
  private GameObject highScorePrefab;
  private bool onFinish;
  private BattleUI05RankingMenu.AnimState state;
  private QuestScoreBattleFinishContext campaign;
  private const int iconWidth = 628;
  private const int iconHeight = 42;
  private bool toNext;

  public override IEnumerator Init(BattleInfo info, BattleEnd result)
  {
    IEnumerator e = this.LoadResources();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.campaign = result.score_campaigns[0];
    this.InitStagePoint(this.campaign.score_acquisitions, this.campaign.bonus_rate, ((IEnumerable<QuestScoreBonusTimetable>) SMManager.Get<QuestScoreBonusTimetable[]>()).Any<QuestScoreBonusTimetable>((Func<QuestScoreBonusTimetable, bool>) (x => info.quest_type == CommonQuestType.Extra && x.quest_s_id == info.quest_s_id)));
    if (info.extraQuest != null)
      this.txt_TotalScoreTitle.SetTextLocalize(Consts.Format(Consts.GetInstance().RESULT_RANKING_TOTAL_TITLE, (IDictionary) new Hashtable()
      {
        {
          (object) "name",
          (object) info.extraQuest.quest_extra_s.quest_m.name
        }
      }));
    this.txt_TotalScore.SetTextLocalize(Consts.Format(Consts.GetInstance().RESULT_RANKING_MENU_POINT, (IDictionary) new Hashtable()
    {
      {
        (object) "point",
        (object) this.campaign.score_max
      }
    }));
    this.txt_GetPoint.SetTextLocalize(Consts.Format(Consts.GetInstance().RESULT_RANKING_MENU_POINT, (IDictionary) new Hashtable()
    {
      {
        (object) "point",
        (object) this.campaign.battle_score
      }
    }));
    this.txt_StageHishScore.SetTextLocalize(Consts.Format(Consts.GetInstance().RESULT_RANKING_MENU_POINT, (IDictionary) new Hashtable()
    {
      {
        (object) "point",
        (object) this.campaign.battle_score_max
      }
    }));
  }

  private void InitStagePoint(
    QuestScoreAcquisition[] scores,
    string specialRate,
    bool activeSpecialRate)
  {
    this.scroll.Reset();
    for (int index = 0; index < scores.Length; ++index)
    {
      GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.breakDownPrefab);
      gameObject.GetComponent<BattleUI05BreakDown>().SetPoint(scores[index].description, scores[index].score);
      this.scroll.AddColumn1(gameObject, 628, 42, false);
    }
    if (activeSpecialRate)
    {
      GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.breakDownPrefab);
      gameObject.GetComponent<BattleUI05BreakDown>().SetSpecialRate(specialRate);
      this.scroll.AddColumn1(gameObject, 628, 42, false);
    }
    this.scroll.ResolvePosition();
  }

  private IEnumerator LoadResources()
  {
    Future<GameObject> breakDownPrefabF;
    IEnumerator e;
    if ((UnityEngine.Object) this.breakDownPrefab == (UnityEngine.Object) null)
    {
      breakDownPrefabF = Res.Prefabs.battle.dir_PtBreakdown.Load<GameObject>();
      e = breakDownPrefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.breakDownPrefab = breakDownPrefabF.Result;
      breakDownPrefabF = (Future<GameObject>) null;
    }
    if ((UnityEngine.Object) this.highScorePrefab == (UnityEngine.Object) null)
    {
      breakDownPrefabF = Res.Prefabs.battle.dir_RankingEvent_HighScore.Load<GameObject>();
      e = breakDownPrefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.highScorePrefab = breakDownPrefabF.Result;
      breakDownPrefabF = (Future<GameObject>) null;
    }
  }

  public override IEnumerator Run()
  {
    yield return (object) new WaitForSeconds(0.5f);
    this.state = BattleUI05RankingMenu.AnimState.NowScoreInit;
    this.onFinish = false;
    this.PlayAnimation();
    while (!this.onFinish)
      yield return (object) null;
    IEnumerator e = this.WaitForTapping();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public void PlayAnimation()
  {
    if (this.state == BattleUI05RankingMenu.AnimState.NowScoreInit || this.state == BattleUI05RankingMenu.AnimState.NowScore)
    {
      if (this.state == BattleUI05RankingMenu.AnimState.NowScoreInit)
      {
        Singleton<NGSoundManager>.GetInstance().PlaySe("SE_1011", false, 0.0f, -1);
        Singleton<NGSoundManager>.GetInstance().PlaySe("SE_1012", false, 0.5f, -1);
        Singleton<NGSoundManager>.GetInstance().PlaySe("SE_1012", false, 0.7f, -1);
        this.state = BattleUI05RankingMenu.AnimState.NowScore;
      }
      this.DispNowScore();
    }
    if (this.state == BattleUI05RankingMenu.AnimState.StageScoreUpdate)
      this.DispNowScoreUpdate();
    else if (this.state == BattleUI05RankingMenu.AnimState.TotalScore)
      this.DispTotalScore();
    else if (this.state == BattleUI05RankingMenu.AnimState.TotalScoreUpdate)
    {
      this.DispTotalScoreUpdate();
    }
    else
    {
      if (this.state != BattleUI05RankingMenu.AnimState.End)
        return;
      this.onFinish = true;
    }
  }

  public void ChangeState()
  {
    if (this.state == BattleUI05RankingMenu.AnimState.End)
      return;
    if (this.state == BattleUI05RankingMenu.AnimState.NowScore)
    {
      this.state = this.campaign.battle_score_max_updated ? BattleUI05RankingMenu.AnimState.StageScoreUpdate : BattleUI05RankingMenu.AnimState.TotalScore;
      if (this.state == BattleUI05RankingMenu.AnimState.TotalScore)
      {
        Singleton<NGSoundManager>.GetInstance().PlaySe("SE_1011", false, 0.0f, -1);
        Singleton<NGSoundManager>.GetInstance().PlaySe("SE_1012", false, 0.4f, -1);
      }
    }
    else if (this.state == BattleUI05RankingMenu.AnimState.StageScoreUpdate)
    {
      this.state = BattleUI05RankingMenu.AnimState.TotalScore;
      Singleton<NGSoundManager>.GetInstance().PlaySe("SE_1011", false, 0.0f, -1);
      Singleton<NGSoundManager>.GetInstance().PlaySe("SE_1012", false, 0.4f, -1);
    }
    else if (this.state == BattleUI05RankingMenu.AnimState.TotalScore)
      this.state = this.campaign.score_max_updated ? BattleUI05RankingMenu.AnimState.TotalScoreUpdate : BattleUI05RankingMenu.AnimState.End;
    else if (this.state == BattleUI05RankingMenu.AnimState.TotalScoreUpdate)
      this.state = BattleUI05RankingMenu.AnimState.End;
    this.PlayAnimation();
  }

  public void DispNowScore()
  {
    this.dir_RankingEvent.SetActive(true);
    this.baseObj.SetActive(true);
    this.nextBtnObj.SetActive(true);
    this.dir_StageTitle.SetActive(true);
  }

  private void DispNowScoreUpdate()
  {
    this.CreateHighScore(this.obj_NowHighScore.transform, new System.Action(this.ChangeState));
  }

  private void DispTotalScore()
  {
    this.obj_TotalScoreTitle.SetActive(true);
    this.obj_TotalScore.SetActive(true);
  }

  private void DispTotalScoreUpdate()
  {
    this.CreateHighScore(this.obj_TotalHighScore.transform, new System.Action(this.ChangeState));
  }

  private void CreateHighScore(Transform parent, System.Action callback)
  {
    UITweener componentInChildren = this.highScorePrefab.Clone(parent).GetComponentInChildren<UITweener>();
    Singleton<NGSoundManager>.GetInstance().PlaySe("SE_1024", false, 0.2f, -1);
    if ((UnityEngine.Object) componentInChildren == (UnityEngine.Object) null)
      callback();
    else
      componentInChildren.SetOnFinished(new EventDelegate((EventDelegate.Callback) (() => callback())));
  }

  public void onTapToNext()
  {
    this.toNext = true;
  }

  private IEnumerator WaitForTapping()
  {
    while (!this.toNext)
      yield return (object) null;
  }

  private enum AnimState
  {
    NowScoreInit,
    NowScore,
    StageScoreUpdate,
    TotalScore,
    TotalScoreUpdate,
    End,
  }
}
