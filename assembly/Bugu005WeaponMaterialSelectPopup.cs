﻿// Decompiled with JetBrains decompiler
// Type: Bugu005WeaponMaterialSelectPopup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Bugu005WeaponMaterialSelectPopup : BackButtonMenuBase
{
  [SerializeField]
  private UISlider slider;
  [SerializeField]
  private UILabel TxtItemName;
  [SerializeField]
  private UILabel TxtItemDescription;
  [SerializeField]
  private UILabel TxtPossessionNum;
  [SerializeField]
  private UILabel TxtConvertNum;
  [SerializeField]
  private UILabel TxtItemMaxNum;
  [SerializeField]
  private UILabel TxtSelectMax;
  [SerializeField]
  private UI2DSprite LinkItem;
  private Bugu005WeaponMaterialConversionMenu menu;
  private int itemCount;
  private int totalItem;
  private InventoryItem item;

  public void IbtnPopupOk()
  {
    if (this.IsPushAndSet())
      return;
    this.menu.SetConvertCount(this.item, this.itemCount);
    Singleton<PopupManager>.GetInstance().onDismiss();
  }

  public void IbtnNo()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<PopupManager>.GetInstance().onDismiss();
  }

  public override void onBackButton()
  {
    this.IbtnNo();
  }

  public IEnumerator InitSceneAsync(
    InventoryItem item,
    Bugu005WeaponMaterialConversionMenu menu)
  {
    Bugu005WeaponMaterialSelectPopup materialSelectPopup = this;
    materialSelectPopup.GetComponent<UIWidget>().alpha = 0.0f;
    Future<UnityEngine.Sprite> spriteF = item.LoadSpriteThumbnail();
    IEnumerator e = spriteF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    materialSelectPopup.LinkItem.sprite2D = spriteF.Result;
    materialSelectPopup.item = item;
    materialSelectPopup.menu = menu;
    materialSelectPopup.Set(item);
  }

  public void Set(InventoryItem item)
  {
    int num = ((IEnumerable<PlayerItem>) SMManager.Get<PlayerItem[]>()).Count<PlayerItem>((Func<PlayerItem, bool>) (x => !x.isSupply() && !x.isReisou()));
    this.totalItem = item.Item.quantity < this.menu.SelectMax ? item.Item.quantity : this.menu.SelectMax;
    this.TxtItemName.SetText(item.GetName());
    this.TxtItemDescription.SetText(item.GetDescription());
    this.TxtPossessionNum.SetTextLocalize(item.Item.quantity);
    this.TxtConvertNum.SetTextLocalize(this.totalItem);
    this.TxtSelectMax.SetTextLocalize(this.totalItem);
    this.TxtItemMaxNum.SetTextLocalize(num);
    this.slider.value = 1f;
  }

  protected override void Update()
  {
    this.itemCount = (int) ((double) this.slider.value * (double) this.totalItem);
    if (this.totalItem <= 1 && (double) this.slider.value < 1.0)
    {
      if ((double) this.slider.value >= 0.00999999977648258)
        this.itemCount = 1;
    }
    else if (this.itemCount > this.totalItem)
    {
      this.itemCount = this.totalItem;
      this.slider.value = (float) this.itemCount / (float) this.totalItem;
    }
    this.TxtConvertNum.SetTextLocalize(this.itemCount);
  }
}
