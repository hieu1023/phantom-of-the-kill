﻿// Decompiled with JetBrains decompiler
// Type: Popup023418Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;
using UnityEngine;

public class Popup023418Menu : BackButtonMenuBase
{
  protected System.Action onCallback;
  [SerializeField]
  private UILabel TxtDescription1;
  [SerializeField]
  private UILabel TxtDescription2;
  [SerializeField]
  private GameObject link_Icon;

  protected string GetRewardTypeText(MasterDataTable.CommonRewardType type)
  {
    string str = "";
    if (type != MasterDataTable.CommonRewardType.money && type != MasterDataTable.CommonRewardType.peculiar && (type != MasterDataTable.CommonRewardType.player_exp && type != MasterDataTable.CommonRewardType.unit_exp) && (type != MasterDataTable.CommonRewardType.gear_experience_point && type != MasterDataTable.CommonRewardType.gear_training_point && (type != MasterDataTable.CommonRewardType.coin && type != MasterDataTable.CommonRewardType.recover)) && (type != MasterDataTable.CommonRewardType.max_unit && type != MasterDataTable.CommonRewardType.max_item && (type != MasterDataTable.CommonRewardType.medal && type != MasterDataTable.CommonRewardType.friend_point) && (type != MasterDataTable.CommonRewardType.deck && type != MasterDataTable.CommonRewardType.awake_skill && type == MasterDataTable.CommonRewardType.battle_medal)))
      str = Consts.GetInstance().UNIQUE_ICON_BATTLE_MEDAL;
    return str;
  }

  protected string GetRewardTypeUnitText(MasterDataTable.CommonRewardType type)
  {
    string str = "";
    if (type != MasterDataTable.CommonRewardType.money && type != MasterDataTable.CommonRewardType.peculiar && (type != MasterDataTable.CommonRewardType.player_exp && type != MasterDataTable.CommonRewardType.unit_exp) && (type != MasterDataTable.CommonRewardType.gear_experience_point && type != MasterDataTable.CommonRewardType.gear_training_point && (type != MasterDataTable.CommonRewardType.coin && type != MasterDataTable.CommonRewardType.recover)) && (type != MasterDataTable.CommonRewardType.max_unit && type != MasterDataTable.CommonRewardType.max_item && (type != MasterDataTable.CommonRewardType.medal && type != MasterDataTable.CommonRewardType.friend_point) && (type != MasterDataTable.CommonRewardType.deck && type != MasterDataTable.CommonRewardType.awake_skill && type == MasterDataTable.CommonRewardType.battle_medal)))
      str = Consts.GetInstance().UNIQUE_ICON_MEDAL_COUNT;
    return str;
  }

  public IEnumerator Init(ResultMenuBase.BonusReward reward, int totalWin)
  {
    Future<GameObject> uniquePrefabF = Res.Icons.UniqueIconPrefab.Load<GameObject>();
    IEnumerator e = uniquePrefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    e = ColosseumUtility.CreateUniqueIcon(uniquePrefabF.Result, this.link_Icon.transform, (MasterDataTable.CommonRewardType) reward.reward_type_id, reward.reward_id, 0, false);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.TxtDescription2.SetTextLocalize(Consts.Format(Consts.GetInstance().COLOSSEUM_0023418_TEXT2, (IDictionary) new Hashtable()
    {
      {
        (object) "name",
        (object) this.GetRewardTypeText((MasterDataTable.CommonRewardType) reward.reward_type_id)
      },
      {
        (object) "value",
        (object) reward.reward_quantity.ToLocalizeNumberText()
      },
      {
        (object) "unit",
        (object) this.GetRewardTypeUnitText((MasterDataTable.CommonRewardType) reward.reward_type_id)
      }
    }));
    this.TxtDescription1.SetTextLocalize(Consts.Format(Consts.GetInstance().COLOSSEUM_0023418_TEXT1, (IDictionary) new Hashtable()
    {
      {
        (object) "cnt",
        (object) totalWin
      }
    }));
  }

  public void SetCallback(System.Action callback)
  {
    this.onCallback = callback;
  }

  public virtual void IbtnOK()
  {
    if (this.onCallback != null)
      this.onCallback();
    Singleton<PopupManager>.GetInstance().onDismiss();
  }

  public override void onBackButton()
  {
    this.IbtnOK();
  }
}
