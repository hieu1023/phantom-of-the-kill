﻿// Decompiled with JetBrains decompiler
// Type: BattleUI05MissionCompleteBonusMenu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System.Collections;
using UnityEngine;

public class BattleUI05MissionCompleteBonusMenu : ResultMenuBase
{
  public GameObject IconObject;
  public UILabel CompleteText;

  public override IEnumerator Init(BattleInfo info, BattleEnd result)
  {
    BattleEndMission_complete_rewards[] missionCompleteRewards = result.mission_complete_rewards;
    int rewardQuantity = missionCompleteRewards[0].reward_quantity;
    int rewardTypeID = missionCompleteRewards[0].reward_type_id;
    int rewardId = missionCompleteRewards[0].reward_id;
    this.CompleteText.SetText(missionCompleteRewards[0].message);
    CreateIconObject target = this.IconObject.GetOrAddComponent<CreateIconObject>();
    IEnumerator e = target.CreateThumbnail((MasterDataTable.CommonRewardType) rewardTypeID, rewardId, rewardQuantity, true, true, new CommonQuestType?(), false);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if (rewardTypeID == 1 || rewardTypeID == 24)
    {
      UnitIcon component = target.GetIcon().GetComponent<UnitIcon>();
      component.setLevelText(1.ToLocalizeNumberText());
      component.ShowBottomInfo(UnitSortAndFilter.SORT_TYPES.Level);
    }
  }
}
