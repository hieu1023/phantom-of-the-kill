﻿// Decompiled with JetBrains decompiler
// Type: BattleHealCharacterInfoBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleHealCharacterInfoBase : NGBattleMenuBase
{
  [SerializeField]
  protected UILabel TxtCharaname;
  [SerializeField]
  protected UILabel TxtHpNumber;
  [SerializeField]
  protected Transform ImageParent;
  [SerializeField]
  protected int depth;
  [SerializeField]
  protected NGTweenGaugeScale hpBar;
  [SerializeField]
  protected NGTweenGaugeScale consumeBar;
  [SerializeField]
  private UISprite slcCountry;
  protected AttackStatus[] attacks;
  protected BL.UnitPosition currentUnit;
  protected AttackStatus currentAttack;

  public virtual IEnumerator Init(BL.UnitPosition up, AttackStatus[] attacks_)
  {
    BL.Unit unit = up.unit;
    this.currentUnit = up;
    this.attacks = attacks_;
    if ((UnityEngine.Object) this.slcCountry != (UnityEngine.Object) null)
    {
      this.slcCountry.gameObject.SetActive(false);
      if (unit.unit.country_attribute.HasValue)
      {
        this.slcCountry.gameObject.SetActive(true);
        unit.unit.SetCuntrySpriteName(ref this.slcCountry);
      }
    }
    this.TxtCharaname.SetTextLocalize(unit.unit.name);
    this.setHPNumbers(unit.hp.ToString());
    Future<GameObject> future = unit.unit.LoadStory();
    IEnumerator e = future.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.ImageParent.Clear();
    GameObject go = future.Result.Clone(this.ImageParent);
    unit.unit.SetStoryData(go, "normal", new int?());
    e = unit.unit.SetLargeSpriteWithMask(go, this.maskResource().Load<Texture2D>(), this.depth, 0, 0);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public void setHPNumbers(string hp)
  {
    this.TxtHpNumber.SetTextLocalize(hp);
  }

  public BL.UnitPosition getCurrent()
  {
    return this.currentUnit;
  }

  public AttackStatus getCurrentAttack()
  {
    return this.currentAttack;
  }

  public int getCurrentAttackIndex()
  {
    return ((IEnumerable<AttackStatus>) this.attacks).FirstIndexOrNull<AttackStatus>((Func<AttackStatus, bool>) (x => x == this.currentAttack)).Value;
  }

  protected virtual ResourceObject maskResource()
  {
    return (ResourceObject) null;
  }
}
