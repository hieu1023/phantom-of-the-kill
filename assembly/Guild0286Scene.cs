﻿// Decompiled with JetBrains decompiler
// Type: Guild0286Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System;
using System.Collections;
using UnityEngine;

public class Guild0286Scene : NGSceneBase
{
  [SerializeField]
  private Guild0286Menu menu;

  public static void ChangeScene(bool bStack)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("guild028_6", bStack, (object[]) Array.Empty<object>());
  }

  public IEnumerator onStartSceneAsync()
  {
    Guild0286Scene guild0286Scene = this;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    // ISSUE: reference to a compiler-generated method
    Future<WebAPI.Response.GuildGiftReceiveList> receive = WebAPI.GuildGiftReceiveList(false, new System.Action<WebAPI.Response.UserError>(guild0286Scene.\u003ConStartSceneAsync\u003Eb__2_0));
    IEnumerator e = receive.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if (receive.Result != null)
    {
      e = guild0286Scene.menu.Init(receive.Result.player_gift);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      Singleton<CommonRoot>.GetInstance().isLoading = false;
    }
  }

  public void onStartScene()
  {
  }
}
