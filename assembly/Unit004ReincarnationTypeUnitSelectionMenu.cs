﻿// Decompiled with JetBrains decompiler
// Type: Unit004ReincarnationTypeUnitSelectionMenu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Unit004ReincarnationTypeUnitSelectionMenu : UnitMenuBase
{
  [SerializeField]
  private GameObject dirNoUnit;
  private UnitTypeTicket ticket_;
  public bool isPopupOpen;

  protected override void Update()
  {
    base.Update();
    this.ScrollUpdate();
  }

  public virtual IEnumerator Init(
    Player player,
    PlayerUnit[] playerUnits,
    bool isEquip,
    UnitTypeTicket ticket)
  {
    Unit004ReincarnationTypeUnitSelectionMenu unitSelectionMenu = this;
    unitSelectionMenu.ticket_ = ticket;
    yield return (object) unitSelectionMenu.Initialize();
    PlayerUnit[] filteringUnits = unitSelectionMenu.getFilteringUnits(playerUnits);
    unitSelectionMenu.InitializeInfo((IEnumerable<PlayerUnit>) filteringUnits, (IEnumerable<PlayerMaterialUnit>) null, Persist.unit004ReincarnationTypeAndFilter, isEquip, false, true, true, false, (System.Action) null, 0);
    yield return (object) unitSelectionMenu.CreateUnitIcon();
    Singleton<PopupManager>.GetInstance().closeAll(false);
    unitSelectionMenu.isPopupOpen = false;
    unitSelectionMenu.lastReferenceUnitID = -1;
    unitSelectionMenu.InitializeEnd();
  }

  public IEnumerator reload(PlayerUnit[] playerUnits)
  {
    // ISSUE: reference to a compiler-generated field
    int num = this.\u003C\u003E1__state;
    Unit004ReincarnationTypeUnitSelectionMenu unitSelectionMenu = this;
    if (num != 0)
    {
      if (num != 1)
        return false;
      // ISSUE: reference to a compiler-generated field
      this.\u003C\u003E1__state = -1;
      return false;
    }
    // ISSUE: reference to a compiler-generated field
    this.\u003C\u003E1__state = -1;
    PlayerUnit[] filteringUnits = unitSelectionMenu.getFilteringUnits(playerUnits);
    // ISSUE: reference to a compiler-generated field
    this.\u003C\u003E2__current = (object) unitSelectionMenu.UpdateInfoAndScroll(filteringUnits, (PlayerMaterialUnit[]) null);
    // ISSUE: reference to a compiler-generated field
    this.\u003C\u003E1__state = 1;
    return true;
  }

  private PlayerUnit[] getFilteringUnits(PlayerUnit[] playerUnits)
  {
    List<PlayerUnit> source = new List<PlayerUnit>();
    List<PlayerUnit> usableUnitList = this.getUsableUnitList(((IEnumerable<PlayerUnit>) playerUnits).Where<PlayerUnit>((Func<PlayerUnit, bool>) (x => x.unit.IsNormalUnit)));
    UnitTypeTicket unitTypeTicket = ((IEnumerable<UnitTypeTicket>) MasterData.UnitTypeTicketList).FirstOrDefault<UnitTypeTicket>((Func<UnitTypeTicket, bool>) (x => x.ID == this.ticket_.ID));
    string[] strArray = unitTypeTicket.ticketTypeParam.Split(',');
    List<int> idList = new List<int>();
    foreach (string str in strArray)
    {
      if (!string.IsNullOrEmpty(str))
      {
        int num = !str.Contains(".") ? Convert.ToInt32(str) : Convert.ToInt32(Convert.ToDouble(str));
        idList.Add(num);
      }
    }
    switch (unitTypeTicket.ticketTypeID_UnitTypeTicketType)
    {
      case 1:
        source = usableUnitList;
        break;
      case 2:
        source = this.getCharacterFilterList(usableUnitList, idList);
        break;
      case 3:
        source = this.getUnitFilterList(usableUnitList, idList);
        break;
      case 4:
        source = this.getKindFilterList(usableUnitList, idList);
        break;
      case 5:
        source = this.getGroupClothingFilterList(usableUnitList, idList);
        break;
      case 6:
        source = this.getGroupGenerationFilterList(usableUnitList, idList);
        break;
      case 7:
        source = this.getGroupLargeFilterList(usableUnitList, idList);
        break;
      case 8:
        source = this.getGroupSmallFilterList(usableUnitList, idList);
        break;
      case 9:
        source = this.getElementFilterList(usableUnitList, idList);
        break;
      case 10:
        source = this.getUnitTypeFilterList(usableUnitList, idList);
        break;
      case 11:
        source = this.getJobFilterList(usableUnitList, idList);
        break;
    }
    this.dirNoUnit.SetActive(!source.Any<PlayerUnit>());
    return source.ToArray();
  }

  private List<PlayerUnit> getUsableUnitList(IEnumerable<PlayerUnit> srcUnitList)
  {
    List<PlayerUnit> playerUnitList = new List<PlayerUnit>();
    UnitTypeTicketUnusable[] ticketUnusableList = MasterData.UnitTypeTicketUnusableList;
    if (((IEnumerable<UnitTypeTicketUnusable>) ticketUnusableList).Any<UnitTypeTicketUnusable>())
    {
      foreach (PlayerUnit srcUnit in srcUnitList)
      {
        PlayerUnit unit = srcUnit;
        if (((IEnumerable<UnitTypeTicketUnusable>) ticketUnusableList).FirstOrDefault<UnitTypeTicketUnusable>((Func<UnitTypeTicketUnusable, bool>) (x => x.ID == unit.unit.character_UnitCharacter)) == null)
          playerUnitList.Add(unit);
      }
    }
    else
    {
      foreach (PlayerUnit srcUnit in srcUnitList)
        playerUnitList.Add(srcUnit);
    }
    return playerUnitList;
  }

  private List<PlayerUnit> getCharacterFilterList(
    List<PlayerUnit> srcUnitList,
    List<int> idList)
  {
    List<PlayerUnit> playerUnitList = new List<PlayerUnit>();
    foreach (PlayerUnit srcUnit in srcUnitList)
    {
      PlayerUnit unit = srcUnit;
      if (idList.FirstOrDefault<int>((Func<int, bool>) (x => x == unit.unit.character_UnitCharacter)) > 0)
        playerUnitList.Add(unit);
    }
    return playerUnitList;
  }

  private List<PlayerUnit> getUnitFilterList(
    List<PlayerUnit> srcUnitList,
    List<int> idList)
  {
    List<PlayerUnit> playerUnitList = new List<PlayerUnit>();
    foreach (PlayerUnit srcUnit in srcUnitList)
    {
      PlayerUnit unit = srcUnit;
      if (idList.FirstOrDefault<int>((Func<int, bool>) (x => x == unit.unit.ID)) > 0)
        playerUnitList.Add(unit);
    }
    return playerUnitList;
  }

  private List<PlayerUnit> getKindFilterList(
    List<PlayerUnit> srcUnitList,
    List<int> idList)
  {
    List<PlayerUnit> playerUnitList = new List<PlayerUnit>();
    foreach (PlayerUnit srcUnit in srcUnitList)
    {
      PlayerUnit unit = srcUnit;
      if (idList.FirstOrDefault<int>((Func<int, bool>) (x => x == unit.unit.kind.ID)) > 0)
        playerUnitList.Add(unit);
    }
    return playerUnitList;
  }

  private List<PlayerUnit> getGroupClothingFilterList(
    List<PlayerUnit> srcUnitList,
    List<int> idList)
  {
    List<PlayerUnit> playerUnitList = new List<PlayerUnit>();
    foreach (PlayerUnit srcUnit in srcUnitList)
    {
      UnitGroup groupInfo = this.getGroupInfo(srcUnit.unit.ID);
      if (groupInfo != null && idList.FirstOrDefault<int>((Func<int, bool>) (x => x == groupInfo.group_clothing_category_id_UnitGroupClothingCategory || x == groupInfo.group_clothing_category_id_2_UnitGroupClothingCategory)) > 0)
        playerUnitList.Add(srcUnit);
    }
    return playerUnitList;
  }

  private List<PlayerUnit> getGroupGenerationFilterList(
    List<PlayerUnit> srcUnitList,
    List<int> idList)
  {
    List<PlayerUnit> playerUnitList = new List<PlayerUnit>();
    foreach (PlayerUnit srcUnit in srcUnitList)
    {
      UnitGroup groupInfo = this.getGroupInfo(srcUnit.unit.ID);
      if (groupInfo != null && idList.FirstOrDefault<int>((Func<int, bool>) (x => x == groupInfo.group_generation_category_id_UnitGroupGenerationCategory)) > 0)
        playerUnitList.Add(srcUnit);
    }
    return playerUnitList;
  }

  private List<PlayerUnit> getGroupLargeFilterList(
    List<PlayerUnit> srcUnitList,
    List<int> idList)
  {
    List<PlayerUnit> playerUnitList = new List<PlayerUnit>();
    foreach (PlayerUnit srcUnit in srcUnitList)
    {
      UnitGroup groupInfo = this.getGroupInfo(srcUnit.unit.ID);
      if (groupInfo != null && idList.FirstOrDefault<int>((Func<int, bool>) (x => x == groupInfo.group_large_category_id_UnitGroupLargeCategory)) > 0)
        playerUnitList.Add(srcUnit);
    }
    return playerUnitList;
  }

  private List<PlayerUnit> getGroupSmallFilterList(
    List<PlayerUnit> srcUnitList,
    List<int> idList)
  {
    List<PlayerUnit> playerUnitList = new List<PlayerUnit>();
    foreach (PlayerUnit srcUnit in srcUnitList)
    {
      UnitGroup groupInfo = this.getGroupInfo(srcUnit.unit.ID);
      if (groupInfo != null && idList.FirstOrDefault<int>((Func<int, bool>) (x => x == groupInfo.group_small_category_id_UnitGroupSmallCategory)) > 0)
        playerUnitList.Add(srcUnit);
    }
    return playerUnitList;
  }

  private UnitGroup getGroupInfo(int unit_id)
  {
    UnitGroup unitGroup = (UnitGroup) null;
    Dictionary<int, UnitGroup> dictionary = ((IEnumerable<UnitGroup>) MasterData.UnitGroupList).ToDictionary<UnitGroup, int>((Func<UnitGroup, int>) (x => x.unit_id));
    if (dictionary == null)
      return (UnitGroup) null;
    dictionary.TryGetValue(unit_id, out unitGroup);
    return unitGroup;
  }

  private List<PlayerUnit> getElementFilterList(
    List<PlayerUnit> srcUnitList,
    List<int> idList)
  {
    List<PlayerUnit> playerUnitList = new List<PlayerUnit>();
    foreach (PlayerUnit srcUnit in srcUnitList)
    {
      int element = (int) srcUnit.unit.GetElement();
      if (idList.FirstOrDefault<int>((Func<int, bool>) (x => x == element)) > 0)
        playerUnitList.Add(srcUnit);
    }
    return playerUnitList;
  }

  private List<PlayerUnit> getUnitTypeFilterList(
    List<PlayerUnit> srcUnitList,
    List<int> idList)
  {
    List<PlayerUnit> playerUnitList = new List<PlayerUnit>();
    foreach (PlayerUnit srcUnit in srcUnitList)
    {
      PlayerUnit unit = srcUnit;
      if (idList.FirstOrDefault<int>((Func<int, bool>) (x => x == unit._unit_type)) > 0)
        playerUnitList.Add(unit);
    }
    return playerUnitList;
  }

  private List<PlayerUnit> getJobFilterList(
    List<PlayerUnit> srcUnitList,
    List<int> idList)
  {
    List<PlayerUnit> playerUnitList = new List<PlayerUnit>();
    foreach (PlayerUnit srcUnit in srcUnitList)
    {
      PlayerUnit unit = srcUnit;
      if (idList.FirstOrDefault<int>((Func<int, bool>) (x => x == unit.unit.job_UnitJob)) > 0)
        playerUnitList.Add(unit);
    }
    return playerUnitList;
  }

  private void ChangeScene(UnitIconBase unitIcon)
  {
    if (this.isPopupOpen)
      return;
    if (unitIcon.PlayerUnit != (PlayerUnit) null)
    {
      this.lastReferenceUnitID = unitIcon.PlayerUnit.id;
      this.lastReferenceUnitIndex = this.GetUnitInfoDisplayIndex(unitIcon.PlayerUnit);
      this.StartCoroutine(this.openPopupTypeSelection(unitIcon.PlayerUnit));
      this.isPopupOpen = true;
    }
    else
      Debug.LogWarning((object) "PlayerUnit Null : Unit004ReincarnationTypeUnitSelectionMenu");
  }

  private IEnumerator openPopupTypeSelection(PlayerUnit PlayerUnit)
  {
    Unit004ReincarnationTypeUnitSelectionMenu menu = this;
    Future<GameObject> prefabF = new ResourceObject("Prefabs/unit004_Reincarnation_Type/popup_004_Reincarnation_Type_exchange_confirmation__anim_popup01").Load<GameObject>();
    IEnumerator e = prefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    e = Singleton<PopupManager>.GetInstance().open(prefabF.Result, false, false, false, true, false, false, "SE_1006").GetComponent<Unit004ReincarnationTypeTicketPopupExchangeMenu>().coInitialize(menu.ticket_, PlayerUnit, menu);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  protected override IEnumerator CreateUnitIcon(
    int info_index,
    int unit_index,
    PlayerUnit baseUnit = null)
  {
    IEnumerator e = base.CreateUnitIcon(info_index, unit_index, baseUnit);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.CreateUnitIconAction(info_index, unit_index);
  }

  protected override void CreateUnitIconCache(int info_index, int unit_index, PlayerUnit baseUnit = null)
  {
    base.CreateUnitIconCache(info_index, unit_index, (PlayerUnit) null);
    this.CreateUnitIconAction(info_index, unit_index);
  }

  private void CreateUnitIconAction(int info_index, int unit_index)
  {
    this.allUnitIcons[unit_index].onClick = (System.Action<UnitIconBase>) (ui => this.ChangeScene(ui));
  }
}
