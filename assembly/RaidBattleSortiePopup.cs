﻿// Decompiled with JetBrains decompiler
// Type: RaidBattleSortiePopup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class RaidBattleSortiePopup : BackButtonMenuBase
{
  [SerializeField]
  private UIButton[] largeButton = new UIButton[0];
  private bool isCompletedOverkillersDeck = true;
  private const float LINK_WIDTH = 92f;
  private const float LINK_DEFWIDTH = 114f;
  private const float scale = 0.8070176f;
  private const int FRIEND_INDEX = 5;
  [SerializeField]
  private UILabel lblLeaderSkillName;
  [SerializeField]
  private UILabel lblLeaderSkillDesc;
  [SerializeField]
  private GameObject objLeaderSkillZoom;
  [SerializeField]
  private GameObject slc_NotFriend_Skill;
  [SerializeField]
  private UILabel lblFriendSkillName;
  [SerializeField]
  private UILabel lblFriendSkillDesc;
  [SerializeField]
  private GameObject objFriendSkillZoom;
  [SerializeField]
  private UILabel lblNoFriend;
  [SerializeField]
  private UILabel lblDeckCombat;
  [SerializeField]
  private UILabel lblRecommendCombat;
  [SerializeField]
  protected GameObject[] linkCharacters;
  [SerializeField]
  protected GameObject[] linkUnabaibleIcons;
  [SerializeField]
  protected GameObject[] dir_Items;
  private GameObject unitIconPrefab;
  private GameObject itemIconPrefab;
  private RaidBattlePreparationPopup parent;
  private int mDeckCombat;
  private int mRecommendCombat;
  private GameObject skillDetailPrefab;
  private PlayerUnitLeader_skills leaderSkill;
  private PlayerUnitLeader_skills friendSkill;

  public IEnumerator InitializeAsync(
    RaidBattlePreparationPopup parent,
    PlayerUnit[] deck,
    int[] gray_out_unit_ids,
    PlayerUnit friend,
    PlayerItem[] supplys,
    string recommendCombat)
  {
    this.parent = parent;
    this.setButtonMode(RaidBattleSortiePopup.ButtonMode.Sortie);
    Future<GameObject> unitIconPrefabF;
    IEnumerator e;
    if ((UnityEngine.Object) this.unitIconPrefab == (UnityEngine.Object) null)
    {
      unitIconPrefabF = Res.Prefabs.UnitIcon.normal.Load<GameObject>();
      e = unitIconPrefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.unitIconPrefab = unitIconPrefabF.Result;
      unitIconPrefabF = (Future<GameObject>) null;
    }
    if ((UnityEngine.Object) this.itemIconPrefab == (UnityEngine.Object) null)
    {
      unitIconPrefabF = Res.Prefabs.ItemIcon.prefab.Load<GameObject>();
      e = unitIconPrefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.itemIconPrefab = unitIconPrefabF.Result;
      unitIconPrefabF = (Future<GameObject>) null;
    }
    if ((UnityEngine.Object) this.skillDetailPrefab == (UnityEngine.Object) null)
    {
      unitIconPrefabF = PopupSkillDetails.createPrefabLoader(false);
      yield return (object) unitIconPrefabF.Wait();
      this.skillDetailPrefab = unitIconPrefabF.Result;
      unitIconPrefabF = (Future<GameObject>) null;
    }
    this.setRecommendCombat(recommendCombat);
    yield return (object) this.SetDeck(deck, gray_out_unit_ids);
    yield return (object) this.SetFriendUnit(friend);
    yield return (object) this.SetSupplys(supplys);
    if (!this.isCompletedOverkillersDeck)
      this.setErrorOverkillersDeck();
  }

  public IEnumerator ReloadAsync(
    PlayerUnit[] deck,
    int[] gray_out_unit_ids,
    PlayerUnit friend,
    PlayerItem[] supplys)
  {
    this.DestroyAllUnitIcons();
    this.DestroyAllSupplyIcons();
    yield return (object) this.SetDeck(deck, gray_out_unit_ids);
    yield return (object) this.SetFriendUnit(friend);
    yield return (object) this.SetSupplys(supplys);
    if (!this.isCompletedOverkillersDeck)
      this.setErrorOverkillersDeck();
  }

  private IEnumerator SetDeck(PlayerUnit[] deck, int[] usedUnitIds)
  {
    RaidBattleSortiePopup battleSortiePopup = this;
    battleSortiePopup.leaderSkill = (PlayerUnitLeader_skills) null;
    foreach (GameObject linkUnabaibleIcon in battleSortiePopup.linkUnabaibleIcons)
      linkUnabaibleIcon.SetActive(true);
    OverkillersUtil.checkCompletedDeck(deck, out battleSortiePopup.isCompletedOverkillersDeck, (HashSet<int>) null, (bool[]) null);
    bool isGray = false;
    bool isGearBroken = false;
    for (int i = 0; i < deck.Length; ++i)
    {
      isGray |= ((IEnumerable<int>) usedUnitIds).Contains<int>(deck[i].id);
      isGearBroken |= deck[i].IsBrokenEquippedGear;
      yield return (object) battleSortiePopup.LoadUnitPrefab(i, deck[i], false, isGray);
      if (i == 0)
        battleSortiePopup.leaderSkill = deck[i].leader_skill;
      battleSortiePopup.linkUnabaibleIcons[i].SetActive(false);
    }
    if (battleSortiePopup.leaderSkill != null)
    {
      BattleskillSkill skill = battleSortiePopup.leaderSkill.skill;
      battleSortiePopup.lblLeaderSkillName.SetTextLocalize(skill.name);
      battleSortiePopup.lblLeaderSkillDesc.SetTextLocalize(skill.description);
      battleSortiePopup.objLeaderSkillZoom.SetActive(true);
    }
    else
    {
      battleSortiePopup.lblLeaderSkillName.SetText("---");
      battleSortiePopup.lblLeaderSkillDesc.SetText("-----");
      battleSortiePopup.objLeaderSkillZoom.SetActive(false);
    }
    if (isGearBroken)
      battleSortiePopup.setButtonMode(RaidBattleSortiePopup.ButtonMode.Repair);
    else if (isGray || deck.Length < 1 || !battleSortiePopup.isCompletedOverkillersDeck)
      battleSortiePopup.setButtonMode(RaidBattleSortiePopup.ButtonMode.Edit);
    else
      battleSortiePopup.setButtonMode(RaidBattleSortiePopup.ButtonMode.Sortie);
    battleSortiePopup.mDeckCombat = 0;
    // ISSUE: reference to a compiler-generated method
    ((IEnumerable<PlayerUnit>) deck).ForEach<PlayerUnit>(new System.Action<PlayerUnit>(battleSortiePopup.\u003CSetDeck\u003Eb__30_0));
    if (battleSortiePopup.mRecommendCombat == 0)
      battleSortiePopup.lblDeckCombat.SetTextLocalize(battleSortiePopup.mDeckCombat);
    else if (battleSortiePopup.mRecommendCombat <= battleSortiePopup.mDeckCombat)
      battleSortiePopup.lblDeckCombat.SetTextLocalize(Consts.Format(Consts.GetInstance().QUEST_0028_DECK_ENOUGH, (IDictionary) new Hashtable()
      {
        {
          (object) "combat",
          (object) battleSortiePopup.mDeckCombat.ToString()
        }
      }));
    else
      battleSortiePopup.lblDeckCombat.SetTextLocalize(Consts.Format(Consts.GetInstance().QUEST_0028_DECK_SHORT, (IDictionary) new Hashtable()
      {
        {
          (object) "combat",
          (object) battleSortiePopup.mDeckCombat.ToString()
        }
      }));
  }

  public IEnumerator SetFriendUnit(PlayerUnit friend)
  {
    this.DestroyFriendUnitIcon();
    this.friendSkill = (PlayerUnitLeader_skills) null;
    if (friend == (PlayerUnit) null)
    {
      this.slc_NotFriend_Skill.SetActive(true);
      this.lblNoFriend.SetTextLocalize(Consts.GetInstance().QUEST_0028_INDICATOR_NOT_RENTAL);
      yield return (object) this.LoadUnitPrefab(5, (PlayerUnit) null, true, false);
    }
    else
    {
      this.slc_NotFriend_Skill.SetActive(false);
      PlayerUnit unit = friend;
      if (unit.leader_skills != null)
        this.friendSkill = unit.leader_skill;
      yield return (object) this.LoadUnitPrefab(5, unit, true, false);
    }
    if (this.friendSkill != null)
    {
      BattleskillSkill skill = this.friendSkill.skill;
      this.lblFriendSkillDesc.SetText(skill.description);
      this.lblFriendSkillName.SetText(skill.name);
      this.objFriendSkillZoom.SetActive(true);
    }
    else
    {
      this.lblFriendSkillName.SetText("---");
      this.lblFriendSkillDesc.SetText("-----");
      this.objFriendSkillZoom.SetActive(false);
    }
  }

  private IEnumerator SetSupplys(PlayerItem[] supplys)
  {
    for (int i = 0; i < supplys.Length; ++i)
    {
      GameObject gameObject = this.itemIconPrefab.Clone(this.dir_Items[i].transform);
      gameObject.transform.localScale = new Vector3(0.8070176f, 0.8070176f);
      IEnumerator e = gameObject.GetComponent<ItemIcon>().InitByPlayerItem(supplys[i]);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    for (int length = supplys.Length; length < Consts.GetInstance().DECK_SUPPLY_MAX; ++length)
    {
      GameObject gameObject = this.itemIconPrefab.Clone(this.dir_Items[length].transform);
      gameObject.transform.localScale = new Vector3(0.8070176f, 0.8070176f);
      ItemIcon component = gameObject.GetComponent<ItemIcon>();
      component.SetModeSupply();
      component.SetEmpty(true);
    }
  }

  private void setErrorOverkillersDeck()
  {
    this.slc_NotFriend_Skill.gameObject.SetActive(true);
    this.lblNoFriend.SetTextLocalize(Consts.GetInstance().QUEST_0028_INDICATOR_LIMITED_OVERKILLERS);
  }

  private void setRecommendCombat(string combat)
  {
    int.TryParse(combat, out this.mRecommendCombat);
    this.lblRecommendCombat.SetTextLocalize(combat);
  }

  private IEnumerator LoadUnitPrefab(
    int index,
    PlayerUnit unit,
    bool isFriend,
    bool isGray)
  {
    GameObject gameObject = this.unitIconPrefab.Clone(this.linkCharacters[index].transform);
    gameObject.transform.localScale = new Vector3(0.8070176f, 0.8070176f);
    UnitIcon iconScript = gameObject.GetComponent<UnitIcon>();
    IEnumerator e = iconScript.setSimpleUnit(unit);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    iconScript.setLevelText(unit);
    iconScript.ShowBottomInfo(UnitSortAndFilter.SORT_TYPES.Level);
    if (unit != (PlayerUnit) null)
    {
      iconScript.onLongPress = (System.Action<UnitIconBase>) (x => this.parent.OnLongPressUnitIcon(unit, isFriend));
      iconScript.BreakWeapon = !isFriend && unit.IsBrokenEquippedGear;
      iconScript.SpecialIcon = false;
    }
    else
      iconScript.SetEmpty();
    iconScript.onClick = (System.Action<UnitIconBase>) (x => this.parent.OnClickUnitIcon(unit, isFriend));
    iconScript.Favorite = false;
    iconScript.Gray = isGray;
    iconScript.UnitUsed = isGray;
    iconScript.SetupDeckStatusBlink();
  }

  private void DestroyAllUnitIcons()
  {
    if (this.linkCharacters == null)
      return;
    foreach (GameObject linkCharacter in this.linkCharacters)
    {
      UnitIcon componentInChildren = linkCharacter.GetComponentInChildren<UnitIcon>();
      if ((UnityEngine.Object) componentInChildren != (UnityEngine.Object) null)
        UnityEngine.Object.Destroy((UnityEngine.Object) componentInChildren.gameObject);
    }
  }

  private void DestroyFriendUnitIcon()
  {
    if (this.linkCharacters == null)
      return;
    UnitIcon componentInChildren = this.linkCharacters[5].GetComponentInChildren<UnitIcon>();
    if (!((UnityEngine.Object) componentInChildren != (UnityEngine.Object) null))
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) componentInChildren.gameObject);
  }

  private void DestroyAllSupplyIcons()
  {
    foreach (GameObject dirItem in this.dir_Items)
    {
      foreach (Component child in dirItem.transform.GetChildren())
        UnityEngine.Object.Destroy((UnityEngine.Object) child.gameObject);
    }
  }

  private void setButtonMode(RaidBattleSortiePopup.ButtonMode mode)
  {
    for (int index = 0; index < this.largeButton.Length; ++index)
    {
      if ((RaidBattleSortiePopup.ButtonMode) index == mode)
        this.largeButton[index].gameObject.SetActive(true);
      else
        this.largeButton[index].gameObject.SetActive(false);
    }
  }

  public void OnAutoDeckEditButton()
  {
    this.parent.OnAutoDeckEditButton();
  }

  public void OnDeckEditButton()
  {
    this.parent.OnDeckEditButton();
  }

  public void OnGearEquipButton()
  {
    this.parent.OnGearEquipButton();
  }

  public void OnGearRepairButton()
  {
    this.parent.OnGearRepairButton();
  }

  public void OnSupplyEquipButton()
  {
    this.parent.OnSupplyEquipButton();
  }

  public void OnBattleConfigButton()
  {
    this.parent.OnBattleConfigButton();
  }

  public void onSortieButton()
  {
    this.parent.OnSortieButton();
  }

  public override void onBackButton()
  {
    if (this.IsPushAndSet())
      return;
    this.parent.OnBackButton();
  }

  public void onClickedLeaderSkillZoom()
  {
    if (this.leaderSkill == null || (UnityEngine.Object) this.skillDetailPrefab == (UnityEngine.Object) null || this.IsPushAndSet())
      return;
    PopupSkillDetails.show(this.skillDetailPrefab, new PopupSkillDetails.Param(this.leaderSkill), false, new System.Action(this.onClosedSkillZoom), false);
  }

  public void onClickedFriendSkillZoom()
  {
    if (this.friendSkill == null || (UnityEngine.Object) this.skillDetailPrefab == (UnityEngine.Object) null || this.IsPushAndSet())
      return;
    PopupSkillDetails.show(this.skillDetailPrefab, new PopupSkillDetails.Param(this.friendSkill), false, new System.Action(this.onClosedSkillZoom), false);
  }

  private void onClosedSkillZoom()
  {
    this.IsPush = false;
  }

  public enum ButtonMode
  {
    Sortie,
    Edit,
    Repair,
  }
}
