﻿// Decompiled with JetBrains decompiler
// Type: Unit004JobDialogUp
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Unit004JobDialogUp : BackButtonMenuBase
{
  private bool activeLevelUpButton = true;
  private List<int> materialIDs = new List<int>();
  [SerializeField]
  private Transform dirJobAfter;
  [SerializeField]
  private UIGrid dynMaterial;
  [SerializeField]
  private UILabel[] txtMaterialNeedValue;
  [SerializeField]
  private UILabel[] txtMaterialPossessedValue;
  [SerializeField]
  private UILabel txtZenyNeed;
  [SerializeField]
  private UILabel txtZenyPossession;
  [SerializeField]
  private UILabel txtToutaNeed;
  [SerializeField]
  private UILabel txtToutaPossession;
  [SerializeField]
  private SpreadColorButton ibtnLevelUp;
  [SerializeField]
  private GameObject DialogBox;
  [SerializeField]
  private UILabel TxtDialogMaterialName;
  [SerializeField]
  private UILabel TxtDialogMaterialPlace;
  private GameObject JobAfterPanel;
  private GameObject unitPrefab;
  private GameObject popupLevelUpPrefab;
  private PlayerUnit _unit;
  private PlayerUnitJob_abilities _jobAbility;
  private int[] materialIDsArray;
  private bool isClassChangeScene;
  private System.Action onUpdatedJobAbility_;
  private int countIconInitializing_;

  public IEnumerator Init(
    PlayerUnit unit,
    PlayerUnitJob_abilities jobAbility,
    System.Action eventUpdatedJobAbility,
    bool isClassChangeScene = false)
  {
    Unit004JobDialogUp unit004JobDialogUp = this;
    UIWidget w = unit004JobDialogUp.GetComponent<UIWidget>();
    if ((UnityEngine.Object) w != (UnityEngine.Object) null)
      w.alpha = 0.0f;
    unit004JobDialogUp.ibtnLevelUp.isEnabled = false;
    unit004JobDialogUp.activeLevelUpButton = true;
    unit004JobDialogUp.DialogBox.SetActive(false);
    unit004JobDialogUp.onUpdatedJobAbility_ = eventUpdatedJobAbility;
    unit004JobDialogUp._unit = unit;
    unit004JobDialogUp._jobAbility = jobAbility;
    Modified<Player> p = SMManager.Observe<Player>();
    IEnumerator e = unit004JobDialogUp.LoadPrefabs(isClassChangeScene);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    e = unit004JobDialogUp.JobAfterPanel.Clone(unit004JobDialogUp.dirJobAfter).GetComponent<Unit004JobAfter>().Init(false, jobAbility, (PlayerUnitJob_abilities) null, true);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    JobCharacteristicsLevelupPattern levelPattern = jobAbility.current_levelup_pattern;
    KeyValuePair<UnitUnit, int>[] keyValuePairArray = jobAbility.getLevelupMaterials(unit004JobDialogUp._unit, -1)?.ToArray() ?? new KeyValuePair<UnitUnit, int>[0];
    unit004JobDialogUp.materialIDs.Clear();
    unit004JobDialogUp.countIconInitializing_ = 0;
    PlayerMaterialUnit[] playerUnitMaterial = SMManager.Get<PlayerMaterialUnit[]>();
    for (int index = 0; index < unit004JobDialogUp.txtMaterialNeedValue.Length; ++index)
    {
      if (index < keyValuePairArray.Length)
        unit004JobDialogUp.ControlAndAddItem(playerUnitMaterial, keyValuePairArray[index].Key, new int?(keyValuePairArray[index].Value), unit004JobDialogUp.txtMaterialNeedValue[index], unit004JobDialogUp.txtMaterialPossessedValue[index]);
      else
        unit004JobDialogUp.ControlAndAddItem(playerUnitMaterial, (UnitUnit) null, new int?(), unit004JobDialogUp.txtMaterialNeedValue[index], unit004JobDialogUp.txtMaterialPossessedValue[index]);
    }
    while (unit004JobDialogUp.countIconInitializing_ > 0)
      yield return (object) null;
    unit004JobDialogUp.dynMaterial.Reposition();
    unit004JobDialogUp.materialIDsArray = unit004JobDialogUp.materialIDs.ToArray();
    unit004JobDialogUp.txtZenyNeed.text = levelPattern.amount.ToString();
    unit004JobDialogUp.txtZenyPossession.text = p.Value.money.ToString();
    if (p.Value.money < (long) levelPattern.amount)
    {
      unit004JobDialogUp.txtZenyNeed.color = Color.red;
      unit004JobDialogUp.activeLevelUpButton = false;
    }
    unit004JobDialogUp.txtToutaNeed.text = levelPattern.culled_value.ToString();
    unit004JobDialogUp.txtToutaPossession.text = (double) unit004JobDialogUp._unit.unityTotal < 99.0 ? unit004JobDialogUp._unit.unityTotal.ToString("f1") : unit004JobDialogUp._unit.unityTotal.ToString();
    double unityTotal = (double) unit004JobDialogUp._unit.unityTotal;
    int? culledValue = levelPattern.culled_value;
    float? nullable = culledValue.HasValue ? new float?((float) culledValue.GetValueOrDefault()) : new float?();
    double valueOrDefault = (double) nullable.GetValueOrDefault();
    if (unityTotal < valueOrDefault & nullable.HasValue)
    {
      unit004JobDialogUp.txtToutaNeed.color = Color.red;
      unit004JobDialogUp.activeLevelUpButton = false;
    }
    unit004JobDialogUp.ibtnLevelUp.isEnabled = unit004JobDialogUp.activeLevelUpButton;
    if ((UnityEngine.Object) w != (UnityEngine.Object) null)
      w.alpha = 1f;
  }

  private void ControlAndAddItem(
    PlayerMaterialUnit[] playerUnitMaterial,
    UnitUnit materialRequested,
    int? quantityRequested,
    UILabel txtMaterialNeededLabel,
    UILabel txtMaterialPossessedLabel)
  {
    if (materialRequested != null)
    {
      PlayerMaterialUnit playerMaterialUnit = Array.Find<PlayerMaterialUnit>(playerUnitMaterial, (Predicate<PlayerMaterialUnit>) (x => x._unit == materialRequested.ID));
      if (playerMaterialUnit != null)
      {
        bool isGray = false;
        int quantity = playerMaterialUnit.quantity;
        int? nullable = quantityRequested;
        int valueOrDefault = nullable.GetValueOrDefault();
        if (quantity < valueOrDefault & nullable.HasValue)
        {
          this.activeLevelUpButton = false;
          isGray = true;
          txtMaterialNeededLabel.color = Color.red;
        }
        txtMaterialPossessedLabel.SetTextLocalize(playerMaterialUnit.quantity.ToString() + "体所持");
        this.AddItemIcon(materialRequested, isGray);
        this.materialIDs.Add(playerMaterialUnit.id);
      }
      else
      {
        this.activeLevelUpButton = false;
        txtMaterialNeededLabel.color = Color.red;
        this.AddItemIcon(materialRequested, true);
        txtMaterialPossessedLabel.SetTextLocalize("0体所持");
      }
      txtMaterialNeededLabel.SetTextLocalize(quantityRequested.ToString());
    }
    else
    {
      this.AddItemIcon((UnitUnit) null, false);
      txtMaterialPossessedLabel.gameObject.SetActive(false);
    }
  }

  private void AddItemIcon(UnitUnit sozai = null, bool isGray = false)
  {
    UnitIconBase component = this.unitPrefab.CloneAndGetComponent<UnitIconBase>(this.dynMaterial.transform);
    if (sozai != null)
    {
      Singleton<PopupManager>.GetInstance().monitorCoroutine(this.doIconInitalize(component, sozai, isGray));
    }
    else
    {
      UnitIcon unitIcon = (UnitIcon) component;
      unitIcon.Button.enabled = false;
      unitIcon.buttonBoxCollider.enabled = false;
      unitIcon.SetEmpty();
    }
  }

  private IEnumerator doIconInitalize(UnitIconBase icon, UnitUnit sozai, bool isGray)
  {
    // ISSUE: reference to a compiler-generated field
    int num = this.\u003C\u003E1__state;
    Unit004JobDialogUp unit004JobDialogUp = this;
    if (num != 0)
    {
      if (num != 1)
        return false;
      // ISSUE: reference to a compiler-generated field
      this.\u003C\u003E1__state = -1;
      icon.Gray = isGray;
      // ISSUE: reference to a compiler-generated method
      icon.onClick = new System.Action<UnitIconBase>(unit004JobDialogUp.\u003CdoIconInitalize\u003Eb__26_0);
      --unit004JobDialogUp.countIconInitializing_;
      return false;
    }
    // ISSUE: reference to a compiler-generated field
    this.\u003C\u003E1__state = -1;
    ++unit004JobDialogUp.countIconInitializing_;
    // ISSUE: reference to a compiler-generated field
    this.\u003C\u003E2__current = (object) icon.SetUnit(sozai, sozai.GetElement(), false);
    // ISSUE: reference to a compiler-generated field
    this.\u003C\u003E1__state = 1;
    return true;
  }

  public IEnumerator LoadPrefabs(bool isClassChangeScene)
  {
    Future<GameObject> prefabF = Res.Prefabs.UnitIcon.normal.Load<GameObject>();
    IEnumerator e = prefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.unitPrefab = prefabF.Result;
    this.isClassChangeScene = isClassChangeScene;
    Future<GameObject> JobAfterPanelF = (Future<GameObject>) null;
    JobAfterPanelF = !Singleton<NGGameDataManager>.GetInstance().IsSea || isClassChangeScene ? Res.Prefabs.unit004_Job.Unit_job_after.Load<GameObject>() : Res.Prefabs.unit004_Job.Unit_job_after_sea.Load<GameObject>();
    e = JobAfterPanelF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.JobAfterPanel = JobAfterPanelF.Result;
    Future<GameObject> popupLevelUpPrefabf = (Future<GameObject>) null;
    popupLevelUpPrefabf = !Singleton<NGGameDataManager>.GetInstance().IsSea || isClassChangeScene ? Res.Prefabs.unit004_Job.popup_JobCharacteristic_Confirmation__anim_popup01.Load<GameObject>() : Res.Prefabs.unit004_Job.popup_JobCharacteristic_Confirmation_sea__anim_popup01.Load<GameObject>();
    e = popupLevelUpPrefabf.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.popupLevelUpPrefab = popupLevelUpPrefabf.Result;
  }

  private void ShowMaterialQuestInfo(UnitUnit material)
  {
    int num = !this.DialogBox.activeInHierarchy ? 1 : 0;
    this.DialogBox.SetActive(true);
    if (num != 0)
    {
      UITweener[] tweeners = NGTween.findTweeners(this.DialogBox, true);
      NGTween.playTweens(tweeners, NGTween.Kind.START_END, false);
      NGTween.playTweens(tweeners, NGTween.Kind.START, false);
      foreach (UITweener uiTweener in tweeners)
        uiTweener.onFinished.Clear();
    }
    this.TxtDialogMaterialName.SetText(material.name);
    UnitMaterialQuestInfo materialQuestInfo = ((IEnumerable<UnitMaterialQuestInfo>) MasterData.UnitMaterialQuestInfoList).SingleOrDefault<UnitMaterialQuestInfo>((Func<UnitMaterialQuestInfo, bool>) (x => x.unit_id == material.ID));
    if (materialQuestInfo == null)
      this.TxtDialogMaterialPlace.SetText("");
    else
      this.TxtDialogMaterialPlace.SetText(materialQuestInfo.long_desc);
  }

  public UITweener[] EndTweensMaterialQuestInfo(bool isForce = false)
  {
    if (!this.DialogBox.activeInHierarchy)
      return (UITweener[]) null;
    UITweener[] tweeners = NGTween.findTweeners(this.DialogBox, true);
    if (!isForce && ((IEnumerable<UITweener>) tweeners).Any<UITweener>((Func<UITweener, bool>) (x => x.enabled)))
      return (UITweener[]) null;
    NGTween.playTweens(tweeners, NGTween.Kind.START_END, true);
    NGTween.playTweens(tweeners, NGTween.Kind.END, false);
    return tweeners;
  }

  public void HideMaterialQuestInfo()
  {
    UITweener[] tweens = this.EndTweensMaterialQuestInfo(false);
    if (tweens == null)
      return;
    NGTween.setOnTweenFinished(tweens, (MonoBehaviour) this, "HideDialogBox");
  }

  private void HideDialogBox()
  {
    this.DialogBox.SetActive(false);
  }

  public void IbtnLevelUp()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<PopupManager>.GetInstance().monitorCoroutine(this.openPopupLevelUp());
  }

  private IEnumerator openPopupLevelUp()
  {
    Unit004JobDialogUp unit004JobDialogUp = this;
    GameObject popup = Singleton<PopupManager>.GetInstance().open(unit004JobDialogUp.popupLevelUpPrefab, false, false, false, true, false, false, "SE_1006");
    popup.SetActive(false);
    IEnumerator e = popup.GetComponent<Unit004JobDialogConfirmationPopup>().Init(unit004JobDialogUp._unit, unit004JobDialogUp._jobAbility, unit004JobDialogUp.materialIDsArray, unit004JobDialogUp.onUpdatedJobAbility_, unit004JobDialogUp.isClassChangeScene);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    popup.SetActive(true);
    unit004JobDialogUp.IsPush = false;
  }

  public void IbtnNo()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<PopupManager>.GetInstance().onDismiss();
  }

  public override void onBackButton()
  {
    this.IbtnNo();
  }
}
