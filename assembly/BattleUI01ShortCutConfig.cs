﻿// Decompiled with JetBrains decompiler
// Type: BattleUI01ShortCutConfig
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;
using UnityEngine;

public class BattleUI01ShortCutConfig : BattleMonoBehaviour
{
  private BL.StructValue<bool> requestAutoBattle_ = new BL.StructValue<bool>(false);
  private BL.StructValue<bool> requestSimpleBattle_ = new BL.StructValue<bool>(false);
  [SerializeField]
  [Tooltip("\"オート\"/\"簡易演出\"/\"戦闘速度変速\"ボタン群の先頭ノードをセット")]
  private NGTweenParts topConfig_;
  [SerializeField]
  [Tooltip("オート戦闘ボタンの先頭ノードをセット")]
  private GameObject topAutoBattle_;
  [SerializeField]
  [Tooltip("押すとオート戦闘が有効になるボタンをセット")]
  private UIButton btnEnabledAutoBattle_;
  [SerializeField]
  [Tooltip("押すとオート戦闘が無効になるボタンをセット")]
  private UIButton btnDisabledAutoBattle_;
  [SerializeField]
  [Tooltip("オートボタン無効期間中の表示")]
  private GameObject objWaitAutoBattle_;
  [SerializeField]
  [Tooltip("簡易戦闘ボタンの先頭ノードをセット")]
  private GameObject topSimpleBattle_;
  [SerializeField]
  [Tooltip("押すと簡易戦闘が有効になるボタンをセット")]
  private UIButton btnEnabledSimpleBattle_;
  [SerializeField]
  [Tooltip("押すと簡易戦闘が無効になるボタンをセット")]
  private UIButton btnDisabledSimpleBattle_;
  [SerializeField]
  [Tooltip("簡易戦闘ボタン無効期間中の表示")]
  private GameObject objWaitSimpleBattle_;
  [SerializeField]
  [Tooltip("戦闘進行速度変更オブジェクト先頭ノードをセット")]
  private GameObject topBattleSpeed_;
  private Battle01SelectNode selectNode_;
  private bool isWait_;
  private bool isControlAll_;
  private bool isControlAutoBattle_;
  private bool isControlSimpleBattle_;
  private bool isControlBattleSpeed_;
  private bool coroutineAutoBattle_;
  private bool coroutineSimpleBattle_;
  private const float WAIT_BUTTON_ON = 1f;

  public BL.BattleModified<BL.StructValue<bool>> requestAutoBattleModified_ { get; private set; }

  public BL.BattleModified<BL.StructValue<bool>> requestSimpleBattleModified_ { get; private set; }

  public bool isActive_
  {
    get
    {
      return this.isControlAll_ && this.topConfig_.isActive;
    }
    set
    {
      if (!this.isControlAll_ || this.topConfig_.isActive == value)
        return;
      this.topConfig_.isActive = value;
      this.isWait_ = true;
    }
  }

  public override IEnumerator onInitAsync()
  {
    // ISSUE: reference to a compiler-generated field
    int num = this.\u003C\u003E1__state;
    BattleUI01ShortCutConfig ui01ShortCutConfig = this;
    if (num != 0)
      return false;
    // ISSUE: reference to a compiler-generated field
    this.\u003C\u003E1__state = -1;
    ui01ShortCutConfig.requestAutoBattle_.value = ui01ShortCutConfig.env.core.isAutoBattle.value;
    ui01ShortCutConfig.requestSimpleBattle_.value = ui01ShortCutConfig.battleManager.noDuelScene;
    ui01ShortCutConfig.requestAutoBattleModified_ = new BL.BattleModified<BL.StructValue<bool>>(ui01ShortCutConfig.requestAutoBattle_);
    ui01ShortCutConfig.requestSimpleBattleModified_ = new BL.BattleModified<BL.StructValue<bool>>(ui01ShortCutConfig.requestSimpleBattle_);
    ui01ShortCutConfig.selectNode_ = ui01ShortCutConfig.GetComponent<Battle01SelectNode>();
    ui01ShortCutConfig.isWait_ = false;
    ui01ShortCutConfig.resetControlButtons(ui01ShortCutConfig.env.core.battleInfo.isAutoBattleEnable, true, true);
    if (ui01ShortCutConfig.isControlAutoBattle_)
      ui01ShortCutConfig.setAutoBattleButton(ui01ShortCutConfig.requestAutoBattle_.value, true);
    if (ui01ShortCutConfig.isControlSimpleBattle_)
      ui01ShortCutConfig.setSimpleBattleButton(ui01ShortCutConfig.requestSimpleBattle_.value, true);
    return false;
  }

  protected override void Update_Battle()
  {
    if (!this.isControlAll_)
      return;
    if (this.isWait_ && this.topConfig_.isActive == this.topConfig_.gameObject.activeSelf)
      this.isWait_ = false;
    if (!this.isControlSimpleBattle_ || this.requestSimpleBattle_.value == this.battleManager.noDuelScene || this.requestSimpleBattleModified_.isChanged)
      return;
    if (this.coroutineSimpleBattle_)
    {
      this.StopCoroutine("doWaitEnabledSimpleBattleButton");
      this.coroutineSimpleBattle_ = false;
    }
    this.setRequestSimpleBattle(this.battleManager.noDuelScene, true);
  }

  private void OnEnable()
  {
    this.recoveryAutoBattleButton();
    this.recoverySimpleBattleButton();
  }

  private void recoveryAutoBattleButton()
  {
    if (!this.coroutineAutoBattle_)
      return;
    this.coroutineAutoBattle_ = false;
    this.btnEnabledAutoBattle_.gameObject.SetActive(true);
    this.objWaitAutoBattle_.SetActive(false);
  }

  private void recoverySimpleBattleButton()
  {
    if (!this.coroutineSimpleBattle_)
      return;
    this.coroutineSimpleBattle_ = false;
    this.btnEnabledSimpleBattle_.gameObject.SetActive(true);
    this.objWaitSimpleBattle_.SetActive(false);
  }

  public void resetControlButtons(
    bool enabledAutoBattle,
    bool enabledSimpleBattle,
    bool enabledBattleSpeed)
  {
    this.isControlAutoBattle_ = enabledAutoBattle && (Object) this.topAutoBattle_ != (Object) null && ((Object) this.btnEnabledAutoBattle_ != (Object) null && (Object) this.btnDisabledAutoBattle_ != (Object) null) && (Object) this.objWaitAutoBattle_ != (Object) null;
    if (!this.isControlAutoBattle_ && (Object) this.topAutoBattle_ != (Object) null)
      this.topAutoBattle_.SetActive(false);
    this.isControlSimpleBattle_ = enabledSimpleBattle && (Object) this.topSimpleBattle_ != (Object) null && ((Object) this.btnEnabledSimpleBattle_ != (Object) null && (Object) this.btnDisabledSimpleBattle_ != (Object) null) && (Object) this.objWaitSimpleBattle_ != (Object) null;
    if (!this.isControlSimpleBattle_ && (Object) this.topSimpleBattle_ != (Object) null)
      this.topSimpleBattle_.SetActive(false);
    this.isControlBattleSpeed_ = enabledBattleSpeed && (Object) this.topBattleSpeed_ != (Object) null;
    if (!this.isControlBattleSpeed_ && (Object) this.topBattleSpeed_ != (Object) null)
      this.topBattleSpeed_.SetActive(false);
    this.isControlAll_ = Singleton<TutorialRoot>.GetInstance().IsTutorialFinish() && (Object) this.topConfig_ != (Object) null && (Object) this.selectNode_ != (Object) null && (this.isControlAutoBattle_ || this.isControlSimpleBattle_ || this.isControlBattleSpeed_);
    if (!((Object) this.topConfig_ != (Object) null))
      return;
    if (this.isControlAll_)
      this.topConfig_.forceActive(true);
    else
      this.topConfig_.resetActive(false);
  }

  public void onClickedAutoBattleEnabled()
  {
    this.setRequestAutoBattle(true, false);
  }

  public void onClickedAutoBattleDisabled()
  {
    this.setRequestAutoBattle(false, false);
  }

  private void setRequestAutoBattle(bool isAuto, bool isInit = false)
  {
    if ((Object) this.selectNode_ != (Object) null && (this.selectNode_.IsPush || this.selectNode_.IsForceAutoDisable) || (this.isWait_ || !this.isControlAutoBattle_))
      return;
    if (this.requestAutoBattle_.value != isAuto)
    {
      this.requestAutoBattle_.value = isAuto;
      this.setAutoBattleButton(isAuto, isInit);
    }
    else if (isInit)
      this.setAutoBattleButton(isAuto, isInit);
    if (!isInit)
      return;
    this.requestAutoBattleModified_.isChangedOnce();
  }

  private void setAutoBattleButton(bool isAuto, bool isInit)
  {
    UIButton btn;
    if (isAuto)
    {
      this.btnEnabledAutoBattle_.gameObject.SetActive(false);
      btn = this.btnDisabledAutoBattle_;
    }
    else
    {
      btn = this.btnEnabledAutoBattle_;
      this.btnDisabledAutoBattle_.gameObject.SetActive(false);
    }
    if (isInit)
    {
      btn.gameObject.SetActive(true);
      this.objWaitAutoBattle_.SetActive(false);
    }
    else
      this.StartCoroutine(this.doWaitEnabledAutoBattleButton(btn));
  }

  private IEnumerator doWaitEnabledAutoBattleButton(UIButton btn)
  {
    if ((Object) btn == (Object) this.btnEnabledAutoBattle_)
    {
      this.coroutineAutoBattle_ = true;
      btn.gameObject.SetActive(false);
      this.objWaitAutoBattle_.SetActive(true);
      yield return (object) new WaitForSeconds(1f);
      btn.gameObject.SetActive(true);
    }
    else
      btn.gameObject.SetActive(true);
    this.objWaitAutoBattle_.SetActive(false);
    this.coroutineAutoBattle_ = false;
  }

  public void onClickedSimpleBattleEnabled()
  {
    this.setRequestSimpleBattle(true, false);
  }

  public void onClickedSimpleBattleDisabled()
  {
    this.setRequestSimpleBattle(false, false);
  }

  private void setRequestSimpleBattle(bool isSimple, bool isInit = false)
  {
    if (this.isWait_ || !this.isControlSimpleBattle_)
      return;
    if (this.requestSimpleBattle_.value != isSimple)
    {
      this.requestSimpleBattle_.value = isSimple;
      this.setSimpleBattleButton(isSimple, isInit);
    }
    else if (isInit)
      this.setSimpleBattleButton(isSimple, isInit);
    if (!isInit)
      return;
    this.requestSimpleBattleModified_.isChangedOnce();
  }

  private void setSimpleBattleButton(bool isSimple, bool isInit)
  {
    UIButton uiButton;
    if (isSimple)
    {
      this.btnEnabledSimpleBattle_.gameObject.SetActive(false);
      uiButton = this.btnDisabledSimpleBattle_;
    }
    else
    {
      uiButton = this.btnEnabledSimpleBattle_;
      this.btnDisabledSimpleBattle_.gameObject.SetActive(false);
    }
    if (isInit)
    {
      uiButton.gameObject.SetActive(true);
      this.objWaitSimpleBattle_.SetActive(false);
    }
    else
      this.StartCoroutine("doWaitEnabledSimpleBattleButton", (object) uiButton);
  }

  private IEnumerator doWaitEnabledSimpleBattleButton(UIButton btn)
  {
    if ((Object) btn == (Object) this.btnEnabledSimpleBattle_)
    {
      this.coroutineSimpleBattle_ = true;
      btn.gameObject.SetActive(false);
      this.objWaitSimpleBattle_.SetActive(true);
      yield return (object) new WaitForSeconds(1f);
      btn.gameObject.SetActive(true);
    }
    else
      btn.gameObject.SetActive(true);
    this.objWaitSimpleBattle_.SetActive(false);
    this.coroutineSimpleBattle_ = false;
  }
}
