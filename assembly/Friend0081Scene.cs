﻿// Decompiled with JetBrains decompiler
// Type: Friend0081Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Friend0081Scene : NGSceneBase
{
  private bool isInit = true;
  [SerializeField]
  private UILabel TxtNumber;
  [SerializeField]
  private GameObject BadgeIcon;
  [SerializeField]
  private Friend0081Menu menu;
  private int friendCnt;
  private GameObject badgeGo;

  public override IEnumerator onInitSceneAsync()
  {
    Future<GameObject> badgeIconF = Res.Prefabs.BadgeIcon.prefab.Load<GameObject>();
    IEnumerator e = badgeIconF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.badgeGo = badgeIconF.Result.Clone((Transform) null);
    this.badgeGo.gameObject.SetParent(this.BadgeIcon);
    e = WebAPI.FriendFriends((System.Action<WebAPI.Response.UserError>) null).Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.friendCnt = ((IEnumerable<PlayerFriend>) SMManager.Get<PlayerFriend[]>()).Friends().Length;
  }

  private void UpdateInfomation()
  {
    Player player = SMManager.Get<Player>();
    PlayerFriend[] playerFriendArray1 = ((IEnumerable<PlayerFriend>) SMManager.Get<PlayerFriend[]>()).Friends();
    PlayerFriend[] playerFriendArray2 = ((IEnumerable<PlayerFriend>) SMManager.Get<PlayerFriend[]>()).ReceivedFriendApplications();
    this.menu.DirNoFriend.SetActive(playerFriendArray1.Length == 0);
    this.TxtNumber.SetTextLocalize(playerFriendArray1.Length.ToString("D3") + "/" + player.max_friends.ToString("D3"));
    ButtonBadge component = this.badgeGo.GetComponent<ButtonBadge>();
    if (playerFriendArray2 != null)
    {
      Singleton<NGGameDataManager>.GetInstance().ReceivedFriendRequestCount = playerFriendArray2.Length;
      component.set(playerFriendArray2.Length);
    }
    else
    {
      Singleton<NGGameDataManager>.GetInstance().ReceivedFriendRequestCount = 0;
      component.set(0);
    }
  }

  public IEnumerator onStartSceneAsync()
  {
    this.UpdateInfomation();
    IEnumerator e;
    if (!this.isInit && this.friendCnt == ((IEnumerable<PlayerFriend>) SMManager.Get<PlayerFriend[]>()).Friends().Length)
    {
      e = this.menu.UpdateList();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    else
    {
      e = this.menu.InitFriendScroll(((IEnumerable<PlayerFriend>) SMManager.Get<PlayerFriend[]>()).Friends());
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.friendCnt = ((IEnumerable<PlayerFriend>) SMManager.Get<PlayerFriend[]>()).Friends().Length;
      this.isInit = false;
    }
    if (Singleton<CommonRoot>.GetInstance().isLoading)
    {
      yield return (object) null;
      Singleton<CommonRoot>.GetInstance().isLoading = false;
    }
  }

  public override void onEndScene()
  {
    Persist.sortOrder.Flush();
  }

  public static void ChangeScene(bool stack)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("friend008_1", stack, (object[]) Array.Empty<object>());
  }
}
