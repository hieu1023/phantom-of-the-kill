﻿// Decompiled with JetBrains decompiler
// Type: RouletteOttimoAnimationController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class RouletteOttimoAnimationController : MonoBehaviour
{
  private List<string> ottimoSerifList = new List<string>()
  {
    "グラッティエ！\n私の居城に良く来てくれた！\n心から歓迎する",
    "ここでは一日一回\n私の輝きしサタンへの忠誠を\nこの回転盤で映し出すことができる",
    "さあっ！\nスタートボタンをタップするのだ！\nオォッティモォォォッ～！！"
  };
  public float enableAllButtonsDelay = 1f;
  [SerializeField]
  private Animator ottimoAnimator;
  [SerializeField]
  private RouletteMenu rouletteMenu;
  [SerializeField]
  private UILabel ottimoSerifLabel;
  private int currentSerifIndex;

  public void InitAndPlay()
  {
    this.currentSerifIndex = 0;
    this.ottimoSerifLabel.SetTextLocalize(this.ottimoSerifList[this.currentSerifIndex]);
    this.ottimoAnimator.SetTrigger("FadeIn");
  }

  public void DisableAnimator()
  {
    this.ottimoAnimator.SetTrigger("Close");
  }

  public void OnTapOttimo()
  {
    ++this.currentSerifIndex;
    if (this.currentSerifIndex < this.ottimoSerifList.Count)
    {
      this.ottimoSerifLabel.SetTextLocalize(this.ottimoSerifList[this.currentSerifIndex]);
    }
    else
    {
      if (this.currentSerifIndex != this.ottimoSerifList.Count)
        return;
      this.ottimoAnimator.SetTrigger("OttimoOut");
      this.rouletteMenu.SetButtonsAvailbility(true, this.enableAllButtonsDelay);
    }
  }

  public void OnTapCurtain()
  {
    this.ottimoAnimator.SetTrigger("Skip");
  }
}
