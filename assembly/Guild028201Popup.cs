﻿// Decompiled with JetBrains decompiler
// Type: Guild028201Popup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Guild028201Popup : MonoBehaviour
{
  private Guild02811Menu menu2811;
  private Guild02812Menu menu2812;
  private GuildUtil.MenuType aMenu;

  public void Initialize(Guild02811Menu guild02811menu)
  {
    this.aMenu = GuildUtil.MenuType.menu2811;
    this.menu2811 = guild02811menu;
  }

  public void Initialize(Guild02812Menu guild02812menu)
  {
    this.aMenu = GuildUtil.MenuType.menu2812;
    this.menu2812 = guild02812menu;
  }

  public void onButtonOk()
  {
    Singleton<PopupManager>.GetInstance().dismiss(false);
    switch (this.aMenu)
    {
      case GuildUtil.MenuType.menu2811:
        this.menu2811.onButtonSetting();
        break;
      case GuildUtil.MenuType.menu2812:
        this.menu2812.onButtonSetting();
        break;
    }
  }
}
