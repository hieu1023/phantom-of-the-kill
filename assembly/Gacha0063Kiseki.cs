﻿// Decompiled with JetBrains decompiler
// Type: Gacha0063Kiseki
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Gacha0063Kiseki : Gacha0063hindicator
{
  [SerializeField]
  private GameObject dyn_Chara;
  [SerializeField]
  private GameObject dyn_CharaShadow;
  [SerializeField]
  private GameObject dyn_mask;
  [SerializeField]
  private GameObject dyn_mask_CharaShadow;
  [SerializeField]
  private UI2DSprite dyn_Star;
  [SerializeField]
  private BattleSkillIcon dyn_Unit_Skill;
  [SerializeField]
  private GearKindIcon dyn_Weapon;
  [SerializeField]
  private UILabel txt_Title;
  [SerializeField]
  private UILabel txt_Skillexplanation;
  [SerializeField]
  private UILabel txt_leaderskill_description;
  [SerializeField]
  private UILabel txt_LeaderSkillname;
  [SerializeField]
  private UILabel txt_JobName;
  [SerializeField]
  private Transform modelLocator;
  [SerializeField]
  private Animator animator_;
  [SerializeField]
  private BoxCollider buyKisekiButton;
  [SerializeField]
  private UI2DSprite ui3DModel2D;
  [SerializeField]
  private GameObject slc_Kiseki_Bonus;
  [SerializeField]
  private GameObject slc_Kiseki_BonusEx;
  private Modified<CoinBonus[]> coinBonus;
  [SerializeField]
  private GameObject singleGachaMode;
  [SerializeField]
  private GameObject multiGachaMode;
  [SerializeField]
  private UI2DSprite TitleImg;
  [SerializeField]
  private UISprite TitleImgBase;
  [SerializeField]
  private UI2DSprite kakuteiImg;
  [SerializeField]
  private UI2DSprite kakuteiImgEx;
  [SerializeField]
  private MeshRenderer kakuteiEffect;
  [SerializeField]
  private UI2DSprite TopImg;
  [SerializeField]
  private GameObject NoneTopImgObj;
  [SerializeField]
  private UIWidget charactersWidget;
  [SerializeField]
  private Gacha0063KisekiExtention kisekiEx;
  [SerializeField]
  private GameObject dir_redrawn_gacha;
  [SerializeField]
  private UILabel txt_remain_chance;
  private UI3DModel ui3DModel;
  private int pickupUnitIdNumber;
  private List<int> pickupUnitIdList;
  private List<GameObject> imagePrefabList;
  private List<UnityEngine.Sprite> pickupImageList;
  private List<GameObject> modelPrefabList;
  private List<UnityEngine.Sprite> spriteList;
  private bool gachaExFlag;
  private bool gachaIntroduction;
  private DateTime? serverTime;
  private bool CreateCharacterImageEnd;
  private bool CreateSimpleCharacter3DImageEnd;
  private bool CreateSkillImageEnd;

  private void Start()
  {
    this.coinBonus = SMManager.Observe<CoinBonus[]>();
    this.coinBonus.NotifyChanged();
  }

  public override void PlayAnim()
  {
    this.kakuteiEffect.GetComponent<Animator>().enabled = true;
    this.kakuteiEffect.gameObject.SetActive(true);
  }

  public override void EndAnim()
  {
    this.kakuteiEffect.GetComponent<Animator>().enabled = false;
    this.kakuteiEffect.gameObject.SetActive(false);
  }

  private void MultiGachBtnSettinng(GachaModule gachaModule)
  {
    this.multiGachaMode.SetActive(true);
    this.singleGachaMode.SetActive(false);
    ((IEnumerable<GachaModuleGacha>) gachaModule.gacha).ForEachIndex<GachaModuleGacha>((System.Action<GachaModuleGacha, int>) ((x, n) =>
    {
      if (x.payment_type_id == Gacha0063Menu.PaymentTypeIDCompensation)
      {
        this.dirNormalObject[n].SetActive(false);
        this.dirCompensationObject[n].SetActive(true);
        this.compensationGachaButton[n].Init(gachaModule.name, x, this.Menu, gachaModule.type, gachaModule.number);
      }
      else
      {
        this.dirNormalObject[n].SetActive(true);
        this.dirCompensationObject[n].SetActive(false);
        this.gachaButton[n].Init(gachaModule.name, x, this.Menu, gachaModule.type, gachaModule.number);
      }
    }));
  }

  public override void InitGachaModuleGacha(
    Gacha0063Menu gacha0063Menu,
    GachaModule gachaModule,
    DateTime serverTime,
    UIScrollView scrollView)
  {
    if ((UnityEngine.Object) this.dir_redrawn_gacha != (UnityEngine.Object) null)
    {
      this.dir_redrawn_gacha.SetActive(!Singleton<TutorialRoot>.GetInstance().IsTutorialFinish());
      this.txt_remain_chance.SetTextLocalize(Consts.GetInstance().TUTORIAL_GACHA_RETRY_INFINITY_BANNER);
    }
    this.GachaModule = gachaModule;
    this.Menu = gacha0063Menu;
    this.serverTime = new DateTime?(serverTime);
    if (gachaModule.number == 1)
    {
      if (gachaModule.gacha.Length > 1)
      {
        this.MultiGachBtnSettinng(gachaModule);
      }
      else
      {
        this.singleGachaMode.SetActive(true);
        this.multiGachaMode.SetActive(false);
        if (gachaModule.gacha[0].payment_type_id == Gacha0063Menu.PaymentTypeIDCompensation)
          this.singleCompensationGachaButton.Init(gachaModule.name, gachaModule.gacha[0], this.Menu, gachaModule.type, gachaModule.number);
        else
          this.singleGachaButton.Init(gachaModule.name, gachaModule.gacha[0], this.Menu, gachaModule.type, gachaModule.number);
      }
      this.gachaExFlag = false;
    }
    else if (gachaModule.gacha.Length > 1)
    {
      this.MultiGachBtnSettinng(gachaModule);
      this.gachaExFlag = false;
    }
    else
    {
      this.singleCompensationGachaButtonExObject.SetActive(false);
      this.singleGachaButtonEx.gameObject.SetActive(false);
      if (gachaModule.gacha[0].payment_type_id == Gacha0063Menu.PaymentTypeIDCompensation)
      {
        this.singleCompensationGachaButtonExObject.SetActive(true);
        this.singleCompensationGachaButtonEx.Init(gachaModule.name, gachaModule.gacha[0], this.Menu, gachaModule.type, gachaModule.number);
        this.singleCompensationGachaButtonEx.ChangeButtonEvent(this.GachaModule);
      }
      else
      {
        this.singleGachaButtonEx.gameObject.SetActive(true);
        this.singleGachaButtonEx.Init(gachaModule.name, gachaModule.gacha[0], this.Menu, gachaModule.type, gachaModule.number);
        this.singleGachaButtonEx.ChangeButtonEvent(this.GachaModule);
      }
      this.kisekiEx.SetKisekiEx(gachaModule, this.Menu);
      this.gachaExFlag = true;
    }
    if (!this.GachaModule.period.display_count_down)
      return;
    this.kisekiEx.SetTimiLimit(this.GachaModule);
  }

  private void Init()
  {
    this.pickupUnitIdNumber = -1;
  }

  public override IEnumerator Set(GameObject detailPopup)
  {
    Gacha0063Kiseki gacha0063Kiseki = this;
    if (gacha0063Kiseki.coinBonus != null && gacha0063Kiseki.coinBonus.IsChangedOnce())
      gacha0063Kiseki.slc_Kiseki_Bonus.SetActive((uint) gacha0063Kiseki.coinBonus.Value.Length > 0U);
    gacha0063Kiseki.buyKisekiButton.enabled = true;
    IEnumerator e = gacha0063Kiseki.kisekiEx.InitDetail(gacha0063Kiseki.GachaModule, detailPopup, gacha0063Kiseki.Menu);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if (gacha0063Kiseki.GachaModule.title_banner_url != "")
    {
      e = Singleton<NGGameDataManager>.GetInstance().GetWebImage(gacha0063Kiseki.GachaModule.title_banner_url, gacha0063Kiseki.TitleImg);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      gacha0063Kiseki.TitleImg.gameObject.SetActive(true);
      gacha0063Kiseki.TitleImgBase.gameObject.SetActive(false);
    }
    else
    {
      gacha0063Kiseki.TitleImg.sprite2D = (UnityEngine.Sprite) null;
      gacha0063Kiseki.TitleImg.gameObject.SetActive(false);
      gacha0063Kiseki.TitleImgBase.gameObject.SetActive(true);
    }
    if (!string.IsNullOrEmpty(gacha0063Kiseki.GachaModule.gacha[gacha0063Kiseki.GachaModule.gacha.Length - 1].button_url))
    {
      e = Singleton<NGGameDataManager>.GetInstance().GetWebImage(gacha0063Kiseki.GachaModule.gacha[gacha0063Kiseki.GachaModule.gacha.Length - 1].button_url, gacha0063Kiseki.gachaExFlag ? gacha0063Kiseki.kakuteiImgEx : gacha0063Kiseki.kakuteiImg);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      if (gacha0063Kiseki.gachaExFlag)
        gacha0063Kiseki.kakuteiImgEx.gameObject.SetActive(true);
      else
        gacha0063Kiseki.kakuteiImg.gameObject.SetActive(true);
    }
    else if (gacha0063Kiseki.gachaExFlag)
      gacha0063Kiseki.kakuteiImgEx.sprite2D = (UnityEngine.Sprite) null;
    else
      gacha0063Kiseki.kakuteiImg.sprite2D = (UnityEngine.Sprite) null;
    bool isTopImgLoad = false;
    if (gacha0063Kiseki.GachaModule.front_image_url != "")
    {
      e = Singleton<NGGameDataManager>.GetInstance().GetWebImage(gacha0063Kiseki.GachaModule.front_image_url, gacha0063Kiseki.TopImg);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      isTopImgLoad = true;
      gacha0063Kiseki.TopImg.gameObject.SetActive(true);
    }
    else
    {
      gacha0063Kiseki.TopImg.sprite2D = (UnityEngine.Sprite) null;
      e = gacha0063Kiseki.CashClean();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    gacha0063Kiseki.CreatePickupUnitIdList();
    gacha0063Kiseki.StartCoroutine(gacha0063Kiseki.CreateCharacterImage());
    gacha0063Kiseki.StartCoroutine(gacha0063Kiseki.CreateSimpleCharacter3DImage());
    gacha0063Kiseki.StartCoroutine(gacha0063Kiseki.CreateSkillImage());
    gacha0063Kiseki.gachaIntroduction = !string.IsNullOrEmpty(gacha0063Kiseki.GachaModule.front_image_url) && (uint) gacha0063Kiseki.GachaModule.newentity.Length > 0U;
    if (string.IsNullOrEmpty(gacha0063Kiseki.GachaModule.front_image_url) && gacha0063Kiseki.GachaModule.newentity.Length == 0)
    {
      Debug.LogError((object) "注目ユニットも、フロント画像も無いので　画面が正常に表示されません");
    }
    else
    {
      while (!(!gacha0063Kiseki.gachaIntroduction & isTopImgLoad) && (gacha0063Kiseki.imagePrefabList.Count <= 0 || gacha0063Kiseki.pickupImageList.Count <= 0 || gacha0063Kiseki.spriteList.Count <= 0) && (!gacha0063Kiseki.CreateCharacterImageEnd || !gacha0063Kiseki.CreateSimpleCharacter3DImageEnd || !gacha0063Kiseki.CreateSkillImageEnd))
        yield return (object) null;
      if (gacha0063Kiseki.gachaIntroduction)
      {
        gacha0063Kiseki.animator_.enabled = true;
        gacha0063Kiseki.ChangeTopImg();
      }
      else if (gacha0063Kiseki.GachaModule.newentity.Length != 0)
      {
        gacha0063Kiseki.animator_.enabled = true;
        gacha0063Kiseki.ChangeCharacter(0);
        gacha0063Kiseki.animator_.Play("Fade", 0, 0.0f);
      }
      else if (isTopImgLoad)
        gacha0063Kiseki.NoneTopImgObj.SetActive(false);
      if (gacha0063Kiseki.GachaModule.type == 6)
      {
        int paymentTypeId = gacha0063Kiseki.GachaModule.gacha[0].payment_type_id;
        switch (paymentTypeId)
        {
          case 1:
            break;
          case 9:
            break;
          default:
            gacha0063Kiseki.StartCoroutine(Gacha0063Kiseki.createPaymentIcon(gacha0063Kiseki.singleGachaButtonExIcon, paymentTypeId));
            break;
        }
      }
    }
  }

  private void CreatePickupUnitIdList()
  {
    this.pickupUnitIdList = (List<int>) null;
    this.pickupUnitIdList = new List<int>();
    foreach (GachaModuleNewentity gachaModuleNewentity in this.GachaModule.newentity)
    {
      if (1 == gachaModuleNewentity.reward_type_id || 24 == gachaModuleNewentity.reward_type_id)
        this.pickupUnitIdList.Add(gachaModuleNewentity.reward_id);
    }
  }

  private IEnumerator CreateCharacterImage()
  {
    this.CreateCharacterImageEnd = false;
    UI2DSprite component1 = this.dyn_Chara.GetComponent<UI2DSprite>();
    int depth = component1.depth;
    component1.enabled = false;
    if (this.imagePrefabList != null)
    {
      foreach (GameObject imagePrefab in this.imagePrefabList)
        UnityEngine.Object.Destroy((UnityEngine.Object) imagePrefab.gameObject);
      this.imagePrefabList.Clear();
    }
    this.imagePrefabList = (List<GameObject>) null;
    this.imagePrefabList = new List<GameObject>();
    Future<UnityEngine.Sprite> maskf = Res.GUI._006_3_sozai.mask_Character.Load<UnityEngine.Sprite>();
    IEnumerator e = maskf.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    UnityEngine.Sprite mask = maskf.Result;
    foreach (int pickupUnitId in this.pickupUnitIdList)
    {
      Future<GameObject> prefabf = MasterData.UnitUnit[pickupUnitId].LoadMypage();
      e = prefabf.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      GameObject prefab = prefabf.Result.Clone(this.dyn_Chara.transform);
      Future<UnityEngine.Sprite> imgf = MasterData.UnitUnit[pickupUnitId].LoadSpriteLarge(1f);
      e = imgf.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      UnityEngine.Sprite result = imgf.Result;
      prefab.GetComponent<UIWidget>().depth = depth;
      UI2DSprite component2 = prefab.GetComponent<UI2DSprite>();
      component2.sprite2D = result;
      NGxMaskSpriteWithScale component3 = prefab.GetComponent<NGxMaskSpriteWithScale>();
      component3.yOffsetPixel = -180;
      component3.scale = 0.6f;
      component3.MainUI2DSprite = component2;
      component3.maskTexture = mask.texture;
      prefab.SetActive(false);
      this.imagePrefabList.Add(prefab);
      yield return (object) null;
      prefabf = (Future<GameObject>) null;
      prefab = (GameObject) null;
      imgf = (Future<UnityEngine.Sprite>) null;
    }
    this.CreateCharacterImageEnd = true;
  }

  private IEnumerator CreateSkillImage()
  {
    Gacha0063Kiseki gacha0063Kiseki = this;
    gacha0063Kiseki.CreateSkillImageEnd = false;
    if (gacha0063Kiseki.spriteList != null)
    {
      foreach (UnityEngine.Object sprite in gacha0063Kiseki.spriteList)
        UnityEngine.Object.Destroy(sprite);
      gacha0063Kiseki.spriteList.Clear();
    }
    gacha0063Kiseki.spriteList = (List<UnityEngine.Sprite>) null;
    gacha0063Kiseki.spriteList = new List<UnityEngine.Sprite>();
    foreach (int pickupUnitId in gacha0063Kiseki.pickupUnitIdList)
    {
      UnitUnit unit_data = MasterData.UnitUnit[pickupUnitId];
      if (unit_data.PickupSkill != null)
      {
        UnityEngine.Sprite sprite;
        if (unit_data.PickupSkill.skill_type != BattleskillSkillType.magic)
        {
          Future<UnityEngine.Sprite> ft = unit_data.PickupSkill.LoadBattleSkillIcon((BattleFuncs.InvestSkill) null);
          IEnumerator e = ft.Wait();
          while (e.MoveNext())
            yield return e.Current;
          e = (IEnumerator) null;
          sprite = ft.Result;
          ft = (Future<UnityEngine.Sprite>) null;
        }
        else
        {
          gacha0063Kiseki.Menu.commonElementIcon.Init(unit_data.PickupSkill.element, false);
          sprite = gacha0063Kiseki.Menu.commonElementIcon.iconSprite.sprite2D;
        }
        if ((UnityEngine.Object) sprite == (UnityEngine.Object) null)
        {
          gacha0063Kiseki.Menu.commonElementIcon.Init(unit_data.PickupSkill.element, false);
          sprite = gacha0063Kiseki.Menu.commonElementIcon.iconSprite.sprite2D;
          BattleskillSkill pickupSkill = MasterData.UnitUnit[pickupUnitId].PickupSkill;
          Debug.LogError((object) ("スキルID " + (object) pickupSkill.ID + " ユニットID " + (object) pickupUnitId + " ユニット名 " + MasterData.UnitUnit[pickupUnitId].name + " スキル名 " + pickupSkill.name + "の画像がない"));
        }
        gacha0063Kiseki.spriteList.Add(sprite);
        yield return (object) null;
        unit_data = (UnitUnit) null;
      }
    }
    gacha0063Kiseki.CreateSkillImageEnd = true;
  }

  private IEnumerator CreateSimpleCharacter3DImage()
  {
    this.CreateSimpleCharacter3DImageEnd = false;
    if (this.pickupImageList != null)
    {
      foreach (UnityEngine.Object pickupImage in this.pickupImageList)
        UnityEngine.Object.Destroy(pickupImage);
      this.pickupImageList.Clear();
    }
    this.pickupImageList = (List<UnityEngine.Sprite>) null;
    this.pickupImageList = new List<UnityEngine.Sprite>();
    foreach (int pickupUnitId in this.pickupUnitIdList)
    {
      Future<UnityEngine.Sprite> imgf = MasterData.UnitUnit[pickupUnitId].LoadSpritePickup();
      IEnumerator e = imgf.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.pickupImageList.Add(imgf.Result);
      yield return (object) null;
      imgf = (Future<UnityEngine.Sprite>) null;
    }
    this.ui3DModel2D.enabled = true;
    this.CreateSimpleCharacter3DImageEnd = true;
  }

  private IEnumerator CreateCharacterModel()
  {
    if ((UnityEngine.Object) this.ui3DModel == (UnityEngine.Object) null)
    {
      Future<GameObject> fModel = Res.Prefabs.gacha006_8.slc_3DModel.Load<GameObject>();
      IEnumerator e = fModel.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.ui3DModel = fModel.Result.Clone(this.modelLocator).GetComponent<UI3DModel>();
      UnityEngine.Object.Destroy((UnityEngine.Object) this.ui3DModel.GetComponent<UIPanel>());
      this.ui3DModel.GetComponent<UIWidget>().depth = 10;
      this.ui3DModel.GetComponent<UITexture>().enabled = true;
      this.ui3DModel.transform.localPosition = new Vector3(60f, 0.0f, 0.0f);
      fModel = (Future<GameObject>) null;
    }
    if (this.modelPrefabList != null)
    {
      foreach (GameObject modelPrefab in this.modelPrefabList)
        UnityEngine.Object.Destroy((UnityEngine.Object) modelPrefab.gameObject);
      this.modelPrefabList.Clear();
    }
    this.modelPrefabList = (List<GameObject>) null;
    this.modelPrefabList = new List<GameObject>();
  }

  private IEnumerator CashClean()
  {
    GC.Collect();
    GC.WaitForPendingFinalizers();
    Singleton<ResourceManager>.GetInstance().ClearCache();
    AsyncOperation asyncOP = Resources.UnloadUnusedAssets();
    while (!asyncOP.isDone)
      yield return (object) null;
  }

  public static IEnumerator createPaymentIcon(UI2DSprite icon, int payment_type_id)
  {
    string path;
    switch (payment_type_id)
    {
      case 1:
        path = "Icons/Kiseki_Icon";
        break;
      case 2:
        path = "Icons/Zeny_Icon";
        break;
      case 3:
        path = "Icons/Item_Icon_Medal";
        break;
      case 4:
        path = "Icons/ManaPoint_Icon";
        break;
      case 5:
        path = "Icons/GachaTicket_Icon";
        break;
      case 6:
        path = "Icons/BattleMedal_Icon";
        break;
      case 8:
        path = "Icons/Item_Icon_TowerMedal";
        break;
      case 9:
        path = "Icons/Kiseki_Icon";
        break;
      case 10:
        path = "Icons/GuildMedal_Icon";
        break;
      default:
        path = "Icons/Common_Icon";
        break;
    }
    Future<UnityEngine.Sprite> future = Singleton<ResourceManager>.GetInstance().Load<UnityEngine.Sprite>(path, 1f);
    IEnumerator e = future.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    icon.sprite2D = future.Result;
    icon.SetDimensions(icon.sprite2D.texture.width, icon.sprite2D.texture.height);
  }

  public override void IbtnBuyKiseki()
  {
    base.IbtnBuyKiseki();
    this.Menu.IbtnBuyKiseki();
    this.StartCoroutine(PopupUtility.BuyKiseki(false));
  }

  public void ChangeCharacter(int flag)
  {
    if (this.pickupUnitIdNumber >= this.pickupUnitIdList.Count)
    {
      this.pickupUnitIdNumber = 0;
      if (this.gachaIntroduction)
      {
        this.ChangeTopImg();
        return;
      }
    }
    this.NoneTopImgObj.SetActive(true);
    this.TopImg.gameObject.SetActive(false);
    this.ChangeCharacterImage(this.pickupUnitIdNumber);
    this.ChangeSimpleCharacter3DImage(this.pickupUnitIdNumber);
    this.SetStatus(this.pickupUnitIdList[this.pickupUnitIdNumber]);
    this.ChangeSkillIcon(this.pickupUnitIdNumber);
    if (flag == 1)
      this.animator_.Play("Fade", 0, 0.0f);
    ++this.pickupUnitIdNumber;
  }

  public void ChangeTopImg()
  {
    this.NoneTopImgObj.SetActive(false);
    this.TopImg.gameObject.SetActive(true);
    this.imagePrefabList.ForEach((System.Action<GameObject>) (x => x.SetActive(false)));
    this.animator_.Play("FadeTopImg", 0, 0.0f);
  }

  private void ChangeCharacterImage(int id)
  {
    this.imagePrefabList.ForEachIndex<GameObject>((System.Action<GameObject, int>) ((x, n) => x.SetActive(n == id)));
  }

  private void ChangeSimpleCharacter3DImage(int id)
  {
    this.pickupImageList.ForEachIndex<UnityEngine.Sprite>((System.Action<UnityEngine.Sprite, int>) ((x, n) =>
    {
      if (n != id)
        return;
      this.ui3DModel2D.sprite2D = x;
    }));
  }

  private IEnumerator CreateModel(int id)
  {
    if (this.pickupUnitIdList != null && this.modelPrefabList != null && this.pickupUnitIdList.Count == this.modelPrefabList.Count)
    {
      this.ChangeCharacterModel(id);
    }
    else
    {
      IEnumerator e = this.CashClean();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      e = this.ui3DModel.PickUpUnit(MasterData.UnitUnit[this.pickupUnitIdList[id]]);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.ui3DModel.model_creater_.UnitModel.SetActive(true);
      this.modelPrefabList.Add(this.ui3DModel.model_creater_.UnitModel);
      this.ChangeCharacterModel(id);
    }
  }

  private void ChangeCharacterModel(int id)
  {
    this.ui3DModel.ModelTarget.localEulerAngles = new Vector3(0.0f, -45f, 0.0f);
    this.modelPrefabList.ForEachIndex<GameObject>((System.Action<GameObject, int>) ((x, n) => x.SetActive(n == id)));
  }

  private void ChangeSkillIcon(int id)
  {
    if (this.spriteList.Count<UnityEngine.Sprite>() <= id)
      return;
    this.dyn_Unit_Skill.Init(this.spriteList[id]);
    this.dyn_Unit_Skill.iconSprite.SetDimensions(this.dyn_Unit_Skill.iconSprite.mainTexture.width == 60 ? 60 : 64, this.dyn_Unit_Skill.iconSprite.mainTexture.height == 62 ? 62 : 58);
  }

  private void SetStatus(int unitId)
  {
    UnitUnit unit = MasterData.UnitUnit[unitId];
    RarityIcon.SetRarity(unit, this.dyn_Star, true, false, false);
    this.txt_Title.SetTextLocalize(unit.name);
    this.dyn_Weapon.Init(unit.kind, unit.GetElement());
    this.txt_JobName.SetTextLocalize(unit.job.name.ToConverter());
    BattleskillSkill pickupSkill = unit.PickupSkill;
    if (pickupSkill != null)
      this.txt_Skillexplanation.SetTextLocalize(pickupSkill.description.ToConverter());
    else
      this.txt_Skillexplanation.text = string.Empty;
    BattleskillSkill rememberLeaderSkill = unit.RememberLeaderSkill;
    if (rememberLeaderSkill != null)
    {
      this.txt_LeaderSkillname.SetTextLocalize(rememberLeaderSkill.name.ToConverter());
      this.txt_leaderskill_description.SetTextLocalize(rememberLeaderSkill.description.ToConverter());
    }
    else
    {
      this.txt_LeaderSkillname.text = string.Empty;
      this.txt_leaderskill_description.text = string.Empty;
    }
  }

  private void LateUpdate()
  {
    if (this.coinBonus != null && this.coinBonus.IsChangedOnce())
    {
      if (this.gachaExFlag)
        this.slc_Kiseki_BonusEx.SetActive((uint) this.coinBonus.Value.Length > 0U);
      else
        this.slc_Kiseki_Bonus.SetActive((uint) this.coinBonus.Value.Length > 0U);
    }
    if (this.serverTime.HasValue && this.GachaModule.period.end_at.HasValue)
      this.serverTime = new DateTime?(ServerTime.NowAppTimeAddDelta());
    if (this.GachaModule == null || !this.GachaModule.period.display_count_down)
      return;
    this.kisekiEx.UpdateLimitTime(this.GachaModule, this.serverTime.Value);
  }
}
