﻿// Decompiled with JetBrains decompiler
// Type: BattleUI04SetHP
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class BattleUI04SetHP : MonoBehaviour
{
  [SerializeField]
  public GameObject[] hpNumbers;

  public void setValue(int v)
  {
    this.notDisplay();
    this.hpNumbers[v].SetActive(true);
  }

  public void notDisplay()
  {
    foreach (GameObject hpNumber in this.hpNumbers)
      hpNumber.SetActive(false);
  }
}
