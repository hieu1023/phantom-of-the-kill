﻿// Decompiled with JetBrains decompiler
// Type: Raid032RankingRewardConfScene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using System.Collections;
using UnityEngine;

public class Raid032RankingRewardConfScene : NGSceneBase
{
  [SerializeField]
  private Raid032RankingRewardConfMenu menu;

  public static void ChangeScene(bool stack, GuildRaid raid)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("raid032_PlayerRankingReward_conf", (stack ? 1 : 0) != 0, (object) raid);
  }

  public IEnumerator onStartSceneAsync(GuildRaid raid)
  {
    IEnumerator e = this.menu.Init(raid);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public void onStartScene(GuildRaid raid)
  {
    Singleton<CommonRoot>.GetInstance().isLoading = false;
  }
}
