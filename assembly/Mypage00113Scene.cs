﻿// Decompiled with JetBrains decompiler
// Type: Mypage00113Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

public class Mypage00113Scene : NGSceneBase
{
  public Mypage00113Menu menu;

  public static void changeScene(bool stack)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("mypage001_13", stack, (object[]) Array.Empty<object>());
  }

  public void onStartScene()
  {
    this.menu.Initialize();
  }
}
