﻿// Decompiled with JetBrains decompiler
// Type: BattleUI01UnitInformationTab
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;
using UnityEngine;

public abstract class BattleUI01UnitInformationTab : MonoBehaviour
{
  protected BattleUI01UnitInformation main_ { get; private set; }

  protected BL.Unit unit_ { get; private set; }

  public abstract IEnumerator initialize();

  public abstract BattleUI01UnitInformationTab.Type type { get; }

  public void preInitialize(BattleUI01UnitInformation main, BL.Unit unit)
  {
    this.main_ = main;
    this.unit_ = unit;
  }

  public bool isEnabled
  {
    get
    {
      return this.gameObject.activeSelf;
    }
    set
    {
      this.gameObject.SetActive(value);
    }
  }

  public enum Type
  {
    Skill,
    Weapon,
    Option,
  }
}
