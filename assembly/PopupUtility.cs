﻿// Decompiled with JetBrains decompiler
// Type: PopupUtility
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class PopupUtility : MonoBehaviour
{
  public static IEnumerator _999_11_1()
  {
    Future<GameObject> fobj = (Future<GameObject>) null;
    fobj = Res.Prefabs.popup.popup_999_11_1__anim_popup01.Load<GameObject>();
    IEnumerator e = fobj.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Singleton<PopupManager>.GetInstance().open(fobj.Result, false, false, false, true, false, false, "SE_1006").GetComponent<Shop999111Menu>().SetText();
  }

  public static IEnumerator _999_12_1()
  {
    Future<GameObject> fobj = (Future<GameObject>) null;
    fobj = Res.Prefabs.popup.popup_999_12_1__anim_popup01.Load<GameObject>();
    IEnumerator e = fobj.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Singleton<PopupManager>.GetInstance().open(fobj.Result, false, false, false, true, false, false, "SE_1006").GetComponent<Shop999121Menu>().SetText();
  }

  public static IEnumerator _007_16(float wait = 0.0f)
  {
    Player player_data = SMManager.Get<Player>();
    yield return (object) new WaitForSeconds(wait);
    int quantity = MasterData.ShopContent[100000033].quantity;
    int pri = MasterData.ShopArticle[10000003].price;
    int now = player_data.max_items;
    int max = player_data.max_items + quantity;
    Future<GameObject> fobj = (Future<GameObject>) null;
    fobj = Res.Prefabs.popup.popup_007_16__anim_popup01.Load<GameObject>();
    IEnumerator e = fobj.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    GameObject gameObject = Singleton<PopupManager>.GetInstance().open(fobj.Result, false, false, false, true, false, false, "SE_1006");
    gameObject.GetComponent<Shop00716Menu>().Init(pri, now, max);
    gameObject.GetComponent<Shop00716SetText>().SetText(now, max);
  }

  public static IEnumerator _007_14(float wait)
  {
    Player player_data = SMManager.Get<Player>();
    yield return (object) new WaitForSeconds(wait);
    int quantity = MasterData.ShopContent[100000022].quantity;
    int pri = MasterData.ShopArticle[10000002].price;
    int now = player_data.max_units;
    int max = player_data.max_units + quantity;
    Future<GameObject> fobj = (Future<GameObject>) null;
    fobj = Res.Prefabs.popup.popup_007_14__anim_popup01.Load<GameObject>();
    IEnumerator e = fobj.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    GameObject gameObject = Singleton<PopupManager>.GetInstance().open(fobj.Result, false, false, false, true, false, false, "SE_1006");
    gameObject.GetComponent<Shop00714Menu>().Init(pri, now, max);
    gameObject.GetComponent<Shop00714SetText>().SetText(now, max);
  }

  public static IEnumerator _999_7_1(ShopArticle shopArticle)
  {
    Future<GameObject> fobj = (Future<GameObject>) null;
    fobj = Res.Prefabs.popup.popup_999_7_1__anim_popup01.Load<GameObject>();
    IEnumerator e = fobj.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Singleton<PopupManager>.GetInstance().open(fobj.Result, false, false, false, true, false, false, "SE_1006").GetComponent<Shop99971Menu>().SetText(shopArticle);
  }

  public static IEnumerator _999_7_1(CommonPayType payType)
  {
    Future<GameObject> fobj = (Future<GameObject>) null;
    fobj = Res.Prefabs.popup.popup_999_7_1__anim_popup01.Load<GameObject>();
    IEnumerator e = fobj.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Singleton<PopupManager>.GetInstance().open(fobj.Result, false, false, false, true, false, false, "SE_1006").GetComponent<Shop99971Menu>().SetText(payType);
  }

  public static IEnumerator _999_6_1(bool Gacha)
  {
    Player player_data = SMManager.Get<Player>();
    int now = ((IEnumerable<PlayerItem>) ((IEnumerable<PlayerItem>) SMManager.Get<PlayerItem[]>()).Where<PlayerItem>((Func<PlayerItem, bool>) (x => x.entity_type == MasterDataTable.CommonRewardType.gear && !x.isReisou())).ToArray<PlayerItem>()).ToList<PlayerItem>().Count;
    int max = player_data.max_items;
    Future<GameObject> fobj = (Future<GameObject>) null;
    fobj = !Gacha ? Res.Prefabs.popup.popup_999_6_1__anim_popup01.Load<GameObject>() : Res.Prefabs.popup.popup_999_6_1a__anim_popup01.Load<GameObject>();
    IEnumerator e = fobj.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    GameObject gameObject = Singleton<PopupManager>.GetInstance().open(fobj.Result, false, false, false, true, false, false, "SE_1006");
    if (Gacha)
      gameObject.GetComponent<Gacha99961aMenu>().SetText(now, max, player_data);
    else
      gameObject.GetComponent<Quest99961Menu>().SetText(now, max, player_data);
  }

  public static IEnumerator popupMaxReisou()
  {
    Player player = SMManager.Get<Player>();
    int now = ((IEnumerable<PlayerItem>) ((IEnumerable<PlayerItem>) SMManager.Get<PlayerItem[]>()).Where<PlayerItem>((Func<PlayerItem, bool>) (x => x.isReisou())).ToArray<PlayerItem>()).ToList<PlayerItem>().Count;
    int max = player.max_reisou_items;
    Future<GameObject> fobj = (Future<GameObject>) null;
    fobj = new ResourceObject("Prefabs/popup/popup_Item_Possession_limit_Over__anim_popup01").Load<GameObject>();
    IEnumerator e = fobj.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Singleton<PopupManager>.GetInstance().open(fobj.Result, false, false, false, true, false, false, "SE_1006").GetComponent<Quest999ReisouMenu>().SetText(now, max);
  }

  public static IEnumerator _999_5_1()
  {
    Player player_data = SMManager.Get<Player>();
    int now = ((IEnumerable<PlayerUnit>) SMManager.Get<PlayerUnit[]>()).ToList<PlayerUnit>().Count;
    int max = player_data.max_units;
    int reserves_now = SMManager.Get<PlayerUnitReservesCount>().count;
    int reserves_max = SMManager.Get<Player>().max_unit_reserves;
    Future<GameObject> fobj = (Future<GameObject>) null;
    fobj = Res.Prefabs.popup.popup_999_5_1a__anim_popup01.Load<GameObject>();
    IEnumerator e = fobj.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Singleton<PopupManager>.GetInstance().open(fobj.Result, false, false, false, true, false, false, "SE_1006").GetComponent<Gacha99951aMenu>().SetText(now, max, reserves_now, reserves_max, player_data);
  }

  public static IEnumerator BuyKiseki(bool isBattle = false)
  {
    IEnumerator e = PurchaseBehavior.OpenItemList(isBattle);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public static IEnumerator _999_15(string name = null)
  {
    Future<GameObject> fobj = (Future<GameObject>) null;
    fobj = Singleton<ResourceManager>.GetInstance().Load<GameObject>("Prefabs/popup/popup_999_15__anim_popup", 1f);
    IEnumerator e = fobj.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Singleton<PopupManager>.GetInstance().open(fobj.Result, false, false, false, true, false, false, "SE_1006").GetComponent<Shop99915Menu>().SetText(name);
  }

  public static IEnumerator _007_19()
  {
    Future<GameObject> prefab = Res.Prefabs.popup.popup_007_19__anim_popup01.Load<GameObject>();
    IEnumerator e = prefab.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Singleton<PopupManager>.GetInstance().openAlert(prefab.Result, false, false, (EventDelegate) null, false, true, false, true).GetComponent<Shop00719Menu>().setData();
  }

  public static IEnumerator _007_18()
  {
    Future<GameObject> prefab = Res.Prefabs.popup.popup_007_18__anim_popup01.Load<GameObject>();
    IEnumerator e = prefab.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Singleton<PopupManager>.GetInstance().openAlert(prefab.Result, false, false, (EventDelegate) null, false, true, false, true).GetComponent<Shop00718Menu>().setData();
  }

  public static IEnumerator _999_3_1(
    string text1 = "",
    string text2 = "",
    string text3 = "",
    Gacha99931Menu.PaymentType paymentType = Gacha99931Menu.PaymentType.ALL)
  {
    Future<GameObject> prefab = Res.Prefabs.popup.popup_999_3_1__anim_popup01.Load<GameObject>();
    IEnumerator e = prefab.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Singleton<PopupManager>.GetInstance().open(prefab.Result, false, false, false, true, false, false, "SE_1006").GetComponent<Gacha99931Menu>().SetText(text1, text2, text3, paymentType);
  }

  public static IEnumerator SeaError(WebAPI.Response.UserError error)
  {
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    ModalWindow.Show(error.Code, error.Reason, (System.Action) (() =>
    {
      Singleton<NGGameDataManager>.GetInstance().lastReferenceUnitID = -1;
      Singleton<NGGameDataManager>.GetInstance().lastReferenceUnitIndex = -1;
      Singleton<NGSceneManager>.GetInstance().destroyLoadedScenes();
      Singleton<NGSceneManager>.GetInstance().clearStack();
      Singleton<NGGameDataManager>.GetInstance().isCallHomeUpdateAllData = true;
      Singleton<NGGameDataManager>.GetInstance().refreshHomeHome = true;
      MypageScene.ChangeScene(false, false, false);
    }));
    yield return (object) null;
  }

  public static IEnumerator SeaErrorStartUp(WebAPI.Response.UserError error)
  {
    ModalWindow.Show(error.Code, error.Reason, (System.Action) (() =>
    {
      Singleton<NGGameDataManager>.GetInstance().IsSea = false;
      Singleton<NGGameDataManager>.GetInstance().StartSceneAsyncProxy((System.Action<NGGameDataManager.StartSceneProxyResult>) (data => MypageScene.ChangeScene(false, false, false)));
    }));
    yield return (object) null;
  }
}
