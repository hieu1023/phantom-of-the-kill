﻿// Decompiled with JetBrains decompiler
// Type: DetailMenu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnitDetails;
using UnityEngine;

public class DetailMenu : DetailMenuBase
{
  private static int currentLoadTimes = 0;
  private static readonly string specialIconSpriteBaseName = "slc_icon_specific_effectiveness_{0}.png__GUI__unit_detail{1}__unit_detail{1}_prefab";
  public const int GearSlot1 = 0;
  public const int GearSlot2 = 1;
  public const int GearSlotNum = 2;
  private const int maxLoadTimesBeforeGC = 10;
  private Vector3 modelPos;
  [SerializeField]
  protected UI2DSprite DynCharacter;
  public bool isFavorited;
  [SerializeField]
  private GameObject dirFavorite;
  [SerializeField]
  private GameObject btnFavoritedOff;
  [SerializeField]
  private GameObject btnFavoritedOn;
  [SerializeField]
  private GameObject btnIntimacy;
  [SerializeField]
  private GameObject dirMainWeapon;
  [SerializeField]
  private GameObject link_MainWeapon;
  [SerializeField]
  private GameObject dirMainWeapon2;
  [SerializeField]
  private GameObject link_MainWeapon2;
  [SerializeField]
  private GameObject slc_MainWeapon2_NonBase;
  [SerializeField]
  private GameObject dirMainWeaponMulti;
  [SerializeField]
  private GameObject[] link_MainWeaponMulti;
  private ItemIcon[] gearIcons;
  [SerializeField]
  private UnitDetailGroupButton[] groupBtns;
  [SerializeField]
  private SpriteSelectDirectButton[] groupSprites;
  [SerializeField]
  private UIGrid groupBtnGrid;
  [SerializeField]
  private NGHorizontalScrollParts informationScrollView;
  [SerializeField]
  private DetailMenuScrollViewBase[] informationPanels;
  [SerializeField]
  private GameObject dirSpecial;
  [SerializeField]
  private UISprite specialIcon;
  [SerializeField]
  private UILabel specialFactor;
  [SerializeField]
  private UILabel specialEventName;
  [SerializeField]
  private GameObject floatingSpecialPointDialog;
  private Unit0042FloatingSpecialPointDialog floatingSpecialPointDialogObject;
  [SerializeField]
  private UISprite slcCountry;
  [SerializeField]
  private UI2DSprite slcInclusion;
  [SerializeField]
  private GameObject floatingGroupDialog;
  private GameObject floatingGroupDialogObject;
  private PlayerItem[] equippedGears;
  private PlayerUnit _playerUnit;
  private bool isLimit;
  private GameObject jobAbilityUpPrefab;
  private GameObject jobAbilityDialogPrefab;
  private bool isModelLoading;
  private NGSoundManager sm;
  public bool isEarthMode;
  public bool isMemory;
  [SerializeField]
  private UIButton IbtnUnitTraining;
  [SerializeField]
  private Transform dirUnitTrainingContainer;
  private GameObject trainingPrefab;
  private DeteilTraining training;
  [SerializeField]
  private UIButton btnSpeciality;
  [SerializeField]
  [Tooltip("主スクロール用Collider")]
  private BoxCollider[] mainScrollColliders;
  [SerializeField]
  [Tooltip("下部スクロール用Collider")]
  private BoxCollider[] bottomScrollColliders;
  [SerializeField]
  private UIButton[] btnJobAbilities_;

  private bool AwakeUnit
  {
    get
    {
      return !(this._playerUnit == (PlayerUnit) null) && this._playerUnit.unit.awake_unit_flag;
    }
  }

  public Vector3 ModelPos
  {
    set
    {
      this.modelPos = value;
    }
    get
    {
      return this.modelPos;
    }
  }

  public NGHorizontalScrollParts InformationScrollView
  {
    get
    {
      return this.informationScrollView;
    }
  }

  public PlayerUnit PlayerUnit
  {
    get
    {
      return this._playerUnit;
    }
  }

  public PlayerItem EquippedGearSlot1
  {
    get
    {
      return this.equippedGears[0];
    }
  }

  public PlayerItem EquippedGearSlot2
  {
    get
    {
      return this.equippedGears[1];
    }
  }

  private void deleteMainScrollColliders()
  {
    if (this.mainScrollColliders == null)
      return;
    for (int index = 0; index < this.mainScrollColliders.Length; ++index)
    {
      if (!((UnityEngine.Object) this.mainScrollColliders[index] == (UnityEngine.Object) null))
      {
        UIDragScrollView component = this.mainScrollColliders[index].GetComponent<UIDragScrollView>();
        UnityEngine.Object.Destroy((UnityEngine.Object) this.mainScrollColliders[index]);
        if ((UnityEngine.Object) component != (UnityEngine.Object) null)
          UnityEngine.Object.Destroy((UnityEngine.Object) component);
      }
    }
    this.mainScrollColliders = (BoxCollider[]) null;
  }

  private void deleteBottomScrollColliders()
  {
    if (this.bottomScrollColliders == null)
      return;
    for (int index = 0; index < this.bottomScrollColliders.Length; ++index)
    {
      if (!((UnityEngine.Object) this.bottomScrollColliders[index] == (UnityEngine.Object) null))
      {
        UIDragScrollView component = this.bottomScrollColliders[index].GetComponent<UIDragScrollView>();
        UnityEngine.Object.Destroy((UnityEngine.Object) this.bottomScrollColliders[index]);
        if ((UnityEngine.Object) component != (UnityEngine.Object) null)
          UnityEngine.Object.Destroy((UnityEngine.Object) component);
      }
    }
    this.bottomScrollColliders = (BoxCollider[]) null;
  }

  public void IbtnIntimacy()
  {
    if (this.IsPushAndSet())
      return;
    if (this.isEarthMode)
    {
      if (Singleton<NGGameDataManager>.GetInstance().IsSea)
        Singleton<CommonRoot>.GetInstance().headerType = CommonRoot.HeaderType.Earth;
      Singleton<NGSceneManager>.GetInstance().changeScene("unit054_2_2", true, (object) this._playerUnit);
    }
    else
    {
      if (Singleton<NGGameDataManager>.GetInstance().IsSea)
        Singleton<CommonRoot>.GetInstance().headerType = CommonRoot.HeaderType.Normal;
      Singleton<NGSceneManager>.GetInstance().changeScene("unit004_2_2", true, (object) this._playerUnit);
    }
  }

  public void IbtnZoom()
  {
    if (this.IsPushAndSet())
      return;
    if (this.isEarthMode)
      Unit0543Scene.changeScene(true, this._playerUnit);
    else
      Unit0043Scene.changeScene(true, this._playerUnit, false);
  }

  public void IbtnWpEquip(int index)
  {
    if (this.IsPushAndSet() || this.isLimit)
      return;
    if (this.isEarthMode)
    {
      if (Singleton<NGGameDataManager>.GetInstance().IsSea)
        Singleton<CommonRoot>.GetInstance().headerType = CommonRoot.HeaderType.Earth;
      Unit0544Scene.ChangeScene(true, this._playerUnit, 1);
    }
    else
    {
      if (Singleton<NGGameDataManager>.GetInstance().IsSea)
        Singleton<CommonRoot>.GetInstance().headerType = CommonRoot.HeaderType.Normal;
      Unit0044Scene.ChangeScene(true, this._playerUnit, index);
    }
  }

  public void IbtnFavoriteToggle()
  {
    this.SetFavorite(!this.isFavorited);
  }

  public void SetFavorite(bool active)
  {
    if (!this.isEarthMode)
      return;
    this.isFavorited = active;
    this.btnFavoritedOff.SetActive(!active);
    this.btnFavoritedOn.SetActive(active);
    this.menu.UpdateSetting(this._playerUnit.id, this.isFavorited);
  }

  public void IbtnStatusDetail()
  {
    this.StartCoroutine(this.openStatusDetail(this._playerUnit));
  }

  private IEnumerator openStatusDetail(PlayerUnit playerUnit)
  {
    DetailMenu detailMenu = this;
    GameObject pop = Singleton<PopupManager>.GetInstance().open(detailMenu.menu.StatusDetailPrefab, false, false, false, true, false, true, "SE_1006");
    Unit004StatusDetailDialog dialog = pop.GetComponent<Unit004StatusDetailDialog>();
    dialog.gameObject.SetActive(false);
    Future<UnityEngine.Sprite> futureSprite = playerUnit.unit.LoadSpriteLarge(playerUnit.job_id, 1f);
    IEnumerator e = futureSprite.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    dialog.Initialize(detailMenu.PlayerUnit, futureSprite.Result, detailMenu.isMemory);
    Singleton<PopupManager>.GetInstance().startOpenAnime(pop, false);
  }

  public void IbtnTraining()
  {
    this.training.Show();
  }

  public void IbtnUnitQuest()
  {
    if (this.menu.IsPushAndSet())
      return;
    Singleton<NGGameDataManager>.GetInstance().IsSea = false;
    Quest00214Scene.ChangeScene(true, this._playerUnit.unit.same_character_id, false, true);
  }

  public void IbtnJobTokusei0()
  {
    this.StartCoroutine(this.openTokusei(0));
  }

  public void IbtnJobTokusei1()
  {
    this.StartCoroutine(this.openTokusei(1));
  }

  public void IbtnJobTokusei2()
  {
    this.StartCoroutine(this.openTokusei(2));
  }

  public void IbtnJobTokusei3()
  {
    this.StartCoroutine(this.openTokusei(3));
  }

  private IEnumerator openTokusei(int range)
  {
    PlayerUnitJob_abilities[] playerJobAbilities = this._playerUnit.job_abilities;
    if (playerJobAbilities.Length != 0 && playerJobAbilities.Length > range && playerJobAbilities[range].master != null)
    {
      if (!Singleton<NGSoundManager>.GetInstance().IsVoiceStopAll())
        Singleton<NGSoundManager>.GetInstance().StopVoice(-1, 0.0f);
      GameObject popup;
      Future<GameObject> prefab;
      IEnumerator e;
      if (!this.isLimit && !this._playerUnit.is_storage && playerJobAbilities[range].current_levelup_pattern != null)
      {
        if ((UnityEngine.Object) this.jobAbilityUpPrefab == (UnityEngine.Object) null)
        {
          prefab = !Singleton<NGGameDataManager>.GetInstance().IsSea ? Res.Prefabs.unit004_Job.Unit_JobCharacteristic_UP_Dialog.Load<GameObject>() : Res.Prefabs.unit004_Job.Unit_JobCharacteristic_UP_Dialog_sea.Load<GameObject>();
          yield return (object) prefab.Wait();
          this.jobAbilityUpPrefab = prefab.Result;
          prefab = (Future<GameObject>) null;
        }
        popup = Singleton<PopupManager>.GetInstance().open(this.jobAbilityUpPrefab, false, false, false, true, true, true, "SE_1006");
        DetailMenuScrollViewJob menuScrollViewJob = (DetailMenuScrollViewJob) null;
        foreach (DetailMenuScrollViewBase informationPanel in this.informationPanels)
        {
          if (informationPanel is DetailMenuScrollViewJob)
          {
            menuScrollViewJob = (DetailMenuScrollViewJob) informationPanel;
            break;
          }
        }
        e = popup.GetComponent<Unit004JobDialogUp>().Init(this._playerUnit, playerJobAbilities[range], (UnityEngine.Object) menuScrollViewJob != (UnityEngine.Object) null ? new System.Action(menuScrollViewJob.updateJobAbility) : (System.Action) null, false);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        Singleton<PopupManager>.GetInstance().startOpenAnime(popup, false);
        popup = (GameObject) null;
      }
      else
      {
        if ((UnityEngine.Object) this.jobAbilityDialogPrefab == (UnityEngine.Object) null)
        {
          prefab = Res.Prefabs.unit004_Job.Unit_JobCharacteristic_Dialog.Load<GameObject>();
          yield return (object) prefab.Wait();
          this.jobAbilityDialogPrefab = prefab.Result;
          prefab = (Future<GameObject>) null;
        }
        popup = Singleton<PopupManager>.GetInstance().open(this.jobAbilityDialogPrefab, false, false, false, true, false, false, "SE_1006");
        popup.SetActive(false);
        e = popup.GetComponent<Unit004JobDialog>().Init(playerJobAbilities[range]);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        popup.SetActive(true);
        popup = (GameObject) null;
      }
    }
  }

  public void IbtnSpeciality()
  {
    if (this.IsPushAndSet())
      return;
    this.StartCoroutine(this.ShowSpecialityPopup());
  }

  private IEnumerator ShowSpecialityPopup()
  {
    DetailMenu detailMenu = this;
    Future<GameObject> f = Singleton<ResourceManager>.GetInstance().Load<GameObject>(Singleton<NGGameDataManager>.GetInstance().IsSea ? "Prefabs/popup/popup_004_specialty_details_sea" : "Prefabs/popup/popup_004_specialty_details", 1f);
    IEnumerator e = f.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if (!((UnityEngine.Object) f.Result == (UnityEngine.Object) null))
    {
      GameObject popup = f.Result.Clone((Transform) null);
      popup.SetActive(false);
      e = popup.GetComponent<Popup004SpecialtyDetails>().Init(detailMenu.PlayerUnit.unit.compose_max_unity_value_setting_id);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      popup.SetActive(true);
      Singleton<PopupManager>.GetInstance().open(popup, false, false, true, true, false, false, "SE_1006");
      detailMenu.IsPush = false;
    }
  }

  private void Update()
  {
    if ((UnityEngine.Object) this.menu == (UnityEngine.Object) null)
      return;
    if (this.menu.CurrentIndex == this.index)
    {
      this.informationScrollView.SeEnable = true;
      if (this.menu.InfoIndex == this.informationScrollView.selected)
        return;
      this.menu.InfoIndex = Math.Max(this.informationScrollView.selected, 0);
      this.menu.UpdateInfoIndicator(this.menu.InfoIndex);
    }
    else
      this.informationScrollView.SeEnable = false;
  }

  private void Update3DModelViewActive()
  {
    if (this.isModelLoading || (UnityEngine.Object) this.menu == (UnityEngine.Object) null || !this.isEarthMode)
      return;
    if (this.informationScrollView.selected != 0)
    {
      if (!((UnityEngine.Object) this.informationPanels[0].GetComponent<DetailMenuScrollView01>().UI3DModel != (UnityEngine.Object) null))
        return;
      this.informationPanels[0].GetComponent<DetailMenuScrollView01>().UI3DModel.gameObject.SetActive(false);
    }
    else
    {
      if (!((UnityEngine.Object) this.informationPanels[0].GetComponent<DetailMenuScrollView01>().UI3DModel != (UnityEngine.Object) null))
        return;
      this.informationPanels[0].GetComponent<DetailMenuScrollView01>().UI3DModel.gameObject.SetActive(true);
    }
  }

  public override IEnumerator SetInformationPanelIndex(int infoIndex)
  {
    // ISSUE: reference to a compiler-generated field
    int num = this.\u003C\u003E1__state;
    DetailMenu detailMenu = this;
    if (num != 0)
      return false;
    // ISSUE: reference to a compiler-generated field
    this.\u003C\u003E1__state = -1;
    if ((UnityEngine.Object) detailMenu.menu == (UnityEngine.Object) null)
      return false;
    if (detailMenu.menu.CurrentIndex != detailMenu.index && infoIndex != detailMenu.informationScrollView.selected)
      detailMenu.informationScrollView.resetCenterItem(infoIndex);
    detailMenu.Update3DModelViewActive();
    return false;
  }

  private IEnumerator InitInformationPanels(
    PlayerUnit playerUnit,
    int infoIndex,
    bool isLimit,
    bool isMaterial,
    bool isUpdate = false)
  {
    DetailMenu detailMenu = this;
    List<GameObject[]> prefabs;
    if (detailMenu.isEarthMode)
      prefabs = new List<GameObject[]>()
      {
        new GameObject[1]{ detailMenu.menu.skillDetailDialogPrefab },
        new GameObject[6]
        {
          detailMenu.menu.skillDetailDialogPrefab,
          detailMenu.menu.gearKindIconPrefab,
          detailMenu.menu.profIconPrefab,
          detailMenu.menu.skillTypeIconPrefab,
          detailMenu.menu.skillDetailDialog_changePrefab,
          detailMenu.menu.skillListPrefab
        },
        new GameObject[5]
        {
          detailMenu.menu.skillDetailDialogPrefab,
          detailMenu.menu.gearKindIconPrefab,
          detailMenu.menu.commonElementIconPrefab,
          detailMenu.menu.spAtkTypeIconPrefab,
          detailMenu.menu.skillDetailDialog_changePrefab
        },
        (GameObject[]) null,
        (GameObject[]) null
      };
    else
      prefabs = new List<GameObject[]>()
      {
        new GameObject[7]
        {
          detailMenu.menu.skillDetailDialogPrefab,
          detailMenu.menu.gearKindIconPrefab,
          detailMenu.menu.profIconPrefab,
          detailMenu.menu.unityDetailPrefab,
          detailMenu.menu.overkillersSlotReleasePrefab,
          detailMenu.menu.unitIconPrefab,
          detailMenu.menu.stageItemPrefab
        },
        new GameObject[3]
        {
          detailMenu.menu.skillDetailDialogPrefab,
          detailMenu.menu.skillTypeIconPrefab,
          detailMenu.menu.skillLockIconPrefab
        },
        new GameObject[3]
        {
          detailMenu.menu.skillfullnessIconPrefab,
          detailMenu.menu.specialPointDetailDialogPrefab,
          detailMenu.menu.terraiAbilityDialogPrefab
        },
        new GameObject[3]
        {
          detailMenu.menu.skillDetailDialogPrefab,
          detailMenu.menu.gearKindIconPrefab,
          detailMenu.menu.commonElementIconPrefab
        }
      };
    Unit0042Scene scene = detailMenu.menu.gameObject.GetComponent<Unit0042Scene>();
    bool isOverkillersView = (UnityEngine.Object) scene != (UnityEngine.Object) null && scene.bootParam.controlFlags.IsOn(Control.OverkillersUnit);
    if (isOverkillersView)
    {
      detailMenu.deleteMainScrollColliders();
      detailMenu.deleteBottomScrollColliders();
    }
    DetailMenuScrollViewParam viewParam = (DetailMenuScrollViewParam) null;
    for (int i = 0; i < detailMenu.informationPanels.Length; ++i)
    {
      DetailMenuScrollViewBase informationPanel = detailMenu.informationPanels[i];
      informationPanel.isEarthMode = detailMenu.isEarthMode;
      informationPanel.isMemory = detailMenu.isMemory;
      informationPanel.setControlFlags(scene);
      DetailMenuScrollViewParam menuScrollViewParam;
      if ((UnityEngine.Object) (menuScrollViewParam = informationPanel as DetailMenuScrollViewParam) != (UnityEngine.Object) null)
        menuScrollViewParam.changeTab(detailMenu.menu.viewParamTabMode);
      if ((UnityEngine.Object) viewParam == (UnityEngine.Object) null && (UnityEngine.Object) menuScrollViewParam != (UnityEngine.Object) null)
        viewParam = menuScrollViewParam;
      if (isOverkillersView && (UnityEngine.Object) menuScrollViewParam == (UnityEngine.Object) null)
      {
        if (informationPanel.gameObject.activeSelf)
          informationPanel.gameObject.SetActive(false);
      }
      else if (informationPanel.Init(playerUnit))
      {
        if (i == infoIndex)
        {
          IEnumerator e = informationPanel.initAsync(playerUnit, isLimit, isMaterial, prefabs[i]);
          while (e.MoveNext())
            yield return e.Current;
          e = (IEnumerator) null;
        }
        else
          detailMenu.StartCoroutine(detailMenu.InitInformationPanelAsync(informationPanel, playerUnit, isLimit, isMaterial, prefabs[i]));
      }
    }
    detailMenu.informationScrollView.resetCenterItem(infoIndex);
    if (isOverkillersView)
    {
      detailMenu.informationScrollView.uiScrollView.enabled = false;
      detailMenu.informationScrollView.leftArrow.SetActive(false);
      detailMenu.informationScrollView.rightArrow.SetActive(false);
      detailMenu.informationScrollView.disabledClipping();
      if ((UnityEngine.Object) detailMenu.informationScrollView.dot != (UnityEngine.Object) null)
        detailMenu.informationScrollView.dot.gameObject.SetActive(false);
      if ((UnityEngine.Object) viewParam != (UnityEngine.Object) null)
      {
        UIScrollView inParents = NGUITools.FindInParents<UIScrollView>(detailMenu.transform.parent);
        foreach (UIDragScrollView componentsInChild in viewParam.GetComponentsInChildren<UIDragScrollView>(true))
          componentsInChild.scrollView = inParents;
      }
    }
    if (detailMenu.isEarthMode)
    {
      detailMenu.isModelLoading = true;
      // ISSUE: reference to a compiler-generated method
      yield return (object) detailMenu.informationPanels[0].GetComponent<DetailMenuScrollView01>().setModel(playerUnit, detailMenu.menu.modelPrefab, detailMenu.ModelPos, detailMenu.menu.LightON, new System.Action(detailMenu.\u003CInitInformationPanels\u003Eb__86_0));
    }
  }

  private IEnumerator InitInformationPanelAsync(
    DetailMenuScrollViewBase panel,
    PlayerUnit playerUnit,
    bool isLimit,
    bool isMaterial,
    GameObject[] prefabs)
  {
    yield return (object) new WaitWhile((Func<bool>) (() => Singleton<NGSceneManager>.GetInstance().IsDangerAsyncLoadResource()));
    yield return (object) panel.initAsync(playerUnit, isLimit, isMaterial, prefabs);
  }

  private IEnumerator ItemGearFavoriteAscync(
    int[] favorite_player_gear_ids,
    int[] un_favorite_player_gear_ids)
  {
    IEnumerator e = WebAPI.ItemGearFavorite(favorite_player_gear_ids, un_favorite_player_gear_ids, (System.Action<WebAPI.Response.UserError>) null).Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public IEnumerator onEndSceneAsync()
  {
    if ((UnityEngine.Object) this.sm != (UnityEngine.Object) null)
    {
      this.sm.stopVoice(-1);
      yield break;
    }
  }

  private void DisplayGroupLogo(UnitUnit playerUnit)
  {
    if (!this.isEarthMode)
      return;
    int index = 0;
    ((IEnumerable<UnitDetailGroupButton>) this.groupBtns).ForEach<UnitDetailGroupButton>((System.Action<UnitDetailGroupButton>) (x => x.gameObject.SetActive(false)));
    UnitGroup groupInfo = ((IEnumerable<UnitGroup>) MasterData.UnitGroupList).FirstOrDefault<UnitGroup>((Func<UnitGroup, bool>) (x => x.unit_id == playerUnit.ID));
    if (groupInfo == null)
      return;
    UnitGroupLargeCategory groupLargeCategory = ((IEnumerable<UnitGroupLargeCategory>) MasterData.UnitGroupLargeCategoryList).FirstOrDefault<UnitGroupLargeCategory>((Func<UnitGroupLargeCategory, bool>) (x => x.ID == groupInfo.group_large_category_id_UnitGroupLargeCategory));
    UnitGroupSmallCategory groupSmallCategory = ((IEnumerable<UnitGroupSmallCategory>) MasterData.UnitGroupSmallCategoryList).FirstOrDefault<UnitGroupSmallCategory>((Func<UnitGroupSmallCategory, bool>) (x => x.ID == groupInfo.group_small_category_id_UnitGroupSmallCategory));
    UnitGroupClothingCategory clothingCategory1 = ((IEnumerable<UnitGroupClothingCategory>) MasterData.UnitGroupClothingCategoryList).FirstOrDefault<UnitGroupClothingCategory>((Func<UnitGroupClothingCategory, bool>) (x => x.ID == groupInfo.group_clothing_category_id_UnitGroupClothingCategory));
    UnitGroupClothingCategory clothingCategory2 = ((IEnumerable<UnitGroupClothingCategory>) MasterData.UnitGroupClothingCategoryList).FirstOrDefault<UnitGroupClothingCategory>((Func<UnitGroupClothingCategory, bool>) (x => x.ID == groupInfo.group_clothing_category_id_2_UnitGroupClothingCategory));
    UnitGroupGenerationCategory generationCategory = ((IEnumerable<UnitGroupGenerationCategory>) MasterData.UnitGroupGenerationCategoryList).FirstOrDefault<UnitGroupGenerationCategory>((Func<UnitGroupGenerationCategory, bool>) (x => x.ID == groupInfo.group_generation_category_id_UnitGroupGenerationCategory));
    if (groupLargeCategory == null && groupSmallCategory == null && (clothingCategory1 == null && clothingCategory2 == null) && generationCategory == null)
      return;
    if (groupLargeCategory != null && groupLargeCategory.ID != 1)
    {
      this.groupSprites[index].SetSpriteName<string>(groupLargeCategory.GetSpriteName(), true);
      this.groupBtns[index].gameObject.SetActive(true);
      ++index;
    }
    if (groupSmallCategory != null && groupSmallCategory.ID != 1)
    {
      this.groupSprites[index].SetSpriteName<string>(groupSmallCategory.GetSpriteName(), true);
      this.groupBtns[index].gameObject.SetActive(true);
      ++index;
    }
    if (clothingCategory1 != null && clothingCategory1.ID != 1)
    {
      this.groupSprites[index].SetSpriteName<string>(clothingCategory1.GetSpriteName(), true);
      this.groupBtns[index].gameObject.SetActive(true);
      ++index;
    }
    if (clothingCategory2 != null && clothingCategory2.ID != 1)
    {
      this.groupSprites[index].SetSpriteName<string>(clothingCategory2.GetSpriteName(), true);
      this.groupBtns[index].gameObject.SetActive(true);
      ++index;
    }
    if (generationCategory != null && generationCategory.ID != 1)
    {
      this.groupSprites[index].SetSpriteName<string>(generationCategory.GetSpriteName(), true);
      this.groupBtns[index].gameObject.SetActive(true);
      int num = index + 1;
    }
    this.groupBtnGrid.repositionNow = true;
  }

  public override IEnumerator Init(
    Unit0042Menu menu,
    int index,
    PlayerUnit playerUnit,
    int infoIndex,
    bool isLimit,
    bool isMaterial,
    QuestScoreBonusTimetable[] tables,
    UnitBonus[] unitBonus,
    bool isUpdate = true)
  {
    DetailMenu detailMenu = this;
    playerUnit.resetOnceOverkillers();
    detailMenu.menu = menu;
    detailMenu.index = index;
    detailMenu._playerUnit = playerUnit;
    detailMenu.gearIcons = new ItemIcon[2];
    detailMenu.equippedGears = new PlayerItem[2];
    detailMenu.equippedGears[0] = playerUnit.equippedGear;
    detailMenu.equippedGears[1] = playerUnit.equippedGear2;
    detailMenu.isLimit = isLimit;
    detailMenu.informationScrollView.SeEnable = false;
    Transform childInFind1 = detailMenu.transform.GetChildInFind("Top");
    childInFind1.transform.localPosition = new Vector3(0.0f, (float) (((double) menu.scrollView.scrollView.panel.GetViewSize().y - (double) childInFind1.GetComponent<UIWidget>().height) / 2.0), 0.0f);
    Transform childInFind2 = detailMenu.transform.GetChildInFind("Bottom__anim_fade01");
    childInFind2.transform.localPosition = new Vector3(0.0f, (float) -(((double) menu.scrollView.scrollView.panel.GetViewSize().y - (double) childInFind2.GetComponent<UIWidget>().height) / 2.0), 0.0f);
    if ((UnityEngine.Object) detailMenu.floatingSpecialPointDialog != (UnityEngine.Object) null)
    {
      if ((UnityEngine.Object) detailMenu.floatingSpecialPointDialogObject == (UnityEngine.Object) null)
        detailMenu.floatingSpecialPointDialogObject = menu.specialPointDetailDialogPrefab.Clone(detailMenu.floatingSpecialPointDialog.transform).GetComponentInChildren<Unit0042FloatingSpecialPointDialog>();
      detailMenu.floatingSpecialPointDialogObject.transform.parent.gameObject.SetActive(false);
    }
    IEnumerator e = detailMenu.InitPlayer(playerUnit, infoIndex, detailMenu.menu.gearIconPrefab, tables, unitBonus);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    e = detailMenu.InitInformationPanels(playerUnit, infoIndex, isLimit, isMaterial, isUpdate);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if ((UnityEngine.Object) detailMenu.floatingGroupDialog != (UnityEngine.Object) null)
    {
      if ((UnityEngine.Object) detailMenu.floatingGroupDialogObject == (UnityEngine.Object) null)
        detailMenu.floatingGroupDialogObject = menu.GroupDetailDialogPrefab.Clone(detailMenu.floatingGroupDialog.transform);
      detailMenu.floatingGroupDialogObject.SetActive(false);
      detailMenu.DisplayGroupLogo(detailMenu.PlayerUnit.unit);
    }
    if (isLimit)
      detailMenu.LimitMode();
    if (((isMaterial ? 1 : (detailMenu.PlayerUnit.is_storage ? 1 : 0)) | (isLimit ? 1 : 0)) != 0 && (UnityEngine.Object) detailMenu.IbtnUnitTraining != (UnityEngine.Object) null)
      detailMenu.IbtnUnitTraining.gameObject.SetActive(false);
    if (detailMenu.PlayerUnit.is_storage)
      detailMenu.LimitStorageMode();
    if ((UnityEngine.Object) detailMenu.slcCountry != (UnityEngine.Object) null)
    {
      detailMenu.slcCountry.gameObject.SetActive(false);
      if (playerUnit.unit.country_attribute.HasValue)
      {
        detailMenu.slcCountry.gameObject.SetActive(true);
        playerUnit.unit.SetCuntrySpriteName(ref detailMenu.slcCountry);
      }
    }
    if ((UnityEngine.Object) detailMenu.slcInclusion != (UnityEngine.Object) null)
    {
      detailMenu.slcInclusion.gameObject.SetActive(false);
      if (playerUnit.unit.inclusion_ip.HasValue)
      {
        detailMenu.slcInclusion.gameObject.SetActive(true);
        e = playerUnit.unit.SetInclusionIP(detailMenu.slcInclusion);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
      }
    }
    if (!detailMenu.isEarthMode && !isMaterial && (!detailMenu.PlayerUnit.is_storage && !isLimit))
    {
      if ((UnityEngine.Object) detailMenu.trainingPrefab == (UnityEngine.Object) null)
        detailMenu.trainingPrefab = menu.TrainingPrefab.Clone(detailMenu.dirUnitTrainingContainer);
      if ((UnityEngine.Object) detailMenu.training == (UnityEngine.Object) null)
        detailMenu.training = detailMenu.trainingPrefab.GetComponent<DeteilTraining>();
      detailMenu.training.Initialize(detailMenu.PlayerUnit, menu.UnitList);
    }
    if ((UnityEngine.Object) detailMenu.training != (UnityEngine.Object) null)
      detailMenu.training.Hide();
    if ((UnityEngine.Object) detailMenu.btnSpeciality != (UnityEngine.Object) null)
      detailMenu.btnSpeciality.gameObject.SetActive(detailMenu.PlayerUnit.unit.compose_max_unity_value_setting_id_ComposeMaxUnityValueSetting > 0);
  }

  private IEnumerator InitPlayer(
    PlayerUnit playerUnit,
    int infoIndex,
    GameObject gearIconPrefab,
    QuestScoreBonusTimetable[] tables,
    UnitBonus[] unitBonus)
  {
    DetailMenu detailMenu = this;
    if (!detailMenu.isEarthMode)
      detailMenu.SetFavorite(detailMenu.menu.GetSetting(playerUnit.id));
    if ((UnityEngine.Object) detailMenu.link_MainWeapon != (UnityEngine.Object) null)
      detailMenu.link_MainWeapon.transform.GetChildren().ForEach<Transform>((System.Action<Transform>) (transform => UnityEngine.Object.Destroy((UnityEngine.Object) transform.gameObject)));
    if ((UnityEngine.Object) detailMenu.link_MainWeapon2 != (UnityEngine.Object) null)
      detailMenu.link_MainWeapon2.transform.GetChildren().ForEach<Transform>((System.Action<Transform>) (transform => UnityEngine.Object.Destroy((UnityEngine.Object) transform.gameObject)));
    if (detailMenu.link_MainWeaponMulti != null)
    {
      for (int index = 0; index < detailMenu.link_MainWeaponMulti.Length; ++index)
        detailMenu.link_MainWeaponMulti[index].transform.GetChildren().ForEach<Transform>((System.Action<Transform>) (transform => UnityEngine.Object.Destroy((UnityEngine.Object) transform.gameObject)));
    }
    IEnumerator e;
    if (detailMenu.isEarthMode)
    {
      if ((UnityEngine.Object) detailMenu.dirMainWeaponMulti != (UnityEngine.Object) null)
      {
        detailMenu.dirMainWeapon.SetActive(!detailMenu.AwakeUnit);
        detailMenu.dirMainWeaponMulti.SetActive(detailMenu.AwakeUnit);
      }
      else if ((UnityEngine.Object) detailMenu.dirMainWeapon != (UnityEngine.Object) null)
        detailMenu.dirMainWeapon.SetActive(true);
      if (detailMenu.AwakeUnit)
      {
        e = detailMenu.SetGearIconMulti(gearIconPrefab);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
      }
      else
      {
        e = detailMenu.SetGearIconSingle(gearIconPrefab);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
      }
    }
    else
    {
      if ((UnityEngine.Object) detailMenu.dirMainWeapon != (UnityEngine.Object) null)
        detailMenu.dirMainWeapon.SetActive(true);
      if ((UnityEngine.Object) detailMenu.dirMainWeapon2 != (UnityEngine.Object) null)
        detailMenu.dirMainWeapon2.SetActive(detailMenu.AwakeUnit);
      if ((UnityEngine.Object) detailMenu.slc_MainWeapon2_NonBase != (UnityEngine.Object) null)
        detailMenu.slc_MainWeapon2_NonBase.SetActive(!detailMenu.AwakeUnit);
      e = detailMenu.SetGearIconSlot(gearIconPrefab, detailMenu.link_MainWeapon, 0);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      if (detailMenu.AwakeUnit)
      {
        e = detailMenu.SetGearIconSlot(gearIconPrefab, detailMenu.link_MainWeapon2, 1);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
      }
    }
    if ((UnityEngine.Object) detailMenu.dirSpecial != (UnityEngine.Object) null)
    {
      string str1 = playerUnit.SpecialEffectType((IEnumerable<QuestScoreBonusTimetable>) tables, (IEnumerable<UnitBonus>) unitBonus);
      bool flag = !string.IsNullOrEmpty(str1);
      detailMenu.dirSpecial.SetActive(flag);
      if (flag)
      {
        string str2 = "×" + playerUnit.SpecialEffectFactor((IEnumerable<QuestScoreBonusTimetable>) tables, (IEnumerable<UnitBonus>) unitBonus);
        string str3 = playerUnit.SpecialEffectEventName((IEnumerable<QuestScoreBonusTimetable>) tables, (IEnumerable<UnitBonus>) unitBonus);
        string iconSpriteName = string.Format(DetailMenu.specialIconSpriteBaseName, (object) str1, Singleton<NGGameDataManager>.GetInstance().IsSea ? (object) "_sea" : (object) "");
        if ((UnityEngine.Object) detailMenu.specialIcon != (UnityEngine.Object) null)
          detailMenu.specialIcon.spriteName = iconSpriteName;
        if ((UnityEngine.Object) detailMenu.specialFactor != (UnityEngine.Object) null)
          detailMenu.specialFactor.SetTextLocalize(str2);
        if ((UnityEngine.Object) detailMenu.specialEventName != (UnityEngine.Object) null)
          detailMenu.specialEventName.SetTextLocalize(str3);
        if ((UnityEngine.Object) detailMenu.floatingSpecialPointDialogObject != (UnityEngine.Object) null)
          detailMenu.floatingSpecialPointDialogObject.setData(str2, str3, iconSpriteName);
      }
    }
    yield return (object) detailMenu.LoadCharacterImage(playerUnit);
  }

  public void IbtnSpecialPoint()
  {
    this.floatingSpecialPointDialogObject.Show();
  }

  private IEnumerator SetGearIconSingle(GameObject gearIconPrefab)
  {
    IEnumerator e = this.SetGearIconSlot(gearIconPrefab, this.link_MainWeapon, 0);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  private IEnumerator SetGearIconSlot(
    GameObject gearIconPrefab,
    GameObject linkObject,
    int slot)
  {
    DetailMenu detailMenu = this;
    detailMenu.gearIcons[slot] = gearIconPrefab.CloneAndGetComponent<ItemIcon>(linkObject.transform);
    detailMenu.gearIcons[slot].transform.localPosition = Vector3.zero;
    IEnumerator e;
    if (detailMenu.equippedGears[slot] != (PlayerItem) null)
    {
      e = detailMenu.gearIcons[slot].InitByPlayerItem(detailMenu.equippedGears[slot]);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      if (detailMenu.equippedGears[slot].broken)
        detailMenu.gearIcons[slot].Broken = true;
      detailMenu.gearIcons[slot].BottomModeValue = ItemIcon.BottomMode.Visible_wIconNone;
      if (!detailMenu.isLimit)
        detailMenu.gearIcons[slot].EnableLongPressEvent(true);
      else
        detailMenu.gearIcons[slot].DisableLongPressEvent();
    }
    else
    {
      e = detailMenu.gearIcons[slot].InitForEquipGear();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      detailMenu.gearIcons[slot].setEquipPlus(true);
      detailMenu.gearIcons[slot].EnableLongPressEventEmptyGear(new System.Action<int>(detailMenu.IbtnWpEquip), slot + 1);
    }
    detailMenu.gearIcons[slot].onClick = (System.Action<ItemIcon>) (_ => this.IbtnWpEquip(slot + 1));
  }

  private IEnumerator SetGearIconMulti(GameObject gearIconPrefab)
  {
    DetailMenu detailMenu = this;
    PlayerItem[] gears = new PlayerItem[detailMenu.link_MainWeaponMulti.Length];
    gears[0] = detailMenu.EquippedGearSlot1;
    gears[1] = detailMenu.EquippedGearSlot2;
    for (int i = 0; i < detailMenu.link_MainWeaponMulti.Length; ++i)
    {
      detailMenu.gearIcons[i] = gearIconPrefab.CloneAndGetComponent<ItemIcon>(detailMenu.link_MainWeaponMulti[i].transform);
      detailMenu.gearIcons[i].transform.localPosition = Vector3.zero;
      IEnumerator e;
      if (gears[i] != (PlayerItem) null)
      {
        e = detailMenu.gearIcons[i].InitByPlayerItem(gears[i]);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        if (gears[i].broken)
          detailMenu.gearIcons[i].Broken = true;
      }
      else
      {
        e = detailMenu.gearIcons[i].InitForEquipGear();
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        detailMenu.gearIcons[i].setEquipPlus(true);
        detailMenu.gearIcons[i].EnableLongPressEventEmptyGear(new System.Action<int>(detailMenu.IbtnWpEquip), i + 1);
      }
      switch (i)
      {
        case 0:
          // ISSUE: reference to a compiler-generated method
          detailMenu.gearIcons[i].onClick = new System.Action<ItemIcon>(detailMenu.\u003CSetGearIconMulti\u003Eb__96_0);
          break;
        case 1:
          // ISSUE: reference to a compiler-generated method
          detailMenu.gearIcons[i].onClick = new System.Action<ItemIcon>(detailMenu.\u003CSetGearIconMulti\u003Eb__96_1);
          break;
      }
    }
  }

  private IEnumerator LoadCharacterImage(PlayerUnit playerUnit)
  {
    Future<UnityEngine.Sprite> loader = playerUnit.job_id == playerUnit.unit.job_UnitJob ? playerUnit.unit.LoadFullSprite(1f) : playerUnit.unit.LoadJobFullSprite(playerUnit.job_id);
    IEnumerator e = loader.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.DynCharacter.sprite2D = loader.Result;
    ++DetailMenu.currentLoadTimes;
    if (DetailMenu.currentLoadTimes >= 10)
    {
      Resources.UnloadUnusedAssets();
      DetailMenu.currentLoadTimes = 0;
    }
  }

  public void LimitMode()
  {
    this.link_MainWeapon.GetComponent<UIButton>().onClick = (List<EventDelegate>) null;
    this.gearIcons[0].setEquipPlus(false);
    this.gearIcons[0].Broken = false;
    this.gearIcons[0].onClick = (System.Action<ItemIcon>) (_ => {});
    this.gearIcons[0].DisableLongPressEvent();
    if ((UnityEngine.Object) this.link_MainWeapon2 != (UnityEngine.Object) null)
      this.link_MainWeapon2.GetComponent<UIButton>().onClick = (List<EventDelegate>) null;
    if ((UnityEngine.Object) this.gearIcons[1] != (UnityEngine.Object) null)
    {
      this.gearIcons[1].setEquipPlus(false);
      this.gearIcons[1].Broken = false;
      this.gearIcons[1].onClick = (System.Action<ItemIcon>) (_ => {});
      this.gearIcons[1].DisableLongPressEvent();
    }
    if (!this.isEarthMode && (UnityEngine.Object) this.dirFavorite != (UnityEngine.Object) null)
      this.dirFavorite.SetActive(false);
    if (!((UnityEngine.Object) this.btnIntimacy != (UnityEngine.Object) null))
      return;
    this.btnIntimacy.SetActive(false);
  }

  public void LimitStorageMode()
  {
    if ((UnityEngine.Object) this.dirMainWeapon != (UnityEngine.Object) null)
      this.dirMainWeapon.SetActive(false);
    if (!((UnityEngine.Object) this.dirMainWeaponMulti != (UnityEngine.Object) null))
      return;
    this.dirMainWeaponMulti.SetActive(false);
  }

  public IEnumerator setDefaultWeapon(int index, GearGear gear)
  {
    if (index < this.gearIcons.Length && !((UnityEngine.Object) this.gearIcons[index] == (UnityEngine.Object) null))
    {
      this.gearIcons[index].SetEmpty(false);
      if (gear != null)
      {
        IEnumerator e = this.gearIcons[index].InitByGear(gear, gear.GetElement(), false, false, false);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
      }
    }
  }

  private IEnumerator WaitScrollSe()
  {
    // ISSUE: reference to a compiler-generated field
    int num = this.\u003C\u003E1__state;
    DetailMenu detailMenu = this;
    if (num != 0)
      return false;
    // ISSUE: reference to a compiler-generated field
    this.\u003C\u003E1__state = -1;
    detailMenu.Invoke("EnableScrollView", 0.3f);
    return false;
  }

  private void EnableScrollView()
  {
  }

  public void onEndScene()
  {
    foreach (DetailMenuScrollViewBase informationPanel in this.informationPanels)
      informationPanel.EndScene();
  }

  public PlayerAwakeSkill ExtraSkill()
  {
    DetailMenuScrollView02 component = this.informationPanels[1].GetComponent<DetailMenuScrollView02>();
    return (UnityEngine.Object) this.informationPanels[1] != (UnityEngine.Object) null && (UnityEngine.Object) component != (UnityEngine.Object) null ? component.ExtraSkill() : (PlayerAwakeSkill) null;
  }

  public void SetInformationPanelTab(DetailMenuScrollViewParam.TabMode mode)
  {
    foreach (DetailMenuScrollViewBase informationPanel in this.informationPanels)
    {
      if (informationPanel is DetailMenuScrollViewParam)
      {
        ((DetailMenuScrollViewParam) informationPanel).changeTab(mode);
        break;
      }
    }
  }

  public UIButton[] btnJobAbilities
  {
    get
    {
      return this.btnJobAbilities_;
    }
  }

  public IEnumerator initAsyncDiffMode(
    PlayerUnit playerUnit,
    PlayerUnit prevUnit,
    int center,
    IDetailMenuContainer menuContainer)
  {
    this._playerUnit = playerUnit;
    this.gearIcons = new ItemIcon[2];
    this.equippedGears = new PlayerItem[2];
    this.equippedGears[0] = playerUnit.equippedGear;
    this.equippedGears[1] = playerUnit.equippedGear2;
    this.informationScrollView.SeEnable = false;
    UIWidget w = this.informationScrollView.GetComponent<UIWidget>();
    w.alpha = 0.0f;
    this.inactivateGameObject<UI2DSprite>(this.DynCharacter);
    this.inactivateGameObject(this.dirSpecial);
    this.inactivateGameObject(this.floatingSpecialPointDialog);
    this.inactivateGameObject(this.floatingGroupDialog);
    this.inactivateGameObject<UIButton>(this.IbtnUnitTraining);
    this.inactivateGameObject<UISprite>(this.slcCountry);
    this.inactivateGameObject<UI2DSprite>(this.slcInclusion);
    this.inactivateGameObject<UIButton>(this.btnSpeciality);
    this.clearWeaponIcons();
    yield return (object) this.setWeaponIconsWithoutTouch(menuContainer.gearIconPrefab);
    yield return (object) this.InitInformationPanelsDiffMode(playerUnit, prevUnit, center, menuContainer);
    w.alpha = 1f;
  }

  private IEnumerator InitInformationPanelsDiffMode(
    PlayerUnit playerUnit,
    PlayerUnit prevUnit,
    int center,
    IDetailMenuContainer menuContainer)
  {
    for (int i = 0; i < this.informationPanels.Length; ++i)
    {
      DetailMenuScrollViewBase informationPanel = this.informationPanels[i];
      informationPanel.isEarthMode = false;
      informationPanel.isMemory = false;
      informationPanel.setControlFlags(Control.SelfAbility);
      informationPanel.gameObject.SetActive(true);
      yield return (object) informationPanel.initAsyncDiffMode(playerUnit, prevUnit, menuContainer);
    }
    this.informationScrollView.resetCenterItem(center);
  }

  private void inactivateGameObject<T>(T co) where T : Component
  {
    if (!((UnityEngine.Object) co != (UnityEngine.Object) null))
      return;
    co.gameObject.SetActive(false);
  }

  private void inactivateGameObject(GameObject go)
  {
    if (!((UnityEngine.Object) go != (UnityEngine.Object) null))
      return;
    go.SetActive(false);
  }

  private void clearWeaponIcons()
  {
    if ((UnityEngine.Object) this.link_MainWeapon != (UnityEngine.Object) null)
      this.link_MainWeapon.transform.Clear();
    if ((UnityEngine.Object) this.link_MainWeapon2 != (UnityEngine.Object) null)
      this.link_MainWeapon2.transform.Clear();
    if (this.link_MainWeaponMulti == null)
      return;
    for (int index = 0; index < this.link_MainWeaponMulti.Length; ++index)
      this.link_MainWeaponMulti[index].transform.Clear();
  }

  private IEnumerator setWeaponIconsWithoutTouch(GameObject gearIconPrefab)
  {
    if ((UnityEngine.Object) this.dirMainWeapon != (UnityEngine.Object) null)
      this.dirMainWeapon.SetActive(true);
    if ((UnityEngine.Object) this.dirMainWeapon2 != (UnityEngine.Object) null)
      this.dirMainWeapon2.SetActive(this.AwakeUnit);
    if ((UnityEngine.Object) this.slc_MainWeapon2_NonBase != (UnityEngine.Object) null)
      this.slc_MainWeapon2_NonBase.SetActive(!this.AwakeUnit);
    yield return (object) this.setGearIconSlotWithoutTouch(gearIconPrefab, this.link_MainWeapon, 0);
    if (this.AwakeUnit)
      yield return (object) this.setGearIconSlotWithoutTouch(gearIconPrefab, this.link_MainWeapon2, 1);
  }

  private IEnumerator setGearIconSlotWithoutTouch(
    GameObject gearIconPrefab,
    GameObject linkObject,
    int slot)
  {
    ItemIcon gIcon = gearIconPrefab.CloneAndGetComponent<ItemIcon>(linkObject.transform);
    this.gearIcons[slot] = gIcon;
    gIcon.transform.localPosition = Vector3.zero;
    PlayerItem gItem = this.equippedGears[slot];
    if (gItem != (PlayerItem) null)
    {
      yield return (object) gIcon.InitByPlayerItem(gItem);
      if (gItem.broken)
        gIcon.Broken = true;
    }
    else
    {
      gIcon.SetModeGear();
      gIcon.SetEmpty(true);
    }
    gIcon.BottomModeValue = ItemIcon.BottomMode.Visible_wIconNone;
    gIcon.onClick = (System.Action<ItemIcon>) null;
    gIcon.gear.button.onClick.Clear();
  }
}
