﻿// Decompiled with JetBrains decompiler
// Type: Guide01142Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using System.Collections;
using UnityEngine;

public class Guide01142Scene : NGSceneBase
{
  public Guide01142Menu menu;

  public static void changeScene(bool stack, ItemInfo itemInfo)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("guide011_4_2", (stack ? 1 : 0) != 0, (object) itemInfo.gear, (object) itemInfo.quantity, (object) 0);
  }

  public IEnumerator onStartSceneAsync(GearGear gear, bool isDispNumber, int index = 0)
  {
    IEnumerator e = this.menu.onStartSceneAsync(gear, isDispNumber, index);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public IEnumerator onStartSceneAsync(GearGear[] gears, bool isDispNumber, int index = 0)
  {
    IEnumerator e = this.menu.onStartSceneAsync(gears, isDispNumber, index);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public IEnumerator onStartSceneAsync(GearGear gear, int quantity, int index)
  {
    Guide01142Scene guide01142Scene = this;
    Future<GameObject> bgF = Res.Prefabs.BackGround.DefaultBackground_storage.Load<GameObject>();
    IEnumerator e = bgF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    guide01142Scene.backgroundPrefab = bgF.Result;
    e = guide01142Scene.menu.onStartSceneAsync(gear, quantity, index);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public override IEnumerator onEndSceneAsync()
  {
    this.menu.EndScene();
    yield return (object) null;
  }
}
