﻿// Decompiled with JetBrains decompiler
// Type: Popup0702Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using Earth;
using System.Collections;
using UnityEngine;

public class Popup0702Menu : BackButtonMonoBehaiviour
{
  private bool pushEnable = true;
  private System.Action<bool> mCloseCallback;

  public void Init(System.Action<bool> closeCallBack)
  {
    this.mCloseCallback = closeCallBack;
    this.pushEnable = false;
  }

  public IEnumerator pushOnWait()
  {
    yield return (object) new WaitForSeconds(0.2f);
    this.pushEnable = true;
  }

  private void OnEnable()
  {
    this.StartCoroutine(this.pushOnWait());
  }

  public void onYes()
  {
    if (!this.pushEnable)
      return;
    this.pushEnable = false;
    Singleton<EarthDataManager>.GetInstance().EarthDataRevert();
    this.mCloseCallback(true);
    Singleton<PopupManager>.GetInstance().closeAll(false);
  }

  public void onNo()
  {
    if (!this.pushEnable)
      return;
    this.pushEnable = false;
    Singleton<PopupManager>.GetInstance().dismiss(false);
  }

  public override void onBackButton()
  {
    this.onNo();
  }
}
