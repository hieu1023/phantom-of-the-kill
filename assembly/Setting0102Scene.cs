﻿// Decompiled with JetBrains decompiler
// Type: Setting0102Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

public class Setting0102Scene : NGSceneBase
{
  [SerializeField]
  private Setting0102Menu menu;

  public override IEnumerator onInitSceneAsync()
  {
    IEnumerator e = this.menu.onInitSceneAsync();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public override void onEndScene()
  {
    this.menu.onEndScene();
  }
}
