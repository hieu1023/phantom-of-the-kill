﻿// Decompiled with JetBrains decompiler
// Type: Quest0022MissionStar
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections.Generic;
using UnityEngine;

public class Quest0022MissionStar : MonoBehaviour
{
  public GameObject[] notClearStar;
  public GameObject[] clearStar;

  public void initStar()
  {
    ((IEnumerable<GameObject>) this.notClearStar).ForEach<GameObject>((System.Action<GameObject>) (x => x.SetActive(true)));
    ((IEnumerable<GameObject>) this.clearStar).ForEach<GameObject>((System.Action<GameObject>) (x => x.SetActive(false)));
  }

  public void setStar(List<bool> clearList)
  {
    this.initStar();
    for (int index = 0; index < this.clearStar.Length; ++index)
    {
      this.clearStar[index].SetActive(clearList[index]);
      this.notClearStar[index].SetActive(!clearList[index]);
    }
  }
}
