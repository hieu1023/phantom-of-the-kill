﻿// Decompiled with JetBrains decompiler
// Type: BackButtonPopupBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public abstract class BackButtonPopupBase : BackButtonMenuBase
{
  [SerializeField]
  [Tooltip("先頭位置を示す(false=\"Update()\"を実行しない)")]
  protected bool isTopObject = true;

  public void setTopObject(GameObject obj)
  {
    this.topObject = obj;
  }

  public GameObject topObject { get; private set; }

  protected override void Update()
  {
  }
}
