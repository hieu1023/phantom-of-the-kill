﻿// Decompiled with JetBrains decompiler
// Type: Guild02871Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Guild02871Menu : BackButtonMenuBase
{
  [SerializeField]
  private UILabel sceneTitle;
  [SerializeField]
  private NGxScrollMasonry Scroll;

  public IEnumerator InitializeAsync()
  {
    this.sceneTitle.SetTextLocalize(Consts.GetInstance().GUILD_BANK_HOWTO_TITLE);
    GuildBankHowto guildBankHowto = ((IEnumerable<GuildBankHowto>) MasterData.GuildBankHowtoList).FirstOrDefault<GuildBankHowto>((Func<GuildBankHowto, bool>) (x => x.kind == 1));
    GuildBankHowto[] array = ((IEnumerable<GuildBankHowto>) MasterData.GuildBankHowtoList).Where<GuildBankHowto>((Func<GuildBankHowto, bool>) (x => x.kind >= 2)).ToArray<GuildBankHowto>();
    IEnumerator e = DetailController.Init(this.Scroll, guildBankHowto == null ? string.Empty : guildBankHowto.body, array);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public void Initialize()
  {
    Singleton<CommonRoot>.GetInstance().isLoading = false;
  }

  public override void onBackButton()
  {
    if (this.IsPushAndSet())
      return;
    this.backScene();
  }
}
