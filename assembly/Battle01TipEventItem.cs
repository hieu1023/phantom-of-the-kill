﻿// Decompiled with JetBrains decompiler
// Type: Battle01TipEventItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Battle01TipEventItem : Battle01TipEventBase
{
  private ItemIcon icon;

  public override IEnumerator onInitAsync()
  {
    Battle01TipEventItem battle01TipEventItem = this;
    Future<GameObject> f = !battle01TipEventItem.battleManager.isSea ? Res.Prefabs.ItemIcon.prefab.Load<GameObject>() : Res.Prefabs.Sea.ItemIcon.prefab_sea.Load<GameObject>();
    IEnumerator e = f.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    battle01TipEventItem.icon = battle01TipEventItem.cloneIcon<ItemIcon>(f.Result, 0);
    battle01TipEventItem.icon.QuantitySupply = false;
    battle01TipEventItem.selectIcon(0);
  }

  private IEnumerator doSetIcon(SupplySupply supply)
  {
    IEnumerator e = this.icon.InitBySupply(supply);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  private IEnumerator doSetIcon(GearGear gear)
  {
    IEnumerator e = this.icon.InitByGear(gear, gear.GetElement(), false, false, false);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public override void setData(BL.DropData e, BL.Unit unit)
  {
    if (e.reward.Type != MasterDataTable.CommonRewardType.gear && e.reward.Type != MasterDataTable.CommonRewardType.supply)
      return;
    Dictionary<string, string> dictionary = new Dictionary<string, string>();
    dictionary["item"] = "";
    switch (e.reward.Type)
    {
      case MasterDataTable.CommonRewardType.supply:
        if (MasterData.SupplySupply.ContainsKey(e.reward.Id))
        {
          SupplySupply supply = MasterData.SupplySupply[e.reward.Id];
          dictionary["item"] = supply.name;
          Singleton<NGBattleManager>.GetInstance().StartCoroutine(this.doSetIcon(supply));
          break;
        }
        break;
      case MasterDataTable.CommonRewardType.gear:
        if (MasterData.GearGear.ContainsKey(e.reward.Id))
        {
          GearGear gear = MasterData.GearGear[e.reward.Id];
          dictionary["item"] = gear.name;
          Singleton<NGBattleManager>.GetInstance().StartCoroutine(this.doSetIcon(gear));
          break;
        }
        break;
    }
    this.setText(Consts.Format(Consts.GetInstance().TipEvent_text_item, (IDictionary) dictionary));
  }
}
