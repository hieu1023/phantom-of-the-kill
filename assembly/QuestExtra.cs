﻿// Decompiled with JetBrains decompiler
// Type: QuestExtra
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using SM;
using System;
using System.Collections.Generic;
using System.Linq;

public class QuestExtra
{
  public static QuestExtra.SeekType toSeekType(string str_seek)
  {
    if (str_seek == "m" || str_seek == "M")
      return QuestExtra.SeekType.M;
    return str_seek == "l" || str_seek == "L" ? QuestExtra.SeekType.L : QuestExtra.SeekType.None;
  }

  public static QuestExtra.SeekType toSeekType(PlayerExtraQuestS.SeekType seek_type)
  {
    if (seek_type == PlayerExtraQuestS.SeekType.M)
      return QuestExtra.SeekType.M;
    return seek_type == PlayerExtraQuestS.SeekType.L ? QuestExtra.SeekType.L : QuestExtra.SeekType.None;
  }

  public static QuestExtra.SeekType toSeekType(QuestExtraS.SeekType seek_type)
  {
    if (seek_type == QuestExtraS.SeekType.M)
      return QuestExtra.SeekType.M;
    return seek_type == QuestExtraS.SeekType.L ? QuestExtra.SeekType.L : QuestExtra.SeekType.None;
  }

  public static void getStatusLL(
    QuestExtraLL target,
    PlayerExtraQuestS[] quests,
    HashSet<int> emphasis,
    out bool isNew,
    out bool isAllCleared,
    out bool isEmphasis)
  {
    isNew = true;
    isEmphasis = false;
    int num = 0;
    foreach (PlayerExtraQuestS playerExtraQuestS in ((IEnumerable<PlayerExtraQuestS>) quests).Where<PlayerExtraQuestS>((Func<PlayerExtraQuestS, bool>) (x => x.quest_ll == target)))
    {
      if (playerExtraQuestS.is_clear)
        ++num;
      if (!playerExtraQuestS.is_new)
        isNew = false;
      if (!isEmphasis && emphasis.Contains(playerExtraQuestS._quest_extra_s))
        isEmphasis = true;
    }
    isAllCleared = num == MasterData.QuestExtraSList.LL(target.ID).Length;
  }

  public static void getStatusL(
    int LId,
    PlayerExtraQuestS[] quests,
    HashSet<int> emphasis,
    out bool isNew,
    out bool isAllCleared,
    out bool isEmphasis)
  {
    isNew = true;
    isEmphasis = false;
    int num = 0;
    foreach (PlayerExtraQuestS playerExtraQuestS in ((IEnumerable<PlayerExtraQuestS>) quests).Where<PlayerExtraQuestS>((Func<PlayerExtraQuestS, bool>) (x => x.quest_extra_s.quest_l_QuestExtraL == LId)))
    {
      if (playerExtraQuestS.is_clear)
        ++num;
      if (!playerExtraQuestS.is_new)
        isNew = false;
      if (!isEmphasis && emphasis.Contains(playerExtraQuestS._quest_extra_s))
        isEmphasis = true;
    }
    isAllCleared = num == ((IEnumerable<QuestExtraS>) MasterData.QuestExtraSList).Where<QuestExtraS>((Func<QuestExtraS, bool>) (x => x.quest_l_QuestExtraL == LId)).Count<QuestExtraS>();
  }

  public static void getStatusM(
    int MId,
    PlayerExtraQuestS[] quests,
    HashSet<int> emphasis,
    out bool isNew,
    out bool isAllCleared,
    out bool isEmphasis)
  {
    isNew = true;
    isEmphasis = false;
    int num = 0;
    foreach (PlayerExtraQuestS playerExtraQuestS in ((IEnumerable<PlayerExtraQuestS>) quests).Where<PlayerExtraQuestS>((Func<PlayerExtraQuestS, bool>) (x => x.quest_extra_s.quest_m_QuestExtraM == MId)))
    {
      if (playerExtraQuestS.is_clear)
        ++num;
      if (!playerExtraQuestS.is_new)
        isNew = false;
      if (!isEmphasis && emphasis.Contains(playerExtraQuestS._quest_extra_s))
        isEmphasis = true;
    }
    isAllCleared = num == ((IEnumerable<QuestExtraS>) MasterData.QuestExtraSList).Where<QuestExtraS>((Func<QuestExtraS, bool>) (x => x.quest_m_QuestExtraM == MId)).Count<QuestExtraS>();
  }

  public enum SeekType
  {
    None,
    M,
    L,
    LL,
  }
}
