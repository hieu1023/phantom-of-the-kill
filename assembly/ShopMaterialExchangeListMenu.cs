﻿// Decompiled with JetBrains decompiler
// Type: ShopMaterialExchangeListMenu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class ShopMaterialExchangeListMenu : BackButtonMenuBase
{
  private List<ShopMaterialExchangeItem> selectlist = new List<ShopMaterialExchangeItem>();
  private List<SelectTicketSelectSample> materialExchange_ = new List<SelectTicketSelectSample>();
  private bool isInitialized_;
  private const int QUANTITY_DISPLAY_MAX = 999;
  [SerializeField]
  private UILabel txtTitle_;
  [SerializeField]
  private UILabel txtExpirationDate_;
  [SerializeField]
  private UILabel txtQuantity_;
  [SerializeField]
  private UILabel txtQuantityDesabled_;
  [SerializeField]
  private UILabel txtCost_;
  [SerializeField]
  private NGxScroll scroll_;
  private PlayerSelectTicketSummary playerMaterialTicket_;
  private SM.SelectTicket materialTicket_;
  private SelectTicketSelectSample currentMaterialExchange__;
  private GameObject prefabMaterial_;
  private GameObject prefabExchange_;
  private GameObject prefabMaterialPack;
  private GameObject prefab0078;
  private int quantity;
  private GameObject detailPopup;
  private GameObject iconObj;
  public int quantity_;
  private int _playerItemQuantity;
  private int _playerUnitQuantity;
  private PlayerQuestKey key;
  private PlayerGachaTicket gTicket;
  private PlayerSeasonTicket sTicket;
  private SM.SelectTicket uTicket;
  private PlayerSelectTicketSummary playerUnitTicket;
  private PlayerUnitTypeTicket uTypeTicket;

  public GameObject prefabIconMaterial { get; private set; }

  public GameObject unitIconPrefab { get; private set; }

  public GameObject uniqueIconPrefab { get; private set; }

  public IEnumerator coInitialize(
    ShopMaterialExchangeListScene scene,
    SM.SelectTicket materialTicket,
    PlayerSelectTicketSummary playerMaterialTicket)
  {
    this.playerMaterialTicket_ = playerMaterialTicket;
    this.materialTicket_ = materialTicket;
    this.txtTitle_.SetTextLocalize(string.Format(Consts.GetInstance().SHOP_00723_TITLE_NAME, (object) this.materialTicket_.name));
    this.txtExpirationDate_.SetTextLocalize(string.Format(Consts.GetInstance().SHOP_00723_EXPIRATION_DATE, (object) this.materialTicket_.end_at));
    this.quantity_ = this.playerMaterialTicket_.quantity;
    this.txtCost_.SetTextLocalize(this.materialTicket_.cost);
    this.materialExchange_.Clear();
    foreach (KeyValuePair<int, SelectTicketSelectSample> keyValuePair in MasterData.SelectTicketSelectSample)
    {
      if (keyValuePair.Value.ticketID == this.materialTicket_.id && (keyValuePair.Value.entity_type == MasterDataTable.CommonRewardType.deck || !keyValuePair.Value.deckID.HasValue))
        this.materialExchange_.Add(keyValuePair.Value);
    }
    bool iswait;
    Future<GameObject> ldPrefab;
    IEnumerator e;
    if ((UnityEngine.Object) this.prefabMaterial_ == (UnityEngine.Object) null)
    {
      iswait = true;
      ldPrefab = Res.Prefabs.ShopMaterialExchangeListMenu.dir_material_exchange_list.Load<GameObject>();
      e = ldPrefab.Wait();
      while (e.MoveNext())
      {
        iswait = false;
        yield return e.Current;
      }
      e = (IEnumerator) null;
      this.prefabMaterial_ = ldPrefab.Result;
      if ((UnityEngine.Object) this.prefabMaterial_ == (UnityEngine.Object) null)
      {
        yield break;
      }
      else
      {
        if (iswait)
          yield return (object) null;
        ldPrefab = (Future<GameObject>) null;
      }
    }
    if ((UnityEngine.Object) this.prefabIconMaterial == (UnityEngine.Object) null)
    {
      iswait = true;
      ldPrefab = Res.Prefabs.ItemIcon.prefab.Load<GameObject>();
      e = ldPrefab.Wait();
      while (e.MoveNext())
      {
        iswait = false;
        yield return e.Current;
      }
      e = (IEnumerator) null;
      this.prefabIconMaterial = ldPrefab.Result;
      if ((UnityEngine.Object) this.prefabIconMaterial == (UnityEngine.Object) null)
      {
        yield break;
      }
      else
      {
        if (iswait)
          yield return (object) null;
        ldPrefab = (Future<GameObject>) null;
      }
    }
    if ((UnityEngine.Object) this.unitIconPrefab == (UnityEngine.Object) null)
    {
      iswait = true;
      ldPrefab = Res.Prefabs.UnitIcon.normal.Load<GameObject>();
      e = ldPrefab.Wait();
      while (e.MoveNext())
      {
        iswait = false;
        yield return e.Current;
      }
      e = (IEnumerator) null;
      this.unitIconPrefab = ldPrefab.Result;
      if ((UnityEngine.Object) this.unitIconPrefab == (UnityEngine.Object) null)
      {
        yield break;
      }
      else
      {
        if (iswait)
          yield return (object) null;
        ldPrefab = (Future<GameObject>) null;
      }
    }
    if ((UnityEngine.Object) this.uniqueIconPrefab == (UnityEngine.Object) null)
    {
      iswait = true;
      ldPrefab = Res.Icons.UniqueIconPrefab.Load<GameObject>();
      e = ldPrefab.Wait();
      while (e.MoveNext())
      {
        iswait = false;
        yield return e.Current;
      }
      e = (IEnumerator) null;
      this.uniqueIconPrefab = ldPrefab.Result;
      if ((UnityEngine.Object) this.uniqueIconPrefab == (UnityEngine.Object) null)
      {
        yield break;
      }
      else
      {
        if (iswait)
          yield return (object) null;
        ldPrefab = (Future<GameObject>) null;
      }
    }
    if ((UnityEngine.Object) this.prefabExchange_ == (UnityEngine.Object) null)
    {
      iswait = true;
      ldPrefab = Res.Prefabs.popup.popup_material_exchange_confirmation__anim_popup01.Load<GameObject>();
      e = ldPrefab.Wait();
      while (e.MoveNext())
      {
        iswait = false;
        yield return e.Current;
      }
      e = (IEnumerator) null;
      this.prefabExchange_ = ldPrefab.Result;
      if ((UnityEngine.Object) this.prefabExchange_ == (UnityEngine.Object) null)
      {
        yield break;
      }
      else
      {
        if (iswait)
          yield return (object) null;
        ldPrefab = (Future<GameObject>) null;
      }
    }
    if ((UnityEngine.Object) this.prefabMaterialPack == (UnityEngine.Object) null)
    {
      iswait = true;
      ldPrefab = Res.Prefabs.popup.popup_material_exchange_pack_menu__anim_popup01.Load<GameObject>();
      e = ldPrefab.Wait();
      while (e.MoveNext())
      {
        iswait = false;
        yield return e.Current;
      }
      e = (IEnumerator) null;
      this.prefabMaterialPack = ldPrefab.Result;
      if ((UnityEngine.Object) this.prefabMaterialPack == (UnityEngine.Object) null)
      {
        yield break;
      }
      else
      {
        if (iswait)
          yield return (object) null;
        ldPrefab = (Future<GameObject>) null;
      }
    }
    if ((UnityEngine.Object) this.detailPopup == (UnityEngine.Object) null)
    {
      ldPrefab = Res.Prefabs.popup.popup_000_7_4_2__anim_popup01.Load<GameObject>();
      e = ldPrefab.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.detailPopup = ldPrefab.Result;
      ldPrefab = (Future<GameObject>) null;
    }
    e = this.coInitializeMaterialSelect();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.updateTicketQuantity(false);
    this.isInitialized_ = true;
  }

  private IEnumerator coInitializeMaterialSelect()
  {
    ShopMaterialExchangeListMenu menu = this;
    if (menu.materialExchange_ == null || menu.materialExchange_.Count == 0)
    {
      Debug.LogError((object) "materialExchange is null or empty");
    }
    else
    {
      menu.scroll_.Clear();
      foreach (SelectTicketSelectSample sample in menu.materialExchange_)
      {
        ShopMaterialExchangeItem component = menu.prefabMaterial_.Clone((Transform) null).GetComponent<ShopMaterialExchangeItem>();
        menu.scroll_.Add(component.gameObject, true);
        menu.selectlist.Add(component);
        IEnumerator e = component.coInitialize(menu, sample, menu.playerMaterialTicket_, menu.materialTicket_);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
      }
      menu.scroll_.ResolvePosition();
    }
  }

  private IEnumerator updateMaterialNum()
  {
    if (this.materialExchange_ == null || this.materialExchange_.Count == 0)
    {
      Debug.LogError((object) "materialExchange is null or empty");
    }
    else
    {
      for (int index = 0; index < this.selectlist.Count; ++index)
      {
        if (this.selectlist[index].sample_.ID == this.currentMaterialExchange__.ID)
          this.selectlist[index].UpdateNums(this.currentMaterialExchange__.reward_value);
      }
      yield break;
    }
  }

  private void updateTicketQuantity(bool usedTicket = false)
  {
    if (usedTicket)
      this.quantity_ -= this.materialTicket_.cost;
    int num1 = this.quantity_ >= this.materialTicket_.cost ? 1 : 0;
    int num2 = Mathf.Clamp(this.quantity_, 0, 999);
    if (num1 != 0)
    {
      this.txtQuantity_.SetTextLocalize(num2);
      this.txtQuantity_.gameObject.SetActive(true);
      this.txtQuantityDesabled_.gameObject.SetActive(false);
    }
    else
    {
      this.txtQuantity_.gameObject.SetActive(false);
      this.txtQuantityDesabled_.SetTextLocalize(num2);
      this.txtQuantityDesabled_.gameObject.SetActive(true);
    }
  }

  public void onClickSelect(SelectTicketSelectSample material)
  {
    if (!this.isInitialized_ || this.IsPushAndSet())
      return;
    this.currentMaterialExchange__ = material;
    this.StartCoroutine(this.coPopupSelect());
    this.IsPush = false;
  }

  public IEnumerator SetIcon(SelectTicketSelectSample sample_, Transform tran)
  {
    this._playerItemQuantity = 0;
    this._playerItemQuantity += SMManager.Get<PlayerItem[]>().AmountHavingTargetItem(sample_.reward_id, sample_.entity_type);
    this._playerItemQuantity += SMManager.Get<PlayerMaterialGear[]>().AmountHavingTargetItem(sample_.reward_id);
    this._playerUnitQuantity = 0;
    if (MasterData.UnitUnit.ContainsKey(sample_.reward_id))
    {
      if (MasterData.UnitUnit[sample_.reward_id].IsMaterialUnit)
      {
        PlayerMaterialUnit playerMaterialUnit = ((IEnumerable<PlayerMaterialUnit>) SMManager.Get<PlayerMaterialUnit[]>()).FirstOrDefault<PlayerMaterialUnit>((Func<PlayerMaterialUnit, bool>) (x => x._unit == sample_.reward_id));
        if (playerMaterialUnit != null)
          this._playerUnitQuantity = playerMaterialUnit.quantity;
      }
      else
        this._playerUnitQuantity = SMManager.Get<PlayerUnit[]>().AmountHavingTargetUnit(sample_.reward_id, sample_.entity_type);
    }
    this.key = sample_.entity_type == MasterDataTable.CommonRewardType.quest_key ? ((IEnumerable<PlayerQuestKey>) SMManager.Get<PlayerQuestKey[]>()).Where<PlayerQuestKey>((Func<PlayerQuestKey, bool>) (x => x.quest_key_id == sample_.reward_id)).FirstOrDefault<PlayerQuestKey>() : (PlayerQuestKey) null;
    this.quantity = 0;
    IEnumerator e;
    ItemIcon material;
    switch (sample_.entity_type)
    {
      case MasterDataTable.CommonRewardType.unit:
      case MasterDataTable.CommonRewardType.material_unit:
        UnitIcon icon = this.unitIconPrefab.Clone(tran).GetComponent<UnitIcon>();
        UnitUnit unit = (UnitUnit) null;
        this.quantity = this._playerUnitQuantity;
        if (MasterData.UnitUnit.TryGetValue(sample_.reward_id, out unit))
        {
          e = icon.SetUnit(unit, unit.GetElement(), false);
          while (e.MoveNext())
            yield return e.Current;
          e = (IEnumerator) null;
        }
        else
          icon.GetComponent<UnitIcon>().SetEmpty();
        icon.gameObject.SetActive(true);
        icon.onClick = (System.Action<UnitIconBase>) null;
        icon.onClick = (System.Action<UnitIconBase>) (x => this.ShowDetail(sample_.entity_type, sample_.reward_id));
        break;
      case MasterDataTable.CommonRewardType.supply:
        material = this.prefabIconMaterial.Clone(tran).GetComponent<ItemIcon>();
        this.quantity = this._playerItemQuantity;
        e = material.InitByMaterialExchange(sample_.entity_type, sample_.reward_id);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        material.gameObject.SetActive(true);
        material.onClick = (System.Action<ItemIcon>) null;
        material.onClick = (System.Action<ItemIcon>) (x => this.ShowDetail(sample_.entity_type, sample_.reward_id));
        break;
      case MasterDataTable.CommonRewardType.gear:
      case MasterDataTable.CommonRewardType.material_gear:
      case MasterDataTable.CommonRewardType.gear_body:
        material = this.prefabIconMaterial.Clone(tran).GetComponent<ItemIcon>();
        this.quantity = this._playerItemQuantity;
        e = material.InitByMaterialExchange(sample_.entity_type, sample_.reward_id);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        material.gameObject.SetActive(true);
        material.onClick = (System.Action<ItemIcon>) null;
        material.onClick = (System.Action<ItemIcon>) (x => this.ShowDetail(sample_.entity_type, sample_.reward_id));
        break;
      default:
        UniqueIcons component = this.uniqueIconPrefab.Clone(tran).GetComponent<UniqueIcons>();
        component.LabelActivated = false;
        component.gameObject.SetActive(true);
        this.quantity = 0;
        component.CreateButton();
        component.onClick = (System.Action<UniqueIcons>) null;
        component.onClick = (System.Action<UniqueIcons>) (x => this.ShowDetail(sample_.entity_type, sample_.reward_id));
        switch (sample_.entity_type)
        {
          case MasterDataTable.CommonRewardType.money:
            e = component.SetZeny(0);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            break;
          case MasterDataTable.CommonRewardType.player_exp:
            e = component.SetPlayerExp(0);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            break;
          case MasterDataTable.CommonRewardType.unit_exp:
            e = component.SetUnitExp(0);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            break;
          case MasterDataTable.CommonRewardType.coin:
            e = component.SetKiseki(0);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            break;
          case MasterDataTable.CommonRewardType.recover:
            e = component.SetApRecover(0);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            break;
          case MasterDataTable.CommonRewardType.max_unit:
            e = component.SetMaxUnit(0);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            break;
          case MasterDataTable.CommonRewardType.max_item:
            e = component.SetMaxItem(0);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            break;
          case MasterDataTable.CommonRewardType.medal:
            e = component.SetMedal(0);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            break;
          case MasterDataTable.CommonRewardType.friend_point:
            e = component.SetPoint(0);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            break;
          case MasterDataTable.CommonRewardType.emblem:
            e = component.SetEmblem();
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            break;
          case MasterDataTable.CommonRewardType.battle_medal:
            e = component.SetBattleMedal(0);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            break;
          case MasterDataTable.CommonRewardType.cp_recover:
            e = component.SetCpRecover(0);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            break;
          case MasterDataTable.CommonRewardType.quest_key:
            this.quantity = this.key == null ? 0 : this.key.quantity;
            e = component.SetKey(sample_.reward_id, 0);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            break;
          case MasterDataTable.CommonRewardType.gacha_ticket:
            this.gTicket = ((IEnumerable<PlayerGachaTicket>) SMManager.Get<Player>().gacha_tickets).FirstOrDefault<PlayerGachaTicket>((Func<PlayerGachaTicket, bool>) (x => x.ticket.ID == sample_.reward_id));
            this.quantity = this.gTicket == null ? 0 : this.gTicket.quantity;
            e = component.SetGachaTicket(0, sample_.reward_id);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            break;
          case MasterDataTable.CommonRewardType.season_ticket:
            this.sTicket = ((IEnumerable<PlayerSeasonTicket>) SMManager.Get<PlayerSeasonTicket[]>()).FirstOrDefault<PlayerSeasonTicket>((Func<PlayerSeasonTicket, bool>) (x => x.season_ticket_id == sample_.reward_id));
            this.quantity = this.sTicket == null ? 0 : this.sTicket.quantity;
            e = component.SetSeasonTicket(0, sample_.reward_id);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            break;
          case MasterDataTable.CommonRewardType.mp_recover:
            e = component.SetMpRecover(0);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            break;
          case MasterDataTable.CommonRewardType.unit_ticket:
            this.playerUnitTicket = ((IEnumerable<PlayerSelectTicketSummary>) SMManager.Get<PlayerSelectTicketSummary[]>()).FirstOrDefault<PlayerSelectTicketSummary>((Func<PlayerSelectTicketSummary, bool>) (x => x.ticket_id == sample_.reward_id));
            this.uTicket = ((IEnumerable<SM.SelectTicket>) SMManager.Get<SM.SelectTicket[]>()).FirstOrDefault<SM.SelectTicket>((Func<SM.SelectTicket, bool>) (x => x.id == sample_.reward_id));
            this.quantity = this.uTicket == null ? 0 : this.playerUnitTicket.quantity;
            e = component.SetKillersTicket(sample_.reward_id, 0);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            break;
          case MasterDataTable.CommonRewardType.stamp:
            e = component.SetStamp(0);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            break;
          case MasterDataTable.CommonRewardType.awake_skill:
            this.quantity = ((IEnumerable<PlayerAwakeSkill>) SMManager.Get<PlayerAwakeSkill[]>()).Count<PlayerAwakeSkill>((Func<PlayerAwakeSkill, bool>) (x => x.skill_id == sample_.reward_id));
            e = component.SetAwakeSkill(sample_.reward_id);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            break;
          case MasterDataTable.CommonRewardType.guild_town:
            e = component.SetGuildMap(sample_.reward_id);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            break;
          case MasterDataTable.CommonRewardType.guild_facility:
            int id = 0;
            PlayerGuildFacility playerGuildFacility = ((IEnumerable<PlayerGuildFacility>) SMManager.Get<PlayerGuildFacility[]>()).FirstOrDefault<PlayerGuildFacility>((Func<PlayerGuildFacility, bool>) (x => x.master.ID == sample_.reward_id));
            if (playerGuildFacility != null)
            {
              this.quantity = playerGuildFacility.hasnum;
              id = playerGuildFacility.unit.ID;
            }
            else
            {
              this.quantity = 0;
              FacilityLevel facilityLevel = ((IEnumerable<FacilityLevel>) MasterData.FacilityLevelList).FirstOrDefault<FacilityLevel>((Func<FacilityLevel, bool>) (x => x.level == 1 && x.facility_MapFacility == sample_.reward_id));
              if (facilityLevel != null)
                id = facilityLevel.unit.ID;
            }
            if (id != 0)
            {
              e = component.SetGuildFacility(id);
              while (e.MoveNext())
                yield return e.Current;
              e = (IEnumerator) null;
              break;
            }
            break;
          case MasterDataTable.CommonRewardType.reincarnation_type_ticket:
            Debug.LogError((object) sample_.reward_id);
            this.uTypeTicket = ((IEnumerable<PlayerUnitTypeTicket>) SMManager.Get<PlayerUnitTypeTicket[]>()).FirstOrDefault<PlayerUnitTypeTicket>((Func<PlayerUnitTypeTicket, bool>) (x => x.ticket_id == sample_.reward_id));
            this.quantity = this.uTypeTicket == null ? 0 : this.uTypeTicket.quantity;
            e = component.SetReincarnationTypeTicket(sample_.reward_id, 0);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            break;
          case MasterDataTable.CommonRewardType.deck:
            e = component.SetMaterialPack(sample_.ticketID, 0);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            break;
        }
        break;
    }
    yield return (object) this.quantity;
  }

  private IEnumerator ShowDetailPopup(MasterDataTable.CommonRewardType rType, int rID)
  {
    GameObject popup = Singleton<PopupManager>.GetInstance().open(this.detailPopup, false, false, false, true, false, false, "SE_1006");
    popup.SetActive(false);
    IEnumerator e = popup.GetComponent<Shop00742Menu>().Init(rType, rID);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    popup.SetActive(true);
  }

  public void ShowDetail(MasterDataTable.CommonRewardType rType, int rID)
  {
    if (!Shop00742Menu.IsEnableShowPopup(rType))
      return;
    this.StartCoroutine(this.ShowDetailPopup(rType, rID));
  }

  private IEnumerator coPopupSelect()
  {
    ShopMaterialExchangeListMenu menu = this;
    GameObject prefab = menu.prefabExchange_.Clone((Transform) null);
    Singleton<PopupManager>.GetInstance().open(prefab, false, false, true, true, false, false, "SE_1006");
    IEnumerator e = prefab.GetComponent<MaterialPopupExchangeMenu>().coInitialize(menu, menu.currentMaterialExchange__, menu.playerMaterialTicket_, menu.materialTicket_);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public void InitObj(GameObject icon)
  {
    this.iconObj = UnityEngine.Object.Instantiate<GameObject>(icon);
    this.iconObj.transform.position = (Vector3) new Vector2(999f, 999f);
    this.iconObj.SetActive(false);
  }

  public void doExchangeMaterial(int nums)
  {
    this.StartCoroutine(this.coExchangeMaterial(nums, this.iconObj));
  }

  private IEnumerator coExchangeMaterial(int nums, GameObject icon)
  {
    ShopMaterialExchangeListMenu exchangeListMenu = this;
    Singleton<CommonRoot>.GetInstance().loadingMode = 1;
    Future<WebAPI.Response.SelectticketSpend> future = WebAPI.SelectticketSpend(exchangeListMenu.currentMaterialExchange__.ID, 0, (System.Action<WebAPI.Response.UserError>) (e =>
    {
      Singleton<PopupManager>.GetInstance().closeAll(false);
      Singleton<CommonRoot>.GetInstance().loadingMode = 0;
      WebAPI.DefaultUserErrorCallback(e);
      Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
    }));
    IEnumerator e1 = future.Wait();
    while (e1.MoveNext())
      yield return e1.Current;
    e1 = (IEnumerator) null;
    if (future.Result != null)
    {
      exchangeListMenu.updateTicketQuantity(true);
      exchangeListMenu.UpdateTicketLimit();
      if (exchangeListMenu.currentMaterialExchange__.entity_type == MasterDataTable.CommonRewardType.deck)
      {
        // ISSUE: reference to a compiler-generated method
        ModalWindow.Show(Consts.GetInstance().VERSUS_0026872POPUP_TITLE2, string.Format(Consts.GetInstance().VERSUS_0026872POPUP_DESCRIPTION3, (object) exchangeListMenu.currentMaterialExchange__.reward_title), new System.Action(exchangeListMenu.\u003CcoExchangeMaterial\u003Eb__52_1));
      }
      else
        exchangeListMenu.StartCoroutine(exchangeListMenu.OpenPopup0078(nums, icon));
    }
  }

  private IEnumerator OpenPopup0078(int nums, GameObject icon)
  {
    ShopMaterialExchangeListMenu exchangeListMenu = this;
    Future<GameObject> prefab0078F = Res.Prefabs.popup.popup_007_8__anim_popup01.Load<GameObject>();
    IEnumerator e = prefab0078F.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    exchangeListMenu.prefab0078 = prefab0078F.Result.Clone((Transform) null);
    Shop0078Menu component = exchangeListMenu.prefab0078.GetComponent<Shop0078Menu>();
    icon.SetActive(true);
    component.InitObj(icon);
    // ISSUE: reference to a compiler-generated method
    component.InitDataSet(exchangeListMenu.currentMaterialExchange__.reward_title, exchangeListMenu.currentMaterialExchange__.reward_value, nums, new System.Action(exchangeListMenu.\u003COpenPopup0078\u003Eb__53_0), true);
    Singleton<PopupManager>.GetInstance().open(exchangeListMenu.prefab0078, false, false, true, true, false, false, "SE_1006");
    if ((UnityEngine.Object) exchangeListMenu.iconObj != (UnityEngine.Object) null)
      UnityEngine.Object.DestroyImmediate((UnityEngine.Object) exchangeListMenu.iconObj);
  }

  private void UpdateTicketLimit()
  {
    foreach (ShopMaterialExchangeItem materialExchangeItem in this.selectlist)
      materialExchangeItem.UpdateInfo(this.playerMaterialTicket_, this.currentMaterialExchange__, this.quantity_, this.materialTicket_);
  }

  public void onBtnInfo(SelectTicketSelectSample sample)
  {
    if (!this.isInitialized_ || this.IsPushAndSet())
      return;
    this.StartCoroutine(this.materialPopupInit(sample));
    this.IsPush = false;
  }

  private IEnumerator materialPopupInit(SelectTicketSelectSample sample)
  {
    GameObject go = this.prefabMaterialPack.Clone((Transform) null);
    Singleton<PopupManager>.GetInstance().open(go, false, false, true, true, true, true, "SE_1006").GetComponent<UIWidget>().alpha = 0.0f;
    IEnumerator e = go.GetComponent<Shop007_MaterialExchangePackMenu>().InitMaterialIcon(sample);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Singleton<PopupManager>.GetInstance().startOpenAnime(go, false);
  }

  public override void onBackButton()
  {
    this.OnIbtnBack();
  }

  public void OnIbtnBack()
  {
    if (this.IsPushAndSet())
      return;
    if ((UnityEngine.Object) this.iconObj != (UnityEngine.Object) null)
      UnityEngine.Object.DestroyImmediate((UnityEngine.Object) this.iconObj);
    this.backScene();
    this.IsPush = false;
  }
}
