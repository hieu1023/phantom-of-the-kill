﻿// Decompiled with JetBrains decompiler
// Type: Shop00742SeasonTicket
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using System.Collections;
using UnityEngine;

public class Shop00742SeasonTicket : MonoBehaviour
{
  private float scale = 0.7f;
  [SerializeField]
  protected UILabel TxtFlavor;
  [SerializeField]
  protected UILabel TxtName;
  [SerializeField]
  protected UI2DSprite SlcTarget;

  public IEnumerator Init(int entity_id)
  {
    SeasonTicketSeasonTicket ticketSeasonTicket = MasterData.SeasonTicketSeasonTicket[entity_id];
    this.TxtFlavor.SetText(ticketSeasonTicket.description);
    this.TxtName.SetText(ticketSeasonTicket.name);
    Future<UnityEngine.Sprite> spriteF = ticketSeasonTicket.LoadLargeF();
    IEnumerator e = spriteF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.SlcTarget.sprite2D = spriteF.Result;
    this.SlcTarget.width = Mathf.FloorToInt(spriteF.Result.textureRect.width);
    this.SlcTarget.height = Mathf.FloorToInt(spriteF.Result.textureRect.height);
    this.SlcTarget.transform.localScale = new Vector3(this.scale, this.scale);
    spriteF = (Future<UnityEngine.Sprite>) null;
  }
}
