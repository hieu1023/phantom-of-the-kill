﻿// Decompiled with JetBrains decompiler
// Type: IRaidResultMenu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System.Collections;

public interface IRaidResultMenu
{
  bool isSkip { get; set; }

  IEnumerator Init(
    GuildRaid masterData,
    BattleInfo info,
    WebAPI.Response.GuildraidBattleFinish result,
    PlayerUnit bossUnit);

  IEnumerator Run();

  IEnumerator OnFinish();
}
