﻿// Decompiled with JetBrains decompiler
// Type: Raid032HuntingInfoScene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System;
using System.Collections;
using UnityEngine;

public class Raid032HuntingInfoScene : NGSceneBase
{
  private Raid032HuntingInfoMenu menu;
  private GameObject dir_raid_HuntingInfo_scrollPrefab;
  private GameObject unitNormalIconPrefab;

  public static void ChangeScene(bool stack, int period_id)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("raid032_HuntingInfo", (stack ? 1 : 0) != 0, (object) period_id);
  }

  public IEnumerator onStartSceneAsync(int period_id)
  {
    Raid032HuntingInfoScene huntingInfoScene = this;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    huntingInfoScene.menu = huntingInfoScene.menuBase as Raid032HuntingInfoMenu;
    huntingInfoScene.menu.ClearScrollView();
    Future<WebAPI.Response.GuildraidSubjugationHistory> ft = WebAPI.GuildraidSubjugationHistory(period_id, (System.Action<WebAPI.Response.UserError>) (e =>
    {
      WebAPI.DefaultUserErrorCallback(e);
      Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
    }));
    IEnumerator e1 = ft.Wait();
    while (e1.MoveNext())
      yield return e1.Current;
    e1 = (IEnumerator) null;
    if (ft.Result != null)
    {
      if ((UnityEngine.Object) huntingInfoScene.dir_raid_HuntingInfo_scrollPrefab == (UnityEngine.Object) null)
        yield return (object) huntingInfoScene.LoadHuntingInfoScrollItemPrefab();
      if ((UnityEngine.Object) huntingInfoScene.unitNormalIconPrefab == (UnityEngine.Object) null)
        yield return (object) huntingInfoScene.LoadUnitIconPrefab();
      e1 = huntingInfoScene.menu.InitAsync(huntingInfoScene.dir_raid_HuntingInfo_scrollPrefab, huntingInfoScene.unitNormalIconPrefab, ft.Result);
      while (e1.MoveNext())
        yield return e1.Current;
      e1 = (IEnumerator) null;
      Singleton<CommonRoot>.GetInstance().isLoading = false;
    }
  }

  private IEnumerator LoadHuntingInfoScrollItemPrefab()
  {
    Future<GameObject> future = Singleton<ResourceManager>.GetInstance().Load<GameObject>("Prefabs/raid032_HuntingInfo/dir_raid_HuntingInfo_scroll", 1f);
    IEnumerator e = future.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if ((UnityEngine.Object) future.Result == (UnityEngine.Object) null)
      Debug.LogError((object) "failed to load dir_raid_HuntingInfo_scroll.prefab");
    else
      this.dir_raid_HuntingInfo_scrollPrefab = future.Result;
  }

  private IEnumerator LoadUnitIconPrefab()
  {
    Future<GameObject> prefabF = Res.Prefabs.UnitIcon.normal.Load<GameObject>();
    IEnumerator e = prefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.unitNormalIconPrefab = prefabF.Result;
  }

  public void onStartScene(int period_id)
  {
    Singleton<CommonRoot>.GetInstance().isLoading = false;
  }
}
