﻿// Decompiled with JetBrains decompiler
// Type: Sea030DateMenu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Sea030DateMenu : BackButtonMenuBase
{
  private readonly string ImageSpriteNameBase = "slc_time_{0}.png__GUI__sea_home__sea_home_prefab";
  [SerializeField]
  private UISprite timeIcon;
  [SerializeField]
  private UILabel timeLabel;
  [SerializeField]
  private SeaDateSpotUIButton[] BtnSpots;
  private GameObject dateConfirmPopupPrefab;
  private Sea030HomeMenu seaHomeMenu;
  private SeaHomeManager.UnitConrtolleData current2DUnitData;

  public IEnumerator Init(
    Sea030HomeMenu _seaHomeMenu,
    SeaHomeManager.UnitConrtolleData _current2DUnitData,
    DateTime _now,
    SeaHomeTimeZone _timeZone)
  {
    this.seaHomeMenu = _seaHomeMenu;
    this.current2DUnitData = _current2DUnitData;
    IEnumerator e;
    if ((UnityEngine.Object) this.dateConfirmPopupPrefab == (UnityEngine.Object) null)
    {
      Future<GameObject> futurePrefab = Res.Prefabs.popup.popup_030_sea_mypage_date_confirm__anim_fade.Load<GameObject>();
      e = futurePrefab.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.dateConfirmPopupPrefab = futurePrefab.Result;
      futurePrefab = (Future<GameObject>) null;
    }
    e = this.setTime(_now, _timeZone);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    IEnumerable<SeaDateDateSpotDisplaySetting> spotDisplaySettings = ((IEnumerable<SeaDateDateSpotDisplaySetting>) MasterData.SeaDateDateSpotDisplaySettingList).Where<SeaDateDateSpotDisplaySetting>((Func<SeaDateDateSpotDisplaySetting, bool>) (x =>
    {
      if (x.start_at.HasValue && (!x.start_at.HasValue || !(x.start_at.Value <= _now)) || x.end_at.HasValue && (!x.end_at.HasValue || !(x.end_at.Value >= _now)))
        return false;
      if (x.time_zone == null)
        return true;
      return x.time_zone != null && x.time_zone.WithIn(_now);
    }));
    foreach (SeaDateSpotUIButton btnSpot in this.BtnSpots)
    {
      btnSpot.isEnabled = false;
      btnSpot.gameObject.SetActive(false);
    }
    foreach (SeaDateDateSpotDisplaySetting spotDisplaySetting in spotDisplaySettings)
    {
      SeaDateDateSpotDisplaySetting setting = spotDisplaySetting;
      SeaDateSpotUIButton btnSpot = this.BtnSpots[setting.datespot.ID - 1];
      btnSpot.isEnabled = true;
      btnSpot.gameObject.SetActive(true);
      btnSpot.onClick.Clear();
      EventDelegate.Add(btnSpot.onClick, (EventDelegate.Callback) (() => this.onDateSpot(setting)));
    }
  }

  public IEnumerator setTime(DateTime _now, SeaHomeTimeZone _timeZone)
  {
    this.timeLabel.SetTextLocalize(_now.ToString("HH:mm"));
    if (_timeZone != null)
    {
      this.timeIcon.spriteName = string.Format(this.ImageSpriteNameBase, (object) _timeZone.image_pattern);
      yield break;
    }
  }

  public virtual void IbtnBack()
  {
    if (this.IsPushAndSet())
      return;
    this.seaHomeMenu.isReturnSelectSpot = true;
    this.seaHomeMenu.isSelectedSpot = false;
    this.backScene();
  }

  public override void onBackButton()
  {
    this.IbtnBack();
  }

  private void onDateSpot(SeaDateDateSpotDisplaySetting setting)
  {
    if (this.IsPushAndSet())
      return;
    this.seaHomeMenu.isReturnSelectSpot = false;
    this.OpenDateConfirmPopup(setting);
  }

  private void OpenDateConfirmPopup(SeaDateDateSpotDisplaySetting setting)
  {
    Singleton<PopupManager>.GetInstance().open(this.dateConfirmPopupPrefab, false, false, false, true, false, false, "SE_1006").GetComponent<Sea030HomeDateConfirmPopup>().Init(this.current2DUnitData.PlayerUnit, setting, new System.Action<SeaDateDateSpotDisplaySetting>(this.DepartForDate));
  }

  public void DepartForDate(SeaDateDateSpotDisplaySetting setting)
  {
    Singleton<CommonRoot>.GetInstance().isTouchBlock = true;
    this.StartCoroutine(this.internalDepartForDate(setting));
  }

  private IEnumerator internalDepartForDate(SeaDateDateSpotDisplaySetting setting)
  {
    Sea030DateMenu sea030DateMenu = this;
    bool isSeaSeasonEnd = false;
    Singleton<PopupManager>.GetInstance().closeAllWithoutAnim(true);
    CommonRoot cr = Singleton<CommonRoot>.GetInstance();
    cr.isTouchBlock = true;
    while (Singleton<PopupManager>.GetInstance().isOpen)
      yield return (object) null;
    cr.loadingMode = 1;
    cr.isLoading = true;
    Future<WebAPI.Response.SeaDateStart> futureAPI = WebAPI.SeaDateStart(setting.datespot.ID, sea030DateMenu.current2DUnitData.PlayerUnit.id, (System.Action<WebAPI.Response.UserError>) (e =>
    {
      if (string.Equals(e.Code, "SEA000"))
      {
        isSeaSeasonEnd = true;
        this.StartCoroutine(PopupUtility.SeaError(e));
      }
      else
      {
        WebAPI.DefaultUserErrorCallback(e);
        Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
      }
    }));
    IEnumerator e1 = futureAPI.Wait();
    while (e1.MoveNext())
      yield return e1.Current;
    e1 = (IEnumerator) null;
    if (futureAPI.Result == null)
    {
      if (!isSeaSeasonEnd)
      {
        cr.isLoading = false;
        cr.loadingMode = 0;
        cr.isTouchBlock = false;
        sea030DateMenu.IsPush = false;
        Singleton<NGGameDataManager>.GetInstance().refreshHomeHome = true;
        cr.isLoading = true;
        e1 = new Future<NGGameDataManager.StartSceneProxyResult>(new Func<Promise<NGGameDataManager.StartSceneProxyResult>, IEnumerator>(Singleton<NGGameDataManager>.GetInstance().StartSceneAsyncProxyImpl)).Wait();
        while (e1.MoveNext())
          yield return e1.Current;
        e1 = (IEnumerator) null;
        cr.isLoading = false;
      }
    }
    else
    {
      sea030DateMenu.seaHomeMenu.ExistSelect = futureAPI.Result.happening_id > 0 || futureAPI.Result.quiz_id > 0;
      cr.isLoading = false;
      sea030DateMenu.seaHomeMenu.DateSetting = setting;
      sea030DateMenu.seaHomeMenu.DateFlows = ((IEnumerable<int>) futureAPI.Result.script_ids).ToList<int>();
      sea030DateMenu.seaHomeMenu.NowFlowIndex = 0;
      sea030DateMenu.seaHomeMenu.EventSelectData.Clear();
      sea030DateMenu.seaHomeMenu.QuizId = Sea030HomeMenu.hasQuiz(futureAPI.Result.date_flow) ? new int?(futureAPI.Result.quiz_id) : new int?();
      sea030DateMenu.seaHomeMenu.isSelectedSpot = true;
      cr.loadingMode = 3;
      cr.isLoading = true;
      cr.isTouchBlock = false;
      sea030DateMenu.IsPush = false;
      sea030DateMenu.backScene();
    }
  }
}
