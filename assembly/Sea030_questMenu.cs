﻿// Decompiled with JetBrains decompiler
// Type: Sea030_questMenu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Sea030_questMenu : BackButtonMenuBase
{
  private const int STORY_BTN_TWEEN_START = 41;
  private const int STORY_BTN_TWEEN_END = 42;
  [SerializeField]
  protected UILabel TxtTitle;
  [SerializeField]
  private List<Sea030_questMenu.StoryGroupInfo> storyList;
  [SerializeField]
  private UIScrollView scrollView;
  [SerializeField]
  private GameObject tweenObject;
  [SerializeField]
  private float tweenDuration;
  [SerializeField]
  private AnimationCurve tweenCurve;
  [SerializeField]
  private float tweenDelay;
  private GameObject unitIconPrefab;
  private TweenPosition currentTweenPos;
  private TweenScale currentTweenScale;
  private int originID;
  private int tweenFinishCount;
  private float minStartDelay;
  private GameObject StoryButtons;
  private PlayerSeaQuestS[] StoryData;
  [SerializeField]
  private GameObject dyn_leader_icon;
  private bool isInitialized;

  private IEnumerator LoadLeaderUnit()
  {
    IEnumerator e;
    if ((UnityEngine.Object) this.unitIconPrefab == (UnityEngine.Object) null)
    {
      Future<GameObject> f = Res.Prefabs.Sea.UnitIcon.normal_sea.Load<GameObject>();
      e = f.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.unitIconPrefab = f.Result;
      f = (Future<GameObject>) null;
    }
    if (SMManager.Get<PlayerSeaDeck[]>() != null)
    {
      int?[] playerUnitIDs = SMManager.Get<PlayerSeaDeck[]>()[0].player_unit_ids;
      if (playerUnitIDs != null && playerUnitIDs.Length != 0 && playerUnitIDs[0].HasValue)
      {
        PlayerUnit unit = ((IEnumerable<PlayerUnit>) SMManager.Get<PlayerUnit[]>()).Where<PlayerUnit>((Func<PlayerUnit, bool>) (x => x.id == playerUnitIDs[0].Value)).FirstOrDefault<PlayerUnit>();
        if (!(unit == (PlayerUnit) null))
        {
          foreach (Component component in this.dyn_leader_icon.transform)
            UnityEngine.Object.Destroy((UnityEngine.Object) component.gameObject);
          UnitIcon unitScript = this.unitIconPrefab.Clone(this.dyn_leader_icon.transform).GetComponent<UnitIcon>();
          e = unitScript.setSimpleUnit(unit);
          while (e.MoveNext())
            yield return e.Current;
          e = (IEnumerator) null;
          unitScript.setLevelText(unit);
          unitScript.ShowBottomInfo(UnitSortAndFilter.SORT_TYPES.Level);
          if (unit == (PlayerUnit) null)
            unitScript.SetEmpty();
          unitScript.Favorite = false;
          unitScript.Gray = false;
        }
      }
    }
  }

  private void InitStoryChoiceButton(
    GameObject btnContainer,
    int xl,
    int l,
    int index,
    bool isStart,
    PlayerSeaQuestS[] StoryDataM)
  {
    Sea030StoryButton componentInChildren = btnContainer.transform.GetComponentInChildren<Sea030StoryButton>();
    componentInChildren.Lock();
    QuestSeaM[] array = ((IEnumerable<QuestSeaM>) MasterData.QuestSeaMList).Where<QuestSeaM>((Func<QuestSeaM, bool>) (x => x.quest_xl_QuestSeaXL == xl && x.quest_l_QuestSeaL == l)).OrderBy<QuestSeaM, int>((Func<QuestSeaM, int>) (x => x.priority)).ToArray<QuestSeaM>();
    if (array != null && array.Length > index)
      componentInChildren.StoryName = array[index].name;
    if (StoryDataM.Length > index)
    {
      int bonusCategory = StoryDataM[index].bonus_category;
      componentInChildren.SetBonus(bonusCategory);
    }
    componentInChildren.initTween();
  }

  private IEnumerator StoryChoiceButtonSetting(PlayerSeaQuestS[] StoryData)
  {
    for (int i = 0; i < this.storyList.Count; i++)
    {
      if (this.storyList[i].storyData != null && this.storyList[i].storyData.Length != 0)
      {
        PlayerSeaQuestS[] storyData = this.storyList[i].storyData;
        int cnt = 0;
        this.storyList[i].storyM.ForEach<GameObject, PlayerSeaQuestS>((IEnumerable<PlayerSeaQuestS>) storyData, (System.Action<GameObject, PlayerSeaQuestS>) ((storyM, storyDataM) =>
        {
          PlayerSeaQuestS[] playerSeaQuestSArray = StoryData.S(3, i + 1, storyDataM.quest_sea_s.quest_m_QuestSeaM);
          bool clearflag = ((IEnumerable<PlayerSeaQuestS>) playerSeaQuestSArray).Count<PlayerSeaQuestS>() == ((IEnumerable<PlayerSeaQuestS>) ((IEnumerable<PlayerSeaQuestS>) playerSeaQuestSArray).Where<PlayerSeaQuestS>((Func<PlayerSeaQuestS, bool>) (x => x.is_clear)).ToArray<PlayerSeaQuestS>()).Count<PlayerSeaQuestS>();
          Sea030StoryButton storyButton = storyM.transform.GetComponentInChildren<Sea030StoryButton>();
          storyButton.SetSprite(this.storyList[i].group);
          storyButton.UnLock(clearflag, playerSeaQuestSArray[0].is_new);
          if (this.storyList[i].line.Count > 0 && (UnityEngine.Object) this.storyList[i].line[cnt] != (UnityEngine.Object) null)
            this.storyList[i].line[cnt].SetActive(true);
          storyButton.onClick = new EventDelegate((EventDelegate.Callback) (() =>
          {
            Singleton<CommonRoot>.GetInstance().isTouchBlockAutoClose = true;
            Singleton<CommonRoot>.GetInstance().isTouchBlock = true;
            storyButton.changeClickMySelf();
            this.StoryChoiceButtonTween(41, 0.0f);
            this.TweenMap(Tuple.Create<int, int, int>(storyButton.Lnumber, storyButton.Mnumber, storyButton.TweenIndex));
          }));
          storyButton.PathNumber = playerSeaQuestSArray[0].quest_sea_s.quest_m.number_m;
          storyButton.Mnumber = playerSeaQuestSArray[0].quest_sea_s.quest_m_QuestSeaM;
          storyButton.Lnumber = playerSeaQuestSArray[0].quest_sea_s.quest_l_QuestSeaL;
          storyButton.TweenIndex = cnt;
          storyButton.slc_pointer_area.SetActive(!clearflag);
          ++cnt;
        }));
        int[] array = ((IEnumerable<PlayerSeaQuestS>) storyData).Select<PlayerSeaQuestS, int>((Func<PlayerSeaQuestS, int>) (x => x.quest_sea_s.quest_m_QuestSeaM)).Distinct<int>().OrderBy<int, int>((Func<int, int>) (y => y)).ToArray<int>();
        for (int index = 0; index < this.storyList[i].storyM.Count && index < array.Length; ++index)
        {
          PlayerSeaQuestS[] playerSeaQuestSArray = StoryData.S(3, i + 1, array[index]);
          if (playerSeaQuestSArray.Length != 0)
            this.MissionAchievementRate(playerSeaQuestSArray[0], this.storyList[i].storyM[index].transform.GetComponentInChildren<Sea030StoryButton>());
        }
      }
    }
    yield break;
  }

  private void MissionAchievementRate(PlayerSeaQuestS quest, Sea030StoryButton button)
  {
    int num = 0;
    int nowCount = 0;
    PlayerMissionHistory[] array1 = ((IEnumerable<PlayerMissionHistory>) SMManager.Get<PlayerMissionHistory[]>()).Where<PlayerMissionHistory>((Func<PlayerMissionHistory, bool>) (x => x.story_category == 9)).ToArray<PlayerMissionHistory>();
    QuestSeaMission[] array2 = ((IEnumerable<QuestSeaMission>) MasterData.QuestSeaMissionList).Where<QuestSeaMission>((Func<QuestSeaMission, bool>) (x => x.quest_s.quest_m_QuestSeaM == quest.quest_sea_s.quest_m_QuestSeaM)).ToArray<QuestSeaMission>();
    foreach (QuestSeaMission questSeaMission in array2)
      nowCount += ((IEnumerable<PlayerMissionHistory>) array1).Select<PlayerMissionHistory, int>((Func<PlayerMissionHistory, int>) (x => x.mission_id)).Contains<int>(questSeaMission.ID) ? 1 : 0;
    int allCount = num + array2.Length;
    button.MissionAchevement(nowCount, allCount);
  }

  private void TweenMap(Tuple<int, int, int> param)
  {
    GameObject gameObject = this.storyList[param.Item1 - 1].tween[param.Item3];
    if (!((UnityEngine.Object) gameObject != (UnityEngine.Object) null))
      return;
    TweenPosition orAddComponent1 = this.tweenObject.GetOrAddComponent<TweenPosition>();
    TweenScale orAddComponent2 = this.tweenObject.GetOrAddComponent<TweenScale>();
    orAddComponent1.from = this.tweenObject.transform.localPosition;
    orAddComponent1.to = gameObject.transform.localPosition;
    orAddComponent1.to -= this.scrollView.transform.localPosition;
    orAddComponent2.from = this.tweenObject.transform.localScale;
    orAddComponent2.to = gameObject.transform.localScale;
    orAddComponent1.duration = this.tweenDuration;
    if ((double) this.tweenDuration == 0.0)
      orAddComponent1.duration = 0.0f;
    orAddComponent1.delay = this.tweenDelay;
    orAddComponent1.animationCurve = this.tweenCurve;
    orAddComponent2.duration = this.tweenDuration;
    if ((double) this.tweenDuration == 0.0)
      orAddComponent2.duration = 0.0f;
    orAddComponent2.delay = this.tweenDelay;
    orAddComponent2.animationCurve = this.tweenCurve;
    EventDelegate.Set(orAddComponent1.onFinished, (EventDelegate.Callback) (() => this.ChangeScene(param.Item1, param.Item2)));
    orAddComponent1.PlayForward();
    orAddComponent2.PlayForward();
    this.currentTweenPos = orAddComponent1;
    this.currentTweenScale = orAddComponent2;
  }

  private bool checkTowerOpen()
  {
    return SMManager.Get<SM.TowerPeriod[]>() != null && SMManager.Get<SM.TowerPeriod[]>().Length != 0 && SMManager.Get<SM.TowerPeriod[]>()[0].final_at > ServerTime.NowAppTime();
  }

  public IEnumerator Init(PlayerSeaQuestS[] StoryData, bool forceInitialize = false)
  {
    Sea030_questMenu sea030QuestMenu1 = this;
    sea030QuestMenu1.StoryData = StoryData;
    if (sea030QuestMenu1.StoryData == null)
    {
      sea030QuestMenu1.IsPush = false;
    }
    else
    {
      IEnumerator e = ServerTime.WaitSync();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      bool isTowerOpen = sea030QuestMenu1.checkTowerOpen();
      if (sea030QuestMenu1.isInitialized & forceInitialize)
        sea030QuestMenu1.isInitialized = false;
      if (!sea030QuestMenu1.isInitialized)
      {
        Future<GameObject> prefab;
        if (StoryData != null)
        {
          for (int i = 0; i < sea030QuestMenu1.storyList.Count; ++i)
          {
            int[] storyMnumbers = ((IEnumerable<PlayerSeaQuestS>) StoryData).Where<PlayerSeaQuestS>((Func<PlayerSeaQuestS, bool>) (x => x.quest_sea_s.quest_l_QuestSeaL == i + 1)).Select<PlayerSeaQuestS, int>((Func<PlayerSeaQuestS, int>) (x => x.quest_sea_s.quest_m_QuestSeaM)).Distinct<int>().ToArray<int>();
            if (storyMnumbers.Length != 0)
            {
              prefab = new ResourceObject("Prefabs/sea030_quest/dir_Story_sea").Load<GameObject>();
              e = prefab.Wait();
              while (e.MoveNext())
                yield return e.Current;
              e = (IEnumerator) null;
              int index = 0;
              foreach (GameObject gameObject in sea030QuestMenu1.storyList[i].storyM)
              {
                if (index < sea030QuestMenu1.storyList[i].storyM.Count)
                {
                  if (index < storyMnumbers.Length)
                  {
                    if (StoryData.S(3, i + 1, storyMnumbers[index]).Length != 0)
                      prefab.Result.Clone(gameObject.transform);
                    ++index;
                  }
                  else
                    break;
                }
                else
                  break;
              }
              prefab = (Future<GameObject>) null;
            }
            storyMnumbers = (int[]) null;
          }
        }
        if (isTowerOpen)
        {
          prefab = new ResourceObject("Prefabs/sea030_quest/dir_Story_sea_tower").Load<GameObject>();
          e = prefab.Wait();
          while (e.MoveNext())
            yield return e.Current;
          e = (IEnumerator) null;
          prefab.Result.Clone(sea030QuestMenu1.storyList[6].storyM[0].transform);
          prefab = (Future<GameObject>) null;
        }
      }
      else if ((UnityEngine.Object) sea030QuestMenu1.currentTweenPos != (UnityEngine.Object) null && (UnityEngine.Object) sea030QuestMenu1.currentTweenScale != (UnityEngine.Object) null)
      {
        sea030QuestMenu1.currentTweenPos.onFinished.Clear();
        // ISSUE: reference to a compiler-generated method
        EventDelegate.Set(sea030QuestMenu1.currentTweenPos.onFinished, new EventDelegate.Callback(sea030QuestMenu1.\u003CInit\u003Eb__27_0));
        sea030QuestMenu1.currentTweenPos.PlayReverse();
        sea030QuestMenu1.currentTweenScale.PlayReverse();
      }
      else
        sea030QuestMenu1.StoryChoiceButtonTween(42, 0.5f);
      if (StoryData != null)
      {
        for (int i = 0; i < sea030QuestMenu1.storyList.Count; i++)
        {
          int[] array = ((IEnumerable<PlayerSeaQuestS>) StoryData).Where<PlayerSeaQuestS>((Func<PlayerSeaQuestS, bool>) (x => x.quest_sea_s.quest_l_QuestSeaL == i + 1)).Select<PlayerSeaQuestS, int>((Func<PlayerSeaQuestS, int>) (x => x.quest_sea_s.quest_m_QuestSeaM)).Distinct<int>().ToArray<int>();
          if (array.Length != 0)
          {
            int index = 0;
            foreach (var data in sea030QuestMenu1.storyList[i].storyM.Select((s, j) => new
            {
              s = s,
              j = j
            }))
            {
              if (index < sea030QuestMenu1.storyList[i].storyM.Count)
              {
                if (index < array.Length)
                {
                  if (StoryData.S(3, i + 1, array[index]).Length != 0)
                  {
                    sea030QuestMenu1.storyList[i].storyData = StoryData.M(3, i + 1);
                    sea030QuestMenu1.InitStoryChoiceButton(data.s, 3, i + 1, data.j, !sea030QuestMenu1.isInitialized, sea030QuestMenu1.storyList[i].storyData);
                  }
                  ++index;
                }
                else
                  break;
              }
              else
                break;
            }
          }
        }
      }
      if (isTowerOpen)
      {
        Sea030_questMenu sea030QuestMenu = sea030QuestMenu1;
        Sea030StoryTowerButton storyButton = sea030QuestMenu1.storyList[6].storyM[0].GetComponentInChildren<Sea030StoryTowerButton>();
        if ((UnityEngine.Object) storyButton != (UnityEngine.Object) null)
        {
          storyButton.Lnumber = 7;
          storyButton.Mnumber = 1;
          storyButton.TweenIndex = 0;
          storyButton.initTween();
          UIButton componentInChildren = storyButton.GetComponentInChildren<UIButton>(true);
          if ((UnityEngine.Object) componentInChildren != (UnityEngine.Object) null)
            EventDelegate.Set(componentInChildren.onClick, (EventDelegate.Callback) (() =>
            {
              Singleton<CommonRoot>.GetInstance().isTouchBlockAutoClose = true;
              Singleton<CommonRoot>.GetInstance().isTouchBlock = true;
              storyButton.changeAnimDirection();
              sea030QuestMenu.StoryChoiceButtonTween(41, 0.0f);
              sea030QuestMenu.TweenMap(Tuple.Create<int, int, int>(storyButton.Lnumber, storyButton.Mnumber, storyButton.TweenIndex));
            }));
        }
      }
      if (StoryData != null)
      {
        e = sea030QuestMenu1.StoryChoiceButtonSetting(StoryData);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
      }
      e = sea030QuestMenu1.LoadLeaderUnit();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      if (!sea030QuestMenu1.isInitialized)
        sea030QuestMenu1.StoryChoiceButtonTween(42, 0.5f);
      sea030QuestMenu1.isInitialized = true;
      sea030QuestMenu1.IsPush = false;
    }
  }

  public void ChangeScene(int passLdata, int passMdata)
  {
    Debug.Log((object) ("M:" + (object) passLdata + "\nL:" + (object) passMdata));
    if (passLdata == 7)
      Tower029TopScene.ChangeScene(true, false);
    else
      Quest0022Scene.ChangeScene0022(true, passLdata, passMdata);
  }

  public void StoryChoiceButtonTween(int tweengroup, float delay = 0.0f)
  {
    foreach (Sea030_questMenu.StoryGroupInfo story in this.storyList)
    {
      GameObject storyButton = story.storyButton;
      if (!((UnityEngine.Object) storyButton == (UnityEngine.Object) null))
        ((IEnumerable<UITweener>) storyButton.GetComponentsInChildren<UITweener>()).ForEach<UITweener>((System.Action<UITweener>) (x =>
        {
          if (x.tweenGroup != tweengroup)
            return;
          x.delay = delay;
          x.ResetToBeginning();
          x.PlayForward();
        }));
    }
  }

  public virtual void IbtnBack()
  {
    Debug.Log((object) "click default event IbtnBack");
  }

  public virtual void IbtnStory01()
  {
    Debug.Log((object) "click default event IbtnStory01");
  }

  public virtual void IbtnStory02()
  {
    Debug.Log((object) "click default event IbtnStory02");
  }

  public virtual void IbtnStory03()
  {
    Debug.Log((object) "click default event IbtnStory03");
  }

  public virtual void IbtnStory04()
  {
    Debug.Log((object) "click default event IbtnStory04");
  }

  public virtual void IbtnStory05()
  {
    Debug.Log((object) "click default event IbtnStory05");
  }

  public void onTeamEditButton()
  {
    if ((UnityEngine.Object) this.currentTweenPos != (UnityEngine.Object) null || this.IsPushAndSet())
      return;
    Unit0046Scene.changeScene(true, (QuestLimitationBase[]) null, (string) null, false);
  }

  public override void onBackButton()
  {
    if ((UnityEngine.Object) this.currentTweenPos != (UnityEngine.Object) null || this.IsPushAndSet())
      return;
    Singleton<NGSceneManager>.GetInstance().sceneBase.IsPush = true;
    Singleton<NGGameDataManager>.GetInstance().lastReferenceUnitID = -1;
    Singleton<NGGameDataManager>.GetInstance().lastReferenceUnitIndex = -1;
    Singleton<NGSceneManager>.GetInstance().destroyLoadedScenes();
    Singleton<NGSceneManager>.GetInstance().clearStack();
    Sea030HomeScene.ChangeScene(false, false);
  }

  public enum storyGroup
  {
    Pool,
    Beach,
    Jungle,
    Unify,
    Beach2,
    Beach3,
    Tower,
    Max,
  }

  [Serializable]
  public class StoryGroupInfo
  {
    public Sea030_questMenu.storyGroup group;
    public GameObject storyButton;
    public List<GameObject> storyM;
    public List<GameObject> line;
    public List<GameObject> tween;
    [HideInInspector]
    public PlayerSeaQuestS[] storyData;
  }
}
