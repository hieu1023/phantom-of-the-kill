﻿// Decompiled with JetBrains decompiler
// Type: Quest00230Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quest00230Scene : NGSceneBase
{
  [SerializeField]
  private Quest00230Menu menu;

  public static void ChangeScene(bool stack, int period_id)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("quest002_30", (stack ? 1 : 0) != 0, (object) period_id);
  }

  public static void ChangeScene(bool stack, EventInfo eventInfo)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("quest002_30", (stack ? 1 : 0) != 0, (object) eventInfo);
  }

  public IEnumerator onStartSceneAsync(EventInfo eventInfo)
  {
    Quest00230Scene quest00230Scene = this;
    if (eventInfo.IsGuild())
    {
      quest00230Scene.bgmFile = "BgmGuild";
      quest00230Scene.bgmName = "bgm085";
      quest00230Scene.currentSceneGuildChatDisplayingStatus = NGSceneBase.GuildChatDisplayingStatus.Opened;
    }
    else
    {
      quest00230Scene.bgmFile = "";
      quest00230Scene.bgmName = "bgm001";
      Singleton<CommonRoot>.GetInstance().guildChatManager.CloseGuildChat();
      quest00230Scene.currentSceneGuildChatDisplayingStatus = NGSceneBase.GuildChatDisplayingStatus.Closed;
    }
    Future<WebAPI.Response.EventTop> request = WebAPI.EventTop(eventInfo.period_id, (System.Action<WebAPI.Response.UserError>) (e => WebAPI.DefaultUserErrorCallback(e)));
    IEnumerator e1 = request.Wait();
    while (e1.MoveNext())
      yield return e1.Current;
    e1 = (IEnumerator) null;
    WebAPI.Response.EventTop result = request.Result;
    if (result != null)
    {
      if ((UnityEngine.Object) quest00230Scene.menu != (UnityEngine.Object) null)
      {
        e1 = quest00230Scene.menu.Init(eventInfo, result);
        while (e1.MoveNext())
          yield return e1.Current;
        e1 = (IEnumerator) null;
      }
      Singleton<CommonRoot>.GetInstance().isLoading = false;
      Singleton<CommonRoot>.GetInstance().loadingMode = 0;
    }
  }

  public IEnumerator onStartSceneAsync(int period_id)
  {
    Future<WebAPI.Response.QuestProgressExtra> extra = WebAPI.QuestProgressExtra((System.Action<WebAPI.Response.UserError>) (error =>
    {
      WebAPI.DefaultUserErrorCallback(error);
      Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
    }));
    IEnumerator e = extra.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if (extra.Result != null)
    {
      int? nullable = ((IEnumerable<EventInfo>) extra.Result.event_infos).FirstIndexOrNull<EventInfo>((Func<EventInfo, bool>) (x => x.period_id == period_id));
      if (!nullable.HasValue)
      {
        Debug.LogError((object) ("event not found period_id = " + (object) period_id));
      }
      else
      {
        e = this.onStartSceneAsync(extra.Result.event_infos[nullable.Value]);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
      }
    }
  }
}
