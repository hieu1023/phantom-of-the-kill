﻿// Decompiled with JetBrains decompiler
// Type: Battle01CommandSkillUse
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections.Generic;
using UnityEngine;

public class Battle01CommandSkillUse : BattleMonoBehaviour
{
  private UIButton button;
  private BL.Unit unit;
  private BL.Skill skill;
  private List<BL.Unit> targets;
  private List<BL.Panel> panels;

  private void Awake()
  {
    this.button = this.GetComponent<UIButton>();
    EventDelegate.Set(this.button.onClick, new EventDelegate((MonoBehaviour) this, "onClick"));
  }

  public void setData(BL.Unit unit, BL.Skill skill, List<BL.Unit> targets, List<BL.Panel> panels)
  {
    this.unit = unit;
    this.skill = skill;
    this.targets = targets;
    this.panels = panels;
  }

  public void setEnable(bool enable)
  {
    this.button.isEnabled = enable;
  }

  public void onClick()
  {
    if (!this.battleManager.isBattleEnable)
      return;
    if (this.battleManager.useGameEngine)
      this.battleManager.gameEngine.moveUnitWithSkill(this.unit, this.skill, this.targets, this.panels);
    else
      this.env.useSkill(this.unit, this.skill, this.targets, this.panels, (BL.BattleSkillResult) null, this.battleManager.getManager<BattleTimeManager>(), (XorShift) null);
    this.battleManager.getController<BattleInputObserver>().cancelTargetSelect();
    NGUITools.FindInParents<Battle01SelectNode>(this.transform).backToTop();
  }
}
