﻿// Decompiled with JetBrains decompiler
// Type: CallFuncData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[Serializable]
public class CallFuncData
{
  [SerializeField]
  private string func_name = "";
  [SerializeField]
  private int id;
  [SerializeField]
  private GameObject func_obj;
  [SerializeField]
  private float wait_time;

  public int ID
  {
    get
    {
      return this.id;
    }
  }

  public string FuncName
  {
    get
    {
      return this.func_name;
    }
    set
    {
      this.func_name = value;
    }
  }

  public GameObject FuncObj
  {
    get
    {
      return this.func_obj;
    }
    set
    {
      this.func_obj = value;
    }
  }

  public float WaitTime
  {
    get
    {
      return this.wait_time;
    }
    set
    {
      this.wait_time = value;
    }
  }
}
