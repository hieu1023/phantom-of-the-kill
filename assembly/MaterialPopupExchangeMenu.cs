﻿// Decompiled with JetBrains decompiler
// Type: MaterialPopupExchangeMenu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialPopupExchangeMenu : BackButtonMenuBase
{
  private List<UIButton> pauseButtons_ = new List<UIButton>();
  public GameObject topIconUnit_;
  [SerializeField]
  private UILabel txtName_;
  [SerializeField]
  private UILabel txtDetail_;
  [SerializeField]
  private UIButton btnOk_;
  [SerializeField]
  private UIButton btnCancel_;
  [SerializeField]
  private UILabel txtTicketNum;
  private ShopMaterialExchangeListMenu menu_;
  private SelectTicketSelectSample sample_;
  private bool isInitialized_;
  private bool isOpenPopup_;
  private bool isOK_;
  private int nums;

  public IEnumerator coInitialize(
    ShopMaterialExchangeListMenu menu,
    SelectTicketSelectSample unitSample,
    PlayerSelectTicketSummary playerUnitTicket,
    SM.SelectTicket unitTicket)
  {
    // ISSUE: reference to a compiler-generated field
    int num = this.\u003C\u003E1__state;
    MaterialPopupExchangeMenu popupExchangeMenu = this;
    IEnumerator coroutine;
    if (num != 0)
    {
      if (num != 1)
        return false;
      // ISSUE: reference to a compiler-generated field
      this.\u003C\u003E1__state = -1;
      popupExchangeMenu.nums = (int) coroutine.Current;
      popupExchangeMenu.txtName_.SetTextLocalize(popupExchangeMenu.sample_.reward_title);
      popupExchangeMenu.txtTicketNum.SetTextLocalize(popupExchangeMenu.menu_.quantity_.ToString() + "→[FFFF00]" + (object) (popupExchangeMenu.menu_.quantity_ - unitTicket.cost) + "[-]");
      popupExchangeMenu.txtDetail_.SetTextLocalize(string.Format(Consts.GetInstance().SHOP_00723_POPUP_MATERIAL_EXCHANGE_DETAIL, (object) unitTicket.cost));
      popupExchangeMenu.isOK_ = playerUnitTicket.quantity >= unitTicket.cost;
      popupExchangeMenu.btnCancel_.onClick.Clear();
      // ISSUE: reference to a compiler-generated method
      popupExchangeMenu.btnCancel_.onClick.Add(new EventDelegate(new EventDelegate.Callback(popupExchangeMenu.\u003CcoInitialize\u003Eb__13_0)));
      popupExchangeMenu.btnOk_.isEnabled = popupExchangeMenu.isOK_;
      popupExchangeMenu.isInitialized_ = true;
      return false;
    }
    // ISSUE: reference to a compiler-generated field
    this.\u003C\u003E1__state = -1;
    popupExchangeMenu.isInitialized_ = false;
    popupExchangeMenu.menu_ = menu;
    popupExchangeMenu.sample_ = unitSample;
    coroutine = popupExchangeMenu.menu_.SetIcon(popupExchangeMenu.sample_, popupExchangeMenu.topIconUnit_.transform);
    // ISSUE: reference to a compiler-generated field
    this.\u003C\u003E2__current = (object) popupExchangeMenu.StartCoroutine(coroutine);
    // ISSUE: reference to a compiler-generated field
    this.\u003C\u003E1__state = 1;
    return true;
  }

  private void OnDestroy()
  {
    this.StopCoroutine("waitPopupClose");
  }

  public override void onBackButton()
  {
    this.onClickCancel();
  }

  public void onClickOk()
  {
    if (this.IsPushAndSet())
      return;
    this.menu_.InitObj(this.topIconUnit_.transform.GetChild(0).gameObject);
    Singleton<PopupManager>.GetInstance().dismiss(false);
    this.menu_.doExchangeMaterial(this.nums);
  }

  public void onClickCancel()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<PopupManager>.GetInstance().dismiss(false);
  }
}
