﻿// Decompiled with JetBrains decompiler
// Type: TransformSizeInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

public class TransformSizeInfo
{
  public float Scale;
  public float PositionX;
  public float PositionY;

  public TransformSizeInfo(float scale, float positionX, float positionY)
  {
    this.Scale = scale;
    this.PositionX = positionX;
    this.PositionY = positionY;
  }
}
