﻿// Decompiled with JetBrains decompiler
// Type: Mypage051Transition
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Mypage051Transition : MonoBehaviour
{
  [SerializeField]
  private Mypage051Menu menu;

  public void onHeavenly()
  {
    if (this.menu.IsPushAndSet())
      return;
    Singleton<CommonRoot>.GetInstance().isTouchBlock = true;
    NGSoundManager instance = Singleton<NGSoundManager>.GetInstance();
    if ((Object) instance != (Object) null)
      instance.playSE("SE_1039", false, 0.0f, -1);
    this.StartCoroutine(this.menu.CloudAnimationStart());
  }

  public void onEventQuest()
  {
    if (this.menu.IsPushAndSet())
      return;
    NGSoundManager instance = Singleton<NGSoundManager>.GetInstance();
    if ((Object) instance != (Object) null)
      instance.playSE("SE_1002", false, 0.0f, -1);
    Quest052171Scene.ChangeScene(true);
  }

  public void onQuest()
  {
    if (this.menu.IsPushAndSet())
      return;
    NGSoundManager instance = Singleton<NGSoundManager>.GetInstance();
    if ((Object) instance != (Object) null)
      instance.playSE("SE_1002", false, 0.0f, -1);
    Quest0528Scene.changeScene(true);
  }

  public void onMenu()
  {
    if (this.menu.IsPushAndSet())
      return;
    Menu0591Scene.ChangeScene(true);
  }
}
