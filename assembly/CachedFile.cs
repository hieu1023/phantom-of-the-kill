﻿// Decompiled with JetBrains decompiler
// Type: CachedFile
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.IO;

public static class CachedFile
{
  private static HashSet<string> hash = new HashSet<string>();

  public static bool Exists(string path)
  {
    if (CachedFile.hash.Contains(path))
      return true;
    if (!File.Exists(path))
      return false;
    CachedFile.hash.Add(path);
    return true;
  }

  public static void Add(string path)
  {
    CachedFile.hash.Add(path);
  }

  public static void Clear()
  {
    CachedFile.hash.Clear();
  }
}
