﻿// Decompiled with JetBrains decompiler
// Type: Battle01TipEventMoney
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;
using System.Collections.Generic;

public class Battle01TipEventMoney : Battle01TipEventBase
{
  protected override IEnumerator Start_Original()
  {
    // ISSUE: reference to a compiler-generated field
    int num = this.\u003C\u003E1__state;
    Battle01TipEventMoney battle01TipEventMoney = this;
    if (num != 0)
      return false;
    // ISSUE: reference to a compiler-generated field
    this.\u003C\u003E1__state = -1;
    battle01TipEventMoney.selectIcon(0);
    return false;
  }

  public override void setData(BL.DropData e, BL.Unit unit)
  {
    if (e.reward.Type != MasterDataTable.CommonRewardType.money)
      return;
    this.setText(Consts.Format(Consts.GetInstance().TipEvent_text_money, (IDictionary) new Dictionary<string, int>()
    {
      ["money"] = e.reward.Quantity
    }));
  }
}
