﻿// Decompiled with JetBrains decompiler
// Type: Shop00712Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System;
using System.Collections;
using UnityEngine;

public class Shop00712Menu : BackButtonMenuBase
{
  private Modified<Player> player;
  [SerializeField]
  private GameObject DirPopup01;
  [SerializeField]
  private GameObject DirPopup02;
  [SerializeField]
  private UILabel TxtDescription01;
  [SerializeField]
  private UILabel TxtDescription02;
  [SerializeField]
  private UILabel TxtDescription03;
  [SerializeField]
  private UILabel TxtPopuptitle;
  [SerializeField]
  private UILabel txtnumber;
  private const int APBP_RECOVERY_SHOP_ID = 10000001;
  private System.Action btnAct;

  private IEnumerator APRecover()
  {
    if (Singleton<CommonRoot>.GetInstance().isLoading)
    {
      Singleton<CommonRoot>.GetInstance().isLoading = true;
      Future<WebAPI.Response.ShopBuy> paramF = WebAPI.ShopBuy(10000001, 1, (System.Action<WebAPI.Response.UserError>) (e =>
      {
        WebAPI.DefaultUserErrorCallback(e);
        Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
      })).Then<WebAPI.Response.ShopBuy>((Func<WebAPI.Response.ShopBuy, WebAPI.Response.ShopBuy>) (result =>
      {
        Singleton<NGGameDataManager>.GetInstance().Parse(result);
        return result;
      }));
      IEnumerator e1 = paramF.Wait();
      while (e1.MoveNext())
        yield return e1.Current;
      e1 = (IEnumerator) null;
      if (paramF.Result != null)
      {
        e1 = OnDemandDownload.WaitLoadHasUnitResource(false, false);
        while (e1.MoveNext())
          yield return e1.Current;
        e1 = (IEnumerator) null;
        EventTracker.TrackSpend("COIN", "COIN_SHOP_" + (object) 10000001, 1);
        EventTracker.TrackEvent("SHOP", "COIN_SHOP", "ID_" + (object) 10000001, 1);
        Singleton<NGGameDataManager>.GetInstance().refreshHomeHome = true;
        paramF = (Future<WebAPI.Response.ShopBuy>) null;
      }
    }
  }

  public void IbtnPopupYes()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<PopupManager>.GetInstance().onDismiss(true);
    if (this.player.Value.CheckApFull())
      Singleton<PopupManager>.GetInstance().monitorCoroutine(this.popupApFull());
    else if (SMManager.Get<Player>().CheckKiseki(1))
      Singleton<PopupManager>.GetInstance().monitorCoroutine(this.APRecoverAsync());
    else
      Singleton<PopupManager>.GetInstance().monitorCoroutine(PopupUtility._999_3_1(Consts.GetInstance().SHOP_99931_TXT_DESCRIPTION, "", "", Gacha99931Menu.PaymentType.ALL));
  }

  private IEnumerator popupApFull()
  {
    Future<GameObject> popupF = Res.Prefabs.popup.popup_999_11_1_1__anim_popup01.Load<GameObject>();
    IEnumerator e = popupF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Singleton<PopupManager>.GetInstance().open(popupF.Result, false, false, false, true, false, false, "SE_1006");
  }

  private IEnumerator APRecoverAsync()
  {
    Singleton<PopupManager>.GetInstance().closeAll(true);
    Singleton<CommonRoot>.GetInstance().loadingMode = 1;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    IEnumerator e = this.APRecover();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Singleton<CommonRoot>.GetInstance().isLoading = false;
    Singleton<CommonRoot>.GetInstance().loadingMode = 0;
    e = this.popup00713();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  private IEnumerator popup00713()
  {
    Future<GameObject> prefab = Res.Prefabs.popup.popup_007_13__anim_popup01.Load<GameObject>();
    IEnumerator e = prefab.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Singleton<PopupManager>.GetInstance().open(prefab.Result, false, false, false, true, false, false, "SE_1006").GetComponent<Shop00713Menu>().SetBtnAct(this.btnAct);
  }

  public void IbtnNo()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<PopupManager>.GetInstance().onDismiss();
  }

  public override void onBackButton()
  {
    this.IbtnNo();
  }

  public void setUserData()
  {
    this.player = SMManager.Observe<Player>();
    Player player = this.player.Value;
    this.TxtDescription02.SetTextLocalize(Consts.Format(Consts.GetInstance().SHOP_00712_MENU_AP_RECOVERY, (IDictionary) new Hashtable()
    {
      {
        (object) "now",
        (object) player.ap
      },
      {
        (object) "max",
        (object) player.ap_max
      }
    }));
    this.txtnumber.SetTextLocalize(player.coin);
  }

  public void SetBtnAction(System.Action questChangeScene)
  {
    this.btnAct = questChangeScene;
  }
}
