﻿// Decompiled with JetBrains decompiler
// Type: Guild0287Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using UnityEngine;

public class Guild0287Scene : NGSceneBase
{
  [SerializeField]
  private Guild0287Menu menu;

  public static void ChangeScene()
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("guild028_7", true, (object[]) Array.Empty<object>());
  }

  public IEnumerator onStartSceneAsync()
  {
    if (!Persist.guildBankSetting.Exists)
    {
      Persist.guildBankSetting.Data.reset();
      Persist.guildBankSetting.Flush();
    }
    if (Persist.guildBankSetting.Exists && Persist.guildBankSetting.Data.guildBankFirstTime)
    {
      Persist.guildBankSetting.Data.guildBankFirstTime = false;
      Persist.guildBankSetting.Flush();
      Guild02871Scene.ChangeScene(true);
    }
    else
    {
      IEnumerator e = this.menu.InitializeAsync();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
  }

  public void onStartScene()
  {
    this.menu.Initialize();
  }

  public override IEnumerator onEndSceneAsync()
  {
    Guild0287Scene guild0287Scene = this;
    float startTime = Time.time;
    while (!guild0287Scene.isTweenFinished && (double) Time.time - (double) startTime < (double) guild0287Scene.tweenTimeoutTime)
      yield return (object) null;
    Singleton<CommonRoot>.GetInstance().loadingMode = 0;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    guild0287Scene.isTweenFinished = true;
    yield return (object) null;
    // ISSUE: reference to a compiler-generated method
    yield return (object) guild0287Scene.\u003C\u003En__0();
  }
}
