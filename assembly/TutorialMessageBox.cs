﻿// Decompiled with JetBrains decompiler
// Type: TutorialMessageBox
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using System.Collections;
using UnityEngine;

public class TutorialMessageBox : MonoBehaviour
{
  [SerializeField]
  private UILabel label;
  [SerializeField]
  private GameObject arrow;
  [SerializeField]
  private GameObject[] characters;
  [SerializeField]
  private NGxFaceSprite[] face;
  [SerializeField]
  private GameObject ItemIconRoot;
  private GameObject UniqueIconsPrefab;
  private GameObject effectPrefab;

  public void Init(
    string message,
    int characterIndex,
    bool dispArrow,
    string faceName,
    string itemName,
    int keyId)
  {
    this.label.SetTextLocalize(message);
    this.DispCharacter(characterIndex);
    if ((Object) this.arrow != (Object) null)
      this.arrow.SetActive(dispArrow);
    this.StartCoroutine(this.ChangeItemIcon(itemName, keyId));
    this.StartCoroutine(this.FaceChange(faceName, characterIndex));
  }

  public IEnumerator FaceChange(string faceName, int characterIndex)
  {
    if (this.face.Length != 0)
    {
      if (string.IsNullOrEmpty(faceName))
        faceName = "normal";
      NGxFaceSprite ngxFaceSprite = (NGxFaceSprite) null;
      try
      {
        ngxFaceSprite = this.face[characterIndex];
      }
      catch
      {
        Debug.LogWarning((object) ("キャラID:" + (object) characterIndex + "のfaceがアタッチされていない"));
      }
      if ((Object) ngxFaceSprite != (Object) null)
      {
        IEnumerator i = ngxFaceSprite.ChangeFace(faceName);
        while (i.MoveNext())
          yield return i.Current;
        i = (IEnumerator) null;
      }
    }
  }

  private IEnumerator ChangeItemIcon(string itemName, int keyId = 1)
  {
    if (!((Object) this.ItemIconRoot == (Object) null))
    {
      if (string.IsNullOrEmpty(itemName))
      {
        this.ItemIconRoot.gameObject.SetActive(false);
      }
      else
      {
        this.ItemIconRoot.gameObject.SetActive(true);
        IEnumerator e;
        if ((Object) this.effectPrefab == (Object) null)
        {
          Future<GameObject> effectPrefabF = Res.Prefabs.Tutorial.dir_effect.Load<GameObject>();
          e = effectPrefabF.Wait();
          while (e.MoveNext())
            yield return e.Current;
          e = (IEnumerator) null;
          this.effectPrefab = effectPrefabF.Result;
          effectPrefabF = (Future<GameObject>) null;
        }
        foreach (Component component in this.ItemIconRoot.transform)
          Object.Destroy((Object) component.gameObject);
        CreateIconObject target = this.ItemIconRoot.GetOrAddComponent<CreateIconObject>();
        GameObject icon = target.GetIcon();
        MasterDataTable.CommonRewardType? reward = this.GetReward(itemName);
        if (reward.HasValue)
        {
          if (reward.Value != MasterDataTable.CommonRewardType.quest_key)
            keyId = 0;
          e = target.CreateThumbnail(reward.Value, keyId, 0, true, true, new CommonQuestType?(), false);
          while (e.MoveNext())
            yield return e.Current;
          e = (IEnumerator) null;
          this.effectPrefab.Clone(target.GetIcon().transform);
        }
        else if ((Object) icon != (Object) null)
          icon.SetActive(false);
      }
    }
  }

  private void DispCharacter(int characterIndex)
  {
    for (int index = 0; index < this.characters.Length; ++index)
      this.characters[index].SetActive(index == characterIndex);
  }

  private MasterDataTable.CommonRewardType? GetReward(string itemName)
  {
    switch (itemName)
    {
      case "battlemedal":
        return new MasterDataTable.CommonRewardType?(MasterDataTable.CommonRewardType.battle_medal);
      case "gachaticket":
        return new MasterDataTable.CommonRewardType?(MasterDataTable.CommonRewardType.gacha_ticket);
      case "key":
        return new MasterDataTable.CommonRewardType?(MasterDataTable.CommonRewardType.quest_key);
      case "kiseki":
        return new MasterDataTable.CommonRewardType?(MasterDataTable.CommonRewardType.coin);
      case "medal":
        return new MasterDataTable.CommonRewardType?(MasterDataTable.CommonRewardType.medal);
      case "none":
        return new MasterDataTable.CommonRewardType?();
      case "point":
        return new MasterDataTable.CommonRewardType?(MasterDataTable.CommonRewardType.friend_point);
      case "zeny":
        return new MasterDataTable.CommonRewardType?(MasterDataTable.CommonRewardType.money);
      default:
        Debug.LogWarning((object) ("ChangeItemIcon アイテム名：" + itemName + "が見つかりませんでした。"));
        return new MasterDataTable.CommonRewardType?();
    }
  }
}
