﻿// Decompiled with JetBrains decompiler
// Type: Shop99981Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System;
using System.Collections;
using UnityEngine;

public class Shop99981Menu : BackButtonMenuBase
{
  [SerializeField]
  protected UIInput nen;
  [SerializeField]
  protected UIInput getsu;
  [SerializeField]
  protected UIInput hi;
  [SerializeField]
  private UILabel TxtDescription01;
  [SerializeField]
  private UILabel TxtDescription02;
  [SerializeField]
  private UILabel TxtDescription04;
  [SerializeField]
  private UILabel TxtDescription06;
  [SerializeField]
  private UILabel TxtPopuptitle;
  private System.Action onCancel;

  public void SetOnCancel(System.Action callback)
  {
    this.onCancel = callback;
  }

  private void Awake()
  {
    this.nen.caretColor = Color.black;
    this.getsu.caretColor = Color.black;
    this.hi.caretColor = Color.black;
  }

  private void update()
  {
  }

  public void IbtnNo()
  {
    if (this.IsPushAndSet())
      return;
    PurchaseBehavior.PopupDismiss(false);
    PurchaseBehavior.PopupDismiss(false);
    this.onCancel();
  }

  public override void onBackButton()
  {
    this.IbtnNo();
  }

  public void IbtnPopupDecide()
  {
    string str1 = this.nen.value;
    string str2 = this.getsu.value;
    string day = this.hi.value;
    if (DateCheck.YearCheck(str1) && DateCheck.MonthCheck(str2))
    {
      if (DateTime.TryParse(str1 + "/" + str2 + "/" + day, out DateTime _))
      {
        this.StartCoroutine(this.popup99982(str1, str2, day));
        return;
      }
    }
    PurchaseBehavior.ShowPopupWithMessage(Consts.GetInstance().SHOP_99981_MENU_01, Consts.GetInstance().SHOP_99981_MENU_02, (System.Action) null);
  }

  public void Shop99981Visible(bool b)
  {
    this.transform.Find("MainPanel").gameObject.SetActive(b);
  }

  private IEnumerator popup99982(string year, string month, string day)
  {
    Future<GameObject> prefab = Res.Prefabs.popup.popup_999_8_2__anim_popup01.Load<GameObject>();
    IEnumerator e = prefab.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    GameObject gameObject = PurchaseBehavior.PopupOpen(prefab.Result);
    gameObject.GetComponent<SetPopupText>().SetText(string.Format(Consts.GetInstance().SHOP_99981_MENU_03, (object) year, (object) month, (object) day), true);
    gameObject.GetComponent<Shop99982Menu>().Init(year, month, day);
  }
}
