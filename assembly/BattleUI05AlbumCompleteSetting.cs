﻿// Decompiled with JetBrains decompiler
// Type: BattleUI05AlbumCompleteSetting
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleUI05AlbumCompleteSetting : MonoBehaviour
{
  [SerializeField]
  private UILabel txtReward;
  [SerializeField]
  private UILabel txtAlbumName;
  [SerializeField]
  private CreateIconObject rewardIcon;
  [SerializeField]
  private UI2DSprite Illustration;
  [SerializeField]
  private UI2DSprite IllustrationEff;

  public IEnumerator Init(MasterDataTable.SeaAlbum seaAlbum, List<SeaAlbumRewardGroup> rewardList)
  {
    IEnumerator e = this.CreateRewardIcon(seaAlbum, rewardList);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    e = this.LoadIllustration(seaAlbum);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.txtAlbumName.SetTextLocalize(seaAlbum.name);
    foreach (SeaAlbumRewardGroup reward in rewardList)
    {
      if (reward.reward_type_id == MasterDataTable.CommonRewardType.emblem)
        this.txtReward.SetTextLocalize(reward.reward_title);
    }
  }

  private IEnumerator CreateRewardIcon(
    MasterDataTable.SeaAlbum seaAlbum,
    List<SeaAlbumRewardGroup> rewardList)
  {
    IEnumerator e;
    foreach (SeaAlbumRewardGroup reward in rewardList)
    {
      if (reward.reward_type_id == MasterDataTable.CommonRewardType.emblem)
      {
        e = this.rewardIcon.CreateThumbnail(reward.reward_type_id, reward.reward_id, 0, true, true, new CommonQuestType?(), false);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
      }
    }
    e = this.LoadIllustration(seaAlbum);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    yield return (object) null;
  }

  private IEnumerator LoadIllustration(MasterDataTable.SeaAlbum seaAlbum)
  {
    Future<UnityEngine.Sprite> imageF = Singleton<ResourceManager>.GetInstance().Load<UnityEngine.Sprite>(string.Format("Album/{0}/slc_album_s", (object) seaAlbum.ID), 1f);
    IEnumerator e = imageF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    UnityEngine.Sprite result = imageF.Result;
    if (!((Object) result == (Object) null))
    {
      this.Illustration.sprite2D = result;
      this.IllustrationEff.sprite2D = result;
    }
  }
}
