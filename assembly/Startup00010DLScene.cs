﻿// Decompiled with JetBrains decompiler
// Type: Startup00010DLScene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Startup00010DLScene : MonoBehaviour
{
  [SerializeField]
  private Startup00010DLMenu menu;

  private void Awake()
  {
    ModalWindow.setupRootPanel(this.GetComponent<UIRoot>());
  }

  public void Start()
  {
    this.menu.onStartSceneAsync();
  }
}
