﻿// Decompiled with JetBrains decompiler
// Type: Unit00443Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit00443Scene : NGSceneBase
{
  public Unit00443Menu menu;
  private bool targetGear_favorite;
  private static bool block;
  private static bool for_drilling;

  public static void changeScene(bool stack, ItemInfo choiceGear)
  {
    Unit00443Scene.block = false;
    Unit00443Scene.for_drilling = false;
    Singleton<NGSceneManager>.GetInstance().changeScene("unit004_4_3", (stack ? 1 : 0) != 0, (object) choiceGear);
  }

  public static void changeSceneLimited(bool stack, ItemInfo choiceGear)
  {
    Unit00443Scene.block = true;
    Unit00443Scene.for_drilling = false;
    Singleton<NGSceneManager>.GetInstance().changeScene("unit004_4_3", (stack ? 1 : 0) != 0, (object) choiceGear);
  }

  public static void changeSceneForDrillingMaterial(bool stack, ItemInfo choiceGear)
  {
    Unit00443Scene.block = false;
    Unit00443Scene.for_drilling = true;
    Singleton<NGSceneManager>.GetInstance().changeScene("unit004_4_3", (stack ? 1 : 0) != 0, (object) choiceGear);
  }

  public override IEnumerator onInitSceneAsync()
  {
    Unit00443Scene unit00443Scene = this;
    if (Singleton<CommonRoot>.GetInstance().headerType == CommonRoot.HeaderType.Tower)
    {
      unit00443Scene.bgmFile = TowerUtil.BgmFile;
      unit00443Scene.bgmName = TowerUtil.BgmName;
    }
    else if (Singleton<NGGameDataManager>.GetInstance().IsSea)
    {
      IEnumerator e = unit00443Scene.SetSeaBgm();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
  }

  public IEnumerator onStartSceneAsync(ItemInfo choiceGear)
  {
    foreach (PlayerItem playerItem in SMManager.Get<PlayerItem[]>())
    {
      if (playerItem.id == choiceGear.itemID)
      {
        choiceGear = new ItemInfo(playerItem);
        break;
      }
    }
    this.targetGear_favorite = choiceGear.favorite;
    IEnumerator e = this.menu.Init(choiceGear, Unit00443Scene.block, Unit00443Scene.for_drilling);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public override IEnumerator onEndSceneAsync()
  {
    if (this.targetGear_favorite != this.menu.nowFavorite.gameObject.activeSelf)
    {
      Singleton<CommonRoot>.GetInstance().isLoading = true;
      IEnumerator e = this.menu.FavoriteAPI();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      Singleton<CommonRoot>.GetInstance().isLoading = false;
      this.targetGear_favorite = this.menu.nowFavorite.gameObject.activeSelf;
    }
    else
    {
      yield return (object) new WaitForSeconds(0.5f);
      this.menu.EndScene();
    }
  }

  private IEnumerator SetSeaBgm()
  {
    Unit00443Scene unit00443Scene = this;
    IEnumerator e = ServerTime.WaitSync();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    SeaHomeMap seaHomeMap = ((IEnumerable<SeaHomeMap>) MasterData.SeaHomeMapList).ActiveSeaHomeMap(ServerTime.NowAppTimeAddDelta());
    if (seaHomeMap != null && !string.IsNullOrEmpty(seaHomeMap.bgm_cuesheet_name) && !string.IsNullOrEmpty(seaHomeMap.bgm_cue_name))
    {
      unit00443Scene.bgmFile = seaHomeMap.bgm_cuesheet_name;
      unit00443Scene.bgmName = seaHomeMap.bgm_cue_name;
    }
  }
}
