﻿// Decompiled with JetBrains decompiler
// Type: DailyMission0271Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using UnityEngine;

public class DailyMission0271Scene : NGSceneBase
{
  [SerializeField]
  private DailyMission0271Menu menu;

  public static void ChangeScene0271(bool stack)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("dailymission027_1", stack, (object[]) Array.Empty<object>());
  }

  public IEnumerator onStartSceneAsync()
  {
    Debug.Log((object) "[daily mission] onStartSceneAsync");
    IEnumerator e = this.menu.Init();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public void onStartScene()
  {
    Singleton<CommonRoot>.GetInstance().isLoading = false;
    Singleton<CommonRoot>.GetInstance().loadingMode = 0;
  }
}
