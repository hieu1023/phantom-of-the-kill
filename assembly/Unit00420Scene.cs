﻿// Decompiled with JetBrains decompiler
// Type: Unit00420Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Unit00420Scene : NGSceneBase
{
  [SerializeField]
  private Unit00420Menu menu;

  public static void changeScene(
    bool stack,
    PlayerUnit basePlayerUnit,
    PlayerUnit[] materialPlayerUnits)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("unit004_20", (stack ? 1 : 0) != 0, (object) basePlayerUnit, (object) materialPlayerUnits);
  }

  public static void changeScene(
    bool stack,
    PlayerUnit basePlayerUnit,
    PlayerUnit[] materialPlayerUnits,
    System.Action exceptionBackScene)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("unit004_20", (stack ? 1 : 0) != 0, (object) basePlayerUnit, (object) materialPlayerUnits, (object) exceptionBackScene);
  }

  public IEnumerator onStartSceneAsync(
    PlayerUnit basePlayerUnit,
    PlayerUnit[] materialPlayerUnits)
  {
    this.menu.IsPush = false;
    PlayerUnit basePlayerUnit1 = ((IEnumerable<PlayerUnit>) SMManager.Get<PlayerUnit[]>()).FirstOrDefault<PlayerUnit>((Func<PlayerUnit, bool>) (x => x.id == basePlayerUnit.id));
    if (basePlayerUnit1 == (PlayerUnit) null)
      basePlayerUnit1 = basePlayerUnit;
    IEnumerator e = this.menu.Init(basePlayerUnit1, materialPlayerUnits);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public IEnumerator onStartSceneAsync(
    PlayerUnit basePlayerUnit,
    PlayerUnit[] materialPlayerUnits,
    System.Action exceptionBackScene)
  {
    this.menu.exceptionBackScene = exceptionBackScene;
    IEnumerator e = this.onStartSceneAsync(basePlayerUnit, materialPlayerUnits);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }
}
