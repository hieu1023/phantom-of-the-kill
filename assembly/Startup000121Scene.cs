﻿// Decompiled with JetBrains decompiler
// Type: Startup000121Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Startup000121Scene : NGSceneBase
{
  public Startup000121Menu menu;
  [SerializeField]
  private UIPanel newsPanel;

  public override IEnumerator onInitSceneAsync()
  {
    int depth = this.newsPanel.depth;
    this.newsPanel.depth = -100;
    this.menu.containers[0].SetActive(true);
    this.menu.containers[1].SetActive(true);
    this.menu.containers[2].SetActive(true);
    this.menu.isNews = false;
    this.menu.isEvent = false;
    this.menu.isFunction = false;
    OfficialInformationArticle[] officialInfos = Singleton<NGGameDataManager>.GetInstance().officialInfos;
    OfficialInformationArticle[] array = ((IEnumerable<OfficialInformationArticle>) officialInfos).Where<OfficialInformationArticle>((Func<OfficialInformationArticle, bool>) (x => x.category_id == 1)).ToArray<OfficialInformationArticle>();
    OfficialInformationArticle[] eventList = ((IEnumerable<OfficialInformationArticle>) officialInfos).Where<OfficialInformationArticle>((Func<OfficialInformationArticle, bool>) (x => x.category_id == 2)).ToArray<OfficialInformationArticle>();
    OfficialInformationArticle[] functionList = ((IEnumerable<OfficialInformationArticle>) officialInfos).Where<OfficialInformationArticle>((Func<OfficialInformationArticle, bool>) (x => x.category_id == 3)).ToArray<OfficialInformationArticle>();
    IEnumerator e = this.menu.InitNoticeList(array, 0);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    e = this.menu.InitNoticeList(eventList, 1);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    e = this.menu.InitNoticeList(functionList, 2);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.menu.SetScrolls();
    this.menu.ActivContainers(0);
    this.newsPanel.depth = depth;
  }

  public IEnumerator onStartSceneAsync()
  {
    IEnumerator e = this.menu.CheckNew();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }
}
