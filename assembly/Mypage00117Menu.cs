﻿// Decompiled with JetBrains decompiler
// Type: Mypage00117Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

public class Mypage00117Menu : BackButtonMenuBase
{
  [SerializeField]
  protected UILabel TxtPopuptitle;
  [SerializeField]
  protected UILabel TxtREADME;
  [SerializeField]
  protected UILabel TxtTitle;
  [SerializeField]
  private UITextList textList;
  private string text;

  public virtual void IbtnBack()
  {
    if (this.IsPushAndSet())
      return;
    this.backScene();
  }

  public virtual void IbtnPopupOk()
  {
    Debug.Log((object) "click default event IbtnPopupOk");
  }

  public IEnumerator InitSceneAsync(string text)
  {
    this.textList.Clear();
    this.textList.Add(text);
    this.textList.scrollValue = 0.0f;
    yield break;
  }

  public override void onBackButton()
  {
    this.IbtnBack();
  }
}
