﻿// Decompiled with JetBrains decompiler
// Type: NGBattleAIManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using AI.Logic;
using GameCore;
using System.Collections;

public class NGBattleAIManager : BattleManagerBase
{
  public AILogicBase ai;

  public override IEnumerator initialize(BattleInfo battleInfo, BE env_ = null)
  {
    // ISSUE: reference to a compiler-generated field
    int num = this.\u003C\u003E1__state;
    NGBattleAIManager ngBattleAiManager = this;
    if (num != 0)
      return false;
    // ISSUE: reference to a compiler-generated field
    this.\u003C\u003E1__state = -1;
    env_ = env_ ?? Singleton<NGBattleManager>.GetInstance().environment;
    BattleFuncs.createAsterNodeCache(env_.core);
    ngBattleAiManager.ai = ngBattleAiManager.gameObject.AddComponent<BasicAI>().aiLogic;
    return false;
  }

  public override IEnumerator cleanup()
  {
    this.ai = (AILogicBase) null;
    yield break;
  }

  public void clearCache()
  {
    if (!(this.ai is LispAILogic ai))
      return;
    ai.clearCache();
  }
}
