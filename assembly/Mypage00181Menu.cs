﻿// Decompiled with JetBrains decompiler
// Type: Mypage00181Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Mypage00181Menu : BackButtonMenuBase
{
  private List<Startup000121ScrollParts> partList = new List<Startup000121ScrollParts>();
  private List<GameObject> eventObjects = new List<GameObject>();
  private List<GameObject> eventForwards = new List<GameObject>();
  private float delay = 3f;
  private bool autoScrollFlag;
  [SerializeField]
  private NGWrapScrollParts infoEventScroll;
  [SerializeField]
  private NGxScroll2 InfoScroll;
  private GameObject newsPrefab;
  private GameObject funcPrefab;
  private GameObject eventPrefab;
  [SerializeField]
  private string nextSceneName;
  private UIScrollView uiEventScroll_;
  private UICenterOnChild uiCenter_;
  private int currentIndex_;

  public IEnumerator onInitMenuAsync()
  {
    this.autoScrollFlag = false;
    this.infoEventScroll.leftArrow.SetActive(false);
    this.infoEventScroll.rightArrow.SetActive(false);
    this.Initialize();
    IEnumerator e = this.LoadPrefab();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.CreateInformationList();
  }

  public IEnumerator onStartMenuAsync()
  {
    IEnumerator e = this.CheckNew();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    int count = this.eventObjects.Count;
    if (count > 0)
      this.infoEventScroll.content.gameObject.SetActive(true);
    if (count > 1)
      this.autoScrollFlag = true;
  }

  private void Initialize()
  {
    this.partList.Clear();
    this.eventObjects.Clear();
    this.eventForwards.Clear();
    this.infoEventScroll.destroyParts(false);
    this.InfoScroll.GridChildren().ToList<GameObject>().ForEach((System.Action<GameObject>) (x => UnityEngine.Object.Destroy((UnityEngine.Object) x)));
    this.InfoScroll.Reset();
  }

  private IEnumerator LoadPrefab()
  {
    Future<GameObject> fNews = Res.Prefabs.startup000_12_1.vscroll_732_2.Load<GameObject>();
    IEnumerator e = fNews.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.newsPrefab = fNews.Result;
    Future<GameObject> fFunc = Res.Prefabs.startup000_12_1.vscroll_732_3.Load<GameObject>();
    e = fFunc.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.funcPrefab = fFunc.Result;
    Future<GameObject> fEvent = Res.Prefabs.startup000_12_1.vscroll_732_4.Load<GameObject>();
    e = fEvent.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.eventPrefab = fEvent.Result;
  }

  private UIScrollView uiEventScroll
  {
    get
    {
      return !((UnityEngine.Object) this.uiEventScroll_ != (UnityEngine.Object) null) ? (this.uiEventScroll_ = this.infoEventScroll.scrollView.GetComponent<UIScrollView>()) : this.uiEventScroll_;
    }
  }

  private void CreateInformationList()
  {
    foreach (OfficialInformationArticle officialInfo in Singleton<NGGameDataManager>.GetInstance().officialInfos)
      this.CreateInformation(officialInfo);
    bool canDisp = this.eventObjects.Count > 1;
    this.uiEventScroll.enabled = canDisp;
    this.DispArrow(canDisp);
    this.infoEventScroll.ResetPosition();
    this.InfoScroll.ResolvePosition();
    if (!canDisp)
      return;
    this.uiCenter.CenterOn(this.eventObjects.First<GameObject>().transform);
    this.currentIndex_ = 0;
  }

  private void CreateInformation(OfficialInformationArticle article)
  {
    GameObject infomationPrefab = this.GetInfomationPrefab(article.category_id);
    GameObject gameObject1;
    if (article.category_id == 2)
    {
      gameObject1 = this.infoEventScroll.instantiateParts(infomationPrefab, false);
      gameObject1.transform.localPosition = new Vector3((float) (this.infoEventScroll.content.itemSize * this.eventObjects.Count), 0.0f, 0.0f);
      this.eventObjects.Add(gameObject1);
      GameObject gameObject2 = new GameObject("dir_forward");
      gameObject2.transform.parent = gameObject1.transform;
      gameObject2.transform.localScale = Vector3.one;
      gameObject2.transform.localRotation = Quaternion.identity;
      gameObject2.transform.localPosition = new Vector3((float) this.infoEventScroll.content.itemSize, 0.0f, 0.0f);
      gameObject2.SetActive(false);
      this.eventForwards.Add(gameObject2);
    }
    else
    {
      gameObject1 = UnityEngine.Object.Instantiate<GameObject>(infomationPrefab);
      this.InfoScroll.AddColumn1(gameObject1, 595, 120, false);
    }
    Startup000121ScrollParts component = gameObject1.GetComponent<Startup000121ScrollParts>();
    component.Set(article, this.nextSceneName);
    component.SetData(article, (NGMenuBase) this);
    this.partList.Add(component);
  }

  private void DispArrow(bool canDisp)
  {
    this.infoEventScroll.leftArrow.SetActive(canDisp);
    this.infoEventScroll.rightArrow.SetActive(canDisp);
    this.infoEventScroll.ForceArrowDisplay(canDisp);
  }

  private GameObject GetInfomationPrefab(int category_id)
  {
    switch (category_id)
    {
      case 1:
        return this.newsPrefab;
      case 2:
        return this.eventPrefab;
      case 3:
        return this.funcPrefab;
      default:
        return (GameObject) null;
    }
  }

  public IEnumerator CheckNew()
  {
    IEnumerator e = ServerTime.WaitSync();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.partList.ForEach((System.Action<Startup000121ScrollParts>) (x => x.SetNewSprite()));
  }

  private UICenterOnChild uiCenter
  {
    get
    {
      return !((UnityEngine.Object) this.uiCenter_ != (UnityEngine.Object) null) ? (this.uiCenter_ = this.infoEventScroll.content.GetComponent<UICenterOnChild>()) : this.uiCenter_;
    }
  }

  protected override void Update()
  {
    base.Update();
    if (!this.autoScrollFlag)
      return;
    if (!this.uiEventScroll_.isDragging)
      this.delay -= Time.deltaTime;
    int? centerIndex = this.getCenterIndex();
    int? nullable;
    if ((double) this.delay < 0.0)
    {
      if (centerIndex.HasValue)
      {
        int currentIndex = this.currentIndex_;
        nullable = centerIndex;
        int valueOrDefault = nullable.GetValueOrDefault();
        if (!(currentIndex == valueOrDefault & nullable.HasValue))
          goto label_10;
      }
      int num = this.currentIndex_ + 1;
      if (num >= this.eventObjects.Count)
        num = 0;
      this.uiCenter.CenterOn(this.eventForwards[this.currentIndex_].transform);
      this.currentIndex_ = num;
      this.delay = 5f;
      return;
    }
label_10:
    if (!centerIndex.HasValue)
      return;
    int currentIndex1 = this.currentIndex_;
    nullable = centerIndex;
    int valueOrDefault1 = nullable.GetValueOrDefault();
    if (currentIndex1 == valueOrDefault1 & nullable.HasValue)
      return;
    this.currentIndex_ = centerIndex.Value;
    if ((double) this.delay >= 3.0)
      return;
    this.delay = 3f;
  }

  private int? getCenterIndex()
  {
    int? nullable = new int?();
    GameObject selgo = this.uiCenter.centeredObject;
    if ((UnityEngine.Object) selgo != (UnityEngine.Object) null)
    {
      int index = this.eventObjects.FindIndex((Predicate<GameObject>) (x => (UnityEngine.Object) x == (UnityEngine.Object) selgo));
      if (index >= 0)
        nullable = new int?(index);
    }
    return nullable;
  }

  public virtual void IbtnBack()
  {
    if (this.IsPushAndSet() || Singleton<NGGameDataManager>.GetInstance().InfoOrLoginBonusJump())
      return;
    Singleton<NGSceneManager>.GetInstance().destoryNonStackScenes();
    Singleton<NGSceneManager>.GetInstance().destroyCurrentScene();
    if (Singleton<NGGameDataManager>.GetInstance().IsSea)
    {
      Singleton<CommonRoot>.GetInstance().isLoading = true;
      Sea030HomeScene.ChangeScene(false, false);
    }
    else
      MypageScene.ChangeScene(false, false, false);
  }

  public override void onBackButton()
  {
    this.IbtnBack();
  }

  public virtual void IbtnEvent()
  {
    Debug.Log((object) "click default event IbtnEvent");
  }

  public virtual void IbtnFunctionadd()
  {
    Debug.Log((object) "click default event IbtnFunctionadd");
  }

  public virtual void IbtnNews()
  {
    Debug.Log((object) "click default event IbtnNews");
  }

  public virtual void IbtnNewslist()
  {
    Debug.Log((object) "click default event IbtnNewslist");
  }

  protected virtual void Foreground()
  {
    Debug.Log((object) "click default event Foreground");
  }

  protected virtual void Foreground2()
  {
    Debug.Log((object) "click default event Foreground2");
  }

  protected virtual void Foreground3()
  {
    Debug.Log((object) "click default event Foreground3");
  }

  protected virtual void IbtnList()
  {
    Debug.Log((object) "click default event IbtnList");
  }

  protected virtual void IbtnNewslist2()
  {
    Debug.Log((object) "click default event IbtnNewslist2");
  }

  protected virtual void VScrollBar()
  {
    Debug.Log((object) "click default event VScrollBar");
  }

  protected virtual void VScrollBar2()
  {
    Debug.Log((object) "click default event VScrollBar2");
  }

  protected virtual void VScrollBar3()
  {
    Debug.Log((object) "click default event VScrollBar3");
  }
}
