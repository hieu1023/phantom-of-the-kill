﻿// Decompiled with JetBrains decompiler
// Type: SM_PlayerFriendExtension
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;
using System;
using System.Collections.Generic;
using UniLinq;

public static class SM_PlayerFriendExtension
{
  public static PlayerFriend[] Friends(this IEnumerable<PlayerFriend> self)
  {
    return self.Where<PlayerFriend>((Func<PlayerFriend, bool>) (x => !x.application)).ToArray<PlayerFriend>();
  }

  public static PlayerFriend[] SentFriendApplications(
    this IEnumerable<PlayerFriend> self)
  {
    return self.Where<PlayerFriend>((Func<PlayerFriend, bool>) (x => x.application && x.sent_player_id == Player.Current.id)).ToArray<PlayerFriend>();
  }

  public static PlayerFriend[] ReceivedFriendApplications(
    this IEnumerable<PlayerFriend> self)
  {
    return self.Where<PlayerFriend>((Func<PlayerFriend, bool>) (x => x.application && x.sent_player_id != Player.Current.id)).ToArray<PlayerFriend>();
  }
}
