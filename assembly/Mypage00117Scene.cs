﻿// Decompiled with JetBrains decompiler
// Type: Mypage00117Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

public class Mypage00117Scene : NGSceneBase
{
  [SerializeField]
  private Mypage00117Menu menu;
  private Startup0008Data data;

  public IEnumerator onStartSceneAsync()
  {
    this.data = new Startup0008Data();
    IEnumerator e = this.data.Initialize();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    e = this.menu.InitSceneAsync(this.data.agreement);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }
}
