﻿// Decompiled with JetBrains decompiler
// Type: Raid032BattleResultDamageRewardPopup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;
using System.Collections;
using UnityEngine;

public class Raid032BattleResultDamageRewardPopup : MonoBehaviour
{
  [SerializeField]
  private UILabel percentageLabel;
  [SerializeField]
  private Raid032BattleResultRewardItem rewardItem;

  public IEnumerator InitAsync(RaidDamageReward reward)
  {
    this.percentageLabel.SetTextLocalize(string.Format("{0}％", (object) (reward.damage_ratio / 100)));
    yield return (object) this.rewardItem.InitAsync(reward);
  }
}
