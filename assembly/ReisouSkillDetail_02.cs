﻿// Decompiled with JetBrains decompiler
// Type: ReisouSkillDetail_02
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReisouSkillDetail_02 : MonoBehaviour
{
  [SerializeField]
  protected UIDragScrollView uiDragScrollView;
  [SerializeField]
  protected UILabel[] txtStatus;

  public IEnumerator Init(UIScrollView scrollView, List<string> paramTextList)
  {
    this.uiDragScrollView.scrollView = scrollView;
    int length = this.txtStatus.Length;
    for (int index = 0; index < length; ++index)
      this.txtStatus[index].SetTextLocalize("");
    for (int index = 0; index < paramTextList.Count; ++index)
      this.txtStatus[index].SetTextLocalize(paramTextList[index]);
    yield break;
  }
}
