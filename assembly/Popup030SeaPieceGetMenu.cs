﻿// Decompiled with JetBrains decompiler
// Type: Popup030SeaPieceGetMenu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Popup030SeaPieceGetMenu : ResultMenuBase
{
  private List<PieceGetResult> pieceGetResultList = new List<PieceGetResult>();
  private GameObject popupObject;

  public override IEnumerator Init(BattleInfo info, BattleEnd result)
  {
    BattleEndGet_sea_album_piece_counts[] albumPieceCounts1 = result.get_sea_album_piece_counts;
    MasterDataTable.SeaAlbum[] seaAlbumList = MasterData.SeaAlbumList;
    SeaAlbumPiece[] seaAlbumPieceList = MasterData.SeaAlbumPieceList;
    foreach (BattleEndGet_sea_album_piece_counts albumPieceCounts2 in albumPieceCounts1)
    {
      BattleEndGet_sea_album_piece_counts targetPiece = albumPieceCounts2;
      SeaAlbumPiece piece = ((IEnumerable<SeaAlbumPiece>) seaAlbumPieceList).FirstOrDefault<SeaAlbumPiece>((Func<SeaAlbumPiece, bool>) (x => x.ID == targetPiece.album_piece_id));
      if (piece != null)
      {
        MasterDataTable.SeaAlbum seaAlbum = ((IEnumerable<MasterDataTable.SeaAlbum>) seaAlbumList).FirstOrDefault<MasterDataTable.SeaAlbum>((Func<MasterDataTable.SeaAlbum, bool>) (x => x.ID == piece.album_id));
        if (seaAlbum != null)
          this.pieceGetResultList.Add(new PieceGetResult(seaAlbum.name, piece.name, targetPiece.count, piece.same_character_id));
      }
    }
    IEnumerator e = this.LoadResources();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  private IEnumerator LoadResources()
  {
    Future<GameObject> handler = Res.Prefabs.popup.popup_030_sea_piece_get__anim_fade.Load<GameObject>();
    IEnumerator e = handler.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.popupObject = handler.Result.Clone(Singleton<CommonRoot>.GetInstance().LoadTmpObj.transform);
    this.popupObject.SetActive(false);
  }

  public override IEnumerator Run()
  {
    Popup030SeaPieceGetMenu popup030SeaPieceGetMenu = this;
    popup030SeaPieceGetMenu.popupObject.SetParent(popup030SeaPieceGetMenu.gameObject);
    popup030SeaPieceGetMenu.popupObject.SetActive(true);
    Popup030SeaPieceGetSetting component = popup030SeaPieceGetMenu.popupObject.GetComponent<Popup030SeaPieceGetSetting>();
    bool toNext = false;
    IEnumerator e = component.Init(popup030SeaPieceGetMenu.pieceGetResultList, (System.Action) (() => toNext = true));
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Singleton<NGSoundManager>.GetInstance().playSE("SE_1034", false, 0.0f, -1);
    while (!toNext)
      yield return (object) null;
    UnityEngine.Object.DestroyObject((UnityEngine.Object) popup030SeaPieceGetMenu.popupObject);
  }
}
