﻿// Decompiled with JetBrains decompiler
// Type: RepairSoundEffect
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class RepairSoundEffect : MonoBehaviour
{
  public bool mLost = true;
  public EffectController ef;
  public bool result;

  public void OnAnimationEnd()
  {
    this.ef.OnAnimationEnd();
  }

  public void OnSE0018()
  {
    Debug.Log((object) "OnSE0018()");
    Singleton<NGSoundManager>.GetInstance().playSE("SE_0018", false, 0.0f, -1);
  }

  public void OnSE0019()
  {
    Debug.Log((object) "OnSE0019()");
    if (!this.mLost)
      return;
    Singleton<NGSoundManager>.GetInstance().playSE("SE_0019", false, 0.0f, -1);
  }

  public void OnSE0020()
  {
    this.result = true;
    Debug.Log((object) "OnSE0020()");
    Singleton<NGSoundManager>.GetInstance().playSE("SE_0020", false, 0.0f, -1);
  }

  public void SetLost(bool lost)
  {
    this.mLost = lost;
  }
}
