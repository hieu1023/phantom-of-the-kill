﻿// Decompiled with JetBrains decompiler
// Type: PurchaseBehavior
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using Gsc.Purchase;
using System.Collections;
using UnityEngine;

public class PurchaseBehavior : Singleton<PurchaseBehavior>
{
  public static PurchaseFlow actualFlow;

  public static bool IsBattleNow { get; set; }

  public static bool IsOpen { get; set; }

  public static int UsedCoinInBattleHere
  {
    get
    {
      if (PurchaseBehavior.IsBattleNow)
      {
        NGBattleManager instance = Singleton<NGBattleManager>.GetInstance();
        if ((UnityEngine.Object) instance != (UnityEngine.Object) null && instance.environment != null)
          return instance.environment.core.continueCount;
      }
      return 0;
    }
  }

  protected override void Initialize()
  {
  }

  public static IEnumerator OpenItemList(bool isBattle)
  {
    PurchaseBehavior.IsOpen = false;
    PurchaseBehavior.IsBattleNow = isBattle;
    Singleton<CommonRoot>.GetInstance().loadingMode = 1;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    Future<WebAPI.Response.CoinbonusHistory> handler = WebAPI.CoinbonusHistory((System.Action<WebAPI.Response.UserError>) null);
    IEnumerator e = handler.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Future<GameObject> prefab = Res.Prefabs.popup.popup_007_9__anim_popup01.Load<GameObject>();
    e = prefab.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    PurchaseBehavior.IsOpen = true;
    e = PurchaseBehavior.PopupOpen(prefab.Result).GetComponent<Shop0079Menu>().Init(handler.Result);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Singleton<CommonRoot>.GetInstance().isLoading = false;
    Singleton<CommonRoot>.GetInstance().loadingMode = 0;
  }

  public static GameObject PopupOpen(GameObject prefab)
  {
    return PurchaseBehavior.IsBattleNow ? Singleton<NGBattleManager>.GetInstance().popupOpen(prefab, false, (EventDelegate) null, false, false, false, true, false) : Singleton<PopupManager>.GetInstance().open(prefab, false, false, false, true, false, false, "SE_1006");
  }

  public static void PopupDismiss(bool withoutAnim = false)
  {
    if (PurchaseBehavior.IsBattleNow)
      Singleton<NGBattleManager>.GetInstance().popupDismiss(false, withoutAnim);
    else if (withoutAnim)
      Singleton<PopupManager>.GetInstance().dismissWithoutAnim(false, true);
    else
      Singleton<PopupManager>.GetInstance().dismiss(false);
  }

  public static void PopupAllDismiss()
  {
    if (PurchaseBehavior.IsBattleNow)
      Singleton<NGBattleManager>.GetInstance().popupCloseAll(false);
    else
      Singleton<PopupManager>.GetInstance().closeAll(false);
  }

  public static void ShowPopupYesNo(string title, string message, System.Action callback = null)
  {
    ModalWindow.ShowYesNo(title, message, (System.Action) (() =>
    {
      PurchaseBehaviorLoadingLayer.Disable();
      if (callback == null)
        return;
      callback();
    }), (System.Action) (() => PurchaseBehaviorLoadingLayer.Disable()));
  }

  public static void ShowPopupWithMessage(string title, string message, System.Action callback = null)
  {
    PopupCommon component = PurchaseBehavior.PopupOpen(PopupCommon.LoadPrefab()).GetComponent<PopupCommon>();
    component.Init(title, message, (System.Action) null);
    component.OK.onClick.Clear();
    EventDelegate.Add(component.OK.onClick, (EventDelegate.Callback) (() => PurchaseBehavior.PopupDismiss(false)));
    if (callback == null)
      return;
    EventDelegate.Add(component.OK.onClick, (EventDelegate.Callback) (() => callback()));
  }

  public static void SendAge(int year, int month, int day)
  {
    PurchaseBehavior.actualFlow.InputBirthday(year, month, day);
  }

  public static void ShowInputBirthday(bool isYes)
  {
    if (isYes)
    {
      Singleton<PurchaseBehavior>.GetInstance().StartCoroutine(Singleton<PurchaseBehavior>.GetInstance().ShowPopupInputBirthday());
    }
    else
    {
      PurchaseBehaviorLoadingLayer.Disable();
      PurchaseBehavior.PopupDismiss(false);
    }
  }

  private IEnumerator ShowPopupInputBirthday()
  {
    Future<GameObject> prefab = Res.Prefabs.popup.popup_999_8_1__anim_popup01.Load<GameObject>();
    IEnumerator e = prefab.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    GameObject gameObject = PurchaseBehavior.PopupOpen(prefab.Result);
    gameObject.transform.parent.GetComponent<UIPanel>().depth = 110;
    gameObject.GetComponent<Shop99981Menu>().SetOnCancel((System.Action) (() => PurchaseBehaviorLoadingLayer.Disable()));
  }
}
