﻿// Decompiled with JetBrains decompiler
// Type: Mypage00181EventScrollParts
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using System;
using UnityEngine;

public class Mypage00181EventScrollParts : MonoBehaviour
{
  [SerializeField]
  private UI2DSprite sprite;
  [SerializeField]
  private GameObject newSprite;
  [SerializeField]
  private UILabel date;
  [SerializeField]
  private UILabel time;
  private InformationInformation master;
  private DateTime endDate;

  public void IbtnNewslist()
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("mypage001_8_2", false, (object) this.master);
  }
}
