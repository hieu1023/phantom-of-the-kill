﻿// Decompiled with JetBrains decompiler
// Type: Quest0025Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;
using System.Collections;
using UnityEngine;

public class Quest0025Scene : NGSceneBase
{
  public Quest0025Menu menu;

  public static void changeScene0025(bool stack, Quest0025Scene.Quest0025Param param)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("quest002_5", (stack ? 1 : 0) != 0, (object) param);
  }

  public IEnumerator onStartSceneAsync(Quest0025Scene.Quest0025Param param)
  {
    Quest0025Scene quest0025Scene = this;
    quest0025Scene.tweens = (UITweener[]) null;
    quest0025Scene.IsPush = false;
    QuestBG backgroundComponent = Singleton<CommonRoot>.GetInstance().getBackgroundComponent<QuestBG>();
    if ((Object) backgroundComponent != (Object) null)
      backgroundComponent.currentPos = QuestBG.QuestPosition.Chapter;
    BGChange bgchange = quest0025Scene.GetComponent<BGChange>();
    bool resetBG = false;
    IEnumerator e;
    if ((Object) backgroundComponent == (Object) null)
    {
      resetBG = true;
      e = bgchange.BGprefabCreate(true, param.L, 0, 1);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    if (param.createBG | resetBG)
    {
      e = quest0025Scene.GetComponent<BGChange>().SetChapterBg((Quest00240723Menu.StoryMode) param.XL);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    PlayerStoryQuestS[] StoryData = SMManager.Get<PlayerStoryQuestS[]>();
    bgchange.CrossToXL(false, 1f);
    Singleton<CommonRoot>.GetInstance().getBackgroundComponent<QuestBG>().Toggle(QuestBG.AnimApply.LMainPostion);
    e = quest0025Scene.menu.Init(StoryData, param.XL, param.L, param.Hard, param.reStart);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public class Quest0025Param
  {
    public int XL;
    public int L;
    public bool Hard;
    public bool reStart;
    public bool createBG;

    public Quest0025Param(int xl, int l, bool hard, bool restart = false, bool createBG = true)
    {
      this.XL = xl;
      this.L = l;
      this.Hard = hard;
      this.reStart = restart;
      this.createBG = createBG;
    }
  }
}
