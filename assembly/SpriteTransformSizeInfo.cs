﻿// Decompiled with JetBrains decompiler
// Type: SpriteTransformSizeInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

public class SpriteTransformSizeInfo
{
  public float PositionX;
  public float PositionY;
  public int SizeY;
  public int SizeX;

  public SpriteTransformSizeInfo(float positionX, float positionY, int SizeY, int SizeX)
  {
    this.PositionX = positionX;
    this.PositionY = positionY;
    this.SizeY = SizeY;
    this.SizeX = SizeX;
  }
}
