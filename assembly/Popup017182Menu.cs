﻿// Decompiled with JetBrains decompiler
// Type: Popup017182Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using UnityEngine;

public class Popup017182Menu : BattleBackButtonMenuBase
{
  [SerializeField]
  private UILabel label;

  public void Awake()
  {
    this.label.SetTextLocalize(Consts.GetInstance().BATTLE_SUSPEND_POPUP);
  }

  public void IbtnYes()
  {
    this.battleManager.getController<BattleAIController>().stopAIAction();
    this.battleManager.getManager<BattleTimeManager>().setPhaseState(BL.Phase.suspend, false);
    this.battleManager.popupCloseAll(false);
  }

  public override void onBackButton()
  {
    this.IbtnNo();
  }

  public void IbtnNo()
  {
    this.battleManager.popupDismiss(false, false);
  }
}
