﻿// Decompiled with JetBrains decompiler
// Type: Story0092Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;
using System.Collections;
using UnityEngine;

public class Story0092Scene : NGSceneBase
{
  public Story0092Menu menu;
  public UILabel TxtTitle;
  [SerializeField]
  private NGxScroll ScrollContainer;

  public IEnumerator onStartSceneAsync(PlayerStoryQuestS quest, int XL)
  {
    this.TxtTitle.SetTextLocalize(quest.quest_story_s.quest_l.short_name + "　" + quest.quest_story_s.quest_l.name);
    PlayerStoryQuestS[] self = SMManager.Get<PlayerStoryQuestS[]>();
    this.ScrollContainer.Clear();
    IEnumerator e = this.menu.InitChapterButton(quest, self.DisplayScrollM(quest.quest_story_s.quest_xl.ID, quest.quest_story_s.quest_l.ID), XL);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.ScrollContainer.ResolvePosition();
  }
}
