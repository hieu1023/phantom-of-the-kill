﻿// Decompiled with JetBrains decompiler
// Type: Mypage00181Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

public class Mypage00181Scene : NGSceneBase
{
  [SerializeField]
  private Mypage00181Menu menu;

  public Mypage00181Menu Menu
  {
    get
    {
      return this.menu;
    }
    set
    {
      this.menu = value;
    }
  }

  public override IEnumerator onInitSceneAsync()
  {
    IEnumerator e = this.Menu.onInitMenuAsync();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public IEnumerator onStartSceneAsync()
  {
    IEnumerator e = this.Menu.onStartMenuAsync();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public void onStartScene()
  {
    Singleton<CommonRoot>.GetInstance().isLoading = false;
  }
}
