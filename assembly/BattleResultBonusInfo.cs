﻿// Decompiled with JetBrains decompiler
// Type: BattleResultBonusInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

public class BattleResultBonusInfo
{
  public int rewardID;
  public MasterDataTable.CommonRewardType rewardType;
  public string rewardMessage;
  public bool isMessage;

  public BattleResultBonusInfo(
    int reward_id,
    MasterDataTable.CommonRewardType type,
    string message,
    bool isMessage = true)
  {
    this.rewardID = reward_id;
    this.rewardType = type;
    this.rewardMessage = message;
    this.isMessage = isMessage;
  }
}
