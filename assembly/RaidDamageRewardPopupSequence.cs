﻿// Decompiled with JetBrains decompiler
// Type: RaidDamageRewardPopupSequence
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class RaidDamageRewardPopupSequence
{
  private GameObject rewardPopupPrefab;
  private List<RaidDamageReward> rewardList;

  public IEnumerator Init(RaidDamageReward[] rewardList)
  {
    this.rewardList = ((IEnumerable<RaidDamageReward>) rewardList).ToList<RaidDamageReward>();
    yield return (object) this.LoadPopupPrefab();
  }

  public IEnumerator Run()
  {
    foreach (RaidDamageReward reward in this.rewardList)
    {
      yield return (object) this.ShowDamageRewardPopup(reward);
      yield return (object) new WaitForSeconds(0.5f);
    }
  }

  private IEnumerator LoadPopupPrefab()
  {
    Future<GameObject> future = Singleton<ResourceManager>.GetInstance().Load<GameObject>("Prefabs/raid0032_result/dir_RaidBoss_result_reward", 1f);
    IEnumerator e = future.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if ((Object) future.Result == (Object) null)
      Debug.LogError((object) "failed to load dir_RaidBoss_result_reward.prefab");
    else
      this.rewardPopupPrefab = future.Result;
  }

  private IEnumerator ShowDamageRewardPopup(RaidDamageReward reward)
  {
    GameObject obj = Singleton<PopupManager>.GetInstance().open(this.rewardPopupPrefab, false, false, false, true, false, false, "SE_1006");
    IEnumerator e = obj.GetComponent<Raid032BattleResultDamageRewardPopup>().InitAsync(reward);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Singleton<NGSoundManager>.GetInstance().playSE("SE_1034", false, 0.0f, -1);
    bool toNext = false;
    RaidUtils.CreateTouchObject((EventDelegate.Callback) (() => toNext = true), obj.transform);
    while (!toNext)
      yield return (object) null;
    Singleton<PopupManager>.GetInstance().onDismiss();
  }
}
