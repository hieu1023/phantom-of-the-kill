﻿// Decompiled with JetBrains decompiler
// Type: Bugu00510Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using System;
using System.Collections;
using System.Collections.Generic;

public class Bugu00510Scene : NGSceneBase
{
  private Bugu00510Menu _menu;
  private static GearGear selectedGear;
  private static List<InventoryItem> selectedItems;
  private static List<GearCombineRecipe> selectedGearRecipes;

  private Bugu00510Menu menu
  {
    get
    {
      if ((UnityEngine.Object) this._menu == (UnityEngine.Object) null)
        this._menu = this.menuBase as Bugu00510Menu;
      return this._menu;
    }
  }

  public static void changeSceneRecipe(bool isStack)
  {
    Bugu00510Scene.selectedItems = (List<InventoryItem>) null;
    Singleton<NGSceneManager>.GetInstance().changeScene("bugu005_10", isStack, (object[]) Array.Empty<object>());
  }

  public static void changeSceneMaterialRecipe(
    bool isStack,
    GearGear sGear,
    List<InventoryItem> sItems,
    List<GearCombineRecipe> sGearRecipes)
  {
    Bugu00510Scene.selectedGear = sGear;
    Bugu00510Scene.selectedItems = sItems;
    Bugu00510Scene.selectedGearRecipes = sGearRecipes;
    Singleton<NGSceneManager>.GetInstance().changeScene("bugu005_10", isStack, (object[]) Array.Empty<object>());
  }

  public override IEnumerator onInitSceneAsync()
  {
    if ((UnityEngine.Object) this.menu != (UnityEngine.Object) null)
    {
      IEnumerator e = this.menu.InitAsync();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
  }

  public IEnumerator onStartSceneAsync()
  {
    if ((UnityEngine.Object) this.menu != (UnityEngine.Object) null)
    {
      IEnumerator e;
      if (Bugu00510Scene.selectedItems != null)
      {
        e = this.menu.StartAsync(Bugu00510Scene.selectedGear, Bugu00510Scene.selectedItems, Bugu00510Scene.selectedGearRecipes);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
      }
      else
      {
        e = this.menu.StartAsync(false);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
      }
      Bugu00510Scene.selectedItems = (List<InventoryItem>) null;
    }
  }

  public void onStartScene()
  {
  }
}
