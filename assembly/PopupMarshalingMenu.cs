﻿// Decompiled with JetBrains decompiler
// Type: PopupMarshalingMenu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class PopupMarshalingMenu : Quest0028Menu
{
  private bool isSea_;
  private object quest_;
  private System.Action onOK_;
  private System.Action onNG_;
  private GameObject prefabHelperSelector_;
  private PlayerHelper helper_;
  private System.Action<PlayerHelper> eventSetHelper_;

  private PlayerStoryQuestS storyQuest_
  {
    get
    {
      return this.quest_ as PlayerStoryQuestS;
    }
  }

  private PlayerExtraQuestS extraQuest_
  {
    get
    {
      return this.quest_ as PlayerExtraQuestS;
    }
  }

  private PlayerQuestSConverter charaQuest_
  {
    get
    {
      return this.quest_ as PlayerQuestSConverter;
    }
  }

  private PlayerSeaQuestS seaQuest_
  {
    get
    {
      return this.quest_ as PlayerSeaQuestS;
    }
  }

  public static IEnumerator show(object questS, System.Action onOK, System.Action onNG)
  {
    Singleton<CommonRoot>.GetInstance().loadingMode = 0;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    Future<GameObject> ldMenu = new ResourceObject("Prefabs/battle/popup_Re_sortie_preparation__anim_popup01").Load<GameObject>();
    IEnumerator e = ldMenu.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if ((UnityEngine.Object) ldMenu.Result == (UnityEngine.Object) null)
    {
      onNG();
    }
    else
    {
      Future<GameObject> ldHelperSelector = PopupHelperSelector.loadPrefab();
      e = ldHelperSelector.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      if ((UnityEngine.Object) ldHelperSelector.Result == (UnityEngine.Object) null)
      {
        onNG();
      }
      else
      {
        Singleton<CommonRoot>.GetInstance().isLoading = false;
        bool toNext = false;
        PlayerHelper helper = (PlayerHelper) null;
        string curScene = Singleton<NGSceneManager>.GetInstance().sceneName;
        do
        {
          while (curScene != Singleton<NGSceneManager>.GetInstance().sceneName)
            yield return (object) null;
          GameObject goMenu = Singleton<PopupManager>.GetInstance().open(ldMenu.Result, false, false, false, true, true, true, "SE_1006");
          goMenu.GetComponent<PopupMarshalingMenu>();
          e = goMenu.GetComponent<PopupMarshalingMenu>().initialize(ldHelperSelector.Result, questS, helper, (System.Action) (() =>
          {
            toNext = true;
            onOK();
          }), (System.Action) (() =>
          {
            toNext = true;
            onNG();
          }), (System.Action<PlayerHelper>) (hp => helper = hp));
          while (e.MoveNext())
            yield return e.Current;
          e = (IEnumerator) null;
          Singleton<PopupManager>.GetInstance().startOpenAnime(goMenu, false);
          while (Singleton<PopupManager>.GetInstance().isOpen)
            yield return (object) null;
          while (Singleton<CommonRoot>.GetInstance().isLoading)
            yield return (object) null;
          goMenu = (GameObject) null;
        }
        while (!toNext);
      }
    }
  }

  private IEnumerator initialize(
    GameObject prefab,
    object questS,
    PlayerHelper helper,
    System.Action onOK,
    System.Action onNG,
    System.Action<PlayerHelper> eventSetHelper)
  {
    PopupMarshalingMenu popupMarshalingMenu = this;
    popupMarshalingMenu.IsPush = true;
    popupMarshalingMenu.isSea_ = Singleton<NGGameDataManager>.GetInstance().IsSea && questS is PlayerSeaQuestS;
    popupMarshalingMenu.quest_ = questS;
    popupMarshalingMenu.prefabHelperSelector_ = prefab;
    PlayerItem[] playerItemArray = SMManager.Get<PlayerItem[]>().AllBattleSupplies();
    PlayerDeck[] playerDecks = popupMarshalingMenu.isSea_ ? PlayerSeaDeck.convertDeckData(SMManager.Get<PlayerSeaDeck[]>()) : SMManager.Get<PlayerDeck[]>();
    popupMarshalingMenu.helper_ = helper;
    popupMarshalingMenu.eventSetHelper_ = eventSetHelper;
    popupMarshalingMenu.setEventSetHelper(new System.Action<PlayerHelper>(popupMarshalingMenu.onSetHelper));
    IEnumerator e = popupMarshalingMenu.InitPlayerDecks(playerDecks, ((IEnumerable<PlayerItem>) playerItemArray).ToList<PlayerItem>(), popupMarshalingMenu.helper_, popupMarshalingMenu.storyQuest_, popupMarshalingMenu.extraQuest_, (PlayerCharacterQuestS) null, popupMarshalingMenu.charaQuest_, popupMarshalingMenu.seaQuest_, false);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    popupMarshalingMenu.onOK_ = onOK;
    popupMarshalingMenu.onNG_ = onNG;
    popupMarshalingMenu.IsPush = false;
  }

  private void onSetHelper(PlayerHelper helper)
  {
    if (this.eventSetHelper_ != null)
      this.eventSetHelper_(helper);
    this.helper_ = helper;
    this.StartCoroutine(this.doResetHelper());
  }

  private IEnumerator doResetHelper()
  {
    PopupMarshalingMenu popupMarshalingMenu = this;
    popupMarshalingMenu.IsPush = true;
    yield return (object) null;
    IEnumerator e = popupMarshalingMenu.resetHelper(popupMarshalingMenu.helper_);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    popupMarshalingMenu.IsPush = false;
  }

  protected override void preInitializeIndicator(Quest0028Indicator indicator)
  {
    indicator.disableUnitDetail = true;
  }

  public override void IbtnSelectFriend()
  {
    if ((UnityEngine.Object) this.prefabHelperSelector_ == (UnityEngine.Object) null || this.IsPushAndSet())
      return;
    PopupHelperSelector.open(this.prefabHelperSelector_, this.quest_, new System.Action<PlayerHelper>(this.onSetHelper), new System.Action(this.onCloseHelperSelect));
  }

  private void onCloseHelperSelect()
  {
    this.IsPush = false;
  }

  public override void onBackButton()
  {
    this.onClickedNG();
  }

  public override void IbtnSortie()
  {
    if (this.IsPushAndSet())
      return;
    if (this.QuestStart())
    {
      if (this.onOK_ != null)
        this.onOK_();
      Singleton<PopupManager>.GetInstance().dismiss(false);
    }
    else
      this.IsPush = false;
  }

  protected override void onChangedSceneRepair(bool isSea)
  {
    Singleton<PopupManager>.GetInstance().dismiss(false);
  }

  public void onClickedNG()
  {
    if (this.IsPushAndSet())
      return;
    if (this.onNG_ != null)
      this.onNG_();
    Singleton<PopupManager>.GetInstance().dismiss(false);
  }

  private void OnDestroy()
  {
    this.SaveSetting();
  }
}
