﻿// Decompiled with JetBrains decompiler
// Type: PopupUnityValueDetailStageItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using UnityEngine;

public class PopupUnityValueDetailStageItem : MonoBehaviour
{
  [SerializeField]
  private UILabel eventQuestTitle;
  [SerializeField]
  private UILabel eventQuestSubTitle;
  [SerializeField]
  private UILabel storyQuestChapterTitle;
  [SerializeField]
  private UILabel storyQuestStageTitle;
  [SerializeField]
  private UILabel AP;
  [SerializeField]
  private GameObject questNotOpened;
  [SerializeField]
  private GameObject disabledCover;
  private QuestConverterData stageData;
  private System.Action beforeChangeSceneAction;
  private bool isAvailable;

  public void Initialize(
    QuestConverterData stageData,
    System.Action beforeChangeSceneAction,
    bool isFromUnityPopupStageList)
  {
    this.stageData = stageData;
    this.beforeChangeSceneAction = beforeChangeSceneAction;
    if (stageData.type == CommonQuestType.Story)
    {
      QuestStoryL questStoryL = (QuestStoryL) null;
      string str = string.Empty;
      if (MasterData.QuestStoryL.TryGetValue(stageData.id_L, out questStoryL))
        str = questStoryL.name;
      this.storyQuestChapterTitle.text = str;
      this.storyQuestStageTitle.text = stageData.title_S;
    }
    else if (stageData.type == CommonQuestType.Extra)
    {
      this.eventQuestTitle.text = stageData.title_M;
      this.eventQuestSubTitle.text = stageData.title_S;
    }
    this.AP.text = stageData.lost_ap.ToString();
    if (stageData.canPlay && !isFromUnityPopupStageList)
    {
      this.isAvailable = true;
      this.questNotOpened.SetActive(false);
      this.disabledCover.SetActive(false);
    }
    else
    {
      this.isAvailable = false;
      this.questNotOpened.SetActive(!stageData.canPlay);
      this.disabledCover.SetActive(true);
    }
  }

  public void Initialize(string title1, string title2, int ap, CommonQuestType type)
  {
    this.isAvailable = false;
    this.questNotOpened.SetActive(true);
    this.disabledCover.SetActive(true);
    switch (type)
    {
      case CommonQuestType.Story:
        this.storyQuestChapterTitle.text = title1;
        this.storyQuestStageTitle.text = title2;
        break;
      case CommonQuestType.Extra:
        this.eventQuestTitle.text = title1;
        this.eventQuestSubTitle.text = title2;
        break;
    }
    this.AP.text = ap.ToString();
  }

  public void OnClick()
  {
    if (!this.isAvailable)
      return;
    StageAvailibilityCheckHelper availibilityCheckHelper = this.gameObject.GetComponent<StageAvailibilityCheckHelper>() ?? this.gameObject.AddComponent<StageAvailibilityCheckHelper>();
    bool storyOnly = QuestStageMenuBase.checkForStoryOnly(this.stageData.type, this.stageData.id_S);
    bool Event = this.stageData.type == CommonQuestType.Extra;
    this.StartCoroutine(availibilityCheckHelper.PopupJudge(this.stageData, new System.Action(this.OnBeforeSceneChange), Event, storyOnly));
  }

  private void OnBeforeSceneChange()
  {
    Singleton<CommonRoot>.GetInstance().ShowLoadingLayer(0, false);
    Singleton<NGSceneManager>.GetInstance().SaveCurrentChangeSceneParam();
    System.Action changeSceneAction = this.beforeChangeSceneAction;
    if (changeSceneAction != null)
      changeSceneAction();
    NGGameDataManager instance = Singleton<NGGameDataManager>.GetInstance();
    instance.IsFromUnityPopupStageList = true;
    if (this.stageData != null)
      instance.QuestType = new CommonQuestType?(this.stageData.type);
    Singleton<PopupManager>.GetInstance().dismiss(false);
  }
}
