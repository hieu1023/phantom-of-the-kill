﻿// Decompiled with JetBrains decompiler
// Type: Gacha0063SheetModel
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;

public class Gacha0063SheetModel
{
  private GachaModule module;

  public bool IsSheetGachaOpen
  {
    get
    {
      return this.module.type == 6;
    }
  }

  public Gacha0063SheetModel(GachaModule module)
  {
    this.module = module;
  }
}
