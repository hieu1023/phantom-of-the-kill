﻿// Decompiled with JetBrains decompiler
// Type: StatusUsual
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;
using System;
using System.Collections;
using UnityEngine;

[Serializable]
public class StatusUsual
{
  public UILabel txt_waiting_for_entery_to_start;
  [SerializeField]
  private GuildStatus myStatus;

  public GuildStatus MyStatus
  {
    get
    {
      return this.myStatus;
    }
    set
    {
      this.myStatus = value;
    }
  }

  public IEnumerator ResourceLoad(GuildRegistration myData)
  {
    IEnumerator e = this.MyStatus.ResourceLoad(myData);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }
}
