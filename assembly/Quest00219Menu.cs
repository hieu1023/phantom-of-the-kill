﻿// Decompiled with JetBrains decompiler
// Type: Quest00219Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[AddComponentMenu("Scenes/QuestExtra/M_Menu")]
public class Quest00219Menu : BackButtonMenuBase
{
  private List<Coroutine> loadingCoroutineList = new List<Coroutine>();
  [SerializeField]
  protected UILabel TxtTitle;
  public UIScrollView scrollview;
  public UIGrid grid;
  public UI2DSprite EventSprite;
  [Header("AnchorTopControl")]
  [Tooltip("EventSpriteの有無に合わせてScrollViewのTop位置を調整")]
  [SerializeField]
  protected UIWidget widgetScrollViewTop;
  [SerializeField]
  protected int whenOnEventSprite;
  [SerializeField]
  protected int whenOffEventSprige;
  protected DateTime serverTime;
  private HashSet<int> emphasis_;
  private const int maxDefaultSetupBanner = 5;
  private int setupBannerCount;
  private int focusSId_;
  private QuestExtraL headerInfo_;

  public bool IncludingKeyGate { get; set; }

  private PlayerExtraQuestS[] ExtraData { get; set; }

  private List<QuestExtraTimetableNotice> Notices { get; set; }

  private void OnDestroy()
  {
    foreach (Coroutine loadingCoroutine in this.loadingCoroutineList)
    {
      if (loadingCoroutine != null)
        this.StopCoroutine(loadingCoroutine);
    }
    this.loadingCoroutineList.Clear();
  }

  protected string LoadSpriteEvent(int LId)
  {
    string path = "Prefabs/Banners/ExtraQuest/L/" + LId.ToString() + "/Specialquest_Story";
    return Singleton<ResourceManager>.GetInstance().Contains(path) ? path : "Prefabs/Banners/ExtraQuest/L/4/Specialquest_Story";
  }

  public IEnumerator Init(
    Future<GameObject> ListPrefab,
    Future<GameObject> ScrollPrefab,
    PlayerExtraQuestS[] ExtraData,
    int lid,
    int Sid,
    int[] Emphasis,
    QuestExtraTimetableNotice[] Notices)
  {
    this.grid.transform.Clear();
    this.grid.gameObject.SetActive(false);
    this.ExtraData = ExtraData;
    this.emphasis_ = new HashSet<int>((IEnumerable<int>) Emphasis);
    this.Notices = ((IEnumerable<QuestExtraTimetableNotice>) Notices).ToList<QuestExtraTimetableNotice>();
    PlayerQuestGate[] questGate = SMManager.Get<PlayerQuestGate[]>();
    HashSet<int> idGateS = new HashSet<int>(((IEnumerable<PlayerQuestGate>) questGate).SelectMany<PlayerQuestGate, int>((Func<PlayerQuestGate, IEnumerable<int>>) (s => (IEnumerable<int>) s.quest_ids)));
    PlayerExtraQuestS[] questExtra = SMManager.Get<PlayerExtraQuestS[]>();
    PlayerExtraQuestS[] list = ((IEnumerable<PlayerExtraQuestS>) ((IEnumerable<PlayerExtraQuestS>) this.ExtraData).M(lid, true)).Where<PlayerExtraQuestS>((Func<PlayerExtraQuestS, bool>) (w => !idGateS.Contains(w._quest_extra_s))).ToArray<PlayerExtraQuestS>();
    IEnumerator e;
    if (list.Length == 0)
    {
      Singleton<CommonRoot>.GetInstance().isLoading = false;
      Future<GameObject> time_popup = Res.Prefabs.popup.popup_002_23__anim_popup01.Load<GameObject>();
      e = time_popup.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      Singleton<PopupManager>.GetInstance().openAlert(time_popup.Result, false, false, (EventDelegate) null, false, true, false, true);
    }
    else
    {
      this.focusSId_ = Sid;
      this.setupBannerCount = 0;
      this.headerInfo_ = MasterData.QuestExtraL[lid];
      this.TxtTitle.SetTextLocalize(this.headerInfo_.name);
      if (this.headerInfo_.enabled_header)
      {
        this.widgetScrollViewTop.topAnchor.absolute = this.whenOnEventSprite;
        this.EventSprite.gameObject.SetActive(true);
        Future<Texture2D> futureEvent = Singleton<ResourceManager>.GetInstance().Load<Texture2D>(this.LoadSpriteEvent(lid), 1f);
        e = futureEvent.Wait();
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        Texture2D result = futureEvent.Result;
        UnityEngine.Sprite sprite = UnityEngine.Sprite.Create(result, new Rect(0.0f, 0.0f, (float) result.width, (float) result.height), new Vector2(0.5f, 0.5f), 1f, 100U, SpriteMeshType.FullRect);
        sprite.name = result.name;
        this.EventSprite.sprite2D = sprite;
        futureEvent = (Future<Texture2D>) null;
      }
      else
      {
        this.widgetScrollViewTop.topAnchor.absolute = this.whenOffEventSprige;
        this.EventSprite.gameObject.SetActive(false);
      }
      e = ServerTime.WaitSync();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.serverTime = ServerTime.NowAppTime();
      if (BannerBase.PathExist(((IEnumerable<PlayerExtraQuestS>) list).First<PlayerExtraQuestS>().quest_extra_s.quest_m_QuestExtraM, BannerBase.Type.quest, QuestExtra.SeekType.M, false))
      {
        e = this.PutBannerList(ScrollPrefab, list);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
      }
      else
      {
        e = this.PutGeneralBtnList(ListPrefab, list);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
      }
      e = this.InitQuestGate(lid, questExtra, questGate);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.grid.gameObject.SetActive(true);
      this.grid.Reposition();
      this.scrollview.ResetPosition();
      Singleton<CommonRoot>.GetInstance().isLoading = false;
    }
  }

  private Quest00217Scroll.Parameter GenerateParam(PlayerExtraQuestS extra)
  {
    Quest00217Scroll.Parameter parameter = new Quest00217Scroll.Parameter();
    QuestExtra.getStatusM(extra.quest_extra_s.quest_m_QuestExtraM, this.ExtraData, this.emphasis_, out parameter.isNew, out parameter.isClear, out parameter.isHighlighting);
    return parameter;
  }

  private IEnumerator PutBannerList(Future<GameObject> prefab, PlayerExtraQuestS[] list)
  {
    IEnumerator e = prefab.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    GameObject scrollPrefab = prefab.Result;
    this.grid.cellHeight = 168f;
    PlayerExtraQuestS[] playerExtraQuestSArray = list;
    for (int index = 0; index < playerExtraQuestSArray.Length; ++index)
    {
      PlayerExtraQuestS extra = playerExtraQuestSArray[index];
      Quest00217Scroll.Parameter parameter = this.GenerateParam(extra);
      QuestExtraTimetableNotice extraTimetableNotice = this.Notices.Find((Predicate<QuestExtraTimetableNotice>) (n => n._quest_extra_s == extra._quest_extra_s));
      if (extraTimetableNotice != null && extraTimetableNotice.start_at.HasValue)
      {
        parameter.isNotice = true;
        parameter.startTime = extraTimetableNotice.start_at;
      }
      parameter.extra = extra;
      parameter.seek = QuestExtra.SeekType.M;
      e = this.ScrollInit(parameter, scrollPrefab);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    playerExtraQuestSArray = (PlayerExtraQuestS[]) null;
  }

  private IEnumerator PutGeneralBtnList(
    Future<GameObject> prefab,
    PlayerExtraQuestS[] list)
  {
    IEnumerator e = prefab.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    GameObject listPrefab = prefab.Result;
    this.grid.cellHeight = 114f;
    PlayerExtraQuestS[] playerExtraQuestSArray = list;
    for (int index = 0; index < playerExtraQuestSArray.Length; ++index)
    {
      PlayerExtraQuestS extra = playerExtraQuestSArray[index];
      Quest00217Scroll.Parameter parameter = this.GenerateParam(extra);
      e = this.ListInit(extra, listPrefab, parameter.isClear, parameter.isNew);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    playerExtraQuestSArray = (PlayerExtraQuestS[]) null;
  }

  private IEnumerator InitQuestGate(
    int idQuestL,
    PlayerExtraQuestS[] questExtra,
    PlayerQuestGate[] questGates)
  {
    IEnumerable<PlayerExtraQuestS> source = ((IEnumerable<PlayerExtraQuestS>) questExtra).Where<PlayerExtraQuestS>((Func<PlayerExtraQuestS, bool>) (w => w.quest_extra_s != null)).Where<PlayerExtraQuestS>((Func<PlayerExtraQuestS, bool>) (w => w.quest_extra_s.quest_l_QuestExtraL == idQuestL));
    if (source.Count<PlayerExtraQuestS>() != 0)
    {
      List<int> targetIdS = source.Select<PlayerExtraQuestS, int>((Func<PlayerExtraQuestS, int>) (s => s.quest_extra_s.ID)).ToList<int>();
      List<PlayerQuestGate> gates = ((IEnumerable<PlayerQuestGate>) questGates).Where<PlayerQuestGate>((Func<PlayerQuestGate, bool>) (w => ((IEnumerable<int>) w.quest_ids).Any<int>((Func<int, bool>) (a => targetIdS.Contains(a))))).ToList<PlayerQuestGate>();
      if (gates.Count<PlayerQuestGate>() != 0)
      {
        Dictionary<int, bool> inProgressQuests = gates.Aggregate<PlayerQuestGate, Dictionary<int, bool>>(new Dictionary<int, bool>(), (Func<Dictionary<int, bool>, PlayerQuestGate, Dictionary<int, bool>>) ((acc, quest) =>
        {
          if (quest.in_progress)
            acc[quest.quest_key_id] = true;
          return acc;
        }));
        List<int> keyKinds = gates.Distinct<PlayerQuestGate>((IEqualityComparer<PlayerQuestGate>) new LambdaEqualityComparer<PlayerQuestGate>((Func<PlayerQuestGate, PlayerQuestGate, bool>) ((a, b) => a.quest_key_id == b.quest_key_id))).OrderByDescending<PlayerQuestGate, bool>((Func<PlayerQuestGate, bool>) (x => inProgressQuests.ContainsKey(x.quest_key_id))).ThenBy<PlayerQuestGate, int>((Func<PlayerQuestGate, int>) (y => y.quest_key_id)).Select<PlayerQuestGate, int>((Func<PlayerQuestGate, int>) (z => z.quest_key_id)).ToList<int>();
        if (keyKinds.Count<int>() != 0)
        {
          this.IncludingKeyGate = true;
          Future<GameObject> ScrollPrefab = Res.Prefabs.quest002_17_1.scroll.Load<GameObject>();
          IEnumerator e = ScrollPrefab.Wait();
          while (e.MoveNext())
            yield return e.Current;
          e = (IEnumerator) null;
          GameObject prefab = ScrollPrefab.Result;
          foreach (int num in keyKinds)
          {
            int keyKind = num;
            e = this.ScrollInit(gates.Where<PlayerQuestGate>((Func<PlayerQuestGate, bool>) (x => x.quest_key_id == keyKind)).ToArray<PlayerQuestGate>(), prefab);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
          }
        }
      }
    }
  }

  private IEnumerator ScrollInit(PlayerQuestGate[] gates, GameObject prefab)
  {
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(prefab);
    gameObject.transform.parent = this.grid.transform;
    gameObject.transform.localScale = Vector3.one;
    gameObject.transform.localPosition = Vector3.zero;
    IEnumerator e = gameObject.GetComponent<Quest002171Scroll>().InitScroll(gates, this.serverTime, false);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public virtual IEnumerator ScrollInit(
    Quest00217Scroll.Parameter param,
    GameObject prefab)
  {
    Quest00219Menu quest00219Menu = this;
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(prefab);
    gameObject.transform.parent = quest00219Menu.grid.transform;
    gameObject.transform.localScale = Vector3.one;
    gameObject.transform.localPosition = Vector3.zero;
    Quest00217Scroll component = gameObject.GetComponent<Quest00217Scroll>();
    if (PerformanceConfig.GetInstance().IsTuningEventTopSetting && quest00219Menu.setupBannerCount >= 5)
    {
      component.Setup(param, quest00219Menu.serverTime);
      Coroutine coroutine = quest00219Menu.StartCoroutine(component.SetAndCreate_BannerSprite());
      quest00219Menu.loadingCoroutineList.Add(coroutine);
    }
    else
    {
      IEnumerator e = component.InitScroll(param, quest00219Menu.serverTime);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    ++quest00219Menu.setupBannerCount;
  }

  public virtual IEnumerator ListInit(
    PlayerExtraQuestS extra,
    GameObject prefab,
    bool isClear,
    bool isNew)
  {
    GameObject list = UnityEngine.Object.Instantiate<GameObject>(prefab);
    list.transform.parent = this.grid.transform;
    list.transform.localScale = Vector3.one;
    list.transform.localPosition = Vector3.zero;
    IEnumerator e = list.GetComponent<Quest00219List>().Init(extra, isClear, isNew);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    EventDelegate.Set(list.GetComponent<Quest00219List>().Dock.onClick, (EventDelegate.Callback) (() => this.ChangeScene00220(extra, list, false, false)));
  }

  protected void ChangeScene00220(
    PlayerExtraQuestS extra,
    GameObject obj,
    bool Guerrilla = false,
    bool rankingEvent = false)
  {
    this.StartCoroutine(this.QuestTimeCompare(extra, obj, Guerrilla, rankingEvent));
  }

  public virtual void Foreground()
  {
    Debug.Log((object) "click default event Foreground");
  }

  public virtual void IbtnBack()
  {
    if (this.IsPushAndSet())
      return;
    if (this.headerInfo_ == null)
    {
      this.backScene();
    }
    else
    {
      QuestExtraLL questLl = this.headerInfo_.quest_ll;
      Singleton<NGSceneManager>.GetInstance().destroyCurrentScene();
      if (questLl != null)
        Quest00218Scene.backOrChangeScene(questLl.ID, new int?(this.focusSId_));
      else
        Quest00217Scene.backOrChangeScene(this.headerInfo_.category_QuestExtraCategory);
    }
  }

  public override void onBackButton()
  {
    this.IbtnBack();
  }

  public virtual void IbtnEvent()
  {
    Debug.Log((object) "click default event IbtnEvent");
  }

  public virtual void VScrollBar()
  {
    Debug.Log((object) "click default event VScrollBar");
  }

  public IEnumerator QuestTimeCompare(
    PlayerExtraQuestS StageData,
    GameObject obj,
    bool Guerrilla = false,
    bool rankingEvent = false)
  {
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    IEnumerator e = ServerTime.WaitSync();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if (ServerTime.NowAppTime() < StageData.today_day_end_at)
    {
      Quest00220Scene.ChangeScene00220(false, StageData.quest_extra_s.quest_l_QuestExtraL, StageData.quest_extra_s.quest_m_QuestExtraM, Guerrilla, false, false);
    }
    else
    {
      Singleton<CommonRoot>.GetInstance().isLoading = false;
      Future<GameObject> time_popup = Res.Prefabs.popup.popup_002_23__anim_popup01.Load<GameObject>();
      e = time_popup.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      Singleton<PopupManager>.GetInstance().openAlert(time_popup.Result, false, false, (EventDelegate) null, false, true, false, true);
      time_popup = (Future<GameObject>) null;
    }
  }
}
