﻿// Decompiled with JetBrains decompiler
// Type: Unit00446Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using System.Collections;

public class Unit00446Scene : NGSceneBase
{
  public Unit00446Menu menu;

  public static void changeScene(bool stack, GearGear targetgear)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("unit004_4_6", (stack ? 1 : 0) != 0, (object) targetgear);
  }

  public IEnumerator onStartSceneAsync(GearGear targetgear)
  {
    IEnumerator e = this.menu.SetSprite(targetgear);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }
}
