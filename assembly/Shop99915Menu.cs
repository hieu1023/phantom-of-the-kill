﻿// Decompiled with JetBrains decompiler
// Type: Shop99915Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;
using UnityEngine;

public class Shop99915Menu : BackButtonMenuBase
{
  [SerializeField]
  private UILabel TxtTitle;
  [SerializeField]
  private UILabel TxtDescription;

  public void SetText(string name)
  {
    string str = name != null ? name : Consts.GetInstance().UNIQUE_ICON_KEY;
    string text1 = Consts.Format(Consts.GetInstance().SHOP_99915_TXT_TITLE, (IDictionary) null);
    string text2 = Consts.Format(Consts.GetInstance().SHOP_99915_TXT_DESCRIPTION, (IDictionary) new Hashtable()
    {
      {
        (object) nameof (name),
        (object) str
      }
    });
    this.TxtTitle.SetText(text1);
    this.TxtDescription.SetText(text2);
  }

  public void IbtnOK()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<PopupManager>.GetInstance().onDismiss();
  }

  public override void onBackButton()
  {
    this.IbtnOK();
  }
}
