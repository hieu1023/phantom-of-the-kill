﻿// Decompiled with JetBrains decompiler
// Type: ClipEventEffectData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public class ClipEventEffectData : ScriptableObject
{
  public List<ClipEventEffectData.EffectData> dataList;

  public ClipEventEffectData()
  {
    this.dataList = new List<ClipEventEffectData.EffectData>();
  }

  [Serializable]
  public class EffectData
  {
    public string effect_name = "";
    public string parent = "";
    public Vector3 position = Vector3.zero;
    public Vector3 rotation = Vector3.zero;
    public bool is_local_postion;
    public bool is_add_bip;
    public bool is_local_rotation;
  }
}
