﻿// Decompiled with JetBrains decompiler
// Type: Story009132Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Story009132Menu : BackButtonMenuBase
{
  [SerializeField]
  protected UILabel TxtTitle;
  [SerializeField]
  private NGxScroll ScrollContainer;
  private PlayerSeaQuestS quest;

  public virtual void Foreground()
  {
  }

  public virtual void VScrollBar()
  {
  }

  public override void onBackButton()
  {
    this.IbtnBack();
  }

  public virtual void IbtnBack()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<NGSceneManager>.GetInstance().changeScene("story009_13", false, (object[]) Array.Empty<object>());
  }

  public IEnumerator InitEpisodeButton(PlayerSeaQuestS[] quests, PlayerSeaQuestS quest)
  {
    this.TxtTitle.SetTextLocalize(quest.quest_sea_s.quest_m.name);
    this.quest = quest;
    Array.Reverse((Array) quests);
    Future<GameObject> prefabScrollPartsF = new ResourceObject("Prefabs/Story009_13_2/story009_13_2_button").Load<GameObject>();
    IEnumerator e = prefabScrollPartsF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    GameObject prefabScrollParts = prefabScrollPartsF.Result;
    ((IEnumerable<PlayerSeaQuestS>) quests).OrderBy<PlayerSeaQuestS, int>((Func<PlayerSeaQuestS, int>) (x => x.quest_sea_s.ID)).ForEach<PlayerSeaQuestS>((System.Action<PlayerSeaQuestS>) (q =>
    {
      if (!q.is_clear || q.quest_sea_s.StoryDetails().Length == 0)
        return;
      foreach (StoryPlaybackSeaDetail storyDetail in q.quest_sea_s.StoryDetails())
      {
        Story009132ScrollParts component = prefabScrollParts.CloneAndGetComponent<Story009132ScrollParts>((GameObject) null);
        this.ScrollContainer.Add(component.gameObject, false);
        component.Init(this, storyDetail);
      }
    }));
    this.ScrollContainer.ResolvePosition();
  }
}
