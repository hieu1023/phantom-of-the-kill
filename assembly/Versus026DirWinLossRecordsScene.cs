﻿// Decompiled with JetBrains decompiler
// Type: Versus026DirWinLossRecordsScene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

public class Versus026DirWinLossRecordsScene : NGSceneBase
{
  [SerializeField]
  private Versus026DirWinLossRecordsMenu menu;

  public static void ChangeScene(bool stack, Versus02613Scene.BootParam param)
  {
    param.push("versus026_win_loss_records", (Versus02613Scene.BootArgument) null);
    Singleton<NGSceneManager>.GetInstance().changeScene(param.current.scene, (stack ? 1 : 0) != 0, (object) param);
  }

  public IEnumerator onStartSceneAsync(Versus02613Scene.BootParam param)
  {
    IEnumerator e = this.menu.Init(param);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }
}
