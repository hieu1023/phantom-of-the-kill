﻿// Decompiled with JetBrains decompiler
// Type: Shop007231Coupon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System.Collections;
using UnityEngine;

public class Shop007231Coupon : MonoBehaviour
{
  [SerializeField]
  private UILabel txtTitle_;
  [SerializeField]
  private GameObject linkThum_;
  [SerializeField]
  private UILabel txtLimitDate_;
  [SerializeField]
  private UILabel txtNeed_;
  [SerializeField]
  private UILabel txtProgress_;
  [SerializeField]
  private UILabel txtProgressRed_;
  [SerializeField]
  private UIButton btnToShop_;
  private Shop007231Menu menu_;
  private ShopMaterialExchangeMenu materialMenu;
  private PlayerSelectTicketSummary playerUnitTicket_;
  private SelectTicket unitTicket_;

  public IEnumerator coInitialize(
    Shop007231Menu menu,
    PlayerSelectTicketSummary playerUnitTicket,
    SelectTicket unitTicket)
  {
    Future<GameObject> ldicon = Res.Icons.UniqueIconPrefab.Load<GameObject>();
    IEnumerator e = ldicon.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    GameObject result = ldicon.Result;
    if ((Object) result != (Object) null)
    {
      UniqueIcons ticketicon = result.Clone(this.linkThum_.transform).GetComponent<UniqueIcons>();
      e = ticketicon.SetKillersTicket(unitTicket.id, 0);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      ticketicon.LabelActivated = false;
      ticketicon.BackGroundActivated = false;
      ticketicon = (UniqueIcons) null;
    }
    this.menu_ = menu;
    this.playerUnitTicket_ = playerUnitTicket;
    this.unitTicket_ = unitTicket;
    this.txtTitle_.SetTextLocalize(unitTicket.name);
    this.txtLimitDate_.SetTextLocalize(string.Format(Consts.GetInstance().SHOP_00723_EXPIRATION_DATE, (object) unitTicket.end_at));
    this.txtNeed_.SetTextLocalize(unitTicket.cost);
    if (playerUnitTicket.quantity >= unitTicket.cost)
    {
      this.txtProgress_.SetTextLocalize(playerUnitTicket.quantity);
      this.txtProgress_.gameObject.SetActive(true);
      this.txtProgressRed_.gameObject.SetActive(false);
    }
    else
    {
      this.txtProgress_.gameObject.SetActive(false);
      this.txtProgressRed_.SetTextLocalize(playerUnitTicket.quantity);
      this.txtProgressRed_.gameObject.SetActive(true);
    }
    this.btnToShop_.isEnabled = playerUnitTicket.quantity >= unitTicket.cost;
  }

  public IEnumerator coInitialize(
    ShopMaterialExchangeMenu menu,
    PlayerSelectTicketSummary playerUnitTicket,
    SelectTicket unitTicket)
  {
    Future<GameObject> ldicon = Res.Icons.UniqueIconPrefab.Load<GameObject>();
    IEnumerator e = ldicon.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    GameObject result = ldicon.Result;
    if ((Object) result != (Object) null)
    {
      UniqueIcons ticketicon = result.Clone(this.linkThum_.transform).GetComponent<UniqueIcons>();
      if (unitTicket.category_id == 1)
      {
        e = ticketicon.SetKillersTicket(unitTicket.id, 0);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
      }
      else if (unitTicket.category_id == 2)
      {
        e = ticketicon.SetMaterialTicket(unitTicket.id, 0);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
      }
      ticketicon.LabelActivated = false;
      ticketicon.BackGroundActivated = false;
      ticketicon = (UniqueIcons) null;
    }
    this.materialMenu = menu;
    this.playerUnitTicket_ = playerUnitTicket;
    this.unitTicket_ = unitTicket;
    this.txtTitle_.SetTextLocalize(unitTicket.name);
    this.txtLimitDate_.SetTextLocalize(string.Format(Consts.GetInstance().SHOP_00723_EXPIRATION_DATE, (object) unitTicket.end_at));
    this.txtNeed_.SetTextLocalize(unitTicket.cost);
    if (playerUnitTicket.quantity >= unitTicket.cost)
    {
      this.txtProgress_.SetTextLocalize(playerUnitTicket.quantity);
      this.txtProgress_.gameObject.SetActive(true);
      this.txtProgressRed_.gameObject.SetActive(false);
    }
    else
    {
      this.txtProgress_.gameObject.SetActive(false);
      this.txtProgressRed_.SetTextLocalize(playerUnitTicket.quantity);
      this.txtProgressRed_.gameObject.SetActive(true);
    }
    this.btnToShop_.isEnabled = playerUnitTicket.quantity >= unitTicket.cost;
  }

  public void onClickedSelect()
  {
    if ((Object) this.menu_ != (Object) null)
    {
      this.menu_.selectedCoupon(this.playerUnitTicket_);
    }
    else
    {
      if (!((Object) this.materialMenu != (Object) null))
        return;
      this.materialMenu.selectedCoupon(this.playerUnitTicket_);
    }
  }

  public void onClickedDescription()
  {
    if ((Object) this.menu_ != (Object) null)
    {
      this.menu_.onClickedDescription(this.unitTicket_);
    }
    else
    {
      if (!((Object) this.materialMenu != (Object) null))
        return;
      this.materialMenu.onClickedDescription(this.unitTicket_);
    }
  }
}
