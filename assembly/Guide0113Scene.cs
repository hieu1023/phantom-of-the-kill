﻿// Decompiled with JetBrains decompiler
// Type: Guide0113Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Guide0113Scene : NGSceneBase
{
  private bool isFirstInit = true;
  public Guide0113Menu menu;

  public static void changeScene(bool stack, Guide0111Scene.UpdateInfo updateInfo)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("guide011_3", (stack ? 1 : 0) != 0, (object) updateInfo);
  }

  public override IEnumerator onInitSceneAsync()
  {
    Guide0113Scene guide0113Scene = this;
    Singleton<CommonRoot>.GetInstance().ShowLoadingLayer(0, false);
    Future<GameObject> fBG = Res.Prefabs.BackGround.picturebook.Load<GameObject>();
    IEnumerator e = fBG.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    guide0113Scene.backgroundPrefab = fBG.Result;
  }

  public IEnumerator onStartSceneAsync(Guide0111Scene.UpdateInfo updateInfo)
  {
    Singleton<CommonRoot>.GetInstance().ShowLoadingLayer(0, false);
    if (!updateInfo.version.HasValue)
    {
      Future<WebAPI.Response.ZukanEnemy> enemy = WebAPI.ZukanEnemy((System.Action<WebAPI.Response.UserError>) (e =>
      {
        WebAPI.DefaultUserErrorCallback(e);
        Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
      }));
      yield return (object) enemy.Wait();
      if (enemy.Result == null)
      {
        yield break;
      }
      else
      {
        updateInfo.version = new long?(1L);
        List<UnitUnit> source = new List<UnitUnit>(enemy.Result.histories.Length);
        foreach (PlayerEnemyHistory history in enemy.Result.histories)
        {
          UnitUnit unitUnit;
          if (MasterData.UnitUnit.TryGetValue(history.unit_id, out unitUnit))
            source.Add(unitUnit);
        }
        if (source.Any<UnitUnit>())
          yield return (object) OnDemandDownload.WaitLoadUnitResource((IEnumerable<UnitUnit>) source, false, (IEnumerable<string>) null, false);
        this.isFirstInit = true;
        enemy = (Future<WebAPI.Response.ZukanEnemy>) null;
      }
    }
    if (this.isFirstInit)
    {
      this.isFirstInit = false;
      IEnumerator e = this.menu.onInitMenuAsync();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    Singleton<CommonRoot>.GetInstance().HideLoadingLayer();
    this.menu.IbtnUse();
  }

  public void onStartScene(Guide0111Scene.UpdateInfo updateInfo)
  {
  }

  public override void onEndScene()
  {
    this.menu.StopCreateUnitIconImage();
    Singleton<PopupManager>.GetInstance().closeAll(false);
  }
}
