﻿// Decompiled with JetBrains decompiler
// Type: Story0093Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using Earth;
using MasterDataTable;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Story0093Scene : NGSceneBase
{
  private static string sceneNormal_ = "story009_3";
  private static string sceneSea_ = "story009_3_sea";
  private string msgError_ = string.Empty;
  public StoryExecuter executer;
  private float timeScaleTmp;
  private StoryExecuter.ScriptMode modeScript_;
  private System.Action endAction;
  private Story0093Scene.DateParam dateParam_;

  public static bool setTestScript(string script)
  {
    return false;
  }

  public static void changeScene(bool stack, int scriptId, bool? isSea = null, System.Action endAction = null)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene(isSea.HasValue ? (isSea.Value ? Story0093Scene.sceneSea_ : Story0093Scene.sceneNormal_) : (Singleton<NGGameDataManager>.GetInstance().IsSea ? Story0093Scene.sceneSea_ : Story0093Scene.sceneNormal_), (stack ? 1 : 0) != 0, (object) scriptId, (object) endAction);
  }

  public static void changeSceneModeDate(
    bool stack,
    int scriptId,
    UnitUnit characterA,
    string background = null,
    System.Action<int, int> eventSelected = null,
    bool isLoadedScript = false,
    bool? isSea = null,
    int? questionId = null)
  {
    Hashtable replaceTable = new Hashtable()
    {
      {
        (object) nameof (background),
        string.IsNullOrEmpty(background) ? (object) "\"black\"" : (object) ("\"" + background + "\"")
      }
    };
    Story0093Scene.changeSceneModeDate(stack, scriptId, characterA, replaceTable, eventSelected, isLoadedScript, isSea, questionId);
  }

  public static void changeSceneModeDate(
    bool stack,
    int scriptId,
    UnitUnit characterA,
    Hashtable replaceTable,
    System.Action<int, int> eventSelected = null,
    bool isLoadedScript = false,
    bool? isSea = null,
    int? questionId = null)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene(isSea.HasValue ? (isSea.Value ? Story0093Scene.sceneSea_ : Story0093Scene.sceneNormal_) : (Singleton<NGGameDataManager>.GetInstance().IsSea ? Story0093Scene.sceneSea_ : Story0093Scene.sceneNormal_), true, (object) new Story0093Scene.DateParam(scriptId, characterA, replaceTable, eventSelected, isLoadedScript, questionId));
  }

  public static IEnumerator loadDateScript(int scriptId, UnitUnit unit, bool isQuestion)
  {
    IEnumerator e = MasterData.LoadDateScriptBase(scriptId);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if (unit != null)
    {
      e = MasterData.LoadDateScriptParts(unit.resource_reference_unit_id_UnitUnit);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      if (isQuestion)
      {
        e = MasterData.LoadDateScriptQuestion(unit.resource_reference_unit_id_UnitUnit);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
      }
    }
  }

  public static void unloadDateScript()
  {
    MasterDataCache.Unload("DateScriptBase");
    MasterDataCache.Unload("DateScriptParts");
    MasterDataCache.Unload("DateScriptQuestion");
  }

  public static void changeScene009_3(bool stack, int scriptId)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene(Story0093Scene.sceneNormal_, true, (object) scriptId);
  }

  public IEnumerator onStartSceneAsync()
  {
    return this.onStartSceneAsync(MasterData.ScriptScriptList[0].ID);
  }

  public IEnumerator onStartSceneAsync(int scriptId, System.Action endAction)
  {
    this.endAction = endAction;
    IEnumerator e = this.onStartSceneAsync(scriptId);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public IEnumerator onStartSceneAsync(Story0093Scene.DateParam dateParam)
  {
    this.dateParam_ = dateParam;
    if (this.dateParam_ != null)
    {
      this.modeScript_ = StoryExecuter.ScriptMode.Date;
      if ((UnityEngine.Object) this.executer != (UnityEngine.Object) null)
      {
        this.executer.setMode(this.modeScript_);
        this.executer.setEventSelected(this.dateParam_.eventSelected_);
      }
      IEnumerator e = this.onStartSceneAsync(this.dateParam_.scriptId_);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
  }

  public IEnumerator onStartSceneAsync(int scriptId)
  {
    this.msgError_ = string.Empty;
    if ((UnityEngine.Object) this.executer != (UnityEngine.Object) null)
    {
      string[] filepatterns = new string[3]
      {
        "unit_large",
        "Face",
        "VO_"
      };
      Tuple<IEnumerable<UnitUnit>, IEnumerable<string>> resources;
      IEnumerator e;
      if (this.modeScript_ != StoryExecuter.ScriptMode.Date)
      {
        e = MasterData.LoadScriptScript(scriptId);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        if (MasterData.ScriptScript == null || !MasterData.ScriptScript.ContainsKey(scriptId))
        {
          this.msgError_ = string.Format("Not Found MasterData.ScriptScript[{0}]", (object) scriptId);
          yield break;
        }
        else
        {
          resources = StoryUtility.GetResource(scriptId);
          if (resources.Item1 != null)
          {
            e = OnDemandDownload.WaitLoadUnitResource(resources.Item1, false, (IEnumerable<string>) filepatterns, false);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
          }
          if (resources.Item2 != null)
          {
            e = OnDemandDownload.waitLoadSomethingResource(resources.Item2, false, false);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
          }
          this.executer.gameObject.SetActive(true);
          e = this.executer.initialize(MasterData.ScriptScript[scriptId].script, this.endAction);
          while (e.MoveNext())
            yield return e.Current;
          e = (IEnumerator) null;
          resources = (Tuple<IEnumerable<UnitUnit>, IEnumerable<string>>) null;
        }
      }
      else
      {
        if (!this.dateParam_.isLoadedScript_)
        {
          e = Story0093Scene.loadDateScript(scriptId, this.dateParam_.characterA_, this.dateParam_.isQuestion_);
          while (e.MoveNext())
            yield return e.Current;
          e = (IEnumerator) null;
        }
        if (MasterData.DateScriptBase == null || !MasterData.DateScriptBase.ContainsKey(scriptId))
        {
          this.msgError_ = string.Format("Not Found MasterData.DateScriptBase[{0}]", (object) scriptId);
          yield break;
        }
        else
        {
          string txtscript = MasterData.DateScriptBase[scriptId].script;
          StoryCasting storyCasting = new StoryCasting();
          if (this.dateParam_.characterA_ != null)
          {
            storyCasting.setCasting(this.dateParam_.characterA_.ID, (int[]) Array.Empty<int>());
            if (this.dateParam_.isQuestion_)
              storyCasting.setQuestion(this.dateParam_.question_, (int[]) Array.Empty<int>());
          }
          txtscript = storyCasting.buildScript(txtscript, this.dateParam_.replaceExtension_);
          resources = StoryUtility.GetResource(txtscript);
          if (resources.Item1 != null)
          {
            e = OnDemandDownload.WaitLoadUnitResource(resources.Item1, false, (IEnumerable<string>) filepatterns, false);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
          }
          if (resources.Item2 != null)
          {
            e = OnDemandDownload.waitLoadSomethingResource(resources.Item2, false, false);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
          }
          this.executer.gameObject.SetActive(true);
          e = this.executer.initialize(txtscript, this.endAction);
          while (e.MoveNext())
            yield return e.Current;
          e = (IEnumerator) null;
          txtscript = (string) null;
          resources = (Tuple<IEnumerable<UnitUnit>, IEnumerable<string>>) null;
        }
      }
      filepatterns = (string[]) null;
      this.executer.render();
      while (!this.executer.isRenderDone)
        yield return (object) 0;
    }
  }

  public void onStartScene()
  {
    Singleton<CommonRoot>.GetInstance().isLoading = false;
    this.timeScaleTmp = Time.timeScale;
    Time.timeScale = 1f;
    if (string.IsNullOrEmpty(this.msgError_))
      return;
    Debug.LogError((object) this.msgError_);
    if (this.endAction != null)
      return;
    Singleton<NGSceneManager>.GetInstance().backScene();
  }

  public void onStartScene(int scriptId, System.Action endAction)
  {
    this.onStartScene();
  }

  public void onStartScene(int scriptId)
  {
    this.onStartScene();
  }

  public void onStartScene(Story0093Scene.DateParam dateParam)
  {
    this.onStartScene();
  }

  public override void onEndScene()
  {
    Time.timeScale = this.timeScaleTmp;
    if ((UnityEngine.Object) this.executer != (UnityEngine.Object) null)
      this.executer.onEndScene();
    EarthDataManager instanceOrNull = Singleton<EarthDataManager>.GetInstanceOrNull();
    if ((UnityEngine.Object) instanceOrNull != (UnityEngine.Object) null)
      instanceOrNull.isStoryPlayBackMode = false;
    if (this.modeScript_ != StoryExecuter.ScriptMode.Date)
    {
      MasterDataCache.Unload("ScriptScript");
    }
    else
    {
      if (this.dateParam_.isLoadedScript_)
        return;
      Story0093Scene.unloadDateScript();
    }
  }

  public class DateParam
  {
    public int scriptId_ { get; private set; }

    public UnitUnit characterA_ { get; private set; }

    public Hashtable replaceExtension_ { get; private set; }

    public System.Action<int, int> eventSelected_ { get; private set; }

    public bool isLoadedScript_ { get; private set; }

    public int question_ { get; private set; }

    public bool isQuestion_ { get; private set; }

    public DateParam(
      int scriptId,
      UnitUnit characterA,
      Hashtable replaceExtension,
      System.Action<int, int> eventSelected,
      bool isLoadedScript,
      int? questionId)
    {
      this.scriptId_ = scriptId;
      this.characterA_ = characterA;
      this.replaceExtension_ = replaceExtension;
      this.eventSelected_ = eventSelected;
      this.isLoadedScript_ = isLoadedScript;
      this.question_ = questionId.HasValue ? questionId.Value : 0;
      this.isQuestion_ = questionId.HasValue;
    }
  }
}
