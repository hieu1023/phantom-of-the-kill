﻿// Decompiled with JetBrains decompiler
// Type: GearKindIcon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using System;
using UnityEngine;

public class GearKindIcon : IconPrefabBase
{
  public UI2DSprite iconSprite;
  private static GameObject self;

  public void Init(GearKind kind, CommonElement element = CommonElement.none)
  {
    this.iconSprite.sprite2D = GearKindIcon.LoadSprite(kind.Enum, element);
  }

  public void Init(int ID, CommonElement element = CommonElement.none)
  {
    this.iconSprite.sprite2D = GearKindIcon.LoadSprite((GearKindEnum) ID, element);
  }

  public static UnityEngine.Sprite LoadSprite(GearKindEnum kind, CommonElement element)
  {
    string empty = string.Empty;
    return Resources.Load<UnityEngine.Sprite>(!Singleton<NGGameDataManager>.GetInstance().IsSea ? string.Format("Icons/Materials/GearKind_Element_Icon/slc_{0}_{1}_34_30", (object) kind.ToString(), (object) element.ToString()) : string.Format("Icons/Materials/Sea/GearKind_Element_Icon/slc_{0}_{1}_34_30", (object) kind.ToString(), (object) element.ToString()));
  }

  public void None()
  {
    string empty = string.Empty;
    this.iconSprite.sprite2D = Resources.Load<UnityEngine.Sprite>(!Singleton<NGGameDataManager>.GetInstance().IsSea ? string.Format("Icons/Materials/GearKindIcon/9", (object[]) Array.Empty<object>()) : string.Format("Icons/Materials/Sea/GearKindIcon/s_type_none", (object[]) Array.Empty<object>()));
  }

  public static GameObject GetPrefab()
  {
    if ((UnityEngine.Object) GearKindIcon.self == (UnityEngine.Object) null)
      GearKindIcon.self = Resources.Load<GameObject>("Icons/GearKindIcon");
    return GearKindIcon.self;
  }
}
