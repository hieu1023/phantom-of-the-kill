﻿// Decompiled with JetBrains decompiler
// Type: Sea030HomeMenu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnitStatusInformation;
using UnityEngine;

public class Sea030HomeMenu : BackButtonMenuBase
{
  private Dictionary<int, int> eventSelectData = new Dictionary<int, int>();
  [SerializeField]
  private SeaHomeManager manager;
  [SerializeField]
  private GameObject homeRoot;
  [SerializeField]
  private GameObject buttonsRoot;
  [SerializeField]
  private GameObject[] cameraButtons;
  [SerializeField]
  private GameObject talkBackButton;
  [SerializeField]
  private GameObject talkRoot;
  [SerializeField]
  private GameObject dynUnit2D;
  [SerializeField]
  private UILabel characterNameLabel;
  [SerializeField]
  private UILabel textLabel;
  [SerializeField]
  private UI2DSprite unitThum;
  [SerializeField]
  private UIButton BtnDate;
  [SerializeField]
  private GameObject commingSoon;
  [SerializeField]
  private UIButton BtnPresent;
  [SerializeField]
  private GameObject GaugeObject;
  [SerializeField]
  private LoveGaugeController loveGauge;
  [SerializeField]
  private UILabel trustLabel;
  [SerializeField]
  private UIButton ibtnCharacterQuest;
  [SerializeField]
  private GameObject dirExtraSkill;
  [SerializeField]
  private UI2DSprite slcExtraSkill;
  [SerializeField]
  private GameObject resultEffectObject;
  [SerializeField]
  private Animator resultEffectAnimator;
  [SerializeField]
  private Animator unitAnimator;
  [SerializeField]
  private GameObject resultGaugeEffectObject;
  [SerializeField]
  private Animator resultGaugeEffectAnimator;
  [SerializeField]
  private UILabel trustUpLabel;
  [SerializeField]
  private Animator trustUpLabelAnimator;
  [SerializeField]
  private GameObject floatingSkillDialogRoot;
  [SerializeField]
  private GameObject callDialogRoot;
  [SerializeField]
  private BannersProc bannerParent;
  private bool isLoadBanner;
  private Modified<SM.Banner[]> bannerModified;
  private SeaHomeManager.UnitConrtolleData current2DUnitData;
  private GameObject callPopupPrefab;
  private GameObject presentPopupPrefab;
  private GameObject dateSelectPopupPrefab;
  private GameObject dateConfirmPopupPrefab;
  private GameObject favorabilityRatingEffectPopupPrefab;
  private Sea030HomeCallPopup callPopupObject;
  private PopupSkillDetails.Param skillParams;
  private GameObject skillDetailDialogPrefab;
  private Sea030HomeMenu.HomeMode mode;
  private UITweener[] tweeners;
  private NGxFaceSprite unitFace;
  private NGxEyeSprite unitEye;
  private SeaHomeUnitEyeBlink eyeBlink;
  private List<int> serifIndex;
  private int serifIndexCount;
  private SeaDateDateSpotDisplaySetting dateSetting;
  private List<int> dateFlows;
  private int? quizId;
  private int nowFlowIndex;
  private bool existSelect;
  public bool isChangeSelectSpot;
  public bool isReturnSelectSpot;
  public bool isSelectedSpot;
  private const int AppreciationToTalkTween = 3012;
  private const int TalkToAppreciationTween = 3021;
  private const int CAMERABUTTON_OFF = 0;
  private const int CAMERABUTTON_ON = 1;
  private bool isInit;
  private int LoginBonusCloseCounter;
  private int LevelUpBonusCloseCounter;
  private int LevelUpBonusCount;
  private bool nowMenuTween;
  private Modified<Player> playerModified;
  private bool isDepartDate;

  public SeaDateDateSpotDisplaySetting DateSetting
  {
    get
    {
      return this.dateSetting;
    }
    set
    {
      this.dateSetting = value;
    }
  }

  public List<int> DateFlows
  {
    get
    {
      return this.dateFlows;
    }
    set
    {
      this.dateFlows = value;
    }
  }

  public int? QuizId
  {
    get
    {
      return this.quizId;
    }
    set
    {
      this.quizId = value;
    }
  }

  public int NowFlowIndex
  {
    get
    {
      return this.nowFlowIndex;
    }
    set
    {
      this.nowFlowIndex = value;
    }
  }

  public bool ExistSelect
  {
    get
    {
      return this.existSelect;
    }
    set
    {
      this.existSelect = value;
    }
  }

  public Dictionary<int, int> EventSelectData
  {
    get
    {
      return this.eventSelectData;
    }
    set
    {
      this.eventSelectData = value;
    }
  }

  public IEnumerator onStartSceneAsync(bool isDuringDate)
  {
    Sea030HomeMenu sea030HomeMenu1 = this;
    if (sea030HomeMenu1.playerModified == null)
      sea030HomeMenu1.playerModified = SMManager.Observe<Player>();
    sea030HomeMenu1.manager.gameObject.SetActive(true);
    sea030HomeMenu1.mode = Sea030HomeMenu.HomeMode.Appreciation;
    sea030HomeMenu1.ActiveHome(true, false);
    sea030HomeMenu1.talkRoot.SetActive(false);
    sea030HomeMenu1.talkBackButton.SetActive(false);
    sea030HomeMenu1.resultEffectObject.SetActive(false);
    sea030HomeMenu1.resultGaugeEffectObject.SetActive(false);
    sea030HomeMenu1.tweeners = sea030HomeMenu1.gameObject.GetComponentsInChildren<UITweener>(true);
    ((IEnumerable<UITweener>) sea030HomeMenu1.tweeners).ForEach<UITweener>((System.Action<UITweener>) (x =>
    {
      if (x.tweenGroup != 3012 && x.tweenGroup != 3021)
        return;
      x.ResetToBeginning();
    }));
    if (sea030HomeMenu1.manager.CameraMode == SeaHomeCameraController.CameraMode.NORMAL)
      ((IEnumerable<GameObject>) sea030HomeMenu1.cameraButtons).ToggleOnce(0);
    else
      ((IEnumerable<GameObject>) sea030HomeMenu1.cameraButtons).ToggleOnce(1);
    sea030HomeMenu1.characterNameLabel.SetTextLocalize(string.Empty);
    sea030HomeMenu1.textLabel.SetTextLocalize(string.Empty);
    IEnumerator e = sea030HomeMenu1.manager.Init();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Future<GameObject> futureCallPopupPrefab;
    if ((UnityEngine.Object) sea030HomeMenu1.callPopupPrefab == (UnityEngine.Object) null)
    {
      futureCallPopupPrefab = Res.Prefabs.popup.popup_030_sea_mypage_call__anim_fade.Load<GameObject>();
      e = futureCallPopupPrefab.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      sea030HomeMenu1.callPopupPrefab = futureCallPopupPrefab.Result;
      futureCallPopupPrefab = (Future<GameObject>) null;
    }
    else if ((UnityEngine.Object) sea030HomeMenu1.callPopupObject != (UnityEngine.Object) null)
    {
      sea030HomeMenu1.callPopupObject.ScrollReset();
      sea030HomeMenu1.callPopupObject.Hide();
      sea030HomeMenu1.manager.AllUnitShow();
      sea030HomeMenu1.manager.SetCameraAutoFocus(true);
      sea030HomeMenu1.IsPush = false;
    }
    if ((UnityEngine.Object) sea030HomeMenu1.presentPopupPrefab == (UnityEngine.Object) null)
    {
      futureCallPopupPrefab = Res.Prefabs.popup.popup_030_sea_present__anim_fade.Load<GameObject>();
      e = futureCallPopupPrefab.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      sea030HomeMenu1.presentPopupPrefab = futureCallPopupPrefab.Result;
      futureCallPopupPrefab = (Future<GameObject>) null;
    }
    if ((UnityEngine.Object) sea030HomeMenu1.dateSelectPopupPrefab == (UnityEngine.Object) null)
    {
      futureCallPopupPrefab = Res.Prefabs.popup.popup_030_sea_mypage_date_select__anim_fade.Load<GameObject>();
      e = futureCallPopupPrefab.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      sea030HomeMenu1.dateSelectPopupPrefab = futureCallPopupPrefab.Result;
      futureCallPopupPrefab = (Future<GameObject>) null;
    }
    if ((UnityEngine.Object) sea030HomeMenu1.dateConfirmPopupPrefab == (UnityEngine.Object) null)
    {
      futureCallPopupPrefab = Res.Prefabs.popup.popup_030_sea_mypage_date_confirm__anim_fade.Load<GameObject>();
      e = futureCallPopupPrefab.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      sea030HomeMenu1.dateConfirmPopupPrefab = futureCallPopupPrefab.Result;
      futureCallPopupPrefab = (Future<GameObject>) null;
    }
    if ((UnityEngine.Object) sea030HomeMenu1.favorabilityRatingEffectPopupPrefab == (UnityEngine.Object) null)
    {
      futureCallPopupPrefab = Res.Animations.extraskill.FavorabilityRatingEffect.Load<GameObject>();
      e = futureCallPopupPrefab.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      sea030HomeMenu1.favorabilityRatingEffectPopupPrefab = futureCallPopupPrefab.Result;
      futureCallPopupPrefab = (Future<GameObject>) null;
    }
    if ((UnityEngine.Object) sea030HomeMenu1.skillDetailDialogPrefab == (UnityEngine.Object) null)
    {
      futureCallPopupPrefab = PopupSkillDetails.createPrefabLoader(true);
      e = futureCallPopupPrefab.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      sea030HomeMenu1.skillDetailDialogPrefab = futureCallPopupPrefab.Result;
      futureCallPopupPrefab = (Future<GameObject>) null;
    }
    if (isDuringDate)
    {
      // ISSUE: reference to a compiler-generated method
      Future<WebAPI.Response.SeaDateResume> futureAPI = WebAPI.SeaDateResume(new System.Action<WebAPI.Response.UserError>(sea030HomeMenu1.\u003ConStartSceneAsync\u003Eb__86_1));
      e = futureAPI.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      PlayerUnit playerUnit = ((IEnumerable<PlayerUnit>) SMManager.Get<PlayerUnit[]>()).FirstOrDefault<PlayerUnit>((Func<PlayerUnit, bool>) (x => x.id == futureAPI.Result.player_unit_id));
      sea030HomeMenu1.existSelect = futureAPI.Result.happening_id > 0 || futureAPI.Result.quiz_id > 0;
      sea030HomeMenu1.dateFlows = ((IEnumerable<int>) futureAPI.Result.script_ids).ToList<int>();
      sea030HomeMenu1.nowFlowIndex = 0;
      sea030HomeMenu1.eventSelectData.Clear();
      sea030HomeMenu1.quizId = Sea030HomeMenu.hasQuiz(futureAPI.Result.date_flow) ? new int?(futureAPI.Result.quiz_id) : new int?();
      sea030HomeMenu1.dateSetting = ((IEnumerable<SeaDateDateSpotDisplaySetting>) MasterData.SeaDateDateSpotDisplaySettingList).FirstOrDefault<SeaDateDateSpotDisplaySetting>((Func<SeaDateDateSpotDisplaySetting, bool>) (x => x.datespot.ID == futureAPI.Result.spot.ID && x.time_zone.WithIn(futureAPI.Result.started_at)));
      e = sea030HomeMenu1.InternalChangeTalkMode(new SeaHomeManager.UnitConrtolleData(playerUnit.unit.ID, playerUnit));
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      List<int> dateFlows = sea030HomeMenu1.dateFlows;
      Sea030HomeMenu sea030HomeMenu2 = sea030HomeMenu1;
      int nowFlowIndex = sea030HomeMenu1.nowFlowIndex;
      int num = nowFlowIndex + 1;
      sea030HomeMenu2.nowFlowIndex = num;
      int index = nowFlowIndex;
      Story0093Scene.changeSceneModeDate(true, dateFlows[index], sea030HomeMenu1.current2DUnitData.Unit, sea030HomeMenu1.dateSetting.GetImageHash(), new System.Action<int, int>(sea030HomeMenu1.EventSelected), false, new bool?(true), sea030HomeMenu1.quizId);
    }
    sea030HomeMenu1.isInit = true;
  }

  public IEnumerator onBackSceneAsync()
  {
    Sea030HomeMenu sea030HomeMenu1 = this;
    bool flag = false;
    IEnumerator e1;
    if (!sea030HomeMenu1.isInit)
    {
      e1 = sea030HomeMenu1.onStartSceneAsync(false);
      while (e1.MoveNext())
        yield return e1.Current;
      e1 = (IEnumerator) null;
      flag = true;
    }
    if (sea030HomeMenu1.dateFlows != null && !sea030HomeMenu1.isSelectedSpot)
    {
      if (sea030HomeMenu1.dateFlows.Count > sea030HomeMenu1.nowFlowIndex)
      {
        List<int> dateFlows = sea030HomeMenu1.dateFlows;
        Sea030HomeMenu sea030HomeMenu2 = sea030HomeMenu1;
        int nowFlowIndex = sea030HomeMenu1.nowFlowIndex;
        int num = nowFlowIndex + 1;
        sea030HomeMenu2.nowFlowIndex = num;
        int index = nowFlowIndex;
        Story0093Scene.changeSceneModeDate(true, dateFlows[index], sea030HomeMenu1.current2DUnitData.Unit, sea030HomeMenu1.dateSetting.GetImageHash(), new System.Action<int, int>(sea030HomeMenu1.EventSelected), false, new bool?(true), sea030HomeMenu1.quizId);
      }
      else if (sea030HomeMenu1.existSelect && sea030HomeMenu1.eventSelectData.Count > 0)
      {
        Sea030HomeMenu sea030HomeMenu = sea030HomeMenu1;
        bool isSeaSeasonEnd = false;
        Future<WebAPI.Response.SeaDateChoice> futureAPI = WebAPI.SeaDateChoice(sea030HomeMenu1.eventSelectData.OrderBy<KeyValuePair<int, int>, int>((Func<KeyValuePair<int, int>, int>) (x => x.Key)).Last<KeyValuePair<int, int>>().Value + 1, (System.Action<WebAPI.Response.UserError>) (e =>
        {
          if (string.Equals(e.Code, "SEA000"))
          {
            isSeaSeasonEnd = true;
            sea030HomeMenu.StartCoroutine(PopupUtility.SeaError(e));
          }
          else
            WebAPI.DefaultUserErrorCallback(e);
        }));
        e1 = futureAPI.Wait();
        while (e1.MoveNext())
          yield return e1.Current;
        e1 = (IEnumerator) null;
        if (!isSeaSeasonEnd)
        {
          sea030HomeMenu1.existSelect = futureAPI.Result.happening_id > 0 || futureAPI.Result.quiz_id > 0;
          sea030HomeMenu1.dateFlows = ((IEnumerable<int>) futureAPI.Result.script_ids).ToList<int>();
          sea030HomeMenu1.nowFlowIndex = 0;
          sea030HomeMenu1.eventSelectData.Clear();
          sea030HomeMenu1.quizId = Sea030HomeMenu.hasQuiz(futureAPI.Result.date_flow) ? new int?(futureAPI.Result.quiz_id) : new int?();
          List<int> dateFlows = sea030HomeMenu1.dateFlows;
          Sea030HomeMenu sea030HomeMenu2 = sea030HomeMenu1;
          int nowFlowIndex = sea030HomeMenu1.nowFlowIndex;
          int num = nowFlowIndex + 1;
          sea030HomeMenu2.nowFlowIndex = num;
          int index = nowFlowIndex;
          Story0093Scene.changeSceneModeDate(true, dateFlows[index], sea030HomeMenu1.current2DUnitData.Unit, sea030HomeMenu1.dateSetting.GetImageHash(), new System.Action<int, int>(sea030HomeMenu1.EventSelected), false, new bool?(true), sea030HomeMenu1.quizId);
        }
      }
      else
      {
        Sea030HomeMenu sea030HomeMenu = sea030HomeMenu1;
        bool isSeaSeasonEnd = false;
        Future<WebAPI.Response.SeaDateFinish> futureAPI = WebAPI.SeaDateFinish((System.Action<WebAPI.Response.UserError>) (e =>
        {
          if (string.Equals(e.Code, "SEA000"))
          {
            isSeaSeasonEnd = true;
            sea030HomeMenu.StartCoroutine(PopupUtility.SeaError(e));
          }
          else
          {
            WebAPI.DefaultUserErrorCallback(e);
            Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
          }
        }));
        e1 = futureAPI.Wait();
        while (e1.MoveNext())
          yield return e1.Current;
        e1 = (IEnumerator) null;
        if (!isSeaSeasonEnd)
        {
          float trustUp = futureAPI.Result.trust_up;
          // ISSUE: reference to a compiler-generated method
          PlayerUnit unit = ((IEnumerable<PlayerUnit>) SMManager.Get<PlayerUnit[]>()).FirstOrDefault<PlayerUnit>(new Func<PlayerUnit, bool>(sea030HomeMenu1.\u003ConBackSceneAsync\u003Eb__87_3));
          if (unit != (PlayerUnit) null)
          {
            trustUp = unit.trust_rate - sea030HomeMenu1.current2DUnitData.PlayerUnit.trust_rate;
            sea030HomeMenu1.current2DUnitData.PlayerUnit = unit;
            sea030HomeMenu1.SetUnitData(sea030HomeMenu1.current2DUnitData);
          }
          SeaDateResult seaDateResult = sea030HomeMenu1.GetSeaDateResult(futureAPI.Result.trust_up);
          if (seaDateResult != null)
          {
            sea030HomeMenu1.SetSerif(seaDateResult.serif, seaDateResult.face, seaDateResult.voice_cue_name, (Hashtable) null);
            sea030HomeMenu1.PlayLoveResultEffect(seaDateResult.result_staging.home_result, trustUp, futureAPI.Result.gain_trust_result, unit);
            sea030HomeMenu1.eyeBlink.StartBlink(2f);
          }
          sea030HomeMenu1.manager.gameObject.SetActive(true);
          sea030HomeMenu1.dateFlows = (List<int>) null;
          sea030HomeMenu1.existSelect = false;
          sea030HomeMenu1.nowFlowIndex = 0;
          Singleton<CommonRoot>.GetInstance().loadingMode = 0;
        }
      }
    }
    else
    {
      sea030HomeMenu1.manager.gameObject.SetActive(true);
      if (!sea030HomeMenu1.manager.isUnitInit && !flag)
      {
        e1 = sea030HomeMenu1.manager.Init();
        while (e1.MoveNext())
          yield return e1.Current;
        e1 = (IEnumerator) null;
      }
      if (sea030HomeMenu1.isReturnSelectSpot || sea030HomeMenu1.isSelectedSpot)
        sea030HomeMenu1.StartCoroutine(sea030HomeMenu1.onBackForSelectSpot());
      if ((UnityEngine.Object) sea030HomeMenu1.callPopupObject != (UnityEngine.Object) null && sea030HomeMenu1.callPopupObject.IsInitialize)
        sea030HomeMenu1.callPopupObject.UpdateUnitData();
    }
  }

  private IEnumerator onBackForSelectSpot()
  {
    Sea030HomeMenu sea030HomeMenu1 = this;
    Singleton<CommonRoot>.GetInstance().isTouchBlock = true;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    yield return (object) new WaitForSeconds(0.3f);
    if (sea030HomeMenu1.isSelectedSpot)
    {
      while (Singleton<CommonRoot>.GetInstance().isTouchBlock)
        yield return (object) null;
      Singleton<CommonRoot>.GetInstance().isTouchBlock = true;
    }
    Singleton<CommonRoot>.GetInstance().isLoading = false;
    if (sea030HomeMenu1.isSelectedSpot)
    {
      NGSoundManager sm = Singleton<NGSoundManager>.GetInstance();
      int playVoiceIndex = -1;
      SeaDateSerifAtDepart serifDepart = sea030HomeMenu1.GetSerifDepart();
      if (serifDepart != null)
        playVoiceIndex = sea030HomeMenu1.SetSerif(serifDepart.serif, serifDepart.face, serifDepart.voice_cue_name, new Hashtable()
        {
          {
            (object) "dateName",
            (object) sea030HomeMenu1.dateSetting.date_name
          }
        });
      sm.PlaySe("SE_2702", false, 0.0f, -1);
      yield return (object) new WaitForSeconds(1f);
      while (playVoiceIndex >= 0 && sm.IsVoicePlaying(playVoiceIndex))
        yield return (object) null;
      Singleton<CommonRoot>.GetInstance().isActiveHeader = false;
      List<int> dateFlows = sea030HomeMenu1.dateFlows;
      Sea030HomeMenu sea030HomeMenu2 = sea030HomeMenu1;
      int nowFlowIndex = sea030HomeMenu1.nowFlowIndex;
      int num = nowFlowIndex + 1;
      sea030HomeMenu2.nowFlowIndex = num;
      int index = nowFlowIndex;
      Story0093Scene.changeSceneModeDate(true, dateFlows[index], sea030HomeMenu1.current2DUnitData.Unit, sea030HomeMenu1.dateSetting.GetImageHash(), new System.Action<int, int>(sea030HomeMenu1.EventSelected), false, new bool?(true), sea030HomeMenu1.quizId);
      sm = (NGSoundManager) null;
    }
    sea030HomeMenu1.isReturnSelectSpot = false;
    sea030HomeMenu1.isSelectedSpot = false;
  }

  public void onStartScene()
  {
    this.manager.ResetAmbientLight();
  }

  private void ActiveBanner(bool active)
  {
    if (active)
    {
      if (this.bannerModified == null)
        this.bannerModified = SMManager.Observe<SM.Banner[]>();
      if (this.isLoadBanner && !this.bannerModified.IsChangedOnce())
        return;
      this.isLoadBanner = false;
      this.StartCoroutine("LoadBanner");
    }
    else
    {
      this.StopCoroutine("LoadBanner");
      this.bannerParent.Clear();
      this.isLoadBanner = false;
    }
  }

  private IEnumerator LoadBanner()
  {
    Sea030HomeMenu sea030HomeMenu = this;
    if ((UnityEngine.Object) sea030HomeMenu.bannerParent != (UnityEngine.Object) null)
    {
      // ISSUE: reference to a compiler-generated method
      IEnumerator e = sea030HomeMenu.bannerParent.BannerCreate(false, new System.Action(sea030HomeMenu.\u003CLoadBanner\u003Eb__91_0), false);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      sea030HomeMenu.isLoadBanner = true;
    }
  }

  private void ActiveHome(bool state, bool camBtn = false)
  {
    this.bannerParent.gameObject.SetActive(state);
    if (camBtn)
      this.buttonsRoot.SetActive(state);
    else
      this.homeRoot.SetActive(state);
    this.ActiveBanner(state);
  }

  public void onEndScene()
  {
    this.StopCoroutine("LoadUnit2D");
    if (!this.ExistDateFlows() && !this.isChangeSelectSpot && this.mode == Sea030HomeMenu.HomeMode.Talk)
      this.ChangeAppreciationMode();
    this.manager.onEndScene();
    this.manager.gameObject.SetActive(false);
    this.isChangeSelectSpot = false;
  }

  public void OnChangeCameraMode()
  {
    if (this.IsPush)
      return;
    this.manager.ChangeCameraMode();
    if (this.manager.CameraMode == SeaHomeCameraController.CameraMode.NORMAL)
    {
      ((IEnumerable<GameObject>) this.cameraButtons).ToggleOnce(0);
      this.ActiveHome(true, true);
    }
    else
    {
      ((IEnumerable<GameObject>) this.cameraButtons).ToggleOnce(1);
      this.ActiveHome(false, true);
    }
  }

  public void OnChangeTalkMode(SeaHomeManager.UnitConrtolleData unitData)
  {
    if (this.IsPush)
      return;
    Singleton<NGSoundManager>.GetInstance().playSE("SE_1040", false, 0.0f, -1);
    this.ChangeTalkMode(unitData);
  }

  public void OnCall()
  {
    if (this.mode == Sea030HomeMenu.HomeMode.Talk || this.IsPushAndSet())
      return;
    this.manager.AllUnitHide();
    this.manager.SetCameraAutoFocus(false);
    if (this.manager.CameraMode == SeaHomeCameraController.CameraMode.NORMAL)
      ((IEnumerable<GameObject>) this.cameraButtons).ToggleOnce(0);
    else
      ((IEnumerable<GameObject>) this.cameraButtons).ToggleOnce(1);
    CommonRoot instance = Singleton<CommonRoot>.GetInstance();
    instance.loadingMode = 1;
    instance.isLoading = true;
    this.StartCoroutine("OpneCallPopup");
  }

  public void OnSeaQuest()
  {
    if (this.mode == Sea030HomeMenu.HomeMode.Talk || this.IsPushAndSet())
      return;
    Singleton<NGGameDataManager>.GetInstance().IsSea = true;
    if (!(Singleton<NGSceneManager>.GetInstance().sceneName != "sea030_quest"))
      return;
    this.ClearSceneStacks();
    Sea030_questScene.ChangeScene(true, true, false);
  }

  private void ClearSceneStacks()
  {
    NGSceneManager instance = Singleton<NGSceneManager>.GetInstance();
    instance.sceneBase.IsPush = true;
    instance.destroyLoadedScenes();
    instance.clearStack();
  }

  private IEnumerator OpneCallPopup()
  {
    Sea030HomeMenu sea030HomeMenu = this;
    if ((UnityEngine.Object) sea030HomeMenu.callPopupObject == (UnityEngine.Object) null)
    {
      GameObject gameObject = sea030HomeMenu.callPopupPrefab.Clone(sea030HomeMenu.callDialogRoot.transform);
      sea030HomeMenu.callPopupObject = gameObject.GetComponent<Sea030HomeCallPopup>();
    }
    IEnumerator e;
    if ((UnityEngine.Object) sea030HomeMenu.callPopupObject != (UnityEngine.Object) null && !sea030HomeMenu.callPopupObject.IsInitialize)
    {
      // ISSUE: reference to a compiler-generated method
      e = sea030HomeMenu.callPopupObject.Init(((IEnumerable<SeaHomeUnitController>) sea030HomeMenu.manager.UnitControlers).Select<SeaHomeUnitController, SeaHomeManager.UnitConrtolleData>((Func<SeaHomeUnitController, SeaHomeManager.UnitConrtolleData>) (x => x.UnitData)).ToArray<SeaHomeManager.UnitConrtolleData>(), new System.Action<SeaHomeManager.UnitConrtolleData>(sea030HomeMenu.ChangeTalkMode), new System.Action(sea030HomeMenu.\u003COpneCallPopup\u003Eb__99_1), sea030HomeMenu.manager.IsShowAllGuest);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    else
    {
      e = sea030HomeMenu.callPopupObject.UpdateUnitIcon();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    CommonRoot instance = Singleton<CommonRoot>.GetInstance();
    instance.isLoading = false;
    instance.loadingMode = 0;
    sea030HomeMenu.callPopupObject.Show();
    sea030HomeMenu.callPopupObject.StartLoadThum();
  }

  public void OnPresent()
  {
    if (this.mode == Sea030HomeMenu.HomeMode.Appreciation || this.IsPushAndSet())
      return;
    CommonRoot instance = Singleton<CommonRoot>.GetInstance();
    instance.loadingMode = 1;
    instance.isLoading = true;
    this.StartCoroutine("OpnePresentPopup");
  }

  private IEnumerator OpnePresentPopup()
  {
    Sea030HomeMenu sea030HomeMenu = this;
    GameObject popup = sea030HomeMenu.presentPopupPrefab.Clone(Singleton<CommonRoot>.GetInstance().LoadTmpObj.transform);
    IEnumerator e = popup.GetComponent<Sea030HomePresentPopup>().Init(sea030HomeMenu.current2DUnitData.PlayerUnit, new System.Action<GameCore.ItemInfo, int>(sea030HomeMenu.PresentGive));
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    CommonRoot instance = Singleton<CommonRoot>.GetInstance();
    instance.isLoading = false;
    instance.loadingMode = 0;
    popup.SetActive(false);
    Singleton<PopupManager>.GetInstance().open(popup, false, false, true, true, false, false, "SE_1006");
    popup.SetActive(true);
  }

  public void OnDate()
  {
    if (this.mode == Sea030HomeMenu.HomeMode.Appreciation || this.IsPushAndSet())
      return;
    this.isChangeSelectSpot = true;
    Sea030DateScene.changeScene(true, this, this.current2DUnitData);
  }

  public void OnBack()
  {
    if (this.IsPush)
      return;
    this.ChangeAppreciationMode();
  }

  public void OnCharacterQuest()
  {
    if (this.mode == Sea030HomeMenu.HomeMode.Appreciation || this.current2DUnitData == null || this.IsPushAndSet())
      return;
    Singleton<NGGameDataManager>.GetInstance().IsSea = false;
    NGSceneManager instance = Singleton<NGSceneManager>.GetInstance();
    instance.sceneBase.IsPush = true;
    instance.destroyLoadedScenes();
    instance.clearStack();
    Quest00214Scene.ChangeScene(false, this.current2DUnitData.Unit.same_character_id, false, true);
  }

  public void OnClicExtraSKill()
  {
    PlayerUnit playerUnit = this.current2DUnitData.PlayerUnit;
    UnitSkillAwake[] awakeSkills = playerUnit.GetAwakeSkills();
    if (awakeSkills == null || awakeSkills.Length == 0)
      return;
    UnitSkillAwake s = awakeSkills[0];
    this.skillParams = new PopupSkillDetails.Param(s.skill, UnitParameter.SkillGroup.Extra, new int?());
    string empty = string.Empty;
    if ((double) s.need_affection > (double) playerUnit.trust_rate)
    {
      Consts.Format(Consts.GetInstance().popup_004_ExtraSkill_affection_condition, (IDictionary) new Hashtable()
      {
        {
          (object) "percent",
          (object) s.need_affection
        }
      });
    }
    else
    {
      string affectionComplete = Consts.GetInstance().popup_004_ExtraSkill_affection_complete;
    }
    PopupSkillDetails.show(this.skillDetailDialogPrefab, PopupSkillDetails.Param.createByUnitView(s, playerUnit, (System.Action) null), false, (System.Action) null, false);
  }

  private void FinishTween()
  {
    this.nowMenuTween = false;
    this.ActiveHome(this.mode == Sea030HomeMenu.HomeMode.Appreciation, false);
    this.talkRoot.SetActive(this.mode == Sea030HomeMenu.HomeMode.Talk);
    this.talkBackButton.SetActive(this.mode == Sea030HomeMenu.HomeMode.Talk);
  }

  public void ChangeTalkMode(SeaHomeManager.UnitConrtolleData unitData)
  {
    this.IsPush = false;
    if (this.mode == Sea030HomeMenu.HomeMode.Talk)
      return;
    this.mode = Sea030HomeMenu.HomeMode.Talk;
    this.talkRoot.SetActive(true);
    this.talkBackButton.SetActive(true);
    this.manager.AllUnitHide();
    this.manager.SetCameraAutoFocus(false);
    this.characterNameLabel.SetTextLocalize(unitData.Unit.name);
    this.textLabel.SetTextLocalize(string.Empty);
    if (this.current2DUnitData == null || this.current2DUnitData != null && this.current2DUnitData.UnitID != unitData.UnitID)
    {
      this.current2DUnitData = unitData;
      this.StartCoroutine("LoadUnit2D", (object) unitData);
    }
    else
    {
      this.current2DUnitData = unitData;
      this.SetUnitData(unitData);
      this.StartCoroutine(this.waitTweenPlaySerif());
    }
    ((IEnumerable<UITweener>) this.tweeners).ForEach<UITweener>((System.Action<UITweener>) (x =>
    {
      x.onFinished.Clear();
      x.enabled = false;
    }));
    if (!NGTween.playTweens(this.tweeners, 3012, false))
      return;
    this.nowMenuTween = true;
    UITweener uiTweener = ((IEnumerable<UITweener>) this.tweeners).Where<UITweener>((Func<UITweener, bool>) (x => x.gameObject.activeInHierarchy && x.enabled)).OrderByDescending<UITweener, float>((Func<UITweener, float>) (x => x.delay + x.duration)).FirstOrDefault<UITweener>();
    if (!((UnityEngine.Object) uiTweener != (UnityEngine.Object) null))
      return;
    uiTweener.onFinished.Add(new EventDelegate(new EventDelegate.Callback(this.FinishTween)));
  }

  public IEnumerator InternalChangeTalkMode(SeaHomeManager.UnitConrtolleData unitData)
  {
    this.mode = Sea030HomeMenu.HomeMode.Talk;
    this.ActiveHome(false, false);
    this.talkRoot.SetActive(true);
    this.talkBackButton.SetActive(true);
    this.manager.AllUnitHide();
    this.manager.SetCameraAutoFocus(false);
    this.characterNameLabel.SetTextLocalize(unitData.Unit.name);
    this.textLabel.SetTextLocalize(string.Empty);
    if (this.current2DUnitData == null || this.current2DUnitData != null && this.current2DUnitData.UnitID != unitData.UnitID)
    {
      this.current2DUnitData = unitData;
      IEnumerator e = this.LoadUnit2D(this.current2DUnitData, false);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    else
    {
      this.current2DUnitData = unitData;
      this.SetUnitData(unitData);
    }
  }

  private void SetUnitData(SeaHomeManager.UnitConrtolleData unitData)
  {
    Player player = SMManager.Get<Player>();
    if (unitData.PlayerUnit == (PlayerUnit) null)
    {
      this.BtnDate.isEnabled = false;
      this.BtnPresent.isEnabled = false;
      this.GaugeObject.SetActive(false);
      this.ibtnCharacterQuest.gameObject.SetActive(false);
    }
    else
    {
      this.BtnDate.isEnabled = player.IsSeaDate() && !((IEnumerable<SeaDateProhibition>) MasterData.SeaDateProhibitionList).Any<SeaDateProhibition>((Func<SeaDateProhibition, bool>) (x =>
      {
        int? characterIdUnitUnit = x.same_character_id_UnitUnit;
        int sameCharacterId = unitData.PlayerUnit.unit.same_character_id;
        return characterIdUnitUnit.GetValueOrDefault() == sameCharacterId & characterIdUnitUnit.HasValue;
      }));
      this.BtnPresent.isEnabled = true;
      this.GaugeObject.SetActive(true);
      this.StartCoroutine(this.loveGauge.setValue((int) unitData.PlayerUnit.trust_rate, (int) unitData.PlayerUnit.trust_rate, (int) unitData.PlayerUnit.trust_max_rate, (int) Consts.GetInstance().TRUST_RATE_LEVEL_SIZE, false, false));
      this.trustLabel.SetTextLocalize(string.Format("{0}{1}", (object) (Math.Round((double) unitData.PlayerUnit.trust_rate * 100.0) / 100.0), (object) Consts.GetInstance().PERCENT));
      if (unitData.PlayerUnit != (PlayerUnit) null)
      {
        UnitSkillAwake[] awakeSkills = unitData.PlayerUnit.GetAwakeSkills();
        bool flag = awakeSkills != null && (uint) awakeSkills.Length > 0U;
        this.dirExtraSkill.SetActive(flag);
        if (flag)
        {
          if ((double) awakeSkills[0].need_affection <= (double) unitData.PlayerUnit.trust_rate)
            this.slcExtraSkill.color = Color.white;
          else
            this.slcExtraSkill.color = Color.gray;
        }
      }
      this.ibtnCharacterQuest.gameObject.SetActive(!Mathf.Approximately(unitData.PlayerUnit.trust_max_rate, (float) PlayerUnit.GetTrustRateMax()));
    }
    this.commingSoon.SetActive(!player.IsSeaDate());
    bool flag1 = false;
    if (unitData.PlayerUnit != (PlayerUnit) null)
    {
      if (Mathf.Approximately(unitData.PlayerUnit.trust_rate, (float) PlayerUnit.GetTrustRateMax()))
      {
        try
        {
          if (!Persist.seaHomeUnitDate.Data.TrustMaxSameUnitIDs.Contains(unitData.Unit.same_character_id))
          {
            flag1 = true;
            Persist.seaHomeUnitDate.Data.TrustMaxSameUnitIDs.Add(unitData.Unit.same_character_id);
            Persist.seaHomeUnitDate.Flush();
          }
        }
        catch
        {
          Persist.seaHomeUnitDate.Delete();
        }
      }
    }
    this.serifIndexCount = 0;
    SeaHomeSerif[] serifs = ((IEnumerable<SeaHomeSerif>) MasterData.SeaHomeSerifList).GetSerifs(ServerTime.NowAppTimeAddDelta(), unitData.Unit, unitData.PlayerUnit != (PlayerUnit) null, unitData.PlayerUnit != (PlayerUnit) null ? (int) unitData.PlayerUnit.trust_rate : 0);
    this.serifIndex = new List<int>();
    foreach (SeaHomeSerif seaHomeSerif in serifs)
    {
      for (int index = 0; index < seaHomeSerif.weiht; ++index)
        this.serifIndex.Add(seaHomeSerif.ID);
    }
    this.serifIndex = this.serifIndex.Shuffle<int>().ToList<int>();
    if (!flag1)
      return;
    SeaHomeSerif seaHomeSerif1 = ((IEnumerable<SeaHomeSerif>) serifs).FirstOrDefault<SeaHomeSerif>((Func<SeaHomeSerif, bool>) (x => x.is_rare));
    if (seaHomeSerif1 == null)
      return;
    this.serifIndex.Insert(0, seaHomeSerif1.ID);
  }

  private IEnumerator LoadUnit2D(SeaHomeManager.UnitConrtolleData unitData)
  {
    IEnumerator e = this.LoadUnit2D(unitData, true);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  private IEnumerator LoadUnit2D(
    SeaHomeManager.UnitConrtolleData unitData,
    bool playSerif)
  {
    this.dirExtraSkill.SetActive(false);
    this.SetUnitData(unitData);
    UnitUnit Unit = unitData.Unit;
    this.dynUnit2D.transform.Clear();
    Future<GameObject> future = Unit.LoadStory();
    IEnumerator e = future.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    UnityEngine.Sprite sprite = (UnityEngine.Sprite) null;
    Future<UnityEngine.Sprite> spriteFuture;
    if (Unit.ExistSpriteStory())
    {
      spriteFuture = Unit.LoadSpriteStory();
      e = spriteFuture.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      sprite = spriteFuture.Result;
      spriteFuture = (Future<UnityEngine.Sprite>) null;
    }
    GameObject gameObject = future.Result.Clone(this.dynUnit2D.transform);
    Unit.SetStoryData(gameObject, "normal", new int?());
    NGxMaskSpriteWithScale component1 = gameObject.GetComponent<NGxMaskSpriteWithScale>();
    if ((UnityEngine.Object) sprite != (UnityEngine.Object) null)
      component1.MainUI2DSprite.sprite2D = sprite;
    component1.SetMaskEnable(false);
    UIWidget component2 = this.dynUnit2D.GetComponent<UIWidget>();
    if ((UnityEngine.Object) component2 != (UnityEngine.Object) null)
    {
      UIWidget w = gameObject.GetComponent<UIWidget>();
      w.depth = component2.depth;
      UIWidget[] componentsInChildren = gameObject.GetComponentsInChildren<UIWidget>();
      ((IEnumerable<UIWidget>) componentsInChildren).Where<UIWidget>((Func<UIWidget, bool>) (v => v.transform.name == "face")).ForEach<UIWidget>((System.Action<UIWidget>) (v => v.depth = w.depth + 1));
      ((IEnumerable<UIWidget>) componentsInChildren).Where<UIWidget>((Func<UIWidget, bool>) (v => v.transform.name == "eye")).ForEach<UIWidget>((System.Action<UIWidget>) (v => v.depth = w.depth + 2));
    }
    this.eyeBlink = gameObject.GetOrAddComponent<SeaHomeUnitEyeBlink>();
    this.unitFace = gameObject.GetComponent<NGxFaceSprite>();
    this.unitEye = gameObject.GetComponent<NGxEyeSprite>();
    this.eyeBlink.Init(Unit, this.unitFace, this.unitEye);
    Future<UnityEngine.Sprite> thumFuture = Unit.LoadSpriteThumbnail();
    e = thumFuture.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    UnityEngine.Sprite result = thumFuture.Result;
    this.unitThum.sprite2D = result;
    UI2DSprite unitThum1 = this.unitThum;
    Rect rect = result.rect;
    int width = (int) rect.width;
    unitThum1.width = width;
    UI2DSprite unitThum2 = this.unitThum;
    rect = result.rect;
    int height = (int) rect.height;
    unitThum2.height = height;
    if (unitData.PlayerUnit != (PlayerUnit) null)
    {
      PlayerUnit playerUnit = unitData.PlayerUnit;
      UnitSkillAwake[] awakeSkills = playerUnit.GetAwakeSkills();
      bool flag = awakeSkills != null && (uint) awakeSkills.Length > 0U;
      this.dirExtraSkill.SetActive(flag);
      if (flag)
      {
        UnitSkillAwake awakeSkill = awakeSkills[0];
        spriteFuture = awakeSkill.skill.LoadBattleSkillIcon((BattleFuncs.InvestSkill) null);
        e = spriteFuture.Wait();
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        this.slcExtraSkill.sprite2D = spriteFuture.Result;
        if ((double) awakeSkill.need_affection <= (double) playerUnit.trust_rate)
          this.slcExtraSkill.color = Color.white;
        else
          this.slcExtraSkill.color = Color.gray;
        awakeSkill = (UnitSkillAwake) null;
        spriteFuture = (Future<UnityEngine.Sprite>) null;
      }
      playerUnit = (PlayerUnit) null;
    }
    Singleton<ResourceManager>.GetInstance().ClearCache();
    Resources.UnloadUnusedAssets();
    if (playSerif)
    {
      while (this.nowMenuTween)
        yield return (object) null;
      this.TapSerif();
    }
  }

  private IEnumerator waitTweenPlaySerif()
  {
    yield return (object) null;
    while (this.nowMenuTween)
      yield return (object) null;
    this.TapSerif();
  }

  public void TapSerif()
  {
    if (this.serifIndex.Count <= 0)
      return;
    if (this.serifIndexCount >= this.serifIndex.Count)
    {
      this.serifIndex = this.serifIndex.Shuffle<int>().ToList<int>();
      this.serifIndexCount = 0;
    }
    SeaHomeSerif seaHomeSerif = MasterData.SeaHomeSerif[this.serifIndex[this.serifIndexCount++]];
    this.SetSerif(seaHomeSerif.serif, seaHomeSerif.face, seaHomeSerif.voice_cue_name, (Hashtable) null);
    this.eyeBlink.StartBlink(2f);
  }

  public int SetSerif(string serif, string face, string voice_cue, Hashtable formtTables = null)
  {
    int num = -1;
    this.eyeBlink.StopBlink();
    if (formtTables == null || formtTables != null && !formtTables.ContainsKey((object) "userName"))
    {
      if (formtTables == null)
        formtTables = new Hashtable();
      formtTables.Add((object) "userName", (object) this.playerModified.Value.name);
    }
    this.textLabel.SetTextLocalize(Consts.Format(serif, (IDictionary) formtTables));
    if (!string.IsNullOrEmpty(face))
    {
      this.StartCoroutine(this.unitFace.ChangeFace(face));
      this.StartCoroutine(this.unitEye.ChangeEye("normal"));
    }
    if (!string.IsNullOrEmpty(voice_cue))
      num = Singleton<NGSoundManager>.GetInstance().playVoiceByStringID(this.current2DUnitData.Unit.unitVoicePattern, voice_cue, -1, 0.0f);
    return num;
  }

  public void ChangeAppreciationMode()
  {
    if (this.mode == Sea030HomeMenu.HomeMode.Appreciation)
      return;
    this.ResetPlayLoveResultEffect();
    this.mode = Sea030HomeMenu.HomeMode.Appreciation;
    this.ActiveHome(true, false);
    this.manager.AllUnitShow();
    this.manager.SetCameraAutoFocus(true);
    ((IEnumerable<UITweener>) this.tweeners).ForEach<UITweener>((System.Action<UITweener>) (x =>
    {
      x.onFinished.Clear();
      x.enabled = false;
    }));
    if (!NGTween.playTweens(this.tweeners, 3021, false))
      return;
    this.nowMenuTween = true;
    UITweener uiTweener = ((IEnumerable<UITweener>) this.tweeners).Where<UITweener>((Func<UITweener, bool>) (x => x.gameObject.activeInHierarchy && x.enabled)).OrderByDescending<UITweener, float>((Func<UITweener, float>) (x => x.delay + x.duration)).FirstOrDefault<UITweener>();
    if (!((UnityEngine.Object) uiTweener != (UnityEngine.Object) null))
      return;
    uiTweener.onFinished.Add(new EventDelegate(new EventDelegate.Callback(this.FinishTween)));
  }

  private void PresentGive(GameCore.ItemInfo present, int selected)
  {
    Singleton<CommonRoot>.GetInstance().isTouchBlock = true;
    this.StartCoroutine(this.internalPresentGive(present, selected));
  }

  private IEnumerator internalPresentGive(GameCore.ItemInfo present, int selected)
  {
    Sea030HomeMenu sea030HomeMenu = this;
    CommonRoot cr = Singleton<CommonRoot>.GetInstance();
    Singleton<PopupManager>.GetInstance().closeAllWithoutAnim(false);
    while (Singleton<PopupManager>.GetInstance().isOpen)
      yield return (object) null;
    List<int> intList = new List<int>();
    for (int index = 0; index < selected; ++index)
      intList.Add(present.itemID);
    cr.loadingMode = 1;
    // ISSUE: reference to a compiler-generated method
    Future<WebAPI.Response.SeaPresentGive> future = WebAPI.SeaPresentGive(intList.ToArray(), sea030HomeMenu.current2DUnitData.PlayerUnit.id, new System.Action<WebAPI.Response.UserError>(sea030HomeMenu.\u003CinternalPresentGive\u003Eb__117_0));
    IEnumerator e = future.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if (future.Result == null)
    {
      cr.isTouchBlock = false;
      cr.isLoading = false;
      cr.loadingMode = 0;
    }
    else
    {
      cr.isTouchBlock = false;
      cr.isLoading = false;
      cr.loadingMode = 0;
      // ISSUE: reference to a compiler-generated method
      PlayerUnit playerUnit = ((IEnumerable<PlayerUnit>) future.Result.player_units).FirstOrDefault<PlayerUnit>(new Func<PlayerUnit, bool>(sea030HomeMenu.\u003CinternalPresentGive\u003Eb__117_1));
      float trustUp = 0.0f;
      if (playerUnit != (PlayerUnit) null)
      {
        trustUp = playerUnit.trust_rate - sea030HomeMenu.current2DUnitData.PlayerUnit.trust_rate;
        sea030HomeMenu.manager.UpdateUnitData(playerUnit);
        sea030HomeMenu.current2DUnitData.PlayerUnit = playerUnit;
        sea030HomeMenu.SetUnitData(sea030HomeMenu.current2DUnitData);
      }
      SeaPresentPresentAffinity presentPresentAffnity = SeaPresentPresentAffinity.GetSeaPresentPresentAffnity(sea030HomeMenu.current2DUnitData.Unit, present.gear.ID);
      SeaPresentPresentResult seaPresentResult = sea030HomeMenu.GetSeaPresentResult(presentPresentAffnity.affinity);
      if (seaPresentResult != null)
      {
        sea030HomeMenu.SetSerif(seaPresentResult.serif, seaPresentResult.face, seaPresentResult.voice_cue_name, (Hashtable) null);
        sea030HomeMenu.PlayLoveResultEffect(seaPresentResult.affinity.home_result, trustUp, future.Result.gain_trust_result, playerUnit);
        sea030HomeMenu.eyeBlink.StartBlink(2f);
      }
      cr.isTouchBlock = false;
    }
  }

  private void DepartForDate(SeaDateDateSpotDisplaySetting setting)
  {
    if (this.isDepartDate)
      return;
    this.isDepartDate = true;
    this.IsPush = true;
    Singleton<CommonRoot>.GetInstance().isTouchBlock = true;
    this.StartCoroutine(this.internalDepartForDate(setting));
  }

  private IEnumerator internalDepartForDate(SeaDateDateSpotDisplaySetting setting)
  {
    Sea030HomeMenu sea030HomeMenu1 = this;
    bool isSeaSeasonEnd = false;
    Singleton<PopupManager>.GetInstance().closeAllWithoutAnim(true);
    CommonRoot cr = Singleton<CommonRoot>.GetInstance();
    cr.isTouchBlock = true;
    while (Singleton<PopupManager>.GetInstance().isOpen)
      yield return (object) null;
    cr.loadingMode = 1;
    cr.isLoading = true;
    Future<WebAPI.Response.SeaDateStart> futureAPI = WebAPI.SeaDateStart(setting.datespot.ID, sea030HomeMenu1.current2DUnitData.PlayerUnit.id, (System.Action<WebAPI.Response.UserError>) (e =>
    {
      if (string.Equals(e.Code, "SEA000"))
      {
        isSeaSeasonEnd = true;
        this.StartCoroutine(PopupUtility.SeaError(e));
      }
      else
      {
        WebAPI.DefaultUserErrorCallback(e);
        Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
      }
    }));
    IEnumerator e1 = futureAPI.Wait();
    while (e1.MoveNext())
      yield return e1.Current;
    e1 = (IEnumerator) null;
    if (futureAPI.Result == null)
    {
      if (!isSeaSeasonEnd)
      {
        cr.isLoading = false;
        cr.loadingMode = 0;
        cr.isTouchBlock = false;
        sea030HomeMenu1.isDepartDate = false;
        sea030HomeMenu1.IsPush = false;
        Singleton<NGGameDataManager>.GetInstance().refreshHomeHome = true;
        cr.isLoading = true;
        e1 = new Future<NGGameDataManager.StartSceneProxyResult>(new Func<Promise<NGGameDataManager.StartSceneProxyResult>, IEnumerator>(Singleton<NGGameDataManager>.GetInstance().StartSceneAsyncProxyImpl)).Wait();
        while (e1.MoveNext())
          yield return e1.Current;
        e1 = (IEnumerator) null;
        cr.isLoading = false;
      }
    }
    else
    {
      sea030HomeMenu1.existSelect = futureAPI.Result.happening_id > 0 || futureAPI.Result.quiz_id > 0;
      cr.isLoading = false;
      NGSoundManager sm = Singleton<NGSoundManager>.GetInstance();
      int playVoiceIndex = -1;
      SeaDateSerifAtDepart serifDepart = sea030HomeMenu1.GetSerifDepart();
      if (serifDepart != null)
        playVoiceIndex = sea030HomeMenu1.SetSerif(serifDepart.serif, serifDepart.face, serifDepart.voice_cue_name, new Hashtable()
        {
          {
            (object) "dateName",
            (object) setting.date_name
          }
        });
      sm.PlaySe("SE_2702", false, 0.0f, -1);
      yield return (object) new WaitForSeconds(1f);
      while (playVoiceIndex >= 0 && sm.IsVoicePlaying(playVoiceIndex))
        yield return (object) null;
      sea030HomeMenu1.dateSetting = setting;
      sea030HomeMenu1.dateFlows = ((IEnumerable<int>) futureAPI.Result.script_ids).ToList<int>();
      sea030HomeMenu1.nowFlowIndex = 0;
      sea030HomeMenu1.eventSelectData.Clear();
      sea030HomeMenu1.quizId = Sea030HomeMenu.hasQuiz(futureAPI.Result.date_flow) ? new int?(futureAPI.Result.quiz_id) : new int?();
      cr.loadingMode = 3;
      cr.isLoading = true;
      List<int> dateFlows = sea030HomeMenu1.dateFlows;
      Sea030HomeMenu sea030HomeMenu2 = sea030HomeMenu1;
      int nowFlowIndex = sea030HomeMenu1.nowFlowIndex;
      int num = nowFlowIndex + 1;
      sea030HomeMenu2.nowFlowIndex = num;
      int index = nowFlowIndex;
      Story0093Scene.changeSceneModeDate(true, dateFlows[index], sea030HomeMenu1.current2DUnitData.Unit, sea030HomeMenu1.dateSetting.GetImageHash(), new System.Action<int, int>(sea030HomeMenu1.EventSelected), false, new bool?(true), sea030HomeMenu1.quizId);
      cr.isTouchBlock = false;
      sea030HomeMenu1.isDepartDate = false;
      sea030HomeMenu1.IsPush = false;
      sea030HomeMenu1.SetSerif(string.Empty, "normal", string.Empty, (Hashtable) null);
    }
  }

  public static bool hasQuiz(SeaDateDateFlow dateFlow)
  {
    return dateFlow == SeaDateDateFlow.happening_choices || dateFlow == SeaDateDateFlow.quiz_choices;
  }

  private void EventSelected(int index, int choice)
  {
    this.eventSelectData[index] = choice;
  }

  private void ResetPlayLoveResultEffect()
  {
    this.resultEffectObject.SetActive(false);
    this.resultGaugeEffectObject.SetActive(false);
    this.trustUpLabel.gameObject.SetActive(false);
  }

  private void PlayLoveResultEffect(
    SeaHomeResult result,
    float trustUp,
    GainTrustResult gainTrustResult,
    PlayerUnit unit)
  {
    if (result == null)
      return;
    this.resultEffectObject.SetActive(result.effect_on);
    this.resultGaugeEffectObject.SetActive(result.gauge_effect_on);
    if (!string.IsNullOrEmpty(result.effect_trigger))
    {
      this.resultEffectAnimator.SetTrigger(result.effect_trigger);
      this.unitAnimator.SetTrigger(result.effect_trigger);
    }
    if (!string.IsNullOrEmpty(result.gauge_effect_trigger))
      this.resultGaugeEffectAnimator.SetTrigger(result.gauge_effect_trigger);
    if ((double) trustUp > 0.0)
    {
      this.trustUpLabel.gameObject.SetActive(true);
      this.trustUpLabel.SetTextLocalize(string.Format("+{0:f2}{1}", (object) trustUp, (object) Consts.GetInstance().PERCENT));
      this.trustUpLabelAnimator.SetTrigger("dear_degree_up");
    }
    else
      this.trustUpLabel.gameObject.SetActive(false);
    Singleton<CommonRoot>.GetInstance().isTouchBlock = true;
    Consts instance = Consts.GetInstance();
    bool isFirstTimeGaugeMax = (double) unit.trust_rate >= (double) instance.TRUST_RATE_LEVEL_SIZE && (double) unit.trust_rate - (double) trustUp < (double) instance.TRUST_RATE_LEVEL_SIZE;
    this.StartCoroutine(this.PlayGainTrustResult(gainTrustResult, unit, 2f, isFirstTimeGaugeMax));
  }

  private IEnumerator PlayGainTrustResult(
    GainTrustResult result,
    PlayerUnit unit,
    float wait,
    bool isFirstTimeGaugeMax = false)
  {
    FavorabilityRatingEffect.AnimationType type = FavorabilityRatingEffect.AnimationType.None;
    if (result.is_equip_awake_skill_release)
      type = FavorabilityRatingEffect.AnimationType.SkillFrameRelease;
    else if (result.has_new_player_awake_skill)
      type = FavorabilityRatingEffect.AnimationType.SkillRelease;
    if (type == FavorabilityRatingEffect.AnimationType.None)
    {
      Singleton<CommonRoot>.GetInstance().isTouchBlock = false;
    }
    else
    {
      yield return (object) new WaitForSeconds(wait);
      FavorabilityRatingEffect script = this.favorabilityRatingEffectPopupPrefab.CloneAndGetComponent<FavorabilityRatingEffect>(Singleton<CommonRoot>.GetInstance().LoadTmpObj);
      IEnumerator e = script.Init(type, unit, (System.Action) (() =>
      {
        if (result.is_equip_awake_skill_release && result.has_new_player_awake_skill)
        {
          result.is_equip_awake_skill_release = false;
          this.StartCoroutine(this.PlayGainTrustResult(result, unit, 0.0f, false));
        }
        else
          Singleton<CommonRoot>.GetInstance().isTouchBlock = false;
        Singleton<PopupManager>.GetInstance().dismiss(false);
      }), !isFirstTimeGaugeMax);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      Singleton<PopupManager>.GetInstance().open(script.gameObject, false, false, true, true, false, false, "SE_1006");
      script.GetComponent<FavorabilityRatingEffect>().StartEffect();
    }
  }

  public bool ExistDateFlows()
  {
    return this.dateFlows != null;
  }

  public override void onBackButton()
  {
    if (this.IsPushAndSet())
      return;
    if (this.mode == Sea030HomeMenu.HomeMode.Talk)
    {
      this.ChangeAppreciationMode();
      this.IsPush = false;
    }
    else if ((UnityEngine.Object) this.callPopupObject != (UnityEngine.Object) null && this.callPopupObject.IsShow)
    {
      this.IsPush = false;
    }
    else
    {
      if (this.mode != Sea030HomeMenu.HomeMode.Appreciation)
        return;
      Singleton<NGGameDataManager>.GetInstance().IsSea = false;
      if (!Singleton<NGSceneManager>.GetInstance().isSceneInitialized)
        return;
      if (Singleton<NGSceneManager>.GetInstance().sceneName == "mypage")
      {
        MypageScene sceneBase = Singleton<NGSceneManager>.GetInstance().sceneBase as MypageScene;
        if ((UnityEngine.Object) sceneBase != (UnityEngine.Object) null && sceneBase.isAnimePlaying)
          return;
      }
      Singleton<NGSceneManager>.GetInstance().sceneBase.IsPush = true;
      Singleton<NGGameDataManager>.GetInstance().lastReferenceUnitID = -1;
      Singleton<NGGameDataManager>.GetInstance().lastReferenceUnitIndex = -1;
      Singleton<NGSceneManager>.GetInstance().destroyLoadedScenes();
      Singleton<NGSceneManager>.GetInstance().clearStack();
      MypageScene.ChangeScene(false, false, false);
    }
  }

  public void LoginPopupStart(
    List<PlayerLoginBonus> loginBonuses,
    int loginBonusCount,
    System.Action finish)
  {
    this.LoginBonusCloseCounter = 0;
    this.LoginPopupShow(loginBonuses, loginBonusCount, finish);
  }

  private void LoginPopupShow(
    List<PlayerLoginBonus> loginBonuses,
    int loginBonusCount,
    System.Action finish)
  {
    ModalWindow.Show(loginBonuses[this.LoginBonusCloseCounter].loginbonus.name, loginBonuses[this.LoginBonusCloseCounter].rewards[0].client_reward_message, (System.Action) (() =>
    {
      Singleton<NGGameDataManager>.GetInstance().loginBonuses.Remove(loginBonuses[this.LoginBonusCloseCounter]);
      ++this.LoginBonusCloseCounter;
      if (loginBonusCount <= this.LoginBonusCloseCounter)
      {
        List<LevelRewardSchemaMixin> playerLevelRewards = Singleton<NGGameDataManager>.GetInstance().playerLevelRewards;
        if (playerLevelRewards != null && playerLevelRewards.Count > 0)
          this.LevelUpPopupStart(playerLevelRewards, finish);
        else
          finish();
      }
      else
        this.LoginPopupShow(loginBonuses, loginBonusCount, finish);
    }));
  }

  public void LevelUpPopupStart(List<LevelRewardSchemaMixin> levelRewards, System.Action finish)
  {
    this.LevelUpBonusCloseCounter = 0;
    this.LevelUpBonusCount = levelRewards.Count;
    this.LevelUpPopupShow(levelRewards, finish);
  }

  private void LevelUpPopupShow(List<LevelRewardSchemaMixin> levelRewards, System.Action finish)
  {
    ModalWindow.Show(levelRewards[this.LevelUpBonusCloseCounter].reward_title, levelRewards[this.LevelUpBonusCloseCounter].reward_message, (System.Action) (() =>
    {
      ++this.LevelUpBonusCloseCounter;
      if (this.LevelUpBonusCount <= this.LevelUpBonusCloseCounter)
      {
        Singleton<NGGameDataManager>.GetInstance().playerLevelRewards = (List<LevelRewardSchemaMixin>) null;
        if (finish == null)
          return;
        finish();
      }
      else
        this.LevelUpPopupShow(levelRewards, finish);
    }));
  }

  public SeaDateSerifAtDepart GetSerifDepart()
  {
    return ((IEnumerable<SeaDateSerifAtDepart>) MasterData.SeaDateSerifAtDepartList).FirstOrDefault<SeaDateSerifAtDepart>((Func<SeaDateSerifAtDepart, bool>) (x => x.same_character_id_UnitUnit.HasValue && x.same_character_id_UnitUnit.Value == this.current2DUnitData.Unit.same_character_id && x.trust_provision.WithIn((int) this.current2DUnitData.PlayerUnit.trust_rate))) ?? ((IEnumerable<SeaDateSerifAtDepart>) MasterData.SeaDateSerifAtDepartList).FirstOrDefault<SeaDateSerifAtDepart>((Func<SeaDateSerifAtDepart, bool>) (x => x.character_id.HasValue && x.character_id.Value == this.current2DUnitData.Unit.character.ID && x.trust_provision.WithIn((int) this.current2DUnitData.PlayerUnit.trust_rate)));
  }

  private SeaDateResult GetSeaDateResult(float trust_up)
  {
    return ((IEnumerable<SeaDateResult>) MasterData.SeaDateResultList).FirstOrDefault<SeaDateResult>((Func<SeaDateResult, bool>) (x => x.same_character_id_UnitUnit.HasValue && x.same_character_id_UnitUnit.Value == this.current2DUnitData.Unit.same_character_id && x.result_staging.WithIn(trust_up))) ?? ((IEnumerable<SeaDateResult>) MasterData.SeaDateResultList).FirstOrDefault<SeaDateResult>((Func<SeaDateResult, bool>) (x => x.character_id.HasValue && x.character_id.Value == this.current2DUnitData.Unit.character.ID && x.result_staging.WithIn(trust_up)));
  }

  private SeaPresentPresentResult GetSeaPresentResult(
    SeaPresentAffinity affinity)
  {
    return ((IEnumerable<SeaPresentPresentResult>) MasterData.SeaPresentPresentResultList).FirstOrDefault<SeaPresentPresentResult>((Func<SeaPresentPresentResult, bool>) (x => x.same_character_id_UnitUnit.HasValue && x.same_character_id_UnitUnit.Value == this.current2DUnitData.Unit.same_character_id && affinity.ID == x.affinity.ID)) ?? ((IEnumerable<SeaPresentPresentResult>) MasterData.SeaPresentPresentResultList).FirstOrDefault<SeaPresentPresentResult>((Func<SeaPresentPresentResult, bool>) (x => x.character_id.HasValue && x.character_id.Value == this.current2DUnitData.Unit.character.ID && affinity.ID == x.affinity.ID));
  }

  private enum HomeMode
  {
    Appreciation,
    Talk,
  }
}
