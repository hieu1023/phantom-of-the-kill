﻿// Decompiled with JetBrains decompiler
// Type: SozaiItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class SozaiItem : MonoBehaviour
{
  [SerializeField]
  private GameObject linkSozaiBase;
  [SerializeField]
  private GameObject dirSozaiBase;
  [SerializeField]
  private GameObject dirSozaiBase2;
  [SerializeField]
  private UILabel txtPossessionNum;
  [SerializeField]
  private GameObject wLine;
  [HideInInspector]
  public UnitIcon UnitIcon;

  public GameObject LinkSozaiBase
  {
    get
    {
      return this.linkSozaiBase;
    }
  }

  public GameObject DirSozaiBase
  {
    get
    {
      return this.dirSozaiBase;
    }
  }

  public GameObject DirSozaiBase2
  {
    get
    {
      return this.dirSozaiBase2;
    }
  }

  public UILabel TxtPossessionNum
  {
    get
    {
      return this.txtPossessionNum;
    }
  }

  public GameObject WLine
  {
    get
    {
      return this.wLine;
    }
  }

  public void SetOnlyWLine()
  {
    foreach (Transform transform in this.transform)
    {
      if ((Object) transform.gameObject == (Object) this.wLine)
        transform.gameObject.SetActive(true);
      else
        transform.gameObject.SetActive(false);
    }
  }
}
