﻿// Decompiled with JetBrains decompiler
// Type: Bugu0053GearPopup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

public class Bugu0053GearPopup : ItemDetailPopupBase
{
  private Bugu0053DirRecipePopup root;
  [SerializeField]
  private UIButton[] IbtnClose;

  public virtual IEnumerator Init(
    Bugu0053DirRecipePopup recipePopup,
    MasterDataTable.CommonRewardType type,
    int id)
  {
    Bugu0053GearPopup bugu0053GearPopup = this;
    bugu0053GearPopup.root = recipePopup;
    bugu0053GearPopup.root.isBackKey = false;
    bugu0053GearPopup.GetComponent<UIWidget>().alpha = 0.0f;
    IEnumerator e = bugu0053GearPopup.SetInfo(type, id, 0);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    bugu0053GearPopup.GetComponent<UIWidget>().alpha = 1f;
    bugu0053GearPopup.GetComponent<Shop00742Menu>().enabled = false;
    bugu0053GearPopup.GetComponent<ItemDetailPopupBase>().enabled = false;
    for (int index = 0; index < bugu0053GearPopup.IbtnClose.Length; ++index)
    {
      // ISSUE: reference to a compiler-generated method
      EventDelegate.Set(bugu0053GearPopup.IbtnClose[index].onClick, new EventDelegate.Callback(bugu0053GearPopup.\u003CInit\u003Eb__2_0));
    }
  }

  public override void IbtnNo()
  {
    if ((Object) this.root != (Object) null)
      Singleton<CommonRoot>.GetInstance().StartCoroutine(this.root.BackKeyEnable());
    Singleton<PopupManager>.GetInstance().onDismiss();
  }

  public override void onBackButton()
  {
    this.IbtnNo();
  }
}
