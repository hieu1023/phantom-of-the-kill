﻿// Decompiled with JetBrains decompiler
// Type: Battle01GrandStatus
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using System.Collections;
using UnityEngine;

public class Battle01GrandStatus : NGBattleMenuBase
{
  [SerializeField]
  private UILabel place;
  [SerializeField]
  private UILabel hit;
  [SerializeField]
  private UILabel pDefense;
  [SerializeField]
  private UILabel mDefense;
  [SerializeField]
  private UILabel stay;
  [SerializeField]
  private GameObject descriptionRoot;
  [SerializeField]
  private UILabel description;
  [SerializeField]
  private NGTweenParts descriptionTween;
  [SerializeField]
  private UILabel[] landformTagLabels;
  private BL.BattleModified<BL.ClassValue<BL.Panel>> modified;

  private void OnEnable()
  {
    if (this.env == null || this.env.core.fieldCurrent.value == null)
      return;
    this.updateTagLabels(this.env.core.fieldCurrent.value.landform);
  }

  private string numberString(int v)
  {
    return v <= 0 ? string.Concat((object) v) : "+" + (object) v;
  }

  private string percentString(int v)
  {
    return v <= 0 ? v.ToString() + "%" : "+" + (object) v + "%";
  }

  public override IEnumerator onInitAsync()
  {
    // ISSUE: reference to a compiler-generated field
    int num = this.\u003C\u003E1__state;
    Battle01GrandStatus battle01GrandStatus = this;
    if (num != 0)
      return false;
    // ISSUE: reference to a compiler-generated field
    this.\u003C\u003E1__state = -1;
    battle01GrandStatus.modified = BL.Observe<BL.ClassValue<BL.Panel>>(battle01GrandStatus.env.core.fieldCurrent);
    return false;
  }

  public void setText(UILabel label, int v, float? ratio)
  {
    if (ratio.HasValue)
    {
      int v1 = Mathf.RoundToInt((float) ((double) ratio.Value * 100.0 - 100.0));
      this.setText(label, this.percentString(v1));
    }
    else
      this.setText(label, this.numberString(v));
  }

  protected override void LateUpdate_Battle()
  {
    if (!this.modified.isChangedOnce() || this.env.core.fieldCurrent.value == null)
      return;
    BattleLandform landform = this.env.core.fieldCurrent.value.landform;
    BattleLandformIncr displayIncr = landform.GetDisplayIncr();
    this.setText(this.place, landform.name);
    this.setText(this.hit, displayIncr.hit_incr, displayIncr.hit_ratio_incr);
    this.setText(this.pDefense, displayIncr.physical_defense_incr, displayIncr.physical_defense_ratio_incr);
    this.setText(this.mDefense, displayIncr.magic_defense_incr, displayIncr.magic_defense_ratio_incr);
    this.setText(this.stay, displayIncr.evasion_incr, displayIncr.evasion_ratio_incr);
    this.updateTagLabels(landform);
    BL.Unit unit = this.env.core.unitCurrent.unit;
    if (unit == (BL.Unit) null || this.env.core.currentUnitPosition != null && unit.isView && !this.env.unitResource[unit].unitParts_.isMoving)
    {
      this.SetTextLandformDescription(landform.description);
    }
    else
    {
      if (this.env.core.currentUnitPosition == null || !unit.isView || !this.env.unitResource[unit].unitParts_.isMoving)
        return;
      this.descriptionTween.isActive = false;
    }
  }

  private void SetTextLandformDescription(string descriptionText)
  {
    if ((Object) this.descriptionTween == (Object) null)
      return;
    if (string.IsNullOrEmpty(descriptionText))
    {
      this.descriptionTween.isActive = false;
    }
    else
    {
      this.setText(this.description, descriptionText);
      this.descriptionTween.isActive = true;
    }
  }

  private void updateTagLabels(BattleLandform landform)
  {
    if (this.landformTagLabels == null || this.landformTagLabels.Length < 3)
      return;
    int[] numArray = new int[3]
    {
      landform.tag1,
      landform.tag2,
      landform.tag3
    };
    for (int index = 0; index < this.landformTagLabels.Length; ++index)
    {
      BattleLandformTag battleLandformTag;
      if (numArray[index] != 0 && MasterData.BattleLandformTag.TryGetValue(numArray[index], out battleLandformTag))
      {
        this.landformTagLabels[index].gameObject.SetActive(true);
        this.setText(this.landformTagLabels[index], battleLandformTag.type);
      }
      else
      {
        this.setText(this.landformTagLabels[index], "");
        this.landformTagLabels[index].gameObject.SetActive(false);
      }
      this.landformTagLabels[index].SetDirty();
    }
  }
}
