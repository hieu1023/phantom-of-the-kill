﻿// Decompiled with JetBrains decompiler
// Type: Transfer01272Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;

public class Transfer01272Scene : NGSceneBase
{
  public Transfer01272Menu menu;

  public static void ChangeScene(bool stack)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("transfer012_7_2", stack, (object[]) Array.Empty<object>());
  }

  public IEnumerator onStartSceneAsync()
  {
    this.menu.InitTransfer();
    yield break;
  }
}
