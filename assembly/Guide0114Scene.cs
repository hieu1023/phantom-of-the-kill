﻿// Decompiled with JetBrains decompiler
// Type: Guide0114Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System;
using System.Collections;
using UnityEngine;

public class Guide0114Scene : NGSceneBase
{
  private bool isFirstInit = true;
  public Guide0114Menu menu;

  public static void changeScene(bool stack, Guide0111Scene.UpdateInfo updateInfo)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("guide011_4", (stack ? 1 : 0) != 0, (object) updateInfo);
  }

  public override IEnumerator onInitSceneAsync()
  {
    Guide0114Scene guide0114Scene = this;
    Singleton<CommonRoot>.GetInstance().ShowLoadingLayer(0, false);
    Future<GameObject> fBG = Res.Prefabs.BackGround.picturebook.Load<GameObject>();
    IEnumerator e = fBG.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    guide0114Scene.backgroundPrefab = fBG.Result;
  }

  public IEnumerator onStartSceneAsync(Guide0111Scene.UpdateInfo updateInfo)
  {
    long ver = SMManager.Revision<PlayerItem[]>();
    if (updateInfo.version.HasValue)
    {
      long num = ver;
      long? version = updateInfo.version;
      long valueOrDefault = version.GetValueOrDefault();
      if (num == valueOrDefault & version.HasValue)
        goto label_5;
    }
    Singleton<CommonRoot>.GetInstance().ShowLoadingLayer(0, false);
    Future<WebAPI.Response.ZukanGear> item = WebAPI.ZukanGear((System.Action<WebAPI.Response.UserError>) (e =>
    {
      WebAPI.DefaultUserErrorCallback(e);
      Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
    }));
    yield return (object) item.Wait();
    if (item.Result == null)
    {
      yield break;
    }
    else
    {
      updateInfo.version = new long?(ver);
      this.isFirstInit = true;
      item = (Future<WebAPI.Response.ZukanGear>) null;
    }
label_5:
    if (this.isFirstInit)
    {
      this.isFirstInit = false;
      IEnumerator e = this.menu.onInitMenuAsync();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    this.menu.IbtnUse();
    Singleton<CommonRoot>.GetInstance().HideLoadingLayer();
  }

  public void onStartScene(Guide0111Scene.UpdateInfo updateInfo)
  {
  }

  public override void onEndScene()
  {
    this.menu.StopCreateUnitIconImage();
    Singleton<PopupManager>.GetInstance().closeAll(false);
  }
}
