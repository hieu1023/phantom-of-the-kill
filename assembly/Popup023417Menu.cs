﻿// Decompiled with JetBrains decompiler
// Type: Popup023417Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;

public class Popup023417Menu : BackButtonMenuBase
{
  public virtual void IbtnBack()
  {
    Singleton<PopupManager>.GetInstance().onDismiss();
  }

  public virtual void IbtnTeamSetting()
  {
    Singleton<PopupManager>.GetInstance().onDismiss();
    Unit0046Scene.changeScene(true, (QuestLimitationBase[]) null, (string) null, false);
  }

  public void IbtnNo()
  {
    this.IbtnBack();
  }

  public override void onBackButton()
  {
    this.IbtnNo();
  }
}
