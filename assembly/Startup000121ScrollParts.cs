﻿// Decompiled with JetBrains decompiler
// Type: Startup000121ScrollParts
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;
using UnityEngine;

public class Startup000121ScrollParts : MonoBehaviour
{
  [SerializeField]
  protected UILabel title;
  [SerializeField]
  protected UILabel date;
  [SerializeField]
  protected UILabel time;
  [SerializeField]
  protected UILabel badge;
  protected OfficialInformationArticle article;
  [SerializeField]
  protected GameObject newSprite;
  [SerializeField]
  private string nextSceneName;

  public string NextSceneName
  {
    get
    {
      return this.nextSceneName;
    }
    set
    {
      this.nextSceneName = value;
    }
  }

  public void IbtnNewslist()
  {
    Singleton<NGSoundManager>.GetInstance().stopVoice(-1);
    Singleton<NGSceneManager>.GetInstance().changeScene(this.NextSceneName, true, (object) this.article);
  }

  public virtual void Set(OfficialInformationArticle info, string listSceneName)
  {
    this.NextSceneName = listSceneName;
    this.article = info;
    if ((Object) this.title != (Object) null)
      this.title.SetText(info.title);
    this.badge.SetTextLocalize(info.badge_display);
  }

  public virtual void SetData(OfficialInformationArticle info, NGMenuBase menu)
  {
  }

  public virtual bool SetNewSprite()
  {
    try
    {
      if (Persist.infoUnRead.Data.GetUnRead(this.article))
      {
        this.newSprite.SetActive(false);
        return false;
      }
    }
    catch
    {
      Persist.infoUnRead.Delete();
    }
    this.newSprite.SetActive(true);
    return true;
  }
}
