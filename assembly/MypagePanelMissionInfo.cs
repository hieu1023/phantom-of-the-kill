﻿// Decompiled with JetBrains decompiler
// Type: MypagePanelMissionInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class MypagePanelMissionInfo : MonoBehaviour
{
  [SerializeField]
  private UILabel titel;
  [SerializeField]
  private UILabel description;
  [SerializeField]
  private CreateIconObject iconBase;
  private BingoMission bingoMission;

  public IEnumerator InitPanelMissionInfo(int panel_id)
  {
    MypagePanelMissionInfo panelMissionInfo = this;
    panelMissionInfo.bingoMission = MasterData.BingoMission[panel_id];
    panelMissionInfo.titel.SetTextLocalize(panelMissionInfo.bingoMission.name);
    panelMissionInfo.description.SetTextLocalize(panelMissionInfo.bingoMission.detail);
    // ISSUE: reference to a compiler-generated method
    MasterDataTable.BingoRewardGroup bingoRewardGroup = ((IEnumerable<MasterDataTable.BingoRewardGroup>) MasterData.BingoRewardGroupList).Where<MasterDataTable.BingoRewardGroup>(new Func<MasterDataTable.BingoRewardGroup, bool>(panelMissionInfo.\u003CInitPanelMissionInfo\u003Eb__4_0)).OrderBy<MasterDataTable.BingoRewardGroup, int>((Func<MasterDataTable.BingoRewardGroup, int>) (x => x.ID)).FirstOrDefault<MasterDataTable.BingoRewardGroup>();
    panelMissionInfo.iconBase.transform.GetChildren().ForEach<Transform>((System.Action<Transform>) (x =>
    {
      if (!((UnityEngine.Object) x.gameObject != (UnityEngine.Object) null))
        return;
      x.gameObject.SetActive(false);
    }));
    panelMissionInfo.iconBase.transform.Clear();
    IEnumerator e = panelMissionInfo.iconBase.CreateThumbnail(bingoRewardGroup.reward_type_id, bingoRewardGroup.reward_id, bingoRewardGroup.reward_quantity, false, true, new CommonQuestType?(), false);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public void onMissionInfo()
  {
    if (this.bingoMission == null || string.IsNullOrEmpty(this.bingoMission.scene_name))
      return;
    string sceneName = this.bingoMission.scene_name;
    int? arg1 = this.bingoMission.scene_arg;
    if (sceneName == "mypage001_8_2")
      Singleton<NGSceneManager>.GetInstance().changeScene("mypage001_8_2", false, (object) arg1);
    else if (sceneName == "quest002_4")
    {
      PlayerStoryQuestS[] playerStoryQuestSArray = SMManager.Get<PlayerStoryQuestS[]>();
      QuestStoryS questStoryS = (QuestStoryS) null;
      Quest00240723Menu.StoryMode storyMode = !arg1.HasValue || !MasterData.QuestStoryS.TryGetValue(arg1.Value, out questStoryS) ? Quest00240723Menu.StoryMode.LostRagnarok : (Quest00240723Menu.StoryMode) questStoryS.quest_xl_QuestStoryXL;
      if (questStoryS != null && ((IEnumerable<PlayerStoryQuestS>) playerStoryQuestSArray).Any<PlayerStoryQuestS>((Func<PlayerStoryQuestS, bool>) (x => x._quest_story_s == arg1.Value)))
        Quest00240723Scene.ChangeScene0024(false, MasterData.QuestStoryS[arg1.Value].quest_l_QuestStoryL, true);
      else
        Quest00240723Scene.ChangeScene0024(false, ((IEnumerable<PlayerStoryQuestS>) playerStoryQuestSArray).Where<PlayerStoryQuestS>((Func<PlayerStoryQuestS, bool>) (x => (Quest00240723Menu.StoryMode) x.quest_story_s.quest_xl_QuestStoryXL == storyMode)).Select<PlayerStoryQuestS, int>((Func<PlayerStoryQuestS, int>) (x => x.quest_story_s.quest_l_QuestStoryL)).Max(), true);
    }
    else if (sceneName == "quest002_20" || sceneName == "quest002_19" || sceneName == "quest002_26")
    {
      if (arg1.HasValue)
        this.StartCoroutine(this.onBannerEventConnection(sceneName, arg1.Value));
      else
        Quest00217Scene.ChangeScene(false);
    }
    else if (sceneName == "gacha006_3")
    {
      if (arg1.HasValue)
        Singleton<NGSceneManager>.GetInstance().changeScene(sceneName, false, (object) arg1.Value);
      else
        Singleton<NGSceneManager>.GetInstance().changeScene(sceneName, false, (object[]) Array.Empty<object>());
    }
    else if (sceneName == "shop007_21")
      Shop00721Scene.changeScene(false, true);
    else if (sceneName == "quest002_30")
      Quest00230Scene.ChangeScene(false, arg1.Value);
    else if (sceneName.StartsWith("colosseum023"))
    {
      if (!SMManager.Get<Player>().GetFeatureColosseum() || !SMManager.Get<Player>().GetReleaseColosseum())
        this.StartCoroutine(PopupCommon.Show(Consts.GetInstance().DAILY_MISSION_0271_POPUP_TITLE, Consts.GetInstance().DAILY_MISSION_0271_COLOSSEUM, (System.Action) null));
      else
        Singleton<NGSceneManager>.GetInstance().changeScene(sceneName, true, (object[]) Array.Empty<object>());
    }
    else if (sceneName == "sea030_quest")
      Sea030_questScene.ChangeScene(false, true, false);
    else if (sceneName == "sea030_home")
      Sea030HomeScene.ChangeScene(false, false);
    else if (sceneName == "sea030_album")
      Sea030AlbumScene.ChangeScene(false);
    else
      Singleton<NGSceneManager>.GetInstance().changeScene(sceneName, true, (object[]) Array.Empty<object>());
  }

  private IEnumerator onBannerEventConnection(string changeSchene, int sID)
  {
    Future<WebAPI.Response.QuestProgressExtra> Extra;
    IEnumerator e1;
    if (!WebAPI.IsResponsedAtRecent("QuestProgressExtra", 60.0))
    {
      Singleton<CommonRoot>.GetInstance().loadingMode = 1;
      Extra = WebAPI.QuestProgressExtra((System.Action<WebAPI.Response.UserError>) (error =>
      {
        Singleton<CommonRoot>.GetInstance().loadingMode = 0;
        WebAPI.DefaultUserErrorCallback(error);
      }));
      e1 = Extra.Wait();
      while (e1.MoveNext())
        yield return e1.Current;
      e1 = (IEnumerator) null;
      Singleton<CommonRoot>.GetInstance().loadingMode = 0;
      if (Extra.Result != null)
        WebAPI.SetLatestResponsedAt("QuestProgressExtra");
      Extra = (Future<WebAPI.Response.QuestProgressExtra>) null;
    }
    if (!((IEnumerable<PlayerExtraQuestS>) SMManager.Get<PlayerExtraQuestS[]>()).Any<PlayerExtraQuestS>((Func<PlayerExtraQuestS, bool>) (x => x.quest_extra_s != null && x.quest_extra_s.ID == sID)))
    {
      Future<GameObject> time_popup = Res.Prefabs.popup.popup_002_23__anim_popup01.Load<GameObject>();
      e1 = time_popup.Wait();
      while (e1.MoveNext())
        yield return e1.Current;
      e1 = (IEnumerator) null;
      Singleton<PopupManager>.GetInstance().openAlert(time_popup.Result, false, false, (EventDelegate) null, false, true, false, true);
    }
    else
    {
      if (changeSchene == "quest002_26")
      {
        QuestScoreCampaignProgress[] ScoreCampaingProgress = SMManager.Get<QuestScoreCampaignProgress[]>();
        if (ScoreCampaingProgress == null)
        {
          Extra = WebAPI.QuestProgressExtra((System.Action<WebAPI.Response.UserError>) (e => WebAPI.DefaultUserErrorCallback(e)));
          e1 = Extra.Wait();
          while (e1.MoveNext())
            yield return e1.Current;
          e1 = (IEnumerator) null;
          if (Extra.Result == null)
          {
            yield break;
          }
          else
          {
            ScoreCampaingProgress = SMManager.Get<QuestScoreCampaignProgress[]>();
            Extra = (Future<WebAPI.Response.QuestProgressExtra>) null;
          }
        }
        int idl = MasterData.QuestExtraS[sID].quest_l_QuestExtraL;
        if (ScoreCampaingProgress != null)
        {
          e1 = ServerTime.WaitSync();
          while (e1.MoveNext())
            yield return e1.Current;
          e1 = (IEnumerator) null;
          QuestScoreCampaignProgress campaign = ((IEnumerable<QuestScoreCampaignProgress>) ScoreCampaingProgress).FirstOrDefault<QuestScoreCampaignProgress>((Func<QuestScoreCampaignProgress, bool>) (x => x.quest_extra_l == idl));
          if (campaign != null && CampaignQuest.GetEvetnTerm(campaign, ServerTime.NowAppTimeAddDelta()) == CampaignQuest.RankingEventTerm.aggregate)
          {
            Consts instance = Consts.GetInstance();
            ModalWindow.Show(instance.DAILY_MISSION_0271_RANKING_TITILE, instance.DAILY_MISSION_0271_RANKING_TITILE, (System.Action) (() => {}));
            yield break;
          }
        }
        ScoreCampaingProgress = (QuestScoreCampaignProgress[]) null;
      }
      if (changeSchene == "quest002_20")
        Quest00220Scene.ChangeScene00220(sID, false, false);
      else if (changeSchene == "quest002_19")
        Quest00219Scene.ChangeScene(sID, true);
      else if (changeSchene == "quest002_26")
        Quest00226Scene.ChangeScene(sID, true);
    }
  }
}
