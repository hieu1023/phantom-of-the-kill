﻿// Decompiled with JetBrains decompiler
// Type: PopupJobChangeConditions
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class PopupJobChangeConditions : BackButtonPopupBase
{
  [SerializeField]
  [Tooltip("解放条件表示")]
  private PopupJobChangeConditions.UnitJob[] jobs_;

  public static Future<GameObject> createLoader()
  {
    return new ResourceObject("Prefabs/unit004_Job/popup_JobReleasedClass_Conditions__anim_popup01").Load<GameObject>();
  }

  public static void show(GameObject prefab, PlayerUnit playerUnit)
  {
    Singleton<PopupManager>.GetInstance().open(prefab, false, false, false, true, true, false, "SE_1006").GetComponent<PopupJobChangeConditions>().initialize(playerUnit);
  }

  private void initialize(PlayerUnit playerUnit)
  {
    this.setTopObject(this.gameObject);
    int[] numArray = ((object) playerUnit != null ? playerUnit.unit.getClassChangeJobIdList().ToArray() : (int[]) null) ?? new int[0];
    for (int index1 = 0; index1 < this.jobs_.Length; ++index1)
    {
      int index2 = index1 + 1;
      this.jobs_[index1].setConditions(playerUnit, numArray.Length > index2 ? new int?(numArray[index2]) : new int?());
    }
  }

  public override void onBackButton()
  {
    this.onClickedClose();
  }

  public void onClickedClose()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<PopupManager>.GetInstance().dismiss(false);
  }

  [Serializable]
  private class UnitCondition
  {
    [SerializeField]
    [Tooltip("先頭位置")]
    private GameObject objTop_;
    [SerializeField]
    [Tooltip("条件表示")]
    private UILabel txtCondition_;
    [SerializeField]
    [Tooltip("現レベル表示")]
    private UILabel txtStatus_;

    public void setCondition(JobCharacteristics jobAbility, int level)
    {
      if (jobAbility == null)
      {
        this.objTop_.SetActive(false);
      }
      else
      {
        BattleskillSkill skill = jobAbility.skill;
        string placeholder1;
        string placeholder2;
        if (skill.upper_level > level)
        {
          placeholder1 = Consts.GetInstance().POPUP_JOBCHANGE_CONDITION;
          placeholder2 = Consts.GetInstance().POPUP_JOBCHANGE_STATUS;
        }
        else
        {
          placeholder1 = Consts.GetInstance().POPUP_JOBCHANGE_CONDITION_COMPLETED;
          placeholder2 = Consts.GetInstance().POPUP_JOBCHANGE_STATUS_COMPLETED;
        }
        string text1 = Consts.Format(placeholder1, (IDictionary) new Hashtable()
        {
          {
            (object) "name",
            (object) skill.name
          },
          {
            (object) nameof (level),
            (object) skill.upper_level
          }
        });
        string text2 = Consts.Format(placeholder2, (IDictionary) new Hashtable()
        {
          {
            (object) "now",
            (object) level
          },
          {
            (object) "max",
            (object) skill.upper_level
          }
        });
        this.txtCondition_.SetTextLocalize(text1);
        this.txtStatus_.SetTextLocalize(text2);
        this.objTop_.SetActive(true);
      }
    }
  }

  [Serializable]
  private class UnitJob
  {
    [SerializeField]
    [Tooltip("先頭位置")]
    private GameObject objTop_;
    [SerializeField]
    [Tooltip("ジョブ名")]
    private UILabel txtName_;
    [SerializeField]
    [Tooltip("解放条件表示")]
    private PopupJobChangeConditions.UnitCondition[] conditions_;

    public void setConditions(PlayerUnit playerUnit, int? jobId)
    {
      MasterDataTable.UnitJob unitJob;
      if (jobId.HasValue && jobId.Value != 0 && (MasterData.UnitJob.TryGetValue(jobId.Value, out unitJob) && unitJob.JobAbilities.Length != 0))
      {
        PlayerUnitAll_saved_job_abilities[] allJobAbilities = playerUnit.all_saved_job_abilities ?? new PlayerUnitAll_saved_job_abilities[0];
        \u003C\u003Ef__AnonymousType9<JobCharacteristics, PlayerUnitAll_saved_job_abilities>[] array = ((IEnumerable<JobCharacteristics>) unitJob.JobAbilities).Where<JobCharacteristics>((Func<JobCharacteristics, bool>) (x => x.skill.upper_level > 0)).Select(y => new
        {
          m = y,
          v = Array.Find<PlayerUnitAll_saved_job_abilities>(allJobAbilities, (Predicate<PlayerUnitAll_saved_job_abilities>) (z => z.job_ability_id == y.ID))
        }).ToArray();
        this.txtName_.SetTextLocalize(unitJob.name);
        for (int index = 0; index < this.conditions_.Length; ++index)
        {
          if (array.Length <= index)
          {
            this.conditions_[index].setCondition((JobCharacteristics) null, 0);
          }
          else
          {
            PopupJobChangeConditions.UnitCondition condition = this.conditions_[index];
            JobCharacteristics m = array[index].m;
            PlayerUnitAll_saved_job_abilities v = array[index].v;
            int level = v != null ? v.level : 0;
            condition.setCondition(m, level);
          }
        }
        this.objTop_.SetActive(true);
      }
      else
        this.objTop_.SetActive(false);
    }
  }
}
