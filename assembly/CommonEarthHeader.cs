﻿// Decompiled with JetBrains decompiler
// Type: CommonEarthHeader
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using Earth;
using GameCore;
using System.Collections.Generic;
using UnityEngine;

public class CommonEarthHeader : NGMenuBase
{
  [SerializeField]
  private UILabel txtChapterNum;
  [SerializeField]
  private UILabel txtChapterName;
  [SerializeField]
  private UILabel txtQuestName;
  [SerializeField]
  private UILabel txtZeny;
  private BL.BattleModified<EarthQuestProgress> questProgressModified;
  private BL.BattleModified<BL.ClassValue<Dictionary<MasterDataTable.CommonRewardType, long>>> userPropertiesModified;

  private void Awake()
  {
  }

  public void Reset()
  {
    EarthDataManager instance = Singleton<EarthDataManager>.GetInstance();
    EarthQuestProgress questProgress = instance.questProgress;
    BL.ClassValue<Dictionary<MasterDataTable.CommonRewardType, long>> mUserProperties = instance.mUserProperties;
    this.questProgressModified = BL.Observe<EarthQuestProgress>(questProgress);
    this.userPropertiesModified = BL.Observe<BL.ClassValue<Dictionary<MasterDataTable.CommonRewardType, long>>>(mUserProperties);
    this.UpdateQuestProgressInfomation();
  }

  private void Start()
  {
    this.Reset();
  }

  private void Update()
  {
    if (this.questProgressModified.isChangedOnce())
      this.UpdateQuestProgressInfomation();
    if (!this.userPropertiesModified.isChangedOnce())
      return;
    this.UpdateZeny();
  }

  private void UpdateQuestProgressInfomation()
  {
    EarthDataManager instanceOrNull = Singleton<EarthDataManager>.GetInstanceOrNull();
    if (!((Object) instanceOrNull != (Object) null))
      return;
    EarthQuestProgress questProgress = instanceOrNull.questProgress;
    this.txtChapterNum.SetTextLocalize(questProgress.currentEpisode.chapter.chapter);
    this.txtChapterName.SetTextLocalize(questProgress.currentEpisode.chapter.chapter_name);
    this.txtQuestName.SetTextLocalize(questProgress.currentEpisode.episode_name);
    this.txtZeny.SetTextLocalize(instanceOrNull.GetProperty(MasterDataTable.CommonRewardType.money));
  }

  private void UpdateZeny()
  {
    EarthDataManager instanceOrNull = Singleton<EarthDataManager>.GetInstanceOrNull();
    if (!((Object) instanceOrNull != (Object) null))
      return;
    this.txtZeny.SetTextLocalize(instanceOrNull.GetProperty(MasterDataTable.CommonRewardType.money));
  }
}
