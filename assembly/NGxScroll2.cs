﻿// Decompiled with JetBrains decompiler
// Type: NGxScroll2
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class NGxScroll2 : MonoBehaviour
{
  public int topSpace = 2;
  public int bottomSpace = 2;
  private List<GameObject> arr = new List<GameObject>();
  public UIScrollView scrollView;
  private int ColumnSize;
  private int iconHeight;
  private GameObject Top;
  private GameObject Bottom;

  private int TopSpace
  {
    get
    {
      return (Mathf.Max(2, this.topSpace) + 1) / 2 * 2;
    }
  }

  private int BottomSpace
  {
    get
    {
      return (Mathf.Max(2, this.bottomSpace) + 1) / 2 * 2;
    }
  }

  public GameObject TopObject
  {
    get
    {
      return this.Top;
    }
  }

  public GameObject BottomObject
  {
    get
    {
      return this.Bottom;
    }
  }

  public IEnumerator<GameObject> GetEnumerator()
  {
    return (IEnumerator<GameObject>) this.arr.GetEnumerator();
  }

  private void Awake()
  {
    this.scrollView.contentPivot = UIWidget.Pivot.TopLeft;
    this.scrollView.disableDragIfFits = true;
    this.Top = (GameObject) null;
    this.Bottom = (GameObject) null;
  }

  private void Start()
  {
    foreach (Transform child in this.scrollView.verticalScrollBar.transform.GetChildren())
    {
      if ((UnityEngine.Object) child.GetComponent<Collider>() != (UnityEngine.Object) null)
        child.GetComponent<Collider>().enabled = true;
    }
  }

  public IEnumerable<GameObject> GridChildren()
  {
    return this.scrollView.transform.GetChildren().Select<Transform, GameObject>((Func<Transform, GameObject>) (t => t.gameObject));
  }

  public void Clear()
  {
    if ((UnityEngine.Object) this.Top != (UnityEngine.Object) null)
      this.Top = (GameObject) null;
    if ((UnityEngine.Object) this.Bottom != (UnityEngine.Object) null)
      this.Bottom = (GameObject) null;
    this.arr.Clear();
    this.scrollView.transform.Clear();
  }

  public void Add(GameObject obj, int width, int height, bool ignoreResizeCollider = false)
  {
    this.iconHeight = height;
    this.ColumnSize = 5;
    obj.transform.parent = this.scrollView.transform;
    int num = this.scrollView.transform.childCount - 1;
    obj.transform.localPosition = new Vector3((float) (num % this.ColumnSize * width - width * 2), (float) -(num / this.ColumnSize * height), 0.0f);
    obj.transform.localScale = Vector3.one;
    this.arr.Add(obj);
    if (ignoreResizeCollider)
      return;
    BoxCollider componentInChildren = obj.GetComponentInChildren<BoxCollider>();
    if (!((UnityEngine.Object) componentInChildren != (UnityEngine.Object) null))
      return;
    componentInChildren.size = new Vector3((float) width, (float) height);
  }

  public void Add(
    GameObject obj,
    int width,
    int height,
    int startIndex,
    bool ignoreResizeCollider = false)
  {
    this.iconHeight = height;
    this.ColumnSize = 5;
    obj.transform.parent = this.scrollView.transform;
    int num = startIndex;
    obj.transform.localPosition = new Vector3((float) (num % this.ColumnSize * width - width * 2), (float) -(num / this.ColumnSize * height), 0.0f);
    obj.transform.localScale = Vector3.one;
    this.arr.Add(obj);
    if (ignoreResizeCollider)
      return;
    BoxCollider componentInChildren = obj.GetComponentInChildren<BoxCollider>();
    if (!((UnityEngine.Object) componentInChildren != (UnityEngine.Object) null))
      return;
    componentInChildren.size = new Vector3((float) width, (float) height);
  }

  public void AddColumn1(GameObject obj, int width, int height, bool ignoreResizeCollider = false)
  {
    this.ColumnSize = 1;
    this.iconHeight = height;
    obj.transform.parent = this.scrollView.transform;
    int num = this.scrollView.transform.childCount - 1;
    obj.transform.localPosition = new Vector3(0.0f, (float) -(num * height), 0.0f);
    obj.transform.localScale = Vector3.one;
    this.arr.Add(obj);
    if (ignoreResizeCollider)
      return;
    BoxCollider componentInChildren = obj.GetComponentInChildren<BoxCollider>();
    if (!((UnityEngine.Object) componentInChildren != (UnityEngine.Object) null))
      return;
    componentInChildren.size = new Vector3((float) width, (float) height);
  }

  public void CreateScrollPoint(int height, int count)
  {
    if ((UnityEngine.Object) this.Top == (UnityEngine.Object) null)
    {
      this.Top = new GameObject("Top");
      this.Top.layer = this.scrollView.gameObject.layer;
      this.Top.transform.parent = this.scrollView.transform;
      UIWidget uiWidget = this.Top.AddComponent<UIWidget>();
      uiWidget.SetDimensions(2, this.TopSpace);
      uiWidget.panel = this.scrollView.panel;
    }
    this.Top.transform.localPosition = new Vector3(0.0f, (float) (height / 2), 0.0f);
    this.Top.transform.localScale = Vector3.one;
    this.Top.SetActive(true);
    if ((UnityEngine.Object) this.Bottom == (UnityEngine.Object) null)
    {
      this.Bottom = new GameObject("Bottom");
      this.Bottom.layer = this.scrollView.gameObject.layer;
      this.Bottom.transform.parent = this.scrollView.transform;
      UIWidget uiWidget = this.Bottom.AddComponent<UIWidget>();
      uiWidget.SetDimensions(2, this.BottomSpace);
      uiWidget.panel = this.scrollView.panel;
    }
    this.Bottom.transform.parent = this.scrollView.transform;
    this.Bottom.transform.localPosition = new Vector3(0.0f, (float) (-(Mathf.Max(0, count - 1) / 5 * height) - height / 2), 0.0f);
    this.Bottom.transform.localScale = Vector3.one;
    this.Bottom.SetActive(true);
  }

  public void CreateScrollPointHeight(int height, int count)
  {
    if ((UnityEngine.Object) this.Top == (UnityEngine.Object) null)
    {
      this.Top = new GameObject("Top");
      this.Top.layer = this.scrollView.gameObject.layer;
      this.Top.transform.parent = this.scrollView.transform;
      UIWidget uiWidget = this.Top.AddComponent<UIWidget>();
      uiWidget.SetDimensions(2, this.TopSpace);
      uiWidget.panel = this.scrollView.panel;
    }
    this.Top.transform.localPosition = new Vector3(0.0f, (float) (height / 2), 0.0f);
    this.Top.transform.localScale = Vector3.one;
    this.Top.SetActive(true);
    if ((UnityEngine.Object) this.Bottom == (UnityEngine.Object) null)
    {
      this.Bottom = new GameObject("Bottom");
      this.Bottom.layer = this.scrollView.gameObject.layer;
      this.Bottom.transform.parent = this.scrollView.transform;
      UIWidget uiWidget = this.Bottom.AddComponent<UIWidget>();
      uiWidget.SetDimensions(2, this.BottomSpace);
      uiWidget.panel = this.scrollView.panel;
    }
    this.Bottom.transform.localPosition = new Vector3(0.0f, (float) (-((count - 1) * height) - height / 2), 0.0f);
    this.Bottom.transform.localScale = Vector3.one;
    this.Bottom.SetActive(true);
  }

  public void ResolvePosition(Vector2 pos)
  {
    this.scrollView.ResetPosition();
    this.scrollView.SetDragAmount(pos.x, pos.y, false);
    this.scrollView.SetDragAmount(pos.x, pos.y, true);
  }

  public void ResolvePosition(int index, int iconCount)
  {
    this.scrollView.ResetPosition();
    if ((double) this.scrollView.panel.height > (double) this.scrollView.bounds.size.y)
      return;
    int num1 = this.ColumnSize;
    if (num1 < 1)
      num1 = 1;
    float num2 = (float) (iconCount / num1);
    if ((double) num2 > 0.0)
      ++num2;
    if ((double) num2 < 1.0)
      ;
    float num3 = Mathf.Abs(this.scrollView.bounds.size.y - this.scrollView.panel.height);
    float y = (float) this.iconHeight * (float) (index / num1) / num3;
    if ((double) y < 0.0)
      y = 0.0f;
    else if ((double) y > 1.0)
      y = 1f;
    this.ResolvePosition(new Vector2(0.0f, y));
  }

  public void ResolvePosition()
  {
    this.scrollView.ResetPosition();
  }

  public void ResolvePositionFromScrollValue(float pos)
  {
    this.scrollView.ResetPosition();
    if ((double) this.scrollView.panel.height > (double) this.scrollView.bounds.size.y)
      return;
    float num1 = Mathf.Abs(this.scrollView.bounds.size.y - this.scrollView.panel.height);
    float num2 = (float) this.iconHeight / 2f / num1;
    float num3 = this.scrollView.panel.height / 2f / num1;
    float y = (float) ((double) pos / (double) num1 - ((double) num3 - (double) num2));
    if ((double) y < 0.0)
      y = 0.0f;
    else if ((double) y > 1.0)
      y = 1f;
    this.ResolvePosition(new Vector2(0.0f, y));
  }

  public void Reset()
  {
    if ((UnityEngine.Object) this.Top != (UnityEngine.Object) null)
    {
      UnityEngine.Object.Destroy((UnityEngine.Object) this.Top);
      this.Top = (GameObject) null;
    }
    if ((UnityEngine.Object) this.Bottom != (UnityEngine.Object) null)
    {
      UnityEngine.Object.Destroy((UnityEngine.Object) this.Bottom);
      this.Bottom = (GameObject) null;
    }
    this.scrollView.transform.DetachChildren();
    this.arr.Clear();
  }
}
