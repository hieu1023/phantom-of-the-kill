﻿// Decompiled with JetBrains decompiler
// Type: UnitDetailIcon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class UnitDetailIcon : UnitIconBase
{
  public static readonly int Width = 120;
  public static readonly int Height = 264;
  public static readonly int ColumnValue = 5;
  public static readonly int RowValue = 6;
  public static readonly int RowScreenValue = 3;
  public static readonly int ScreenValue = UnitDetailIcon.ColumnValue * UnitDetailIcon.RowScreenValue;
  public static readonly int MaxValue = UnitDetailIcon.ColumnValue * UnitDetailIcon.RowValue;
  public static readonly int SKILL_ID_ALL = 99;
  private static Dictionary<int, UnitDetailIcon.SpriteCache> spriteCache = new Dictionary<int, UnitDetailIcon.SpriteCache>();
  public GameObject unitIconPrefab;
  public GameObject unitIconParent;
  public UI2DSprite upiconSpriteAtk;
  public UI2DSprite upiconSpriteDef;
  public UI2DSprite upiconSpriteHp;
  public UI2DSprite upiconSpriteMatk;
  public UI2DSprite upiconSpriteMtl;
  public UI2DSprite upiconSpriteSpe;
  public UI2DSprite upiconSpriteTec;
  public UI2DSprite upiconSpriteLuck;
  public UI2DSprite[] upiconSprites;
  public GameObject[] upiconEffect;
  public UILabel txtPossession;
  [SerializeField]
  private GameObject breakThrough;
  [SerializeField]
  private NGxBlinkEx blink;
  [SerializeField]
  private GameObject blinkBreakthrough;
  [SerializeField]
  private GameObject blinkSkillUp;
  [SerializeField]
  private GameObject blinkDeardegreeup;
  [SerializeField]
  private GameObject blinkRelevancedegreeup;
  [SerializeField]
  private GameObject blinkUnityValue;
  [SerializeField]
  private UILabel txtBlinkUnityValue;
  [SerializeField]
  private GameObject skillUp;
  [SerializeField]
  private GameObject deardegreeup;
  [SerializeField]
  private GameObject relevancedegreeup;
  [SerializeField]
  private GameObject unityValue;
  [SerializeField]
  private UILabel txtUnityValue;

  public bool BreakThrough
  {
    get
    {
      return this.breakThrough.activeSelf;
    }
    set
    {
      this.breakThrough.SetActive(value);
    }
  }

  public static void ClearCache()
  {
    UnitDetailIcon.spriteCache.Clear();
  }

  public static bool IsCache(UnitUnit unit)
  {
    return UnitDetailIcon.spriteCache.ContainsKey(unit.ID);
  }

  public static IEnumerator LoadSprite(UnitUnit unit)
  {
    if (unit != null && !UnitDetailIcon.spriteCache.ContainsKey(unit.ID))
    {
      Future<UnityEngine.Sprite> spriteF = unit.LoadSpriteThumbnail();
      IEnumerator e = spriteF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      UnityEngine.Sprite sprite = spriteF.Result;
      Future<GameObject> SetGearPrefab = Res.Icons.GearKindIcon.Load<GameObject>();
      e = SetGearPrefab.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      GameObject result = SetGearPrefab.Result;
      UnitDetailIcon.spriteCache[unit.ID] = new UnitDetailIcon.SpriteCache(sprite, result);
    }
  }

  private void Awake()
  {
    this.UnitIcon = this.unitIconPrefab.CloneAndGetComponent<UnitIcon>(this.unitIconParent);
    this.UnitIcon.BottomBaseObject = false;
    this.UnitIcon.isViewBackObject = false;
    this.UnitIcon.buttonBoxCollider.enabled = false;
    this.txtLabel = this.UnitIcon.TxtLabel;
    this.for_battle = this.UnitIcon.for_battle;
    this.tower_entry = this.UnitIcon.tower_entry;
    this.can_awake = this.UnitIcon.can_awake;
    this.unitRental = this.UnitIcon.unitRental;
    this.overkillers = this.UnitIcon.overkillers;
    this.blinkDeckStatus = this.UnitIcon.blinkDeckStatus;
    this.equip = this.UnitIcon.equip;
    this.SelectNumberSprites = this.UnitIcon.SelectNumberSprites;
    this.SelectNumberBase = this.UnitIcon.SelectNumberBase;
    this.SelectNumber = this.UnitIcon.SelectNumber;
  }

  public bool SkillUp
  {
    get
    {
      return this.skillUp.activeSelf;
    }
    set
    {
      this.skillUp.SetActive(value);
    }
  }

  public bool Deardegreeup
  {
    get
    {
      return this.deardegreeup.activeSelf;
    }
    set
    {
      this.deardegreeup.SetActive(value);
    }
  }

  public bool Relevancedegreeup
  {
    get
    {
      return this.relevancedegreeup.activeSelf;
    }
    set
    {
      this.relevancedegreeup.SetActive(value);
    }
  }

  public bool UnityValue
  {
    get
    {
      return this.unityValue.activeSelf;
    }
    set
    {
      this.unityValue.SetActive(value);
    }
  }

  public void SetSprite(UnitUnit unit)
  {
    ((IEnumerable<UI2DSprite>) this.upiconSprites).ForEach<UI2DSprite>((System.Action<UI2DSprite>) (x => x.gameObject.SetActive(true)));
    ((IEnumerable<GameObject>) this.upiconEffect).ForEach<GameObject>((System.Action<GameObject>) (x => x.SetActive(false)));
    int num = 0;
    if (!unit.IsBuildup)
    {
      if (num < 4 && unit.hp_compose != 0)
        this.upiconSprites[num++].sprite2D = this.upiconSpriteHp.sprite2D;
      if (num < 4 && unit.strength_compose != 0)
        this.upiconSprites[num++].sprite2D = this.upiconSpriteAtk.sprite2D;
      if (num < 4 && unit.vitality_compose != 0)
        this.upiconSprites[num++].sprite2D = this.upiconSpriteDef.sprite2D;
      if (num < 4 && unit.intelligence_compose != 0)
        this.upiconSprites[num++].sprite2D = this.upiconSpriteMatk.sprite2D;
      if (num < 4 && unit.mind_compose != 0)
        this.upiconSprites[num++].sprite2D = this.upiconSpriteMtl.sprite2D;
      if (num < 4 && unit.agility_compose != 0)
        this.upiconSprites[num++].sprite2D = this.upiconSpriteSpe.sprite2D;
      if (num < 4 && unit.dexterity_compose != 0)
        this.upiconSprites[num++].sprite2D = this.upiconSpriteTec.sprite2D;
      if (num < 4 && unit.lucky_compose != 0)
        this.upiconSprites[num++].sprite2D = this.upiconSpriteLuck.sprite2D;
    }
    else
    {
      if (num < 4 && unit.hp_buildup != 0)
        this.upiconSprites[num++].sprite2D = this.upiconSpriteHp.sprite2D;
      if (num < 4 && unit.strength_buildup != 0)
        this.upiconSprites[num++].sprite2D = this.upiconSpriteAtk.sprite2D;
      if (num < 4 && unit.vitality_buildup != 0)
        this.upiconSprites[num++].sprite2D = this.upiconSpriteDef.sprite2D;
      if (num < 4 && unit.intelligence_buildup != 0)
        this.upiconSprites[num++].sprite2D = this.upiconSpriteMatk.sprite2D;
      if (num < 4 && unit.mind_buildup != 0)
        this.upiconSprites[num++].sprite2D = this.upiconSpriteMtl.sprite2D;
      if (num < 4 && unit.agility_buildup != 0)
        this.upiconSprites[num++].sprite2D = this.upiconSpriteSpe.sprite2D;
      if (num < 4 && unit.dexterity_buildup != 0)
        this.upiconSprites[num++].sprite2D = this.upiconSpriteTec.sprite2D;
      if (num < 4 && unit.lucky_buildup != 0)
        this.upiconSprites[num++].sprite2D = this.upiconSpriteLuck.sprite2D;
      for (int index = 0; index < num; ++index)
      {
        this.upiconEffect[index].SetActive(true);
        TweenColor component = this.upiconEffect[index].GetComponent<TweenColor>();
        component.ResetToBeginning();
        component.PlayForward();
      }
    }
    while (num < 4)
      this.upiconSprites[num++].gameObject.SetActive(false);
  }

  private void InitializeLabel()
  {
    this.blink.gameObject.SetActive(false);
    this.BreakThrough = false;
    this.SkillUp = false;
    this.Deardegreeup = false;
    this.Relevancedegreeup = false;
    this.UnityValue = false;
  }

  private void SetLabel(PlayerUnit playerUnit, PlayerUnit basePlayerUnit, bool isTrustMaterial = false)
  {
    UnitUnit unit1 = playerUnit.unit;
    UnitUnit unit2 = basePlayerUnit.unit;
    this.InitializeLabel();
    string text = (string) null;
    bool flag1 = basePlayerUnit.unity_value < PlayerUnit.GetUnityValueMax();
    if (flag1)
    {
      float num = 0.0f;
      if (unit1.IsNormalUnit)
      {
        if (unit2.same_character_id == unit1.same_character_id)
          num = Mathf.Min(playerUnit.unityTotal + 1f, (float) PlayerUnit.GetUnityValueMax());
      }
      else
      {
        UnityValueUpPattern valueUpPattern;
        if (unit1.is_unity_value_up && (double) basePlayerUnit.unityTotal < (double) PlayerUnit.GetUnityValueMax() && (valueUpPattern = unit1.FindValueUpPattern(unit2, (Func<UnitFamily[]>) (() => basePlayerUnit.Families))) != null)
          num = (float) valueUpPattern.up_value;
      }
      if ((double) num > 0.0)
        text = string.Format(Consts.GetInstance().unit_004_8_4_plus_unity_value, (object) (Math.Floor((double) num * 10.0) / 10.0));
      else
        flag1 = false;
    }
    bool flag2 = (unit1.IsBreakThrough || unit2.same_character_id == unit1.same_character_id && unit2.rarity.index <= unit1.rarity.index) && basePlayerUnit.breakthrough_count < unit2.breakthrough_limit;
    IEnumerable<PlayerUnitSkills> source = ((IEnumerable<PlayerUnitSkills>) basePlayerUnit.skills).Where<PlayerUnitSkills>((Func<PlayerUnitSkills, bool>) (x => x.level < x.skill.upper_level));
    bool flag3 = (unit1.IsMaterialUnitSkillUp || unit1.same_character_id == unit2.same_character_id) && source.Count<PlayerUnitSkills>() > 0 || source.Any<PlayerUnitSkills>((Func<PlayerUnitSkills, bool>) (unitBase => playerUnit.skills != null && ((IEnumerable<PlayerUnitSkills>) playerUnit.skills).Count<PlayerUnitSkills>((Func<PlayerUnitSkills, bool>) (unit => unitBase.skill_id == unit.skill_id)) > 0));
    bool flag4 = false;
    if (unit2.trust_target_flag && (double) basePlayerUnit.trust_rate < (double) basePlayerUnit.trust_max_rate && unit1.same_character_id == unit2.same_character_id)
      flag4 = true;
    bool flag5 = unit2.trust_target_flag && (double) basePlayerUnit.trust_rate < (double) basePlayerUnit.trust_max_rate && unit1.character.ID == unit2.character.ID;
    this.BreakThrough = false;
    this.SkillUp = false;
    this.Deardegreeup = false;
    this.Relevancedegreeup = false;
    this.UnityValue = false;
    bool[] flagArray = new bool[4]
    {
      flag2,
      flag3,
      flag4 | isTrustMaterial | flag5,
      flag1
    };
    if (((IEnumerable<bool>) flagArray).Where<bool>((Func<bool, bool>) (b => b)).Count<bool>() > 1)
    {
      GameObject[] gameObjectArray = new GameObject[4]
      {
        this.blinkBreakthrough,
        this.blinkSkillUp,
        this.getBlinkDegreeup(unit2),
        this.blinkUnityValue
      };
      foreach (GameObject gameObject in gameObjectArray)
        gameObject.SetActive(false);
      List<GameObject> gameObjectList = new List<GameObject>();
      for (int index = 0; index < flagArray.Length; ++index)
      {
        if (flagArray[index])
        {
          GameObject gameObject = gameObjectArray[index];
          gameObjectList.Add(gameObject);
          gameObject.SetActive(true);
        }
      }
      this.blink.SetChildren(gameObjectList.ToArray());
      this.blink.gameObject.SetActive(true);
      if (!((UnityEngine.Object) this.txtBlinkUnityValue != (UnityEngine.Object) null) || string.IsNullOrEmpty(text))
        return;
      this.txtBlinkUnityValue.SetTextLocalize(text);
    }
    else
    {
      GameObject[] gameObjectArray = new GameObject[4]
      {
        this.breakThrough,
        this.skillUp,
        unit2.IsSea ? this.deardegreeup : (unit2.IsResonanceUnit ? this.relevancedegreeup : this.deardegreeup),
        this.unityValue
      };
      for (int index = 0; index < flagArray.Length; ++index)
        gameObjectArray[index].SetActive(flagArray[index]);
      if (!((UnityEngine.Object) this.txtUnityValue != (UnityEngine.Object) null) || string.IsNullOrEmpty(text))
        return;
      this.txtUnityValue.SetTextLocalize(text);
    }
  }

  private GameObject getBlinkDegreeup(UnitUnit baseU)
  {
    if (baseU.IsSea)
    {
      this.blinkRelevancedegreeup.SetActive(false);
      return this.blinkDeardegreeup;
    }
    if (baseU.IsResonanceUnit)
    {
      this.blinkDeardegreeup.SetActive(false);
      return this.blinkRelevancedegreeup;
    }
    this.blinkRelevancedegreeup.SetActive(false);
    return this.blinkDeardegreeup;
  }

  public override IEnumerator SetMaterialUnit(
    PlayerUnit playerUnit,
    bool isNew,
    PlayerUnit[] playerUnits,
    bool isTrust)
  {
    UnitDetailIcon unitDetailIcon = this;
    // ISSUE: reference to a compiler-generated method
    IEnumerator e = unitDetailIcon.\u003C\u003En__0(playerUnit, isNew, playerUnits);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if (playerUnit != (PlayerUnit) null)
      EventDelegate.Set(unitDetailIcon.Button.onLongPress, (EventDelegate.Callback) (() =>
      {
        if (this.pressEvent != null)
          this.pressEvent();
        Unit0042Scene.changeScene(true, playerUnit, playerUnits, true, false);
      }));
    unitDetailIcon.InitializeLabel();
    e = unitDetailIcon.SetUnit(unitDetailIcon.playerUnit, false);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    unitDetailIcon.SetSprite(playerUnit.unit);
    if ((UnityEngine.Object) unitDetailIcon.txtPossession != (UnityEngine.Object) null)
    {
      PlayerMaterialUnit playerMaterialUnit = ((IEnumerable<PlayerMaterialUnit>) SMManager.Get<PlayerMaterialUnit[]>()).FirstOrDefault<PlayerMaterialUnit>((Func<PlayerMaterialUnit, bool>) (x => x.id == playerUnit.id));
      if (playerMaterialUnit != null)
      {
        unitDetailIcon.txtPossession.gameObject.SetActive(true);
        unitDetailIcon.txtPossession.SetTextLocalize(Consts.Format(Consts.GetInstance().unit_004_9_9_possession_text, (IDictionary) new Hashtable()
        {
          {
            (object) "Count",
            (object) playerMaterialUnit.quantity
          }
        }));
      }
      else
        unitDetailIcon.txtPossession.gameObject.SetActive(false);
    }
  }

  public override IEnumerator SetMaterialUnit(
    PlayerUnit playerUnit,
    PlayerUnit basePlayerUnit,
    bool isNew,
    PlayerUnit[] playerUnits,
    bool isTrust)
  {
    UnitDetailIcon unitDetailIcon = this;
    IEnumerator e = unitDetailIcon.SetMaterialUnit(playerUnit, isNew, playerUnits, isTrust);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    unitDetailIcon.SetLabel(playerUnit, basePlayerUnit, isTrust);
  }

  public static bool IsSkillUpMaterial(UnitUnit unit, PlayerUnit baseUnit)
  {
    bool flag = false;
    if (unit.skillup_type != 0)
    {
      PlayerUnitSkills[] array = ((IEnumerable<PlayerUnitSkills>) baseUnit.skills).Where<PlayerUnitSkills>((Func<PlayerUnitSkills, bool>) (x => x.level < x.skill.upper_level)).ToArray<PlayerUnitSkills>();
      if (array.Length != 0)
      {
        int materialSkillUpType = unit.skillup_type;
        flag = materialSkillUpType == UnitDetailIcon.SKILL_ID_ALL || ((IEnumerable<PlayerUnitSkills>) array).Any<PlayerUnitSkills>((Func<PlayerUnitSkills, bool>) (target => target.skill.skill_type == (BattleskillSkillType) materialSkillUpType));
        UnitSkillupSetting skillupSetting = ((IEnumerable<UnitSkillupSetting>) MasterData.UnitSkillupSettingList).FirstOrDefault<UnitSkillupSetting>((Func<UnitSkillupSetting, bool>) (x => x.material_unit_id == unit.ID));
        if (skillupSetting != null && skillupSetting.skill_group.HasValue)
        {
          IEnumerable<int> skillIDList = ((IEnumerable<UnitSkillupSkillGroupSetting>) MasterData.UnitSkillupSkillGroupSettingList).Where<UnitSkillupSkillGroupSetting>((Func<UnitSkillupSkillGroupSetting, bool>) (x => x.group_id == skillupSetting.skill_group.Value)).Select<UnitSkillupSkillGroupSetting, int>((Func<UnitSkillupSkillGroupSetting, int>) (x => x.skill_id));
          flag = flag && ((IEnumerable<PlayerUnitSkills>) array).Any<PlayerUnitSkills>((Func<PlayerUnitSkills, bool>) (x => skillIDList.Contains<int>(x.skill.ID)));
        }
      }
    }
    return flag;
  }

  public override IEnumerator SetPlayerUnit(
    PlayerUnit playerUnit,
    PlayerUnit[] playerUnits,
    PlayerUnit basePlayerUnit = null,
    bool isMaterial = false,
    bool isMemory = false)
  {
    UnitDetailIcon unitDetailIcon = this;
    // ISSUE: reference to a compiler-generated method
    IEnumerator e = unitDetailIcon.\u003C\u003En__1(playerUnit, playerUnits, basePlayerUnit, isMaterial, isMemory);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if (playerUnit != (PlayerUnit) null)
      EventDelegate.Set(unitDetailIcon.Button.onLongPress, (EventDelegate.Callback) (() =>
      {
        if (this.pressEvent != null)
          this.pressEvent();
        if (Singleton<NGGameDataManager>.GetInstance().IsSea)
          Singleton<NGSceneManager>.GetInstance().LastHeaderType = new CommonRoot.HeaderType?(Singleton<CommonRoot>.GetInstance().headerType);
        Unit0042Scene.changeScene(true, playerUnit, playerUnits, isMaterial, isMemory);
      }));
    unitDetailIcon.SetSprite(playerUnit.unit);
    unitDetailIcon.UnitIcon.princessType.SetPrincessType(playerUnit);
    e = unitDetailIcon.SetUnit(unitDetailIcon.playerUnit, false);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if (basePlayerUnit != (PlayerUnit) null)
    {
      unitDetailIcon.SetLabel(playerUnit, basePlayerUnit, false);
      NGTween.playTweens(unitDetailIcon.GetComponentsInChildren<UITweener>(true), NGTween.Kind.GRAYOUT, true);
    }
    if ((UnityEngine.Object) unitDetailIcon.txtPossession != (UnityEngine.Object) null)
      unitDetailIcon.txtPossession.gameObject.SetActive(false);
  }

  public IEnumerator SetPlayerUnitEvolution(
    PlayerUnit playerUnit,
    PlayerUnit[] playerUnits,
    PlayerUnit basePlayerUnit = null)
  {
    UnitDetailIcon unitDetailIcon = this;
    // ISSUE: reference to a compiler-generated method
    IEnumerator e = unitDetailIcon.\u003C\u003En__1(playerUnit, playerUnits, basePlayerUnit, false, false);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if (playerUnit != (PlayerUnit) null)
      EventDelegate.Set(unitDetailIcon.Button.onLongPress, (EventDelegate.Callback) (() =>
      {
        if (this.pressEvent != null)
          this.pressEvent();
        Unit0042Scene.changeSceneEvolutionUnit(true, playerUnit, playerUnits, false, false, false);
      }));
    unitDetailIcon.SetSprite(playerUnit.unit);
    e = unitDetailIcon.SetUnit(unitDetailIcon.playerUnit, false);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if (basePlayerUnit != (PlayerUnit) null)
    {
      unitDetailIcon.SetLabel(playerUnit, basePlayerUnit, false);
      NGTween.playTweens(unitDetailIcon.GetComponentsInChildren<UITweener>(true), NGTween.Kind.GRAYOUT, true);
    }
    if ((UnityEngine.Object) unitDetailIcon.txtPossession != (UnityEngine.Object) null)
      unitDetailIcon.txtPossession.gameObject.SetActive(false);
  }

  public override IEnumerator SetUnit(UnitUnit unit, CommonElement element, bool isGray = false)
  {
    UnitDetailIcon unitDetailIcon = this;
    IEnumerator e = unitDetailIcon.UnitIcon.SetUnit(unit, unit.GetElement(), false);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    // ISSUE: reference to a compiler-generated method
    e = unitDetailIcon.\u003C\u003En__2(unit, unit.GetElement(), false);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    unitDetailIcon.UnitIcon.setLevelText(unitDetailIcon.playerUnit);
    unitDetailIcon.UnitIcon.ShowBottomInfo(UnitSortAndFilter.SORT_TYPES.BranchOfAnArmy);
    unitDetailIcon.UnitIcon.setCostText(unitDetailIcon.playerUnit);
    unitDetailIcon.UnitIcon.Favorite = unitDetailIcon.playerUnit.favorite;
    unitDetailIcon.UnitIcon.SpecialIcon = false;
    unitDetailIcon.UnitIcon.SpecialIconType = -1;
    unitDetailIcon.BreakThrough |= unitDetailIcon.playerUnit.unit.IsBreakThrough;
    unitDetailIcon.SkillUp |= unitDetailIcon.playerUnit.unit.IsMaterialUnitSkillUp;
    unitDetailIcon.Deardegreeup = !unitDetailIcon.playerUnit.unit.IsBreakThrough && !unitDetailIcon.playerUnit.unit.IsMaterialUnitSkillUp && (unitDetailIcon.playerUnit.unit.trust_target_flag && (double) unitDetailIcon.playerUnit.trust_rate < (double) unitDetailIcon.playerUnit.trust_max_rate) && unitDetailIcon.playerUnit.unit.IsSea;
    unitDetailIcon.Relevancedegreeup = !unitDetailIcon.playerUnit.unit.IsBreakThrough && !unitDetailIcon.playerUnit.unit.IsMaterialUnitSkillUp && (unitDetailIcon.playerUnit.unit.trust_target_flag && (double) unitDetailIcon.playerUnit.trust_rate < (double) unitDetailIcon.playerUnit.trust_max_rate) && unitDetailIcon.playerUnit.unit.IsResonanceUnit;
    unitDetailIcon.SetSprite(unit);
  }

  public IEnumerator SetUnit(PlayerUnit playerUnit, bool isGray = false)
  {
    UnitDetailIcon unitDetailIcon = this;
    IEnumerator e = unitDetailIcon.UnitIcon.SetUnit(playerUnit, playerUnit.GetElement(), false);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    // ISSUE: reference to a compiler-generated method
    e = unitDetailIcon.\u003C\u003En__2(playerUnit.unit, playerUnit.GetElement(), false);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    unitDetailIcon.UnitIcon.setLevelText(unitDetailIcon.playerUnit);
    unitDetailIcon.UnitIcon.ShowBottomInfo(UnitSortAndFilter.SORT_TYPES.BranchOfAnArmy);
    unitDetailIcon.UnitIcon.setCostText(unitDetailIcon.playerUnit);
    unitDetailIcon.UnitIcon.Favorite = unitDetailIcon.playerUnit.favorite;
    unitDetailIcon.UnitIcon.princessType.SetPrincessType(playerUnit);
    unitDetailIcon.UnitIcon.SpecialIcon = false;
    unitDetailIcon.UnitIcon.SpecialIconType = -1;
    unitDetailIcon.BreakThrough |= unitDetailIcon.playerUnit.unit.IsBreakThrough;
    unitDetailIcon.SkillUp |= unitDetailIcon.playerUnit.unit.IsMaterialUnitSkillUp;
    unitDetailIcon.Deardegreeup = ((unitDetailIcon.Deardegreeup ? 1 : 0) | (unitDetailIcon.playerUnit.unit.IsBreakThrough || unitDetailIcon.playerUnit.unit.IsMaterialUnitSkillUp || (!unitDetailIcon.playerUnit.unit.trust_target_flag || (double) unitDetailIcon.playerUnit.trust_rate >= (double) unitDetailIcon.playerUnit.trust_max_rate) ? 0 : (unitDetailIcon.playerUnit.unit.IsSea ? 1 : 0))) != 0;
    unitDetailIcon.Relevancedegreeup = ((unitDetailIcon.Relevancedegreeup ? 1 : 0) | (unitDetailIcon.playerUnit.unit.IsBreakThrough || unitDetailIcon.playerUnit.unit.IsMaterialUnitSkillUp || (!unitDetailIcon.playerUnit.unit.trust_target_flag || (double) unitDetailIcon.playerUnit.trust_rate >= (double) unitDetailIcon.playerUnit.trust_max_rate) ? 0 : (unitDetailIcon.playerUnit.unit.IsResonanceUnit ? 1 : 0))) != 0;
    unitDetailIcon.SetSprite(playerUnit.unit);
  }

  public void ResetUnit()
  {
    this.Deselect();
    if ((UnityEngine.Object) this.defaultIconSprite != (UnityEngine.Object) null)
    {
      this.icon.sprite2D = this.defaultIconSprite;
      this.icon.width = this.defaultIconSprite.texture.width;
      this.icon.height = this.defaultIconSprite.texture.height;
    }
    this.UnitIcon.BackgroundModeValue = UnitIcon.BackgroundMode.PlayerShadow;
    this.UnitIcon.RarityStar.gameObject.SetActive(false);
    this.UnitIcon.type.gameObject.SetActive(false);
    this.UnitIcon.BottomModeValue = UnitIconBase.BottomMode.Nothing;
  }

  public IEnumerator SetSelectUnit()
  {
    int num = 0;
    while (num < 4)
      this.upiconSprites[num++].gameObject.SetActive(false);
    this.UnitIcon.SetEmpty();
    this.UnitIcon.SelectUnit = true;
    this.txtPossession.gameObject.SetActive(false);
    return this.UnitIcon.SetSelectUnit();
  }

  public void SetMaterialUnitCache(
    PlayerUnit playerUnit,
    bool isNew,
    PlayerUnit[] playerUnits,
    PlayerUnit basePlayerUnit = null,
    bool isTrust = false)
  {
    this.playerUnit = playerUnit;
    if (playerUnit != (PlayerUnit) null)
      EventDelegate.Set(this.Button.onLongPress, (EventDelegate.Callback) (() =>
      {
        if (this.pressEvent != null)
          this.pressEvent();
        Unit0042Scene.changeScene(true, playerUnit, playerUnits, true, false);
      }));
    this.UnitIcon.princessType.DispPrincessType(false);
    this.InitializeLabel();
    if (basePlayerUnit != (PlayerUnit) null)
    {
      this.SetLabel(playerUnit, basePlayerUnit, isTrust);
      NGTween.playTweens(this.GetComponentsInChildren<UITweener>(true), NGTween.Kind.GRAYOUT, true);
    }
    this.SetUnitCache(this.playerUnit, false);
    this.BreakThrough |= playerUnit.unit.IsBreakThrough;
    this.SkillUp |= playerUnit.unit.IsMaterialUnitSkillUp;
    if (!((UnityEngine.Object) this.txtPossession != (UnityEngine.Object) null))
      return;
    PlayerMaterialUnit playerMaterialUnit = ((IEnumerable<PlayerMaterialUnit>) SMManager.Get<PlayerMaterialUnit[]>()).FirstOrDefault<PlayerMaterialUnit>((Func<PlayerMaterialUnit, bool>) (x => x.id == playerUnit.id));
    if (playerMaterialUnit != null)
    {
      this.txtPossession.gameObject.SetActive(true);
      this.txtPossession.SetTextLocalize(Consts.Format(Consts.GetInstance().unit_004_9_9_possession_text, (IDictionary) new Hashtable()
      {
        {
          (object) "Count",
          (object) playerMaterialUnit.quantity
        }
      }));
    }
    else
      this.txtPossession.gameObject.SetActive(false);
  }

  public void SetPlayerUnitCache(
    PlayerUnit playerUnit,
    PlayerUnit[] playerUnits,
    PlayerUnit basePlayerUnit = null,
    bool isMaterial = false,
    bool isMemory = false)
  {
    this.playerUnit = playerUnit;
    if (playerUnit != (PlayerUnit) null)
      EventDelegate.Set(this.Button.onLongPress, (EventDelegate.Callback) (() =>
      {
        if (this.pressEvent != null)
          this.pressEvent();
        if (Singleton<NGGameDataManager>.GetInstance().IsSea)
          Singleton<NGSceneManager>.GetInstance().LastHeaderType = new CommonRoot.HeaderType?(Singleton<CommonRoot>.GetInstance().headerType);
        Unit0042Scene.changeScene(true, playerUnit, playerUnits, isMaterial, isMemory);
      }));
    this.SetSprite(playerUnit.unit);
    this.UnitIcon.princessType.SetPrincessType(playerUnit);
    if (basePlayerUnit != (PlayerUnit) null)
    {
      this.SetLabel(playerUnit, basePlayerUnit, false);
      NGTween.playTweens(this.GetComponentsInChildren<UITweener>(true), NGTween.Kind.GRAYOUT, true);
    }
    this.SetUnitCache(this.playerUnit, false);
    if (!((UnityEngine.Object) this.txtPossession != (UnityEngine.Object) null))
      return;
    this.txtPossession.gameObject.SetActive(false);
  }

  public void SetUnitCache(PlayerUnit playerUnit, bool isGray = false)
  {
    this.UnitIcon.SetUnitCache(playerUnit.unit, playerUnit.GetElement(), playerUnit.job_id);
    this.unit = playerUnit.unit;
    this.UnitIcon.setLevelText(this.playerUnit);
    this.UnitIcon.ShowBottomInfo(UnitSortAndFilter.SORT_TYPES.BranchOfAnArmy);
    this.UnitIcon.setCostText(this.playerUnit);
    this.UnitIcon.Favorite = this.playerUnit.favorite;
    this.UnitIcon.SpecialIcon = false;
    this.UnitIcon.SpecialIconType = -1;
    this.SetSprite(playerUnit.unit);
  }

  private class SpriteCache
  {
    public UnityEngine.Sprite thumbnail;
    public GameObject gear;

    public SpriteCache(UnityEngine.Sprite s, GameObject o)
    {
      this.thumbnail = s;
      this.gear = o;
    }
  }
}
