﻿// Decompiled with JetBrains decompiler
// Type: LumpToutaResultScene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LumpToutaResultScene : NGSceneBase
{
  [SerializeField]
  private LumpToutaResultMenu menu;

  public override IEnumerator onInitSceneAsync()
  {
    LumpToutaResultScene toutaResultScene = this;
    Future<GameObject> bgF = Res.Prefabs.BackGround.UnitBackground_60.Load<GameObject>();
    IEnumerator e = bgF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    toutaResultScene.backgroundPrefab = bgF.Result;
  }

  public IEnumerator onStartSceneAsync(
    List<Unit004832Menu.ResultPlayerUnit> resultPlayerUnits,
    int incrementMedal,
    GainTrustResult[] gainTrustResults,
    UnlockQuest[] unlockQuests)
  {
    yield return (object) this.menu.Init(resultPlayerUnits, incrementMedal, gainTrustResults, unlockQuests);
  }

  public void onStartScene(
    List<Unit004832Menu.ResultPlayerUnit> resultPlayerUnits,
    int incrementMedal,
    GainTrustResult[] gainTrustResults,
    UnlockQuest[] unlockQuests)
  {
    Singleton<CommonRoot>.GetInstance().isLoading = false;
  }
}
