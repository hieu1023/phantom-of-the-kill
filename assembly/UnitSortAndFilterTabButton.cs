﻿// Decompiled with JetBrains decompiler
// Type: UnitSortAndFilterTabButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class UnitSortAndFilterTabButton : UnitSortAndFilterButton
{
  [SerializeField]
  private UILabel TextSelect;

  public void SetText(string title)
  {
    this.TextSelect.SetTextLocalize(title);
  }

  public void SetTextColor(Color col)
  {
    this.TextSelect.color = col;
  }
}
