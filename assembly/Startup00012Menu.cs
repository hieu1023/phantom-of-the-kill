﻿// Decompiled with JetBrains decompiler
// Type: Startup00012Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Startup00012Menu : BackButtonMenuBase
{
  public int oldDay = -7;
  private List<GameObject> cloneList = new List<GameObject>();
  public bool isContinue = true;
  [SerializeField]
  protected UILabel TxtTitle;
  [SerializeField]
  private NGxScrollMasonry scrollMasonry;
  [SerializeField]
  private List<GameObject> categoryList;
  [SerializeField]
  private UILabel badgeDisplay;
  public UIPanel panel;
  public UIWidget widget;
  public UITextList textList;
  public GameObject scrollViewObj;
  public GameObject logContainer;
  public GameObject newSpriteObj;
  private UIScrollView scrollView;
  private UILabel label;
  public GameObject scrollBar;
  public bool unRead;
  private bool isScroll;
  [SerializeField]
  private string nextSceneName;
  private string title;

  protected virtual int GetWidth()
  {
    return 532;
  }

  public List<GameObject> CategoryList
  {
    get
    {
      return this.categoryList;
    }
    set
    {
      this.categoryList = value;
    }
  }

  public UILabel BadgeDisplay
  {
    get
    {
      return this.badgeDisplay;
    }
    set
    {
      this.badgeDisplay = value;
    }
  }

  public string NextSceneName
  {
    get
    {
      return this.nextSceneName;
    }
    set
    {
      this.nextSceneName = value;
    }
  }

  protected virtual bool DeleteTitle()
  {
    return false;
  }

  public virtual void IbtnClose()
  {
    if (this.IsPushAndSet() || Singleton<NGGameDataManager>.GetInstance().InfoOrLoginBonusJump())
      return;
    MypageScene.ChangeScene(false, false, false);
  }

  public virtual void IbtnList()
  {
    if (this.IsPushAndSet())
      return;
    this.EndScene();
    Singleton<NGSceneManager>.GetInstance().changeScene(this.NextSceneName, true, (object[]) Array.Empty<object>());
  }

  public IEnumerator InitAsync()
  {
    foreach (GameObject clone in this.cloneList)
    {
      UILabel component1 = clone.GetComponent<UILabel>();
      if ((UnityEngine.Object) component1 != (UnityEngine.Object) null)
        component1.SetDirty();
      UI2DSprite component2 = clone.GetComponent<UI2DSprite>();
      if ((UnityEngine.Object) component2 != (UnityEngine.Object) null)
        component2.SetDirty();
      clone.SetActive(true);
    }
    this.scrollView.enabled = true;
    this.scrollBar.SetActive(true);
    this.scrollView.ResetPosition();
    yield return (object) new WaitForSeconds(0.1f);
    if (this.isScroll)
    {
      this.scrollView.enabled = false;
      this.scrollBar.SetActive(false);
    }
    else
    {
      this.scrollView.enabled = true;
      this.scrollBar.SetActive(true);
    }
  }

  private void EndScene()
  {
    foreach (Component child in this.scrollMasonry.Scroll.transform.GetChildren())
      UnityEngine.Object.Destroy((UnityEngine.Object) child.gameObject);
    this.scrollMasonry.Reset();
  }

  public IEnumerator InitSceneAsync(OfficialInformationArticle info)
  {
    this.EndScene();
    this.scrollView = this.scrollViewObj.GetComponentInChildren<UIScrollView>();
    this.unRead = false;
    try
    {
      this.unRead = Persist.infoUnRead.Data.GetUnRead(info);
    }
    catch
    {
      Persist.infoUnRead.Delete();
    }
    if ((UnityEngine.Object) this.newSpriteObj != (UnityEngine.Object) null)
      this.newSpriteObj.SetActive(!this.unRead);
    IEnumerator e = ServerTime.WaitSync();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Persist.infoUnRead.Data.SetUnRead(info, ServerTime.NowAppTime());
    Persist.infoUnRead.Flush();
    switch (info.sub_category_id)
    {
      case 1:
        if (this.CategoryList != null)
        {
          this.CategoryList.ToggleOnce(0);
          break;
        }
        break;
      case 2:
        if (this.CategoryList != null)
        {
          this.CategoryList.ToggleOnce(1);
          break;
        }
        break;
      case 3:
        if (this.CategoryList != null)
        {
          this.CategoryList.ToggleOnce(2);
          break;
        }
        break;
      case 4:
        if (this.CategoryList != null)
        {
          this.CategoryList.ToggleOnce(3);
          break;
        }
        break;
    }
    this.BadgeDisplay.SetTextLocalize(info.badge_display);
    if (!this.DeleteTitle())
    {
      switch (info.category_id)
      {
        case 1:
          this.title = Consts.GetInstance().INFO_CATEGORY1;
          break;
        case 2:
          this.title = Consts.GetInstance().INFO_CATEGORY2;
          break;
        case 3:
          this.title = Consts.GetInstance().INFO_CATEGORY3;
          break;
      }
    }
    e = DetailController.Init(this.scrollMasonry, this.title, info.bodies);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public override void onBackButton()
  {
    if (this.IsPushAndSet())
      return;
    MypageScene.ChangeScene(false, false, false);
    DetailController.Release();
  }
}
