﻿// Decompiled with JetBrains decompiler
// Type: TutorialTenseiResultPage
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections;

public class TutorialTenseiResultPage : TutorialPageBase
{
  public override IEnumerator Show()
  {
    WebAPI.Response.TutorialTutorialRagnarokResume resume = Singleton<TutorialRoot>.GetInstance().Resume;
    Singleton<NGSceneManager>.GetInstance().changeScene("unit004_9_8", false, (object) resume.after_levelup1_player_unit, (object) resume.after_transmigrate_player_unit, (object) Unit00499Scene.Mode.Transmigration, (object) false);
    yield break;
  }

  public override void NextPage()
  {
    base.NextPage();
  }

  public override void Advise()
  {
    Singleton<TutorialRoot>.GetInstance().ForceShowAdvice("newchapter_reincarnation4_tutorial", (System.Action) (() => this.NextPage()));
  }
}
