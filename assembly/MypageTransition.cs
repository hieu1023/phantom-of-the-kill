﻿// Decompiled with JetBrains decompiler
// Type: MypageTransition
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class MypageTransition : MonoBehaviour
{
  public int L_id;
  public bool hardmode;
  [SerializeField]
  private MypageMenu menu;

  public void onQuest()
  {
    if (this.menu.IsPushAndSet())
      return;
    NGSoundManager instance = Singleton<NGSoundManager>.GetInstance();
    if ((UnityEngine.Object) instance != (UnityEngine.Object) null)
      instance.playSE("SE_1002", false, 0.0f, -1);
    PlayerStoryQuestS[] playerStoryQuestSArray = SMManager.Get<PlayerStoryQuestS[]>();
    if (playerStoryQuestSArray != null && ((IEnumerable<PlayerStoryQuestS>) playerStoryQuestSArray).Any<PlayerStoryQuestS>((Func<PlayerStoryQuestS, bool>) (x => x.quest_story_s.quest_xl_QuestStoryXL == 4)))
    {
      this.StartCoroutine(this.openStorySelectPopup());
    }
    else
    {
      Singleton<CommonRoot>.GetInstance().getBackgroundComponent<QuestBG>().CloudAnim(false);
      this.StartCoroutine(this.GetComponent<BGChange>().asyncBgAnim(QuestBG.AnimApply.MyPage, 1f));
      Quest00240723Scene.ChangeScene0024(true, this.L_id, this.hardmode, false);
    }
  }

  public void onBattle()
  {
    if (this.menu.IsPushAndSet())
      return;
    NGSoundManager instance = Singleton<NGSoundManager>.GetInstance();
    if ((UnityEngine.Object) instance != (UnityEngine.Object) null)
      instance.playSE("SE_1002", false, 0.0f, -1);
    this.StartCoroutine(this.openBattleSelectPopup());
  }

  public void onEvent()
  {
    if (this.menu.IsPushAndSet())
      return;
    NGSoundManager instance = Singleton<NGSoundManager>.GetInstance();
    if ((UnityEngine.Object) instance != (UnityEngine.Object) null)
      instance.playSE("SE_1002", false, 0.0f, -1);
    Quest00217Scene.ChangeScene(true);
  }

  public void onMenu()
  {
    if (this.menu.IsPushAndSet())
      return;
    Singleton<NGSceneManager>.GetInstance().changeScene("story001_9_1", true, (object[]) Array.Empty<object>());
  }

  public void onInfo()
  {
    if (this.menu.IsPushAndSet())
      return;
    Singleton<NGSceneManager>.GetInstance().changeScene("mypage001_8_1", true, (object[]) Array.Empty<object>());
  }

  public void onMission()
  {
    if (this.menu.IsPushAndSet())
      return;
    Singleton<NGSceneManager>.GetInstance().changeScene("dailymission027_2", true, (object[]) Array.Empty<object>());
  }

  public void onPanelMission()
  {
    if (this.menu.IsPushAndSet())
      return;
    Singleton<NGSceneManager>.GetInstance().changeScene("dailymission027_1", true, (object[]) Array.Empty<object>());
  }

  public void onPresent()
  {
    if (this.menu.IsPushAndSet())
      return;
    Singleton<NGSceneManager>.GetInstance().changeScene("mypage001_7", true, (object[]) Array.Empty<object>());
  }

  public void onColosseum()
  {
    if (this.menu.IsPushAndSet())
      return;
    this._on_colosseum_func();
  }

  public void onRoulette()
  {
    if (this.menu.IsPushAndSet())
      return;
    Singleton<NGSceneManager>.GetInstance().changeScene("roulette", true, (object[]) Array.Empty<object>());
  }

  public static MypageTransition.ColosseumStatus getColosseumStatus()
  {
    PlayerUnit[] playerUnitArray = SMManager.Get<PlayerUnit[]>();
    if (((IEnumerable<PlayerUnit>) playerUnitArray).Count<PlayerUnit>() < 3)
      return MypageTransition.ColosseumStatus.NOT_ENOUGH_UNIT;
    Player player = SMManager.Get<Player>();
    PlayerUnit[] array = ((IEnumerable<PlayerUnit>) playerUnitArray).OrderBy<PlayerUnit, int>((Func<PlayerUnit, int>) (x => x.cost)).ToArray<PlayerUnit>();
    int num = 0;
    for (int index = 0; index < 3; ++index)
      num += array[index].cost;
    return player.max_cost >= num ? MypageTransition.ColosseumStatus.OK : MypageTransition.ColosseumStatus.NOT_ENOUGH_COST;
  }

  private void _on_colosseum_func()
  {
    NGSoundManager instance = Singleton<NGSoundManager>.GetInstance();
    if ((UnityEngine.Object) instance != (UnityEngine.Object) null)
      instance.playSE("SE_1002", false, 0.0f, -1);
    switch (MypageTransition.getColosseumStatus())
    {
      case MypageTransition.ColosseumStatus.OK:
        Colosseum0234Scene.ChangeScene(true, false);
        break;
      case MypageTransition.ColosseumStatus.NOT_ENOUGH_COST:
        this.StartCoroutine(this.openPopup008161CostInsufficiency());
        break;
      case MypageTransition.ColosseumStatus.NOT_ENOUGH_UNIT:
        this.StartCoroutine(this.openPopup008161UnitInsufficiency());
        break;
    }
  }

  public void onCharacter()
  {
    if (this.menu.IsPushAndSet())
      return;
    NGSoundManager instance = Singleton<NGSoundManager>.GetInstance();
    if ((UnityEngine.Object) instance != (UnityEngine.Object) null)
      instance.playSE("SE_1002", false, 0.0f, -1);
    Quest00214Scene.ChangeScene(true);
  }

  public void onMulti()
  {
    if (this.menu.IsPushAndSet())
      return;
    this._on_multi_func();
  }

  private void _on_multi_func()
  {
    NGSoundManager instance = Singleton<NGSoundManager>.GetInstance();
    if ((UnityEngine.Object) instance != (UnityEngine.Object) null)
      instance.playSE("SE_1002", false, 0.0f, -1);
    Versus0261Scene.ChangeScene0261(true);
  }

  public void onEarth()
  {
    if (this.menu.IsPushAndSet())
      return;
    Singleton<CommonRoot>.GetInstance().isTouchBlock = true;
    NGSoundManager instance = Singleton<NGSoundManager>.GetInstance();
    if ((UnityEngine.Object) instance != (UnityEngine.Object) null)
      instance.playSE("SE_1039", false, 0.0f, -1);
    this.StartCoroutine(this.menu.CloudAnimationStart());
  }

  public void onUnitEdit()
  {
    if (this.menu.IsPushAndSet())
      return;
    Singleton<NGSceneManager>.GetInstance().changeScene("mypage_edit", true, (object[]) Array.Empty<object>());
  }

  public void onExplore()
  {
    if (this.menu.IsPushAndSet())
      return;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    Singleton<NGSceneManager>.GetInstance().destoryNonStackScenes();
    Singleton<NGSceneManager>.GetInstance().destroyCurrentScene();
    Explore033TopScene.changeScene(true, true);
  }

  private IEnumerator openPopup008161UnitInsufficiency()
  {
    Future<GameObject> prefabF = Res.Prefabs.popup.popup_008_16_1__anim_popup01.Load<GameObject>();
    IEnumerator e = prefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    GameObject pObj = Singleton<PopupManager>.GetInstance().open(prefabF.Result, false, false, false, true, false, false, "SE_1006");
    pObj.SetActive(false);
    e = pObj.GetComponent<Friend008161Menu>().Init(Consts.GetInstance().COLOSSEUM_ALERT_TITLE_UNIT, Consts.GetInstance().COLOSSEUM_ALERT_MESSAGE_UNIT);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    pObj.SetActive(true);
  }

  private IEnumerator openPopup008161CostInsufficiency()
  {
    Future<GameObject> prefabF = Res.Prefabs.popup.popup_008_16_1__anim_popup01.Load<GameObject>();
    IEnumerator e = prefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    GameObject pObj = Singleton<PopupManager>.GetInstance().open(prefabF.Result, false, false, false, true, false, false, "SE_1006");
    pObj.SetActive(false);
    e = pObj.GetComponent<Friend008161Menu>().Init(Consts.GetInstance().COLOSSEUM_ALERT_TITLE_COST, Consts.GetInstance().COLOSSEUM_ALERT_MESSAGE_COST);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    pObj.SetActive(true);
  }

  private IEnumerator openStorySelectPopup()
  {
    MypageTransition mypageTransition = this;
    Future<GameObject> prefabF = new ResourceObject("Prefabs/mypage/dir_story_menu").Load<GameObject>();
    IEnumerator e = prefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    GameObject popup = prefabF.Result.Clone((Transform) null);
    popup.SetActive(false);
    PopupMypageStorySelect script = popup.GetComponent<PopupMypageStorySelect>();
    // ISSUE: reference to a compiler-generated method
    e = script.Initialize(new System.Action<int, bool>(mypageTransition.\u003CopenStorySelectPopup\u003Eb__24_0), (System.Action) null, mypageTransition.menu, (MonoBehaviour) null);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    mypageTransition.menu.SetMypageBannerHilight(false);
    Singleton<PopupManager>.GetInstance().open(popup, false, false, true, true, true, false, "SE_1006");
    Singleton<NGSoundManager>.GetInstance().playSE("SE_1056", false, 0.0f, -1);
    yield return (object) new WaitForSeconds(1f);
    script.isPush = false;
  }

  private IEnumerator openBattleSelectPopup()
  {
    MypageTransition mypageTransition = this;
    Future<GameObject> prefabF = new ResourceObject("Prefabs/mypage/dir_battle_menu").Load<GameObject>();
    IEnumerator e = prefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    GameObject popup = prefabF.Result.Clone((Transform) null);
    popup.SetActive(false);
    // ISSUE: reference to a compiler-generated method
    e = popup.GetComponent<PopupMypageBattleSelect>().Initialize(new System.Action<PopupMypageBattleSelect.Selection>(mypageTransition.\u003CopenBattleSelectPopup\u003Eb__25_0), mypageTransition.menu);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    mypageTransition.menu.SetMypageBannerHilight(false);
    Singleton<PopupManager>.GetInstance().open(popup, false, false, true, true, false, false, "SE_1006");
  }

  private IEnumerator ChangeScene0024(int L, bool hard)
  {
    Singleton<NGSceneManager>.GetInstance().destoryNonStackScenes();
    Singleton<NGSceneManager>.GetInstance().destroyCurrentScene();
    Quest00240723Scene.ChangeScene0024(false, L, hard, true);
    yield break;
  }

  public void OnPushTotalPaymentBonus()
  {
    if (this.menu.IsPushAndSet())
      return;
    this.StartCoroutine(this.OpenTotalPaymentBonusPopup());
  }

  private IEnumerator OpenTotalPaymentBonusPopup()
  {
    MypageTransition mypageTransition = this;
    Singleton<CommonRoot>.GetInstance().ShowLoadingLayer(1, false);
    // ISSUE: reference to a compiler-generated method
    Future<WebAPI.Response.PaymentbonusList> api = WebAPI.PaymentbonusList(new System.Action<WebAPI.Response.UserError>(mypageTransition.\u003COpenTotalPaymentBonusPopup\u003Eb__28_0));
    IEnumerator e = api.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if (!api.HasResult || api.Result == null)
    {
      Singleton<CommonRoot>.GetInstance().HideLoadingLayer();
      mypageTransition.menu.IsPush = false;
    }
    else
    {
      WebAPI.Response.PaymentbonusList response = api.Result;
      Future<GameObject> loader = new ResourceObject("Prefabs/popup/popup_TotalPaymentBonus").Load<GameObject>();
      e = loader.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      GameObject result = loader.Result;
      Singleton<CommonRoot>.GetInstance().HideLoadingLayer();
      TotalPaymentBonusPopup component = Singleton<PopupManager>.GetInstance().open(result, false, false, false, true, false, false, "SE_1006").GetComponent<TotalPaymentBonusPopup>();
      component.transform.localScale = Vector3.zero;
      e = component.Initialize(response, mypageTransition.menu.EventButtonController.GetButton<TotalPaymentBonusButton>());
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
  }

  public enum ColosseumStatus
  {
    OK,
    NOT_ENOUGH_COST,
    NOT_ENOUGH_UNIT,
  }
}
