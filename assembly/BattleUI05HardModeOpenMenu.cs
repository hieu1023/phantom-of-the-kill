﻿// Decompiled with JetBrains decompiler
// Type: BattleUI05HardModeOpenMenu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System.Collections;
using UnityEngine;

public class BattleUI05HardModeOpenMenu : ResultMenuBase
{
  private GameObject popupObject;
  private string title;
  private string message;

  public bool ToNext { set; get; }

  public override IEnumerator Init(BattleInfo info, BattleEnd result)
  {
    this.title = result.unlock_messages[0].title;
    this.message = result.unlock_messages[0].message;
    Future<GameObject> popupPrefab = Res.Prefabs.popup.popup_020_31__anim_popup01.Load<GameObject>();
    IEnumerator e = popupPrefab.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.popupObject = popupPrefab.Result;
    this.ToNext = false;
  }

  public override IEnumerator Run()
  {
    BattleUI05HardModeOpenMenu menu = this;
    Singleton<PopupManager>.GetInstance().open(menu.popupObject, false, false, false, true, false, false, "SE_1006").GetComponent<BattleUI05HardModeOpenPopup>().Init(menu, menu.title, menu.message);
    while (!menu.ToNext)
      yield return (object) null;
    Singleton<PopupManager>.GetInstance().dismiss(false);
  }
}
