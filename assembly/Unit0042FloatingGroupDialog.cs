﻿// Decompiled with JetBrains decompiler
// Type: Unit0042FloatingGroupDialog
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Unit0042FloatingGroupDialog : FloatingDialogBase
{
  [SerializeField]
  private UILabel txtGroupName;
  [SerializeField]
  private UILabel txtDescription;
  [SerializeField]
  private UISprite slcGroupType;
  [SerializeField]
  private SpriteSelectDirect spriteSelectDirect;

  public void Init(string name, string descript, string spriteName)
  {
    this.txtGroupName.SetTextLocalize(name);
    this.txtDescription.SetTextLocalize(descript);
    this.spriteSelectDirect.SetSpriteName<string>(spriteName, true);
  }
}
