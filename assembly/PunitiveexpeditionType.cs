﻿// Decompiled with JetBrains decompiler
// Type: PunitiveexpeditionType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;

public static class PunitiveexpeditionType
{
  public static bool IsGuild(this WebAPI.Response.EventTop eventTop)
  {
    return eventTop.period_type == 2;
  }

  public static bool IsGuild(this EventBattleFinish eventBattleFinish)
  {
    return eventBattleFinish.period_type == 2;
  }

  public static bool IsGuild(this EventInfo eventInfo)
  {
    return eventInfo.period_type == 2;
  }
}
