﻿// Decompiled with JetBrains decompiler
// Type: Battle02UIPlayerStatus
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using UnityEngine;

public class Battle02UIPlayerStatus : Battle02MenuBase
{
  [SerializeField]
  protected UI2DSprite link_Character;
  [SerializeField]
  protected UILabel txt_CharaName_18;
  [SerializeField]
  protected UILabel txt_Job_22;
  [SerializeField]
  protected UILabel txt_Lv_22;
  [SerializeField]
  protected UILabel txt_HP_22;
  [SerializeField]
  protected UILabel txt_HP_Max_22;
  [SerializeField]
  protected UILabel txt_Combat_22;

  public override void UpdateData()
  {
    if (this.modified == null || !this.modified.isChangedOnce())
      return;
    BL.Unit unit = this.modified.value;
    this.CreateUnitSprite(this.link_Character);
    this.setText(this.txt_CharaName_18, unit.unit.name);
    this.setText(this.txt_Job_22, unit.job.name);
    this.setText(this.txt_Lv_22, unit.lv);
    this.setText(this.txt_HP_22, unit.hp);
    if (unit.hp <= unit.parameter.Hp / 10)
    {
      this.txt_HP_22.color = this.mRed;
      TweenColor.Begin(this.txt_HP_22.gameObject, 1f, new Color(0.5f, 0.0f, 0.0f)).style = UITweener.Style.Loop;
    }
    if (unit.parameter.HpIncr > 0)
      this.setText(this.txt_HP_Max_22, "[00dc1e]" + (object) unit.parameter.Hp + "[-]");
    else if (unit.parameter.HpIncr < 0)
      this.setText(this.txt_HP_Max_22, "[fa0000]" + (object) unit.parameter.Hp + "[-]");
    else
      this.setText(this.txt_HP_Max_22, unit.parameter.Hp);
    Judgement.BattleParameter battleParameter = Judgement.BattleParameter.FromBeUnit((BL.ISkillEffectListUnit) unit, false, true);
    this.setColordText(this.txt_Combat_22, battleParameter.Combat, battleParameter.CombatIncr);
  }

  public override void onBackButton()
  {
  }
}
