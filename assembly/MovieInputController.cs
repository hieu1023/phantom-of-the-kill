﻿// Decompiled with JetBrains decompiler
// Type: MovieInputController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof (RawImage), typeof (CriManaMovieControllerForUI))]
public class MovieInputController : MonoBehaviour, IPointerClickHandler, IEventSystemHandler
{
  [HideInInspector]
  public bool enableSkip = true;
  [SerializeField]
  private WindowsMovieController movieController;

  public void OnPointerClick(PointerEventData ped)
  {
    if (!this.enableSkip)
      return;
    this.movieController.StopMovie();
  }
}
