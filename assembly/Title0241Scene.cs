﻿// Decompiled with JetBrains decompiler
// Type: Title0241Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

public class Title0241Scene : NGSceneBase
{
  [SerializeField]
  private Title0241Menu menu;

  public static void ChangeScene00241(bool stack, string target_player_id)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("title024_1", (stack ? 1 : 0) != 0, (object) target_player_id);
  }

  public IEnumerator onStartSceneAsync(string target_player_id)
  {
    IEnumerator e = this.menu.Init(target_player_id);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }
}
