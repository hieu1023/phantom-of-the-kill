﻿// Decompiled with JetBrains decompiler
// Type: Startup0008TopPageMenu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

public class Startup0008TopPageMenu : MonoBehaviour
{
  [SerializeField]
  private UITextList agreementTextList;
  [SerializeField]
  private UILabel agreementTitle;

  public void Initialize(string title, string hedder, string text)
  {
    this.agreementTitle.SetText(title);
    this.agreementTextList.Clear();
    this.agreementTextList.Add(hedder);
    this.agreementTextList.Add(text);
  }

  public IEnumerator ScrollValue()
  {
    yield return (object) null;
    this.agreementTextList.scrollValue = 0.0f;
    if ((double) this.agreementTextList.scrollValue != 0.0)
      this.agreementTextList.scrollValue = 0.0f;
  }
}
