﻿// Decompiled with JetBrains decompiler
// Type: GuildChatStampSelectItemController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class GuildChatStampSelectItemController : MonoBehaviour
{
  public int stampID;
  [SerializeField]
  private UISprite stampImage;

  private void Start()
  {
  }

  private void Update()
  {
  }

  public void InitializeGuildChatStampItem(int stampID)
  {
    this.stampID = stampID;
    Singleton<CommonRoot>.GetInstance().guildChatManager.SetStampSprite(this.stampImage, this.stampID);
  }

  public void Clear()
  {
    this.stampID = 0;
    this.gameObject.SetActive(false);
  }

  public void OnStampItemClicked()
  {
    Singleton<CommonRoot>.GetInstance().guildChatManager.stampSelectViewController.SelectStamp(this.stampID);
  }
}
