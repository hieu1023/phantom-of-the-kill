﻿// Decompiled with JetBrains decompiler
// Type: Bugu005415Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bugu005415Menu : BackButtonMenuBase
{
  public EffectControllerArmorRepair effect;
  [SerializeField]
  private GameObject back_button_;

  private IEnumerator SkipCurrentAnimation()
  {
    if (!this.effect.sound_effect_.result)
    {
      Singleton<NGSoundManager>.GetInstance().stopSE(-1);
      Time.timeScale = 100f;
      float tempFixedDeltaTime = Time.fixedDeltaTime;
      Time.fixedDeltaTime = tempFixedDeltaTime * 100f;
      while (this.effect.isAnimation)
        yield return (object) this.effect.isAnimation;
      Time.timeScale = 1f;
      Time.fixedDeltaTime = tempFixedDeltaTime;
      yield return (object) null;
      this.effect.EndEffect();
    }
  }

  public virtual void IbtnBack()
  {
    if (this.effect.isAnimation)
    {
      this.StartCoroutine(this.SkipCurrentAnimation());
    }
    else
    {
      Singleton<NGSoundManager>.GetInstance().stopSE(-1);
      this.backScene();
    }
  }

  public override void onBackButton()
  {
    this.IbtnBack();
  }

  public virtual void IbtnStartcomposite()
  {
  }

  public IEnumerator SetEffectData(
    List<ItemInfo> thum_list,
    List<WebAPI.Response.ItemGearRepairRepair_results> result_list)
  {
    this.effect.gameObject.SetActive(true);
    IEnumerator e = this.effect.Set(thum_list, this.back_button_, result_list, (List<WebAPI.Response.ItemGearRepairListRepair_results>) null);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public IEnumerator SetEffectData(
    List<ItemInfo> thum_list,
    List<WebAPI.Response.ItemGearRepairListRepair_results> result_list)
  {
    this.effect.gameObject.SetActive(true);
    IEnumerator e = this.effect.Set(thum_list, this.back_button_, (List<WebAPI.Response.ItemGearRepairRepair_results>) null, result_list);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }
}
