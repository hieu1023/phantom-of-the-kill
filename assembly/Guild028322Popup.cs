﻿// Decompiled with JetBrains decompiler
// Type: Guild028322Popup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;
using UnityEngine;

public class Guild028322Popup : BackButtonMenuBase
{
  [SerializeField]
  private UILabel popupTitle;
  [SerializeField]
  private UILabel popupDesc;
  private Guild0283Menu menu;

  public void Initialize(Guild0283Menu menu)
  {
    if ((UnityEngine.Object) this.GetComponent<UIWidget>() != (UnityEngine.Object) null)
      this.GetComponent<UIWidget>().alpha = 0.0f;
    this.menu = menu;
    this.popupTitle.SetTextLocalize(Consts.Format(Consts.GetInstance().POPUP_GUILD_MENU_DISMISS_CONFIRM_TITLE, (IDictionary) null));
    this.popupDesc.SetTextLocalize(Consts.Format(Consts.GetInstance().POPUP_GUILD_MENU_DISMISS_DESC5, (IDictionary) null));
  }

  public override void onBackButton()
  {
    Singleton<PopupManager>.GetInstance().closeAll(false);
  }

  private IEnumerator DismissGuild()
  {
    Guild028322Popup guild028322Popup = this;
    Singleton<PopupManager>.GetInstance().closeAllWithoutAnim(false);
    while (Singleton<PopupManager>.GetInstance().isOpenNoFinish)
      yield return (object) null;
    Singleton<CommonRoot>.GetInstance().loadingMode = 1;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    // ISSUE: reference to a compiler-generated method
    Future<WebAPI.Response.GuildDissolute> ft = WebAPI.GuildDissolute(false, new System.Action<WebAPI.Response.UserError>(guild028322Popup.\u003CDismissGuild\u003Eb__5_0));
    IEnumerator e = ft.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if (ft.Result != null)
    {
      Singleton<CommonRoot>.GetInstance().isLoading = false;
      Singleton<CommonRoot>.GetInstance().loadingMode = 0;
      Guild02811Scene.ChangeScene();
    }
  }

  public void onYesButton()
  {
    this.StartCoroutine(this.DismissGuild());
  }
}
