﻿// Decompiled with JetBrains decompiler
// Type: PopupClassChange
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using UnityEngine;

public class PopupClassChange : MonoBehaviour
{
  private static readonly string[] spriteName = new string[12]
  {
    "text_ClassDown.png__GUI__versus_results_common__versus_results_common_prefab",
    "text_ClassStayed.png__GUI__versus_results_common__versus_results_common_prefab",
    "text_ClassStayedTop.png__GUI__versus_results_common__versus_results_common_prefab",
    "text_ClassUp.png__GUI__versus_results_common__versus_results_common_prefab",
    "text_ClassTitle.png__GUI__versus_results_common__versus_results_common_prefab",
    "text_ClassTitleTop.png__GUI__versus_results_common__versus_results_common_prefab",
    "text_ClassDownZone.png__GUI__versus_results_common__versus_results_common_prefab",
    "text_ClassStayedZone.png__GUI__versus_results_common__versus_results_common_prefab",
    "text_ClassStayedTop.png__GUI__versus_results_common__versus_results_common_prefab",
    "text_ClassUpZone.png__GUI__versus_results_common__versus_results_common_prefab",
    "text_ClassTitleZone.png__GUI__versus_results_common__versus_results_common_prefab",
    "text_ClassTitleTop.png__GUI__versus_results_common__versus_results_common_prefab"
  };
  private static readonly string spriteNameIsLowest = "text_ClassStayedBottom.png__GUI__versus_results_common__versus_results_common_prefab";
  private static readonly string spriteNameIsLowestZone = "text_ClassStayedZoneBottom.png__GUI__versus_results_common__versus_results_common_prefab";
  [SerializeField]
  private UISprite sprite;

  public void ChangeSprite(PvpClassKind.Condition c, bool isLowest = false)
  {
    this.sprite.spriteName = !isLowest || c != PvpClassKind.Condition.STAY_ZONE ? (!isLowest || c != PvpClassKind.Condition.STAY ? PopupClassChange.spriteName[(int) c] : PopupClassChange.spriteNameIsLowest) : PopupClassChange.spriteNameIsLowestZone;
    this.sprite.MakePixelPerfect();
  }
}
