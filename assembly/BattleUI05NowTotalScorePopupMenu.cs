﻿// Decompiled with JetBrains decompiler
// Type: BattleUI05NowTotalScorePopupMenu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System.Collections;
using UnityEngine;

public class BattleUI05NowTotalScorePopupMenu : ResultMenuBase
{
  [SerializeField]
  private GameObject dir_RankingEvent;
  [SerializeField]
  private GameObject dir_StageTitle;
  private QuestScoreBattleFinishContext campaign;
  private GameObject nowRankPopupPrefab;

  public override IEnumerator Init(BattleInfo info, BattleEnd result)
  {
    this.campaign = result.score_campaigns[0];
    IEnumerator e = this.LoadResources();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  private IEnumerator LoadResources()
  {
    if ((UnityEngine.Object) this.nowRankPopupPrefab == (UnityEngine.Object) null)
    {
      Future<GameObject> nowRankPopupPrefabF = Res.Prefabs.battle.dir_TotalEvent_BattleResult.Load<GameObject>();
      IEnumerator e = nowRankPopupPrefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.nowRankPopupPrefab = nowRankPopupPrefabF.Result;
      nowRankPopupPrefabF = (Future<GameObject>) null;
    }
  }

  public override IEnumerator Run()
  {
    bool toNext = false;
    GameObject gameObject = Singleton<PopupManager>.GetInstance().open(this.nowRankPopupPrefab, false, false, false, true, false, false, "SE_1006");
    BattleUI05NowRankingPopup component = gameObject.GetComponent<BattleUI05NowRankingPopup>();
    gameObject.SetActive(false);
    component.Init(this.campaign.score_total, this.campaign.rank, this.campaign.rank_before);
    gameObject.SetActive(true);
    gameObject.SetActive(true);
    component.SetCloseCallBack((System.Action) (() =>
    {
      toNext = true;
      Singleton<PopupManager>.GetInstance().onDismiss();
    }));
    while (!toNext)
      yield return (object) null;
    yield return (object) new WaitForSeconds(0.5f);
  }

  public override IEnumerator OnFinish()
  {
    this.dir_RankingEvent.SetActive(false);
    this.dir_StageTitle.SetActive(false);
    yield break;
  }
}
