﻿// Decompiled with JetBrains decompiler
// Type: Unit0044ReisouMenu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Unit0044ReisouMenu : Bugu005SelectItemListMenuBase
{
  [SerializeField]
  protected UILabel TxtNumberpossession;
  [SerializeField]
  protected UIButton BtnSort;
  private GameObject StatusChangePopupPrefab;
  private GameObject EquipAlertPopupPrefab;
  private List<int> equipedReisouIdList;
  [SerializeField]
  protected GameObject dirNoItem;
  private GameCore.ItemInfo gearInfo;
  private GameCore.ItemInfo reisouInfo;
  private bool isEarthMode;
  protected new GameObject reisouPopupPrefab;

  public GameCore.ItemInfo GearInfo
  {
    set
    {
      this.gearInfo = value;
    }
    get
    {
      return this.gearInfo;
    }
  }

  public GameCore.ItemInfo ReisouInfo
  {
    set
    {
      this.reisouInfo = value;
    }
    get
    {
      return this.reisouInfo;
    }
  }

  public bool IsEarthMode
  {
    set
    {
      this.isEarthMode = value;
    }
    get
    {
      return this.isEarthMode;
    }
  }

  public override Persist<Persist.ItemSortAndFilterInfo> GetPersist()
  {
    return Persist.unit0044SortAndFilter;
  }

  protected override List<PlayerItem> GetItemList()
  {
    List<PlayerItem> playerItemList = new List<PlayerItem>();
    this.equipedReisouIdList = new List<int>();
    foreach (PlayerItem playerItem in SMManager.Get<PlayerItem[]>())
    {
      if (playerItem.isWeapon())
      {
        if (playerItem.equipped_reisou_player_gear_id != 0)
          this.equipedReisouIdList.Add(playerItem.equipped_reisou_player_gear_id);
      }
      else if (playerItem.isReisou() && playerItem.gear.kind == this.gearInfo.gear.kind)
        playerItemList.Add(playerItem);
    }
    if ((UnityEngine.Object) this.dirNoItem != (UnityEngine.Object) null)
      this.dirNoItem.SetActive(playerItemList.Count <= 0);
    return playerItemList;
  }

  protected override IEnumerator InitExtension()
  {
    // ISSUE: reference to a compiler-generated field
    int num = this.\u003C\u003E1__state;
    Unit0044ReisouMenu unit0044ReisouMenu = this;
    if (num != 0)
      return false;
    // ISSUE: reference to a compiler-generated field
    this.\u003C\u003E1__state = -1;
    if (unit0044ReisouMenu.reisouInfo != null)
    {
      InventoryItem inventoryItem = new InventoryItem();
      unit0044ReisouMenu.InventoryItems.Insert(0, inventoryItem);
    }
    unit0044ReisouMenu.BtnSort.gameObject.SetActive(false);
    return false;
  }

  protected new void OpenReisouDetailPopup(GameCore.ItemInfo item)
  {
    this.StartCoroutine(this.OpenReisouDetailPopupAsync(item));
  }

  protected new IEnumerator OpenReisouDetailPopupAsync(GameCore.ItemInfo item)
  {
    if (item != null)
    {
      IEnumerator e;
      if ((UnityEngine.Object) this.reisouPopupPrefab == (UnityEngine.Object) null)
      {
        Future<GameObject> popupPrefabF = new ResourceObject("Prefabs/UnitGUIs/PopupReisouSkillDetails").Load<GameObject>();
        e = popupPrefabF.Wait();
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        this.reisouPopupPrefab = popupPrefabF.Result;
        popupPrefabF = (Future<GameObject>) null;
      }
      GameObject popup = this.reisouPopupPrefab.Clone((Transform) null);
      PopupReisouDetails script = popup.GetComponent<PopupReisouDetails>();
      popup.SetActive(false);
      e = script.Init(item, (PlayerItem) null, true);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      popup.SetActive(true);
      Singleton<PopupManager>.GetInstance().open(popup, false, false, true, true, false, false, "SE_1006");
      yield return (object) null;
      script.scrollResetPosition();
    }
  }

  protected override void BottomInfoUpdate()
  {
    int num = 0;
    foreach (PlayerItem playerItem in SMManager.Get<PlayerItem[]>())
    {
      if (playerItem.isReisou())
        ++num;
    }
    int maxReisouItems = SMManager.Get<Player>().max_reisou_items;
    this.TxtNumberpossession.SetTextLocalize(string.Format("{0}/{1}", (object) num, (object) maxReisouItems));
  }

  protected override void SelectItemProc(GameCore.ItemInfo item)
  {
    if (this.InventoryItems.FindByItem(item).removeButton)
      this.StartCoroutine(this.callEquipReisouAPI((GameCore.ItemInfo) null));
    else
      this.StartCoroutine(this.callEquipReisouAPI(item));
  }

  protected override void CreateItemIconAdvencedSetting(int inventoryItemIdx, int allItemIdx)
  {
    ItemIcon itemIcon = this.AllItemIcon[allItemIdx];
    InventoryItem displayItem = this.DisplayItems[inventoryItemIdx];
    itemIcon.Gray = false;
    itemIcon.QuantitySupply = false;
    if (displayItem.removeButton)
    {
      itemIcon.Favorite = false;
      itemIcon.ForBattle = false;
      itemIcon.onClick = (System.Action<ItemIcon>) (playeritem => this.StartCoroutine(this.callEquipReisouAPI((GameCore.ItemInfo) null)));
      itemIcon.DisableLongPressEvent();
    }
    else
    {
      itemIcon.ForBattle = this.equipedReisouIdList.FirstIndexOrNull<int>((Func<int, bool>) (x => x == itemIcon.ItemInfo.playerItem.id)).HasValue;
      if (itemIcon.ForBattle)
        displayItem.Gray = true;
      itemIcon.onClick = (System.Action<ItemIcon>) (playeritem => this.SelectItemProc(playeritem.ItemInfo));
      if (this.IsGrayIcon(displayItem))
      {
        itemIcon.Gray = true;
        displayItem.Gray = true;
        itemIcon.onClick = (System.Action<ItemIcon>) (_ => {});
      }
      itemIcon.EnableLongPressEvent(new System.Action<GameCore.ItemInfo>(this.OpenReisouDetailPopup));
    }
  }

  protected override bool DisableTouchIcon(InventoryItem item)
  {
    return item.Item == null || item.Item.ForBattle;
  }

  public override void Sort(
    ItemSortAndFilter.SORT_TYPES type,
    SortAndFilter.SORT_TYPE_ORDER_BUY order,
    bool isEquipFirst)
  {
    this.CurrentSortType = type;
    if ((UnityEngine.Object) this.SortSprite != (UnityEngine.Object) null)
      this.SortSprite = ItemSortAndFilter.SortSpriteLabel(type, this.SortSprite);
    this.DisplayItems = this.InventoryItems.SortByReisou().ToList<InventoryItem>();
    this.scroll.Reset();
    this.AllItemIcon.ForEach((System.Action<ItemIcon>) (x =>
    {
      x.transform.parent = this.transform;
      x.gameObject.SetActive(false);
    }));
    for (int index = 0; index < Mathf.Min(this.iconMaxValue, this.DisplayItems.Count); ++index)
    {
      this.scroll.Add(this.AllItemIcon[index].gameObject, this.iconWidth, this.iconHeight, false);
      this.AllItemIcon[index].gameObject.SetActive(true);
    }
    this.InventoryItems.ForEach((System.Action<InventoryItem>) (v => v.icon = (ItemIcon) null));
    this.StartCoroutine(this.CreateItemIconRange(Mathf.Min(this.iconMaxValue, this.DisplayItems.Count)));
    this.scroll.CreateScrollPoint(this.iconHeight, this.DisplayItems.Count);
    this.scroll.ResolvePosition();
  }

  private IEnumerator callEquipReisouAPI(GameCore.ItemInfo item)
  {
    Unit0044ReisouMenu unit0044ReisouMenu = this;
    Singleton<CommonRoot>.GetInstance().loadingMode = 1;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    int num1;
    int num2;
    if (item != null)
      num2 = num1 = item.itemID;
    else
      num1 = num2 = 0;
    int num3 = num2;
    IEnumerator e = WebAPI.ItemGearReisouEquip(unit0044ReisouMenu.gearInfo.itemID, new int?(num3), (System.Action<WebAPI.Response.UserError>) (error =>
    {
      Singleton<CommonRoot>.GetInstance().isLoading = false;
      Singleton<CommonRoot>.GetInstance().loadingMode = 0;
      if (error == null)
        return;
      WebAPI.DefaultUserErrorCallback(error);
    })).Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    switch (GuildUtil.gvgPopupState)
    {
      case GuildUtil.GvGPopupState.None:
label_17:
        Singleton<CommonRoot>.GetInstance().isLoading = false;
        Singleton<CommonRoot>.GetInstance().loadingMode = 0;
        unit0044ReisouMenu.backScene();
        yield break;
      case GuildUtil.GvGPopupState.AtkTeam:
        // ISSUE: reference to a compiler-generated method
        e = GuildUtil.UpdateGuildDeckAttack(PlayerAffiliation.Current.guild_id, Player.Current.id, new System.Action(unit0044ReisouMenu.\u003CcallEquipReisouAPI\u003Eb__29_1));
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        break;
      case GuildUtil.GvGPopupState.DefTeam:
        // ISSUE: reference to a compiler-generated method
        e = GuildUtil.UpdateGuildDeckDefanse(PlayerAffiliation.Current.guild_id, Player.Current.id, new System.Action(unit0044ReisouMenu.\u003CcallEquipReisouAPI\u003Eb__29_2));
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        break;
      default:
        unit0044ReisouMenu.backScene();
        break;
    }
    Singleton<CommonRoot>.GetInstance().isLoading = false;
    Singleton<CommonRoot>.GetInstance().loadingMode = 0;
    goto label_17;
  }
}
