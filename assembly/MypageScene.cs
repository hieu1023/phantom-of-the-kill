﻿// Decompiled with JetBrains decompiler
// Type: MypageScene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using DeviceKit;
using Earth;
using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class MypageScene : NGSceneBase
{
  private string mypageImagePath = string.Empty;
  private BGChange bg_change;
  private bool is_loginbonus;
  public MypageMenu menu;
  [SerializeField]
  private MypageTransition transition;

  public static void ChangeScene(bool stack, bool isCloudAnim = false, bool isEarthSuspend = false)
  {
    Singleton<NGGameDataManager>.GetInstance().IsSea = false;
    Singleton<NGGameDataManager>.GetInstance().IsEarth = false;
    Singleton<NGSceneManager>.GetInstance().changeScene("mypage", (stack ? 1 : 0) != 0, (object) isCloudAnim, (object) isEarthSuspend);
  }

  public override IEnumerator onInitSceneAsync()
  {
    Singleton<CommonRoot>.GetInstance().ShowLoadingLayer(0, false);
    IEnumerator e = this.menu.InitSceneAsync();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    e = ServerTime.WaitSync();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.menu.CharaAnimInit();
  }

  public IEnumerator onStartSceneAsync()
  {
    IEnumerator e = this.onStartSceneAsync(false, false);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public IEnumerator onBackSceneAsync(bool isCloudAnim, bool isEarthSuspend)
  {
    IEnumerator e = this.onStartSceneAsync(false, false);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public void onBackScene(bool isCloudAnim, bool isEarthSuspend)
  {
    this.onStartScene(false, false);
  }

  public IEnumerator onStartSceneAsync(bool isCloudAnim, bool isEarthSuspend)
  {
    MypageScene mypageScene = this;
    if (isEarthSuspend)
    {
      MasterDataCache.GetList<int, UnitUnit>("UnitUnit", new Func<MasterDataReader, UnitUnit>(UnitUnit.Parse), (Func<UnitUnit, int>) (x => x.ID));
      MasterDataCache.GetList<int, UnitVoicePattern>("UnitVoicePattern", new Func<MasterDataReader, UnitVoicePattern>(UnitVoicePattern.Parse), (Func<UnitVoicePattern, int>) (x => x.ID));
      MasterDataCache.GetList<int, TipsTips>("TipsTips", new Func<MasterDataReader, TipsTips>(TipsTips.Parse), (Func<TipsTips, int>) (x => x.ID));
      MasterDataCache.GetList<int, UnitCharacter>("UnitCharacter", new Func<MasterDataReader, UnitCharacter>(UnitCharacter.Parse), (Func<UnitCharacter, int>) (x => x.ID));
      MasterDataCache.GetList<int, QuestStoryS>("QuestStoryS", new Func<MasterDataReader, QuestStoryS>(QuestStoryS.Parse), (Func<QuestStoryS, int>) (x => x.ID));
      MasterDataCache.GetList<int, QuestStoryL>("QuestStoryL", new Func<MasterDataReader, QuestStoryL>(QuestStoryL.Parse), (Func<QuestStoryL, int>) (x => x.ID));
      SMManager.Swap();
      MasterDataCache.SetGameMode(MasterDataCache.GameMode.HEAVEN);
      EarthDataManager.DestoryInstance();
    }
    Singleton<CommonRoot>.GetInstance().ShowLoadingLayer(0, false);
    Singleton<CommonRoot>.GetInstance().SetFooterEnable(true);
    IEnumerator e;
    if (Singleton<TutorialRoot>.GetInstance().IsTutorialFinish())
    {
      e = new Future<NGGameDataManager.StartSceneProxyResult>(new Func<Promise<NGGameDataManager.StartSceneProxyResult>, IEnumerator>(Singleton<NGGameDataManager>.GetInstance().StartSceneAsyncProxyImpl)).Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    mypageScene.is_loginbonus = false;
    if (!isCloudAnim && Singleton<NGGameDataManager>.GetInstance().InfoOrLoginBonusJump())
    {
      mypageScene.is_loginbonus = true;
      MypageMenuBase.LeaderID = 0;
      mypageScene.menu.ResetTweenDelay();
    }
    else
    {
      QuestBG questBG = Singleton<CommonRoot>.GetInstance().getBackgroundComponent<QuestBG>();
      bool flag = (UnityEngine.Object) questBG == (UnityEngine.Object) null;
      if ((UnityEngine.Object) questBG != (UnityEngine.Object) null)
        flag = questBG.DiffStageLId && (uint) questBG.currentPos > 0U;
      if (flag || (UnityEngine.Object) mypageScene.bg_change == (UnityEngine.Object) null || ((UnityEngine.Object) questBG.current_xl == (UnityEngine.Object) null || (UnityEngine.Object) questBG.current_l == (UnityEngine.Object) null))
      {
        e = mypageScene.LoadBackGround();
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
      }
      else if (!questBG.current_xl.GetComponent<UI2DSprite>().sprite2D.name.Equals(mypageScene.mypageImagePath) && (UnityEngine.Object) mypageScene.bg_change != (UnityEngine.Object) null)
      {
        e = mypageScene.bg_change.SetXLBg((QuestBG) null);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        mypageScene.mypageImagePath = questBG.current_xl.GetComponent<UI2DSprite>().sprite2D.name;
      }
      mypageScene.bg_change = mypageScene.GetComponent<BGChange>();
      if (Singleton<NGGameDataManager>.GetInstance().playbackEventIds != null)
        Persist.eventStoryPlay.Data.SetReserveList(Singleton<NGGameDataManager>.GetInstance().playbackEventIds, mypageScene.sceneName);
      e = mypageScene.menu.StartSceneAsync(isCloudAnim, Persist.eventStoryPlay.Data.isExistEventScript(mypageScene.sceneName, 0));
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
  }

  public void onStartScene()
  {
    this.onStartScene(false, false);
  }

  public void onStartScene(bool isCloudAnm, bool isEarthSuspend)
  {
    Singleton<CommonRoot>.GetInstance().loadingMode = 0;
    Singleton<CommonRoot>.GetInstance().GetHeavenCommonFooter().IsPush = false;
    App.SetAutoSleep(true);
    Singleton<CommonRoot>.GetInstance().ActiveBaseHomeMenu(true);
    if (this.is_loginbonus)
      return;
    if (Singleton<TutorialRoot>.GetInstance().IsTutorialFinish() && !isCloudAnm && Persist.eventStoryPlay.Data.PlayEventScript(this.sceneName, 0))
    {
      MypageMenuBase.LeaderID = 0;
      this.menu.ResetTweenDelay();
    }
    else if (!this.menu.LoginPopupActive())
    {
      this.StartCoroutine(this.doDelayHideLoadingLayer());
      this.menu.CharaAnimProc();
      if (isCloudAnm)
      {
        this.menu.SetJogDialActive(false);
        this.menu.PlayTween(MypageMenuBase.START_TWEENGROUP_TOP);
        Singleton<CommonRoot>.GetInstance().StartCloudAnimEnd((System.Action) (() =>
        {
          this.menu.SetJogDialActive(true);
          this.menu.PlayTween(MypageMenuBase.START_TWEEN_GROUP_JOGDIAL);
        }));
      }
      else
        this.menu.PlayTween(MypageMenuBase.START_TWEENGROUP);
    }
    else
    {
      MypageIntroductionControl introductionControl = new MypageIntroductionControl();
      if (!isCloudAnm)
      {
        introductionControl.addExecute((Func<int, bool>) (executed =>
        {
          List<PlayerLoginBonus> loginBonuses = Singleton<NGGameDataManager>.GetInstance().loginBonuses;
          return loginBonuses != null && loginBonuses.Count<PlayerLoginBonus>((Func<PlayerLoginBonus, bool>) (x => x.loginbonus.draw_type == LoginbonusDrawType.popup)) > 0;
        }), new Func<int, IEnumerator>(this.doLoginPopupBonus));
        introductionControl.addExecute((Func<int, bool>) (executed =>
        {
          List<LevelRewardSchemaMixin> playerLevelRewards = Singleton<NGGameDataManager>.GetInstance().playerLevelRewards;
          return playerLevelRewards != null && playerLevelRewards.Count > 0;
        }), new Func<int, IEnumerator>(this.doLevelUpPopup));
        introductionControl.addExecute((Func<int, bool>) (executed => this.IsExploreRankingResult()), new Func<int, IEnumerator>(this.doExploreRankingPopup));
        introductionControl.addExecute((Func<int, bool>) (executed => !Singleton<ExploreDataManager>.GetInstance().IsNotRegisteredDeck() && !Singleton<ExploreDataManager>.GetInstance().LoginCalcDirty), new Func<int, IEnumerator>(this.doExploreBgCalc));
        introductionControl.addExecute((Func<int, bool>) (executed => Singleton<ExploreDataManager>.GetInstance().LoginCalcDirty), new Func<int, IEnumerator>(this.doShowExploreResult));
      }
      introductionControl.setEnd((System.Action<int>) (executed => this.finishedIntroScene(isCloudAnm, executed)));
      this.StartCoroutine(introductionControl.doExecute());
    }
  }

  private IEnumerator doDelayHideLoadingLayer()
  {
    yield return (object) new WaitForSeconds(0.1f);
    Singleton<CommonRoot>.GetInstance().HideLoadingLayer();
  }

  private bool IsExploreRankingResult()
  {
    return Singleton<ExploreDataManager>.GetInstance().HasRankingResult();
  }

  private IEnumerator doLoginPopupBonus(int executed)
  {
    List<PlayerLoginBonus> list = Singleton<NGGameDataManager>.GetInstance().loginBonuses.Where<PlayerLoginBonus>((Func<PlayerLoginBonus, bool>) (x => x.loginbonus.draw_type == LoginbonusDrawType.popup)).ToList<PlayerLoginBonus>();
    EventTracker.TrackEvent("LOGIN", "GAME_LOGIN", 1);
    yield return (object) this.menu.DoLoginPopup(list);
  }

  private IEnumerator doLevelUpPopup(int executed)
  {
    yield return (object) this.menu.DoLevelUpPopup();
  }

  private IEnumerator doShowExploreResult(int executed)
  {
    MypageScene mypageScene = this;
    Singleton<CommonRoot>.GetInstance().ShowLoadingLayer(0, false);
    Future<GameObject> loader = new ResourceObject("Prefabs/explore033_Top/explore_Result").Load<GameObject>();
    IEnumerator e = loader.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    GameObject result = loader.Result;
    ExploreDataManager explore = Singleton<ExploreDataManager>.GetInstance();
    mypageScene.StartCoroutine(mypageScene.doDelayHideLoadingLayer());
    explore.LoginCalcDirty = false;
    ExploreResultReport report = Singleton<PopupManager>.GetInstance().open(result, false, false, false, true, false, false, "SE_1006").GetComponent<ExploreResultReport>();
    e = report.Initialize();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    e = report.ShowExpAnimation();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    yield return (object) new WaitUntil((Func<bool>) (() => report.IsFinish));
    Singleton<PopupManager>.GetInstance().dismiss(false);
    explore.ClearCache();
  }

  private IEnumerator doExploreRankingPopup(int executed)
  {
    yield return (object) this.menu.DoExploreRankingPopup();
  }

  private IEnumerator doExploreBgCalc(int executed)
  {
    MypageScene mypageScene = this;
    ExploreDataManager explore = Singleton<ExploreDataManager>.GetInstance();
    if ((ServerTime.NowAppTime() - explore.LastSyncTime).TotalMilliseconds < (double) explore.TimeConfig["MYPAGE_UPDATE_SPAN"])
    {
      yield return (object) explore.LoadSuspendData(false, false);
    }
    else
    {
      Singleton<CommonRoot>.GetInstance().ShowLoadingLayer(0, false);
      yield return (object) explore.LoadSuspendData(true, false);
      if (!explore.NeedShowBadge)
        yield return (object) explore.ResumeExplore();
      explore.ClearCache();
    }
    mypageScene.StartCoroutine(mypageScene.doDelayHideLoadingLayer());
  }

  private void finishedIntroScene(bool isCloudAnm, int executed)
  {
    if (executed == 0)
      this.StartCoroutine(this.doDelayHideLoadingLayer());
    this.menu.CharaAnimProc();
    this.menu.ExploreButtonSetting();
    if (isCloudAnm)
    {
      this.menu.SetJogDialActive(false);
      this.menu.PlayTween(MypageMenuBase.START_TWEENGROUP_TOP);
      Singleton<CommonRoot>.GetInstance().StartCloudAnimEnd((System.Action) (() =>
      {
        this.menu.SetJogDialActive(true);
        this.menu.PlayTween(MypageMenuBase.START_TWEEN_GROUP_JOGDIAL);
      }));
    }
    else
    {
      this.menu.PlayTween(MypageMenuBase.START_TWEENGROUP);
      this.menu.TutorialAdvice();
    }
  }

  public override void onEndScene()
  {
    if ((UnityEngine.Object) this.menu.sm != (UnityEngine.Object) null && this.menu.sm.enabled)
      this.menu.sm.stopVoice(-1);
    if ((UnityEngine.Object) this.menu.CirculButton != (UnityEngine.Object) null)
      this.menu.CirculButton.End();
    this.menu.StopMissionClearInfo();
    this.menu.StopBannerScroll();
    this.menu.HidePanelMission();
    Singleton<CommonRoot>.GetInstance().ActiveBaseHomeMenu(false);
  }

  public void onClearSavedata()
  {
    Persist.auth.Delete();
    Persist.battleEnvironment.Delete();
    Persist.pvpSuspend.Delete();
    Persist.notification.Delete();
    Persist.sortOrder.Delete();
    Persist.tutorial.Delete();
    Persist.volume.Delete();
    Persist.earthData.Delete();
    Persist.missionHistory.Delete();
    Application.Quit();
  }

  private IEnumerator LoadBackGround()
  {
    MypageScene mypageScene = this;
    int lId = Persist.lastsortie.Data.l_id;
    int L;
    if (lId == 0)
    {
      L = 1;
      mypageScene.transition.L_id = L;
    }
    else
    {
      mypageScene.transition.L_id = 1;
      foreach (PlayerStoryQuestS playerStoryQuestS in SMManager.Get<PlayerStoryQuestS[]>())
      {
        if (playerStoryQuestS.quest_story_s.quest_l_QuestStoryL == lId)
        {
          if (playerStoryQuestS.quest_story_s.quest_l.origin_id.HasValue)
          {
            mypageScene.transition.hardmode = true;
            lId = playerStoryQuestS.quest_story_s.quest_l.origin_id.Value;
          }
          mypageScene.transition.L_id = lId;
          break;
        }
      }
      L = mypageScene.transition.L_id;
    }
    IEnumerator e = mypageScene.GetComponent<BGChange>().MypageBGprefabCreate(true, L, 0, 1);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    QuestBG backgroundComponent = Singleton<CommonRoot>.GetInstance().getBackgroundComponent<QuestBG>();
    if ((UnityEngine.Object) backgroundComponent != (UnityEngine.Object) null && (UnityEngine.Object) backgroundComponent.current_xl != (UnityEngine.Object) null)
      mypageScene.mypageImagePath = backgroundComponent.current_xl.GetComponent<UI2DSprite>().sprite2D.name;
  }

  public bool isAnimePlaying
  {
    get
    {
      return (UnityEngine.Object) this.menu != (UnityEngine.Object) null && this.menu.isAnimePlaying;
    }
  }

  public void onStartEarthCloud()
  {
    Singleton<CommonRoot>.GetInstance().isTouchBlock = true;
    NGSoundManager instance = Singleton<NGSoundManager>.GetInstance();
    if ((UnityEngine.Object) instance != (UnityEngine.Object) null)
      instance.playSE("SE_1039", false, 0.0f, -1);
    this.StartCoroutine(this.menu.CloudAnimationStart());
  }
}
