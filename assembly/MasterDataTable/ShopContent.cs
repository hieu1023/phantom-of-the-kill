﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.ShopContent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class ShopContent
  {
    public int ID;
    public int article_ShopArticle;
    public int entity_type_CommonRewardType;
    public int entity_id;
    public int quantity;
    public bool upper_limit_check;
    public int upper_limit_count;

    public static ShopContent Parse(MasterDataReader reader)
    {
      return new ShopContent()
      {
        ID = reader.ReadInt(),
        article_ShopArticle = reader.ReadInt(),
        entity_type_CommonRewardType = reader.ReadInt(),
        entity_id = reader.ReadInt(),
        quantity = reader.ReadInt(),
        upper_limit_check = reader.ReadBool(),
        upper_limit_count = reader.ReadInt()
      };
    }

    public ShopArticle article
    {
      get
      {
        ShopArticle shopArticle;
        if (!MasterData.ShopArticle.TryGetValue(this.article_ShopArticle, out shopArticle))
          Debug.LogError((object) ("Key not Found: MasterData.ShopArticle[" + (object) this.article_ShopArticle + "]"));
        return shopArticle;
      }
    }

    public CommonRewardType entity_type
    {
      get
      {
        return (CommonRewardType) this.entity_type_CommonRewardType;
      }
    }
  }
}
