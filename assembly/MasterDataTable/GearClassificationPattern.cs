﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.GearClassificationPattern
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class GearClassificationPattern
  {
    public int ID;
    public int kind_GearKind;
    public string name;
    public int attack_classification_GearAttackClassification;

    public static GearClassificationPattern Parse(MasterDataReader reader)
    {
      return new GearClassificationPattern()
      {
        ID = reader.ReadInt(),
        kind_GearKind = reader.ReadInt(),
        name = reader.ReadString(true),
        attack_classification_GearAttackClassification = reader.ReadInt()
      };
    }

    public GearKind kind
    {
      get
      {
        GearKind gearKind;
        if (!MasterData.GearKind.TryGetValue(this.kind_GearKind, out gearKind))
          Debug.LogError((object) ("Key not Found: MasterData.GearKind[" + (object) this.kind_GearKind + "]"));
        return gearKind;
      }
    }

    public GearAttackClassification attack_classification
    {
      get
      {
        return (GearAttackClassification) this.attack_classification_GearAttackClassification;
      }
    }
  }
}
