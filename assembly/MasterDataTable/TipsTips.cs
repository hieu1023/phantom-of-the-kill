﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.TipsTips
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class TipsTips
  {
    public int ID;
    public string description;
    public int weight;

    public static TipsTips Parse(MasterDataReader reader)
    {
      return new TipsTips()
      {
        ID = reader.ReadInt(),
        description = reader.ReadString(true),
        weight = reader.ReadInt()
      };
    }
  }
}
