﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.RaidPlaybackStory
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class RaidPlaybackStory
  {
    public int ID;
    public string name;
    public int period_id;
    public int priority;
    public DateTime? display_expire_at;

    public static RaidPlaybackStory Parse(MasterDataReader reader)
    {
      return new RaidPlaybackStory()
      {
        ID = reader.ReadInt(),
        name = reader.ReadString(true),
        period_id = reader.ReadInt(),
        priority = reader.ReadInt(),
        display_expire_at = reader.ReadDateTimeOrNull()
      };
    }
  }
}
