﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.SeasonTicketSeasonTicket
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System;

namespace MasterDataTable
{
  [Serializable]
  public class SeasonTicketSeasonTicket
  {
    public int ID;
    public string name;
    public int max_quantity;
    public string description;
    public int increase_match;

    public static SeasonTicketSeasonTicket Parse(MasterDataReader reader)
    {
      return new SeasonTicketSeasonTicket()
      {
        ID = reader.ReadInt(),
        name = reader.ReadString(true),
        max_quantity = reader.ReadInt(),
        description = reader.ReadString(true),
        increase_match = reader.ReadInt()
      };
    }

    public Future<UnityEngine.Sprite> LoadThumneilF()
    {
      return Singleton<ResourceManager>.GetInstance().Load<UnityEngine.Sprite>("SeasonTicket/thum", 1f);
    }

    public Future<UnityEngine.Sprite> LoadLargeF()
    {
      return Singleton<ResourceManager>.GetInstance().Load<UnityEngine.Sprite>("SeasonTicket/large", 1f);
    }
  }
}
