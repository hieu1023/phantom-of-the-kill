﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.GearSpecialDrillingCost
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class GearSpecialDrillingCost
  {
    public int ID;
    public int? rarity_GearRarity;
    public int level;
    public int price;

    public static GearSpecialDrillingCost Parse(MasterDataReader reader)
    {
      return new GearSpecialDrillingCost()
      {
        ID = reader.ReadInt(),
        rarity_GearRarity = reader.ReadIntOrNull(),
        level = reader.ReadInt(),
        price = reader.ReadInt()
      };
    }

    public GearRarity rarity
    {
      get
      {
        if (!this.rarity_GearRarity.HasValue)
          return (GearRarity) null;
        GearRarity gearRarity;
        if (!MasterData.GearRarity.TryGetValue(this.rarity_GearRarity.Value, out gearRarity))
          Debug.LogError((object) ("Key not Found: MasterData.GearRarity[" + (object) this.rarity_GearRarity.Value + "]"));
        return gearRarity;
      }
    }
  }
}
