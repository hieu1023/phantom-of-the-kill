﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.QuestCommonDrop
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class QuestCommonDrop
  {
    public int ID;
    public int quest_type_CommonQuestType;
    public int quest_s_id;
    public int priority;
    public int entity_type_CommonRewardType;
    public int entity_id;
    public int quantity;

    public static QuestCommonDrop Parse(MasterDataReader reader)
    {
      return new QuestCommonDrop()
      {
        ID = reader.ReadInt(),
        quest_type_CommonQuestType = reader.ReadInt(),
        quest_s_id = reader.ReadInt(),
        priority = reader.ReadInt(),
        entity_type_CommonRewardType = reader.ReadInt(),
        entity_id = reader.ReadInt(),
        quantity = reader.ReadInt()
      };
    }

    public CommonQuestType quest_type
    {
      get
      {
        return (CommonQuestType) this.quest_type_CommonQuestType;
      }
    }

    public CommonRewardType entity_type
    {
      get
      {
        return (CommonRewardType) this.entity_type_CommonRewardType;
      }
    }
  }
}
