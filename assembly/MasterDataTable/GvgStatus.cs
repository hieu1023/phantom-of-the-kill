﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.GvgStatus
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

namespace MasterDataTable
{
  public enum GvgStatus
  {
    not_enough_member = 1,
    lock_entry = 2,
    can_entry = 3,
    matching = 4,
    preparing = 5,
    fighting = 6,
    aggregating = 7,
    finished = 8,
    out_of_term = 9,
  }
}
