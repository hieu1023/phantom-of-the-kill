﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.GuildBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class GuildBase
  {
    public int ID;
    public int base_type_GuildBaseType;
    public int release_level;

    public static GuildBase Parse(MasterDataReader reader)
    {
      return new GuildBase()
      {
        ID = reader.ReadInt(),
        base_type_GuildBaseType = reader.ReadInt(),
        release_level = reader.ReadInt()
      };
    }

    public GuildBaseType base_type
    {
      get
      {
        return (GuildBaseType) this.base_type_GuildBaseType;
      }
    }
  }
}
