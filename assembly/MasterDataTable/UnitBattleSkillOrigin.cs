﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.UnitBattleSkillOrigin
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

namespace MasterDataTable
{
  public class UnitBattleSkillOrigin
  {
    public object origin_ { get; private set; }

    public BattleskillSkill skill_ { get; private set; }

    public UnitBattleSkillOrigin(object origin, BattleskillSkill skill)
    {
      this.origin_ = origin;
      this.skill_ = skill;
    }

    public bool IsOriginBasic
    {
      get
      {
        return this.origin_.GetType() == typeof (UnitSkill);
      }
    }

    public UnitSkill Basic
    {
      get
      {
        return this.origin_ as UnitSkill;
      }
    }

    public bool IsOriginLeaderBasic
    {
      get
      {
        return this.origin_.GetType() == typeof (UnitLeaderSkill);
      }
    }

    public UnitLeaderSkill LeaderBasic
    {
      get
      {
        return this.origin_ as UnitLeaderSkill;
      }
    }

    public bool IsOriginCharacterQuest
    {
      get
      {
        return this.origin_.GetType() == typeof (UnitSkillCharacterQuest);
      }
    }

    public UnitSkillCharacterQuest CharacterQuest
    {
      get
      {
        return this.origin_ as UnitSkillCharacterQuest;
      }
    }

    public bool IsOriginHarmonyQuest
    {
      get
      {
        return this.origin_.GetType() == typeof (UnitSkillHarmonyQuest);
      }
    }

    public UnitSkillHarmonyQuest HarmonyQuest
    {
      get
      {
        return this.origin_ as UnitSkillHarmonyQuest;
      }
    }

    public bool IsOriginEvolution
    {
      get
      {
        return this.origin_.GetType() == typeof (UnitSkillEvolution);
      }
    }

    public UnitSkillEvolution Evolution
    {
      get
      {
        return this.origin_ as UnitSkillEvolution;
      }
    }

    public bool IsOriginAwake
    {
      get
      {
        return this.origin_.GetType() == typeof (UnitSkillAwake);
      }
    }

    public UnitSkillAwake Awake
    {
      get
      {
        return this.origin_ as UnitSkillAwake;
      }
    }
  }
}
