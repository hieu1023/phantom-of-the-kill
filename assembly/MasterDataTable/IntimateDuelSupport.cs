﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.IntimateDuelSupport
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class IntimateDuelSupport
  {
    public int ID;
    public int intimate_value;
    public int hit;
    public int evasion;
    public int critical;
    public int critical_evasion;

    public static IntimateDuelSupport Parse(MasterDataReader reader)
    {
      return new IntimateDuelSupport()
      {
        ID = reader.ReadInt(),
        intimate_value = reader.ReadInt(),
        hit = reader.ReadInt(),
        evasion = reader.ReadInt(),
        critical = reader.ReadInt(),
        critical_evasion = reader.ReadInt()
      };
    }
  }
}
