﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.QuestMoviePath
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class QuestMoviePath
  {
    public int ID;
    public string ios_path;
    public string android_path;
    public string title;
    public string windows_path;

    public static QuestMoviePath Parse(MasterDataReader reader)
    {
      return new QuestMoviePath()
      {
        ID = reader.ReadInt(),
        ios_path = reader.ReadString(true),
        android_path = reader.ReadString(true),
        title = reader.ReadString(true),
        windows_path = reader.ReadString(true)
      };
    }
  }
}
