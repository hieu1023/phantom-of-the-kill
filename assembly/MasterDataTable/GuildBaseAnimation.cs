﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.GuildBaseAnimation
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class GuildBaseAnimation
  {
    public int ID;
    public int anim_group_ID;
    public string animPrefixSprite;
    public int animframe;
    public float animXpos;
    public float animYpos;

    public static GuildBaseAnimation Parse(MasterDataReader reader)
    {
      return new GuildBaseAnimation()
      {
        ID = reader.ReadInt(),
        anim_group_ID = reader.ReadInt(),
        animPrefixSprite = reader.ReadString(true),
        animframe = reader.ReadInt(),
        animXpos = reader.ReadFloat(),
        animYpos = reader.ReadFloat()
      };
    }
  }
}
