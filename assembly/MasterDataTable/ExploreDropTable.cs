﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.ExploreDropTable
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class ExploreDropTable
  {
    public int ID;
    public int deck_id;
    public int drop_ratio;
    public int drop_reward_id;

    public static ExploreDropTable Parse(MasterDataReader reader)
    {
      return new ExploreDropTable()
      {
        ID = reader.ReadInt(),
        deck_id = reader.ReadInt(),
        drop_ratio = reader.ReadInt(),
        drop_reward_id = reader.ReadInt()
      };
    }
  }
}
