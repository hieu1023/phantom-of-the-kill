﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.GachaType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

namespace MasterDataTable
{
  public enum GachaType
  {
    normal = 1,
    friend = 2,
    pickup = 3,
    ticket = 4,
    stepup = 5,
    sheet = 6,
    tutorial = 7,
    retry = 8,
  }
}
