﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.UnitSkillupSkillGroupSetting
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class UnitSkillupSkillGroupSetting
  {
    public int ID;
    public int group_id;
    public int skill_id;

    public static UnitSkillupSkillGroupSetting Parse(
      MasterDataReader reader)
    {
      return new UnitSkillupSkillGroupSetting()
      {
        ID = reader.ReadInt(),
        group_id = reader.ReadInt(),
        skill_id = reader.ReadInt()
      };
    }
  }
}
