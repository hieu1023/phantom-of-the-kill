﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.SeaHomeResult
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class SeaHomeResult
  {
    public int ID;
    public string name;
    public bool effect_on;
    public string effect_trigger;
    public bool gauge_effect_on;
    public string gauge_effect_trigger;

    public static SeaHomeResult Parse(MasterDataReader reader)
    {
      return new SeaHomeResult()
      {
        ID = reader.ReadInt(),
        name = reader.ReadString(true),
        effect_on = reader.ReadBool(),
        effect_trigger = reader.ReadString(true),
        gauge_effect_on = reader.ReadBool(),
        gauge_effect_trigger = reader.ReadString(true)
      };
    }
  }
}
