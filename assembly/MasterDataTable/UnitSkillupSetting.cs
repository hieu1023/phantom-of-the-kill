﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.UnitSkillupSetting
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class UnitSkillupSetting
  {
    public int ID;
    public int material_unit_id;
    public float levelup_ratio;
    public int? skill_group;

    public static UnitSkillupSetting Parse(MasterDataReader reader)
    {
      return new UnitSkillupSetting()
      {
        ID = reader.ReadInt(),
        material_unit_id = reader.ReadInt(),
        levelup_ratio = reader.ReadFloat(),
        skill_group = reader.ReadIntOrNull()
      };
    }
  }
}
