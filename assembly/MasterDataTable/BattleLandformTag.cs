﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.BattleLandformTag
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class BattleLandformTag
  {
    public int ID;
    public string type;

    public static BattleLandformTag Parse(MasterDataReader reader)
    {
      return new BattleLandformTag()
      {
        ID = reader.ReadInt(),
        type = reader.ReadString(true)
      };
    }
  }
}
