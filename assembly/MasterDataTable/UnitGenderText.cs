﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.UnitGenderText
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class UnitGenderText
  {
    public int ID;
    public string name;

    public static UnitGenderText Parse(MasterDataReader reader)
    {
      return new UnitGenderText()
      {
        ID = reader.ReadInt(),
        name = reader.ReadString(true)
      };
    }

    public static string GetText(UnitGender gender)
    {
      UnitGenderText unitGenderText = (UnitGenderText) null;
      return MasterData.UnitGenderText.TryGetValue((int) gender, out unitGenderText) ? unitGenderText.name : string.Empty;
    }
  }
}
