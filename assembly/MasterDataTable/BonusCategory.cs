﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.BonusCategory
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

namespace MasterDataTable
{
  public enum BonusCategory
  {
    unit_exp_2 = 1,
    player_exp_2 = 2,
    money_2 = 3,
    item_2 = 4,
    unit_exp_1_5 = 5,
    player_exp_1_5 = 6,
    money_1_5 = 7,
    item_1_5 = 8,
    unit_exp_3 = 9,
    player_exp_3 = 10, // 0x0000000A
    money_3 = 11, // 0x0000000B
    item_3 = 12, // 0x0000000C
  }
}
