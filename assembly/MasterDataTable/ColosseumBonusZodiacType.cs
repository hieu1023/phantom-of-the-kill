﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.ColosseumBonusZodiacType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class ColosseumBonusZodiacType
  {
    public int ID;
    public string name;
    public int value;

    public static ColosseumBonusZodiacType Parse(MasterDataReader reader)
    {
      return new ColosseumBonusZodiacType()
      {
        ID = reader.ReadInt(),
        name = reader.ReadString(true),
        value = reader.ReadInt()
      };
    }
  }
}
