﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.InvitationInvitation
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class InvitationInvitation
  {
    public int ID;
    public string title;
    public string link;
    public string discription;

    public static InvitationInvitation Parse(MasterDataReader reader)
    {
      return new InvitationInvitation()
      {
        ID = reader.ReadInt(),
        title = reader.ReadString(true),
        link = reader.ReadString(true),
        discription = reader.ReadString(true)
      };
    }
  }
}
