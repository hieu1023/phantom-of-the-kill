﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.SeaDateDateFlow
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

namespace MasterDataTable
{
  public enum SeaDateDateFlow
  {
    first_lottery = 1,
    hit_expansion_lottery = 2,
    gen_expansion_lottery = 3,
    personal_quiz_lottery = 4,
    happening_choices = 6,
    quiz_choices = 7,
    date_result = 8,
  }
}
