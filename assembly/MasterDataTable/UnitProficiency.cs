﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.UnitProficiency
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class UnitProficiency
  {
    public int ID;
    public string proficiency;
    public int gear_rarity;

    public static UnitProficiency Parse(MasterDataReader reader)
    {
      return new UnitProficiency()
      {
        ID = reader.ReadInt(),
        proficiency = reader.ReadString(true),
        gear_rarity = reader.ReadInt()
      };
    }
  }
}
