﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.GuildBaseBonusType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

namespace MasterDataTable
{
  public enum GuildBaseBonusType
  {
    physical_attack = 1,
    physical_defense = 2,
    magic_attack = 3,
    magic_defense = 4,
    agility = 5,
    dexterity = 6,
    hit_point = 7,
    accuracy_rate = 8,
    avoidance = 9,
  }
}
