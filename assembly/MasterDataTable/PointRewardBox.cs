﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.PointRewardBox
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class PointRewardBox
  {
    public int ID;
    public int type;
    public int point;
    public int box_type;
    public string point_reward_ids;
    public DateTime? start_at;
    public DateTime? end_at;

    public static PointRewardBox Parse(MasterDataReader reader)
    {
      return new PointRewardBox()
      {
        ID = reader.ReadInt(),
        type = reader.ReadInt(),
        point = reader.ReadInt(),
        box_type = reader.ReadInt(),
        point_reward_ids = reader.ReadString(true),
        start_at = reader.ReadDateTimeOrNull(),
        end_at = reader.ReadDateTimeOrNull()
      };
    }
  }
}
