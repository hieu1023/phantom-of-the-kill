﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.BoostCampaignTypeName
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class BoostCampaignTypeName
  {
    public int ID;
    public string img_name;
    public int position_BoostIconPosition;

    public static BoostCampaignTypeName Parse(MasterDataReader reader)
    {
      return new BoostCampaignTypeName()
      {
        ID = reader.ReadInt(),
        img_name = reader.ReadString(true),
        position_BoostIconPosition = reader.ReadInt()
      };
    }

    public BoostIconPosition position
    {
      get
      {
        return (BoostIconPosition) this.position_BoostIconPosition;
      }
    }
  }
}
