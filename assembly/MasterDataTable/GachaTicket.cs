﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.GachaTicket
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System;

namespace MasterDataTable
{
  [Serializable]
  public class GachaTicket
  {
    private static readonly string GACHATICKET_BASE_PATH = "GachaTicket/{0}/ticket";
    private static readonly string DEFAULT_GACHATICKET_PATH = "GachaTicket/default/ticket";
    public int ID;
    public string name;
    public string short_name;
    public int priority;

    public static GachaTicket Parse(MasterDataReader reader)
    {
      return new GachaTicket()
      {
        ID = reader.ReadInt(),
        name = reader.ReadString(true),
        short_name = reader.ReadString(true),
        priority = reader.ReadInt()
      };
    }

    public Future<UnityEngine.Sprite> LoadSpriteF()
    {
      return Singleton<ResourceManager>.GetInstance().Load<UnityEngine.Sprite>(string.Format(GachaTicket.GACHATICKET_BASE_PATH, (object) this.ID), 1f);
    }

    public Future<UnityEngine.Sprite> LoadSpriteOrDefault()
    {
      return Singleton<ResourceManager>.GetInstance().Contains(string.Format(GachaTicket.GACHATICKET_BASE_PATH, (object) this.ID)) ? Singleton<ResourceManager>.GetInstance().Load<UnityEngine.Sprite>(string.Format(GachaTicket.GACHATICKET_BASE_PATH, (object) this.ID), 1f) : Singleton<ResourceManager>.GetInstance().Load<UnityEngine.Sprite>(GachaTicket.DEFAULT_GACHATICKET_PATH, 1f);
    }
  }
}
