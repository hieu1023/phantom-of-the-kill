﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.SlotS001MedalReelIcon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class SlotS001MedalReelIcon
  {
    public int ID;
    public string name;
    public string description;
    public int medal_flg;
    public string file_name;

    public static SlotS001MedalReelIcon Parse(MasterDataReader reader)
    {
      return new SlotS001MedalReelIcon()
      {
        ID = reader.ReadInt(),
        name = reader.ReadString(true),
        description = reader.ReadString(true),
        medal_flg = reader.ReadInt(),
        file_name = reader.ReadString(true)
      };
    }
  }
}
