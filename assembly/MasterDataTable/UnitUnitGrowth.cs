﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.UnitUnitGrowth
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class UnitUnitGrowth
  {
    public int ID;
    public float hp_growth;
    public float strength_growth;
    public float intelligence_growth;
    public float vitality_growth;
    public float mind_growth;
    public float agility_growth;
    public float dexterity_growth;
    public float lucky_growth;

    public static UnitUnitGrowth Parse(MasterDataReader reader)
    {
      return new UnitUnitGrowth()
      {
        ID = reader.ReadInt(),
        hp_growth = reader.ReadFloat(),
        strength_growth = reader.ReadFloat(),
        intelligence_growth = reader.ReadFloat(),
        vitality_growth = reader.ReadFloat(),
        mind_growth = reader.ReadFloat(),
        agility_growth = reader.ReadFloat(),
        dexterity_growth = reader.ReadFloat(),
        lucky_growth = reader.ReadFloat()
      };
    }
  }
}
