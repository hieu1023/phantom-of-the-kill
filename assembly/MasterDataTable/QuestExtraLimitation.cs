﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.QuestExtraLimitation
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class QuestExtraLimitation
  {
    public int ID;
    public int quest_s_id_QuestExtraS;

    public static QuestExtraLimitation Parse(MasterDataReader reader)
    {
      return new QuestExtraLimitation()
      {
        ID = reader.ReadInt(),
        quest_s_id_QuestExtraS = reader.ReadInt()
      };
    }

    public QuestExtraS quest_s_id
    {
      get
      {
        QuestExtraS questExtraS;
        if (!MasterData.QuestExtraS.TryGetValue(this.quest_s_id_QuestExtraS, out questExtraS))
          Debug.LogError((object) ("Key not Found: MasterData.QuestExtraS[" + (object) this.quest_s_id_QuestExtraS + "]"));
        return questExtraS;
      }
    }
  }
}
