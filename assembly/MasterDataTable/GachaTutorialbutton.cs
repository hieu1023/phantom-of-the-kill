﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.GachaTutorialbutton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class GachaTutorialbutton
  {
    public int ID;
    public int gacha_id;
    public int? count;
    public int? period_id;
    public string url;

    public static GachaTutorialbutton Parse(MasterDataReader reader)
    {
      return new GachaTutorialbutton()
      {
        ID = reader.ReadInt(),
        gacha_id = reader.ReadInt(),
        count = reader.ReadIntOrNull(),
        period_id = reader.ReadIntOrNull(),
        url = reader.ReadString(true)
      };
    }
  }
}
