﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.InitimateLevel
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class InitimateLevel
  {
    public int ID;
    public int level;
    public int from_exp;
    public int to_exp;

    public static InitimateLevel Parse(MasterDataReader reader)
    {
      return new InitimateLevel()
      {
        ID = reader.ReadInt(),
        level = reader.ReadInt(),
        from_exp = reader.ReadInt(),
        to_exp = reader.ReadInt()
      };
    }
  }
}
