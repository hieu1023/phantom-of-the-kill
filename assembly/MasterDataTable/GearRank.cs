﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.GearRank
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class GearRank
  {
    public int ID;
    public int index_GearRarity;
    public int _max_level_GearRankExp;

    public static GearRank Parse(MasterDataReader reader)
    {
      return new GearRank()
      {
        ID = reader.ReadInt(),
        index_GearRarity = reader.ReadInt(),
        _max_level_GearRankExp = reader.ReadInt()
      };
    }

    public GearRarity index
    {
      get
      {
        GearRarity gearRarity;
        if (!MasterData.GearRarity.TryGetValue(this.index_GearRarity, out gearRarity))
          Debug.LogError((object) ("Key not Found: MasterData.GearRarity[" + (object) this.index_GearRarity + "]"));
        return gearRarity;
      }
    }

    public GearRankExp _max_level
    {
      get
      {
        GearRankExp gearRankExp;
        if (!MasterData.GearRankExp.TryGetValue(this._max_level_GearRankExp, out gearRankExp))
          Debug.LogError((object) ("Key not Found: MasterData.GearRankExp[" + (object) this._max_level_GearRankExp + "]"));
        return gearRankExp;
      }
    }
  }
}
