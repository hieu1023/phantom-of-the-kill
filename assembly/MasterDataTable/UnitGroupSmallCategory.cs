﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.UnitGroupSmallCategory
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class UnitGroupSmallCategory
  {
    public int ID;
    public string name;
    public string short_label_name;
    public string description;
    public DateTime? start_at;

    public static UnitGroupSmallCategory Parse(MasterDataReader reader)
    {
      return new UnitGroupSmallCategory()
      {
        ID = reader.ReadInt(),
        name = reader.ReadString(true),
        short_label_name = reader.ReadString(true),
        description = reader.ReadString(true),
        start_at = reader.ReadDateTimeOrNull()
      };
    }

    public string GetSpriteName()
    {
      return string.Format("s_classification_{0}", (object) this.ID);
    }

    public enum Type
    {
      All = 1,
      Collabo = 2,
      Ragnarok = 3,
      Seiyugu = 4,
      Shirogaku = 5,
      Teacher = 6,
      SeaPool = 7,
      SeaBeach = 8,
      SeaJungle = 9,
      SecondAngel = 10, // 0x0000000A
      SecondDevil = 11, // 0x0000000B
      SecondBeast = 12, // 0x0000000C
      SecondFairy = 13, // 0x0000000D
      AlcheCollabo = 14, // 0x0000000E
      ShinobiCollabo = 15, // 0x0000000F
      SecondCommand = 16, // 0x00000010
      ThirdIntegral = 17, // 0x00000011
      ThirdImitate = 18, // 0x00000012
    }
  }
}
