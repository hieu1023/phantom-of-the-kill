﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.BeginnerNaviTitle
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UniLinq;

namespace MasterDataTable
{
  [Serializable]
  public class BeginnerNaviTitle
  {
    public int ID;
    public int category_BeginnerNaviCategory;
    public string title;
    public int priority;

    public BeginnerNaviDetail detail
    {
      get
      {
        return ((IEnumerable<BeginnerNaviDetail>) MasterData.BeginnerNaviDetailList).Single<BeginnerNaviDetail>((Func<BeginnerNaviDetail, bool>) (x => x.title.ID == this.ID));
      }
    }

    public static BeginnerNaviTitle Parse(MasterDataReader reader)
    {
      return new BeginnerNaviTitle()
      {
        ID = reader.ReadInt(),
        category_BeginnerNaviCategory = reader.ReadInt(),
        title = reader.ReadString(true),
        priority = reader.ReadInt()
      };
    }

    public BeginnerNaviCategory category
    {
      get
      {
        BeginnerNaviCategory beginnerNaviCategory;
        if (!MasterData.BeginnerNaviCategory.TryGetValue(this.category_BeginnerNaviCategory, out beginnerNaviCategory))
          Debug.LogError((object) ("Key not Found: MasterData.BeginnerNaviCategory[" + (object) this.category_BeginnerNaviCategory + "]"));
        return beginnerNaviCategory;
      }
    }
  }
}
