﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.ExploreCommonAnimation
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class ExploreCommonAnimation
  {
    public int ID;
    public string target;
    public string changed;
    public bool no_run;

    public static ExploreCommonAnimation Parse(MasterDataReader reader)
    {
      return new ExploreCommonAnimation()
      {
        ID = reader.ReadInt(),
        target = reader.ReadString(true),
        changed = reader.ReadString(true),
        no_run = reader.ReadBool()
      };
    }
  }
}
