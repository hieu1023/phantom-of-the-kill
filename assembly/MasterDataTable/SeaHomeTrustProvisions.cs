﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.SeaHomeTrustProvisions
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class SeaHomeTrustProvisions
  {
    public int ID;
    public string name;
    public int min_trust;
    public int max_trust;

    public static SeaHomeTrustProvisions Parse(MasterDataReader reader)
    {
      return new SeaHomeTrustProvisions()
      {
        ID = reader.ReadInt(),
        name = reader.ReadString(true),
        min_trust = reader.ReadInt(),
        max_trust = reader.ReadInt()
      };
    }

    public bool HasUnit()
    {
      return this.min_trust >= 0 && this.max_trust >= 0;
    }

    public bool WithIn(int trust)
    {
      return this.min_trust <= trust && this.max_trust >= trust;
    }
  }
}
