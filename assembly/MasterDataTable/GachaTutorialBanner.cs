﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.GachaTutorialBanner
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class GachaTutorialBanner
  {
    public int ID;
    public DateTime? start_at;
    public DateTime? end_at;
    public string title_image_url;
    public string front_image_url;

    public static GachaTutorialBanner Parse(MasterDataReader reader)
    {
      return new GachaTutorialBanner()
      {
        ID = reader.ReadInt(),
        start_at = reader.ReadDateTimeOrNull(),
        end_at = reader.ReadDateTimeOrNull(),
        title_image_url = reader.ReadStringOrNull(true),
        front_image_url = reader.ReadStringOrNull(true)
      };
    }
  }
}
