﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.QuestSeaXL
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class QuestSeaXL
  {
    public int ID;
    public string name;
    public int priority;

    public static QuestSeaXL Parse(MasterDataReader reader)
    {
      return new QuestSeaXL()
      {
        ID = reader.ReadInt(),
        name = reader.ReadString(true),
        priority = reader.ReadInt()
      };
    }
  }
}
