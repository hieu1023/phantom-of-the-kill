﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.EarthQuestKey
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System;

namespace MasterDataTable
{
  [Serializable]
  public class EarthQuestKey
  {
    public int ID;
    public string name;
    public int max_stack;
    public int quest_id;

    public static EarthQuestKey Parse(MasterDataReader reader)
    {
      return new EarthQuestKey()
      {
        ID = reader.ReadInt(),
        name = reader.ReadString(true),
        max_stack = reader.ReadInt(),
        quest_id = reader.ReadInt()
      };
    }

    public Future<UnityEngine.Sprite> LoadSpriteThumbnail()
    {
      return Singleton<ResourceManager>.GetInstance().Load<UnityEngine.Sprite>(string.Format("AssetBundle/Resources/EarthQuestkey/{0}/key_thum", (object) this.ID), 1f);
    }

    public Future<UnityEngine.Sprite> LoadSpriteBasic()
    {
      return Singleton<ResourceManager>.GetInstance().Load<UnityEngine.Sprite>(string.Format("AssetBundle/Resources/EarthQuestkey/{0}/key_basic", (object) this.ID), 1f);
    }
  }
}
