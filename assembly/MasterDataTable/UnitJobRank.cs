﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.UnitJobRank
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

namespace MasterDataTable
{
  public enum UnitJobRank
  {
    rank1 = 1,
    rank2 = 2,
    rank3 = 3,
    vertex = 4,
    none = 5,
    vertex1 = 6,
    vertex2 = 7,
    vertex3 = 8,
  }
}
