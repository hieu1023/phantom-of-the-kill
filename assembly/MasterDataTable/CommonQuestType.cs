﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.CommonQuestType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

namespace MasterDataTable
{
  public enum CommonQuestType
  {
    Story = 1,
    Character = 2,
    Extra = 3,
    Harmony = 4,
    Earth = 5,
    PvP = 6,
    EarthExtra = 7,
    Tower = 8,
    Sea = 9,
    GuildRaid = 10, // 0x0000000A
  }
}
