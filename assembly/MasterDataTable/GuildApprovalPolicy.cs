﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.GuildApprovalPolicy
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class GuildApprovalPolicy
  {
    public int ID;
    public string name;
    public bool default_manual;

    public static GuildApprovalPolicy Parse(MasterDataReader reader)
    {
      return new GuildApprovalPolicy()
      {
        ID = reader.ReadInt(),
        name = reader.ReadString(true),
        default_manual = reader.ReadBool()
      };
    }
  }
}
