﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.BattleskillLifeCycle
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class BattleskillLifeCycle
  {
    public int ID;
    public string name;
    public int logic_priority;

    public static BattleskillLifeCycle Parse(MasterDataReader reader)
    {
      return new BattleskillLifeCycle()
      {
        ID = reader.ReadInt(),
        name = reader.ReadString(true),
        logic_priority = reader.ReadInt()
      };
    }
  }
}
