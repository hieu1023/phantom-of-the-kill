﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.UnitModel
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class UnitModel
  {
    public int ID;
    public int unit_id_UnitUnit;
    public int job_metamor_id;
    public int? resource_reference_unit_id;
    public int? model_reference_id;
    public string field_normal_face_material_name;
    public string field_gray_body_material_name;
    public string field_gray_face_material_name;
    public string field_gray_vehicle_material_name;
    public string field_gray_equip_material_name;
    public string field_gray_equip_b_material_name;
    public string job_change_voice;
    public int? initial_gear_GearGear;

    public static UnitModel Parse(MasterDataReader reader)
    {
      return new UnitModel()
      {
        ID = reader.ReadInt(),
        unit_id_UnitUnit = reader.ReadInt(),
        job_metamor_id = reader.ReadInt(),
        resource_reference_unit_id = reader.ReadIntOrNull(),
        model_reference_id = reader.ReadIntOrNull(),
        field_normal_face_material_name = reader.ReadString(true),
        field_gray_body_material_name = reader.ReadString(true),
        field_gray_face_material_name = reader.ReadString(true),
        field_gray_vehicle_material_name = reader.ReadString(true),
        field_gray_equip_material_name = reader.ReadString(true),
        field_gray_equip_b_material_name = reader.ReadString(true),
        job_change_voice = reader.ReadStringOrNull(true),
        initial_gear_GearGear = reader.ReadIntOrNull()
      };
    }

    public UnitUnit unit_id
    {
      get
      {
        UnitUnit unitUnit;
        if (!MasterData.UnitUnit.TryGetValue(this.unit_id_UnitUnit, out unitUnit))
          Debug.LogError((object) ("Key not Found: MasterData.UnitUnit[" + (object) this.unit_id_UnitUnit + "]"));
        return unitUnit;
      }
    }

    public GearGear initial_gear
    {
      get
      {
        if (!this.initial_gear_GearGear.HasValue)
          return (GearGear) null;
        GearGear gearGear;
        if (!MasterData.GearGear.TryGetValue(this.initial_gear_GearGear.Value, out gearGear))
          Debug.LogError((object) ("Key not Found: MasterData.GearGear[" + (object) this.initial_gear_GearGear.Value + "]"));
        return gearGear;
      }
    }
  }
}
