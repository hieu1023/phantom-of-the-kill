﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.JobCharacteristics
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UniLinq;

namespace MasterDataTable
{
  [Serializable]
  public class JobCharacteristics
  {
    public int ID;
    public int skill_BattleskillSkill;
    public int? skill2_BattleskillSkill;
    public string level_pattern_id;
    public int levelmax_bonus_JobCharacteristicsLevelmaxBonus;
    public int levelmax_bonus_value;
    private int[] levelup_patterns_;

    public static JobCharacteristics Parse(MasterDataReader reader)
    {
      return new JobCharacteristics()
      {
        ID = reader.ReadInt(),
        skill_BattleskillSkill = reader.ReadInt(),
        skill2_BattleskillSkill = reader.ReadIntOrNull(),
        level_pattern_id = reader.ReadStringOrNull(true),
        levelmax_bonus_JobCharacteristicsLevelmaxBonus = reader.ReadInt(),
        levelmax_bonus_value = reader.ReadInt()
      };
    }

    public BattleskillSkill skill
    {
      get
      {
        BattleskillSkill battleskillSkill;
        if (!MasterData.BattleskillSkill.TryGetValue(this.skill_BattleskillSkill, out battleskillSkill))
          Debug.LogError((object) ("Key not Found: MasterData.BattleskillSkill[" + (object) this.skill_BattleskillSkill + "]"));
        return battleskillSkill;
      }
    }

    public BattleskillSkill skill2
    {
      get
      {
        if (!this.skill2_BattleskillSkill.HasValue)
          return (BattleskillSkill) null;
        BattleskillSkill battleskillSkill;
        if (!MasterData.BattleskillSkill.TryGetValue(this.skill2_BattleskillSkill.Value, out battleskillSkill))
          Debug.LogError((object) ("Key not Found: MasterData.BattleskillSkill[" + (object) this.skill2_BattleskillSkill.Value + "]"));
        return battleskillSkill;
      }
    }

    public JobCharacteristicsLevelmaxBonus levelmax_bonus
    {
      get
      {
        return (JobCharacteristicsLevelmaxBonus) this.levelmax_bonus_JobCharacteristicsLevelmaxBonus;
      }
    }

    public int[] levelup_patterns
    {
      get
      {
        return this.levelup_patterns_ ?? (this.levelup_patterns_ = this.stringToIntegers(this.level_pattern_id));
      }
    }

    private int[] stringToIntegers(string str)
    {
      if (string.IsNullOrEmpty(str))
        return new int[0];
      double result;
      return ((IEnumerable<string>) str.Split(',')).Select<string, int>((Func<string, int>) (s => !double.TryParse(s.Trim(), out result) ? 0 : (int) Math.Floor(result))).ToArray<int>();
    }
  }
}
