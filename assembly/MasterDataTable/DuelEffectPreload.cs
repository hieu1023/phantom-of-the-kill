﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.DuelEffectPreload
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class DuelEffectPreload
  {
    public int ID;
    public int duel_controller_id;
    public string effect_file_name;

    public static DuelEffectPreload Parse(MasterDataReader reader)
    {
      return new DuelEffectPreload()
      {
        ID = reader.ReadInt(),
        duel_controller_id = reader.ReadInt(),
        effect_file_name = reader.ReadString(true)
      };
    }
  }
}
