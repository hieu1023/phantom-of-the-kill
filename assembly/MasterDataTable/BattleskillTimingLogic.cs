﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.BattleskillTimingLogic
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class BattleskillTimingLogic
  {
    public int ID;
    public string name;

    public static BattleskillTimingLogic Parse(MasterDataReader reader)
    {
      return new BattleskillTimingLogic()
      {
        ID = reader.ReadInt(),
        name = reader.ReadString(true)
      };
    }
  }
}
