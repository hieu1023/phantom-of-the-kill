﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.GuildAutokick
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class GuildAutokick
  {
    public int ID;
    public string name;

    public static GuildAutokick Parse(MasterDataReader reader)
    {
      return new GuildAutokick()
      {
        ID = reader.ReadInt(),
        name = reader.ReadString(true)
      };
    }
  }
}
