﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.GuildBasePos
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class GuildBasePos
  {
    public int ID;
    public float baseXpos;
    public float baseYpos;

    public static GuildBasePos Parse(MasterDataReader reader)
    {
      return new GuildBasePos()
      {
        ID = reader.ReadInt(),
        baseXpos = reader.ReadFloat(),
        baseYpos = reader.ReadFloat()
      };
    }
  }
}
