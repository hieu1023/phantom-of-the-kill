﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.BattleMapFacilitySetting
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class BattleMapFacilitySetting
  {
    public int ID;
    public int map_BattleStage;
    public int coordinate_x;
    public int coordinate_y;

    public static BattleMapFacilitySetting Parse(MasterDataReader reader)
    {
      return new BattleMapFacilitySetting()
      {
        ID = reader.ReadInt(),
        map_BattleStage = reader.ReadInt(),
        coordinate_x = reader.ReadInt(),
        coordinate_y = reader.ReadInt()
      };
    }

    public BattleStage map
    {
      get
      {
        BattleStage battleStage;
        if (!MasterData.BattleStage.TryGetValue(this.map_BattleStage, out battleStage))
          Debug.LogError((object) ("Key not Found: MasterData.BattleStage[" + (object) this.map_BattleStage + "]"));
        return battleStage;
      }
    }
  }
}
