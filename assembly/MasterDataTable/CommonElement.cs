﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.CommonElement
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

namespace MasterDataTable
{
  public enum CommonElement
  {
    none = 1,
    fire = 2,
    wind = 3,
    thunder = 4,
    ice = 5,
    earth = 6,
    light = 7,
    dark = 8,
    saint = 9,
    demon = 10, // 0x0000000A
    dragon = 11, // 0x0000000B
    angel = 12, // 0x0000000C
    devil = 13, // 0x0000000D
    beast = 14, // 0x0000000E
    fairy = 15, // 0x0000000F
    princess = 16, // 0x00000010
  }
}
