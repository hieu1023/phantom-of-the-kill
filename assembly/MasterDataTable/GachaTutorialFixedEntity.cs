﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.GachaTutorialFixedEntity
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class GachaTutorialFixedEntity
  {
    public int ID;
    public int _gacha_id;
    public int _deck_id;
    public int _fix_count;
    public string name;

    public static GachaTutorialFixedEntity Parse(MasterDataReader reader)
    {
      return new GachaTutorialFixedEntity()
      {
        ID = reader.ReadInt(),
        _gacha_id = reader.ReadInt(),
        _deck_id = reader.ReadInt(),
        _fix_count = reader.ReadInt(),
        name = reader.ReadString(true)
      };
    }
  }
}
