﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.UnitUnitDescription
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class UnitUnitDescription
  {
    public int ID;
    public string description;
    public string cv_name;
    public string illustrator_name;

    public static UnitUnitDescription Parse(MasterDataReader reader)
    {
      return new UnitUnitDescription()
      {
        ID = reader.ReadInt(),
        description = reader.ReadString(true),
        cv_name = reader.ReadString(true),
        illustrator_name = reader.ReadString(true)
      };
    }
  }
}
