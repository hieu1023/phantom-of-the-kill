﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.PvpRankingKind
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class PvpRankingKind
  {
    public int ID;
    public int class_kind_PvpClassKind;
    public string name;

    public static PvpRankingKind Parse(MasterDataReader reader)
    {
      return new PvpRankingKind()
      {
        ID = reader.ReadInt(),
        class_kind_PvpClassKind = reader.ReadInt(),
        name = reader.ReadString(true)
      };
    }

    public PvpClassKind class_kind
    {
      get
      {
        PvpClassKind pvpClassKind;
        if (!MasterData.PvpClassKind.TryGetValue(this.class_kind_PvpClassKind, out pvpClassKind))
          Debug.LogError((object) ("Key not Found: MasterData.PvpClassKind[" + (object) this.class_kind_PvpClassKind + "]"));
        return pvpClassKind;
      }
    }
  }
}
