﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.BattleFieldEffectTiming
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

namespace MasterDataTable
{
  public enum BattleFieldEffectTiming
  {
    before_battle = 1,
    first_turn_start = 2,
    turn_start = 3,
    player_start = 4,
    neutral_start = 5,
    enemy_start = 6,
    stageclear = 7,
    pvp_change_player = 8,
    pvp_change_enemy = 9,
  }
}
