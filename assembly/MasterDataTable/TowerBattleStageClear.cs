﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.TowerBattleStageClear
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class TowerBattleStageClear
  {
    public int ID;
    public int reward_group_id;
    public int stage_id;
    public int lap;
    public int entity_type_CommonRewardType;
    public int reward_id;
    public int is_direct;
    public string reward_name;
    public string reward_message;

    public static TowerBattleStageClear Parse(MasterDataReader reader)
    {
      return new TowerBattleStageClear()
      {
        ID = reader.ReadInt(),
        reward_group_id = reader.ReadInt(),
        stage_id = reader.ReadInt(),
        lap = reader.ReadInt(),
        entity_type_CommonRewardType = reader.ReadInt(),
        reward_id = reader.ReadInt(),
        is_direct = reader.ReadInt(),
        reward_name = reader.ReadStringOrNull(true),
        reward_message = reader.ReadStringOrNull(true)
      };
    }

    public CommonRewardType entity_type
    {
      get
      {
        return (CommonRewardType) this.entity_type_CommonRewardType;
      }
    }
  }
}
