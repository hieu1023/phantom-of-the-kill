﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.QuestkeyCondition
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class QuestkeyCondition
  {
    public int ID;
    public int gate_id;
    public string contents;

    public static QuestkeyCondition Parse(MasterDataReader reader)
    {
      return new QuestkeyCondition()
      {
        ID = reader.ReadInt(),
        gate_id = reader.ReadInt(),
        contents = reader.ReadString(true)
      };
    }
  }
}
