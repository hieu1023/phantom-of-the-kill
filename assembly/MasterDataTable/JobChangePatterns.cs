﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.JobChangePatterns
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class JobChangePatterns
  {
    public int ID;
    public int unit_UnitUnit;
    public int job_UnitJob;
    public int job1_UnitJob;
    public int? materials1_JobChangeMaterials;
    public int job2_UnitJob;
    public int materials2_JobChangeMaterials;
    public int? job3_UnitJob;
    public int? materials3_JobChangeMaterials;
    public int? job4_UnitJob;
    public int? materials4_JobChangeMaterials;

    public static JobChangePatterns Parse(MasterDataReader reader)
    {
      return new JobChangePatterns()
      {
        ID = reader.ReadInt(),
        unit_UnitUnit = reader.ReadInt(),
        job_UnitJob = reader.ReadInt(),
        job1_UnitJob = reader.ReadInt(),
        materials1_JobChangeMaterials = reader.ReadIntOrNull(),
        job2_UnitJob = reader.ReadInt(),
        materials2_JobChangeMaterials = reader.ReadInt(),
        job3_UnitJob = reader.ReadIntOrNull(),
        materials3_JobChangeMaterials = reader.ReadIntOrNull(),
        job4_UnitJob = reader.ReadIntOrNull(),
        materials4_JobChangeMaterials = reader.ReadIntOrNull()
      };
    }

    public UnitUnit unit
    {
      get
      {
        UnitUnit unitUnit;
        if (!MasterData.UnitUnit.TryGetValue(this.unit_UnitUnit, out unitUnit))
          Debug.LogError((object) ("Key not Found: MasterData.UnitUnit[" + (object) this.unit_UnitUnit + "]"));
        return unitUnit;
      }
    }

    public UnitJob job
    {
      get
      {
        UnitJob unitJob;
        if (!MasterData.UnitJob.TryGetValue(this.job_UnitJob, out unitJob))
          Debug.LogError((object) ("Key not Found: MasterData.UnitJob[" + (object) this.job_UnitJob + "]"));
        return unitJob;
      }
    }

    public UnitJob job1
    {
      get
      {
        UnitJob unitJob;
        if (!MasterData.UnitJob.TryGetValue(this.job1_UnitJob, out unitJob))
          Debug.LogError((object) ("Key not Found: MasterData.UnitJob[" + (object) this.job1_UnitJob + "]"));
        return unitJob;
      }
    }

    public JobChangeMaterials materials1
    {
      get
      {
        if (!this.materials1_JobChangeMaterials.HasValue)
          return (JobChangeMaterials) null;
        JobChangeMaterials jobChangeMaterials;
        if (!MasterData.JobChangeMaterials.TryGetValue(this.materials1_JobChangeMaterials.Value, out jobChangeMaterials))
          Debug.LogError((object) ("Key not Found: MasterData.JobChangeMaterials[" + (object) this.materials1_JobChangeMaterials.Value + "]"));
        return jobChangeMaterials;
      }
    }

    public UnitJob job2
    {
      get
      {
        UnitJob unitJob;
        if (!MasterData.UnitJob.TryGetValue(this.job2_UnitJob, out unitJob))
          Debug.LogError((object) ("Key not Found: MasterData.UnitJob[" + (object) this.job2_UnitJob + "]"));
        return unitJob;
      }
    }

    public JobChangeMaterials materials2
    {
      get
      {
        JobChangeMaterials jobChangeMaterials;
        if (!MasterData.JobChangeMaterials.TryGetValue(this.materials2_JobChangeMaterials, out jobChangeMaterials))
          Debug.LogError((object) ("Key not Found: MasterData.JobChangeMaterials[" + (object) this.materials2_JobChangeMaterials + "]"));
        return jobChangeMaterials;
      }
    }

    public UnitJob job3
    {
      get
      {
        if (!this.job3_UnitJob.HasValue)
          return (UnitJob) null;
        UnitJob unitJob;
        if (!MasterData.UnitJob.TryGetValue(this.job3_UnitJob.Value, out unitJob))
          Debug.LogError((object) ("Key not Found: MasterData.UnitJob[" + (object) this.job3_UnitJob.Value + "]"));
        return unitJob;
      }
    }

    public JobChangeMaterials materials3
    {
      get
      {
        if (!this.materials3_JobChangeMaterials.HasValue)
          return (JobChangeMaterials) null;
        JobChangeMaterials jobChangeMaterials;
        if (!MasterData.JobChangeMaterials.TryGetValue(this.materials3_JobChangeMaterials.Value, out jobChangeMaterials))
          Debug.LogError((object) ("Key not Found: MasterData.JobChangeMaterials[" + (object) this.materials3_JobChangeMaterials.Value + "]"));
        return jobChangeMaterials;
      }
    }

    public UnitJob job4
    {
      get
      {
        if (!this.job4_UnitJob.HasValue)
          return (UnitJob) null;
        UnitJob unitJob;
        if (!MasterData.UnitJob.TryGetValue(this.job4_UnitJob.Value, out unitJob))
          Debug.LogError((object) ("Key not Found: MasterData.UnitJob[" + (object) this.job4_UnitJob.Value + "]"));
        return unitJob;
      }
    }

    public JobChangeMaterials materials4
    {
      get
      {
        if (!this.materials4_JobChangeMaterials.HasValue)
          return (JobChangeMaterials) null;
        JobChangeMaterials jobChangeMaterials;
        if (!MasterData.JobChangeMaterials.TryGetValue(this.materials4_JobChangeMaterials.Value, out jobChangeMaterials))
          Debug.LogError((object) ("Key not Found: MasterData.JobChangeMaterials[" + (object) this.materials4_JobChangeMaterials.Value + "]"));
        return jobChangeMaterials;
      }
    }
  }
}
