﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.JobCharacteristicsLevelmaxBonus
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

namespace MasterDataTable
{
  public enum JobCharacteristicsLevelmaxBonus
  {
    none,
    hp_add,
    strength_add,
    intelligence_add,
    vitality_add,
    mind_add,
    agility_add,
    dexterity_add,
    lucky_add,
    movement_add,
  }
}
