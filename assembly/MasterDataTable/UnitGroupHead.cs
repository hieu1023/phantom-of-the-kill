﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.UnitGroupHead
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

namespace MasterDataTable
{
  public enum UnitGroupHead
  {
    group_all = 1,
    group_large = 2,
    group_small = 3,
    group_clothing = 4,
    group_generation = 5,
  }
}
