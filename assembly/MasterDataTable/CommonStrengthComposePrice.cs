﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.CommonStrengthComposePrice
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class CommonStrengthComposePrice
  {
    public int ID;
    public int price;

    public static CommonStrengthComposePrice Parse(MasterDataReader reader)
    {
      return new CommonStrengthComposePrice()
      {
        ID = reader.ReadInt(),
        price = reader.ReadInt()
      };
    }
  }
}
