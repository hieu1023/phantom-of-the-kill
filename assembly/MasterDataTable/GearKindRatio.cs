﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.GearKindRatio
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class GearKindRatio
  {
    public int ID;
    public int kind_GearKind;
    public int family_UnitFamily;
    public float ratio;

    public static GearKindRatio Parse(MasterDataReader reader)
    {
      return new GearKindRatio()
      {
        ID = reader.ReadInt(),
        kind_GearKind = reader.ReadInt(),
        family_UnitFamily = reader.ReadInt(),
        ratio = reader.ReadFloat()
      };
    }

    public GearKind kind
    {
      get
      {
        GearKind gearKind;
        if (!MasterData.GearKind.TryGetValue(this.kind_GearKind, out gearKind))
          Debug.LogError((object) ("Key not Found: MasterData.GearKind[" + (object) this.kind_GearKind + "]"));
        return gearKind;
      }
    }

    public UnitFamily family
    {
      get
      {
        return (UnitFamily) this.family_UnitFamily;
      }
    }
  }
}
