﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.GuildRaidSettings
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class GuildRaidSettings
  {
    public int ID;
    public string key;
    public int value;

    public static GuildRaidSettings Parse(MasterDataReader reader)
    {
      return new GuildRaidSettings()
      {
        ID = reader.ReadInt(),
        key = reader.ReadString(true),
        value = reader.ReadInt()
      };
    }
  }
}
