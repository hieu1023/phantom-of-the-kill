﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.FacilitySkillGroup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class FacilitySkillGroup
  {
    public int ID;
    public int facility_level_info_FacilityLevel;
    public int skill_BattleskillSkill;

    public static FacilitySkillGroup Parse(MasterDataReader reader)
    {
      return new FacilitySkillGroup()
      {
        ID = reader.ReadInt(),
        facility_level_info_FacilityLevel = reader.ReadInt(),
        skill_BattleskillSkill = reader.ReadInt()
      };
    }

    public FacilityLevel facility_level_info
    {
      get
      {
        FacilityLevel facilityLevel;
        if (!MasterData.FacilityLevel.TryGetValue(this.facility_level_info_FacilityLevel, out facilityLevel))
          Debug.LogError((object) ("Key not Found: MasterData.FacilityLevel[" + (object) this.facility_level_info_FacilityLevel + "]"));
        return facilityLevel;
      }
    }

    public BattleskillSkill skill
    {
      get
      {
        BattleskillSkill battleskillSkill;
        if (!MasterData.BattleskillSkill.TryGetValue(this.skill_BattleskillSkill, out battleskillSkill))
          Debug.LogError((object) ("Key not Found: MasterData.BattleskillSkill[" + (object) this.skill_BattleskillSkill + "]"));
        return battleskillSkill;
      }
    }
  }
}
