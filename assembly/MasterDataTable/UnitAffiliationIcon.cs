﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.UnitAffiliationIcon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class UnitAffiliationIcon
  {
    public int ID;
    public string english_name;
    public int unit_group_head_UnitGroupHead;
    public int group_refer_id;
    public int priority;
    public string file_name;

    public static UnitAffiliationIcon Parse(MasterDataReader reader)
    {
      return new UnitAffiliationIcon()
      {
        ID = reader.ReadInt(),
        english_name = reader.ReadString(true),
        unit_group_head_UnitGroupHead = reader.ReadInt(),
        group_refer_id = reader.ReadInt(),
        priority = reader.ReadInt(),
        file_name = reader.ReadString(true)
      };
    }

    public UnitGroupHead unit_group_head
    {
      get
      {
        return (UnitGroupHead) this.unit_group_head_UnitGroupHead;
      }
    }
  }
}
