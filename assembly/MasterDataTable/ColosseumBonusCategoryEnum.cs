﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.ColosseumBonusCategoryEnum
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

namespace MasterDataTable
{
  public enum ColosseumBonusCategoryEnum
  {
    type = 1,
    job = 2,
    attribute = 3,
    kind = 4,
    blood = 5,
    zodiac = 6,
    height = 7,
    weight = 8,
    bust = 9,
    waist = 10, // 0x0000000A
    hip = 11, // 0x0000000B
    birthday = 12, // 0x0000000C
    element = 13, // 0x0000000D
    unit_group_large = 14, // 0x0000000E
    unit_group_small = 15, // 0x0000000F
    unit_group_clothing = 16, // 0x00000010
    unit_group_generation = 17, // 0x00000011
  }
}
