﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.ReviewReward
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class ReviewReward
  {
    public int ID;
    public int quest_type_CommonQuestType;
    public int quest_s_id;
    public int reward_type_CommonRewardType;
    public int reward_id;
    public int reward_quantity;
    public string title;
    public string message;

    public static ReviewReward Parse(MasterDataReader reader)
    {
      return new ReviewReward()
      {
        ID = reader.ReadInt(),
        quest_type_CommonQuestType = reader.ReadInt(),
        quest_s_id = reader.ReadInt(),
        reward_type_CommonRewardType = reader.ReadInt(),
        reward_id = reader.ReadInt(),
        reward_quantity = reader.ReadInt(),
        title = reader.ReadString(true),
        message = reader.ReadString(true)
      };
    }

    public CommonQuestType quest_type
    {
      get
      {
        return (CommonQuestType) this.quest_type_CommonQuestType;
      }
    }

    public CommonRewardType reward_type
    {
      get
      {
        return (CommonRewardType) this.reward_type_CommonRewardType;
      }
    }
  }
}
