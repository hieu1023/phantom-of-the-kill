﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.QuestStoryMission
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class QuestStoryMission
  {
    public int ID;
    public int quest_s_QuestStoryS;
    public int priority;
    public string name;
    public int entity_type_CommonRewardType;
    public int entity_id;
    public int quantity;

    public static QuestStoryMission Parse(MasterDataReader reader)
    {
      return new QuestStoryMission()
      {
        ID = reader.ReadInt(),
        quest_s_QuestStoryS = reader.ReadInt(),
        priority = reader.ReadInt(),
        name = reader.ReadString(true),
        entity_type_CommonRewardType = reader.ReadInt(),
        entity_id = reader.ReadInt(),
        quantity = reader.ReadInt()
      };
    }

    public QuestStoryS quest_s
    {
      get
      {
        QuestStoryS questStoryS;
        if (!MasterData.QuestStoryS.TryGetValue(this.quest_s_QuestStoryS, out questStoryS))
          Debug.LogError((object) ("Key not Found: MasterData.QuestStoryS[" + (object) this.quest_s_QuestStoryS + "]"));
        return questStoryS;
      }
    }

    public CommonRewardType entity_type
    {
      get
      {
        return (CommonRewardType) this.entity_type_CommonRewardType;
      }
    }
  }
}
