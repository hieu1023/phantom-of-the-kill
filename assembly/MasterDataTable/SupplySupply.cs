﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.SupplySupply
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System;

namespace MasterDataTable
{
  [Serializable]
  public class SupplySupply
  {
    public int ID;
    public string name;
    public string description;
    public string flavor;
    public int rarity_GearRarity;
    public int max_count;
    public int battle_stack_limit;
    public int sell_price;
    public int skill_BattleskillSkill;

    public static SupplySupply Parse(MasterDataReader reader)
    {
      return new SupplySupply()
      {
        ID = reader.ReadInt(),
        name = reader.ReadString(true),
        description = reader.ReadString(true),
        flavor = reader.ReadString(true),
        rarity_GearRarity = reader.ReadInt(),
        max_count = reader.ReadInt(),
        battle_stack_limit = reader.ReadInt(),
        sell_price = reader.ReadInt(),
        skill_BattleskillSkill = reader.ReadInt()
      };
    }

    public GearRarity rarity
    {
      get
      {
        GearRarity gearRarity;
        if (!MasterData.GearRarity.TryGetValue(this.rarity_GearRarity, out gearRarity))
          Debug.LogError((object) ("Key not Found: MasterData.GearRarity[" + (object) this.rarity_GearRarity + "]"));
        return gearRarity;
      }
    }

    public BattleskillSkill skill
    {
      get
      {
        BattleskillSkill battleskillSkill;
        if (!MasterData.BattleskillSkill.TryGetValue(this.skill_BattleskillSkill, out battleskillSkill))
          Debug.LogError((object) ("Key not Found: MasterData.BattleskillSkill[" + (object) this.skill_BattleskillSkill + "]"));
        return battleskillSkill;
      }
    }

    public Future<UnityEngine.Sprite> LoadSpriteThumbnail()
    {
      return Singleton<ResourceManager>.GetInstance().Load<UnityEngine.Sprite>(string.Format("AssetBundle/Resources/Supplies/{0}/2D/item_thum", (object) this.ID), 1f);
    }

    public Future<UnityEngine.Sprite> LoadSpriteBasic()
    {
      return Singleton<ResourceManager>.GetInstance().Load<UnityEngine.Sprite>(string.Format("AssetBundle/Resources/Supplies/{0}/2D/item_basic", (object) this.ID), 1f);
    }
  }
}
