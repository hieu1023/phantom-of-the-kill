﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.UnitMaterialQuestInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class UnitMaterialQuestInfo
  {
    public int ID;
    public int unit_id;
    public string short_desc;
    public string long_desc;

    public static UnitMaterialQuestInfo Parse(MasterDataReader reader)
    {
      return new UnitMaterialQuestInfo()
      {
        ID = reader.ReadInt(),
        unit_id = reader.ReadInt(),
        short_desc = reader.ReadString(true),
        long_desc = reader.ReadString(true)
      };
    }
  }
}
