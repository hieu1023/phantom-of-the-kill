﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.GuildBaseBonus
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class GuildBaseBonus
  {
    public int ID;
    public int base_type_GuildBaseType;
    public int base_rank;
    public int bonus_type_GuildBaseBonusType;
    public int bonus_grade;

    public static GuildBaseBonus Parse(MasterDataReader reader)
    {
      return new GuildBaseBonus()
      {
        ID = reader.ReadInt(),
        base_type_GuildBaseType = reader.ReadInt(),
        base_rank = reader.ReadInt(),
        bonus_type_GuildBaseBonusType = reader.ReadInt(),
        bonus_grade = reader.ReadInt()
      };
    }

    public GuildBaseType base_type
    {
      get
      {
        return (GuildBaseType) this.base_type_GuildBaseType;
      }
    }

    public GuildBaseBonusType bonus_type
    {
      get
      {
        return (GuildBaseBonusType) this.bonus_type_GuildBaseBonusType;
      }
    }
  }
}
