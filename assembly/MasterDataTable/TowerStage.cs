﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.TowerStage
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System;

namespace MasterDataTable
{
  [Serializable]
  public class TowerStage
  {
    public int ID;
    public int tower_id;
    public int floor;
    public int stage_id;
    public int back_ground_id_TowerCommonBackground;

    public static TowerStage Parse(MasterDataReader reader)
    {
      return new TowerStage()
      {
        ID = reader.ReadInt(),
        tower_id = reader.ReadInt(),
        floor = reader.ReadInt(),
        stage_id = reader.ReadInt(),
        back_ground_id_TowerCommonBackground = reader.ReadInt()
      };
    }

    public TowerCommonBackground back_ground_id
    {
      get
      {
        TowerCommonBackground commonBackground;
        if (!MasterData.TowerCommonBackground.TryGetValue(this.back_ground_id_TowerCommonBackground, out commonBackground))
          Debug.LogError((object) ("Key not Found: MasterData.TowerCommonBackground[" + (object) this.back_ground_id_TowerCommonBackground + "]"));
        return commonBackground;
      }
    }

    public string GetBackgroundPath()
    {
      return !string.IsNullOrEmpty(this.back_ground_id.background_name) ? string.Format(Consts.GetInstance().BACKGROUND_BASE_PATH, (object) this.back_ground_id.background_name) : Consts.GetInstance().DEFULAT_BACKGROUND;
    }
  }
}
