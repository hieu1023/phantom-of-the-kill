﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.UnitJobFamily
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class UnitJobFamily
  {
    public int ID;
    public int job_UnitJob;
    public int element_UnitFamily;

    public static UnitJobFamily Parse(MasterDataReader reader)
    {
      return new UnitJobFamily()
      {
        ID = reader.ReadInt(),
        job_UnitJob = reader.ReadInt(),
        element_UnitFamily = reader.ReadInt()
      };
    }

    public UnitJob job
    {
      get
      {
        UnitJob unitJob;
        if (!MasterData.UnitJob.TryGetValue(this.job_UnitJob, out unitJob))
          Debug.LogError((object) ("Key not Found: MasterData.UnitJob[" + (object) this.job_UnitJob + "]"));
        return unitJob;
      }
    }

    public UnitFamily element
    {
      get
      {
        return (UnitFamily) this.element_UnitFamily;
      }
    }
  }
}
