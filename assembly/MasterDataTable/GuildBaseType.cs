﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.GuildBaseType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

namespace MasterDataTable
{
  public enum GuildBaseType
  {
    walls = 1,
    tower = 2,
    scaffold = 3,
    fortress = 4,
    town = 5,
  }
}
