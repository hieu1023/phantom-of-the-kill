﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.MatchingType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

namespace MasterDataTable
{
  public enum MatchingType
  {
    HARD = 1,
    NORMAL = 2,
    EASY = 3,
    RANK_UP_BATTLE = 4,
    RANK_DOWN_BATTLE = 5,
  }
}
