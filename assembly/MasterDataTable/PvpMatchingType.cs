﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.PvpMatchingType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class PvpMatchingType
  {
    public int ID;
    public string room_key;

    public static PvpMatchingType Parse(MasterDataReader reader)
    {
      return new PvpMatchingType()
      {
        ID = reader.ReadInt(),
        room_key = reader.ReadString(true)
      };
    }
  }
}
