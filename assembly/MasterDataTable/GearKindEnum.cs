﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.GearKindEnum
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

namespace MasterDataTable
{
  public enum GearKindEnum
  {
    sword = 1,
    axe = 2,
    spear = 3,
    bow = 4,
    gun = 5,
    staff = 6,
    shield = 7,
    unique_wepon = 8,
    smith = 9,
    accessories = 10, // 0x0000000A
    drilling = 11, // 0x0000000B
    special_drilling = 12, // 0x0000000C
    sea_present = 13, // 0x0000000D
    magic = 14, // 0x0000000E
    dummy = 1001, // 0x000003E9
    none = 9999, // 0x0000270F
  }
}
