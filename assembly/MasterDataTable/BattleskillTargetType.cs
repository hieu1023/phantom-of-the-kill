﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.BattleskillTargetType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

namespace MasterDataTable
{
  public enum BattleskillTargetType
  {
    myself = 1,
    player_range = 2,
    player_single = 3,
    enemy_single = 4,
    enemy_range = 5,
    dead_player_single = 6,
    complex_single = 7,
    complex_range = 8,
    panel_single = 9,
  }
}
