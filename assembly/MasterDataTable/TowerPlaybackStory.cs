﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.TowerPlaybackStory
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class TowerPlaybackStory
  {
    public int ID;
    public string name;
    public int stage_TowerStage;
    public int priority;
    public int banner_id;

    public static TowerPlaybackStory Parse(MasterDataReader reader)
    {
      return new TowerPlaybackStory()
      {
        ID = reader.ReadInt(),
        name = reader.ReadString(true),
        stage_TowerStage = reader.ReadInt(),
        priority = reader.ReadInt(),
        banner_id = reader.ReadInt()
      };
    }

    public TowerStage stage
    {
      get
      {
        TowerStage towerStage;
        if (!MasterData.TowerStage.TryGetValue(this.stage_TowerStage, out towerStage))
          Debug.LogError((object) ("Key not Found: MasterData.TowerStage[" + (object) this.stage_TowerStage + "]"));
        return towerStage;
      }
    }
  }
}
