﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.DateScriptQuestion
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class DateScriptQuestion
  {
    public int ID;
    public int unitID;
    public int questionID;
    public string script;

    public static DateScriptQuestion Parse(MasterDataReader reader)
    {
      return new DateScriptQuestion()
      {
        ID = reader.ReadInt(),
        unitID = reader.ReadInt(),
        questionID = reader.ReadInt(),
        script = reader.ReadString(true)
      };
    }
  }
}
