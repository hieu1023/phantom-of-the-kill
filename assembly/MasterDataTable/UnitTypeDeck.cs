﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.UnitTypeDeck
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class UnitTypeDeck
  {
    public int ID;
    public int? group_id_UnitUnit;
    public int category_id;
    public int appearance;
    public DateTime? start_at;
    public DateTime? end_at;

    public static UnitTypeDeck Parse(MasterDataReader reader)
    {
      return new UnitTypeDeck()
      {
        ID = reader.ReadInt(),
        group_id_UnitUnit = reader.ReadIntOrNull(),
        category_id = reader.ReadInt(),
        appearance = reader.ReadInt(),
        start_at = reader.ReadDateTimeOrNull(),
        end_at = reader.ReadDateTimeOrNull()
      };
    }

    public UnitUnit group_id
    {
      get
      {
        if (!this.group_id_UnitUnit.HasValue)
          return (UnitUnit) null;
        UnitUnit unitUnit;
        if (!MasterData.UnitUnit.TryGetValue(this.group_id_UnitUnit.Value, out unitUnit))
          Debug.LogError((object) ("Key not Found: MasterData.UnitUnit[" + (object) this.group_id_UnitUnit.Value + "]"));
        return unitUnit;
      }
    }
  }
}
