﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.UnitTypeTicket
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class UnitTypeTicket
  {
    public int ID;
    public string name;
    public string short_name;
    public int cost;
    public int max_ticket;
    public int ticketTypeID_UnitTypeTicketType;
    public string ticketTypeParam;
    public int icon_id;
    public int priority;
    public DateTime? end_at;
    public string description;

    public static UnitTypeTicket Parse(MasterDataReader reader)
    {
      return new UnitTypeTicket()
      {
        ID = reader.ReadInt(),
        name = reader.ReadString(true),
        short_name = reader.ReadString(true),
        cost = reader.ReadInt(),
        max_ticket = reader.ReadInt(),
        ticketTypeID_UnitTypeTicketType = reader.ReadInt(),
        ticketTypeParam = reader.ReadString(true),
        icon_id = reader.ReadInt(),
        priority = reader.ReadInt(),
        end_at = reader.ReadDateTimeOrNull(),
        description = reader.ReadString(true)
      };
    }

    public UnitTypeTicketType ticketTypeID
    {
      get
      {
        return (UnitTypeTicketType) this.ticketTypeID_UnitTypeTicketType;
      }
    }
  }
}
