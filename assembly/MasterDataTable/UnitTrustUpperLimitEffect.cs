﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.UnitTrustUpperLimitEffect
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class UnitTrustUpperLimitEffect
  {
    public int ID;
    public int same_character_id;
    public string face;
    public string serif;
    public string voice;
    public string background;

    public static UnitTrustUpperLimitEffect Parse(MasterDataReader reader)
    {
      return new UnitTrustUpperLimitEffect()
      {
        ID = reader.ReadInt(),
        same_character_id = reader.ReadInt(),
        face = reader.ReadString(true),
        serif = reader.ReadString(true),
        voice = reader.ReadString(true),
        background = reader.ReadString(true)
      };
    }

    public string[] GetSerif()
    {
      return this.serif.Split(',');
    }

    public string[] GetVoice()
    {
      return this.voice.Split(',');
    }
  }
}
