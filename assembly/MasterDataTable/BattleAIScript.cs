﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.BattleAIScript
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class BattleAIScript
  {
    public int ID;
    public string file_name;

    public static BattleAIScript Parse(MasterDataReader reader)
    {
      return new BattleAIScript()
      {
        ID = reader.ReadInt(),
        file_name = reader.ReadString(true)
      };
    }
  }
}
