﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.GuildRaidEndlessKillReward
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class GuildRaidEndlessKillReward
  {
    public int ID;
    public int raid;
    public int entity_type_CommonRewardType;
    public int rewardID;
    public string reward_title;
    public int num;

    public static GuildRaidEndlessKillReward Parse(MasterDataReader reader)
    {
      return new GuildRaidEndlessKillReward()
      {
        ID = reader.ReadInt(),
        raid = reader.ReadInt(),
        entity_type_CommonRewardType = reader.ReadInt(),
        rewardID = reader.ReadInt(),
        reward_title = reader.ReadString(true),
        num = reader.ReadInt()
      };
    }

    public CommonRewardType entity_type
    {
      get
      {
        return (CommonRewardType) this.entity_type_CommonRewardType;
      }
    }
  }
}
