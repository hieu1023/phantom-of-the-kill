﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.SlotS001MedalRarity
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class SlotS001MedalRarity
  {
    public int ID;
    public string name;
    public int index;

    public static SlotS001MedalRarity Parse(MasterDataReader reader)
    {
      return new SlotS001MedalRarity()
      {
        ID = reader.ReadInt(),
        name = reader.ReadString(true),
        index = reader.ReadInt()
      };
    }
  }
}
