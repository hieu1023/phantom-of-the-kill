﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.UnitType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class UnitType
  {
    public int ID;
    public string name;

    public static UnitType Parse(MasterDataReader reader)
    {
      return new UnitType()
      {
        ID = reader.ReadInt(),
        name = reader.ReadString(true)
      };
    }

    public UnitTypeEnum Enum
    {
      get
      {
        return (UnitTypeEnum) this.ID;
      }
    }
  }
}
