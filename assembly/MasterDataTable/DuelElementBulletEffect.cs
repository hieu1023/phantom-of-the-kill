﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.DuelElementBulletEffect
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class DuelElementBulletEffect
  {
    public int ID;
    public string bullet_prefab_name;
    public int element_CommonElement;
    public string effect_name;

    public static DuelElementBulletEffect Parse(MasterDataReader reader)
    {
      return new DuelElementBulletEffect()
      {
        ID = reader.ReadInt(),
        bullet_prefab_name = reader.ReadStringOrNull(true),
        element_CommonElement = reader.ReadInt(),
        effect_name = reader.ReadStringOrNull(true)
      };
    }

    public CommonElement element
    {
      get
      {
        return (CommonElement) this.element_CommonElement;
      }
    }
  }
}
