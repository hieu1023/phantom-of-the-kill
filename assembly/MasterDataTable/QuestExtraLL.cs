﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.QuestExtraLL
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class QuestExtraLL
  {
    public int ID;
    public string name;
    public int? description;
    public string background_image_name;
    public bool enabled_header;

    public static QuestExtraLL Parse(MasterDataReader reader)
    {
      return new QuestExtraLL()
      {
        ID = reader.ReadInt(),
        name = reader.ReadString(true),
        description = reader.ReadIntOrNull(),
        background_image_name = reader.ReadString(true),
        enabled_header = reader.ReadBool()
      };
    }
  }
}
