﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.EarthDesertCharacter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class EarthDesertCharacter
  {
    public int ID;
    public int desert_logic_EarthJoinLogicType;
    public string desert_logic_arg;
    public int character_id;

    public static EarthDesertCharacter Parse(MasterDataReader reader)
    {
      return new EarthDesertCharacter()
      {
        ID = reader.ReadInt(),
        desert_logic_EarthJoinLogicType = reader.ReadInt(),
        desert_logic_arg = reader.ReadStringOrNull(true),
        character_id = reader.ReadInt()
      };
    }

    public EarthJoinLogicType desert_logic
    {
      get
      {
        return (EarthJoinLogicType) this.desert_logic_EarthJoinLogicType;
      }
    }
  }
}
