﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.UnitCutinInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class UnitCutinInfo
  {
    public int ID;
    public int same_character_id;
    public int group_small_category_id;
    public string prefab_name;

    public static UnitCutinInfo Parse(MasterDataReader reader)
    {
      return new UnitCutinInfo()
      {
        ID = reader.ReadInt(),
        same_character_id = reader.ReadInt(),
        group_small_category_id = reader.ReadInt(),
        prefab_name = reader.ReadString(true)
      };
    }
  }
}
