﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.GearKind
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System;
using UnityEngine;

namespace MasterDataTable
{
  [Serializable]
  public class GearKind
  {
    public int ID;
    public string name;
    public int can_equip;
    public int same_element;
    public bool is_attack;
    public bool is_composite;
    public int colosseum_preempt_coefficient;

    public GearKindEnum Enum
    {
      get
      {
        return (GearKindEnum) this.ID;
      }
    }

    public bool isEquip
    {
      get
      {
        return this.Enum != GearKindEnum.smith && this.Enum != GearKindEnum.unique_wepon && (this.Enum != GearKindEnum.drilling && this.Enum != GearKindEnum.special_drilling) && this.Enum != GearKindEnum.sea_present;
      }
    }

    public bool isNonWeapon
    {
      get
      {
        return this.Enum == GearKindEnum.shield || this.Enum == GearKindEnum.accessories || (this.Enum == GearKindEnum.smith || this.Enum == GearKindEnum.drilling) || this.Enum == GearKindEnum.special_drilling || this.Enum == GearKindEnum.sea_present;
      }
    }

    public static GearKind Parse(MasterDataReader reader)
    {
      return new GearKind()
      {
        ID = reader.ReadInt(),
        name = reader.ReadString(true),
        can_equip = reader.ReadInt(),
        same_element = reader.ReadInt(),
        is_attack = reader.ReadBool(),
        is_composite = reader.ReadBool(),
        colosseum_preempt_coefficient = reader.ReadInt()
      };
    }

    public Future<GameObject> LoadFieldWeaponModel()
    {
      return Singleton<ResourceManager>.GetInstance().LoadOrNull<GameObject>(string.Format("GearKinds/{0}/3D/field_weapon/prefab", (object) this.ID));
    }
  }
}
