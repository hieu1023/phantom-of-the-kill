﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.JobMaterialGroup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class JobMaterialGroup
  {
    public int ID;
    public int? check_item_id_JobCheckItem;

    public static JobMaterialGroup Parse(MasterDataReader reader)
    {
      return new JobMaterialGroup()
      {
        ID = reader.ReadInt(),
        check_item_id_JobCheckItem = reader.ReadIntOrNull()
      };
    }

    public JobCheckItem? check_item_id
    {
      get
      {
        int? itemIdJobCheckItem = this.check_item_id_JobCheckItem;
        return !itemIdJobCheckItem.HasValue ? new JobCheckItem?() : new JobCheckItem?((JobCheckItem) itemIdJobCheckItem.GetValueOrDefault());
      }
    }
  }
}
