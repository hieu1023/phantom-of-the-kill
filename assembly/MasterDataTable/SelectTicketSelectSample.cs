﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.SelectTicketSelectSample
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class SelectTicketSelectSample
  {
    public int ID;
    public int ticketID;
    public int? deckID;
    public int entity_type_CommonRewardType;
    public int reward_id;
    public int reward_value;
    public string reward_title;
    public string reward_info;

    public static SelectTicketSelectSample Parse(MasterDataReader reader)
    {
      return new SelectTicketSelectSample()
      {
        ID = reader.ReadInt(),
        ticketID = reader.ReadInt(),
        deckID = reader.ReadIntOrNull(),
        entity_type_CommonRewardType = reader.ReadInt(),
        reward_id = reader.ReadInt(),
        reward_value = reader.ReadInt(),
        reward_title = reader.ReadString(true),
        reward_info = reader.ReadString(true)
      };
    }

    public CommonRewardType entity_type
    {
      get
      {
        return (CommonRewardType) this.entity_type_CommonRewardType;
      }
    }
  }
}
