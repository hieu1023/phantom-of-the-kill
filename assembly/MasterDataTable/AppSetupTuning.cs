﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.AppSetupTuning
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class AppSetupTuning
  {
    public int ID;
    public bool IsNotDeepCopy;
    public bool IsEnable;

    public static AppSetupTuning Parse(MasterDataReader reader)
    {
      return new AppSetupTuning()
      {
        ID = reader.ReadInt(),
        IsNotDeepCopy = reader.ReadBool(),
        IsEnable = reader.ReadBool()
      };
    }
  }
}
