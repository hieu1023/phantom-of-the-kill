﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.UnitMoveType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

namespace MasterDataTable
{
  public enum UnitMoveType
  {
    keihohei = 1,
    hohei = 2,
    madoushi = 3,
    jusohohei = 4,
    kihei = 5,
    hikou = 6,
    mizugi = 7,
    fuyu = 8,
  }
}
