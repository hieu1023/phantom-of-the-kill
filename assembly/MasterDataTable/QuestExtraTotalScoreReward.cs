﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.QuestExtraTotalScoreReward
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class QuestExtraTotalScoreReward
  {
    public int ID;
    public string display_text;
    public string image_name;

    public static QuestExtraTotalScoreReward Parse(MasterDataReader reader)
    {
      return new QuestExtraTotalScoreReward()
      {
        ID = reader.ReadInt(),
        display_text = reader.ReadString(true),
        image_name = reader.ReadString(true)
      };
    }
  }
}
