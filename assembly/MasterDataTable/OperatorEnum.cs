﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.OperatorEnum
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

namespace MasterDataTable
{
  public enum OperatorEnum
  {
    equal = 1,
    greater_than = 2,
    less_than = 3,
    greater_than_or_equal_to = 4,
    less_than_or_equal_to = 5,
    In = 6,
    like = 7,
    Out = 8,
  }
}
