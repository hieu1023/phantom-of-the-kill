﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.PvpStageFormation
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class PvpStageFormation
  {
    public int ID;
    public int stage_BattleStage;
    public int formation_x;
    public int formation_y;
    public int player_order;
    public float initial_direction;

    public static PvpStageFormation Parse(MasterDataReader reader)
    {
      return new PvpStageFormation()
      {
        ID = reader.ReadInt(),
        stage_BattleStage = reader.ReadInt(),
        formation_x = reader.ReadInt(),
        formation_y = reader.ReadInt(),
        player_order = reader.ReadInt(),
        initial_direction = reader.ReadFloat()
      };
    }

    public BattleStage stage
    {
      get
      {
        BattleStage battleStage;
        if (!MasterData.BattleStage.TryGetValue(this.stage_BattleStage, out battleStage))
          Debug.LogError((object) ("Key not Found: MasterData.BattleStage[" + (object) this.stage_BattleStage + "]"));
        return battleStage;
      }
    }
  }
}
