﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.GuildStamp
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class GuildStamp
  {
    public int ID;
    public int groupID_GuildStampGroup;

    public static GuildStamp Parse(MasterDataReader reader)
    {
      return new GuildStamp()
      {
        ID = reader.ReadInt(),
        groupID_GuildStampGroup = reader.ReadInt()
      };
    }

    public GuildStampGroup groupID
    {
      get
      {
        GuildStampGroup guildStampGroup;
        if (!MasterData.GuildStampGroup.TryGetValue(this.groupID_GuildStampGroup, out guildStampGroup))
          Debug.LogError((object) ("Key not Found: MasterData.GuildStampGroup[" + (object) this.groupID_GuildStampGroup + "]"));
        return guildStampGroup;
      }
    }
  }
}
