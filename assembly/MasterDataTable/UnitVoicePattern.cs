﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.UnitVoicePattern
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class UnitVoicePattern
  {
    private string selectorLabel = string.Empty;
    public int ID;
    public int character_id;
    public int voice_pattern;
    public string dead_message;
    public string file_name;
    public int selector_label;

    public static UnitVoicePattern Parse(MasterDataReader reader)
    {
      return new UnitVoicePattern()
      {
        ID = reader.ReadInt(),
        character_id = reader.ReadInt(),
        voice_pattern = reader.ReadInt(),
        dead_message = reader.ReadString(true),
        file_name = reader.ReadString(true),
        selector_label = reader.ReadInt()
      };
    }

    public string SelectorLabel
    {
      get
      {
        if (this.selector_label == 0)
          return (string) null;
        if (string.IsNullOrEmpty(this.selectorLabel))
          this.selectorLabel = this.selector_label.ToString();
        return this.selectorLabel;
      }
    }
  }
}
