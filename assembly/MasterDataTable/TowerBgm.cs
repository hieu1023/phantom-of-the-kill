﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.TowerBgm
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class TowerBgm
  {
    public int ID;
    public int period_id;
    public string bgm_name;
    public string bgm_file;
    public int floor;

    public static TowerBgm Parse(MasterDataReader reader)
    {
      return new TowerBgm()
      {
        ID = reader.ReadInt(),
        period_id = reader.ReadInt(),
        bgm_name = reader.ReadStringOrNull(true),
        bgm_file = reader.ReadStringOrNull(true),
        floor = reader.ReadInt()
      };
    }
  }
}
