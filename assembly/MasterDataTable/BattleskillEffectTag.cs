﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.BattleskillEffectTag
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

namespace MasterDataTable
{
  public enum BattleskillEffectTag
  {
    none = 1,
    buff_debuff = 2,
    fix_value = 3,
    ratio_value = 4,
    unit_incr = 5,
    battle_incr = 6,
    immediately = 7,
    ext_arg = 8,
  }
}
