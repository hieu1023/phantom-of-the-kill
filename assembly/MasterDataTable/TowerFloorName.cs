﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.TowerFloorName
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class TowerFloorName
  {
    public int ID;
    public string prefix;
    public string suffix;
    public int interval;

    public static TowerFloorName Parse(MasterDataReader reader)
    {
      return new TowerFloorName()
      {
        ID = reader.ReadInt(),
        prefix = reader.ReadStringOrNull(true),
        suffix = reader.ReadStringOrNull(true),
        interval = reader.ReadInt()
      };
    }
  }
}
