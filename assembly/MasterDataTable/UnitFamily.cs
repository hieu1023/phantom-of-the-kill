﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.UnitFamily
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

namespace MasterDataTable
{
  public enum UnitFamily
  {
    none,
    beast,
    fly,
    armor,
    dragon,
    undead,
    imitation1,
    imitation2,
    imitation3,
    imitation4,
    imitation5,
    imitation6,
    imitation7,
    imitation8,
    imitation9,
    imitation10,
    angel,
    devil,
    cryptid,
    fairy,
    princess,
    machine_beast,
    machine_fly,
    machine_dragon,
    machine_soldier,
    imaizumi,
    kimura,
    nave,
    tamasan,
    bloodywolf,
    saori,
    kenchan,
    fuyu,
    god,
    rezero,
    command,
    fat,
    konosuba,
    madomagi,
  }
}
