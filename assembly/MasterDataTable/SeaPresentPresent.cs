﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.SeaPresentPresent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class SeaPresentPresent
  {
    public int ID;
    public int gear_id;
    public float trust_base;

    public static SeaPresentPresent Parse(MasterDataReader reader)
    {
      return new SeaPresentPresent()
      {
        ID = reader.ReadInt(),
        gear_id = reader.ReadInt(),
        trust_base = reader.ReadFloat()
      };
    }
  }
}
