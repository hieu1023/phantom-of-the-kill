﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.GvgSettings
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class GvgSettings
  {
    public int ID;
    public string key;
    public float value;

    public static GvgSettings Parse(MasterDataReader reader)
    {
      return new GvgSettings()
      {
        ID = reader.ReadInt(),
        key = reader.ReadString(true),
        value = reader.ReadFloat()
      };
    }
  }
}
