﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.GuildAutoApproval
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class GuildAutoApproval
  {
    public int ID;
    public string name;
    public bool auto_approval;

    public static GuildAutoApproval Parse(MasterDataReader reader)
    {
      return new GuildAutoApproval()
      {
        ID = reader.ReadInt(),
        name = reader.ReadString(true),
        auto_approval = reader.ReadBool()
      };
    }
  }
}
