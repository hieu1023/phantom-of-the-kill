﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.AIScoreCorrection
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class AIScoreCorrection
  {
    public int ID;
    public int pattern_AIScorePattern;
    public int score_AIScore;
    public float var1;
    public float var2;
    public float var3;

    public static AIScoreCorrection Parse(MasterDataReader reader)
    {
      return new AIScoreCorrection()
      {
        ID = reader.ReadInt(),
        pattern_AIScorePattern = reader.ReadInt(),
        score_AIScore = reader.ReadInt(),
        var1 = reader.ReadFloat(),
        var2 = reader.ReadFloat(),
        var3 = reader.ReadFloat()
      };
    }

    public AIScorePattern pattern
    {
      get
      {
        AIScorePattern aiScorePattern;
        if (!MasterData.AIScorePattern.TryGetValue(this.pattern_AIScorePattern, out aiScorePattern))
          Debug.LogError((object) ("Key not Found: MasterData.AIScorePattern[" + (object) this.pattern_AIScorePattern + "]"));
        return aiScorePattern;
      }
    }

    public AIScore score
    {
      get
      {
        AIScore aiScore;
        if (!MasterData.AIScore.TryGetValue(this.score_AIScore, out aiScore))
          Debug.LogError((object) ("Key not Found: MasterData.AIScore[" + (object) this.score_AIScore + "]"));
        return aiScore;
      }
    }
  }
}
