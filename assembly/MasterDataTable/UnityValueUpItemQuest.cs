﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.UnityValueUpItemQuest
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class UnityValueUpItemQuest
  {
    public int ID;
    public int material_unit_id_UnitUnit;
    public string quest_sids;
    public int quest_type_CommonQuestType;

    public static UnityValueUpItemQuest Parse(MasterDataReader reader)
    {
      return new UnityValueUpItemQuest()
      {
        ID = reader.ReadInt(),
        material_unit_id_UnitUnit = reader.ReadInt(),
        quest_sids = reader.ReadStringOrNull(true),
        quest_type_CommonQuestType = reader.ReadInt()
      };
    }

    public UnitUnit material_unit_id
    {
      get
      {
        UnitUnit unitUnit;
        if (!MasterData.UnitUnit.TryGetValue(this.material_unit_id_UnitUnit, out unitUnit))
          Debug.LogError((object) ("Key not Found: MasterData.UnitUnit[" + (object) this.material_unit_id_UnitUnit + "]"));
        return unitUnit;
      }
    }

    public CommonQuestType quest_type
    {
      get
      {
        return (CommonQuestType) this.quest_type_CommonQuestType;
      }
    }
  }
}
