﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.PvpVictoryEffect
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class PvpVictoryEffect
  {
    public int ID;
    public int victory_type_PvpVictoryTypeEnum;

    public static PvpVictoryEffect Parse(MasterDataReader reader)
    {
      return new PvpVictoryEffect()
      {
        ID = reader.ReadInt(),
        victory_type_PvpVictoryTypeEnum = reader.ReadInt()
      };
    }

    public PvpVictoryTypeEnum victory_type
    {
      get
      {
        return (PvpVictoryTypeEnum) this.victory_type_PvpVictoryTypeEnum;
      }
    }
  }
}
