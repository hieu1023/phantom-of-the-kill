﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.QuestMovieQuest
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class QuestMovieQuest
  {
    public int ID;
    public int quest_type_CommonQuestType;
    public int quest_s_id;
    public int movie_path_QuestMoviePath;

    public static QuestMovieQuest Parse(MasterDataReader reader)
    {
      return new QuestMovieQuest()
      {
        ID = reader.ReadInt(),
        quest_type_CommonQuestType = reader.ReadInt(),
        quest_s_id = reader.ReadInt(),
        movie_path_QuestMoviePath = reader.ReadInt()
      };
    }

    public CommonQuestType quest_type
    {
      get
      {
        return (CommonQuestType) this.quest_type_CommonQuestType;
      }
    }

    public QuestMoviePath movie_path
    {
      get
      {
        QuestMoviePath questMoviePath;
        if (!MasterData.QuestMoviePath.TryGetValue(this.movie_path_QuestMoviePath, out questMoviePath))
          Debug.LogError((object) ("Key not Found: MasterData.QuestMoviePath[" + (object) this.movie_path_QuestMoviePath + "]"));
        return questMoviePath;
      }
    }
  }
}
