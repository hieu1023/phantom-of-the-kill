﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.ComposeMaxUnityValueSetting
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class ComposeMaxUnityValueSetting
  {
    public int ID;
    public string hp_compose_add_max;
    public string strength_compose_add_max;
    public string vitality_compose_add_max;
    public string intelligence_compose_add_max;
    public string mind_compose_add_max;
    public string agility_compose_add_max;
    public string dexterity_compose_add_max;
    public string lucky_compose_add_max;
    public string name;
    public string description;

    public static ComposeMaxUnityValueSetting Parse(
      MasterDataReader reader)
    {
      return new ComposeMaxUnityValueSetting()
      {
        ID = reader.ReadInt(),
        hp_compose_add_max = reader.ReadStringOrNull(true),
        strength_compose_add_max = reader.ReadStringOrNull(true),
        vitality_compose_add_max = reader.ReadStringOrNull(true),
        intelligence_compose_add_max = reader.ReadStringOrNull(true),
        mind_compose_add_max = reader.ReadStringOrNull(true),
        agility_compose_add_max = reader.ReadStringOrNull(true),
        dexterity_compose_add_max = reader.ReadStringOrNull(true),
        lucky_compose_add_max = reader.ReadStringOrNull(true),
        name = reader.ReadString(true),
        description = reader.ReadString(true)
      };
    }
  }
}
