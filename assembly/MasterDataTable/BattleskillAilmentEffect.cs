﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.BattleskillAilmentEffect
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System;
using UnityEngine;

namespace MasterDataTable
{
  [Serializable]
  public class BattleskillAilmentEffect
  {
    public int ID;
    public string field_effect_name;
    public string duel_effect_name;

    public static BattleskillAilmentEffect Parse(MasterDataReader reader)
    {
      return new BattleskillAilmentEffect()
      {
        ID = reader.ReadInt(),
        field_effect_name = reader.ReadString(true),
        duel_effect_name = reader.ReadString(true)
      };
    }

    public Future<GameObject> LoadFieldAilmentEffectPrefab()
    {
      return Singleton<ResourceManager>.GetInstance().LoadOrNull<GameObject>(string.Format("BattleEffects/field/{0}", (object) this.field_effect_name));
    }
  }
}
