﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.TotalPaymentBonus
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class TotalPaymentBonus
  {
    public int ID;
    public string description;

    public static TotalPaymentBonus Parse(MasterDataReader reader)
    {
      return new TotalPaymentBonus()
      {
        ID = reader.ReadInt(),
        description = reader.ReadStringOrNull(true)
      };
    }
  }
}
