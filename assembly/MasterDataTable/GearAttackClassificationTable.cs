﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.GearAttackClassificationTable
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class GearAttackClassificationTable
  {
    public int ID;
    public string name;
    public int skill_BattleskillSkill;

    public static GearAttackClassificationTable Parse(
      MasterDataReader reader)
    {
      return new GearAttackClassificationTable()
      {
        ID = reader.ReadInt(),
        name = reader.ReadString(true),
        skill_BattleskillSkill = reader.ReadInt()
      };
    }

    public BattleskillSkill skill
    {
      get
      {
        BattleskillSkill battleskillSkill;
        if (!MasterData.BattleskillSkill.TryGetValue(this.skill_BattleskillSkill, out battleskillSkill))
          Debug.LogError((object) ("Key not Found: MasterData.BattleskillSkill[" + (object) this.skill_BattleskillSkill + "]"));
        return battleskillSkill;
      }
    }
  }
}
