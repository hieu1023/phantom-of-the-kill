﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.DateScriptParts
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class DateScriptParts
  {
    public int ID;
    public int unitID;
    public string scriptID;
    public string script;

    public static DateScriptParts Parse(MasterDataReader reader)
    {
      return new DateScriptParts()
      {
        ID = reader.ReadInt(),
        unitID = reader.ReadInt(),
        scriptID = reader.ReadString(true),
        script = reader.ReadString(true)
      };
    }
  }
}
