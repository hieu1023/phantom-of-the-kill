﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.QuestCharacterMReleaseCondition
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class QuestCharacterMReleaseCondition
  {
    public int ID;
    public int quest_m_QuestCharacterM;
    public string required_condition;
    public int? required_quest_type;
    public int? required_quest_s_id;

    public static QuestCharacterMReleaseCondition Parse(
      MasterDataReader reader)
    {
      return new QuestCharacterMReleaseCondition()
      {
        ID = reader.ReadInt(),
        quest_m_QuestCharacterM = reader.ReadInt(),
        required_condition = reader.ReadStringOrNull(true),
        required_quest_type = reader.ReadIntOrNull(),
        required_quest_s_id = reader.ReadIntOrNull()
      };
    }

    public QuestCharacterM quest_m
    {
      get
      {
        QuestCharacterM questCharacterM;
        if (!MasterData.QuestCharacterM.TryGetValue(this.quest_m_QuestCharacterM, out questCharacterM))
          Debug.LogError((object) ("Key not Found: MasterData.QuestCharacterM[" + (object) this.quest_m_QuestCharacterM + "]"));
        return questCharacterM;
      }
    }
  }
}
