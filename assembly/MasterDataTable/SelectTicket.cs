﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.SelectTicket
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class SelectTicket
  {
    public int ID;
    public string name;
    public int category_id_SelectTicketCategory;
    public int picID;
    public int packID;
    public string short_name;
    public int priority;

    public static SelectTicket Parse(MasterDataReader reader)
    {
      return new SelectTicket()
      {
        ID = reader.ReadInt(),
        name = reader.ReadString(true),
        category_id_SelectTicketCategory = reader.ReadInt(),
        picID = reader.ReadInt(),
        packID = reader.ReadInt(),
        short_name = reader.ReadString(true),
        priority = reader.ReadInt()
      };
    }

    public SelectTicketCategory category_id
    {
      get
      {
        SelectTicketCategory selectTicketCategory;
        if (!MasterData.SelectTicketCategory.TryGetValue(this.category_id_SelectTicketCategory, out selectTicketCategory))
          Debug.LogError((object) ("Key not Found: MasterData.SelectTicketCategory[" + (object) this.category_id_SelectTicketCategory + "]"));
        return selectTicketCategory;
      }
    }
  }
}
