﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.GuildRaidPeriod
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class GuildRaidPeriod
  {
    public int ID;
    public string period_name;
    public DateTime? start_at;
    public DateTime? end_at;
    public int banner_id;
    public string bg_path;

    public static GuildRaidPeriod Parse(MasterDataReader reader)
    {
      return new GuildRaidPeriod()
      {
        ID = reader.ReadInt(),
        period_name = reader.ReadStringOrNull(true),
        start_at = reader.ReadDateTimeOrNull(),
        end_at = reader.ReadDateTimeOrNull(),
        banner_id = reader.ReadInt(),
        bg_path = reader.ReadStringOrNull(true)
      };
    }
  }
}
