﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.BattleSpecialSkill
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class BattleSpecialSkill
  {
    public int ID;
    public int skill;

    public static BattleSpecialSkill Parse(MasterDataReader reader)
    {
      return new BattleSpecialSkill()
      {
        ID = reader.ReadInt(),
        skill = reader.ReadInt()
      };
    }
  }
}
