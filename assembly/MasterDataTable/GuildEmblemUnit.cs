﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.GuildEmblemUnit
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class GuildEmblemUnit
  {
    public int ID;
    public string name;
    public string description;
    public int rarity_GuildEmblemRarity;

    public static GuildEmblemUnit Parse(MasterDataReader reader)
    {
      return new GuildEmblemUnit()
      {
        ID = reader.ReadInt(),
        name = reader.ReadString(true),
        description = reader.ReadString(true),
        rarity_GuildEmblemRarity = reader.ReadInt()
      };
    }

    public GuildEmblemRarity rarity
    {
      get
      {
        GuildEmblemRarity guildEmblemRarity;
        if (!MasterData.GuildEmblemRarity.TryGetValue(this.rarity_GuildEmblemRarity, out guildEmblemRarity))
          Debug.LogError((object) ("Key not Found: MasterData.GuildEmblemRarity[" + (object) this.rarity_GuildEmblemRarity + "]"));
        return guildEmblemRarity;
      }
    }
  }
}
