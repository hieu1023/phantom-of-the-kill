﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.CommonPayType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

namespace MasterDataTable
{
  public enum CommonPayType
  {
    coin = 1,
    money = 2,
    medal = 3,
    friend_point = 4,
    gacha_ticket = 5,
    battle_medal = 6,
    currency = 7,
    tower_medal = 8,
    paid_coin = 9,
    guild_medal = 10, // 0x0000000A
    raid_medal = 11, // 0x0000000B
  }
}
