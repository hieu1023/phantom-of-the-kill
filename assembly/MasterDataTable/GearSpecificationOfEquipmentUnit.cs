﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.GearSpecificationOfEquipmentUnit
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class GearSpecificationOfEquipmentUnit
  {
    public int ID;
    public int group_id;
    public int unit_id;
    public int? job_id;

    public static GearSpecificationOfEquipmentUnit Parse(
      MasterDataReader reader)
    {
      return new GearSpecificationOfEquipmentUnit()
      {
        ID = reader.ReadInt(),
        group_id = reader.ReadInt(),
        unit_id = reader.ReadInt(),
        job_id = reader.ReadIntOrNull()
      };
    }
  }
}
