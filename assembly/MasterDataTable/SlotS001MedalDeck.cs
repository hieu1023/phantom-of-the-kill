﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.SlotS001MedalDeck
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class SlotS001MedalDeck
  {
    public int ID;
    public string name;
    public int result_icon;
    public string description;
    public int prize_first;
    public int prize_second;
    public int prize_third;

    public static SlotS001MedalDeck Parse(MasterDataReader reader)
    {
      return new SlotS001MedalDeck()
      {
        ID = reader.ReadInt(),
        name = reader.ReadString(true),
        result_icon = reader.ReadInt(),
        description = reader.ReadString(true),
        prize_first = reader.ReadInt(),
        prize_second = reader.ReadInt(),
        prize_third = reader.ReadInt()
      };
    }
  }
}
