﻿// Decompiled with JetBrains decompiler
// Type: Unit0543Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System.Collections;
using UnityEngine;

public class Unit0543Scene : NGSceneBase
{
  [SerializeField]
  private Unit0543Menu menu;

  public static void changeScene(bool stack, PlayerUnit unit)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("unit054_3", (stack ? 1 : 0) != 0, (object) unit);
  }

  public override IEnumerator onInitSceneAsync()
  {
    Unit0543Scene unit0543Scene = this;
    Future<GameObject> bgF = Res.Prefabs.BackGround.UnitBackground_p0_60.Load<GameObject>();
    IEnumerator e = bgF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    unit0543Scene.backgroundPrefab = bgF.Result;
  }

  public IEnumerator onStartSceneAsync(PlayerUnit unit)
  {
    IEnumerator e = this.menu.Init(unit.unit, false, unit.GetElement());
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }
}
