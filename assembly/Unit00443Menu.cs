﻿// Decompiled with JetBrains decompiler
// Type: Unit00443Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Unit00443Menu : EquipmentDetailMenuBase
{
  protected Unit004431Menu.Param sendParam = new Unit004431Menu.Param();
  [SerializeField]
  public UI2DSprite DynWeaponIll;
  [SerializeField]
  protected UILabel TxtTitle;
  [SerializeField]
  protected Transform DynWeaponModel;
  [SerializeField]
  protected GameObject charaThum;
  [SerializeField]
  protected GameObject DirAddStauts;
  [SerializeField]
  private GameObject remainingManaSeedContainer;
  [SerializeField]
  private UILabel remainingManaSeedLabel;
  public UIButton nowFavorite;
  public UIButton yetFavorite;
  public Transform TopStarPos;
  public NGHorizontalScrollParts indicator;
  public Unit00443indicator indicatorPage1;
  public Unit00443indicatorDirection indicatorPage2;
  public UIGrid grid;
  public GameCore.ItemInfo RetentionGear;
  public UIWidget ZoomBuguSprite;
  [SerializeField]
  public UI2DSprite rarityStarsIcon;
  [SerializeField]
  protected GameObject DirReisou;
  [SerializeField]
  protected UILabel TxtReisouRank;
  [SerializeField]
  protected GameObject DirReisouExpGauge;
  [SerializeField]
  protected UISprite SlcReisouGauge;
  [SerializeField]
  protected UIButton BtnReisou;
  [SerializeField]
  protected GameObject DynReisouIcon;
  [SerializeField]
  protected GameObject SlcAddReisou;
  protected GameCore.ItemInfo reisouInfo;
  protected ItemIcon reisouIcon;
  protected GameObject itemIconPrefab;
  protected GameObject reisouPopupPrefab;
  protected bool is_for_reisou;
  private int m_windowHeight;
  private int m_windowWidth;

  protected void SetTitleText(string gearName)
  {
    this.TxtTitle.gameObject.SetActive(true);
    this.TxtTitle.SetText(gearName);
  }

  public IEnumerator Init(GameCore.ItemInfo targetGear, bool limited = false, bool is_for_reisou = false)
  {
    Unit00443Menu unit00443Menu = this;
    PlayerUnit[] playerUnitArray = SMManager.Get<PlayerUnit[]>();
    PlayerUnit equiptargets = (PlayerUnit) null;
    foreach (PlayerUnit playerUnit in playerUnitArray)
    {
      if (((IEnumerable<int?>) playerUnit.equip_gear_ids).Contains<int?>(new int?(targetGear.itemID)))
      {
        equiptargets = playerUnit;
        break;
      }
    }
    foreach (Component component in unit00443Menu.charaThum.transform)
      UnityEngine.Object.Destroy((UnityEngine.Object) component.gameObject);
    IEnumerator e = unit00443Menu.SetIncrementalParameter(targetGear, unit00443Menu.DirAddStauts);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Future<GameObject> iconPrefabF = Res.Prefabs.UnitIcon.normal.Load<GameObject>();
    e = iconPrefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    UnitIcon uniticon = iconPrefabF.Result.Clone(unit00443Menu.charaThum.transform).GetComponent<UnitIcon>();
    if (equiptargets == (PlayerUnit) null)
    {
      uniticon.SetEmpty();
      if (!targetGear.broken)
      {
        uniticon.SelectUnit = true;
        uniticon.onClick = (System.Action<UnitIconBase>) (_ => this.choiceUnitChangeScene());
      }
      else
        uniticon.onClick = (System.Action<UnitIconBase>) (_ => {});
    }
    else
    {
      e = uniticon.SetUnit(((IEnumerable<PlayerUnit>) SMManager.Get<PlayerUnit[]>()).FirstOrDefault<PlayerUnit>((Func<PlayerUnit, bool>) (unit => unit.unit.ID == equiptargets.unit.ID)), equiptargets.GetElement(), false);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      uniticon.setBottom(equiptargets);
      uniticon.ShowBottomInfo(UnitSortAndFilter.SORT_TYPES.Level);
      uniticon.setLevelText(equiptargets);
      uniticon.princessType.SetPrincessType(equiptargets);
      uniticon.onClick = (System.Action<UnitIconBase>) (_ => this.choiceUnitChangeScene());
      uniticon.SelectUnit = false;
    }
    if (limited)
    {
      uniticon.SelectUnit = false;
      uniticon.onClick = (System.Action<UnitIconBase>) (_ => {});
    }
    e = unit00443Menu.indicatorPage1.LoadPrefabs();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if ((UnityEngine.Object) unit00443Menu.itemIconPrefab == (UnityEngine.Object) null)
    {
      Future<GameObject> ItemIconF = Res.Prefabs.ItemIcon.prefab.Load<GameObject>();
      e = ItemIconF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      unit00443Menu.itemIconPrefab = ItemIconF.Result;
      ItemIconF = (Future<GameObject>) null;
    }
    unit00443Menu.RetentionGear = targetGear;
    unit00443Menu.indicatorPage1.Init(targetGear);
    e = unit00443Menu.indicatorPage2.Init(targetGear);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    unit00443Menu.indicator.resetScrollView();
    unit00443Menu.indicator.setItemPositionQuick(0);
    unit00443Menu.sendParam.gearId = targetGear.itemID;
    unit00443Menu.sendParam.gearKindId = targetGear.gear.kind_GearKind;
    Future<UnityEngine.Sprite> spriteF = targetGear.gear.LoadSpriteBasic(1f);
    e = spriteF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    unit00443Menu.DynWeaponIll.sprite2D = spriteF.Result;
    unit00443Menu.DynWeaponIll.width = Mathf.FloorToInt(spriteF.Result.textureRect.width);
    unit00443Menu.DynWeaponIll.height = Mathf.FloorToInt(spriteF.Result.textureRect.height);
    unit00443Menu.DynWeaponIll.transform.localScale = (Vector3) new Vector2(0.8f, 0.8f);
    unit00443Menu.SetTitleText(targetGear.name);
    RarityIcon.SetRarity(targetGear.gear, unit00443Menu.rarityStarsIcon);
    unit00443Menu.nowFavorite.gameObject.SetActive(targetGear.favorite);
    unit00443Menu.yetFavorite.gameObject.SetActive(!targetGear.favorite);
    if (unit00443Menu.ZoomBuguSprite.transform.childCount > 0)
    {
      Transform child = unit00443Menu.ZoomBuguSprite.transform.GetChild(0);
      e = unit00443Menu.setTexture(targetGear.gear.LoadSpriteBasic(1f), child.GetComponent<UI2DSprite>());
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    unit00443Menu.StartCoroutine(unit00443Menu.WaitScrollSe());
    if ((UnityEngine.Object) unit00443Menu.remainingManaSeedContainer != (UnityEngine.Object) null)
      unit00443Menu.remainingManaSeedContainer.SetActive(false);
    if (targetGear.gear != null && targetGear.gear.kind.Enum == GearKindEnum.accessories && targetGear.gear.disappearance_type_GearDisappearanceType == 1)
    {
      unit00443Menu.remainingManaSeedContainer.SetActive(true);
      unit00443Menu.remainingManaSeedLabel.SetTextLocalize(targetGear.gearAccessoryRemainingAmount);
    }
    if (Singleton<NGGameDataManager>.GetInstance().IsEarth)
    {
      if ((UnityEngine.Object) unit00443Menu.DirReisou != (UnityEngine.Object) null)
        unit00443Menu.DirReisou.SetActive(false);
    }
    else
    {
      unit00443Menu.reisouInfo = (GameCore.ItemInfo) null;
      if (targetGear.reisou != (PlayerItem) null)
        unit00443Menu.reisouInfo = new GameCore.ItemInfo(targetGear.reisou);
      foreach (Component component in unit00443Menu.DynReisouIcon.transform)
        UnityEngine.Object.Destroy((UnityEngine.Object) component.gameObject);
      if ((UnityEngine.Object) unit00443Menu.reisouIcon != (UnityEngine.Object) null)
        UnityEngine.Object.Destroy((UnityEngine.Object) unit00443Menu.reisouIcon);
      if (unit00443Menu.reisouInfo == null)
      {
        unit00443Menu.DirReisou.SetActive(true);
        unit00443Menu.BtnReisou.gameObject.SetActive(true);
        unit00443Menu.TxtReisouRank.gameObject.SetActive(false);
        unit00443Menu.DirReisouExpGauge.SetActive(false);
        if (is_for_reisou)
        {
          unit00443Menu.BtnReisou.isEnabled = false;
          unit00443Menu.SlcAddReisou.SetActive(false);
        }
        else
        {
          unit00443Menu.BtnReisou.isEnabled = true;
          unit00443Menu.SlcAddReisou.SetActive(true);
        }
      }
      else
      {
        unit00443Menu.DirReisou.SetActive(true);
        unit00443Menu.BtnReisou.gameObject.SetActive(false);
        unit00443Menu.TxtReisouRank.gameObject.SetActive(true);
        unit00443Menu.DirReisouExpGauge.SetActive(true);
        unit00443Menu.TxtReisouRank.SetTextLocalize(Consts.GetInstance().UNIT_00443_REISOU_RANK.F((object) unit00443Menu.reisouInfo.gearLevel, (object) unit00443Menu.reisouInfo.gearLevelLimit));
        unit00443Menu.reisouIcon = unit00443Menu.itemIconPrefab.CloneAndGetComponent<ItemIcon>(unit00443Menu.DynReisouIcon.transform);
        e = unit00443Menu.reisouIcon.InitByItemInfo(unit00443Menu.reisouInfo);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        unit00443Menu.reisouIcon.setEquipReisouDisp();
        unit00443Menu.reisouIcon.onClick = !is_for_reisou ? (System.Action<ItemIcon>) (x => this.SelectReisou(x.ItemInfo)) : (System.Action<ItemIcon>) (x => this.OpenReisouDetailPopup(this.reisouInfo));
        unit00443Menu.reisouIcon.EnableLongPressEvent(new System.Action<GameCore.ItemInfo>(unit00443Menu.OpenReisouDetailPopup));
        float num = (float) unit00443Menu.SlcReisouGauge.width * ((float) unit00443Menu.reisouInfo.gearExp / (float) (unit00443Menu.reisouInfo.gearExpNext + unit00443Menu.reisouInfo.gearExp));
        if ((double) num == 0.0 || unit00443Menu.reisouInfo.gearExpNext + unit00443Menu.reisouInfo.gearExp == 0)
        {
          unit00443Menu.SlcReisouGauge.gameObject.SetActive(false);
        }
        else
        {
          unit00443Menu.SlcReisouGauge.gameObject.SetActive(true);
          unit00443Menu.SlcReisouGauge.width = (int) num;
        }
      }
    }
  }

  protected void SelectReisou(GameCore.ItemInfo item)
  {
    Unit0044ReisouScene.ChangeScene(true, this.RetentionGear, this.reisouInfo);
  }

  protected void OpenReisouDetailPopup(GameCore.ItemInfo item)
  {
    this.StartCoroutine(this.OpenReisouDetailPopupAsync(item));
  }

  protected IEnumerator OpenReisouDetailPopupAsync(GameCore.ItemInfo item)
  {
    if (item != null)
    {
      IEnumerator e;
      if ((UnityEngine.Object) this.reisouPopupPrefab == (UnityEngine.Object) null)
      {
        Future<GameObject> popupPrefabF = new ResourceObject("Prefabs/UnitGUIs/PopupReisouSkillDetails").Load<GameObject>();
        e = popupPrefabF.Wait();
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        this.reisouPopupPrefab = popupPrefabF.Result;
        popupPrefabF = (Future<GameObject>) null;
      }
      GameObject popup = this.reisouPopupPrefab.Clone((Transform) null);
      PopupReisouDetails script = popup.GetComponent<PopupReisouDetails>();
      popup.SetActive(false);
      e = script.Init(item, (PlayerItem) null, true);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      popup.SetActive(true);
      Singleton<PopupManager>.GetInstance().open(popup, false, false, true, true, false, false, "SE_1006");
      yield return (object) null;
      script.scrollResetPosition();
    }
  }

  protected IEnumerator setTexture(Future<UnityEngine.Sprite> src, UI2DSprite to)
  {
    return src.Then<UnityEngine.Sprite>((Func<UnityEngine.Sprite, UnityEngine.Sprite>) (sprite => to.sprite2D = sprite)).Wait();
  }

  public void changeFavorite()
  {
    this.yetFavorite.gameObject.SetActive(this.nowFavorite.gameObject.activeSelf);
    this.nowFavorite.gameObject.SetActive(!this.yetFavorite.gameObject.activeSelf);
  }

  public virtual IEnumerator FavoriteAPI()
  {
    List<int> intList1 = new List<int>();
    List<int> intList2 = new List<int>();
    foreach (PlayerItem playerItem in SMManager.Get<PlayerItem[]>())
    {
      if (playerItem.gear != null)
      {
        if (playerItem.id == this.RetentionGear.itemID)
        {
          if (this.nowFavorite.gameObject.activeSelf)
            intList1.Add(playerItem.id);
          else
            intList2.Add(playerItem.id);
        }
        else if (playerItem.favorite)
          intList1.Add(playerItem.id);
        else
          intList2.Add(playerItem.id);
      }
    }
    IEnumerator f = WebAPI.ItemGearFavorite(intList1.ToArray(), intList2.ToArray(), (System.Action<WebAPI.Response.UserError>) (error => WebAPI.DefaultUserErrorCallback(error))).Wait();
    while (f.MoveNext())
      yield return f.Current;
    f = (IEnumerator) null;
  }

  public virtual void choiceUnitChangeScene()
  {
    Unit00468Scene.changeScene004431(true, this.sendParam);
  }

  protected IEnumerator WaitScrollSe()
  {
    yield return (object) new WaitForSeconds(0.3f);
    this.indicator.SeEnable = true;
  }

  public virtual void EndScene()
  {
    foreach (Component component in this.charaThum.transform)
      UnityEngine.Object.Destroy((UnityEngine.Object) component.gameObject);
  }

  public virtual void IbtnBack()
  {
    if (this.IsPushAndSet())
      return;
    this.backScene();
  }

  public override void onBackButton()
  {
    this.IbtnBack();
  }

  public virtual void IbtnFavoriteOff()
  {
  }

  public virtual void IbtnFavoriteOn()
  {
  }

  public virtual void IbtnZoom()
  {
    Unit00446Scene.changeScene(true, this.RetentionGear.gear);
  }

  public void IbtnReisou()
  {
    if (this.IsPushAndSet())
      return;
    Unit0044ReisouScene.ChangeScene(true, this.RetentionGear, this.reisouInfo);
  }

  protected override void Update()
  {
    if (this.m_windowHeight == 0 || this.m_windowWidth == 0)
    {
      this.m_windowHeight = Screen.height;
      this.m_windowWidth = Screen.width;
    }
    else if (this.m_windowHeight != Screen.height || this.m_windowWidth != Screen.width)
    {
      Debug.Log((object) "Window size change detected.");
      this.StartCoroutine(this.indicatorPage2.Init(this.RetentionGear));
      this.m_windowHeight = Screen.height;
      this.m_windowWidth = Screen.width;
    }
    base.Update();
  }
}
