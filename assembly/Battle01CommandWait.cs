﻿// Decompiled with JetBrains decompiler
// Type: Battle01CommandWait
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

public class Battle01CommandWait : BattleMonoBehaviour, IButtonEnableBeheviour
{
  private BattleUIController controller;
  private UIButton button;

  public bool buttonEnable
  {
    set
    {
      this.button.isEnabled = value;
    }
  }

  public override IEnumerator onInitAsync()
  {
    // ISSUE: reference to a compiler-generated field
    int num = this.\u003C\u003E1__state;
    Battle01CommandWait battle01CommandWait = this;
    if (num != 0)
      return false;
    // ISSUE: reference to a compiler-generated field
    this.\u003C\u003E1__state = -1;
    battle01CommandWait.button = battle01CommandWait.GetComponent<UIButton>();
    EventDelegate.Set(battle01CommandWait.button.onClick, new EventDelegate((MonoBehaviour) battle01CommandWait, "onClick"));
    battle01CommandWait.controller = battle01CommandWait.battleManager.getManager<NGBattleUIManager>().controller;
    battle01CommandWait.controller.setButtonBehaviour((IButtonEnableBeheviour) battle01CommandWait);
    return false;
  }

  public void onClick()
  {
    if (!this.battleManager.isBattleEnable || this.battleManager.getController<BattleStateController>().isWaitCurrentAIActionCancel)
      return;
    this.controller.uiWait();
  }
}
