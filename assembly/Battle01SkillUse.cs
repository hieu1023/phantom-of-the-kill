﻿// Decompiled with JetBrains decompiler
// Type: Battle01SkillUse
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnitStatusInformation;
using UnityEngine;

public class Battle01SkillUse : NGBattleMenuBase
{
  [SerializeField]
  private UI2DSprite icon;
  [SerializeField]
  private UI2DSprite property1;
  [SerializeField]
  private UI2DSprite property2;
  [SerializeField]
  private UILabel txt_name;
  [SerializeField]
  private UILabel txt_description;
  [SerializeField]
  private UILabel txt_consume_hp;
  [SerializeField]
  private Battle01CommandSkillUse button;
  [SerializeField]
  private GameObject title_skill;
  [SerializeField]
  private GameObject title_secrets;
  [SerializeField]
  private GameObject notAvailable;
  [SerializeField]
  private UILabel notAvailableTxt;
  public GameObject typeIconPrefab;
  public GameObject targetIconPrefab;
  private BattleSkillIcon skillIcon;
  private SkillGenreIcon property1Icon;
  private SkillGenreIcon property2Icon;
  private GameObject skillDetailPrefab;
  private BL.BattleModified<BL.Skill> modified;
  private BL.Unit unit;
  private int targetsCount;
  private int panelsCount;
  private int hpCost;
  private int hpMust;
  private bool isSelectPanel;

  private T clonePrefab<T>(GameObject prefab, UI2DSprite parent) where T : IconPrefabBase
  {
    parent.enabled = false;
    T component = prefab.CloneAndGetComponent<T>(parent.transform);
    if ((UnityEngine.Object) component == (UnityEngine.Object) null)
      return default (T);
    NGUITools.AdjustDepth(component.gameObject, parent.depth);
    component.SetBasedOnHeight(parent.height);
    return component;
  }

  protected override IEnumerator Start_Original()
  {
    this.skillIcon = this.clonePrefab<BattleSkillIcon>(this.typeIconPrefab, this.icon);
    this.property1Icon = this.clonePrefab<SkillGenreIcon>(this.targetIconPrefab, this.property1);
    this.property2Icon = this.clonePrefab<SkillGenreIcon>(this.targetIconPrefab, this.property2);
    Future<GameObject> loader = PopupSkillDetails.createPrefabLoader(Singleton<NGGameDataManager>.GetInstance().IsSea);
    yield return (object) loader.Wait();
    this.skillDetailPrefab = loader.Result;
  }

  protected override void Update_Battle()
  {
    if (this.modified == null || !this.modified.isChangedOnce())
      return;
    BL.Skill skill = this.modified.value;
    this.StartCoroutine(this.skillIcon.Init(skill.skill));
    this.property1Icon.Init(skill.genre1);
    this.property2Icon.Init(skill.genre2);
    this.setText(this.txt_name, skill.name);
    this.setText(this.txt_description, skill.description);
    if ((UnityEngine.Object) this.txt_consume_hp != (UnityEngine.Object) null)
    {
      if (this.hpCost > 0)
      {
        this.txt_consume_hp.gameObject.SetActive(true);
        string v = Consts.Format(Consts.GetInstance().BATTLE_UI_CONSUME_HP, (IDictionary) new Hashtable()
        {
          {
            (object) "hp",
            (object) this.hpCost
          }
        });
        if (this.hpCost < this.unit.hp)
          this.setText(this.txt_consume_hp, v);
        else
          this.setText(this.txt_consume_hp, "[ff0000]" + v);
      }
      else
        this.txt_consume_hp.gameObject.SetActive(false);
    }
    if (!((UnityEngine.Object) this.notAvailable != (UnityEngine.Object) null))
      return;
    bool flag1 = this.hpMust < this.unit.hp;
    bool flag2 = this.targetsCount > 0 || this.panelsCount > 0;
    if (flag1 & flag2)
    {
      this.notAvailable.SetActive(false);
      this.button.setEnable(true);
    }
    else
    {
      this.notAvailable.SetActive(true);
      this.button.setEnable(false);
      if (!flag1)
        this.notAvailableTxt.SetTextLocalize(Consts.GetInstance().BATTLE_UI_NOT_ENOUGH_HP);
      else if (!flag2)
      {
        if (!this.isSelectPanel)
          this.notAvailableTxt.SetTextLocalize(Consts.GetInstance().BATTLE_UI_NOT_ENOUGH_TARGET);
        else
          this.notAvailableTxt.SetTextLocalize(Consts.GetInstance().BATTLE_UI_NOT_ENOUGH_PANEL);
      }
      else
        this.notAvailableTxt.SetTextLocalize(string.Empty);
    }
  }

  public void setSkillTargets(
    BL.Unit unit,
    BL.Skill skill,
    List<BL.Unit> targets,
    List<BL.Panel> panels,
    bool isSelectPanel)
  {
    this.modified = BL.Observe<BL.Skill>(skill);
    this.unit = unit;
    if (skill.isOugi)
    {
      this.title_skill.SetActive(false);
      this.title_secrets.SetActive(true);
    }
    else
    {
      this.title_skill.SetActive(true);
      this.title_secrets.SetActive(false);
    }
    this.isSelectPanel = isSelectPanel;
    Tuple<int, int> hpCost = skill.getHpCost(unit);
    this.hpCost = hpCost.Item1;
    this.hpMust = hpCost.Item2;
    if (unit.hp <= this.hpMust)
    {
      targets = new List<BL.Unit>();
      panels = new List<BL.Panel>();
    }
    List<BL.Unit> list1 = targets.Where<BL.Unit>((Func<BL.Unit, bool>) (x => x.skillEffects.CanUseSkill(skill.skill, skill.level, (BL.ISkillEffectListUnit) x, this.env.core, (BL.ISkillEffectListUnit) unit) == 0)).ToList<BL.Unit>();
    BattleskillEffect battleskillEffect = ((IEnumerable<BattleskillEffect>) skill.skill.Effects).FirstOrDefault<BattleskillEffect>((Func<BattleskillEffect, bool>) (x => x.EffectLogic.Enum == BattleskillEffectLogicEnum.random_choice && x.checkLevel(skill.level)));
    if (battleskillEffect != null && list1.Count < battleskillEffect.GetInt(BattleskillEffectLogicArgumentEnum.min_unit_count))
      list1.Clear();
    this.targetsCount = list1.Count;
    List<BL.Unit> list2 = targets.Where<BL.Unit>((Func<BL.Unit, bool>) (x => x.skillEffects.CanUseSkill(skill.skill, skill.level, (BL.ISkillEffectListUnit) x, this.env.core, (BL.ISkillEffectListUnit) unit) == 1)).ToList<BL.Unit>();
    List<BL.Panel> list3 = panels.Where<BL.Panel>((Func<BL.Panel, bool>) (x => BattleFuncs.canUseSkillToPanel(skill.skill, skill.level, x, (BL.ISkillEffectListUnit) unit))).ToList<BL.Panel>();
    this.panelsCount = list3.Count<BL.Panel>();
    this.button.setData(unit, skill, list1, list3);
    List<BL.Unit> list4 = list1.Concat<BL.Unit>((IEnumerable<BL.Unit>) list2).Distinct<BL.Unit>().ToList<BL.Unit>();
    List<BL.Unit> attackTargets;
    List<BL.Unit> healTargets;
    if (skill.targetType == BattleskillTargetType.complex_range || skill.targetType == BattleskillTargetType.complex_single)
    {
      BL.ForceID forceId = this.env.core.getForceID(unit);
      attackTargets = new List<BL.Unit>();
      healTargets = new List<BL.Unit>();
      foreach (BL.Unit unit1 in list4)
      {
        if (this.env.core.getForceID(unit1) == forceId)
          healTargets.Add(unit1);
        else
          attackTargets.Add(unit1);
      }
    }
    else if (skill.isOwn)
    {
      attackTargets = new List<BL.Unit>();
      healTargets = list4;
    }
    else
    {
      attackTargets = list4;
      healTargets = new List<BL.Unit>();
    }
    Singleton<NGBattleManager>.GetInstance().getController<BattleInputObserver>().setTargetSelectMode(attackTargets, healTargets, list2, list3, (System.Action<BL.Unit, BL.Panel>) ((u, p) =>
    {
      Battle01CommandSkillUse[] componentsInChildren = this.GetComponentsInChildren<Battle01CommandSkillUse>();
      if (componentsInChildren.Length == 0)
        return;
      componentsInChildren[0].onClick();
    }));
  }

  public void onClickedZoom()
  {
    BL.Skill skill;
    if ((UnityEngine.Object) this.skillDetailPrefab == (UnityEngine.Object) null || this.modified == null || (skill = this.modified.value) == null)
      return;
    PopupSkillDetails.show(this.skillDetailPrefab, new PopupSkillDetails.Param(skill.skill, UnitParameter.SkillGroup.Command, new int?(skill.level)), false, (System.Action) null, true);
  }
}
