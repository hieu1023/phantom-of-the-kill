﻿// Decompiled with JetBrains decompiler
// Type: Shop00771Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shop00771Menu : BackButtonMenuBase
{
  private GameObject linkTarget;
  private GameObject prefab0078;
  [SerializeField]
  private GameObject linkParent;
  [SerializeField]
  private UIButton ibtnY;
  [SerializeField]
  private UIButton ibtnN;
  [SerializeField]
  private UILabel TxtDescription01;
  [SerializeField]
  private UILabel TxtDescription02;
  [SerializeField]
  private UILabel TxtDescription03;
  [SerializeField]
  private UILabel TxtDescription04;
  [SerializeField]
  private UILabel TxtDescription05;
  public int _itemId;
  public string _itemName;
  public int _purchasedQuantity;
  public int _playerQuantity;
  public int _price;
  public long _holding;
  private System.Action<long> _onPurchasedHolding;
  private Func<IEnumerator> _onPurchased;
  public long holding_next;
  public UnityEngine.Sprite[] backSprite;

  public PlayerShopArticle _playerShopArticle { get; set; }

  public List<Shop0074Scroll> _scrolls { get; set; }

  public Player _player { get; set; }

  public void InitData(
    PlayerShopArticle playerShopArticle,
    int itemId,
    string itemName,
    int purchasedQuantity,
    int playerQuantity,
    int price,
    long holding,
    List<Shop0074Scroll> scrolls,
    PlayerShopArticle[] articles,
    Func<IEnumerator> onPurchased,
    System.Action<long> onPurcheasedHolding)
  {
    this._playerShopArticle = playerShopArticle;
    this._itemId = itemId;
    this._itemName = itemName;
    this._purchasedQuantity = purchasedQuantity;
    this._playerQuantity = playerQuantity;
    this._price = price;
    this._holding = holding;
    this._scrolls = scrolls;
    this._onPurchasedHolding = onPurcheasedHolding;
    this._onPurchased = onPurchased;
    this.CalcBalance();
    this.TxtDescription01.SetTextLocalize(this._itemName);
    this.SetTxtDescription02(this._playerShopArticle);
    this.SetTxtDescription03(this._playerShopArticle);
    this.SetTxtDescription04(this._playerShopArticle);
    this.SetTxtDescription05(this._playerShopArticle);
  }

  public void InitObject(GameObject obj)
  {
    this.linkTarget = obj;
    obj.Clone(this.linkParent.transform);
  }

  private void SetTxtDescription02(PlayerShopArticle psa)
  {
    if (psa.article.pay_type == CommonPayType.money)
      this.TxtDescription02.SetTextLocalize(Consts.Format(Consts.GetInstance().SHOP_00771_TXT_DESCRIPTION02_MONEY, (IDictionary) new Hashtable()
      {
        {
          (object) "money",
          (object) this._holding.ToLocalizeNumberText()
        },
        {
          (object) "moneyNext",
          (object) this.holding_next.ToLocalizeNumberText()
        }
      }));
    else if (psa.article.pay_type == CommonPayType.medal)
      this.TxtDescription02.SetTextLocalize(Consts.Format(Consts.GetInstance().SHOP_00771_TXT_DESCRIPTION02_MEDAL, (IDictionary) new Hashtable()
      {
        {
          (object) "medal",
          (object) this._holding.ToLocalizeNumberText()
        },
        {
          (object) "medalNext",
          (object) this.holding_next.ToLocalizeNumberText()
        }
      }));
    else if (psa.article.pay_type == CommonPayType.battle_medal)
      this.TxtDescription02.SetTextLocalize(Consts.Format(Consts.GetInstance().SHOP_00771_TXT_DESCRIPTION02_MEDAL, (IDictionary) new Hashtable()
      {
        {
          (object) "medal",
          (object) this._holding.ToLocalizeNumberText()
        },
        {
          (object) "medalNext",
          (object) this.holding_next.ToLocalizeNumberText()
        }
      }));
    else if (psa.article.pay_type == CommonPayType.tower_medal)
      this.TxtDescription02.SetTextLocalize(Consts.Format(Consts.GetInstance().SHOP_00771_TXT_DESCRIPTION02_xMEDALxUNIT, (IDictionary) new Hashtable()
      {
        {
          (object) "medal",
          (object) this._holding.ToLocalizeNumberText()
        },
        {
          (object) "medalNext",
          (object) this.holding_next.ToLocalizeNumberText()
        },
        {
          (object) "unit",
          (object) Consts.GetInstance().SHOP_TOWERMEDAL_UNIT
        }
      }));
    else if (psa.article.pay_type == CommonPayType.guild_medal)
      this.TxtDescription02.SetTextLocalize(Consts.Format(Consts.GetInstance().SHOP_00771_TXT_DESCRIPTION02_MEDAL, (IDictionary) new Hashtable()
      {
        {
          (object) "medal",
          (object) this._holding.ToLocalizeNumberText()
        },
        {
          (object) "medalNext",
          (object) this.holding_next.ToLocalizeNumberText()
        }
      }));
    else if (psa.article.pay_type == CommonPayType.raid_medal)
      this.TxtDescription02.SetTextLocalize(Consts.Format(Consts.GetInstance().SHOP_00771_TXT_DESCRIPTION02_MEDAL, (IDictionary) new Hashtable()
      {
        {
          (object) "medal",
          (object) this._holding.ToLocalizeNumberText()
        },
        {
          (object) "medalNext",
          (object) this.holding_next.ToLocalizeNumberText()
        }
      }));
    else
      Debug.LogWarning((object) "selected pay_type is other than money, medal");
  }

  public void SetTxtDescription03(PlayerShopArticle psa)
  {
    if (psa.article.pay_type == CommonPayType.money)
      this.TxtDescription03.SetTextLocalize(Consts.Format(Consts.GetInstance().SHOP_007X_TXT_DESCRIPTION03_MONEY, (IDictionary) new Hashtable()
      {
        {
          (object) "money",
          (object) this._price
        }
      }));
    else if (psa.article.pay_type == CommonPayType.medal)
      this.TxtDescription03.SetTextLocalize(Consts.Format(Consts.GetInstance().SHOP_007X_TXT_DESCRIPTION03_MEDAL, (IDictionary) new Hashtable()
      {
        {
          (object) "medal",
          (object) this._price
        }
      }));
    else if (psa.article.pay_type == CommonPayType.battle_medal)
      this.TxtDescription03.SetTextLocalize(Consts.Format(Consts.GetInstance().SHOP_007X_TXT_DESCRIPTION03_MEDAL, (IDictionary) new Hashtable()
      {
        {
          (object) "medal",
          (object) this._price
        }
      }));
    else if (psa.article.pay_type == CommonPayType.tower_medal)
      this.TxtDescription03.SetTextLocalize(Consts.Format(Consts.GetInstance().SHOP_007X_TXT_DESCRIPTION03_xMEDALxUNIT, (IDictionary) new Hashtable()
      {
        {
          (object) "medal",
          (object) this._price
        },
        {
          (object) "unit",
          (object) Consts.GetInstance().SHOP_TOWERMEDAL_UNIT
        }
      }));
    else if (psa.article.pay_type == CommonPayType.guild_medal)
    {
      this.TxtDescription03.SetTextLocalize(Consts.Format(Consts.GetInstance().SHOP_007X_TXT_DESCRIPTION03_MEDAL, (IDictionary) new Hashtable()
      {
        {
          (object) "medal",
          (object) this._price
        }
      }));
    }
    else
    {
      if (psa.article.pay_type != CommonPayType.raid_medal)
        return;
      this.TxtDescription03.SetTextLocalize(Consts.Format(Consts.GetInstance().SHOP_007X_TXT_DESCRIPTION03_MEDAL, (IDictionary) new Hashtable()
      {
        {
          (object) "medal",
          (object) this._price
        }
      }));
    }
  }

  public void SetTxtDescription04(PlayerShopArticle psa)
  {
    this.TxtDescription04.SetTextLocalize(Consts.Format(Consts.GetInstance().SHOP_00771_TXT_DESCRIPTION04, (IDictionary) new Hashtable()
    {
      {
        (object) "quantity",
        (object) this._purchasedQuantity.ToLocalizeNumberText()
      }
    }));
  }

  public void SetTxtDescription05(PlayerShopArticle psa)
  {
    if (psa.article.pay_type == CommonPayType.money)
      this.TxtDescription05.SetTextLocalize(Consts.Format(Consts.GetInstance().SHOP_007X_TXT_DESCRIPTION05_MONEY, (IDictionary) new Hashtable()
      {
        {
          (object) "money",
          (object) (this._purchasedQuantity * this._price).ToLocalizeNumberText()
        }
      }));
    else if (psa.article.pay_type == CommonPayType.medal)
      this.TxtDescription05.SetTextLocalize(Consts.Format(Consts.GetInstance().SHOP_007X_TXT_DESCRIPTION05_MEDAL, (IDictionary) new Hashtable()
      {
        {
          (object) "medal",
          (object) (this._purchasedQuantity * this._price).ToLocalizeNumberText()
        }
      }));
    else if (psa.article.pay_type == CommonPayType.battle_medal)
      this.TxtDescription05.SetTextLocalize(Consts.Format(Consts.GetInstance().SHOP_007X_TXT_DESCRIPTION05_MEDAL, (IDictionary) new Hashtable()
      {
        {
          (object) "medal",
          (object) (this._purchasedQuantity * this._price).ToLocalizeNumberText()
        }
      }));
    else if (psa.article.pay_type == CommonPayType.tower_medal)
      this.TxtDescription05.SetTextLocalize(Consts.Format(Consts.GetInstance().SHOP_007X_TXT_DESCRIPTION05_xMEDALxUNIT, (IDictionary) new Hashtable()
      {
        {
          (object) "medal",
          (object) (this._purchasedQuantity * this._price).ToLocalizeNumberText()
        },
        {
          (object) "unit",
          (object) Consts.GetInstance().SHOP_TOWERMEDAL_UNIT
        }
      }));
    else if (psa.article.pay_type == CommonPayType.guild_medal)
    {
      this.TxtDescription05.SetTextLocalize(Consts.Format(Consts.GetInstance().SHOP_007X_TXT_DESCRIPTION05_MEDAL, (IDictionary) new Hashtable()
      {
        {
          (object) "medal",
          (object) (this._purchasedQuantity * this._price).ToLocalizeNumberText()
        }
      }));
    }
    else
    {
      if (psa.article.pay_type != CommonPayType.raid_medal)
        return;
      this.TxtDescription05.SetTextLocalize(Consts.Format(Consts.GetInstance().SHOP_007X_TXT_DESCRIPTION05_MEDAL, (IDictionary) new Hashtable()
      {
        {
          (object) "medal",
          (object) (this._purchasedQuantity * this._price).ToLocalizeNumberText()
        }
      }));
    }
  }

  private IEnumerator ShopBuyAsync()
  {
    Shop00771Menu shop00771Menu = this;
    Singleton<CommonRoot>.GetInstance().loadingMode = 1;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    // ISSUE: reference to a compiler-generated method
    Future<WebAPI.Response.ShopBuy> shopInfoF = WebAPI.ShopBuy(shop00771Menu._itemId, shop00771Menu._purchasedQuantity, new System.Action<WebAPI.Response.UserError>(shop00771Menu.\u003CShopBuyAsync\u003Eb__38_0)).Then<WebAPI.Response.ShopBuy>((Func<WebAPI.Response.ShopBuy, WebAPI.Response.ShopBuy>) (result =>
    {
      Singleton<NGGameDataManager>.GetInstance().Parse(result);
      return result;
    }));
    IEnumerator e = shopInfoF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if (shopInfoF.Result != null)
    {
      SMManager.UpdateList<PlayerBattleMedal>(shopInfoF.Result.after.battle_medals, false);
      e = OnDemandDownload.WaitLoadHasUnitResource(false, false);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      string category = "MONEY";
      if (shop00771Menu._playerShopArticle.article.pay_type == CommonPayType.medal)
        category = "MEDAL";
      else if (shop00771Menu._playerShopArticle.article.pay_type == CommonPayType.tower_medal)
        category = "TOWERMEDAL";
      else if (shop00771Menu._playerShopArticle.article.pay_type == CommonPayType.guild_medal)
        category = "GUILDMEDAL";
      else if (shop00771Menu._playerShopArticle.article.pay_type == CommonPayType.raid_medal)
        category = "RAIDMEDAL";
      EventTracker.TrackSpend(category, category + "_SHOP_" + (object) shop00771Menu._itemId, shop00771Menu._purchasedQuantity * shop00771Menu._price);
      EventTracker.TrackEvent("SHOP", category + "_SHOP", "ID_" + (object) shop00771Menu._itemId, shop00771Menu._purchasedQuantity);
      if (shop00771Menu._onPurchasedHolding != null)
        shop00771Menu._onPurchasedHolding(shop00771Menu.holding_next);
      if (shop00771Menu._onPurchased != null)
      {
        e = shop00771Menu._onPurchased();
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
      }
      GameObject gameObject = Singleton<PopupManager>.GetInstance().open(shop00771Menu.prefab0078, false, false, true, true, false, false, "SE_1006");
      gameObject.SetActive(false);
      gameObject.SetActive(true);
      Singleton<CommonRoot>.GetInstance().isLoading = false;
      Singleton<CommonRoot>.GetInstance().loadingMode = 0;
    }
  }

  private long CalcBalance()
  {
    return this.holding_next = this._holding - (long) (this._purchasedQuantity * this._price);
  }

  private IEnumerator OpenPopup0078()
  {
    Future<GameObject> prefab0078F = Res.Prefabs.popup.popup_007_8__anim_popup01.Load<GameObject>();
    IEnumerator e = prefab0078F.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.prefab0078 = prefab0078F.Result.Clone((Transform) null);
    Shop0078Menu component = this.prefab0078.GetComponent<Shop0078Menu>();
    component.InitObj(this.linkTarget);
    component.InitDataSet(this._itemName, this._purchasedQuantity, this._playerQuantity, (System.Action) null, false);
    this.prefab0078.SetActive(false);
  }

  private IEnumerator OpenPopup99971()
  {
    Singleton<PopupManager>.GetInstance().onDismiss();
    Future<GameObject> prefab99971F = Res.Prefabs.popup.popup_999_7_1__anim_popup01.Load<GameObject>();
    IEnumerator e = prefab99971F.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Singleton<PopupManager>.GetInstance().openAlert(prefab99971F.Result, false, false, (EventDelegate) null, false, true, false, true);
  }

  public void IbtnNo()
  {
    this.PushAfter();
    Singleton<PopupManager>.GetInstance().closeAll(false);
  }

  public override void onBackButton()
  {
    this.IbtnNo();
  }

  public void IbtnPopupYes()
  {
    if (this._holding < (long) (this._purchasedQuantity * this._price))
    {
      this.StartCoroutine(this.OpenPopup99971());
    }
    else
    {
      this.PushAfter();
      this.StartCoroutine(this.OpenPopup0078());
      Singleton<PopupManager>.GetInstance().dismiss(true);
      this.StartCoroutine(this.ShopBuyAsync());
    }
  }

  public void PushAfter()
  {
    this.ibtnY.enabled = false;
    this.ibtnN.enabled = false;
  }
}
