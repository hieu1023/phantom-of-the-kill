﻿// Decompiled with JetBrains decompiler
// Type: BattleEffects
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class BattleEffects : BattleMonoBehaviour
{
  private BattleTimeManager btm;
  private Dictionary<BL.FieldEffect, Tuple<GameObject, GameObject[]>> effectResources;
  private GameObject damageEffect;
  private GameObject criticalEffect;
  private bool isPopupDismiss;

  public GameObject effectResourcePrefab(BL.FieldEffect fe)
  {
    return this.effectResources[fe].Item1;
  }

  public GameObject[] effectResourceAllPrefabs(BL.FieldEffect fe)
  {
    return this.effectResources[fe].Item2;
  }

  protected override IEnumerator Start_Battle()
  {
    BattleEffects battleEffects = this;
    battleEffects.btm = battleEffects.battleManager.getManager<BattleTimeManager>();
    battleEffects.effectResources = new Dictionary<BL.FieldEffect, Tuple<GameObject, GameObject[]>>();
    foreach (BL.FieldEffect fieldEffect in battleEffects.env.core.fieldEffectList.value)
    {
      BL.FieldEffect fe = fieldEffect;
      Future<GameObject> f = fe.fieldEffect.LoadPrefab();
      IEnumerator e = f.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      Future<GameObject[]> f2 = fe.fieldEffect.LoadAllEffectPrefab();
      e = f2.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      battleEffects.effectResources[fe] = Tuple.Create<GameObject, GameObject[]>(f.Result, f2.Result);
      f = (Future<GameObject>) null;
      f2 = (Future<GameObject[]>) null;
      fe = (BL.FieldEffect) null;
    }
  }

  public void onPopupDismiss()
  {
    this.isPopupDismiss = true;
  }

  public void startEffect(
    Transform t,
    float time,
    System.Action endAction = null,
    bool isBattleEnableControl = false,
    GameObject popupPrefab = null,
    bool alert = false,
    bool isCloned = false,
    System.Action<GameObject> cloneAction = null,
    BattleEffects.CloneEnumlator cloneE = null,
    bool isUnmask = false,
    bool isViewBack = true)
  {
    this.btm.setSchedule((Schedule) new BattleEffects.StartEffect(t, time, endAction, isBattleEnableControl, this, popupPrefab, alert, isCloned, cloneAction, cloneE, isUnmask, isViewBack));
  }

  public void startEffect(
    string name,
    float time,
    System.Action endAction = null,
    bool isBattleEnableControl = false,
    GameObject popupPrefab = null,
    bool alert = false,
    bool isCloned = false,
    System.Action<GameObject> cloneAction = null,
    BattleEffects.CloneEnumlator cloneE = null)
  {
    this.startEffect(this.transform.GetChildInFind(name), time, endAction, isBattleEnableControl, popupPrefab, alert, isCloned, cloneAction, cloneE, false, true);
  }

  public void startEffect(
    string name,
    float time,
    GameObject popupPrefab,
    bool alert = false,
    bool isCloned = false,
    System.Action<GameObject> cloneAction = null,
    BattleEffects.CloneEnumlator cloneE = null)
  {
    this.startEffect(name, time, (System.Action) null, false, popupPrefab, alert, isCloned, cloneAction, cloneE);
  }

  public void startEffect(BL.FieldEffect effect)
  {
    GameObject p = this.effectResourcePrefab(effect).Clone(this.transform);
    if ((UnityEngine.Object) p == (UnityEngine.Object) null)
      return;
    ClipFieldEffect componentInChildren = p.GetComponentInChildren<ClipFieldEffect>();
    if ((UnityEngine.Object) componentInChildren == (UnityEngine.Object) null)
      return;
    componentInChildren.setEffectData(effect);
    if (effect.fieldEffect.category == BattleFieldEffectCategory.boss)
      this.btm.setTargetUnit(this.env.core.getUnitPosition(this.env.core.getBossUnit()), 0.1f, (GameObject) null, (System.Action) null, (System.Action) null, true);
    this.startEffect(p.transform, 30000f, (System.Action) (() => UnityEngine.Object.Destroy((UnityEngine.Object) p.gameObject)), false, (GameObject) null, effect.fieldEffect.cancelable, false, (System.Action<GameObject>) null, (BattleEffects.CloneEnumlator) null, effect.fieldEffect.is_unmask, effect.fieldEffect.is_view_back);
  }

  public ConditionForVictory getConditionForVictory(string node)
  {
    Transform childInFind = this.transform.GetChildInFind(node);
    if (!((UnityEngine.Object) childInFind != (UnityEngine.Object) null))
      return (ConditionForVictory) null;
    ConditionForVictory[] componentsInChildren = childInFind.GetComponentsInChildren<ConditionForVictory>(true);
    return componentsInChildren != null && componentsInChildren.Length >= 1 ? componentsInChildren[0] : (ConditionForVictory) null;
  }

  public void setTurnNumber(string node, int n)
  {
    Transform childInFind = this.transform.GetChildInFind(node);
    if (!((UnityEngine.Object) childInFind != (UnityEngine.Object) null))
      return;
    EffectNumber[] componentsInChildren = childInFind.GetComponentsInChildren<EffectNumber>(true);
    if (componentsInChildren.Length == 0)
      return;
    foreach (EffectNumber effectNumber in componentsInChildren)
      effectNumber.setNumber(n);
  }

  public void setWaveNumber(string node, int wave, int maxWave)
  {
    Transform childInFind = this.transform.GetChildInFind(node);
    if (!((UnityEngine.Object) childInFind != (UnityEngine.Object) null))
      return;
    WaveNumber[] componentsInChildren = childInFind.GetComponentsInChildren<WaveNumber>(true);
    if (componentsInChildren.Length == 0)
      return;
    foreach (WaveNumber waveNumber in componentsInChildren)
      waveNumber.setNumber(wave + 1, maxWave);
  }

  private IEnumerator doFieldEffect(
    GameObject prefab,
    Transform t,
    float delay = 0.0f,
    Quaternion? rotate = null)
  {
    if ((double) delay != 0.0)
      yield return (object) new WaitForSeconds(delay);
    GameObject o = prefab.Clone(t);
    if (rotate.HasValue)
      o.transform.rotation *= rotate.Value;
    yield return (object) new WaitForSeconds(10f);
    UnityEngine.Object.Destroy((UnityEngine.Object) o);
  }

  private void execFieldEffect(GameObject prefab, Transform t, Quaternion? rotate = null)
  {
    this.StartCoroutine(this.doFieldEffect(prefab, t, 0.0f, rotate));
  }

  private void skillFieldEffectStartOld(
    BL.Unit unit,
    List<BL.Unit> targets,
    GameObject effectPrefab,
    GameObject targetEffectPrefab,
    System.Action action,
    System.Action targetAction)
  {
    if (unit != (BL.Unit) null)
    {
      UnitUpdate uu = this.env.unitResource[unit].gameObject.GetComponent<UnitUpdate>();
      this.btm.setTargetUnit(this.env.core.getUnitPosition(unit), 0.5f, effectPrefab, (System.Action) null, (System.Action) (() =>
      {
        uu.setAnimationBool("isRun", false);
        uu.setAnimationTrigger("isSkill");
        if (action == null)
          return;
        action();
      }), true);
    }
    if (targets == null || !((UnityEngine.Object) targetEffectPrefab != (UnityEngine.Object) null))
      return;
    this.btm.setScheduleAction((System.Action) (() =>
    {
      this.fieldEffectsStart(targetEffectPrefab, targets, 0.0f);
      if (targetAction == null)
        return;
      targetAction();
    }), 0.5f, (System.Action) null, (Func<bool>) null, false);
  }

  public void skillFieldEffectStartCore(
    BattleskillFieldEffect fe,
    BL.Unit unit,
    List<BL.Unit> targets,
    GameObject effectPrefab,
    GameObject invokedEffectPrefab,
    GameObject targetEffectPrefab,
    System.Action action,
    System.Action targetAction,
    List<BL.Unit> invokedTargetUnits,
    System.Action<BL.Unit> targetEndAction = null,
    int waitTiming = 0,
    List<Quaternion?> invokedEffectRotate = null)
  {
    if (fe == null)
    {
      this.skillFieldEffectStartOld(unit, targets, effectPrefab, targetEffectPrefab, action, targetAction);
    }
    else
    {
      if (unit != (BL.Unit) null)
      {
        UnitUpdate uu = this.env.unitResource[unit].gameObject.GetComponent<UnitUpdate>();
        if (fe.user_move_camera)
        {
          if (waitTiming == 0)
          {
            this.btm.setTargetUnit(this.env.core.getUnitPosition(unit), fe.user_wait_seconds, effectPrefab, (System.Action) null, (System.Action) (() =>
            {
              uu.setAnimationBool("isRun", false);
              uu.setAnimationTrigger("isSkill");
              if (action == null)
                return;
              action();
            }), true);
          }
          else
          {
            this.btm.setTargetUnit(this.env.core.getUnitPosition(unit), 0.0f, effectPrefab, (System.Action) null, (System.Action) (() =>
            {
              uu.setAnimationBool("isRun", false);
              uu.setAnimationTrigger("isSkill");
              if (action == null)
                return;
              action();
            }), true);
            this.btm.setScheduleAction((System.Action) null, fe.user_wait_seconds, (System.Action) null, (Func<bool>) null, false);
          }
        }
        else
          this.btm.setScheduleAction((System.Action) (() =>
          {
            uu.setAnimationBool("isRun", false);
            uu.setAnimationTrigger("isSkill");
            if ((UnityEngine.Object) effectPrefab != (UnityEngine.Object) null)
              this.execFieldEffect(effectPrefab, uu.transform, new Quaternion?());
            if (action == null)
              return;
            action();
          }), fe.user_wait_seconds, (System.Action) null, (Func<bool>) null, false);
      }
      if ((UnityEngine.Object) invokedEffectPrefab != (UnityEngine.Object) null && invokedTargetUnits != null)
      {
        if (fe.invoked_move_camera)
          this.btm.setTargetUnit(this.env.core.getUnitPosition(invokedTargetUnits[0]), 0.0f, (GameObject) null, (System.Action) null, (System.Action) null, true);
        this.btm.setScheduleAction((System.Action) (() =>
        {
          int num = 0;
          foreach (BL.Unit invokedTargetUnit in invokedTargetUnits)
          {
            Quaternion? rotate = new Quaternion?();
            if (invokedEffectRotate != null)
              rotate = invokedEffectRotate[num++];
            this.execFieldEffect(invokedEffectPrefab, this.env.unitResource[invokedTargetUnit].gameObject.GetComponent<UnitUpdate>().transform, rotate);
          }
        }), fe.invoked_wait_seconds, (System.Action) null, (Func<bool>) null, false);
      }
      if (!((UnityEngine.Object) targetEffectPrefab != (UnityEngine.Object) null) || targets == null)
        return;
      if (fe.targets_multiple_effect)
      {
        this.btm.setScheduleAction((System.Action) (() =>
        {
          this.fieldEffectsStart(targetEffectPrefab, targets, 0.0f);
          if (targetAction == null)
            return;
          targetAction();
        }), fe.target_wait_seconds, (System.Action) (() =>
        {
          if (targetEndAction == null)
            return;
          foreach (BL.Unit target in targets)
            targetEndAction(target);
        }), (Func<bool>) null, false);
      }
      else
      {
        this.btm.setScheduleAction((System.Action) (() =>
        {
          if (targetAction == null)
            return;
          targetAction();
        }), 0.0f, (System.Action) null, (Func<bool>) null, false);
        if (fe.target_move_camera)
        {
          foreach (BL.Unit target in targets)
          {
            BL.Unit ct = target;
            System.Action endAction = (System.Action) null;
            if (targetEndAction != null)
              endAction = (System.Action) (() => targetEndAction(ct));
            if (waitTiming == 0)
            {
              this.btm.setTargetUnit(this.env.core.getUnitPosition(ct), fe.target_wait_seconds, targetEffectPrefab, (System.Action) null, endAction, false);
            }
            else
            {
              this.btm.setTargetUnit(this.env.core.getUnitPosition(ct), 0.0f, targetEffectPrefab, (System.Action) null, (System.Action) null, false);
              this.btm.setScheduleAction((System.Action) null, fe.target_wait_seconds, endAction, (Func<bool>) null, false);
            }
          }
        }
        else
        {
          foreach (BL.Unit target in targets)
          {
            BL.Unit ct = target;
            System.Action endAction = (System.Action) null;
            if (targetEndAction != null)
              endAction = (System.Action) (() => targetEndAction(ct));
            this.btm.setScheduleAction((System.Action) (() => this.execFieldEffect(targetEffectPrefab, this.env.unitResource[ct].gameObject.transform, new Quaternion?())), fe.target_wait_seconds, endAction, (Func<bool>) null, false);
          }
        }
      }
    }
  }

  public void skillFieldEffectMultiStartCore(
    BattleskillFieldEffect[] aryFe,
    List<BL.Unit>[] aryTargets,
    GameObject[] aryInvokedEffectPrefab,
    GameObject[] aryTargetEffectPrefab,
    List<BL.Unit>[] aryInvokedTargetUnits,
    List<Quaternion?>[] aryInvokedEffectRotate = null)
  {
    if (aryFe == null)
      return;
    int length = aryFe.Length;
    this.btm.setSchedule(new Schedule()
    {
      isSetBattleEnable = true,
      isBattleEnable = false
    });
    for (int index = 0; index < length; ++index)
    {
      if (aryFe[index] != null && aryInvokedTargetUnits[index] != null && aryFe[index].invoked_move_camera)
      {
        this.btm.setTargetUnit(this.env.core.getUnitPosition(aryInvokedTargetUnits[index][0]), 0.0f, (GameObject) null, (System.Action) null, (System.Action) null, true);
        break;
      }
    }
    float num1 = 0.0f;
    for (int index1 = 0; index1 < length; ++index1)
    {
      if (aryFe[index1] != null && (UnityEngine.Object) aryInvokedEffectPrefab[index1] != (UnityEngine.Object) null && aryInvokedTargetUnits[index1] != null)
      {
        int idx = index1;
        this.btm.setScheduleAction((System.Action) (() =>
        {
          foreach (var data in aryInvokedTargetUnits[idx].Select((target, index) => new
          {
            target = target,
            index = index
          }))
          {
            Quaternion? rotate = new Quaternion?();
            if (aryInvokedEffectRotate != null && aryInvokedEffectRotate[idx] != null)
              rotate = aryInvokedEffectRotate[idx][data.index];
            this.execFieldEffect(aryInvokedEffectPrefab[idx], this.env.unitResource[data.target].gameObject.GetComponent<UnitUpdate>().transform, rotate);
          }
        }), 0.0f, (System.Action) null, (Func<bool>) null, false);
        num1 = Mathf.Max(num1, aryFe[index1].invoked_wait_seconds);
      }
    }
    if ((double) num1 > 0.0)
      this.btm.setScheduleAction((System.Action) null, num1, (System.Action) null, (Func<bool>) null, false);
    float num2 = 0.0f;
    for (int index = 0; index < length; ++index)
    {
      if (aryFe[index] != null && (UnityEngine.Object) aryTargetEffectPrefab[index] != (UnityEngine.Object) null && aryTargets[index] != null)
      {
        int idx = index;
        this.btm.setScheduleAction((System.Action) (() => this.fieldEffectsStart(aryTargetEffectPrefab[idx], aryTargets[idx], 0.0f)), 0.0f, (System.Action) null, (Func<bool>) null, false);
        num2 = Mathf.Max(num2, aryFe[index].target_wait_seconds);
      }
    }
    if ((double) num2 > 0.0)
      this.btm.setScheduleAction((System.Action) null, num2, (System.Action) null, (Func<bool>) null, false);
    this.btm.setSchedule(new Schedule()
    {
      isSetBattleEnable = true,
      isBattleEnable = this.battleManager.isBattleEnable
    });
  }

  public void skillFieldEffectStart(
    BL.Unit unit,
    BL.Skill skill,
    List<BL.Unit> targets,
    List<BL.Unit> invokeUnits,
    System.Action targetAction = null,
    List<Quaternion?> invokedEffectRotate = null)
  {
    BE.SkillResource skillResource = this.env.skillResource[skill.skill.field_effect.ID];
    this.skillFieldEffectStartCore(skill.skill.field_effect, unit, targets, skillResource.effectPrefab, skillResource.invokedEffectPrefab, skillResource.targetEffectPrefab, (System.Action) null, targetAction, invokeUnits, (System.Action<BL.Unit>) null, 0, invokedEffectRotate);
  }

  public void mbFieldEffectStart(BL.Unit unit, BL.MagicBullet mb, List<BL.Unit> targets)
  {
    Debug.LogWarning((object) (" === mbFieldEffectStart name:" + mb.name));
    BE.SkillResource skillResource = this.env.skillResource[mb.skill.field_effect.ID];
    this.skillFieldEffectStartCore(mb.skill.field_effect, unit, targets, skillResource.effectPrefab, skillResource.invokedEffectPrefab, skillResource.targetEffectPrefab, (System.Action) null, (System.Action) null, new List<BL.Unit>()
    {
      unit
    }, (System.Action<BL.Unit>) null, 0, (List<Quaternion?>) null);
  }

  public void itemFieldEffectStart(List<BL.Unit> targets, BL.Item item, System.Action action)
  {
    BE.ItemResource itemResource = this.env.itemResource[item.itemId];
    this.skillFieldEffectStartCore(item.item.skill.field_effect, (BL.Unit) null, targets, (GameObject) null, (GameObject) null, itemResource.targetEffectPrefab, (System.Action) null, action, (List<BL.Unit>) null, (System.Action<BL.Unit>) null, 0, (List<Quaternion?>) null);
  }

  public void fieldEffectsStart(GameObject prefab, List<BL.Unit> targets, float delay = 0.0f)
  {
    foreach (BL.Unit target in targets)
    {
      BE.UnitResource unitResource = this.env.unitResource[target];
      this.StartCoroutine(this.doFieldEffect(prefab, unitResource.gameObject.transform, delay, new Quaternion?()));
    }
  }

  public void fieldEffectsStart(GameObject prefab, BL.Unit target, float delay = 0.0f)
  {
    BE.UnitResource unitResource = this.env.unitResource[target];
    this.StartCoroutine(this.doFieldEffect(prefab, unitResource.gameObject.transform, delay, new Quaternion?()));
  }

  public abstract class CloneEnumlator
  {
    public abstract IEnumerator doBody(GameObject o);
  }

  private class StartEffect : ScheduleEnumerator
  {
    private Transform tf;
    private float wait;
    private bool isBattleEnableControl;
    private BattleEffects parent;
    private GameObject popupPrefab;
    private bool alert;
    private bool isCloned;
    private System.Action<GameObject> cloneAction;
    private BattleEffects.CloneEnumlator cloneE;
    private bool isUnmask;
    private bool isViewBack;

    public StartEffect(
      Transform t,
      float wait,
      System.Action endAction,
      bool isBattleEnableControl,
      BattleEffects parent,
      GameObject popupPrefab,
      bool alert,
      bool isCloned,
      System.Action<GameObject> cloneAction,
      BattleEffects.CloneEnumlator cloneE,
      bool isUnmask,
      bool isViewBack)
    {
      this.tf = t;
      this.wait = wait;
      this.endAction = endAction;
      this.isBattleEnableControl = isBattleEnableControl;
      this.parent = parent;
      this.isCompleted = false;
      this.popupPrefab = popupPrefab;
      this.alert = alert;
      this.isCloned = isCloned;
      this.cloneAction = cloneAction;
      this.cloneE = cloneE;
      this.isUnmask = isUnmask;
      this.isViewBack = isViewBack;
      this.isInsertMode = true;
    }

    public override IEnumerator doBody()
    {
      BattleEffects.StartEffect startEffect = this;
      EventDelegate ed = (EventDelegate) null;
      startEffect.parent.battleManager.popupCloseAll(false);
      startEffect.parent.isPopupDismiss = false;
      if (startEffect.alert)
        ed = new EventDelegate((MonoBehaviour) startEffect.parent, "onPopupDismiss");
      GameObject po = startEffect.isCloned ? startEffect.popupPrefab : startEffect.popupPrefab.Clone((Transform) null);
      if (startEffect.cloneAction != null)
        startEffect.cloneAction(po);
      if (startEffect.cloneE != null)
      {
        IEnumerator e = startEffect.cloneE.doBody(po);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
      }
      startEffect.parent.battleManager.popupOpen(po, startEffect.alert, ed, true, startEffect.isBattleEnableControl, startEffect.isUnmask, startEffect.isViewBack, true);
      if ((UnityEngine.Object) po != (UnityEngine.Object) null)
      {
        po.SetActive(false);
        po.SetActive(true);
      }
      if ((UnityEngine.Object) startEffect.tf != (UnityEngine.Object) null)
      {
        startEffect.tf.gameObject.SetActive(true);
        EffectSE component = startEffect.tf.gameObject.GetComponent<EffectSE>();
        if ((UnityEngine.Object) component != (UnityEngine.Object) null)
          startEffect.parent.StartCoroutine(component.PlaySE(0.0f));
      }
      while ((double) startEffect.time - (double) startEffect.startTime < (double) startEffect.wait && !startEffect.parent.isPopupDismiss)
        yield return (object) null;
      NGSoundManager sm = Singleton<NGSoundManager>.GetInstance();
      while (!sm.IsVoiceStopAll())
      {
        if (startEffect.parent.isPopupDismiss)
        {
          sm.stopVoice(-1);
          break;
        }
        yield return (object) null;
      }
      if ((UnityEngine.Object) startEffect.tf != (UnityEngine.Object) null)
        startEffect.tf.gameObject.SetActive(false);
      startEffect.parent.battleManager.popupDismiss(startEffect.isBattleEnableControl, false);
      startEffect.isCompleted = true;
    }
  }
}
