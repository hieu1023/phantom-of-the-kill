﻿// Decompiled with JetBrains decompiler
// Type: Guild0281FacilityInfoController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Guild0281FacilityInfoController : MonoBehaviour
{
  [SerializeField]
  private UILabel unitExperienceLabel;
  [SerializeField]
  private UILabel playerExperienceLabel;
  [SerializeField]
  private UILabel moneyLabel;
  [SerializeField]
  private UILabel rewardDropLabel;
  [SerializeField]
  private GameObject bonusIcon;
  [SerializeField]
  private UILabel scaffoldRankLabel;
  [SerializeField]
  private UILabel scaffoldStatus1TitleLabel;
  [SerializeField]
  private UILabel scaffoldStatus1ValueLabel;
  [SerializeField]
  private UILabel scaffoldStatus2TitleLabel;
  [SerializeField]
  private UILabel scaffoldStatus2ValueLabel;
  [SerializeField]
  private UILabel wallsRankLabel;
  [SerializeField]
  private UILabel wallsStatus1TitleLabel;
  [SerializeField]
  private UILabel wallsStatus1ValueLabel;
  [SerializeField]
  private UILabel wallsStatus2ValueLabel;
  [SerializeField]
  private UILabel towerRankLabel;
  [SerializeField]
  private UILabel towerStatus1TitleLabel;
  [SerializeField]
  private UILabel towerStatus1ValueLabel;
  [SerializeField]
  private UILabel towerStatus2TitleLabel;
  [SerializeField]
  private UILabel towerStatus2ValueLabel;
  [SerializeField]
  private GameObject scaffoldInvestmentIcon;
  [SerializeField]
  private GameObject wallsInvestmentIcon;
  [SerializeField]
  private GameObject towerInvestmentIcon;
  [SerializeField]
  private UIButton scaffoldInvestmentButton;
  [SerializeField]
  private UIButton wallsInvestmentButton;
  [SerializeField]
  private UIButton towerInvestmentButton;
  [SerializeField]
  private Transform scaffoldLevelUpEffectContainer;
  [SerializeField]
  private Transform wallsLevelUpEffectContainer;
  [SerializeField]
  private Transform towerLevelUpEffectContainer;
  private GameObject buildingInvestmentConfirmDialogPrefab;
  private GameObject facilityLevelUpEffectPrefab;

  public IEnumerator InitializeAsync()
  {
    IEnumerator e = this.LoadFacilityInvestDialogPrefab();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    e = this.LoadFacilityLevelUpEffectPrefab();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.RefreshGuildFacilityStatus();
  }

  private IEnumerator LoadFacilityInvestDialogPrefab()
  {
    Future<GameObject> prefab = Res.Prefabs.popup.popup_028_guild_HQ_BuildingInvestmentConfirm__anim_popup01.Load<GameObject>();
    IEnumerator e = prefab.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.buildingInvestmentConfirmDialogPrefab = prefab.Result;
  }

  private IEnumerator LoadFacilityLevelUpEffectPrefab()
  {
    Future<GameObject> prefab = Res.Prefabs.guild.dir_main_facility_level_up_status.Load<GameObject>();
    IEnumerator e = prefab.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.facilityLevelUpEffectPrefab = prefab.Result;
  }

  public void RefreshGuildFacilityStatus()
  {
    GuildLevelBonus levelBonus = PlayerAffiliation.Current.guild.level_bonus;
    Color color1;
    Color color2;
    if (levelBonus.campaign_flag)
    {
      this.bonusIcon.SetActive(true);
      color1 = new Color(1f, 1f, 1f, 1f);
      Color color3 = new Color(1f, 1f, 0.0f, 1f);
      Color color4 = new Color(1f, 0.64f, 0.0f, 1f);
      color2 = new Color(1f, 1f, 1f, 1f);
      Color color5 = new Color(1f, 0.35f, 0.0f, 1f);
      Color color6 = new Color(1f, 0.64f, 0.0f, 1f);
      this.unitExperienceLabel.applyGradient = true;
      this.playerExperienceLabel.applyGradient = true;
      this.moneyLabel.applyGradient = true;
      this.rewardDropLabel.applyGradient = true;
      this.scaffoldStatus1ValueLabel.applyGradient = true;
      this.scaffoldStatus2ValueLabel.applyGradient = true;
      this.towerStatus1ValueLabel.applyGradient = true;
      this.towerStatus2ValueLabel.applyGradient = true;
      this.wallsStatus1ValueLabel.applyGradient = true;
      this.wallsStatus2ValueLabel.applyGradient = true;
      this.unitExperienceLabel.gradientTop = color3;
      this.unitExperienceLabel.gradientBottom = color4;
      this.playerExperienceLabel.gradientTop = color3;
      this.playerExperienceLabel.gradientBottom = color4;
      this.moneyLabel.gradientTop = color3;
      this.moneyLabel.gradientBottom = color4;
      this.rewardDropLabel.gradientTop = color3;
      this.rewardDropLabel.gradientBottom = color4;
      this.scaffoldStatus1ValueLabel.gradientTop = color5;
      this.scaffoldStatus1ValueLabel.gradientBottom = color6;
      this.scaffoldStatus2ValueLabel.gradientTop = color5;
      this.scaffoldStatus2ValueLabel.gradientBottom = color6;
      this.towerStatus1ValueLabel.gradientTop = color5;
      this.towerStatus1ValueLabel.gradientBottom = color6;
      this.towerStatus2ValueLabel.gradientTop = color5;
      this.towerStatus2ValueLabel.gradientBottom = color6;
      this.wallsStatus1ValueLabel.gradientTop = color5;
      this.wallsStatus1ValueLabel.gradientBottom = color6;
      this.wallsStatus2ValueLabel.gradientTop = color5;
      this.wallsStatus2ValueLabel.gradientBottom = color6;
    }
    else
    {
      this.bonusIcon.SetActive(false);
      color1 = new Color(1f, 1f, 1f, 1f);
      color2 = new Color(0.0f, 0.0f, 0.0f, 1f);
      this.unitExperienceLabel.applyGradient = false;
      this.playerExperienceLabel.applyGradient = false;
      this.moneyLabel.applyGradient = false;
      this.rewardDropLabel.applyGradient = false;
      this.scaffoldStatus1ValueLabel.applyGradient = false;
      this.scaffoldStatus2ValueLabel.applyGradient = false;
      this.towerStatus1ValueLabel.applyGradient = false;
      this.towerStatus2ValueLabel.applyGradient = false;
      this.wallsStatus1ValueLabel.applyGradient = false;
      this.wallsStatus2ValueLabel.applyGradient = false;
    }
    this.unitExperienceLabel.color = color1;
    this.playerExperienceLabel.color = color1;
    this.moneyLabel.color = color1;
    this.rewardDropLabel.color = color1;
    this.scaffoldStatus1ValueLabel.color = color2;
    this.scaffoldStatus2ValueLabel.color = color2;
    this.towerStatus1ValueLabel.color = color2;
    this.towerStatus2ValueLabel.color = color2;
    this.wallsStatus1ValueLabel.color = color2;
    this.wallsStatus2ValueLabel.color = color2;
    this.unitExperienceLabel.SetTextLocalize(Guild0281FacilityInfoController.GetFormattedStatusValue(levelBonus.unit_exp, false));
    this.playerExperienceLabel.SetTextLocalize(Guild0281FacilityInfoController.GetFormattedStatusValue(levelBonus.player_exp, false));
    this.moneyLabel.SetTextLocalize(Guild0281FacilityInfoController.GetFormattedStatusValue(levelBonus.money, false));
    this.rewardDropLabel.SetTextLocalize(Guild0281FacilityInfoController.GetFormattedStatusValue(levelBonus.item, false));
    GuildMembership guildMembership = ((IEnumerable<GuildMembership>) PlayerAffiliation.Current.guild.memberships).FirstOrDefault<GuildMembership>((Func<GuildMembership, bool>) (x => x.player.player_id == Player.Current.id));
    if (guildMembership != null && (guildMembership.role == GuildRole.master || guildMembership.role == GuildRole.sub_master))
    {
      this.scaffoldInvestmentIcon.SetActive(true);
      this.wallsInvestmentIcon.SetActive(true);
      this.towerInvestmentIcon.SetActive(true);
      this.scaffoldInvestmentButton.isEnabled = true;
      this.wallsInvestmentButton.isEnabled = true;
      this.towerInvestmentButton.isEnabled = true;
    }
    else
    {
      this.scaffoldInvestmentIcon.SetActive(false);
      this.wallsInvestmentIcon.SetActive(false);
      this.towerInvestmentIcon.SetActive(false);
      this.scaffoldInvestmentButton.isEnabled = false;
      this.wallsInvestmentButton.isEnabled = false;
      this.towerInvestmentButton.isEnabled = false;
    }
    foreach (GuildHq hq in PlayerAffiliation.Current.guild.hqs)
    {
      switch (hq.base_type)
      {
        case GuildBaseType.walls:
          this.wallsRankLabel.SetTextLocalize(hq.rank);
          foreach (GuildBaseBonus bonuse in hq.bonuses)
          {
            if (bonuse.bonus_type == GuildBaseBonusType.hit_point)
            {
              this.wallsStatus1TitleLabel.SetTextLocalize(Consts.GetInstance().Guild0281MENU_FACILITY_STATUS_TITLE_HIT_POINT);
              this.wallsStatus1ValueLabel.SetTextLocalize(Guild0281FacilityInfoController.GetFormattedStatusValue(bonuse.bonus_grade, false));
            }
          }
          break;
        case GuildBaseType.tower:
          this.towerRankLabel.SetTextLocalize(hq.rank);
          foreach (GuildBaseBonus bonuse in hq.bonuses)
          {
            if (bonuse.bonus_type == GuildBaseBonusType.physical_attack)
            {
              this.towerStatus1TitleLabel.SetTextLocalize(Consts.GetInstance().Guild0281MENU_FACILITY_STATUS_TITLE_PHYSICAL_ATK);
              this.towerStatus1ValueLabel.SetTextLocalize(Guild0281FacilityInfoController.GetFormattedStatusValue(bonuse.bonus_grade, false));
            }
            else if (bonuse.bonus_type == GuildBaseBonusType.magic_attack)
            {
              this.towerStatus2TitleLabel.SetTextLocalize(Consts.GetInstance().Guild0281MENU_FACILITY_STATUS_TITLE_MAGICAL_ATK);
              this.towerStatus2ValueLabel.SetTextLocalize(Guild0281FacilityInfoController.GetFormattedStatusValue(bonuse.bonus_grade, false));
            }
          }
          break;
        case GuildBaseType.scaffold:
          this.scaffoldRankLabel.SetTextLocalize(hq.rank);
          foreach (GuildBaseBonus bonuse in hq.bonuses)
          {
            if (bonuse.bonus_type == GuildBaseBonusType.accuracy_rate)
            {
              this.scaffoldStatus1TitleLabel.SetTextLocalize(Consts.GetInstance().Guild0281MENU_FACILITY_STATUS_TITLE_ACCURACY_RATE);
              this.scaffoldStatus1ValueLabel.SetTextLocalize(Guild0281FacilityInfoController.GetFormattedStatusValue(bonuse.bonus_grade, false));
            }
            else if (bonuse.bonus_type == GuildBaseBonusType.avoidance)
            {
              this.scaffoldStatus2TitleLabel.SetTextLocalize(Consts.GetInstance().Guild0281MENU_FACILITY_STATUS_TITLE_AVOIDANCE);
              this.scaffoldStatus2ValueLabel.SetTextLocalize(Guild0281FacilityInfoController.GetFormattedStatusValue(bonuse.bonus_grade, false));
            }
          }
          break;
        default:
          Debug.LogError((object) "The type of facility does not exist!");
          return;
      }
    }
  }

  public void OnScaffoldInvestmentButtonClicked()
  {
    this.OpenFacilityInvestmentDialog(GuildBaseType.scaffold);
  }

  public void OnWallsInvestmentButtonClicked()
  {
    this.OpenFacilityInvestmentDialog(GuildBaseType.walls);
  }

  public void OnTowerInvestmentButtonClicked()
  {
    this.OpenFacilityInvestmentDialog(GuildBaseType.tower);
  }

  public void OpenFacilityInvestmentDialog(GuildBaseType investmentFacilityType)
  {
    if (Singleton<NGSceneManager>.GetInstance().sceneBase.GetComponent<NGMenuBase>().IsPushAndSet())
      return;
    GuildHq selectedFacility = (GuildHq) null;
    foreach (GuildHq hq in PlayerAffiliation.Current.guild.hqs)
    {
      if (hq.base_type == investmentFacilityType)
      {
        selectedFacility = hq;
        break;
      }
    }
    if (selectedFacility.rank < selectedFacility.max_rank && PlayerAffiliation.Current.guild.appearance.level >= selectedFacility.guild_level_cap)
      this.StartCoroutine(this.OpenFacilityInvestmentDialog(selectedFacility));
    else
      ModalWindow.Show(Consts.GetInstance().Guild0281MENU_FACILITY_RANK_MAX_DIALOG_TITLE, Consts.GetInstance().Guild0281MENU_FACILITY_RANK_MAX_DIALOG_CONTENT, (System.Action) (() =>
      {
        if (!((UnityEngine.Object) Singleton<NGSceneManager>.GetInstance().sceneBase != (UnityEngine.Object) null) || !((UnityEngine.Object) Singleton<NGSceneManager>.GetInstance().sceneBase.GetComponent<NGMenuBase>() != (UnityEngine.Object) null))
          return;
        Singleton<NGSceneManager>.GetInstance().sceneBase.GetComponent<NGMenuBase>().IsPush = false;
      }));
  }

  private IEnumerator OpenFacilityInvestmentDialog(GuildHq selectedFacility)
  {
    Guild0281FacilityInfoController facilityInfoController = this;
    if ((UnityEngine.Object) facilityInfoController.buildingInvestmentConfirmDialogPrefab == (UnityEngine.Object) null)
    {
      IEnumerator e = facilityInfoController.LoadFacilityInvestDialogPrefab();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    GameObject prefab = facilityInfoController.buildingInvestmentConfirmDialogPrefab.Clone((Transform) null);
    prefab.GetComponent<Guild0281BuildingInvestmentConfirmDialogController>().Initialize(selectedFacility, facilityInfoController);
    Singleton<PopupManager>.GetInstance().open(prefab, false, false, true, true, false, false, "SE_1006");
  }

  public IEnumerator OnInvestmentFinished(GuildHq selectedFacility)
  {
    Guild0281FacilityInfoController facilityInfoController = this;
    Debug.Log((object) "OnInvestmentFinished is invoked!");
    Guild0281Menu menu = facilityInfoController.GetComponent<Guild0281Menu>();
    IEnumerator e = menu.InitializeAsync(Guild0281Menu.SceneType.HQTop, (WebAPI.Response.GuildTop) null);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    menu.InitializeHQTop();
    menu.HQLevelUp(selectedFacility.base_type);
    facilityInfoController.ShowFacilityLevelUpEffect(selectedFacility.base_type);
    facilityInfoController.RefreshGuildFacilityStatus();
  }

  private void ShowFacilityLevelUpEffect(GuildBaseType facilityType)
  {
    Transform upEffectContainer;
    switch (facilityType)
    {
      case GuildBaseType.walls:
        upEffectContainer = this.wallsLevelUpEffectContainer;
        break;
      case GuildBaseType.tower:
        upEffectContainer = this.towerLevelUpEffectContainer;
        break;
      case GuildBaseType.scaffold:
        upEffectContainer = this.scaffoldLevelUpEffectContainer;
        break;
      default:
        Debug.LogError((object) "The type of facility does not exist!");
        return;
    }
    foreach (Component component in upEffectContainer)
      UnityEngine.Object.Destroy((UnityEngine.Object) component.gameObject);
    UnityEngine.Object.Destroy((UnityEngine.Object) this.facilityLevelUpEffectPrefab.Clone(upEffectContainer), 5f);
  }

  public static string GetFormattedStatusValue(int value, bool isValueOnly = false)
  {
    if (isValueOnly)
      return value.ToString();
    return Consts.Format(Consts.GetInstance().Guild0281MENU_FACILITY_STATUS, (IDictionary) new Hashtable()
    {
      {
        (object) nameof (value),
        (object) value
      }
    });
  }

  public void ClearFacilityLevelUpEffects()
  {
    foreach (Component component in this.scaffoldLevelUpEffectContainer)
      UnityEngine.Object.Destroy((UnityEngine.Object) component.gameObject);
    foreach (Component component in this.wallsLevelUpEffectContainer)
      UnityEngine.Object.Destroy((UnityEngine.Object) component.gameObject);
    foreach (Component component in this.towerLevelUpEffectContainer)
      UnityEngine.Object.Destroy((UnityEngine.Object) component.gameObject);
  }

  public void InvestFacility(GuildHq selectedFacility)
  {
    this.StartCoroutine(this.InvestFacilityCoroutine(selectedFacility));
  }

  private IEnumerator InvestFacilityCoroutine(GuildHq selectedFacility)
  {
    Singleton<CommonRoot>.GetInstance().loadingMode = 1;
    Future<WebAPI.Response.GuildBaseInvest> future = WebAPI.GuildBaseInvest((int) selectedFacility.base_type, selectedFacility.rank, (System.Action<WebAPI.Response.UserError>) (e =>
    {
      Singleton<CommonRoot>.GetInstance().loadingMode = 0;
      ModalWindow.Show(Consts.GetInstance().Guild0281MENU_FACILITY_INVESTMENT_ERROR_POPUP_TITLE, Consts.GetInstance().Guild0281MENU_FACILITY_INVESTMENT_ERROR_POPUP_CONTENT, (System.Action) (() => Guild0281Scene.ChangeSceneGuildTop(false, (Guild0281Menu) null, true)));
      Singleton<PopupManager>.GetInstance().dismiss(false);
    }));
    IEnumerator e1 = future.Wait();
    while (e1.MoveNext())
      yield return e1.Current;
    e1 = (IEnumerator) null;
    if (future.Result != null)
    {
      e1 = this.OnInvestmentFinished(selectedFacility);
      while (e1.MoveNext())
        yield return e1.Current;
      e1 = (IEnumerator) null;
      Singleton<PopupManager>.GetInstance().dismiss(false);
    }
  }
}
