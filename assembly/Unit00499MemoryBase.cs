﻿// Decompiled with JetBrains decompiler
// Type: Unit00499MemoryBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;
using System.Collections;
using UnityEngine;

public class Unit00499MemoryBase : BackButtonMenuBase
{
  protected PlayerUnit unit;
  protected int index;
  [SerializeField]
  protected Unit00499UnitStatus before;
  [SerializeField]
  protected Unit00499UnitStatus after;
  protected Unit00499SaveMemorySlotSelect menu;
  protected System.Action endUpdate;
  [SerializeField]
  protected Transform dir_reinforce_memory_slot_icon_container;
  [SerializeField]
  protected UILabel txt_Description;

  protected IEnumerator Delete()
  {
    Unit00499MemoryBase unit00499MemoryBase = this;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    IEnumerator e = WebAPI.UnitDeleteTransmigrateMemory(unit00499MemoryBase.unit.id, (System.Action<WebAPI.Response.UserError>) null).Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Singleton<PopupManager>.GetInstance().dismiss(false);
    e = unit00499MemoryBase.menu.DeleteUpdate();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Singleton<CommonRoot>.GetInstance().isLoading = false;
    unit00499MemoryBase.IsPush = false;
  }

  protected IEnumerator Save()
  {
    Unit00499MemoryBase unit00499MemoryBase = this;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    IEnumerator e = WebAPI.UnitSaveTransmigrateMemory(unit00499MemoryBase.index, unit00499MemoryBase.unit.id, (System.Action<WebAPI.Response.UserError>) null).Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if ((UnityEngine.Object) unit00499MemoryBase.menu != (UnityEngine.Object) null)
      unit00499MemoryBase.menu.endUpdate();
    else
      unit00499MemoryBase.endUpdate();
    Singleton<PopupManager>.GetInstance().closeAll(false);
    Singleton<CommonRoot>.GetInstance().isLoading = false;
    unit00499MemoryBase.IsPush = false;
  }

  public void IbtnBack()
  {
    Singleton<PopupManager>.GetInstance().dismiss(false);
  }

  public override void onBackButton()
  {
    this.IbtnBack();
  }
}
