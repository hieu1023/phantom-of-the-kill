﻿// Decompiled with JetBrains decompiler
// Type: UILabelExtension
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using UnityEngine;

public static class UILabelExtension
{
  public static void SetText(this UILabel label, string text)
  {
    if (!(bool) (Object) label)
      return;
    label.text = label.GetStandardizationTextJP(text);
  }

  public static void SetTextLocalize(this UILabel label, string text)
  {
    label.SetText(text.ToConverter());
  }

  public static void SetTextLocalize(this UILabel label, int num)
  {
    label.SetTextLocalize(num.ToString());
  }

  public static void SetTextLocalize(this UILabel label, long num)
  {
    label.SetTextLocalize(num.ToString());
  }
}
