﻿// Decompiled with JetBrains decompiler
// Type: SeaGlobalMenu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class SeaGlobalMenu : MonoBehaviour
{
  [SerializeField]
  private CommonSeaHeader owner;
  [SerializeField]
  private GameObject infoNew;
  [SerializeField]
  private GameObject presentNew;
  [SerializeField]
  private GameObject menuBadge;
  [SerializeField]
  private GameObject missionBadge;
  [SerializeField]
  private GameObject guildBadge;
  [SerializeField]
  private GameObject guildChatBadge;
  [SerializeField]
  private GameObject gvgBadge;
  [SerializeField]
  private GameObject raidBossIcon;
  [SerializeField]
  private GameObject[] gvgBadgeLabel;
  [SerializeField]
  private GameObject gachaNewBadge;
  [SerializeField]
  private GameObject limitedShopSeaButtonNewIcon;
  [SerializeField]
  private GameObject newbiePacksIcon;
  [SerializeField]
  private GameObject bikkuriIcon;

  private void OnEnable()
  {
    bool flag1 = false;
    try
    {
      flag1 = ((IEnumerable<OfficialInformationArticle>) SMManager.Get<OfficialInformationArticle[]>()).Any<OfficialInformationArticle>((Func<OfficialInformationArticle, bool>) (w => !Persist.infoUnRead.Data.GetUnRead(w)));
    }
    catch
    {
      Persist.infoUnRead.Delete();
    }
    this.infoNew.SetActive(flag1);
    PlayerPresent[] playerPresentArray = SMManager.Get<PlayerPresent[]>();
    this.presentNew.SetActive(playerPresentArray != null && ((IEnumerable<PlayerPresent>) playerPresentArray).Any<PlayerPresent>((Func<PlayerPresent, bool>) (p => !p.received_at.HasValue)));
    this.menuBadge.SetActive(Singleton<NGGameDataManager>.GetInstance().ReceivedFriendRequestCount > 0);
    this.missionBadge.SetActive(SMManager.Get<Player>().is_open_mission);
    Tuple<bool, bool, GuildUtil.GuildBadgeLabelType> badgeInfo = GuildUtil.getBadgeInfo();
    this.guildBadge.SetActive(badgeInfo.Item1);
    this.guildChatBadge.SetActive(badgeInfo.Item2);
    bool flag2 = (uint) badgeInfo.Item3 > 0U;
    if (GuildUtil.getBadgeState(GuildUtil.GuildBadgeInfoType.guild_raid))
    {
      this.raidBossIcon.SetActive(true);
      this.gvgBadge.SetActive(false);
    }
    else
    {
      this.raidBossIcon.SetActive(false);
      this.gvgBadge.SetActive(flag2);
    }
    ((IEnumerable<GameObject>) this.gvgBadgeLabel).ToggleOnce((int) (badgeInfo.Item3 - 1));
    DateTime? gachaLatestStartTime = Singleton<NGGameDataManager>.GetInstance().gachaLatestStartTime;
    DateTime dateTime1 = new DateTime();
    try
    {
      dateTime1 = Persist.lastAccessTime.Data.gachaRootLastAccessTime;
    }
    catch
    {
      Persist.lastAccessTime.Delete();
    }
    GameObject gachaNewBadge = this.gachaNewBadge;
    DateTime? nullable = gachaLatestStartTime;
    DateTime dateTime2 = dateTime1;
    int num = nullable.HasValue ? (nullable.GetValueOrDefault() > dateTime2 ? 1 : 0) : 0;
    gachaNewBadge.SetActive(num != 0);
    this.StartCoroutine(this.ShowIconNewOnShopButton());
    this.UpdateBikkuriIcon();
    this.UpdateNewbiePacksIcon();
  }

  private IEnumerator ShowIconNewOnShopButton()
  {
    if (Singleton<NGGameDataManager>.GetInstance().receivableGift)
      this.limitedShopSeaButtonNewIcon.SetActive(false);
    else if (Singleton<NGGameDataManager>.GetInstance().newbiePacks)
    {
      this.limitedShopSeaButtonNewIcon.SetActive(false);
    }
    else
    {
      IEnumerator e = ServerTime.WaitSync();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      DateTime dateTime1 = ServerTime.NowAppTime();
      DateTime? shopLatestStartTime = Singleton<NGGameDataManager>.GetInstance().limitedShopLatestStartTime;
      DateTime? shopLatestEndTime = Singleton<NGGameDataManager>.GetInstance().limitedShopLatestEndTime;
      DateTime dateTime2 = new DateTime();
      try
      {
        dateTime2 = Persist.lastAccessTime.Data.limitedShopLastAccessTime;
      }
      catch
      {
        Persist.lastAccessTime.Delete();
      }
      GameObject seaButtonNewIcon = this.limitedShopSeaButtonNewIcon;
      DateTime dateTime3 = dateTime1;
      DateTime? nullable1 = shopLatestStartTime;
      int num;
      if ((nullable1.HasValue ? (dateTime3 > nullable1.GetValueOrDefault() ? 1 : 0) : 0) != 0)
      {
        DateTime dateTime4 = dateTime1;
        DateTime? nullable2 = shopLatestEndTime;
        if ((nullable2.HasValue ? (dateTime4 < nullable2.GetValueOrDefault() ? 1 : 0) : 0) != 0)
        {
          DateTime? nullable3 = shopLatestStartTime;
          DateTime dateTime5 = dateTime2;
          if ((nullable3.HasValue ? (nullable3.GetValueOrDefault() > dateTime5 ? 1 : 0) : 0) != 0)
          {
            DateTime? nullable4 = shopLatestEndTime;
            DateTime dateTime6 = dateTime2;
            num = nullable4.HasValue ? (nullable4.GetValueOrDefault() > dateTime6 ? 1 : 0) : 0;
            goto label_15;
          }
        }
      }
      num = 0;
label_15:
      seaButtonNewIcon.SetActive(num != 0);
    }
  }

  private void UpdateNewbiePacksIcon()
  {
    if (Singleton<NGGameDataManager>.GetInstance().newbiePacks)
    {
      this.newbiePacksIcon.SetActive(true);
      this.limitedShopSeaButtonNewIcon.SetActive(false);
      this.bikkuriIcon.SetActive(false);
    }
    else
      this.newbiePacksIcon.SetActive(false);
  }

  public void UpdateBikkuriIcon()
  {
    if (Singleton<NGGameDataManager>.GetInstance().receivableGift)
    {
      this.bikkuriIcon.SetActive(true);
      this.newbiePacksIcon.SetActive(false);
      this.limitedShopSeaButtonNewIcon.SetActive(false);
    }
    else
      this.bikkuriIcon.SetActive(false);
  }

  private bool ClearSceneStacks(bool clearAll = false, string clearStackName = null)
  {
    bool flag1 = false;
    NGSceneManager instance = Singleton<NGSceneManager>.GetInstance();
    if (!string.IsNullOrEmpty(clearStackName))
    {
      if (instance.sceneName == clearStackName)
      {
        flag1 = true;
        instance.destroyCurrentScene();
      }
      if (instance.clearStack(clearStackName) > 0)
        flag1 = true;
    }
    bool flag2 = false;
    NGSceneBase sceneBase = instance.sceneBase;
    if ((UnityEngine.Object) sceneBase != (UnityEngine.Object) null)
      sceneBase.IsPush = true;
    if (clearAll)
    {
      instance.destroyLoadedScenes();
    }
    else
    {
      flag2 = instance.clearStackBeforeTopGlobalBack();
      instance.destoryNonStackScenes();
    }
    return !flag1 && flag2;
  }

  private bool IsPushAndSet()
  {
    NGSceneBase sceneBase = Singleton<NGSceneManager>.GetInstance().sceneBase;
    if ((UnityEngine.Object) sceneBase != (UnityEngine.Object) null)
    {
      if (sceneBase.IsPush)
        return true;
      sceneBase.IsPush = true;
    }
    return false;
  }

  private bool IsPush
  {
    get
    {
      NGSceneBase sceneBase = Singleton<NGSceneManager>.GetInstance().sceneBase;
      return (UnityEngine.Object) sceneBase != (UnityEngine.Object) null && sceneBase.IsPush;
    }
    set
    {
      NGSceneBase sceneBase = Singleton<NGSceneManager>.GetInstance().sceneBase;
      if ((UnityEngine.Object) sceneBase == (UnityEngine.Object) null || sceneBase.IsPush == value)
        return;
      sceneBase.IsPush = value;
    }
  }

  public void IbtnSeaAlbum()
  {
    if (this.IsPushAndSet())
      return;
    this.owner.CloseMenu();
    string clearStackName = "sea030_album";
    if (Singleton<NGSceneManager>.GetInstance().sceneName != clearStackName || !Singleton<NGGameDataManager>.GetInstance().IsSea)
    {
      Singleton<NGGameDataManager>.GetInstance().IsSea = true;
      Sea030AlbumScene.ChangeScene(this.ClearSceneStacks(false, clearStackName));
    }
    else
      this.IsPush = false;
  }

  public void IbtnSeaHome()
  {
    if (this.IsPushAndSet())
      return;
    this.owner.CloseMenu();
    if (Singleton<NGSceneManager>.GetInstance().sceneName != "sea030_home" || !Singleton<NGGameDataManager>.GetInstance().IsSea)
    {
      Singleton<NGGameDataManager>.GetInstance().IsSea = true;
      this.ClearSceneStacks(true, (string) null);
      Sea030HomeScene.ChangeScene(false, false);
    }
    else
      this.IsPush = false;
  }

  public void IbtnSeaQuest()
  {
    if (this.IsPushAndSet())
      return;
    this.owner.CloseMenu();
    string clearStackName = "sea030_quest";
    if (Singleton<NGSceneManager>.GetInstance().sceneName != clearStackName || !Singleton<NGGameDataManager>.GetInstance().IsSea)
    {
      Singleton<NGGameDataManager>.GetInstance().IsSea = true;
      Sea030_questScene.ChangeScene(this.ClearSceneStacks(false, clearStackName), true, false);
    }
    else
      this.IsPush = false;
  }

  public void IbtnEventQuest()
  {
    if (this.IsPushAndSet())
      return;
    this.owner.CloseMenu();
    string clearStackName = "quest002_17";
    if (Singleton<NGSceneManager>.GetInstance().sceneName != clearStackName || Singleton<NGGameDataManager>.GetInstance().IsSea)
    {
      Singleton<NGGameDataManager>.GetInstance().IsSea = false;
      Quest00217Scene.ChangeScene(this.ClearSceneStacks(false, clearStackName));
    }
    else
      this.IsPush = false;
  }

  public void IbtnHome()
  {
    if (this.IsPushAndSet())
      return;
    this.owner.CloseMenu();
    Singleton<NGGameDataManager>.GetInstance().IsSea = false;
    this.ClearSceneStacks(true, (string) null);
    MypageScene.ChangeScene(false, false, false);
  }

  public void IbtnInfo()
  {
    string str = "mypage001_8_1";
    if (this.IsPushAndSet())
      return;
    this.owner.CloseMenu();
    if (Singleton<NGSceneManager>.GetInstance().sceneName != str || Singleton<NGGameDataManager>.GetInstance().IsSea)
    {
      Singleton<NGGameDataManager>.GetInstance().IsSea = false;
      bool isStack = this.ClearSceneStacks(false, str);
      Singleton<NGSceneManager>.GetInstance().changeScene(str, isStack, (object[]) Array.Empty<object>());
    }
    else
      this.IsPush = false;
  }

  public void IbtnPresent()
  {
    if (this.IsPushAndSet())
      return;
    string str = "mypage001_7";
    this.owner.CloseMenu();
    if (Singleton<NGSceneManager>.GetInstance().sceneName != str || Singleton<NGGameDataManager>.GetInstance().IsSea)
    {
      Singleton<NGGameDataManager>.GetInstance().IsSea = false;
      bool isStack = this.ClearSceneStacks(false, str);
      Singleton<NGSceneManager>.GetInstance().changeScene(str, isStack, (object[]) Array.Empty<object>());
    }
    else
      this.IsPush = false;
  }

  public void IbtnMenu()
  {
    if (this.IsPushAndSet())
      return;
    this.owner.CloseMenu();
    string str = "story001_9_1";
    if (Singleton<NGSceneManager>.GetInstance().sceneName != str || Singleton<NGGameDataManager>.GetInstance().IsSea)
    {
      Singleton<NGGameDataManager>.GetInstance().IsSea = false;
      bool isStack = this.ClearSceneStacks(false, str);
      Singleton<NGSceneManager>.GetInstance().changeScene(str, isStack, (object[]) Array.Empty<object>());
    }
    else
      this.IsPush = false;
  }

  public void IbtnMission()
  {
    if (this.IsPushAndSet())
      return;
    this.owner.CloseMenu();
    Singleton<CommonRoot>.GetInstance().DailyMissionController.Show();
  }

  public void IbtnGacha()
  {
    if (this.IsPushAndSet())
      return;
    this.owner.CloseMenu();
    string str = "gacha006_3";
    if (Singleton<NGSceneManager>.GetInstance().sceneName != str || Singleton<NGGameDataManager>.GetInstance().IsSea)
    {
      Singleton<NGGameDataManager>.GetInstance().IsSea = false;
      bool isStack = this.ClearSceneStacks(false, str);
      Singleton<NGSceneManager>.GetInstance().changeScene(str, isStack, (object[]) Array.Empty<object>());
    }
    else
      this.IsPush = false;
  }

  public void IbtnShop()
  {
    if (this.IsPushAndSet())
      return;
    this.owner.CloseMenu();
    string clearStackName = "shop007_1";
    if (Singleton<NGSceneManager>.GetInstance().sceneName != clearStackName || Singleton<NGGameDataManager>.GetInstance().IsSea)
    {
      Singleton<NGGameDataManager>.GetInstance().IsSea = false;
      Shop0071Scene.changeScene(this.ClearSceneStacks(false, clearStackName));
    }
    else
      this.IsPush = false;
  }

  public void IbtnUnit()
  {
    if (this.IsPushAndSet())
      return;
    this.owner.CloseMenu();
    string clearStackName = "unit004_top";
    if (Singleton<NGSceneManager>.GetInstance().sceneName != clearStackName || Singleton<NGGameDataManager>.GetInstance().IsSea)
    {
      Singleton<NGGameDataManager>.GetInstance().IsSea = false;
      Unit004topScene.ChangeSceneNoClearStack(this.ClearSceneStacks(false, clearStackName));
    }
    else
      this.IsPush = false;
  }

  public void IbtnTeam()
  {
    if (this.IsPushAndSet())
      return;
    this.owner.CloseMenu();
    string clearStackName = "unit004_6_0822_sea";
    if (Singleton<NGSceneManager>.GetInstance().sceneName != clearStackName || !Singleton<NGGameDataManager>.GetInstance().IsSea)
    {
      Singleton<NGGameDataManager>.GetInstance().IsSea = true;
      Unit0046Scene.changeScene(this.ClearSceneStacks(false, clearStackName), (QuestLimitationBase[]) null, (string) null, false);
    }
    else
      this.IsPush = false;
  }

  public void IbtnGuild()
  {
    if (this.IsPushAndSet())
      return;
    this.owner.CloseMenu();
    string clearStackName = "guild028_1";
    if (Singleton<NGSceneManager>.GetInstance().sceneName != clearStackName || Singleton<NGGameDataManager>.GetInstance().IsSea)
    {
      Singleton<NGGameDataManager>.GetInstance().IsSea = false;
      Guild0281Scene.ChangeSceneGuildTop(false, (Guild0281Menu) null, this.ClearSceneStacks(false, clearStackName));
    }
    else
      this.IsPush = false;
  }

  private void Update()
  {
  }

  private void onBackButton()
  {
    this.owner.CloseMenu();
  }
}
