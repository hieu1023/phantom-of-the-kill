﻿// Decompiled with JetBrains decompiler
// Type: MissionGetPointRewardEffectPopupController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissionGetPointRewardEffectPopupController : MonoBehaviour
{
  [SerializeField]
  private List<GameObject> IconObjects;
  [SerializeField]
  private GameObject messageBox;
  private System.Action callback;

  public IEnumerator Init(List<PointReward> pointRewardList, System.Action callback = null)
  {
    this.callback = callback;
    this.IconObjects.ForEachIndex<GameObject>((System.Action<GameObject, int>) ((x, i) => x.SetActive(pointRewardList.Count - 1 == i)));
    GameObject iconObject = this.IconObjects[pointRewardList.Count - 1];
    int count = 0;
    foreach (Component component in iconObject.transform)
    {
      IEnumerator e = component.gameObject.GetOrAddComponent<CreateIconObject>().CreateThumbnail(pointRewardList[count].reward_type, pointRewardList[count].reward_id, pointRewardList[count].reward_quantity, true, false, new CommonQuestType?(), false);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      ++count;
    }
  }

  public void OnClickTouchToNextButton()
  {
    Singleton<PopupManager>.GetInstance().dismiss(false);
    System.Action callback = this.callback;
    if (callback == null)
      return;
    callback();
  }
}
