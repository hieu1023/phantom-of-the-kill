﻿// Decompiled with JetBrains decompiler
// Type: Gacha0065Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System.Collections;
using UnityEngine;

public class Gacha0065Scene : NGSceneBase
{
  private GameObject popUp;
  private Gacha0065Menu obj;

  public IEnumerator onStartSceneAsync()
  {
    GachaModule[] module = SMManager.Get<GachaModule[]>();
    if ((Object) this.popUp == (Object) null)
    {
      Future<GameObject> fPopUp = Res.Prefabs.gacha006_5.popup_006_5__anim_popup01.Load<GameObject>();
      IEnumerator e = fPopUp.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.popUp = fPopUp.Result;
      fPopUp = (Future<GameObject>) null;
    }
    this.obj = Singleton<PopupManager>.GetInstance().open(this.popUp, false, false, false, true, false, false, "SE_1006").GetComponent<Gacha0065Menu>();
    this.obj.Init(module[0].name, module[0].gacha[0], (Gacha0063Scene) null);
  }
}
