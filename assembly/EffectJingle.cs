﻿// Decompiled with JetBrains decompiler
// Type: EffectJingle
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class EffectJingle : MonoBehaviour
{
  public string bgm;
  private string oldBgm;

  private void Start()
  {
    this.oldBgm = Singleton<NGSoundManager>.GetInstance().GetBgmName(0);
    Singleton<NGSoundManager>.GetInstance().PlayBgm(this.bgm, 0, 0.0f, 1f, 0.3f);
  }

  private void Update()
  {
    if (Singleton<NGSoundManager>.GetInstance().IsBgmPlaying(0))
      return;
    Singleton<NGSoundManager>.GetInstance().PlayBgm(this.oldBgm, 0, 0.0f, 0.5f, 0.5f);
  }
}
