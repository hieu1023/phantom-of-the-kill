﻿// Decompiled with JetBrains decompiler
// Type: UnitIconBottomBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitIconBottomBase : MonoBehaviour
{
  public GameObject[] GearRarity;
  public UI2DSprite rarityStar;
  public GameObject textParent;
  public UILabel[] textLevels;
  public GameObject gearIcon;
  public UI2DSprite spriteFrameNormal;
  public UI2DSprite spriteFrameAwake;
  private const int USUAL = 0;
  private const int FRIENDLY = 1;

  public void Set(PlayerUnit playerUnit)
  {
    this.SetText(playerUnit.level.ToLocalizeNumberText());
    UI2DSprite[] ui2DspriteArray = new UI2DSprite[2]
    {
      this.spriteFrameNormal,
      this.spriteFrameAwake
    };
    bool[] flagArray = new bool[2]
    {
      !playerUnit.unit.awake_unit_flag,
      playerUnit.unit.awake_unit_flag
    };
    for (int index = 0; index < ui2DspriteArray.Length; ++index)
    {
      UI2DSprite ui2Dsprite = ui2DspriteArray[index];
      if ((Object) ui2Dsprite != (Object) null)
        ui2Dsprite.enabled = flagArray[index];
    }
    RarityIcon.SetRarity(playerUnit, this.rarityStar, false, false, false);
    this.StartCoroutine(this.SetGearSprite(playerUnit.unit.kind_GearKind, playerUnit.GetElement()));
  }

  public void SetText(string level)
  {
    this.textParent.SetActive(true);
    this.textLevels[0].gameObject.SetActive(true);
    this.textLevels[0].SetTextLocalize(level);
  }

  public void Set(int rarity, int kind, CommonElement element)
  {
    ((IEnumerable<GameObject>) this.GearRarity).ToggleOnce(rarity - 1);
    this.StartCoroutine(this.SetGearSprite(kind, element));
  }

  public void Set(UnitUnit unit)
  {
    this.StartCoroutine(this.SetGearSprite(unit.kind_GearKind, unit.GetElement()));
  }

  private IEnumerator SetGearSprite(int kind, CommonElement element)
  {
    Future<GameObject> SetGearPrefab = Res.Icons.GearKindIcon.Load<GameObject>();
    IEnumerator e = SetGearPrefab.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    SetGearPrefab.Result.Clone(this.gearIcon.transform).GetComponent<GearKindIcon>().Init(kind, element);
  }
}
