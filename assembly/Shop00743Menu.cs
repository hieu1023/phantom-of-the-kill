﻿// Decompiled with JetBrains decompiler
// Type: Shop00743Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Shop00743Menu : ShopArticleListMenu
{
  [SerializeField]
  private UILabel TxtOwnnumber;
  [SerializeField]
  private UILabel txtLimitMedal;
  private List<Shop00743Menu.Medal> medals;
  private GameObject detailPopupM;
  private Modified<PlayerBattleMedal[]> modified;

  public override IEnumerator Init(Future<GameObject> cellPrefab)
  {
    IEnumerator e = base.Init(cellPrefab);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    e = ServerTime.WaitSync();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.detailPopupM = (GameObject) null;
    Future<GameObject> prefabM = Res.Prefabs.popup.popup_007_4_3__anim_popup01.Load<GameObject>();
    e = prefabM.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.detailPopupM = prefabM.Result;
    this.modified = SMManager.Observe<PlayerBattleMedal[]>();
    if (this.modified.IsChangedOnce())
      this.UpdateMedal(this.modified.Value);
  }

  public virtual void IbtnBack()
  {
    if (this.IsPushAndSet())
      return;
    this.backScene();
  }

  public override void onBackButton()
  {
    this.IbtnBack();
  }

  public void onDetailPressed()
  {
    if ((UnityEngine.Object) this.detailPopupM == (UnityEngine.Object) null)
      return;
    this.StartCoroutine(this.openDetail());
  }

  private IEnumerator openDetail()
  {
    GameObject prefab = UnityEngine.Object.Instantiate<GameObject>(this.detailPopupM);
    Singleton<PopupManager>.GetInstance().open(prefab, false, false, true, true, false, false, "SE_1006");
    IEnumerator initw = prefab.GetComponent<Popup00743Menu>().Init(this.medals);
    while (initw.MoveNext())
      yield return initw.Current;
  }

  protected override void Update()
  {
    base.Update();
    if (this.modified == null || !this.modified.IsChangedOnce())
      return;
    this.UpdateMedal(this.modified.Value);
  }

  private void UpdateMedal(PlayerBattleMedal[] allmedal)
  {
    DateTime ndt = ServerTime.NowAppTime();
    this.medals = new List<Shop00743Menu.Medal>();
    int limitmedal = 0;
    int nolimitmedal = 0;
    ((IEnumerable<PlayerBattleMedal>) allmedal).Where<PlayerBattleMedal>((Func<PlayerBattleMedal, bool>) (m => !m.end_at.HasValue || m.end_at.Value > ndt)).ToList<PlayerBattleMedal>().ForEach((System.Action<PlayerBattleMedal>) (m =>
    {
      if (m.end_at.HasValue)
      {
        limitmedal += m.count;
        Shop00743Menu.Medal medal = this.medals.Find((Predicate<Shop00743Menu.Medal>) (tm => tm.limit == m.end_at.Value));
        if (medal != null)
          medal.count += m.count;
        else
          this.medals.Add(new Shop00743Menu.Medal(m.end_at.Value, m.count));
      }
      else
        nolimitmedal += m.count;
    }));
    if (this.medals.Count > 1)
      this.medals = this.medals.OrderBy<Shop00743Menu.Medal, long>((Func<Shop00743Menu.Medal, long>) (m => m.limit.Ticks)).ToList<Shop00743Menu.Medal>();
    this.medals.Add(new Shop00743Menu.Medal(new DateTime(0L), nolimitmedal));
    this.TxtOwnnumber.SetTextLocalize(SMManager.Get<Player>().battle_medal.ToString());
    this.txtLimitMedal.SetTextLocalize(string.Format(Consts.GetInstance().SHOP_00743_MEDAL_LIMIT, (object) limitmedal));
  }

  public class Medal
  {
    public DateTime limit;
    public int count;

    public Medal()
    {
      this.limit = new DateTime(0L);
      this.count = 0;
    }

    public Medal(DateTime l, int c)
    {
      this.limit = l;
      this.count = c;
    }

    public Medal(Shop00743Menu.Medal m)
    {
      this.limit = m.limit;
      this.count = m.count;
    }
  }
}
