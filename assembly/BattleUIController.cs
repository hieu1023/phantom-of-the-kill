﻿// Decompiled with JetBrains decompiler
// Type: BattleUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleUIController : BattleMonoBehaviour
{
  private List<IButtonEnableBeheviour> buttonBehaviours = new List<IButtonEnableBeheviour>();
  private bool mUIButtonEnable = true;
  public BattleInputObserver inputObserver;
  public BattleCameraController cameraController;
  public BattleUnitController unitController;
  private BattleTimeManager _btm;
  private BL.BattleModified<BL.StructValue<bool>> isViewDengerAreaModified;
  private BL.BattleModified<BL.StructValue<int>> sightModified;
  private BL.BattleModified<BL.CurrentUnit> currentUnitModified;
  private BL.BattleModified<BL.UnitPosition> unitPositionModified;

  private BattleTimeManager btm
  {
    get
    {
      if ((Object) this._btm == (Object) null)
        this._btm = this.battleManager.getManager<BattleTimeManager>();
      return this._btm;
    }
  }

  private bool isCameraMoveMode
  {
    get
    {
      return this.inputObserver.isCameraMoveMode;
    }
  }

  private bool isUnitMoveMode
  {
    get
    {
      return this.inputObserver.isUnitMoveMode;
    }
  }

  private bool isDispositionMode
  {
    get
    {
      return this.inputObserver.isDispositionMode;
    }
  }

  private void Awake()
  {
    this.inputObserver = this.gameObject.AddComponent<BattleInputObserver>();
    this.cameraController = this.gameObject.AddComponent<BattleCameraController>();
    this.unitController = this.gameObject.AddComponent<BattleUnitController>();
  }

  protected override IEnumerator Start_Battle()
  {
    // ISSUE: reference to a compiler-generated field
    int num = this.\u003C\u003E1__state;
    BattleUIController battleUiController = this;
    if (num != 0)
      return false;
    // ISSUE: reference to a compiler-generated field
    this.\u003C\u003E1__state = -1;
    battleUiController.isViewDengerAreaModified = BL.Observe<BL.StructValue<bool>>(battleUiController.env.core.isViewDengerArea);
    battleUiController.sightModified = BL.Observe<BL.StructValue<int>>(battleUiController.env.core.sight);
    battleUiController.currentUnitModified = BL.Observe<BL.CurrentUnit>(battleUiController.env.core.unitCurrent);
    battleUiController.unitPositionModified = BL.Observe<BL.UnitPosition>(battleUiController.env.core.currentUnitPosition);
    return false;
  }

  protected override void LateUpdate_Battle()
  {
    if (!this.battleManager.isBattleEnable)
      return;
    if (this.isViewDengerAreaModified.isChangedOnce())
    {
      if (this.isViewDengerAreaModified.value.value)
      {
        this.env.core.createDangerAria();
        this.env.core.viewDangerAria();
      }
      else
        this.env.core.hideDangerAria();
    }
    if (this.sightModified.isChangedOnce())
      this.cameraController.sightDistance = this.battleManager.sightDistances[this.sightModified.value.value];
    if (this.currentUnitModified.isChangedOnce() && this.currentUnitModified.value.unit != (BL.Unit) null)
    {
      this.unitPositionModified.value = this.env.core.currentUnitPosition;
      this.unitPositionModified.notifyChanged();
    }
    if (this.isDispositionMode || !this.unitPositionModified.isChangedOnce() || (this.isCameraMoveMode || this.isUnitMoveMode) || (!(this.env.core.unitCurrent.unit != (BL.Unit) null) || this.env.core.unitCurrent.unit.skillEffects.IsMoveSkillActionWaiting() && this.btm.isRunning))
      return;
    this.cameraController.setLookAtTarget(this.env.core.getFieldPanel(this.unitPositionModified.value, false), false);
  }

  public void setButtonBehaviour(IButtonEnableBeheviour behaviour)
  {
    if (this.buttonBehaviours.Contains(behaviour))
      return;
    this.buttonBehaviours.Add(behaviour);
    behaviour.buttonEnable = this.mUIButtonEnable;
  }

  public bool uiButtonEnable
  {
    get
    {
      return this.mUIButtonEnable;
    }
    set
    {
      this.mUIButtonEnable = value;
      foreach (IButtonEnableBeheviour buttonBehaviour in this.buttonBehaviours)
        buttonBehaviour.buttonEnable = value;
    }
  }

  public void uiWait()
  {
    if (this.env.core.unitCurrent.unit == (BL.Unit) null)
      return;
    BL.UnitPosition unitPosition = this.env.core.getUnitPosition(this.env.core.unitCurrent.unit);
    if (this.battleManager.useGameEngine)
      this.battleManager.gameEngine.moveUnit(unitPosition);
    else
      unitPosition.completeActionUnit(this.env.core, true, false);
  }

  public void startPreDuel(BL.UnitPosition attack, BL.UnitPosition defense)
  {
    this.battleManager.isBattleEnable = false;
    NGSceneManager instance = Singleton<NGSceneManager>.GetInstance();
    if (this.battleManager.isSea)
      instance.changeScene("BattleUI_04_sea", true, (object) attack, (object) defense);
    else
      instance.changeScene("BattleUI_04", true, (object) attack, (object) defense);
  }

  public void startHeal(BL.UnitPosition src, BL.UnitPosition dst)
  {
    this.battleManager.isBattleEnable = false;
    NGSceneManager instance = Singleton<NGSceneManager>.GetInstance();
    if (this.battleManager.isSea)
      instance.changeScene("battle019_6_1_sea", true, (object) src, (object) dst);
    else
      instance.changeScene("battle019_6_1", true, (object) src, (object) dst);
  }
}
