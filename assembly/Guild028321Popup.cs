﻿// Decompiled with JetBrains decompiler
// Type: Guild028321Popup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;
using UnityEngine;

public class Guild028321Popup : BackButtonMenuBase
{
  private Guild0283Menu menu;
  [SerializeField]
  private UILabel popupTitle;
  [SerializeField]
  private UILabel popupDesc;
  [SerializeField]
  private UILabel popupDesc2;

  public void Initialize(Guild0283Menu guild0283Menu)
  {
    if ((Object) this.GetComponent<UIWidget>() != (Object) null)
      this.GetComponent<UIWidget>().alpha = 0.0f;
    this.menu = guild0283Menu;
    this.popupTitle.SetTextLocalize(Consts.Format(Consts.GetInstance().POPUP_GUILD_MENU_DISMISS_TITLE, (IDictionary) null));
    this.popupDesc.SetTextLocalize(Consts.Format(Consts.GetInstance().POPUP_GUILD_MENU_DISMISS_DESC3, (IDictionary) null));
    this.popupDesc2.SetTextLocalize(Consts.Format(Consts.GetInstance().POPUP_GUILD_MENU_DISMISS_DESC4, (IDictionary) null));
  }

  public override void onBackButton()
  {
    Singleton<PopupManager>.GetInstance().dismiss(false);
  }

  public void onYesButton()
  {
    Singleton<PopupManager>.GetInstance().dismiss(false);
    Singleton<PopupManager>.GetInstance().open(this.menu.GuildBrakeUpConfirmPopup, false, false, false, true, false, false, "SE_1006").GetComponent<Guild028322Popup>().Initialize(this.menu);
  }
}
