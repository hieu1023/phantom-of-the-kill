﻿// Decompiled with JetBrains decompiler
// Type: EventTracker
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using com.adjust.sdk;
using Gsc.Purchase;
using SM;
using System.Collections;
using UnityEngine;

public class EventTracker : MonoBehaviour
{
  private static string[] adjustEvents = new string[2]
  {
    "-1",
    "-1"
  };
  [SerializeField]
  private string adjust_AppToken;
  [SerializeField]
  private long adjust_SecretKey_iOS;
  [SerializeField]
  private long adjust_SecretKey_Android;
  [SerializeField]
  private long[] adjust_SecretInfo_iOS;
  [SerializeField]
  private long[] adjust_SecretInfo_Android;
  [SerializeField]
  private string Growthbeat_ApplicationID;
  [SerializeField]
  private string Growthbeat_CredentialID;
  [SerializeField]
  private string Growthbeat_SenderID;

  private IEnumerator Start()
  {
    EventTracker eventTracker = this;
    GrowthPush.Environment environment = GrowthPush.Environment.Production;
    GrowthPush.GetInstance().Initialize(eventTracker.Growthbeat_ApplicationID, eventTracker.Growthbeat_CredentialID, environment, false);
    GrowthPush.GetInstance().RequestDeviceToken(eventTracker.Growthbeat_SenderID);
    eventTracker.StartCoroutine("SetDeviceToken");
    GrowthPush.GetInstance().ClearBadge();
    FaceBookWrapper.setup();
    Player player;
    while (true)
    {
      player = SMManager.Get<Player>();
      if (player == null)
        yield return (object) null;
      else
        break;
    }
    GrowthPush.GetInstance().TrackEvent("Launch");
    GrowthPush.GetInstance().SetTag("Development", "OFF");
    if (Persist.pushnotification.Data.enablePush)
      GrowthPush.GetInstance().SetTag("PushNotification", "ON");
    else
      GrowthPush.GetInstance().SetTag("PushNotification", "OFF");
    GrowthPush.GetInstance().SetTag("PLAYER_LEVEL", player.level.ToString());
    GrowthPush.GetInstance().SetTag("PLAYER_CONTINUATUIN", player.continuation_date.ToString());
    GrowthPush.GetInstance().SetTag("HAVE_COINS", (player.paid_coin + player.free_coin).ToString());
    GrowthPush.GetInstance().SetTag("HAVE_FREE_COINS", player.free_coin.ToString());
    GrowthPush.GetInstance().SetTag("HAVE_PAID_COINS", player.paid_coin.ToString());
  }

  private IEnumerator SetDeviceToken()
  {
    yield break;
  }

  public static void SendPayment(ProductInfo product)
  {
    AdjustEvent adjustEvent = new AdjustEvent("sxcah5");
    adjustEvent.setRevenue((double) product.Price, product.CurrencyCode);
    Adjust.trackEvent(adjustEvent);
    FaceBookWrapper.Purchase(product.Price, product.CurrencyCode, product.ProductId);
    GrowthPush.GetInstance().TrackEvent("BUY_COIN");
    Player player = SMManager.Get<Player>();
    GrowthPush.GetInstance().SetTag("HAVE_COINS", (player.paid_coin + player.free_coin).ToString());
    GrowthPush.GetInstance().SetTag("HAVE_FREE_COINS", player.free_coin.ToString());
    GrowthPush.GetInstance().SetTag("HAVE_PAID_COINS", player.paid_coin.ToString());
    MetapsAnalyticsScript.TrackPurchase(product.ProductId, (double) product.Price, product.CurrencyCode);
  }

  public static void BeaconTutorial(string name, int seconds)
  {
    MetapsAnalyticsScript.TrackEvent("TUTORIAL", name);
  }

  public static void TrackEvent(string category, string name, int value)
  {
    MetapsAnalyticsScript.TrackEvent(category, name, value);
  }

  public static void TrackEvent(string category, string name, string id, int value)
  {
    MetapsAnalyticsScript.TrackEvent(category, string.Format("{0}_{1}", (object) name, (object) id), value);
  }

  public static void TrackSpend(string category, string name, int value)
  {
    MetapsAnalyticsScript.TrackSpend(category, name, value);
  }

  public static void SendEvent(EventTracker.EventType e)
  {
    Adjust.trackEvent(new AdjustEvent(EventTracker.adjustEvents[(int) e]));
    if (e != EventTracker.EventType.FINISH_FIRST_DLC)
    {
      if (e != EventTracker.EventType.FINISH_TUTORIAL)
        return;
      MetapsAnalyticsScript.TrackEvent("TUTORIAL", "FINISH");
    }
    else
      MetapsAnalyticsScript.TrackEvent("FIRST_DLC", "FINISH");
  }

  public enum EventType
  {
    FINISH_FIRST_DLC,
    FINISH_TUTORIAL,
  }
}
