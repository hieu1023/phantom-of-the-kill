﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable_QuestExtraSExtension
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using System;
using System.Collections.Generic;
using System.Linq;

public static class MasterDataTable_QuestExtraSExtension
{
  public static QuestExtraS[] LL(this QuestExtraS[] self, int ll)
  {
    QuestExtraLL questLl;
    return ((IEnumerable<QuestExtraS>) self).Where<QuestExtraS>((Func<QuestExtraS, bool>) (x => (questLl = x.quest_ll) != null && questLl.ID == ll)).ToArray<QuestExtraS>();
  }
}
