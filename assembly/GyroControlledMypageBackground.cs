﻿// Decompiled with JetBrains decompiler
// Type: GyroControlledMypageBackground
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[RequireComponent(typeof (RuntimeImageEffectController))]
public class GyroControlledMypageBackground : GyroControlledObject
{
  public float maxInterpolationProgressRate = 1f;
  private RuntimeImageEffectController imageEffectController;
  private float originalInterpolationProgress;

  public override void OnControllerStart()
  {
    this.imageEffectController = this.GetComponent<RuntimeImageEffectController>();
    this.originalInterpolationProgress = this.imageEffectController.interpolationProgress;
  }

  public override void OnLerp(float? tX, float? tY)
  {
    if (!tX.HasValue && tY.HasValue)
      this.imageEffectController.interpolationProgress = (float) (-(double) Mathf.Cos((float) (((double) tY.Value * 2.0 - 1.0) * 3.14159274101257)) * 0.5 + 0.5) * this.maxInterpolationProgressRate;
    else if (tX.HasValue && !tY.HasValue)
    {
      this.imageEffectController.interpolationProgress = (float) (-(double) Mathf.Cos((float) (((double) tX.Value * 2.0 - 1.0) * 3.14159274101257)) * 0.5 + 0.5) * this.maxInterpolationProgressRate;
    }
    else
    {
      if (!tX.HasValue || !tY.HasValue)
        return;
      if ((double) Mathf.Abs(tX.Value - 0.5f) > (double) Mathf.Abs(tY.Value - 0.5f))
        this.imageEffectController.interpolationProgress = (float) (-(double) Mathf.Cos((float) (((double) tX.Value * 2.0 - 1.0) * 3.14159274101257)) * 0.5 + 0.5) * this.maxInterpolationProgressRate;
      else
        this.imageEffectController.interpolationProgress = (float) (-(double) Mathf.Cos((float) (((double) tY.Value * 2.0 - 1.0) * 3.14159274101257)) * 0.5 + 0.5) * this.maxInterpolationProgressRate;
    }
  }

  public override void OnControllerStop()
  {
    if (!((Object) this.imageEffectController != (Object) null))
      return;
    this.imageEffectController.interpolationProgress = this.originalInterpolationProgress;
  }
}
