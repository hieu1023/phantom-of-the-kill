﻿// Decompiled with JetBrains decompiler
// Type: BattlePrepareCharacterInfoEnemy
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;
using UnityEngine;

public class BattlePrepareCharacterInfoEnemy : BattlePrepareCharacterInfoBase
{
  [SerializeField]
  private GameObject commandGameObject;
  [SerializeField]
  private BattleUI04CommandPrefab commandComponent;

  public override IEnumerator Init(
    BL.UnitPosition up,
    AttackStatus[] attacks,
    bool isFacility)
  {
    BattlePrepareCharacterInfoEnemy characterInfoEnemy = this;
    if (up == null)
    {
      Debug.LogWarning((object) "unit is null");
    }
    else
    {
      // ISSUE: reference to a compiler-generated method
      IEnumerator e = characterInfoEnemy.\u003C\u003En__0(up, attacks, isFacility);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      AttackStatus attack = BattleFuncs.selectCounterAttackStatus(attacks);
      if (attack != null)
      {
        characterInfoEnemy.commandGameObject.SetActive(true);
        yield return (object) characterInfoEnemy.commandComponent.InitOppoSide(attack, up.unit);
      }
      else
        characterInfoEnemy.commandGameObject.SetActive(false);
      characterInfoEnemy.setCurrentAttack(attack);
      attack = (AttackStatus) null;
    }
  }

  protected override ResourceObject maskResource()
  {
    return Res.GUI._009_3_sozai.mask_Chara_R;
  }
}
