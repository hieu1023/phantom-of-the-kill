﻿// Decompiled with JetBrains decompiler
// Type: Sea030StoryTowerButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Sea030StoryTowerButton : MonoBehaviour
{
  [SerializeField]
  private TweenPosition tweenPosition;
  private bool animReverse;
  private int ButtonMnumber;
  private int ButtonLnumber;
  private int _TweenIndex;

  public int Mnumber
  {
    get
    {
      return this.ButtonMnumber;
    }
    set
    {
      this.ButtonMnumber = value;
    }
  }

  public int Lnumber
  {
    get
    {
      return this.ButtonLnumber;
    }
    set
    {
      this.ButtonLnumber = value;
    }
  }

  public int TweenIndex
  {
    get
    {
      return this._TweenIndex;
    }
    set
    {
      this._TweenIndex = value;
    }
  }

  public void changeAnimDirection()
  {
    if (!((Object) this.tweenPosition != (Object) null))
      return;
    this.tweenPosition.to.y = -this.tweenPosition.to.y;
    this.tweenPosition.delay = 0.0f;
    this.animReverse = !this.animReverse;
  }

  public void initTween()
  {
    if (!this.animReverse)
      return;
    this.changeAnimDirection();
  }
}
