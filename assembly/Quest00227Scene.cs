﻿// Decompiled with JetBrains decompiler
// Type: Quest00227Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;
using System.Collections;
using UnityEngine;

public class Quest00227Scene : NGSceneBase
{
  [SerializeField]
  private Quest00227Menu menu;

  public static void ChangeScene(QuestScoreCampaignProgress qscp, bool stack)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("quest002_27", (stack ? 1 : 0) != 0, (object) qscp);
  }

  public IEnumerator onStartSceneAsync(QuestScoreCampaignProgress qscp)
  {
    IEnumerator e = this.menu.Initialize(qscp);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }
}
