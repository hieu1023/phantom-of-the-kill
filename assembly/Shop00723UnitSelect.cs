﻿// Decompiled with JetBrains decompiler
// Type: Shop00723UnitSelect
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Shop00723UnitSelect : MonoBehaviour
{
  [SerializeField]
  private UILabel txtName_;
  [SerializeField]
  private GameObject topIcon_;
  [SerializeField]
  private UIButton btnSkill_;
  [SerializeField]
  private UIButton btnSelect_;
  [SerializeField]
  private UILabel txtCount_;
  private Shop00723Menu menu_;
  private SelectTicketSelectSample sample_;
  private int limitCount;

  public IEnumerator coInitialize(
    Shop00723Menu menu,
    SelectTicketSelectSample unitSample,
    PlayerSelectTicketSummary playerUnitTicket,
    SM.SelectTicket unitTicket)
  {
    this.menu_ = menu;
    this.sample_ = unitSample;
    UnitUnit unitUnit = (UnitUnit) null;
    if (MasterData.UnitUnit.TryGetValue(this.sample_.reward_id, out unitUnit))
    {
      this.txtName_.SetTextLocalize(unitUnit.name);
    }
    else
    {
      this.txtName_.SetTextLocalize("");
      Debug.LogError((object) ("Key Not Found: " + (object) this.sample_.reward_id));
    }
    this.UpdateLimitCountLabel(playerUnitTicket, unitTicket, false);
    this.SetButtonEnabled(playerUnitTicket.quantity, unitTicket.exchange_limit, this.limitCount);
    UnitIcon ui = menu.prefabIconUnit.Clone(this.topIcon_.transform).GetComponent<UnitIcon>();
    UnitUnit unit = unitUnit;
    IEnumerator e = ui.SetUnit(unit, unit.GetElement(), false);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    ui.setLevelText(Consts.GetInstance().SHOP_00723_UNIT_LEVEL);
    ui.ShowBottomInfo(UnitSortAndFilter.SORT_TYPES.Level);
    ui.buttonBoxCollider.enabled = false;
  }

  public void UpdateInfo(
    PlayerSelectTicketSummary playerUnitTicket,
    SelectTicketSelectSample exchangeUnitSample,
    int ticketQuantity,
    SM.SelectTicket unitTicket)
  {
    if (this.sample_ == exchangeUnitSample)
      this.UpdateLimitCountLabel(playerUnitTicket, unitTicket, true);
    this.SetButtonEnabled(ticketQuantity, unitTicket.exchange_limit, this.limitCount);
  }

  private void UpdateLimitCountLabel(
    PlayerSelectTicketSummary playerUnitTicket,
    SM.SelectTicket unitTicket,
    bool isInited = false)
  {
    bool exchangeLimit = unitTicket.exchange_limit;
    if (exchangeLimit)
      this.limitCount = this.UpdateLimitCount(playerUnitTicket, unitTicket, isInited, this.limitCount);
    this.SetLimitCountLabel(exchangeLimit, this.limitCount);
  }

  private int UpdateLimitCount(
    PlayerSelectTicketSummary playerUnitTicket,
    SM.SelectTicket unitTicket,
    bool isInited,
    int count)
  {
    int num;
    if (isInited)
    {
      num = count - 1;
    }
    else
    {
      UnitUnit value = (UnitUnit) null;
      if (!MasterData.UnitUnit.TryGetValue(this.sample_.reward_id, out value))
        Debug.LogError((object) ("Key Not Found: " + (object) this.sample_.reward_id));
      SelectTicketChoices selectTicketChoices = ((IEnumerable<SelectTicketChoices>) unitTicket.choices).FirstOrDefault<SelectTicketChoices>((Func<SelectTicketChoices, bool>) (u => u.reward_id == value.ID));
      if (selectTicketChoices == null)
        return 0;
      PlayerSelectTicketSummaryPlayer_exchange_count_list exchangeCountList = ((IEnumerable<PlayerSelectTicketSummaryPlayer_exchange_count_list>) playerUnitTicket.player_exchange_count_list).FirstOrDefault<PlayerSelectTicketSummaryPlayer_exchange_count_list>((Func<PlayerSelectTicketSummaryPlayer_exchange_count_list, bool>) (u => u.reward_id == value.ID));
      num = exchangeCountList == null ? selectTicketChoices.exchangeable_count.Value : selectTicketChoices.exchangeable_count.Value - exchangeCountList.exchange_count;
    }
    return num;
  }

  private void SetLimitCountLabel(bool isLimit, int count)
  {
    if (isLimit)
      this.txtCount_.SetTextLocalize(string.Format(Consts.GetInstance().SHOP_00723_UNIT_SELECT_UNIT_COUNT, (object) count));
    else
      this.txtCount_.gameObject.SetActive(false);
  }

  private void SetButtonEnabled(int quantity, bool isLimit, int count = 0)
  {
    bool flag = true;
    if (isLimit)
      flag = count > 0;
    this.btnSelect_.isEnabled = flag && quantity > 0;
  }

  public void onClickSkill()
  {
    this.menu_.onClickSkill(this.sample_);
  }

  public void onClickSelect()
  {
    this.menu_.onClickSelect(this.sample_);
  }

  public SelectTicketSelectSample SampleUnit
  {
    get
    {
      return this.sample_;
    }
  }
}
