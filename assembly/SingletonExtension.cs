﻿// Decompiled with JetBrains decompiler
// Type: SingletonExtension
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public static class SingletonExtension
{
  public static void SingletonDestory(this GameObject self)
  {
    foreach (SingletonBase componentsInChild in self.GetComponentsInChildren<SingletonBase>(true))
      componentsInChild.forceDestroy();
    Object.Destroy((Object) self);
  }
}
