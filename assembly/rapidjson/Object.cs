﻿// Decompiled with JetBrains decompiler
// Type: rapidjson.Object
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

namespace rapidjson
{
  public class Object : IEnumerable<KeyValuePair<string, Value>>, IEnumerable
  {
    private IntPtr root;
    private readonly Document doc;
    private readonly uint size;

    public int MemberCount
    {
      get
      {
        return (int) this.size;
      }
    }

    public Object(Document doc, ref IntPtr ptr)
    {
      doc.CheckDisposed();
      if (!DLL._rapidjson_get_object_member_count(ptr, out this.size))
        throw new InvalidOperationException("Not Object Type.");
      this.doc = doc;
      this.root = ptr;
    }

    public IEnumerator<KeyValuePair<string, Value>> GetEnumerator()
    {
      for (uint i = 0; i < this.size; ++i)
      {
        this.doc.CheckDisposed();
        string key;
        IntPtr ptr;
        DLL._rapidjson_get_key_value_pair_by_object_index(this.root, i, out key, out ptr);
        yield return new KeyValuePair<string, Value>(key, new Value(this.doc, ref ptr));
      }
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
      return (IEnumerator) this.GetEnumerator();
    }

    public Value this[string name]
    {
      get
      {
        Value obj;
        if (!this.TryGetValue(name, out obj))
          throw new KeyNotFoundException();
        return obj;
      }
    }

    public bool TryGetValue(string name, out Value value)
    {
      this.doc.CheckDisposed();
      IntPtr dst = IntPtr.Zero;
      bool flag = DLL.TryGet(ref this.root, name, out dst);
      value = new Value(flag ? this.doc : (Document) null, ref dst);
      return flag;
    }

    public bool HasMember(string name)
    {
      this.doc.CheckDisposed();
      IntPtr dst = IntPtr.Zero;
      return DLL.TryGet(ref this.root, name, out dst);
    }
  }
}
