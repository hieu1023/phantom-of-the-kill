﻿// Decompiled with JetBrains decompiler
// Type: LumpToutaMaterialConfirmScene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LumpToutaMaterialConfirmScene : NGSceneBase
{
  [SerializeField]
  private LumpToutaMaterialConfirmMenu menu;
  private bool isInit;

  public IEnumerator onStartSceneAsync(
    List<PlayerUnit> selectedBasePlayerUnits,
    Dictionary<PlayerUnit, List<PlayerUnit>> allSamePlayerUnits)
  {
    if (!this.isInit)
    {
      yield return (object) this.menu.StartAsync(selectedBasePlayerUnits, allSamePlayerUnits);
      this.isInit = true;
    }
  }
}
