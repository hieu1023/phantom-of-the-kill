﻿// Decompiled with JetBrains decompiler
// Type: DuelCutin
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System.Collections;
using UnityEngine;

public class DuelCutin : MonoBehaviour
{
  private UnitUnit unitData;
  private NGSoundManager sm;
  private Animator am;
  public float criticalVoiceDelay;
  public float skillVoiceDelay;
  public bool isTexExist;
  public EffectSE sePlayer;
  private bool combineCutinError;
  private float spent;
  private const int SKILL_VOICE_ID = 31;
  private const int CRITICAL_VOICE_ID = 32;
  [SerializeField]
  private GameObject cameraCutin;
  [SerializeField]
  private MeshRenderer[] cutinObjects;
  private Texture cutinTexture;

  public GameObject CameraCutin
  {
    get
    {
      return this.cameraCutin;
    }
  }

  public IEnumerator Initialize<T>(T unit)
  {
    DuelCutin duelCutin = this;
    duelCutin.isTexExist = false;
    duelCutin.combineCutinError = false;
    duelCutin.GetUnitUnit<T>(unit);
    duelCutin.sm = Singleton<NGSoundManager>.GetInstance();
    duelCutin.am = duelCutin.gameObject.GetComponent<Animator>();
    if ((Object) duelCutin.cutinObjects[0] != (Object) null)
    {
      Future<UnityEngine.Sprite> fs = duelCutin.unitData.LoadCutin();
      IEnumerator e = fs.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      Texture texture = (Texture) fs.Result.texture;
      if ((Object) texture != (Object) null)
      {
        duelCutin.cutinTexture = texture;
        duelCutin.cutinObjects[0].materials[0].SetTexture("_MainTex", duelCutin.cutinTexture);
        duelCutin.isTexExist = true;
      }
      fs = (Future<UnityEngine.Sprite>) null;
    }
  }

  public IEnumerator InitializeForRoulette(int cutInPattern)
  {
    DuelCutin duelCutin = this;
    duelCutin.isTexExist = false;
    duelCutin.combineCutinError = false;
    if ((Object) duelCutin.cutinObjects[0] != (Object) null)
    {
      Future<UnityEngine.Sprite> fs = (Future<UnityEngine.Sprite>) null;
      switch (cutInPattern)
      {
        case 1:
        case 2:
          fs = Singleton<ResourceManager>.GetInstance().Load<UnityEngine.Sprite>("AssetBundle/Resources/Characters/ottimo/ottimo_cutin", 1f);
          break;
        case 3:
          duelCutin.unitData = MasterData.UnitUnit[3103811];
          fs = duelCutin.unitData.LoadCutin();
          break;
      }
      IEnumerator e = fs.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      Texture texture = (Texture) fs.Result.texture;
      if ((Object) texture != (Object) null)
      {
        duelCutin.cutinTexture = texture;
        duelCutin.cutinObjects[0].materials[0].SetTexture("_MainTex", duelCutin.cutinTexture);
        duelCutin.isTexExist = true;
      }
      fs = (Future<UnityEngine.Sprite>) null;
    }
    duelCutin.sm = Singleton<NGSoundManager>.GetInstance();
    duelCutin.am = duelCutin.gameObject.GetComponent<Animator>();
  }

  public void SetCutinTexture(Texture tex, DuelCutin.CUTINPOS pos)
  {
    if ((DuelCutin.CUTINPOS) this.cutinObjects.Length <= pos)
    {
      Debug.LogError((object) "[SetCutinTexture]Index Error");
      this.combineCutinError = true;
    }
    else if ((Object) this.cutinObjects[(int) pos] != (Object) null && (Object) tex != (Object) null)
      this.cutinObjects[(int) pos].materials[0].SetTexture("_MainTex", tex);
    else
      this.combineCutinError = true;
  }

  public IEnumerator LoadAndSetTexture(int unitId, DuelCutin.CUTINPOS pos)
  {
    UnitUnit unitUnit;
    try
    {
      unitUnit = MasterData.UnitUnit[unitId];
    }
    catch
    {
      Debug.LogError((object) "[LoadAndSetTexture] UnitId Not Found");
      this.combineCutinError = true;
      yield break;
    }
    if (unitUnit != null)
    {
      Future<UnityEngine.Sprite> fs = unitUnit.LoadCutin();
      IEnumerator e = fs.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      Texture texture = (Texture) fs.Result.texture;
      if ((Object) texture != (Object) null)
        this.SetCutinTexture(texture, pos);
    }
  }

  private void GetUnitUnit<T>(T unit)
  {
    if ((object) ((object) unit as BL.Unit) != null)
      this.unitData = ((object) unit as BL.Unit).unit;
    else if ((object) ((object) unit as PlayerUnit) != null)
    {
      this.unitData = ((object) unit as PlayerUnit).unit;
    }
    else
    {
      this.unitData = (UnitUnit) null;
      Debug.LogError((object) "CLASS IS UNEXPECTED!!");
    }
  }

  public void PlayCriticalCutin()
  {
    this.am.SetTrigger("cutin_start");
    this.cutinObjects[0].materials[0].SetTexture("_MainTex", this.cutinTexture);
    if (this.unitData.unitVoicePattern != null)
      this.sm.playVoiceByID(this.unitData.unitVoicePattern, 74, -1, this.criticalVoiceDelay);
    if (!((Object) this.sePlayer != (Object) null))
      return;
    this.sePlayer.playSe();
  }

  public void PlaySkillCutin(DuelCutin.PlayMode mode = DuelCutin.PlayMode.FIRST_PERSON)
  {
    if (this.combineCutinError)
      mode = DuelCutin.PlayMode.NONE;
    switch (mode)
    {
      case DuelCutin.PlayMode.FIRST_PERSON:
        this.am.SetTrigger("cutin_start");
        break;
      case DuelCutin.PlayMode.SECOND_PERSON:
        this.am.SetTrigger("cutin_2start");
        break;
      case DuelCutin.PlayMode.THIRD_PERSON:
        this.am.SetTrigger("cutin_3start");
        break;
      default:
        this.am.SetTrigger("cutin_start");
        break;
    }
    if (this.unitData.unitVoicePattern != null)
      this.sm.playVoiceByID(this.unitData.unitVoicePattern, 73, -1, this.skillVoiceDelay);
    if (!((Object) this.sePlayer != (Object) null))
      return;
    this.sePlayer.playSe();
  }

  public void PlaySkillCutInForRoulette(int clipID)
  {
    this.am.SetTrigger("cutin_start");
    this.sm.playVoiceByID("VO_9001", clipID, -1, 0.0f, (string) null);
    if (!((Object) this.sePlayer != (Object) null))
      return;
    this.sePlayer.playSe();
  }

  public enum PlayMode
  {
    NONE,
    FIRST_PERSON,
    SECOND_PERSON,
    THIRD_PERSON,
  }

  public enum CUTINPOS
  {
    TOP,
    CENTER,
    BOTTOM,
  }
}
