﻿// Decompiled with JetBrains decompiler
// Type: DetailUnitParameter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnitStatusInformation;
using UnityEngine;

public class DetailUnitParameter : MonoBehaviour
{
  private Color bgColor = Color.white;
  private readonly Dictionary<CommonElement, Color32> JobBgColors = new Dictionary<CommonElement, Color32>()
  {
    {
      CommonElement.none,
      new Color32((byte) 0, (byte) 0, (byte) 0, byte.MaxValue)
    },
    {
      CommonElement.fire,
      new Color32((byte) 186, (byte) 10, (byte) 10, byte.MaxValue)
    },
    {
      CommonElement.wind,
      new Color32((byte) 11, (byte) 152, (byte) 31, byte.MaxValue)
    },
    {
      CommonElement.ice,
      new Color32((byte) 0, (byte) 117, (byte) 178, byte.MaxValue)
    },
    {
      CommonElement.thunder,
      new Color32((byte) 247, (byte) 163, (byte) 0, byte.MaxValue)
    },
    {
      CommonElement.light,
      new Color32((byte) 190, (byte) 186, (byte) 153, byte.MaxValue)
    },
    {
      CommonElement.dark,
      new Color32((byte) 141, (byte) 2, (byte) 238, byte.MaxValue)
    }
  };
  private readonly Dictionary<CommonElement, Color32> CharactorSkillBgColors = new Dictionary<CommonElement, Color32>()
  {
    {
      CommonElement.none,
      new Color32((byte) 0, (byte) 0, (byte) 0, byte.MaxValue)
    },
    {
      CommonElement.fire,
      new Color32((byte) 157, (byte) 9, (byte) 9, byte.MaxValue)
    },
    {
      CommonElement.wind,
      new Color32((byte) 10, (byte) 141, (byte) 29, byte.MaxValue)
    },
    {
      CommonElement.ice,
      new Color32((byte) 0, (byte) 100, (byte) 152, byte.MaxValue)
    },
    {
      CommonElement.thunder,
      new Color32((byte) 203, (byte) 134, (byte) 0, byte.MaxValue)
    },
    {
      CommonElement.light,
      new Color32((byte) 144, (byte) 141, (byte) 112, byte.MaxValue)
    },
    {
      CommonElement.dark,
      new Color32((byte) 121, (byte) 0, (byte) 206, byte.MaxValue)
    }
  };
  private readonly Dictionary<CommonElement, Color32> BgColors = new Dictionary<CommonElement, Color32>()
  {
    {
      CommonElement.none,
      new Color32((byte) 0, (byte) 0, (byte) 0, byte.MaxValue)
    },
    {
      CommonElement.fire,
      new Color32((byte) 231, (byte) 66, (byte) 66, byte.MaxValue)
    },
    {
      CommonElement.wind,
      new Color32((byte) 73, (byte) 172, (byte) 87, byte.MaxValue)
    },
    {
      CommonElement.ice,
      new Color32((byte) 62, (byte) 140, (byte) 180, byte.MaxValue)
    },
    {
      CommonElement.thunder,
      new Color32((byte) 244, (byte) 194, (byte) 96, byte.MaxValue)
    },
    {
      CommonElement.light,
      new Color32((byte) 216, (byte) 212, (byte) 175, byte.MaxValue)
    },
    {
      CommonElement.dark,
      new Color32((byte) 182, (byte) 88, byte.MaxValue, byte.MaxValue)
    }
  };
  [SerializeField]
  private DetailUnitParameter.UnitObjectData[] unitObjects;
  [SerializeField]
  private DetailUnitParameter.SkillObject[] skillObjects;
  [SerializeField]
  private UISprite bgColorSprite;
  [SerializeField]
  private UILabel unitName;
  [SerializeField]
  private UIWidget unitTypeIconRoot;
  [SerializeField]
  private UI2DSprite raritySprite;
  [SerializeField]
  private GameObject slcAwakening;
  [SerializeField]
  private UISprite slcCountry;
  private GameObject battleSkillIconPrefab;
  private GameObject skillDetailPrefab;
  private List<PopupSkillDetails.Param> skillParams;

  public IEnumerator Init(int unitID, int position)
  {
    if (MasterData.UnitUnit.ContainsKey(unitID))
    {
      UnitUnit unit = MasterData.UnitUnit[unitID];
      Future<GameObject> f = Res.Prefabs.BattleSkillIcon._battleSkillIcon.Load<GameObject>();
      IEnumerator e = f.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.battleSkillIconPrefab = f.Result;
      this.skillParams = new List<PopupSkillDetails.Param>();
      if ((UnityEngine.Object) this.skillDetailPrefab == (UnityEngine.Object) null)
      {
        Future<GameObject> loader = PopupSkillDetails.createPrefabLoader(false);
        yield return (object) loader.Wait();
        this.skillDetailPrefab = loader.Result;
        loader = (Future<GameObject>) null;
      }
      if ((UnityEngine.Object) this.slcAwakening != (UnityEngine.Object) null)
      {
        this.slcAwakening.SetActive(false);
        if (unit.awake_unit_flag)
          this.slcAwakening.SetActive(true);
      }
      if ((UnityEngine.Object) this.slcCountry != (UnityEngine.Object) null)
      {
        this.slcCountry.gameObject.SetActive(false);
        if (unit.country_attribute.HasValue)
        {
          this.slcCountry.gameObject.SetActive(true);
          unit.SetCuntrySpriteName(ref this.slcCountry);
        }
      }
      this.unitName.SetTextLocalize(unit.name);
      GearKindIcon.GetPrefab().CloneAndGetComponent<GearKindIcon>(this.unitTypeIconRoot.gameObject).Init(unit.kind, unit.GetElement());
      this.bgColorSprite.color = (Color) this.BgColors[unit.GetElement()];
      this.raritySprite.sprite2D = RarityIcon.GetSprite(unit, true, false, false);
      UI2DSprite raritySprite1 = this.raritySprite;
      Rect textureRect = this.raritySprite.sprite2D.textureRect;
      int width = (int) textureRect.width;
      raritySprite1.width = width;
      UI2DSprite raritySprite2 = this.raritySprite;
      textureRect = this.raritySprite.sprite2D.textureRect;
      int height = (int) textureRect.height;
      raritySprite2.height = height;
      foreach (DetailUnitParameter.UnitObjectData unitObject in this.unitObjects)
        unitObject.rootObject.SetActive(false);
      int index1 = position == 9 ? 1 : 0;
      e = this.InitUnitObject(unit, this.unitObjects[index1]);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      UnitLeaderSkill leaderSkill = ((IEnumerable<UnitLeaderSkill>) MasterData.UnitLeaderSkillList).FirstOrDefault<UnitLeaderSkill>((Func<UnitLeaderSkill, bool>) (x => x.unit.ID == unitID));
      List<UnitSkillCharacterQuest> list = ((IEnumerable<UnitSkillCharacterQuest>) MasterData.UnitSkillCharacterQuestList).Where<UnitSkillCharacterQuest>((Func<UnitSkillCharacterQuest, bool>) (x => x.unit.ID == unitID && x.skill.skill_type != BattleskillSkillType.leader)).OrderBy<UnitSkillCharacterQuest, int>((Func<UnitSkillCharacterQuest, int>) (x => x.character_quest.priority)).ToList<UnitSkillCharacterQuest>();
      foreach (DetailUnitParameter.SkillObject skillObject in this.skillObjects)
        skillObject.rootObject.SetActive(false);
      int index2 = -1;
      if (leaderSkill != null && (list.Count == 2 || list.Count == 1 && list[0].skillOfEvolution != null))
        index2 = 2;
      else if (leaderSkill != null && list.Count == 1)
        index2 = 1;
      else if (leaderSkill != null)
        index2 = 0;
      if (index2 >= 0)
      {
        e = this.InitSkillObject(this.skillObjects[index2], leaderSkill, list, unit.GetElement());
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
      }
    }
  }

  private IEnumerator InitUnitObject(
    UnitUnit unit,
    DetailUnitParameter.UnitObjectData data)
  {
    data.rootObject.SetActive(true);
    Future<UnityEngine.Sprite> f = unit.LoadSpriteLarge(1f);
    IEnumerator e = f.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    data.charaSprite.MainUI2DSprite.sprite2D = f.Result;
    data.charaSprite.FitMask();
    data.charaShadowSprite.MainUI2DSprite.sprite2D = f.Result;
    data.charaShadowSprite.FitMask();
    data.jobBaseSprite.color = (Color) this.JobBgColors[unit.GetElement()];
    data.jobFramePatternSprite.color = (Color) this.JobBgColors[unit.GetElement()];
    data.jobName.SetTextLocalize(unit.job.name);
    IOrderedEnumerable<UnitSkill> skills = ((IEnumerable<UnitSkill>) unit.RememberUnitSkills(0)).Where<UnitSkill>((Func<UnitSkill, bool>) (x => x.skill.skill_type != BattleskillSkillType.growth && x.skill.DispSkillList)).OrderBy<UnitSkill, int>((Func<UnitSkill, int>) (x => x.level));
    int skillsCount = skills.Count<UnitSkill>();
    for (int i = 0; i < data.linkSkillIcons.Length && skillsCount > i; ++i)
    {
      BattleskillSkill skill = skills.ElementAt<UnitSkill>(i).skill;
      UIWidget rootObj = data.linkSkillIcons[i];
      BattleSkillIcon battleSkillIcon = this.battleSkillIconPrefab.CloneAndGetComponent<BattleSkillIcon>(rootObj.gameObject);
      e = battleSkillIcon.Init(skill);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      battleSkillIcon.SetDepth(rootObj.depth);
      this.skillParams.Add(new PopupSkillDetails.Param(skill, UnitParameter.SkillGroup.None, new int?()));
      UIButton component = rootObj.gameObject.GetComponent<UIButton>();
      if ((UnityEngine.Object) component != (UnityEngine.Object) null)
        this.setEventPopupSkillDetail(component, skill);
      skill = (BattleskillSkill) null;
      rootObj = (UIWidget) null;
      battleSkillIcon = (BattleSkillIcon) null;
    }
  }

  private IEnumerator InitSkillObject(
    DetailUnitParameter.SkillObject skillObject,
    UnitLeaderSkill leaderSkill,
    List<UnitSkillCharacterQuest> charactorSkills,
    CommonElement element)
  {
    skillObject.rootObject.SetActive(true);
    BattleskillSkill s = leaderSkill.skill;
    skillObject.leaderSkillObject.leaderSkillDiscription.SetTextLocalize(s.description);
    this.skillParams.Add(new PopupSkillDetails.Param(s, UnitParameter.SkillGroup.Leader, new int?()));
    this.setEventPopupSkillDetail(skillObject.leaderSkillObject.btnZoom, s);
    UnitSkillCharacterQuest charactorSkill;
    DetailUnitParameter.CharactorSkillObject cso;
    BattleSkillIcon battleSkillIcon;
    IEnumerator e;
    if (charactorSkills != null && charactorSkills.Count == 1)
    {
      charactorSkill = charactorSkills[0];
      if (skillObject.charactorSkillObjects.Length >= 1)
      {
        cso = skillObject.charactorSkillObjects[0];
        s = charactorSkill.skill;
        cso.skillName.SetTextLocalize(s.name);
        cso.skillDiscription.SetTextLocalize(s.description);
        battleSkillIcon = this.battleSkillIconPrefab.CloneAndGetComponent<BattleSkillIcon>(cso.skillIconRoot.gameObject);
        e = battleSkillIcon.Init(s);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        battleSkillIcon.SetDepth(cso.skillIconRoot.depth);
        cso.skillEntryPoint.SetTextLocalize(Consts.Format(Consts.GetInstance().DETAIL_UNIT_PARAMETER_GET_SKILL, (IDictionary) new Hashtable()
        {
          {
            (object) "episode",
            (object) charactorSkill.character_quest.priority
          }
        }));
        cso.skillBGColor.color = (Color) this.CharactorSkillBgColors[element];
        this.skillParams.Add(new PopupSkillDetails.Param(s, UnitParameter.SkillGroup.None, new int?()));
        this.setEventPopupSkillDetail(cso.btnZoom, s);
        cso = new DetailUnitParameter.CharactorSkillObject();
        battleSkillIcon = (BattleSkillIcon) null;
      }
      if (skillObject.charactorSkillObjects.Length >= 2 && charactorSkill.skillOfEvolution != null)
      {
        cso = skillObject.charactorSkillObjects[1];
        s = charactorSkill.skillOfEvolution;
        cso.skillName.SetTextLocalize(s.name);
        cso.skillDiscription.SetTextLocalize(s.description);
        battleSkillIcon = this.battleSkillIconPrefab.CloneAndGetComponent<BattleSkillIcon>(cso.skillIconRoot.gameObject);
        e = battleSkillIcon.Init(s);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        battleSkillIcon.SetDepth(cso.skillIconRoot.depth);
        if (charactorSkill.evolution_character_quest != null)
          cso.skillEntryPoint.SetTextLocalize(Consts.Format(Consts.GetInstance().DETAIL_UNIT_PARAMETER_EVO_SKILL, (IDictionary) new Hashtable()
          {
            {
              (object) "episode",
              (object) charactorSkill.evolution_character_quest.priority
            }
          }));
        else
          cso.skillEntryPoint.gameObject.SetActive(false);
        cso.skillBGColor.color = (Color) this.CharactorSkillBgColors[element];
        this.skillParams.Add(new PopupSkillDetails.Param(s, UnitParameter.SkillGroup.None, new int?()));
        this.setEventPopupSkillDetail(cso.btnZoom, s);
        cso = new DetailUnitParameter.CharactorSkillObject();
        battleSkillIcon = (BattleSkillIcon) null;
      }
      charactorSkill = (UnitSkillCharacterQuest) null;
    }
    else if (charactorSkills != null && charactorSkills.Count == 2)
    {
      charactorSkill = charactorSkills[0];
      if (skillObject.charactorSkillObjects.Length >= 1)
      {
        cso = skillObject.charactorSkillObjects[0];
        s = charactorSkill.skill;
        cso.skillName.SetTextLocalize(s.name);
        cso.skillDiscription.SetTextLocalize(s.description);
        battleSkillIcon = this.battleSkillIconPrefab.CloneAndGetComponent<BattleSkillIcon>(cso.skillIconRoot.gameObject);
        e = battleSkillIcon.Init(s);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        battleSkillIcon.SetDepth(cso.skillIconRoot.depth);
        cso.skillEntryPoint.SetTextLocalize(Consts.Format(Consts.GetInstance().DETAIL_UNIT_PARAMETER_GET_SKILL, (IDictionary) new Hashtable()
        {
          {
            (object) "episode",
            (object) charactorSkill.character_quest.priority
          }
        }));
        cso.skillBGColor.color = (Color) this.CharactorSkillBgColors[element];
        this.skillParams.Add(new PopupSkillDetails.Param(s, UnitParameter.SkillGroup.None, new int?()));
        this.setEventPopupSkillDetail(cso.btnZoom, s);
        cso = new DetailUnitParameter.CharactorSkillObject();
        battleSkillIcon = (BattleSkillIcon) null;
      }
      charactorSkill = charactorSkills[1];
      if (skillObject.charactorSkillObjects.Length >= 2 && charactorSkill != null)
      {
        cso = skillObject.charactorSkillObjects[1];
        s = charactorSkill.skill;
        cso.skillName.SetTextLocalize(s.name);
        cso.skillDiscription.SetTextLocalize(s.description);
        battleSkillIcon = this.battleSkillIconPrefab.CloneAndGetComponent<BattleSkillIcon>(cso.skillIconRoot.gameObject);
        e = battleSkillIcon.Init(s);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        battleSkillIcon.SetDepth(cso.skillIconRoot.depth);
        cso.skillEntryPoint.SetTextLocalize(Consts.Format(Consts.GetInstance().DETAIL_UNIT_PARAMETER_GET_SKILL, (IDictionary) new Hashtable()
        {
          {
            (object) "episode",
            (object) charactorSkill.character_quest.priority
          }
        }));
        cso.skillBGColor.color = (Color) this.CharactorSkillBgColors[element];
        this.skillParams.Add(new PopupSkillDetails.Param(s, UnitParameter.SkillGroup.None, new int?()));
        this.setEventPopupSkillDetail(cso.btnZoom, s);
        cso = new DetailUnitParameter.CharactorSkillObject();
        battleSkillIcon = (BattleSkillIcon) null;
      }
      charactorSkill = (UnitSkillCharacterQuest) null;
    }
  }

  private void setEventPopupSkillDetail(UIButton btn, BattleskillSkill skill)
  {
    EventDelegate.Set(btn.onClick, (EventDelegate.Callback) (() => this.popupSkillDetail(skill)));
  }

  private void popupSkillDetail(BattleskillSkill skill)
  {
    int? nullable = this.skillParams.FirstIndexOrNull<PopupSkillDetails.Param>((Func<PopupSkillDetails.Param, bool>) (x => x.skill == skill));
    if ((UnityEngine.Object) this.skillDetailPrefab == (UnityEngine.Object) null || !nullable.HasValue)
      return;
    PopupSkillDetails.show(this.skillDetailPrefab, this.skillParams[nullable.Value], false, (System.Action) null, false);
  }

  [Serializable]
  private struct UnitObjectData
  {
    public GameObject rootObject;
    public NGxMaskSpriteWithScale charaSprite;
    public NGxMaskSpriteWithScale charaShadowSprite;
    public UISprite jobBaseSprite;
    public UISprite jobFramePatternSprite;
    public UILabel jobName;
    public UIWidget[] linkSkillIcons;
  }

  [Serializable]
  private struct CharactorSkillObject
  {
    public UISprite skillBGColor;
    public UILabel skillName;
    public UILabel skillDiscription;
    public UILabel skillEntryPoint;
    public UIWidget skillIconRoot;
    public UIButton btnZoom;
  }

  [Serializable]
  private struct LeaderSkillObject
  {
    public UILabel leaderSkillDiscription;
    public UIButton btnZoom;
  }

  [Serializable]
  private struct SkillObject
  {
    public GameObject rootObject;
    public DetailUnitParameter.LeaderSkillObject leaderSkillObject;
    public DetailUnitParameter.CharactorSkillObject[] charactorSkillObjects;
  }
}
