﻿// Decompiled with JetBrains decompiler
// Type: DailyMission0271Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class DailyMission0271Menu : BackButtonMenuBase
{
  [SerializeField]
  private NGHorizontalScrollParts scrollParts;
  [SerializeField]
  private DailyMission0271MissionRoot missonRoot;
  private PlayerBingo[] enablePlayerBingos;
  private PlayerBingo selectedPlayerBingo;
  private GameObject rewardPopup;
  private UIScrollView bannerScrollView;

  public IEnumerator Init()
  {
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    Future<WebAPI.Response.BingoIndex> future = WebAPI.BingoIndex((System.Action<WebAPI.Response.UserError>) (error =>
    {
      WebAPI.DefaultUserErrorCallback(error);
      Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
      Singleton<CommonRoot>.GetInstance().isLoading = false;
    }));
    IEnumerator e = future.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if (future.Result != null)
    {
      PlayerBingo[] playerBingo = future.Result.player_bingo;
      if (playerBingo == null)
      {
        MypageScene.ChangeScene(false, false, false);
        Singleton<CommonRoot>.GetInstance().isLoading = false;
      }
      else
      {
        this.enablePlayerBingos = ((IEnumerable<PlayerBingo>) playerBingo).Where<PlayerBingo>((Func<PlayerBingo, bool>) (x =>
        {
          if (x.is_end || x.bingo == null)
            return false;
          if (!x.bingo.end_at.HasValue)
            return true;
          return x.bingo.end_at.HasValue && x.bingo.end_at.Value > ServerTime.NowAppTimeAddDelta();
        })).OrderBy<PlayerBingo, int>((Func<PlayerBingo, int>) (x => x.bingo.priority)).ThenBy<PlayerBingo, int>((Func<PlayerBingo, int>) (x => x.bingo_id)).ToArray<PlayerBingo>();
        if (((IEnumerable<PlayerBingo>) this.enablePlayerBingos).Count<PlayerBingo>() <= 0)
        {
          MypageScene.ChangeScene(false, false, false);
          Singleton<CommonRoot>.GetInstance().isLoading = false;
        }
        else
        {
          ResourceManager resourceManager = Singleton<ResourceManager>.GetInstance();
          Future<GameObject> rewardPopupF = resourceManager.Load<GameObject>("Prefabs/popup/popup_027_1__anim_popup01", 1f);
          e = rewardPopupF.Wait();
          while (e.MoveNext())
            yield return e.Current;
          e = (IEnumerator) null;
          this.rewardPopup = rewardPopupF.Result;
          Future<GameObject> headerPrefabF = resourceManager.Load<GameObject>("Prefabs/dailymission027_1/dir_panel_mission_header", 1f);
          e = headerPrefabF.Wait();
          while (e.MoveNext())
            yield return e.Current;
          e = (IEnumerator) null;
          e = this.missonRoot.InitPrefabs();
          while (e.MoveNext())
            yield return e.Current;
          e = (IEnumerator) null;
          this.scrollParts.destroyParts(false);
          GameObject headerPrefab = headerPrefabF.Result;
          PlayerBingo[] playerBingoArray = this.enablePlayerBingos;
          for (int index = 0; index < playerBingoArray.Length; ++index)
          {
            PlayerBingo bingoData = playerBingoArray[index];
            e = this.scrollParts.instantiateParts(headerPrefab, true).GetComponent<DailyMission0271PanelMissionHeader>().InitHeader(bingoData);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
          }
          playerBingoArray = (PlayerBingo[]) null;
          this.scrollParts.resetScrollView();
          this.scrollParts.setItemPositionQuick(0);
          e = this.SetBingoPanel(this.enablePlayerBingos[0], false);
          while (e.MoveNext())
            yield return e.Current;
          e = (IEnumerator) null;
          yield return (object) new WaitForEndOfFrame();
          yield return (object) new WaitForEndOfFrame();
          Singleton<CommonRoot>.GetInstance().isLoading = false;
        }
      }
    }
  }

  private void onItemChanged(int selected)
  {
    this.IsPush = true;
    this.StartCoroutine(this.SetBingoPanel(this.enablePlayerBingos[selected], true));
  }

  private IEnumerator SetBingoPanel(PlayerBingo playerBingoData, bool isCheckObject = false)
  {
    DailyMission0271Menu dailyMission0271Menu = this;
    if (dailyMission0271Menu.selectedPlayerBingo != playerBingoData)
    {
      dailyMission0271Menu.selectedPlayerBingo = playerBingoData;
      IEnumerator e = dailyMission0271Menu.missonRoot.SetMissionData(dailyMission0271Menu.selectedPlayerBingo);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    dailyMission0271Menu.IsPush = false;
  }

  public void SetPlayerBingoData(PlayerBingo playerBingo)
  {
    for (int index = 0; index < this.enablePlayerBingos.Length; ++index)
    {
      if (this.enablePlayerBingos[index] == this.selectedPlayerBingo)
        this.enablePlayerBingos[index] = playerBingo;
    }
    this.selectedPlayerBingo = playerBingo;
  }

  public void IbtnBack()
  {
    this.IsPush = this.CheckIsPush();
    if (this.IsPushAndSet())
      return;
    Singleton<NGSceneManager>.GetInstance().destroyCurrentScene();
    MypageScene.ChangeScene(false, false, false);
  }

  public override void onBackButton()
  {
    this.IbtnBack();
  }

  private IEnumerator selectCompRewardAsync(int groupId)
  {
    CommonRoot common = Singleton<CommonRoot>.GetInstance();
    common.loadingMode = 2;
    common.isLoading = true;
    Future<WebAPI.Response.BingoSelectComplete> future = WebAPI.BingoSelectComplete(this.selectedPlayerBingo.bingo_id, groupId, (System.Action<WebAPI.Response.UserError>) (e =>
    {
      WebAPI.DefaultUserErrorCallback(e);
      Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
    }));
    IEnumerator e1 = future.Wait();
    while (e1.MoveNext())
      yield return e1.Current;
    e1 = (IEnumerator) null;
    if (future.Result != null)
    {
      common.isLoading = false;
      common.loadingMode = 0;
    }
  }

  public void IbtnLeftArrow()
  {
    this.IsPush = this.CheckIsPush();
    if (this.IsPushAndSet() || this.scrollParts.selected <= 0)
      return;
    this.scrollParts.setItemPosition(this.scrollParts.selected - 1);
  }

  public void IbtnRightArrow()
  {
    this.IsPush = this.CheckIsPush();
    if (this.IsPushAndSet() || this.scrollParts.selected >= this.scrollParts.PartsCnt - 1)
      return;
    this.scrollParts.setItemPosition(this.scrollParts.selected + 1);
  }

  public void IbtnCompleteReward()
  {
    this.IsPush = this.CheckIsPush();
    if (this.IsPushAndSet())
      return;
    this.StartCoroutine(this.ShowCompleteReward());
  }

  public bool CheckIsPush()
  {
    if (this.bannerScrollView.isDragging)
      return true;
    return this.scrollParts.selected != this.scrollParts.PartsCnt - 1 && this.scrollParts.selected != 0 && this.IsPush;
  }

  private IEnumerator ShowCompleteReward()
  {
    DailyMission0271Menu dailyMission0271Menu = this;
    // ISSUE: reference to a compiler-generated method
    MasterDataTable.BingoRewardGroup completeReward = ((IEnumerable<MasterDataTable.BingoRewardGroup>) MasterData.BingoRewardGroupList).Where<MasterDataTable.BingoRewardGroup>(new Func<MasterDataTable.BingoRewardGroup, bool>(dailyMission0271Menu.\u003CShowCompleteReward\u003Eb__17_0)).OrderBy<MasterDataTable.BingoRewardGroup, int>((Func<MasterDataTable.BingoRewardGroup, int>) (x => x.ID)).FirstOrDefault<MasterDataTable.BingoRewardGroup>();
    IEnumerator e = Singleton<PopupManager>.GetInstance().open(dailyMission0271Menu.rewardPopup, false, false, false, true, false, false, "SE_1006").GetComponent<DailyMission0271ConfirmationCompRewardPopup>().Init(completeReward);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  private void Start()
  {
    this.bannerScrollView = this.scrollParts.scrollView.GetComponent<UIScrollView>();
  }
}
