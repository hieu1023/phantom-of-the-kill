﻿// Decompiled with JetBrains decompiler
// Type: Startup00016Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;
using UnityEngine;

public class Startup00016Scene : NGSceneBase
{
  public Startup00016Menu menu;
  public Transform middleTransform;

  public override IEnumerator onInitSceneAsync()
  {
    Future<GameObject> future = Res.Prefabs.popup.popup_000_16__anim_popup01.Load<GameObject>();
    IEnumerator e = future.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    future.Result.Clone(this.middleTransform);
  }

  public IEnumerator onStartSceneAsync()
  {
    Startup00016Scene startup00016Scene = this;
    yield return (object) new WaitForSeconds(5f);
    startup00016Scene.menu = startup00016Scene.gameObject.GetComponentInChildren<Startup00016Menu>();
    IEnumerator e = startup00016Scene.menu.InitSceneAsync();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }
}
