﻿// Decompiled with JetBrains decompiler
// Type: BattleFiledSkillIconEffect
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using System.Collections;
using UnityEngine;

public class BattleFiledSkillIconEffect : MonoBehaviour
{
  [SerializeField]
  private Renderer renderer;
  [SerializeField]
  private Animator animator;

  public void SetSkillIcon(BattleskillSkill skill)
  {
    this.StartCoroutine(this.set(skill));
  }

  private IEnumerator set(BattleskillSkill skill)
  {
    UnityEngine.Material cpyMaterial = new UnityEngine.Material(this.renderer.material);
    Future<UnityEngine.Sprite> f = skill.LoadBattleSkillIcon((BattleFuncs.InvestSkill) null);
    IEnumerator e = f.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    cpyMaterial.mainTexture = (Texture) f.Result.texture;
    if ((Object) this.renderer.material != (Object) null)
      Object.Destroy((Object) this.renderer.material);
    this.renderer.material = cpyMaterial;
    this.animator.SetTrigger("play");
  }
}
