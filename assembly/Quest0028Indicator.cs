﻿// Decompiled with JetBrains decompiler
// Type: Quest0028Indicator
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnitStatusInformation;
using UnityEngine;

public class Quest0028Indicator : MonoBehaviour
{
  [SerializeField]
  private GameObject[] slcTextGuests;
  [SerializeField]
  protected UILabel TxtFriendskillDescription;
  [SerializeField]
  protected UILabel TxtFriendskillName;
  [SerializeField]
  private GameObject objFriendskillZoom;
  [SerializeField]
  protected UILabel TxtLeaderskillDescription;
  [SerializeField]
  protected UILabel TxtLeaderskillName;
  [SerializeField]
  private GameObject objLeaderskillZoom;
  [SerializeField]
  protected GameObject[] linkCharacters;
  [SerializeField]
  protected UISprite slc_NotFriend_Skill;
  [SerializeField]
  private UILabel txtNote;
  [HideInInspector]
  public UIButton friendSelect;
  [SerializeField]
  private UILabel TxtCombat;
  [NonSerialized]
  public bool disableUnitDetail;
  private PlayerHelper friend;
  private bool enabledFriend;
  private GameObject normalPrefab;
  private const float LINK_WIDTH = 92f;
  private const float LINK_DEFWIDTH = 114f;
  private const float scale = 0.8070176f;
  private const int FRIEND_NUM = 5;
  private PlayerUnit[] DeckUnitData;
  private PlayerDeck[] regulationDeck;
  private bool isLimitation;
  private bool isUserDeckStage;
  private UnitGender genderRestriction;
  private const int DECK_UNIT_MAX = 5;
  private GameObject skillDetailPrefab_;
  private BattleskillSkill friendSkill_;
  private BattleskillSkill leaderSkill_;

  public PlayerUnit[] deckUnitData
  {
    get
    {
      return this.DeckUnitData;
    }
  }

  public UnitIcon friendIcon_ { get; private set; }

  public bool isCompletedOverkillersDeck { get; private set; } = true;

  public IEnumerator InitPlayerDeck(
    PlayerDeck playerDeck,
    PlayerHelper friend,
    PlayerExtraQuestS extraQuest,
    PlayerStoryQuestS storyQuest,
    PlayerCharacterQuestS charQuest,
    PlayerQuestSConverter convertQuest,
    PlayerSeaQuestS seaQuest,
    PlayerDeck[] regulationDeck,
    QuestScoreBonusTimetable[] tables,
    UnitBonus[] unitBonus,
    BattleStageGuest[] guest,
    int battleStageID,
    int recommenCombat)
  {
    Future<GameObject> prefabF;
    IEnumerator e;
    if ((UnityEngine.Object) this.normalPrefab == (UnityEngine.Object) null)
    {
      prefabF = !Singleton<NGGameDataManager>.GetInstance().IsSea || seaQuest == null ? Res.Prefabs.UnitIcon.normal.Load<GameObject>() : new ResourceObject("Prefabs/Sea/UnitIcon/normal_sea").Load<GameObject>();
      e = prefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.normalPrefab = prefabF.Result;
      prefabF = (Future<GameObject>) null;
    }
    this.isLimitation = storyQuest != null && Quest0028Menu.IsStoryLimitation(storyQuest) || extraQuest != null && Quest0028Menu.IsExtraLimitation(extraQuest) || charQuest != null && Quest0028Menu.IsCharaLimitation(charQuest) || (convertQuest != null && Quest0028Menu.IsConvertLimitation(convertQuest) || seaQuest != null && Quest0028Menu.IsSeaLimitation(seaQuest));
    if (this.isLimitation)
      this.regulationDeck = regulationDeck;
    this.isUserDeckStage = extraQuest != null && extraQuest.extra_quest_area == 3;
    this.genderRestriction = extraQuest != null ? extraQuest.quest_extra_s.gender_restriction : (storyQuest != null ? storyQuest.quest_story_s.gender_restriction : (charQuest != null ? charQuest.quest_character_s.gender_restriction : (convertQuest != null ? convertQuest.questS.gender_restriction : (seaQuest != null ? seaQuest.quest_sea_s.gender_restriction : UnitGender.none))));
    this.friendIcon_ = (UnitIcon) null;
    this.friend = friend;
    BattleskillSkill LSkill = (BattleskillSkill) null;
    ((IEnumerable<GameObject>) this.slcTextGuests).ToggleOnceEx(-1);
    KeyValuePair<int, BattleStagePlayer>[] stagePlayers = MasterData.BattleStagePlayer.Where<KeyValuePair<int, BattleStagePlayer>>((Func<KeyValuePair<int, BattleStagePlayer>, bool>) (x => x.Value.stage_BattleStage == battleStageID)).ToArray<KeyValuePair<int, BattleStagePlayer>>();
    int maxPlayer = ((IEnumerable<KeyValuePair<int, BattleStagePlayer>>) stagePlayers).Count<KeyValuePair<int, BattleStagePlayer>>((Func<KeyValuePair<int, BattleStagePlayer>, bool>) (x => x.Value.deck_position != Consts.GetInstance().DECK_POSITION_FRIEND));
    maxPlayer = Mathf.Min(maxPlayer, playerDeck.player_units.Length);
    this.DeckUnitData = new PlayerUnit[5];
    int guestIdx = 0;
    bool[] posGuests = Enumerable.Repeat<bool>(true, 5).ToArray<bool>();
    for (int i = 0; i < playerDeck.player_units.Length; ++i)
    {
      if (guest != null && guest.Length != 0 && ((IEnumerable<BattleStageGuest>) guest).Any<BattleStageGuest>((Func<BattleStageGuest, bool>) (x => x.deck_position == i + 1)))
      {
        this.DeckUnitData[i] = PlayerUnit.FromGuest(guest[guestIdx]);
        ++guestIdx;
      }
      else
      {
        this.DeckUnitData[i] = playerDeck.player_units[i];
        if (i < maxPlayer)
          posGuests[i] = false;
      }
      e = this.LoadUnitPrefab(i, this.DeckUnitData[i], false, tables, unitBonus, i >= maxPlayer);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      if (i == 0 && this.DeckUnitData[i].leader_skill != null)
        LSkill = this.DeckUnitData[i].leader_skill.skill;
    }
    for (int i = playerDeck.player_units.Length; i < 5; ++i)
    {
      e = this.LoadUnitPrefab(i, (PlayerUnit) null, false, tables, unitBonus, true);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    int deck_combat = 0;
    ((IEnumerable<PlayerUnit>) playerDeck.player_units).ForEach<PlayerUnit>((System.Action<PlayerUnit>) (x =>
    {
      if (!(x != (PlayerUnit) null))
        return;
      deck_combat += Judgement.NonBattleParameter.FromPlayerUnit(x, false).Combat;
    }));
    if (recommenCombat == 0)
      this.TxtCombat.SetText(deck_combat.ToString());
    else if (recommenCombat <= deck_combat)
    {
      if (Singleton<NGGameDataManager>.GetInstance().IsSea)
        this.TxtCombat.SetText(Consts.Format(Consts.GetInstance().QUEST_0028_DECK_ENOUGH_SEA, (IDictionary) new Hashtable()
        {
          {
            (object) "combat",
            (object) deck_combat.ToString()
          }
        }));
      else
        this.TxtCombat.SetText(Consts.Format(Consts.GetInstance().QUEST_0028_DECK_ENOUGH, (IDictionary) new Hashtable()
        {
          {
            (object) "combat",
            (object) deck_combat.ToString()
          }
        }));
    }
    else if (Singleton<NGGameDataManager>.GetInstance().IsSea)
      this.TxtCombat.SetText(Consts.Format(Consts.GetInstance().QUEST_0028_DECK_SHORT_SEA, (IDictionary) new Hashtable()
      {
        {
          (object) "combat",
          (object) deck_combat.ToString()
        }
      }));
    else
      this.TxtCombat.SetText(Consts.Format(Consts.GetInstance().QUEST_0028_DECK_SHORT, (IDictionary) new Hashtable()
      {
        {
          (object) "combat",
          (object) deck_combat.ToString()
        }
      }));
    if ((UnityEngine.Object) this.skillDetailPrefab_ == (UnityEngine.Object) null)
    {
      prefabF = PopupSkillDetails.createPrefabLoader(Singleton<NGGameDataManager>.GetInstance().IsSea);
      yield return (object) prefabF.Wait();
      this.skillDetailPrefab_ = prefabF.Result;
      prefabF = (Future<GameObject>) null;
    }
    this.setFriendSkill((BattleskillSkill) null);
    this.enabledFriend = false;
    this.slc_NotFriend_Skill.gameObject.SetActive(false);
    if (((IEnumerable<KeyValuePair<int, BattleStagePlayer>>) stagePlayers).Any<KeyValuePair<int, BattleStagePlayer>>((Func<KeyValuePair<int, BattleStagePlayer>, bool>) (x => x.Value.deck_position == Consts.GetInstance().DECK_POSITION_FRIEND)))
    {
      if (guest != null && guest.Length != 0 && ((IEnumerable<BattleStageGuest>) guest).Any<BattleStageGuest>((Func<BattleStageGuest, bool>) (x => x.deck_position == Consts.GetInstance().DECK_POSITION_FRIEND)))
      {
        PlayerUnit unit = PlayerUnit.FromGuest(((IEnumerable<BattleStageGuest>) guest).FirstOrDefault<BattleStageGuest>((Func<BattleStageGuest, bool>) (x => x.deck_position == Consts.GetInstance().DECK_POSITION_FRIEND)));
        if (unit.leader_skill != null)
          this.setFriendSkill(unit.leader_skill.skill);
        e = this.LoadUnitPrefab(5, unit, false, tables, unitBonus, false);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
      }
      else
      {
        this.enabledFriend = true;
        if (friend != null)
        {
          if (!friend.enableLeaderSkill())
          {
            this.slc_NotFriend_Skill.gameObject.SetActive(true);
            this.slc_NotFriend_Skill.alpha = 0.8f;
            this.txtNote.SetTextLocalize(Consts.GetInstance().QUEST_0028_INDICATOR_NOT_FRIEND);
          }
          try
          {
            BattleskillSkill leaderSkillFromCache = friend.leader_skill_from_cache;
            if (leaderSkillFromCache != null)
              this.setFriendSkill(leaderSkillFromCache);
          }
          catch (InvalidOperationException ex)
          {
            Debug.LogError((object) ("0028Indicator.cs InitPlayerDeck Error:" + ex.Message + "[friend leaderUnit Id" + (object) friend.leader_unit_id + "]"));
          }
          PlayerUnit unit = friend.leader_unit;
          if (unit == (PlayerUnit) null)
            unit = PlayerUnit.create_by_unitunit(friend.leader_unit_from_cache, 0);
          e = this.LoadUnitPrefab(5, unit, true, tables, unitBonus, false);
          while (e.MoveNext())
            yield return e.Current;
          e = (IEnumerator) null;
        }
        else
        {
          this.slc_NotFriend_Skill.gameObject.SetActive(true);
          if (this.isLimitation)
            this.txtNote.SetTextLocalize(Consts.GetInstance().QUEST_0028_INDICATOR_CAN_NOT_RENTAL);
          else
            this.txtNote.SetTextLocalize(Consts.GetInstance().QUEST_0028_INDICATOR_NOT_RENTAL);
          e = this.LoadUnitPrefab(5, (PlayerUnit) null, true, tables, unitBonus, false);
          while (e.MoveNext())
            yield return e.Current;
          e = (IEnumerator) null;
        }
      }
    }
    else
    {
      this.slc_NotFriend_Skill.gameObject.SetActive(true);
      this.txtNote.SetTextLocalize(Consts.GetInstance().QUEST_0028_INDICATOR_CAN_NOT_RENTAL);
      e = this.LoadUnitPrefab(5, (PlayerUnit) null, false, tables, unitBonus, true);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    this.setLeaderSkill(LSkill);
    bool bCompleted;
    OverkillersUtil.checkCompletedDeck(this.DeckUnitData, out bCompleted, (HashSet<int>) null, posGuests);
    if (!bCompleted)
      this.setErrorOverkillersDeck();
    this.isCompletedOverkillersDeck = bCompleted;
  }

  private void setFriendSkill(BattleskillSkill skill = null)
  {
    this.friendSkill_ = skill;
    string text1 = skill?.description ?? string.Empty;
    string text2 = skill?.name ?? string.Empty;
    this.TxtFriendskillDescription.SetText(text1);
    this.TxtFriendskillName.SetText(text2);
    this.objFriendskillZoom.SetActive(skill != null);
  }

  public void onClickedFriendSkillZoom()
  {
    if (this.friendSkill_ == null)
      return;
    PopupSkillDetails.show(this.skillDetailPrefab_, new PopupSkillDetails.Param(this.friendSkill_, UnitParameter.SkillGroup.Leader, new int?()), false, (System.Action) null, false);
  }

  private void setLeaderSkill(BattleskillSkill skill = null)
  {
    this.leaderSkill_ = skill;
    string text1 = skill?.description ?? string.Empty;
    string text2 = skill?.name ?? string.Empty;
    this.TxtLeaderskillDescription.SetText(text1);
    this.TxtLeaderskillName.SetText(text2);
    this.objLeaderskillZoom.SetActive(skill != null);
  }

  public void onClickedLeaderSkillZoom()
  {
    if (this.leaderSkill_ == null)
      return;
    PopupSkillDetails.show(this.skillDetailPrefab_, new PopupSkillDetails.Param(this.leaderSkill_, UnitParameter.SkillGroup.Leader, new int?()), false, (System.Action) null, false);
  }

  public IEnumerator resetHelper(
    PlayerHelper friend,
    QuestScoreBonusTimetable[] tables,
    UnitBonus[] unitBonus)
  {
    if (this.enabledFriend)
    {
      this.friendIcon_ = (UnitIcon) null;
      UnitIcon componentInChildren = this.linkCharacters[5].transform.GetComponentInChildren<UnitIcon>();
      if ((UnityEngine.Object) componentInChildren != (UnityEngine.Object) null)
        UnityEngine.Object.Destroy((UnityEngine.Object) componentInChildren.gameObject);
      this.setFriendSkill((BattleskillSkill) null);
      this.slc_NotFriend_Skill.gameObject.SetActive(false);
      IEnumerator e;
      if (friend != null)
      {
        if (!friend.enableLeaderSkill())
        {
          this.slc_NotFriend_Skill.gameObject.SetActive(true);
          this.slc_NotFriend_Skill.alpha = 0.8f;
          this.txtNote.SetTextLocalize(Consts.GetInstance().QUEST_0028_INDICATOR_NOT_FRIEND);
        }
        try
        {
          BattleskillSkill leaderSkillFromCache = friend.leader_skill_from_cache;
          if (leaderSkillFromCache != null)
            this.setFriendSkill(leaderSkillFromCache);
        }
        catch (InvalidOperationException ex)
        {
          Debug.LogError((object) ("0028Indicator.cs InitPlayerDeck Error:" + ex.Message + "[friend leaderUnit Id" + (object) friend.leader_unit_id + "]"));
        }
        PlayerUnit unit = friend.leader_unit;
        if (unit == (PlayerUnit) null)
          unit = PlayerUnit.create_by_unitunit(friend.leader_unit_from_cache, 0);
        e = this.LoadUnitPrefab(5, unit, true, tables, unitBonus, false);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
      }
      else
      {
        this.slc_NotFriend_Skill.gameObject.SetActive(true);
        if (this.isLimitation)
          this.txtNote.SetTextLocalize(Consts.GetInstance().QUEST_0028_INDICATOR_CAN_NOT_RENTAL);
        else
          this.txtNote.SetTextLocalize(Consts.GetInstance().QUEST_0028_INDICATOR_NOT_RENTAL);
        e = this.LoadUnitPrefab(5, (PlayerUnit) null, true, tables, unitBonus, false);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
      }
      if (!this.isCompletedOverkillersDeck)
        this.setErrorOverkillersDeck();
    }
  }

  private void setErrorOverkillersDeck()
  {
    this.slc_NotFriend_Skill.gameObject.SetActive(true);
    this.txtNote.SetTextLocalize(Consts.GetInstance().QUEST_0028_INDICATOR_LIMITED_OVERKILLERS);
  }

  public IEnumerator LoadUnitPrefab(
    int index,
    PlayerUnit unit,
    bool isFriend,
    QuestScoreBonusTimetable[] tables,
    UnitBonus[] unitBonus,
    bool sortieNot = false)
  {
    GameObject gameObject = this.normalPrefab.Clone(this.linkCharacters[index].transform);
    gameObject.transform.localScale = new Vector3(0.8070176f, 0.8070176f);
    UnitIcon unitScript = gameObject.GetComponent<UnitIcon>();
    if (unit != (PlayerUnit) null & isFriend)
      this.friendIcon_ = unitScript;
    IEnumerator e = unitScript.setSimpleUnit(unit);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    unitScript.setLevelText(unit);
    if (unit != (PlayerUnit) null && Singleton<NGGameDataManager>.GetInstance().IsSea && (!unit.is_gesut && !sortieNot))
    {
      if (!isFriend || this.friend == null)
        unitScript.SetSeaPiece(unit.unit.GetPiece);
      else if (this.friend.is_friend || this.friend.is_guild_member)
        unitScript.SetSeaPiece(unit.unit.GetPiece);
    }
    unitScript.ShowBottomInfo(UnitSortAndFilter.SORT_TYPES.Level);
    if (this.isLimitation || this.genderRestriction != UnitGender.none)
      this.SetRegulation(unit, unitScript, index);
    if (unit != (PlayerUnit) null)
    {
      if (sortieNot)
      {
        unitScript.SetEmpty();
        unitScript.SetRegulation(UnitIcon.Regulation.None);
        unitScript.BackgroundModeValue = UnitIcon.BackgroundMode.Empty;
      }
      else
      {
        if (!this.disableUnitDetail)
        {
          unitScript.onClick = (System.Action<UnitIconBase>) (x => this.ChangeDetailScene(unit, isFriend, index));
          EventDelegate.Set(unitScript.Button.onLongPress, (EventDelegate.Callback) (() => this.ChangeDetailScene(unit, isFriend, index)));
        }
        unitScript.BreakWeapon = !isFriend && unit.IsBrokenEquippedGear;
        string color_code = unit.SpecialEffectType((IEnumerable<QuestScoreBonusTimetable>) tables, (IEnumerable<UnitBonus>) unitBonus);
        unitScript.SpecialIcon = !string.IsNullOrEmpty(color_code);
        if (!string.IsNullOrEmpty(color_code))
        {
          int? specialIconType = UnitIcon.GetSpecialIconType(color_code);
          if (specialIconType.HasValue)
            unitScript.SpecialIconType = specialIconType.Value;
        }
        if (unit.is_gesut)
          this.slcTextGuests[index].SetActive(true);
      }
    }
    else
    {
      unitScript.SetEmpty();
      if (index == 5 && (this.isLimitation || this.isUserDeckStage))
        unitScript.BackgroundModeValue = UnitIcon.BackgroundMode.Empty;
      else if (isFriend)
      {
        unitScript.SelectUnit = true;
        this.friendSelect = (UIButton) unitScript.Button;
      }
      else if (sortieNot)
      {
        unitScript.SetRegulation(UnitIcon.Regulation.None);
        unitScript.BackgroundModeValue = UnitIcon.BackgroundMode.Empty;
      }
    }
    unitScript.Favorite = false;
    unitScript.Gray = false;
  }

  private void ChangeDetailScene(PlayerUnit unit, bool isFriend, int index)
  {
    if (!isFriend && !unit.is_gesut)
      Unit0042Scene.changeScene(true, unit, ((IEnumerable<PlayerUnit>) this.DeckUnitData).Where<PlayerUnit>((Func<PlayerUnit, bool>) (x => x != (PlayerUnit) null && !x.is_gesut)).ToArray<PlayerUnit>(), false, false);
    else if (isFriend)
      Unit0042Scene.changeSceneFriendUnit(true, this.friend.target_player_id, this.friend.leader_player_unit_id);
    else if (index == Consts.GetInstance().DECK_POSITION_FRIEND - 1)
      Unit0042Scene.changeSceneGuestUnit(true, unit, new PlayerUnit[1]
      {
        unit
      });
    else
      Unit0042Scene.changeSceneGuestUnit(true, unit, ((IEnumerable<PlayerUnit>) this.DeckUnitData).Where<PlayerUnit>((Func<PlayerUnit, bool>) (x => x != (PlayerUnit) null && x.is_gesut)).ToArray<PlayerUnit>());
    this.DestroyObject();
  }

  public void DestroyObject()
  {
    foreach (GameObject linkCharacter in this.linkCharacters)
    {
      UnitIcon componentInChildren = linkCharacter.GetComponentInChildren<UnitIcon>();
      if ((UnityEngine.Object) componentInChildren != (UnityEngine.Object) null)
        UnityEngine.Object.Destroy((UnityEngine.Object) componentInChildren.gameObject);
    }
  }

  private void SetRegulation(PlayerUnit unit, UnitIcon unitScript, int count)
  {
    bool flag1 = true;
    bool flag2 = true;
    if (unit != (PlayerUnit) null && !unit.is_gesut)
    {
      if (this.regulationDeck != null)
        flag1 = ((IEnumerable<PlayerDeck>) this.regulationDeck).Any<PlayerDeck>((Func<PlayerDeck, bool>) (d => ((IEnumerable<int?>) d.player_unit_ids).Any<int?>((Func<int?, bool>) (u => u.HasValue && u.Value == unit.id))));
      if (this.genderRestriction != UnitGender.none)
        flag2 = unit.unit.character.gender == this.genderRestriction;
    }
    if (count != 5 && (!flag1 || !flag2))
    {
      if (unitScript.BreakWeapon)
        unitScript.SetRegulation(UnitIcon.Regulation.WithBroken);
      else
        unitScript.SetRegulation(UnitIcon.Regulation.Default);
    }
    else
      unitScript.SetRegulation(UnitIcon.Regulation.None);
  }
}
