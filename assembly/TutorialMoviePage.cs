﻿// Decompiled with JetBrains decompiler
// Type: TutorialMoviePage
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

public class TutorialMoviePage : TutorialPageBase
{
  [SerializeField]
  private string iosMoviePath;
  [SerializeField]
  private string androidMoviePath;
  [SerializeField]
  private string windowsMoviePath;

  private string moviePath()
  {
    return this.windowsMoviePath;
  }

  public override IEnumerator Show()
  {
    this.NextPage();
    yield break;
  }
}
