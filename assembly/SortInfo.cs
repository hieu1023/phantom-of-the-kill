﻿// Decompiled with JetBrains decompiler
// Type: SortInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using System.Collections.Generic;

public class SortInfo
{
  public UnitSortAndFilter.SORT_TYPES sortType = UnitSortAndFilter.SORT_TYPES.BranchOfAnArmy;
  public bool isTowerEntry = true;
  public SortAndFilter.SORT_TYPE_ORDER_BUY orderType;
  public bool isBattleFirst;
  public bool[] filters;
  public Dictionary<UnitGroupHead, List<int>> groupIDs;

  public SortInfo(
    UnitSortAndFilter.SORT_TYPES sType,
    SortAndFilter.SORT_TYPE_ORDER_BUY oType,
    bool[] filter,
    Dictionary<UnitGroupHead, List<int>> gIDs,
    bool battleFirst,
    bool towerEntry)
  {
    this.sortType = sType;
    this.orderType = oType;
    this.filters = filter;
    this.groupIDs = gIDs;
    this.isBattleFirst = battleFirst;
    this.isTowerEntry = towerEntry;
  }
}
