﻿// Decompiled with JetBrains decompiler
// Type: unitInfomation
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using UnityEngine;

public class unitInfomation
{
  public bool isplayer = true;
  public BL.Unit bu;
  public Battle0181CharacterStatus p;
  public NGDuelUnit enemy;
  public bool iscriticalcamera;
  public BL.MagicBullet mb;
  public BL.Weapon weapon;
  public Transform trs;
  public int range;
  public IntimateDuelSupport support;
  public int supportHitIncr;
  public int supportEvasionIncr;
  public int supportCriticalIncr;
  public int supportCriticalEvasionIncr;
  public GameObject root3d;
  public NGDuelManager mng;
  public int[] beforeAilmentEffectIDs;
  public SkillMetamorphosis metamorphosis;
}
