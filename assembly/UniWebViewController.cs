﻿// Decompiled with JetBrains decompiler
// Type: UniWebViewController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class UniWebViewController : MonoBehaviour
{
  private string m_url;
  private bool m_visible;
  public GameObject[] m_followerObjects;
  [SerializeField]
  private TopScene m_topSceneObject;

  public string GumiURIPath
  {
    get
    {
      return this.m_url;
    }
    set
    {
      this.m_url = ServerSelector.ApiUrl + "/" + value;
    }
  }

  public string URL
  {
    get
    {
      return this.m_url;
    }
    set
    {
      this.m_url = value;
    }
  }

  public void Navigate()
  {
    Application.OpenURL(this.m_url + "#" + WebQueue.AuthToken);
  }
}
