﻿// Decompiled with JetBrains decompiler
// Type: IconPrefabBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public abstract class IconPrefabBase : MonoBehaviour
{
  protected bool gray;

  public void SetSize(int width, int height)
  {
    UIWidget component = this.GetComponent<UIWidget>();
    component.transform.localScale = new Vector3((float) width / (float) component.width, (float) height / (float) component.height);
  }

  public void SetBasedOnHeight(int height)
  {
    UIWidget component = this.GetComponent<UIWidget>();
    float num = (float) height / (float) component.height;
    component.transform.localScale = new Vector3(num, num);
  }

  public void SetBasedOnWidth(int width)
  {
    UIWidget component = this.GetComponent<UIWidget>();
    float num = (float) width / (float) component.width;
    component.transform.localScale = new Vector3(num, num);
  }

  public virtual bool Gray
  {
    get
    {
      return this.gray;
    }
    set
    {
      this.gray = value;
    }
  }
}
