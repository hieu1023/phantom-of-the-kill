﻿// Decompiled with JetBrains decompiler
// Type: ApPopup0027
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;
using UnityEngine;

public class ApPopup0027 : BackButtonMenuBase
{
  private System.Action btnAct;

  public void ibtnYes()
  {
    this.StartCoroutine(this.popup00712());
  }

  public void SetBtnAct(System.Action questChangeScene)
  {
    this.btnAct = questChangeScene;
  }

  private IEnumerator popup00712()
  {
    Future<GameObject> prefab = Res.Prefabs.popup.popup_007_12__popup01.Load<GameObject>();
    IEnumerator e = prefab.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Shop00712Menu component = Singleton<PopupManager>.GetInstance().open(prefab.Result, false, false, false, true, false, false, "SE_1006").GetComponent<Shop00712Menu>();
    component.setUserData();
    component.SetBtnAction(this.btnAct);
  }

  public void IbtnNo()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<PopupManager>.GetInstance().onDismiss();
  }

  public override void onBackButton()
  {
    this.IbtnNo();
  }
}
