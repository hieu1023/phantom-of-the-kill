﻿// Decompiled with JetBrains decompiler
// Type: GearProfiencyIcon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class GearProfiencyIcon : IconPrefabBase
{
  private static readonly string[] IconName = new string[7]
  {
    "",
    "E",
    "D",
    "C",
    "B",
    "A",
    "S"
  };
  [SerializeField]
  private UI2DSprite iconSprite;
  private static GameObject self;

  public void Init(int level)
  {
    if (level < 1)
      level = 1;
    if (level >= GearProfiencyIcon.IconName.Length)
      level = GearProfiencyIcon.IconName.Length - 1;
    string empty = string.Empty;
    this.iconSprite.sprite2D = Resources.Load<UnityEngine.Sprite>(!Singleton<NGGameDataManager>.GetInstance().IsSea ? string.Format("Icons/Materials/GearProficiency/dyn_Rankicon_{0}", (object) GearProfiencyIcon.IconName[level]) : string.Format("Icons/Materials/Sea/GearProficiency/dyn_Rankicon_{0}", (object) GearProfiencyIcon.IconName[level]));
  }

  public static GameObject GetPrefab()
  {
    if ((Object) GearProfiencyIcon.self == (Object) null)
      GearProfiencyIcon.self = Resources.Load<GameObject>("Icons/GearProfiencyIcon");
    return GearProfiencyIcon.self;
  }
}
