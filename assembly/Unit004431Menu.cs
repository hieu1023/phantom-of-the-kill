﻿// Decompiled with JetBrains decompiler
// Type: Unit004431Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Unit004431Menu : UnitMenuBase
{
  public Unit004431Menu.Param sendParam;
  private PlayerUnit[] PlayerUnits;
  private GameObject prefab_Result;
  private PlayerItem choiceGear;
  private PlayerUnit TakeOffUnit;
  protected bool isEarthMode;
  private GameObject StatusChangePopupPrefab;

  protected override void Update()
  {
    base.Update();
    this.ScrollUpdate();
  }

  public virtual void InitializeAllUnitInfosExtend()
  {
    this.allUnitInfos.ForEach((System.Action<UnitIconInfo>) (x =>
    {
      if (!(x.playerUnit != (PlayerUnit) null) || this.choiceGear.gear.isEquipment(x.playerUnit))
        return;
      x.gray = true;
      x.button_enable = false;
    }));
  }

  public virtual IEnumerator Init(
    Player player,
    PlayerUnit[] playerUnits,
    Unit004431Menu.Param sendParam,
    bool isEquip)
  {
    Unit004431Menu unit004431Menu = this;
    IEnumerator e = unit004431Menu.Initialize();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Future<GameObject> popupPrefabF = Res.Prefabs.popup.popup_004_4_1__anim_popup01.Load<GameObject>();
    e = popupPrefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    unit004431Menu.StatusChangePopupPrefab = popupPrefabF.Result;
    unit004431Menu.prefab_Result = (GameObject) null;
    Future<GameObject> popupF = Res.Prefabs.popup.popup_004_12_4__anim_popup01.Load<GameObject>();
    e = popupF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    unit004431Menu.prefab_Result = popupF.Result;
    unit004431Menu.sendParam = sendParam;
    unit004431Menu.PlayerUnits = playerUnits;
    sendParam.index = 1;
    PlayerUnit[] andResetChoiceGear = unit004431Menu.getCanEquippedUnitsAndResetChoiceGear(playerUnits, sendParam);
    foreach (PlayerUnit playerUnit in playerUnits)
    {
      if (playerUnit != (PlayerUnit) null && (playerUnit.equippedGear != (PlayerItem) null && playerUnit.equippedGear.id == sendParam.gearId || playerUnit.equippedGear2 != (PlayerItem) null && playerUnit.equippedGear2.id == sendParam.gearId))
      {
        unit004431Menu.TakeOffUnit = playerUnit;
        break;
      }
    }
    if (unit004431Menu.choiceGear.ForBattle)
    {
      // ISSUE: reference to a compiler-generated method
      unit004431Menu.InitializeInfo((IEnumerable<PlayerUnit>) andResetChoiceGear, (IEnumerable<PlayerMaterialUnit>) null, Persist.unit004431SortAndFilter, isEquip, true, !unit004431Menu.isEarthMode, !unit004431Menu.isEarthMode, false, new System.Action(unit004431Menu.\u003CInit\u003Eb__10_0), 0);
    }
    else
    {
      // ISSUE: reference to a compiler-generated method
      unit004431Menu.InitializeInfo((IEnumerable<PlayerUnit>) andResetChoiceGear, (IEnumerable<PlayerMaterialUnit>) null, Persist.unit004431SortAndFilter, isEquip, false, !unit004431Menu.isEarthMode, !unit004431Menu.isEarthMode, false, new System.Action(unit004431Menu.\u003CInit\u003Eb__10_1), 0);
    }
    e = unit004431Menu.CreateUnitIcon();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    unit004431Menu.InitializeEnd();
  }

  public PlayerUnit[] getCanEquippedUnitsAndResetChoiceGear(
    PlayerUnit[] playerUnits,
    Unit004431Menu.Param sparam)
  {
    this.choiceGear = ((IEnumerable<PlayerItem>) this.GetAllGears(SMManager.Get<PlayerItem[]>())).FirstOrDefault<PlayerItem>((Func<PlayerItem, bool>) (x => x.id == sparam.gearId));
    if (this.choiceGear == (PlayerItem) null)
      return new PlayerUnit[0];
    IEnumerable enumerable = sparam.gearKindId == 7 || sparam.gearKindId == 10 ? (IEnumerable) ((IEnumerable<PlayerUnit>) playerUnits).Where<PlayerUnit>((Func<PlayerUnit, bool>) (v => v.unit.IsNormalUnit && this.choiceGear.gear.enableEquipmentUnit(v))) : (IEnumerable) ((IEnumerable<PlayerUnit>) playerUnits).Where<PlayerUnit>((Func<PlayerUnit, bool>) (v => v.unit.kind_GearKind == sparam.gearKindId && v.unit.IsNormalUnit && this.choiceGear.gear.enableEquipmentUnit(v)));
    List<PlayerUnit> playerUnitList = new List<PlayerUnit>();
    foreach (PlayerUnit playerUnit in enumerable)
    {
      MasterDataTable.UnitJob jobData = playerUnit.getJobData();
      int num1 = 0;
      if (jobData.classification_GearClassificationPattern.HasValue)
      {
        int? classificationPattern = jobData.classification_GearClassificationPattern;
        int num2 = 0;
        if (!(classificationPattern.GetValueOrDefault() == num2 & classificationPattern.HasValue))
          num1 = jobData.classification_GearClassificationPattern.Value;
      }
      if (num1 == 0)
      {
        playerUnitList.Add(playerUnit);
      }
      else
      {
        int num2 = num1;
        int? classificationPattern = this.choiceGear.gear.classification_GearClassificationPattern;
        int valueOrDefault = classificationPattern.GetValueOrDefault();
        if (num2 == valueOrDefault & classificationPattern.HasValue || this.choiceGear.gear.kind.isNonWeapon)
          playerUnitList.Add(playerUnit);
      }
    }
    return playerUnitList.ToArray();
  }

  protected virtual PlayerItem[] GetAllGears(PlayerItem[] items)
  {
    return items.AllGearsWithEquip();
  }

  protected override IEnumerator CreateUnitIcon(
    int info_index,
    int unit_index,
    PlayerUnit baseUnit = null)
  {
    IEnumerator e = base.CreateUnitIcon(info_index, unit_index, baseUnit);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.CreateUnitIconAction(info_index, unit_index);
  }

  protected override void CreateUnitIconCache(int info_index, int unit_index, PlayerUnit baseUnit = null)
  {
    base.CreateUnitIconCache(info_index, unit_index, (PlayerUnit) null);
    this.CreateUnitIconAction(info_index, unit_index);
  }

  private IEnumerator StatusPopup(
    PlayerUnit baseUnit,
    PlayerItem beforeGear,
    PlayerItem afterGear,
    int gearIndex)
  {
    Future<GameObject> iconPrefabF = Res.Prefabs.ItemIcon.prefab.Load<GameObject>();
    IEnumerator e = iconPrefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    GameObject iconPrefab = iconPrefabF.Result;
    GameObject beforeGearIcon = UnityEngine.Object.Instantiate<GameObject>(iconPrefab);
    ItemIcon beforeGearIconScript = beforeGearIcon.GetComponent<ItemIcon>();
    if (beforeGear != (PlayerItem) null)
    {
      e = beforeGearIconScript.InitByGear(beforeGear.gear, beforeGear.GetElement(), false, beforeGear.isReisouSet, false);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    else
    {
      e = beforeGearIconScript.InitByGear((GearGear) null, CommonElement.none, false, false, false);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      beforeGearIconScript.SetEmpty(true);
    }
    GameObject afterGearIcon = UnityEngine.Object.Instantiate<GameObject>(iconPrefab);
    ItemIcon afterGearIconScript = afterGearIcon.GetComponent<ItemIcon>();
    if (afterGear != (PlayerItem) null)
    {
      e = afterGearIconScript.InitByGear(afterGear.gear, afterGear.GetElement(), false, afterGear.isReisouSet, false);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    else
    {
      e = afterGearIconScript.InitByGear((GearGear) null, CommonElement.none, false, false, false);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      afterGearIconScript.SetEmpty(true);
    }
    Future<GameObject> skillTypeIconLoader = Res.Prefabs.BattleSkillIcon._battleSkillIcon.Load<GameObject>();
    e = skillTypeIconLoader.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    GameObject skillTypeIconPrefab = skillTypeIconLoader.Result;
    List<GameObject> beforeSkillTypeIcons = new List<GameObject>();
    GearGearSkill[] gearGearSkillArray;
    int index;
    GameObject beforeSkillTypeIcon;
    if (beforeGear != (PlayerItem) null)
    {
      gearGearSkillArray = beforeGear.skills;
      for (index = 0; index < gearGearSkillArray.Length; ++index)
      {
        GearGearSkill gearGearSkill = gearGearSkillArray[index];
        beforeSkillTypeIcon = UnityEngine.Object.Instantiate<GameObject>(skillTypeIconPrefab);
        e = beforeSkillTypeIcon.GetComponent<BattleSkillIcon>().Init(gearGearSkill.skill);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        beforeSkillTypeIcons.Add(beforeSkillTypeIcon);
        beforeSkillTypeIcon = (GameObject) null;
      }
      gearGearSkillArray = (GearGearSkill[]) null;
    }
    List<GameObject> afterSkillTypeIcons = new List<GameObject>();
    if (afterGear != (PlayerItem) null && afterGear.skills.Length != 0)
    {
      gearGearSkillArray = afterGear.skills;
      for (index = 0; index < gearGearSkillArray.Length; ++index)
      {
        GearGearSkill gearGearSkill = gearGearSkillArray[index];
        beforeSkillTypeIcon = UnityEngine.Object.Instantiate<GameObject>(skillTypeIconPrefab);
        e = beforeSkillTypeIcon.GetComponent<BattleSkillIcon>().Init(gearGearSkill.skill);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        afterSkillTypeIcons.Add(beforeSkillTypeIcon);
        beforeSkillTypeIcon = (GameObject) null;
      }
      gearGearSkillArray = (GearGearSkill[]) null;
    }
    GameObject statusPopup = this.StatusChangePopupPrefab.Clone((Transform) null);
    Unit00441Menu component = statusPopup.GetComponent<Unit00441Menu>();
    statusPopup.SetActive(false);
    beforeGearIcon.SetActive(false);
    afterGearIcon.SetActive(false);
    e = component.SetGear((PlayerUnit) null, baseUnit, beforeGear, afterGear, beforeGearIcon, afterGearIcon, beforeSkillTypeIcons.ToArray(), afterSkillTypeIcons.ToArray(), gearIndex, this.isEarthMode);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Singleton<PopupManager>.GetInstance().open(statusPopup, false, false, true, true, false, false, "SE_1006");
    statusPopup.SetActive(true);
    beforeGearIcon.SetActive(true);
    afterGearIcon.SetActive(true);
  }

  private void CreateUnitIconAction(int info_index, int unit_index)
  {
    UnitIconBase unitIcon = this.allUnitIcons[unit_index];
    UnitIconInfo unitInfo = this.displayUnitInfos[info_index];
    if (unitInfo.removeButton)
    {
      unitIcon.onClick = (System.Action<UnitIconBase>) (iconBase => this.RemoveGear(this.TakeOffUnit));
    }
    else
    {
      if (this.TakeOffUnit != (PlayerUnit) null && this.TakeOffUnit.id == unitInfo.playerUnit.id)
        unitInfo.button_enable = false;
      unitIcon.onClick = (System.Action<UnitIconBase>) (ui =>
      {
        if (unitIcon.PlayerUnit != (PlayerUnit) null)
        {
          PlayerItem beforeGear = (PlayerItem) null;
          if (unitIcon.PlayerUnit.unit.awake_unit_flag)
          {
            if (this.choiceGear.gear.kind_GearKind == unitInfo.playerUnit.unit.kind_GearKind)
              beforeGear = unitIcon.PlayerUnit.equippedGear;
            else if (this.choiceGear.gear.kind.Enum == GearKindEnum.shield || this.choiceGear.gear.kind.Enum == GearKindEnum.accessories)
            {
              beforeGear = unitIcon.PlayerUnit.equippedGear2;
              this.sendParam.index = 2;
            }
          }
          else
            beforeGear = unitIcon.PlayerUnit.equippedGear;
          if (this.TakeOffUnit != (PlayerUnit) null)
            Singleton<PopupManager>.GetInstance().open(this.prefab_Result, false, false, false, true, false, false, "SE_1006").GetComponent<Unit004431Popup>().Init(this.PlayerUnits, unitIcon.PlayerUnit, this.choiceGear, this.sendParam.index, this.isEarthMode);
          else
            this.StartCoroutine(this.StatusPopup(unitIcon.PlayerUnit, beforeGear, this.choiceGear, this.sendParam.index));
        }
        else
          Debug.LogWarning((object) "PlayerUnit Null : Unit004431Menu");
      });
      EventDelegate.Set(unitIcon.Button.onLongPress, (EventDelegate.Callback) (() => this.ChangeDetailScehe(unitInfo.playerUnit)));
    }
    if (unitInfo.button_enable)
      return;
    unitIcon.onClick = (System.Action<UnitIconBase>) (_ => {});
    unitIcon.Gray = true;
  }

  private bool CheckProficiencie(PlayerUnit unit, PlayerItem item)
  {
    return item.gear.checkCanEquipByProficiency(unit);
  }

  protected virtual void ChangeDetailScehe(PlayerUnit unit)
  {
    Unit0042Scene.changeSceneEvolutionUnit(true, unit, this.getUnits(), false, false, false);
  }

  private void RemoveGear(PlayerUnit baseUnit)
  {
    if (baseUnit.unit.awake_unit_flag)
      this.sendParam.index = this.choiceGear.gear.kind.Enum == GearKindEnum.shield || this.choiceGear.gear.kind.Enum == GearKindEnum.accessories ? 2 : 1;
    this.StartCoroutine(this.RemoveGearAsync(baseUnit));
  }

  private IEnumerator RemoveGearAsync(PlayerUnit baseUnit)
  {
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    IEnumerator e = RequestDispatcher.EquipGear(this.sendParam.index, new int?(0), baseUnit.id, (System.Action<WebAPI.Response.UserError>) (error =>
    {
      if (error == null)
        return;
      WebAPI.DefaultUserErrorCallback(error);
    }), this.isEarthMode);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Singleton<CommonRoot>.GetInstance().isLoading = false;
    // ISSUE: reference to a compiler-generated method
    this.\u003C\u003En__1();
  }

  public class Param
  {
    public int gearKindId;
    public int gearId;
    public int index;
  }
}
