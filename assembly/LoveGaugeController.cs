﻿// Decompiled with JetBrains decompiler
// Type: LoveGaugeController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using System.Collections;
using UnityEngine;

public class LoveGaugeController : MonoBehaviour
{
  private int seChannel = -1;
  [SerializeField]
  private NGTweenGaugeFillAmount gauge;
  [SerializeField]
  private NGTweenGaugeFillAmount gaugeCap;
  [SerializeField]
  private GameObject heartObject;
  [SerializeField]
  private TweenPosition positionTween;
  [SerializeField]
  private UISprite slcDearDegree;
  [SerializeField]
  private float minPositionX;
  [SerializeField]
  private float maxPositionX;
  private bool isGaugeAnimationStopped;

  public void setValue(int value, int cap, int max, bool doTween, bool isSe = false)
  {
    this.gaugeCap.setValue(cap, max, false, -1f, -1f);
    if (!this.gauge.setValue(value, max, doTween, -1f, -1f))
      return;
    float x = (this.maxPositionX - this.minPositionX) * ((float) value / (float) max) + this.minPositionX;
    Vector3 localPosition = this.heartObject.transform.localPosition;
    Vector3 vector3 = new Vector3(x, localPosition.y, localPosition.z);
    if (doTween && (Object) this.positionTween != (Object) null)
    {
      this.positionTween.worldSpace = false;
      this.positionTween.from = localPosition;
      this.positionTween.to = vector3;
      if (isSe)
        this.seChannel = Singleton<NGSoundManager>.GetInstance().playSE("SE_1045", false, 0.0f, -1);
      NGTween.playTween((UITweener) this.positionTween, false);
    }
    else
      this.heartObject.transform.localPosition = vector3;
  }

  public IEnumerator setValue(
    int start,
    int end,
    int max,
    int interval,
    bool doTween,
    bool isSe = false)
  {
    LoveGaugeController loveGaugeController = this;
    loveGaugeController.isGaugeAnimationStopped = false;
    loveGaugeController.gaugeCap.setValue(max, max, false, -1f, -1f);
    Vector3 heartPositionMin = new Vector3(loveGaugeController.minPositionX, loveGaugeController.heartObject.transform.localPosition.y, loveGaugeController.heartObject.transform.localPosition.z);
    Vector3 heartPositionMax = new Vector3(loveGaugeController.maxPositionX, loveGaugeController.heartObject.transform.localPosition.y, loveGaugeController.heartObject.transform.localPosition.z);
    if (start != end)
    {
      bool loopFinish = false;
      int num1 = start;
      while (!loopFinish)
      {
        int num2 = num1 / interval * interval;
        int num3 = (num1 / interval + 1) * interval;
        if (num3 >= max)
          num3 = max;
        int num4 = num3 - num2;
        int toValue = end;
        if (toValue > num3)
          toValue = num3;
        else
          loopFinish = true;
        float num5 = (float) (num1 - num2) / (float) num4;
        float num6 = (float) (toValue - num2) / (float) num4;
        if (doTween && (Object) loveGaugeController.positionTween != (Object) null)
        {
          loveGaugeController.positionTween.worldSpace = false;
          loveGaugeController.positionTween.from = Vector3.Lerp(heartPositionMin, heartPositionMax, num5);
          loveGaugeController.positionTween.to = Vector3.Lerp(heartPositionMin, heartPositionMax, num6);
          if (isSe)
            loveGaugeController.seChannel = Singleton<NGSoundManager>.GetInstance().playSE("SE_1045", false, 0.0f, -1);
          NGTween.playTween((UITweener) loveGaugeController.positionTween, false);
        }
        else
          loveGaugeController.heartObject.transform.localPosition = Vector3.Lerp(heartPositionMin, heartPositionMax, num6);
        loveGaugeController.gauge.setValue(num5, num6, doTween, -1f, -1f);
        while (loveGaugeController.gauge.isAnimationPlaying || loveGaugeController.positionTween.isActiveAndEnabled)
        {
          if (loveGaugeController.isGaugeAnimationStopped)
          {
            loveGaugeController.StartCoroutine(loveGaugeController.setValue(end, end, max, interval, false, false));
            yield break;
          }
          else
            yield return (object) null;
        }
        num1 = toValue;
      }
    }
    else
    {
      float num = (float) (end % interval) / (float) interval;
      if (end != 0 && (double) num == 0.0 || end == max)
        num = 100f;
      loveGaugeController.heartObject.transform.localPosition = Vector3.Lerp(heartPositionMin, heartPositionMax, num);
      loveGaugeController.gauge.setValue(num, num, false, -1f, -1f);
    }
  }

  public void SetGaugeText(UnitUnit unit, string dearSpriteName, string relevanceSpriteName)
  {
    if (unit.IsSea)
    {
      this.slcDearDegree.spriteName = dearSpriteName;
    }
    else
    {
      if (!unit.IsResonanceUnit)
        return;
      this.slcDearDegree.spriteName = relevanceSpriteName;
    }
  }

  public void endPositionTween()
  {
    if (this.seChannel == -1)
      return;
    Singleton<NGSoundManager>.GetInstance().stopSE(this.seChannel);
  }

  public void StopSE()
  {
    if (this.seChannel == -1)
      return;
    Singleton<NGSoundManager>.GetInstance().stopSE(this.seChannel);
  }

  public void StopGaugeAnimation()
  {
    this.isGaugeAnimationStopped = true;
  }
}
