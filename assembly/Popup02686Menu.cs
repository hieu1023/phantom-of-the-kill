﻿// Decompiled with JetBrains decompiler
// Type: Popup02686Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using System.Collections;
using UnityEngine;

public class Popup02686Menu : BackButtonMonoBehaiviour
{
  [SerializeField]
  private GameObject link_icon;
  [SerializeField]
  private UILabel txtDescription;
  [SerializeField]
  private UILabel txtTitle;
  [SerializeField]
  private UILabel txtShowText;
  private System.Action onCallback;

  public IEnumerator Init(Versus0268Menu.PvpParam.FirstBattleReward reward)
  {
    IEnumerator e = this.link_icon.GetOrAddComponent<CreateIconObject>().CreateThumbnail((MasterDataTable.CommonRewardType) reward.reward_type_id, reward.reward_id, 0, true, true, new CommonQuestType?(), false);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Consts instance = Consts.GetInstance();
    this.txtShowText.SetTextLocalize(Consts.Format(reward.show_text, (IDictionary) null));
    this.txtTitle.SetTextLocalize(Consts.Format(instance.VERSUS_002686_TITLE, (IDictionary) null));
    this.txtDescription.SetTextLocalize(Consts.Format(instance.VERSUS_002686_DESCRIPTION, (IDictionary) null));
  }

  public void SetCallback(System.Action callback)
  {
    this.onCallback = callback;
  }

  public void IbtnOK()
  {
    if (this.onCallback != null)
      this.onCallback();
    Singleton<PopupManager>.GetInstance().onDismiss();
  }

  public override void onBackButton()
  {
    this.IbtnOK();
  }
}
