﻿// Decompiled with JetBrains decompiler
// Type: DailyMissionPointRewardItemController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using System.Collections;
using UnityEngine;

public class DailyMissionPointRewardItemController : MonoBehaviour
{
  private DailyMissionController controller;
  [SerializeField]
  private GameObject acquiredEffect;
  [SerializeField]
  private UILabel rewardPointText;
  [SerializeField]
  private UI2DSprite rewardIconSprite;
  private PointRewardBox pointRewardBox;
  private int currentDailyPoint;
  private bool isReceived;
  private System.Action updateDailyPointRewardItemsAction;

  public void Init(
    DailyMissionController controller,
    PointRewardBox pointRewardBox,
    int currentDailyPoint,
    bool isReceived,
    System.Action updateDailyPointRewardItemsAction)
  {
    this.controller = controller;
    this.pointRewardBox = pointRewardBox;
    this.currentDailyPoint = currentDailyPoint;
    this.isReceived = isReceived;
    this.updateDailyPointRewardItemsAction = updateDailyPointRewardItemsAction;
    this.rewardPointText.SetTextLocalize(pointRewardBox.point);
    if (!isReceived)
    {
      this.acquiredEffect.SetActive(currentDailyPoint >= pointRewardBox.point);
      this.rewardIconSprite.color = Color.white;
    }
    else
    {
      this.acquiredEffect.SetActive(false);
      this.rewardIconSprite.color = Color.grey;
    }
  }

  public void OnClickDailyMissionPointReward()
  {
    this.controller.StartCoroutine(this.OpenMissionPointRewardDetailPopup());
  }

  private IEnumerator OpenMissionPointRewardDetailPopup()
  {
    IEnumerator e = Singleton<PopupManager>.GetInstance().open(this.controller.missionPointRewardDetailPopupPrefab, false, false, false, true, false, false, "SE_1006").GetComponent<MissionPointRewardDetailPopupController>().Init(this.controller, this.pointRewardBox, this.currentDailyPoint, this.isReceived, this.updateDailyPointRewardItemsAction);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }
}
