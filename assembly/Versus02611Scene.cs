﻿// Decompiled with JetBrains decompiler
// Type: Versus02611Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

public class Versus02611Scene : NGSceneBase
{
  [SerializeField]
  private Versus02611Menu menu;

  public static void ChangeScene(bool stack, WebAPI.Response.PvpBoot pvpInfo)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("versus026_11", (stack ? 1 : 0) != 0, (object) pvpInfo);
  }

  public IEnumerator onStartSceneAsync(WebAPI.Response.PvpBoot pvpInfo)
  {
    IEnumerator e = this.menu.Init(pvpInfo);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }
}
