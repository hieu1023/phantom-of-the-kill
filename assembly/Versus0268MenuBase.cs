﻿// Decompiled with JetBrains decompiler
// Type: Versus0268MenuBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Versus0268MenuBase : NGMenuBase
{
  [SerializeField]
  protected UILabel TxtAttention1;
  [SerializeField]
  protected UILabel TxtAttentionNum;
  [SerializeField]
  protected UILabel TxtCharaEXP26;
  [SerializeField]
  protected UILabel TxtDraw;
  [SerializeField]
  protected UILabel TxtLeague;
  [SerializeField]
  protected UILabel TxtLose;
  [SerializeField]
  protected UILabel TxtRank;
  [SerializeField]
  protected UILabel TxtRemain;
  [SerializeField]
  protected UILabel TxtRemainNum;
  [SerializeField]
  protected UILabel TxtResultTitleVersus30;
  [SerializeField]
  protected UILabel TxtSeason;
  [SerializeField]
  protected UILabel TxtWin;
  [SerializeField]
  protected UILabel TxtWinPoint;

  public virtual void IbtnReplay()
  {
    Debug.Log((object) "click default event IbtnReplay");
  }

  public virtual void SlcTouchToNext()
  {
    Debug.Log((object) "click default event SlcTouchToNext");
  }
}
