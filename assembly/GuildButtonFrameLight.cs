﻿// Decompiled with JetBrains decompiler
// Type: GuildButtonFrameLight
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

public class GuildButtonFrameLight : MonoBehaviour
{
  [SerializeField]
  private UI2DSprite sprFrame;
  [SerializeField]
  private UI2DSprite sprColor;

  public IEnumerator Init(Color color)
  {
    if ((Object) this.sprFrame != (Object) null)
      this.sprFrame.color = color;
    if ((Object) this.sprColor != (Object) null)
    {
      this.sprColor.color = color;
      yield break;
    }
  }
}
