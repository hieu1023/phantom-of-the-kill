﻿// Decompiled with JetBrains decompiler
// Type: WithNumberInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using UnityEngine;

public class WithNumberInfo
{
  public WithNumber icon;
  public bool buttonOn;
  private GuideUnitUnit ud;
  private WithNumber.ZUKAN_STATUS st;
  private GearKind gk;
  private CommonElement el;
  private SpriteCash sc;

  public bool IsMaterial
  {
    get
    {
      return this.ud.IsMaterial;
    }
    set
    {
      this.ud.IsMaterial = value;
    }
  }

  public GuideUnitUnit unitData
  {
    get
    {
      return this.ud;
    }
    set
    {
      this.ud = value;
    }
  }

  public WithNumber.ZUKAN_STATUS status
  {
    get
    {
      return this.st;
    }
    set
    {
      this.st = value;
    }
  }

  public GearKind gearKind
  {
    get
    {
      return this.gk;
    }
    set
    {
      this.gk = value;
    }
  }

  public CommonElement element
  {
    get
    {
      return this.el;
    }
    set
    {
      this.el = value;
    }
  }

  public SpriteCash spriteCash
  {
    get
    {
      return this.sc;
    }
    set
    {
      this.sc = value;
    }
  }

  public GameObject elementIconPrefab { get; set; }
}
