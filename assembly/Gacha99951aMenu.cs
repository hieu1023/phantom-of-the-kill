﻿// Decompiled with JetBrains decompiler
// Type: Gacha99951aMenu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System.Collections;
using UnityEngine;

public class Gacha99951aMenu : Quest99951Menu
{
  [SerializeField]
  protected UILabel TxtPopupdescripton04;
  [SerializeField]
  protected UILabel TxtPopupPossessionAmount;
  [SerializeField]
  protected UILabel TxtPopupStorageAmount;

  public override void SetText(int haveUnit, int max_haveUnit, Player player_data)
  {
    this.player_data_ = player_data;
    this.TxtPopupdescripton01.SetText(Consts.Format(Consts.GetInstance().GACHA_99951MENU_DESCRIPTION01, (IDictionary) null));
    this.TxtPopupdescripton02.SetText(Consts.Format(Consts.GetInstance().GACHA_99951MENU_DESCRIPTION02, (IDictionary) null));
    this.TxtPopupdescripton03.SetText(Consts.Format(Consts.GetInstance().GACHA_99951MENU_DESCRIPTION05, (IDictionary) null));
    this.TxtPopupPossessionAmount.SetText(Consts.Format(Consts.GetInstance().GACHA_99951MENU_DESCRIPTION07, (IDictionary) new Hashtable()
    {
      {
        (object) "haveunit",
        (object) haveUnit.ToString().ToConverter()
      },
      {
        (object) "maxhaveunit",
        (object) max_haveUnit.ToString().ToConverter()
      }
    }));
    this.TxtPopupdescripton04.gameObject.SetActive(false);
    this.TxtPopupStorageAmount.gameObject.SetActive(false);
    this.TxtTitle.SetText(Consts.Format(Consts.GetInstance().GACHA_99951MENU_DESCRIPTION04, (IDictionary) null));
  }

  public void SetText(
    int haveUnit,
    int max_haveUnit,
    int haveUnitReserves,
    int max_haveUnitReserves,
    Player player_data)
  {
    this.player_data_ = player_data;
    this.TxtPopupdescripton01.SetText(Consts.Format(Consts.GetInstance().GACHA_99951MENU_DESCRIPTION01, (IDictionary) null));
    this.TxtPopupdescripton02.SetText(Consts.Format(Consts.GetInstance().GACHA_99951MENU_DESCRIPTION02, (IDictionary) null));
    this.TxtPopupdescripton03.SetText(Consts.Format(Consts.GetInstance().GACHA_99951MENU_DESCRIPTION05, (IDictionary) null));
    this.TxtPopupPossessionAmount.SetText(Consts.Format(Consts.GetInstance().GACHA_99951MENU_DESCRIPTION07, (IDictionary) new Hashtable()
    {
      {
        (object) "haveunit",
        (object) haveUnit.ToString().ToConverter()
      },
      {
        (object) "maxhaveunit",
        (object) max_haveUnit.ToString().ToConverter()
      }
    }));
    this.TxtPopupdescripton04.SetText(Consts.Format(Consts.GetInstance().GACHA_99951MENU_DESCRIPTION06, (IDictionary) null));
    this.TxtPopupStorageAmount.SetText(Consts.Format(Consts.GetInstance().GACHA_99951MENU_DESCRIPTION07, (IDictionary) new Hashtable()
    {
      {
        (object) "haveunit",
        (object) haveUnitReserves.ToString().ToConverter()
      },
      {
        (object) "maxhaveunit",
        (object) max_haveUnitReserves.ToString().ToConverter()
      }
    }));
    this.TxtTitle.SetText(Consts.Format(Consts.GetInstance().GACHA_99951MENU_DESCRIPTION04, (IDictionary) null));
  }

  public override void onBackButton()
  {
    this.IbtnNo();
  }
}
