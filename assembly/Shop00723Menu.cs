﻿// Decompiled with JetBrains decompiler
// Type: Shop00723Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Shop00723Menu : BackButtonMenuBase
{
  private List<Shop00723UnitSelect> selectlist = new List<Shop00723UnitSelect>();
  [SerializeField]
  private UILabel txtTitle_;
  [SerializeField]
  private UILabel txtDetail_;
  [SerializeField]
  private UILabel txtExpirationDate_;
  [SerializeField]
  private UILabel txtQuantity_;
  [SerializeField]
  private UILabel txtQuantityDesabled_;
  [SerializeField]
  private UILabel txtCost_;
  [SerializeField]
  private NGTweenParts[] firstTweens_;
  [SerializeField]
  private NGTweenParts[] lastTweens_;
  [SerializeField]
  private Transform animeRoot_;
  [SerializeField]
  private NGxScroll scroll_;
  private bool isInitialized_;
  private Shop00723Scene scene_;
  private PlayerSelectTicketSummary playerUnitTicket_;
  private SM.SelectTicket unitTicket_;
  private SelectTicketSelectSample[] unitSamples_;
  private GameObject prefabUnit_;
  private GameObject prefabConfirmation_;
  private GameObject prefabSkillList_;
  private GameObject prefabLeader_;
  private GameObject prefabSkill_;
  private GameObject prefabIconUnit_;
  private GameObject prefabIconSkill_;
  private GameObject prefabIconElement_;
  private GameObject prefabIconGenre_;
  private GameObject prefabExchange_;
  private GameObject prefabExecute_;
  private GameObject charaAnime;
  private Shop00723UnitSelect[] units_;
  private SelectTicketSelectSample currentSample_;
  private UnitTypeEnum currentType_;
  private int quantity_;
  private Shop00723Menu.Phase phase_;
  private GameObject objEffect_;
  private const int QUANTITY_DISPLAY_MAX = 999;
  private const int DEFAULT_UNIT_TYPE = 1;
  private const int MARGIN_FULL_SCREEN_COLLISION = 40;
  private const int DEPTH_FULL_SCREEN_BUTTON = 40;

  public GameObject prefabLeader
  {
    get
    {
      return this.prefabLeader_;
    }
  }

  public GameObject prefabSkill
  {
    get
    {
      return this.prefabSkill_;
    }
  }

  public GameObject prefabIconUnit
  {
    get
    {
      return this.prefabIconUnit_;
    }
  }

  public GameObject prefabIconSkill
  {
    get
    {
      return this.prefabIconSkill_;
    }
  }

  public GameObject prefabIconElement
  {
    get
    {
      return this.prefabIconElement_;
    }
  }

  public GameObject prefabIconGenre
  {
    get
    {
      return this.prefabIconGenre_;
    }
  }

  public IEnumerator coInitialize(
    Shop00723Scene scene,
    SM.SelectTicket unitTicket,
    PlayerSelectTicketSummary playerUnitTicket)
  {
    Shop00723Menu shop00723Menu = this;
    shop00723Menu.scene_ = scene;
    shop00723Menu.playerUnitTicket_ = playerUnitTicket;
    shop00723Menu.unitTicket_ = unitTicket;
    shop00723Menu.phase_ = Shop00723Menu.Phase.Normal;
    shop00723Menu.txtTitle_.SetTextLocalize(string.Format(Consts.GetInstance().SHOP_00723_TITLE_NAME, (object) shop00723Menu.unitTicket_.name));
    shop00723Menu.txtExpirationDate_.SetTextLocalize(string.Format(Consts.GetInstance().SHOP_00723_EXPIRATION_DATE, (object) shop00723Menu.unitTicket_.end_at));
    shop00723Menu.quantity_ = shop00723Menu.playerUnitTicket_.quantity;
    shop00723Menu.txtCost_.SetTextLocalize(shop00723Menu.unitTicket_.cost);
    // ISSUE: reference to a compiler-generated method
    shop00723Menu.unitSamples_ = MasterData.SelectTicketSelectSample.Select<KeyValuePair<int, SelectTicketSelectSample>, SelectTicketSelectSample>((Func<KeyValuePair<int, SelectTicketSelectSample>, SelectTicketSelectSample>) (kv => kv.Value)).Where<SelectTicketSelectSample>(new Func<SelectTicketSelectSample, bool>(shop00723Menu.\u003CcoInitialize\u003Eb__47_1)).ToArray<SelectTicketSelectSample>();
    bool iswait;
    Future<GameObject> ldPrefab;
    IEnumerator e;
    if ((UnityEngine.Object) shop00723Menu.prefabUnit_ == (UnityEngine.Object) null)
    {
      iswait = true;
      ldPrefab = Res.Prefabs.shop007_23.dir_unit_exchange_list.Load<GameObject>();
      e = ldPrefab.Wait();
      while (e.MoveNext())
      {
        iswait = false;
        yield return e.Current;
      }
      e = (IEnumerator) null;
      shop00723Menu.prefabUnit_ = ldPrefab.Result;
      if ((UnityEngine.Object) shop00723Menu.prefabUnit_ == (UnityEngine.Object) null)
      {
        yield break;
      }
      else
      {
        if (iswait)
          yield return (object) null;
        ldPrefab = (Future<GameObject>) null;
      }
    }
    if ((UnityEngine.Object) shop00723Menu.prefabConfirmation_ == (UnityEngine.Object) null)
    {
      iswait = true;
      ldPrefab = Res.Prefabs.popup.popup_007_unit_exchange_confirmation__anim_popup01.Load<GameObject>();
      e = ldPrefab.Wait();
      while (e.MoveNext())
      {
        iswait = false;
        yield return e.Current;
      }
      e = (IEnumerator) null;
      shop00723Menu.prefabConfirmation_ = ldPrefab.Result;
      if ((UnityEngine.Object) shop00723Menu.prefabConfirmation_ == (UnityEngine.Object) null)
      {
        yield break;
      }
      else
      {
        if (iswait)
          yield return (object) null;
        ldPrefab = (Future<GameObject>) null;
      }
    }
    if ((UnityEngine.Object) shop00723Menu.prefabSkillList_ == (UnityEngine.Object) null)
    {
      iswait = true;
      ldPrefab = Res.Prefabs.shop007_unit_exchange.dir_unit_exchange_skill.Load<GameObject>();
      e = ldPrefab.Wait();
      while (e.MoveNext())
      {
        iswait = false;
        yield return e.Current;
      }
      e = (IEnumerator) null;
      shop00723Menu.prefabSkillList_ = ldPrefab.Result;
      if ((UnityEngine.Object) shop00723Menu.prefabSkillList_ == (UnityEngine.Object) null)
      {
        yield break;
      }
      else
      {
        if (iswait)
          yield return (object) null;
        ldPrefab = (Future<GameObject>) null;
      }
    }
    if ((UnityEngine.Object) shop00723Menu.prefabLeader_ == (UnityEngine.Object) null)
    {
      iswait = true;
      ldPrefab = Res.Prefabs.shop007_unit_exchange.dir_unit_leader_skill.Load<GameObject>();
      e = ldPrefab.Wait();
      while (e.MoveNext())
      {
        iswait = false;
        yield return e.Current;
      }
      e = (IEnumerator) null;
      shop00723Menu.prefabLeader_ = ldPrefab.Result;
      if ((UnityEngine.Object) shop00723Menu.prefabLeader_ == (UnityEngine.Object) null)
      {
        yield break;
      }
      else
      {
        if (iswait)
          yield return (object) null;
        ldPrefab = (Future<GameObject>) null;
      }
    }
    if ((UnityEngine.Object) shop00723Menu.prefabSkill_ == (UnityEngine.Object) null)
    {
      iswait = true;
      ldPrefab = Res.Prefabs.shop007_unit_exchange.dir_unit_skill_list.Load<GameObject>();
      e = ldPrefab.Wait();
      while (e.MoveNext())
      {
        iswait = false;
        yield return e.Current;
      }
      e = (IEnumerator) null;
      shop00723Menu.prefabSkill_ = ldPrefab.Result;
      if ((UnityEngine.Object) shop00723Menu.prefabSkill_ == (UnityEngine.Object) null)
      {
        yield break;
      }
      else
      {
        if (iswait)
          yield return (object) null;
        ldPrefab = (Future<GameObject>) null;
      }
    }
    if ((UnityEngine.Object) shop00723Menu.prefabIconUnit_ == (UnityEngine.Object) null)
    {
      iswait = true;
      ldPrefab = Res.Prefabs.UnitIcon.normal.Load<GameObject>();
      e = ldPrefab.Wait();
      while (e.MoveNext())
      {
        iswait = false;
        yield return e.Current;
      }
      e = (IEnumerator) null;
      shop00723Menu.prefabIconUnit_ = ldPrefab.Result;
      if ((UnityEngine.Object) shop00723Menu.prefabUnit_ == (UnityEngine.Object) null)
      {
        yield break;
      }
      else
      {
        if (iswait)
          yield return (object) null;
        ldPrefab = (Future<GameObject>) null;
      }
    }
    if ((UnityEngine.Object) shop00723Menu.prefabIconSkill_ == (UnityEngine.Object) null)
    {
      iswait = true;
      ldPrefab = Res.Prefabs.BattleSkillIcon._battleSkillIcon.Load<GameObject>();
      e = ldPrefab.Wait();
      while (e.MoveNext())
      {
        iswait = false;
        yield return e.Current;
      }
      e = (IEnumerator) null;
      shop00723Menu.prefabIconSkill_ = ldPrefab.Result;
      if ((UnityEngine.Object) shop00723Menu.prefabSkill_ == (UnityEngine.Object) null)
      {
        yield break;
      }
      else
      {
        if (iswait)
          yield return (object) null;
        ldPrefab = (Future<GameObject>) null;
      }
    }
    if ((UnityEngine.Object) shop00723Menu.prefabIconElement_ == (UnityEngine.Object) null)
    {
      iswait = true;
      ldPrefab = Res.Icons.CommonElementIcon.Load<GameObject>();
      e = ldPrefab.Wait();
      while (e.MoveNext())
      {
        iswait = false;
        yield return e.Current;
      }
      e = (IEnumerator) null;
      shop00723Menu.prefabIconElement_ = ldPrefab.Result;
      if ((UnityEngine.Object) shop00723Menu.prefabIconElement_ == (UnityEngine.Object) null)
      {
        yield break;
      }
      else
      {
        if (iswait)
          yield return (object) null;
        ldPrefab = (Future<GameObject>) null;
      }
    }
    if ((UnityEngine.Object) shop00723Menu.prefabIconGenre_ == (UnityEngine.Object) null)
    {
      iswait = true;
      ldPrefab = Res.Icons.SkillGenreIcon.Load<GameObject>();
      e = ldPrefab.Wait();
      while (e.MoveNext())
      {
        iswait = false;
        yield return e.Current;
      }
      e = (IEnumerator) null;
      shop00723Menu.prefabIconGenre_ = ldPrefab.Result;
      if ((UnityEngine.Object) shop00723Menu.prefabIconGenre_ == (UnityEngine.Object) null)
      {
        yield break;
      }
      else
      {
        if (iswait)
          yield return (object) null;
        ldPrefab = (Future<GameObject>) null;
      }
    }
    if ((UnityEngine.Object) shop00723Menu.prefabExchange_ == (UnityEngine.Object) null)
    {
      iswait = true;
      ldPrefab = Res.Prefabs.popup.popup_007_unit_exchange_confirmation__anim_popup01.Load<GameObject>();
      e = ldPrefab.Wait();
      while (e.MoveNext())
      {
        iswait = false;
        yield return e.Current;
      }
      e = (IEnumerator) null;
      shop00723Menu.prefabExchange_ = ldPrefab.Result;
      if ((UnityEngine.Object) shop00723Menu.prefabExchange_ == (UnityEngine.Object) null)
      {
        yield break;
      }
      else
      {
        if (iswait)
          yield return (object) null;
        ldPrefab = (Future<GameObject>) null;
      }
    }
    if ((UnityEngine.Object) shop00723Menu.prefabExecute_ == (UnityEngine.Object) null)
    {
      iswait = true;
      ldPrefab = Res.Prefabs.gacha006_effect.gacha_rarity.Load<GameObject>();
      e = ldPrefab.Wait();
      while (e.MoveNext())
      {
        iswait = false;
        yield return e.Current;
      }
      e = (IEnumerator) null;
      shop00723Menu.prefabExecute_ = ldPrefab.Result;
      if ((UnityEngine.Object) shop00723Menu.prefabExecute_ == (UnityEngine.Object) null)
      {
        yield break;
      }
      else
      {
        if (iswait)
          yield return (object) null;
        ldPrefab = (Future<GameObject>) null;
      }
    }
    if ((UnityEngine.Object) shop00723Menu.charaAnime == (UnityEngine.Object) null)
    {
      iswait = true;
      ldPrefab = Res.Prefabs.popup.popup_007_unit_exchange_confirmation__anim_chara.Load<GameObject>();
      e = ldPrefab.Wait();
      while (e.MoveNext())
      {
        iswait = false;
        yield return e.Current;
      }
      e = (IEnumerator) null;
      shop00723Menu.charaAnime = ldPrefab.Result;
      if ((UnityEngine.Object) shop00723Menu.charaAnime == (UnityEngine.Object) null)
      {
        yield break;
      }
      else
      {
        if (iswait)
          yield return (object) null;
        ldPrefab = (Future<GameObject>) null;
      }
    }
    e = shop00723Menu.coInitializeUnitSelect();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    GameObject gameObject = shop00723Menu.charaAnime.Clone(shop00723Menu.animeRoot_);
    gameObject.transform.position = Vector3.zero;
    gameObject.GetComponent<Shop00723AnimeManager>().SetInfo(shop00723Menu.unitTicket_.description);
    shop00723Menu.updateTicketQuantity(false);
    shop00723Menu.isInitialized_ = true;
  }

  private void UpdateTicketLimit()
  {
    foreach (Shop00723UnitSelect shop00723UnitSelect in this.selectlist)
      shop00723UnitSelect.UpdateInfo(this.playerUnitTicket_, this.currentSample_, this.quantity_, this.unitTicket_);
  }

  private void updateTicketQuantity(bool usedTicket = false)
  {
    if (usedTicket)
      this.quantity_ -= this.unitTicket_.cost;
    int num1 = this.quantity_ >= this.unitTicket_.cost ? 1 : 0;
    int num2 = Mathf.Clamp(this.quantity_, 0, 999);
    if (num1 != 0)
    {
      this.txtQuantity_.SetTextLocalize(num2);
      this.txtQuantity_.gameObject.SetActive(true);
      this.txtQuantityDesabled_.gameObject.SetActive(false);
    }
    else
    {
      this.txtQuantity_.gameObject.SetActive(false);
      this.txtQuantityDesabled_.SetTextLocalize(num2);
      this.txtQuantityDesabled_.gameObject.SetActive(true);
    }
  }

  private IEnumerator coInitializeUnitSelect()
  {
    Shop00723Menu menu = this;
    if (menu.unitSamples_ != null && menu.unitSamples_.Length != 0)
    {
      menu.scroll_.Clear();
      menu.units_ = (Shop00723UnitSelect[]) null;
      SelectTicketSelectSample[] ticketSelectSampleArray = menu.unitSamples_;
      for (int index = 0; index < ticketSelectSampleArray.Length; ++index)
      {
        SelectTicketSelectSample unitSample = ticketSelectSampleArray[index];
        Shop00723UnitSelect component = menu.prefabUnit_.Clone((Transform) null).GetComponent<Shop00723UnitSelect>();
        menu.scroll_.Add(component.gameObject, true);
        menu.selectlist.Add(component);
        IEnumerator e = component.coInitialize(menu, unitSample, menu.playerUnitTicket_, menu.unitTicket_);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
      }
      ticketSelectSampleArray = (SelectTicketSelectSample[]) null;
      menu.units_ = menu.selectlist.ToArray();
      menu.scroll_.ResolvePosition();
    }
  }

  public void onClickSkill(SelectTicketSelectSample unit)
  {
    if (!this.isInitialized_ || this.currentSample_ != null || this.IsPushAndSet())
      return;
    this.currentSample_ = unit;
    this.popupSkillMenu();
  }

  private void popupSkillMenu()
  {
    GameObject prefab = this.prefabSkillList_.Clone((Transform) null);
    UnitUnit value = (UnitUnit) null;
    if (!MasterData.UnitUnit.TryGetValue(this.currentSample_.reward_id, out value))
      Debug.LogError((object) ("Key Not Found: " + (object) this.currentSample_.reward_id));
    SelectTicketChoices choice = ((IEnumerable<SelectTicketChoices>) this.unitTicket_.choices).FirstOrDefault<SelectTicketChoices>((Func<SelectTicketChoices, bool>) (x => x.reward_id == value.ID));
    prefab.GetComponent<Shop00723PopupSkillMenu>().initialize(this, this.currentSample_, choice);
    Singleton<PopupManager>.GetInstance().open(prefab, false, false, true, true, true, true, "SE_1006");
  }

  public void onClosedSkill()
  {
    this.currentSample_ = (SelectTicketSelectSample) null;
  }

  public void onClickSelect(SelectTicketSelectSample unit)
  {
    if (!this.isInitialized_ || this.currentSample_ != null || this.IsPushAndSet())
      return;
    this.currentSample_ = unit;
    this.StartCoroutine(this.coPopupSelect());
  }

  private IEnumerator coPopupSelect()
  {
    Shop00723Menu menu = this;
    GameObject prefab = menu.prefabExchange_.Clone((Transform) null);
    Singleton<PopupManager>.GetInstance().open(prefab, false, false, true, true, false, false, "SE_1006");
    IEnumerator e = prefab.GetComponent<Shop00723PopupExchangeMenu>().coInitialize(menu, menu.currentSample_, menu.playerUnitTicket_, menu.unitTicket_);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public void onClosedSelect()
  {
    this.currentSample_ = (SelectTicketSelectSample) null;
  }

  public void doExchangeUnit(UnitTypeEnum unitType)
  {
    this.currentType_ = unitType;
    this.StartCoroutine(this.coExchangeUnit());
  }

  private IEnumerator coExchangeUnit()
  {
    this.phase_ = Shop00723Menu.Phase.EffectGet;
    GameObject bgo = this.createFullScreenButton();
    Singleton<PopupManager>.GetInstance().open(bgo, true, false, true, true, true, false, "SE_1006");
    Singleton<CommonRoot>.GetInstance().loadingMode = 1;
    Future<WebAPI.Response.SelectticketSpend> future = WebAPI.SelectticketSpend(this.currentSample_.ID, this.currentType_ != UnitTypeEnum.random ? (int) this.currentType_ : 1, (System.Action<WebAPI.Response.UserError>) (e =>
    {
      Singleton<PopupManager>.GetInstance().closeAll(false);
      Singleton<CommonRoot>.GetInstance().loadingMode = 0;
      WebAPI.DefaultUserErrorCallback(e);
      Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
    }));
    IEnumerator e1 = future.Wait();
    while (e1.MoveNext())
      yield return e1.Current;
    e1 = (IEnumerator) null;
    WebAPI.Response.SelectticketSpend result = future.Result;
    if (result != null)
    {
      e1 = OnDemandDownload.WaitLoadUnitResource(((IEnumerable<PlayerUnit>) result.player_units).Select<PlayerUnit, UnitUnit>((Func<PlayerUnit, UnitUnit>) (x => x.unit)), false, (IEnumerable<string>) null, false);
      while (e1.MoveNext())
        yield return e1.Current;
      e1 = (IEnumerator) null;
      this.updateTicketQuantity(true);
      this.UpdateTicketLimit();
      e1 = this.coPlayEffect(result);
      while (e1.MoveNext())
        yield return e1.Current;
      e1 = (IEnumerator) null;
      UIButton nextButton = bgo.GetComponent<UIButton>();
      EventDelegate.Set(nextButton.onClick, (EventDelegate.Callback) (() =>
      {
        if (!nextButton.enabled)
          return;
        GachaResultData.GetInstance().SetData(result);
        Singleton<NGSceneManager>.GetInstance().changeScene("gacha006_8", true, (object) ((IEnumerable<PlayerUnit>) result.player_units).FirstOrDefault<PlayerUnit>(), (object) result.is_new, (object) false);
        nextButton.enabled = false;
      }));
      Singleton<CommonRoot>.GetInstance().loadingMode = 0;
      nextButton.enabled = true;
      while (nextButton.enabled)
        yield return (object) null;
      Singleton<PopupManager>.GetInstance().dismiss(false);
      this.deleteEffect();
    }
  }

  private void OnEnable()
  {
    if (this.phase_ != Shop00723Menu.Phase.EffectGet)
      return;
    this.StartCoroutine(this.coWaitDateTime());
  }

  private IEnumerator coWaitDateTime()
  {
    Shop00723Menu shop00723Menu = this;
    shop00723Menu.IsPush = true;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    IEnumerator e = shop00723Menu.scene_.coCheckTicketDateTime(shop00723Menu.unitTicket_);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if (shop00723Menu.scene_.isErrorTicketDateTime_)
    {
      e = shop00723Menu.scene_.coEndSceneErrorTicketDateTime();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    else
    {
      Singleton<CommonRoot>.GetInstance().isLoading = false;
      shop00723Menu.phase_ = Shop00723Menu.Phase.Normal;
      shop00723Menu.IsPush = false;
      shop00723Menu.currentSample_ = (SelectTicketSelectSample) null;
    }
  }

  private void deleteEffect()
  {
    if (!((UnityEngine.Object) this.objEffect_ != (UnityEngine.Object) null))
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) this.objEffect_);
    this.objEffect_ = (GameObject) null;
  }

  private void OnDestroy()
  {
    this.deleteEffect();
  }

  private IEnumerator coPlayEffect(WebAPI.Response.SelectticketSpend result)
  {
    EffectControllerCouponExchange ce = this.prefabExecute_.Clone((Transform) null).GetComponent<EffectControllerCouponExchange>();
    IEnumerator e = ce.coExchangeUnit(result);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    bool bwait = true;
    ce.callbackOnFinishedEffect_ = (EventDelegate.Callback) (() => bwait = false);
    while (bwait)
      yield return (object) null;
    this.objEffect_ = ce.gameObject;
  }

  private GameObject createFullScreenButton()
  {
    GameObject gameObject = new GameObject("btnFullScreen");
    gameObject.AddComponent<UIWidget>().depth = 40;
    gameObject.AddComponent<BoxCollider>().size = (Vector3) new Vector2((float) (Screen.width + 40), (float) (Screen.height + 40));
    gameObject.AddComponent<UIButton>().enabled = false;
    return gameObject;
  }

  public override void onBackButton()
  {
    this.OnIbtnBack();
  }

  public void OnIbtnBack()
  {
    if (this.phase_ != Shop00723Menu.Phase.Normal || this.IsPushAndSet())
      return;
    this.scene_.shopFinish();
    this.backScene();
  }

  private enum Phase
  {
    Normal,
    EffectGet,
  }
}
