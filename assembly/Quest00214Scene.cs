﻿// Decompiled with JetBrains decompiler
// Type: Quest00214Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using UnityEngine;

public class Quest00214Scene : NGSceneBase
{
  private bool isInit = true;
  [SerializeField]
  private NGxScroll ScrollContainer;
  [SerializeField]
  private Quest00214Menu menu;
  [SerializeField]
  private Quest00214aMenu subMenu;

  public static void ChangeScene(bool stack)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("quest002_14", stack, (object[]) Array.Empty<object>());
  }

  public static void ChangeScene(bool stack, int unitId)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("quest002_14", (stack ? 1 : 0) != 0, (object) unitId, (object) false, (object) false);
  }

  public static void ChangeScene(bool stack, int unitOrQuestId, bool is_change_combi)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("quest002_14", (stack ? 1 : 0) != 0, (object) unitOrQuestId, (object) is_change_combi, (object) false);
  }

  public static void ChangeScene(bool stack, int unitId, bool is_change_combi, bool isSameUnit)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("quest002_14", (stack ? 1 : 0) != 0, (object) unitId, (object) is_change_combi, (object) isSameUnit);
  }

  public override IEnumerator Start()
  {
    Quest00214Scene quest00214Scene = this;
    // ISSUE: reference to a compiler-generated method
    IEnumerator e = quest00214Scene.\u003C\u003En__0();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    quest00214Scene.menuBases = new NGMenuBase[1]
    {
      (NGMenuBase) quest00214Scene.subMenu
    };
  }

  public IEnumerator onStartSceneAsyncForCharacter(
    int? unitId,
    bool is_change_combi,
    bool isSameUnit)
  {
    if (this.isInit)
    {
      IEnumerator e = this.menu.InitCharacterQuestButton(unitId, is_change_combi, isSameUnit);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    if (is_change_combi)
      this.menu.IbtnCombi();
    else
      this.menu.InitCombiQuestButton();
  }

  public IEnumerator onStartSceneAsync()
  {
    IEnumerator e = this.onStartSceneAsyncForCharacter(new int?(), false, false);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public void onStartScene()
  {
    if (this.isInit)
    {
      this.menu.InitializeEnd();
      this.isInit = false;
    }
    Singleton<CommonRoot>.GetInstance().isLoading = false;
  }

  public IEnumerator onStartSceneAsync(int unitId, bool isCombiQuest, bool isSameUnit)
  {
    Singleton<CommonRoot>.GetInstance().SetFooterEnable(true);
    IEnumerator e = this.onStartSceneAsyncForCharacter(new int?(unitId), isCombiQuest, isSameUnit);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public void onStartScene(int unitId, bool isCombiQuest, bool isSameUnit)
  {
    if (isCombiQuest)
      this.menu.IbtnCombi();
    else
      this.menu.InitCombiQuestButton();
    if (this.isInit)
    {
      this.menu.InitializeEnd();
      this.isInit = false;
    }
    Singleton<CommonRoot>.GetInstance().isLoading = false;
  }

  public override void onSceneInitialized()
  {
    base.onSceneInitialized();
    Singleton<CommonRoot>.GetInstance().loadingMode = 0;
  }
}
