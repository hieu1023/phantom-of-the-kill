﻿// Decompiled with JetBrains decompiler
// Type: Battle01SkillSelect
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections.Generic;
using UnityEngine;

public class Battle01SkillSelect : BattleHorizontalSelect<BL.Skill>
{
  private Battle01SelectNode selectNode;

  protected override void initialize(BE e)
  {
    this.selectNode = NGUITools.FindInParents<Battle01SelectNode>(this.transform);
    this.modified = BL.Observe<BL.ClassValue<List<BL.Skill>>>(new BL.ClassValue<List<BL.Skill>>(e.core.getFieldSkills(e.core.unitCurrent.unit)));
  }

  protected override Future<GameObject> resPrefab()
  {
    return this.battleManager.isSea ? Res.Prefabs.battle.Battle01_Skill_Select_sea.Load<GameObject>() : Res.Prefabs.battle.Battle01_Skill_Select.Load<GameObject>();
  }

  protected override void setParts(GameObject o, BL.Skill parts)
  {
    o.GetComponent<Battle01Skill>().setSkill(parts, this.env.core.unitCurrent.unit);
  }

  public override void onClick()
  {
    if (!this.battleManager.isBattleEnable)
      return;
    Battle01Skill inParents = NGUITools.FindInParents<Battle01Skill>(UICamera.selectedObject);
    if (!((Object) inParents != (Object) null) || !((Object) this.selectNode != (Object) null))
      return;
    this.selectNode.useSkillSubject(inParents.getUnit(), inParents.getSkill());
  }
}
