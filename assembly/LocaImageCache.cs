﻿// Decompiled with JetBrains decompiler
// Type: LocaImageCache
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Globalization;
using System.IO;
using UnityEngine;

public class LocaImageCache
{
  private Texture2D _texture = new Texture2D(0, 0, TextureFormat.ARGB32, false);
  private static bool enableSetLastWriteTime = true;
  private static string dir = Path.Combine(Application.temporaryCachePath, "image");
  private string filePath;
  private string url;
  private string _lastWriteTime;

  public string lastWriteTime
  {
    get
    {
      return this._lastWriteTime;
    }
  }

  public Texture2D texture
  {
    get
    {
      return this._texture;
    }
  }

  static LocaImageCache()
  {
    LocaImageCache.Cleaning();
  }

  public static void Clear()
  {
    try
    {
      if (!Directory.Exists(LocaImageCache.dir))
        return;
      Directory.Delete(LocaImageCache.dir, true);
    }
    catch (Exception ex)
    {
      Debug.LogError((object) ex);
    }
  }

  private static void Cleaning()
  {
    try
    {
      if (!Directory.Exists(LocaImageCache.dir))
        return;
      DateTime dateTime = DateTime.Today.AddDays(-3.0);
      foreach (string file in Directory.GetFiles(LocaImageCache.dir, "*", SearchOption.AllDirectories))
      {
        if (dateTime > File.GetLastAccessTime(file))
          File.Delete(file);
      }
    }
    catch (Exception ex)
    {
    }
  }

  public LocaImageCache(string url)
  {
    this.url = url;
    this.filePath = LocaImageCache.dir + new Uri(this.url).AbsolutePath;
  }

  public bool Read()
  {
    bool flag = false;
    try
    {
      if (File.Exists(this.filePath))
      {
        if (this._texture.LoadImage(File.ReadAllBytes(this.filePath)))
        {
          this._lastWriteTime = File.GetLastWriteTime(this.filePath).ToString("r");
          flag = true;
        }
      }
    }
    catch (Exception ex)
    {
    }
    return flag;
  }

  public bool Write(byte[] bytes, string lastWriteTime)
  {
    bool flag = false;
    try
    {
      string directoryName = Path.GetDirectoryName(this.filePath);
      if (!Directory.Exists(directoryName))
        Directory.CreateDirectory(directoryName);
      File.WriteAllBytes(this.filePath, bytes);
      if (LocaImageCache.enableSetLastWriteTime)
        File.SetLastWriteTime(this.filePath, DateTime.ParseExact(lastWriteTime, new string[2]
        {
          "ddd, d MMM yyyy HH':'mm':'ss zzz",
          "r"
        }, (IFormatProvider) DateTimeFormatInfo.InvariantInfo, DateTimeStyles.None));
      this._texture.LoadImage(bytes);
      this._lastWriteTime = lastWriteTime;
      flag = true;
    }
    catch (Exception ex)
    {
      LocaImageCache.enableSetLastWriteTime = false;
    }
    return flag;
  }
}
