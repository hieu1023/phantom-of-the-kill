﻿// Decompiled with JetBrains decompiler
// Type: TutorialUnitListPage
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class TutorialUnitListPage : TutorialPageBase
{
  private PlayerUnit[] baselayerUnits;
  private UnitIcon unitIconPrefab;

  public override IEnumerator Show()
  {
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    PlayerUnit[] players = SMManager.Get<PlayerUnit[]>();
    this.baselayerUnits = ((IEnumerable<PlayerUnit>) players).ToArray<PlayerUnit>();
    int? index = ((IEnumerable<PlayerUnit>) players).FirstIndexOrNull<PlayerUnit>((Func<PlayerUnit, bool>) (x => x.unit.ID == Singleton<TutorialRoot>.GetInstance().Resume.after_levelup1_player_unit.unit.ID));
    players[index.Value] = Singleton<TutorialRoot>.GetInstance().Resume.after_levelup1_player_unit;
    SMManager.UpdateList<PlayerUnit>(players, false);
    Unit00468Scene.changeScene00491Trans(false);
    Future<GameObject> f = Res.Prefabs.UnitIcon.normal.Load<GameObject>();
    IEnumerator e = f.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.unitIconPrefab = f.Result.Clone((Transform) null).GetComponent<UnitIcon>();
    e = this.unitIconPrefab.SetPlayerUnit(players[index.Value], new PlayerUnit[0], (PlayerUnit) null, false, false);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.unitIconPrefab.setBottom(players[index.Value]);
    this.unitIconPrefab.ShowBottomInfo(UnitSortAndFilter.SORT_TYPES.Level);
    this.unitIconPrefab.ForBattle = true;
    this.unitIconPrefab.gameObject.SetActive(false);
    Singleton<CommonRoot>.GetInstance().isLoading = false;
  }

  public override void Advise()
  {
    Singleton<TutorialRoot>.GetInstance().ForceShowAdviceInNextButton("newchapter_reincarnation2_tutorial", new Dictionary<string, Func<Transform, UIButton>>()
    {
      {
        "chapter_unit_Reincarnation_list",
        (Func<Transform, UIButton>) (root =>
        {
          UIButton componentInChildren = root.GetChildInFind("Top").GetComponentInChildren<UIButton>();
          this.unitIconPrefab.transform.parent = componentInChildren.transform;
          this.unitIconPrefab.transform.localPosition = Vector3.zero;
          this.unitIconPrefab.transform.localScale = new Vector3(1f, 1f, 1f);
          this.unitIconPrefab.gameObject.SetActive(true);
          return componentInChildren;
        })
      }
    }, (System.Action) (() =>
    {
      this.baselayerUnits[((IEnumerable<PlayerUnit>) this.baselayerUnits).FirstIndexOrNull<PlayerUnit>((Func<PlayerUnit, bool>) (x => x.unit.ID == Singleton<TutorialRoot>.GetInstance().Resume.after_levelup1_player_unit.unit.ID)).Value] = Singleton<TutorialRoot>.GetInstance().Resume.after_transmigrate_player_unit;
      Singleton<TutorialRoot>.GetInstance().Resume.player_units = this.baselayerUnits;
      SMManager.UpdateList<PlayerUnit>(this.baselayerUnits, false);
      this.NextPage();
    }));
  }
}
