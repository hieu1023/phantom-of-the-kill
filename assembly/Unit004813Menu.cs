﻿// Decompiled with JetBrains decompiler
// Type: Unit004813Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Unit004813Menu : BackButtonMenuBase
{
  private GameObject[] SkillLevelupObject = new GameObject[2];
  private Dictionary<int, UnityEngine.Sprite> dicSkillIcons = new Dictionary<int, UnityEngine.Sprite>();
  private bool isFirstTimeGaugeMax = true;
  [SerializeField]
  protected UIButton BackBtn;
  [SerializeField]
  private UILabel characterName;
  [SerializeField]
  private GameObject[] SlcLBiconNone;
  [SerializeField]
  private GameObject[] SlcLBiconBlue;
  [SerializeField]
  private GameObject[] SlcLBiconBlueEff;
  [SerializeField]
  private GameObject slc_Limitbreak;
  [SerializeField]
  private GameObject DirSkillLevelup;
  [SerializeField]
  private GameObject DirLvmax;
  [SerializeField]
  private UILabel beforeLvmax;
  [SerializeField]
  private UILabel afterLvmax;
  [SerializeField]
  private GameObject slcArrow;
  [SerializeField]
  private UILabel lvmaxUnchanged;
  [SerializeField]
  private UILabel beforeSelection;
  [SerializeField]
  private UILabel afterSelection;
  [SerializeField]
  private UILabel selectionUnchanged;
  [SerializeField]
  private GameObject selectionSlcArrow;
  [SerializeField]
  private GameObject unitTexture;
  [SerializeField]
  private GameObject growthParameter;
  [SerializeField]
  public LoveGaugeController loveGaugeController;
  [SerializeField]
  private UILabel textDearPercent;
  [SerializeField]
  private GameObject lnkOverkillersSkill;
  public UIButton skipButton;
  public List<List<PlayerUnit>> selectedMaterialPlayerUnits;
  public List<Unit004832Menu.ResultPlayerUnit> resultPlayerUnits;
  public List<Unit004832Menu.OhterInfo> otherInfos;
  public List<Dictionary<string, object>> showPopupDatas;
  private int displaySkillIndex;
  private GameObject SkillLevelupPrefab;
  private EffectSkillAcquisition overkillersSkill;
  private bool waitNextOverkillersSkill;
  private GrowthParameter parameterPanel;
  private PlayerUnit BaseUnit;
  private int medal;
  private bool isExtraSkillRelease;
  private bool isExtraSkillRemembere;
  private NGSoundManager sm;
  public Dictionary<string, object> showPopupData;
  private bool isDispNextSkill;
  private bool isPlayPopup;
  private System.Action postPlayPopup;

  public Unit00468Scene.Mode mode { get; set; }

  private void OnDisable()
  {
    if (!((UnityEngine.Object) this.sm != (UnityEngine.Object) null))
      return;
    this.sm.stopVoice(-1);
  }

  private IEnumerator CreateLevelupSkillList(
    PlayerUnit beforeUnit,
    PlayerUnit afterUnit,
    List<LevelupSkill> slist)
  {
    PlayerUnitSkills[] playerUnitSkillsArray1 = afterUnit.skills;
    for (int index1 = 0; index1 < playerUnitSkillsArray1.Length; ++index1)
    {
      PlayerUnitSkills afterSkill = playerUnitSkillsArray1[index1];
      bool flg_exist = false;
      PlayerUnitSkills[] playerUnitSkillsArray2 = beforeUnit.skills;
      for (int index2 = 0; index2 < playerUnitSkillsArray2.Length; ++index2)
      {
        PlayerUnitSkills beforeSkill = playerUnitSkillsArray2[index2];
        if (!flg_exist)
        {
          if (afterSkill.skill_id == beforeSkill.skill_id && beforeSkill.level < afterSkill.level)
          {
            slist.Add(new LevelupSkill(beforeSkill.level, afterSkill.level, afterSkill.skill));
            flg_exist = true;
          }
          UnitSkillEvolution unitSkillEvolution = UnitSkillEvolution.getUnitSkillEvolution(beforeUnit.unit.ID, beforeSkill.skill_id, afterSkill.skill_id);
          if (unitSkillEvolution != null)
          {
            slist.Add(new LevelupSkill(beforeSkill.level, unitSkillEvolution.level, beforeSkill.skill));
            flg_exist = true;
            yield return (object) this.AddEvolutionSkill(beforeSkill);
          }
        }
      }
      playerUnitSkillsArray2 = (PlayerUnitSkills[]) null;
      afterSkill = (PlayerUnitSkills) null;
    }
    playerUnitSkillsArray1 = (PlayerUnitSkills[]) null;
  }

  private IEnumerator AddEvolutionSkill(PlayerUnitSkills beforeSkill)
  {
    Future<UnityEngine.Sprite> iconF = beforeSkill.skill.LoadBattleSkillIcon((BattleFuncs.InvestSkill) null);
    IEnumerator e = iconF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.dicSkillIcons[beforeSkill.skill.ID] = iconF.Result;
  }

  private void SetMaxLv(int beforeLv, int afterLv)
  {
    bool flag = beforeLv < afterLv;
    this.beforeLvmax.gameObject.SetActive(flag);
    this.afterLvmax.gameObject.SetActive(flag);
    this.slcArrow.SetActive(flag);
    this.lvmaxUnchanged.gameObject.SetActive(!flag);
    if (flag)
    {
      this.beforeLvmax.SetTextLocalize(beforeLv);
      this.afterLvmax.SetTextLocalize(afterLv);
    }
    else
      this.lvmaxUnchanged.SetTextLocalize(afterLv);
  }

  private void SetSelection(float before, float after)
  {
    bool flag = (double) before < (double) after;
    this.beforeSelection.gameObject.SetActive(flag);
    this.afterSelection.gameObject.SetActive(flag);
    this.selectionSlcArrow.gameObject.SetActive(flag);
    this.selectionUnchanged.gameObject.SetActive(!flag);
    if (flag)
    {
      this.beforeSelection.SetTextLocalize((double) before < 99.0 ? before.ToString("f1") : before.ToString());
      this.afterSelection.SetTextLocalize((double) after < 99.0 ? after.ToString("f1") : after.ToString());
    }
    else
      this.selectionUnchanged.SetTextLocalize((double) after < 99.0 ? after.ToString("f1") : after.ToString());
  }

  private void SetLimitbreak(int beforeBreakCount, int afterBreakCount, int breakthrough_limit)
  {
    for (int index = 0; index < this.SlcLBiconNone.Length; ++index)
    {
      this.SlcLBiconNone[index].SetActive(index >= afterBreakCount && index < breakthrough_limit);
      this.SlcLBiconBlue[index].SetActive(index < afterBreakCount);
      this.SlcLBiconBlueEff[index].SetActive(index >= beforeBreakCount && index < afterBreakCount);
    }
    if (breakthrough_limit != 0)
      return;
    this.slc_Limitbreak.SetActive(false);
  }

  private IEnumerator SetUnitTexture(UnitUnit unit, int job_id)
  {
    foreach (Component component in this.unitTexture.transform)
      UnityEngine.Object.Destroy((UnityEngine.Object) component.gameObject);
    this.unitTexture.transform.Clear();
    Future<GameObject> future = unit.LoadMypage();
    IEnumerator e = future.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    GameObject texObj = future.Result.Clone(this.unitTexture.transform);
    e = unit.SetLargeSpriteSnap(job_id, texObj, 4);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    e = unit.SetLargeSpriteWithMask(job_id, texObj, Res.GUI._004_8_13_sozai.mask_Chara.Load<Texture2D>(), 5, -146, 36);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public IEnumerator setCharacter(
    PlayerUnit beforeUnit,
    PlayerUnit afterUnit,
    List<int> otherData)
  {
    Unit004813Menu m = this;
    m.isPlayPopup = false;
    bool isSuccess = otherData[0] == 1;
    m.medal = otherData[1];
    m.isExtraSkillRelease = otherData[2] == 1;
    m.isExtraSkillRemembere = otherData[3] == 1;
    m.BaseUnit = afterUnit;
    m.characterName.SetTextLocalize(afterUnit.unit.name);
    Future<GameObject> loader = GrowthParameter.LoadPrefab();
    IEnumerator e = loader.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    foreach (Component component in m.growthParameter.transform)
      UnityEngine.Object.Destroy((UnityEngine.Object) component.gameObject);
    m.growthParameter.transform.Clear();
    m.parameterPanel = GrowthParameter.GetInstance(loader.Result, m.growthParameter.transform);
    Future<GameObject> SkillLevelupPrefabF = Res.Prefabs.unit004_8_13.dir_skill.Load<GameObject>();
    e = SkillLevelupPrefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    m.SkillLevelupPrefab = SkillLevelupPrefabF.Result;
    if (afterUnit.skills != null)
    {
      PlayerUnitSkills[] playerUnitSkillsArray = afterUnit.skills;
      for (int index = 0; index < playerUnitSkillsArray.Length; ++index)
      {
        PlayerUnitSkills skill = playerUnitSkillsArray[index];
        Future<UnityEngine.Sprite> iconF = skill.skill.LoadBattleSkillIcon((BattleFuncs.InvestSkill) null);
        e = iconF.Wait();
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        if (!m.dicSkillIcons.ContainsKey(skill.skill.ID))
          m.dicSkillIcons[skill.skill.ID] = iconF.Result;
        iconF = (Future<UnityEngine.Sprite>) null;
        skill = (PlayerUnitSkills) null;
      }
      playerUnitSkillsArray = (PlayerUnitSkills[]) null;
    }
    m.SetMaxLv(beforeUnit.max_level, afterUnit.max_level);
    m.SetSelection(beforeUnit.unityTotal, afterUnit.unityTotal);
    m.SetLimitbreak(beforeUnit.breakthrough_count, afterUnit.breakthrough_count, afterUnit.unit.breakthrough_limit);
    UnitTypeParameter unitTypeParameter = afterUnit.UnitTypeParameter;
    int maxPt = ((IEnumerable<int>) new int[8]
    {
      afterUnit.hp.initial + afterUnit.hp.inheritance + afterUnit.hp.level_up_max_status + unitTypeParameter.hp_compose_max,
      afterUnit.strength.initial + afterUnit.strength.inheritance + afterUnit.strength.level_up_max_status + unitTypeParameter.strength_compose_max,
      afterUnit.intelligence.initial + afterUnit.intelligence.inheritance + afterUnit.intelligence.level_up_max_status + unitTypeParameter.intelligence_compose_max,
      afterUnit.vitality.initial + afterUnit.vitality.inheritance + afterUnit.vitality.level_up_max_status + unitTypeParameter.vitality_compose_max,
      afterUnit.mind.initial + afterUnit.mind.inheritance + afterUnit.mind.level_up_max_status + unitTypeParameter.mind_compose_max,
      afterUnit.agility.initial + afterUnit.agility.inheritance + afterUnit.agility.level_up_max_status + unitTypeParameter.agility_compose_max,
      afterUnit.dexterity.initial + afterUnit.dexterity.inheritance + afterUnit.dexterity.level_up_max_status + unitTypeParameter.dexterity_compose_max,
      afterUnit.lucky.initial + afterUnit.lucky.inheritance + afterUnit.lucky.level_up_max_status + unitTypeParameter.lucky_compose_max
    }).Max();
    m.parameterPanel.SetParameter(GrowthParameter.ParameterType.HP, beforeUnit.self_total_hp, afterUnit.self_total_hp, beforeUnit.hp.buildup != afterUnit.hp.buildup, maxPt);
    m.parameterPanel.SetParameter(GrowthParameter.ParameterType.STR, beforeUnit.self_total_strength, afterUnit.self_total_strength, beforeUnit.strength.buildup != afterUnit.strength.buildup, maxPt);
    m.parameterPanel.SetParameter(GrowthParameter.ParameterType.INT, beforeUnit.self_total_intelligence, afterUnit.self_total_intelligence, beforeUnit.intelligence.buildup != afterUnit.intelligence.buildup, maxPt);
    m.parameterPanel.SetParameter(GrowthParameter.ParameterType.VIT, beforeUnit.self_total_vitality, afterUnit.self_total_vitality, beforeUnit.vitality.buildup != afterUnit.vitality.buildup, maxPt);
    m.parameterPanel.SetParameter(GrowthParameter.ParameterType.MND, beforeUnit.self_total_mind, afterUnit.self_total_mind, beforeUnit.mind.buildup != afterUnit.mind.buildup, maxPt);
    m.parameterPanel.SetParameter(GrowthParameter.ParameterType.AGI, beforeUnit.self_total_agility, afterUnit.self_total_agility, beforeUnit.agility.buildup != afterUnit.agility.buildup, maxPt);
    m.parameterPanel.SetParameter(GrowthParameter.ParameterType.DEX, beforeUnit.self_total_dexterity, afterUnit.self_total_dexterity, beforeUnit.dexterity.buildup != afterUnit.dexterity.buildup, maxPt);
    m.parameterPanel.SetParameter(GrowthParameter.ParameterType.LUK, beforeUnit.self_total_lucky, afterUnit.self_total_lucky, beforeUnit.lucky.buildup != afterUnit.lucky.buildup, maxPt);
    m.parameterPanel.SetParameterMaxStar(GrowthParameter.ParameterType.HP, afterUnit.hp.is_max);
    m.parameterPanel.SetParameterMaxStar(GrowthParameter.ParameterType.STR, afterUnit.strength.is_max);
    m.parameterPanel.SetParameterMaxStar(GrowthParameter.ParameterType.INT, afterUnit.intelligence.is_max);
    m.parameterPanel.SetParameterMaxStar(GrowthParameter.ParameterType.VIT, afterUnit.vitality.is_max);
    m.parameterPanel.SetParameterMaxStar(GrowthParameter.ParameterType.MND, afterUnit.mind.is_max);
    m.parameterPanel.SetParameterMaxStar(GrowthParameter.ParameterType.AGI, afterUnit.agility.is_max);
    m.parameterPanel.SetParameterMaxStar(GrowthParameter.ParameterType.DEX, afterUnit.dexterity.is_max);
    m.parameterPanel.SetParameterMaxStar(GrowthParameter.ParameterType.LUK, afterUnit.lucky.is_max);
    m.parameterPanel.GaugesScaleZero();
    yield return (object) null;
    m.parameterPanel.SetParameterEffects();
    List<LevelupSkill> slist = new List<LevelupSkill>();
    yield return (object) m.CreateLevelupSkillList(beforeUnit, afterUnit, slist);
    List<LevelupSkill> levelupSkillList1 = new List<LevelupSkill>();
    List<LevelupSkill> levelupSkillList2 = new List<LevelupSkill>();
    int index1 = 0;
    int num1 = 0;
    for (; index1 < slist.Count<LevelupSkill>(); ++index1)
    {
      if (slist[index1].skill.skill_type != BattleskillSkillType.magic && slist[index1].skill.skill_type != BattleskillSkillType.leader)
      {
        if (num1 < 4)
          levelupSkillList1.Add(slist[index1]);
        else
          levelupSkillList2.Add(slist[index1]);
        ++num1;
      }
    }
    m.BackBtn.gameObject.SetActive(true);
    foreach (Component component in m.DirSkillLevelup.transform)
      UnityEngine.Object.Destroy((UnityEngine.Object) component.gameObject);
    m.DirSkillLevelup.transform.Clear();
    m.displaySkillIndex = 0;
    if (levelupSkillList1.Count<LevelupSkill>() > 0)
    {
      m.SkillLevelupObject[0] = m.SkillLevelupPrefab.Clone(m.DirSkillLevelup.transform);
      m.SkillLevelupObject[0].GetComponent<SkillLevelup>().SetSkillSlots(m, beforeUnit, afterUnit, m.dicSkillIcons, levelupSkillList1);
      m.SkillLevelupObject[0].GetComponent<SkillLevelup>().StartTween();
    }
    if (levelupSkillList2.Count<LevelupSkill>() > 0)
    {
      m.isDispNextSkill = true;
      m.SkillLevelupObject[1] = m.SkillLevelupPrefab.Clone(m.DirSkillLevelup.transform);
      m.SkillLevelupObject[1].GetComponent<SkillLevelup>().SetSkillSlots(m, beforeUnit, afterUnit, m.dicSkillIcons, levelupSkillList2);
      m.SkillLevelupObject[1].SetActive(false);
    }
    if ((UnityEngine.Object) m.lnkOverkillersSkill != (UnityEngine.Object) null && m.BaseUnit.unit.exist_overkillers_skill)
    {
      m.lnkOverkillersSkill.SetActive(false);
      OverkillersSkillRelease s = m.BaseUnit.overkillersSkill;
      if (s != null && (double) beforeUnit.unityTotal < (double) s.unity_value && (double) s.unity_value <= (double) afterUnit.unityTotal)
      {
        Future<GameObject> ldPrefab = EffectSkillAcquisition.createLoader();
        yield return (object) ldPrefab.Wait();
        m.overkillersSkill = ldPrefab.Result.Clone(m.lnkOverkillersSkill.transform).GetComponent<EffectSkillAcquisition>();
        m.overkillersSkill.initialize(s.skill);
        m.overkillersSkill.GetComponent<UIPanel>().depth = m.lnkOverkillersSkill.GetComponent<UIPanel>().depth + 1;
        m.lnkOverkillersSkill.GetComponentInChildren<UIButton>(true).gameObject.SetActive(true);
        ldPrefab = (Future<GameObject>) null;
      }
      else
        m.overkillersSkill = (EffectSkillAcquisition) null;
      s = (OverkillersSkillRelease) null;
    }
    else if ((UnityEngine.Object) m.lnkOverkillersSkill != (UnityEngine.Object) null)
      m.lnkOverkillersSkill.SetActive(false);
    Consts consts = Consts.GetInstance();
    m.loveGaugeController.gameObject.SetActive(false);
    if (afterUnit.unit.trust_target_flag)
    {
      string dearSpriteName = "slc_text_love_parameter.png__GUI__004-8-13_sozai__004-8-13_sozai_prefab";
      string relevanceSpriteName = "slc_text_Relevance.png__GUI__004-8-13_sozai__004-8-13_sozai_prefab";
      double num2 = Math.Round((double) afterUnit.trust_rate * 100.0) / 100.0;
      m.textDearPercent.SetTextLocalize(Consts.Format(Consts.GetInstance().UNIT_TRUST_RATE_PERCENT, (IDictionary) new Hashtable()
      {
        {
          (object) "trust_rate",
          (object) string.Format("{0}", (object) num2)
        }
      }));
      m.loveGaugeController.gameObject.SetActive(true);
      m.loveGaugeController.SetGaugeText(afterUnit.unit, dearSpriteName, relevanceSpriteName);
      m.StartCoroutine(m.loveGaugeController.setValue((int) beforeUnit.trust_rate, (int) afterUnit.trust_rate, (int) afterUnit.trust_max_rate, (int) consts.TRUST_RATE_LEVEL_SIZE, true, true));
    }
    e = m.SetUnitTexture(afterUnit.unit, afterUnit.job_id);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    m.sm = Singleton<NGSoundManager>.GetInstance();
    if ((UnityEngine.Object) m.sm != (UnityEngine.Object) null)
    {
      m.sm.stopVoice(-1);
      if (isSuccess)
        m.sm.playVoiceByID(afterUnit.unit.unitVoicePattern, 56, -1, 0.0f);
    }
    m.isFirstTimeGaugeMax = (double) beforeUnit.trust_rate < (double) consts.TRUST_RATE_LEVEL_SIZE && (double) afterUnit.trust_rate >= (double) consts.TRUST_RATE_LEVEL_SIZE;
  }

  public virtual void IbtnBack()
  {
    if (this.IsPushAndSet() || this.isPlayPopup)
    {
      if (!this.isPlayPopup || this.postPlayPopup == null)
        return;
      this.postPlayPopup();
      this.postPlayPopup = (System.Action) null;
    }
    else
    {
      this.isPlayPopup = true;
      this.postPlayPopup = (System.Action) null;
      this.StartCoroutine(this.PopupSequence());
    }
  }

  public void OnSkipButton()
  {
    this.selectedMaterialPlayerUnits.Clear();
    this.resultPlayerUnits.Clear();
    this.otherInfos.Clear();
    this.showPopupDatas.Clear();
    this.IbtnBack();
  }

  public override void onBackButton()
  {
    this.IbtnBack();
  }

  public void StartNextTween()
  {
    ++this.displaySkillIndex;
    if (this.displaySkillIndex < this.SkillLevelupObject.Length)
    {
      this.SkillLevelupObject[this.displaySkillIndex].SetActive(true);
      this.SkillLevelupObject[this.displaySkillIndex].GetComponent<SkillLevelup>().StartTween();
    }
    if (this.displaySkillIndex > this.SkillLevelupObject.Length - 1)
      return;
    this.isDispNextSkill = false;
  }

  private GameObject OpenPopup(GameObject original)
  {
    GameObject gameObject = Singleton<PopupManager>.GetInstance().open(original, false, false, false, true, false, false, "SE_1006");
    gameObject.transform.parent.Find("Popup Mask").gameObject.GetComponent<TweenAlpha>().to = 0.75f;
    return gameObject;
  }

  private IEnumerator PopupSequence()
  {
    Unit004813Menu unit004813Menu = this;
    Unit004813Menu.Runner[] runnerArray1 = new Unit004813Menu.Runner[8]
    {
      new Unit004813Menu.Runner(unit004813Menu.NextSkillPopup),
      new Unit004813Menu.Runner(unit004813Menu.OverkillersSkillPopup),
      new Unit004813Menu.Runner(unit004813Menu.MedalPopup),
      new Unit004813Menu.Runner(unit004813Menu.CharacterQuestStoryPopup),
      new Unit004813Menu.Runner(unit004813Menu.SkillRankUpPopup),
      new Unit004813Menu.Runner(unit004813Menu.ExtraSKillReleaseSequence),
      new Unit004813Menu.Runner(unit004813Menu.ExtraSKillRemembereSequence),
      new Unit004813Menu.Runner(unit004813Menu.FinishSequence)
    };
    unit004813Menu.loveGaugeController.StopGaugeAnimation();
    Unit004813Menu.Runner[] runnerArray = runnerArray1;
    for (int index = 0; index < runnerArray.Length; ++index)
    {
      IEnumerator e = runnerArray[index]();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    runnerArray = (Unit004813Menu.Runner[]) null;
  }

  private IEnumerator NextSkillPopup()
  {
    if (this.isDispNextSkill)
    {
      this.SkillLevelupObject[this.displaySkillIndex].GetComponent<SkillLevelup>().EndTween();
      while (this.isDispNextSkill)
        yield return (object) null;
    }
  }

  public void onClickedNextOverkillersSkill()
  {
    this.waitNextOverkillersSkill = false;
  }

  private IEnumerator OverkillersSkillPopup()
  {
    if (!((UnityEngine.Object) this.overkillersSkill == (UnityEngine.Object) null))
    {
      this.waitNextOverkillersSkill = true;
      this.lnkOverkillersSkill.SetActive(true);
      while (this.waitNextOverkillersSkill || this.overkillersSkill.isRunning)
        yield return (object) null;
      this.lnkOverkillersSkill.GetComponentInChildren<UIButton>().gameObject.SetActive(false);
    }
  }

  private IEnumerator MedalPopup()
  {
    if (this.medal != 0)
    {
      Future<GameObject> prefabf = Res.Prefabs.popup.popup_004_8_13__anim_popup01.Load<GameObject>();
      IEnumerator e = prefabf.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      Popup004813Menu component = Singleton<PopupManager>.GetInstance().openAlert(prefabf.Result, false, false, (EventDelegate) null, false, true, false, true).GetComponent<Popup004813Menu>();
      component.SetText(this.medal);
      bool isFinished = false;
      component.SetIbtnOKCallback((System.Action) (() => isFinished = true));
      while (!isFinished)
        yield return (object) null;
    }
  }

  private IEnumerator CharacterQuestStoryPopup()
  {
    UnlockQuest[] unlockQuests = (UnlockQuest[]) this.showPopupData["unlockQuests"];
    Future<GameObject> prefab = Res.Prefabs.battle.popup_020_11_2__anim_popup01.Load<GameObject>();
    IEnumerator e = prefab.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if (unlockQuests != null && unlockQuests.Length != 0)
    {
      UnlockQuest[] unlockQuestArray = unlockQuests;
      for (int index = 0; index < unlockQuestArray.Length; ++index)
      {
        QuestCharacterS quest = MasterData.QuestCharacterS[unlockQuestArray[index].quest_s_id];
        Singleton<NGSoundManager>.GetInstance().playSE("SE_1028", false, 0.8f, -1);
        Battle020112Menu o = this.OpenPopup(prefab.Result).GetComponent<Battle020112Menu>();
        e = o.Init(quest, false);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        bool isFinished = false;
        o.SetCallback((System.Action) (() => isFinished = true));
        while (!isFinished)
          yield return (object) null;
        o = (Battle020112Menu) null;
      }
      unlockQuestArray = (UnlockQuest[]) null;
    }
  }

  private IEnumerator SkillRankUpPopup()
  {
    int beforeSkillId = (int) this.showPopupData["beforeSkillId"];
    int afterSkillId = (int) this.showPopupData["afterSkillId"];
    if (beforeSkillId > 0 && afterSkillId > 0)
    {
      Future<GameObject> charaSkillPrefabF = Res.Prefabs.battle.CharaSkillRankUpPrefab.Load<GameObject>();
      IEnumerator e = charaSkillPrefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      GameObject popup = this.OpenPopup(charaSkillPrefabF.Result);
      popup.SetActive(false);
      Battle02029Menu o = popup.GetComponent<Battle02029Menu>();
      e = o.InitForEvolution(this.BaseUnit.unit.ID, afterSkillId, beforeSkillId);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      bool isFinished = false;
      o.SetCallback((System.Action) (() => isFinished = true));
      this.unitTexture.SetActive(false);
      popup.SetActive(true);
      yield return (object) new WaitForSeconds(0.6f);
      while (!isFinished)
        yield return (object) null;
      charaSkillPrefabF = (Future<GameObject>) null;
      popup = (GameObject) null;
      o = (Battle02029Menu) null;
    }
  }

  private IEnumerator ExtraSKillReleaseSequence()
  {
    if (this.isExtraSkillRelease)
    {
      yield return (object) new WaitForSeconds(0.6f);
      GameObject Prefab = (GameObject) null;
      Future<GameObject> prefabF;
      IEnumerator e;
      if (this.BaseUnit.unit.IsSea)
      {
        prefabF = new ResourceObject("Animations/extraskill/FavorabilityRatingEffect").Load<GameObject>();
        e = prefabF.Wait();
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        Prefab = prefabF.Result.Clone(Singleton<CommonRoot>.GetInstance().LoadTmpObj.transform);
        prefabF = (Future<GameObject>) null;
      }
      else if (this.BaseUnit.unit.IsResonanceUnit)
      {
        prefabF = new ResourceObject("Animations/common_gear_skill/CommonGearSkillEffect").Load<GameObject>();
        e = prefabF.Wait();
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        Prefab = prefabF.Result.Clone(Singleton<CommonRoot>.GetInstance().LoadTmpObj.transform);
        prefabF = (Future<GameObject>) null;
      }
      if (!((UnityEngine.Object) Prefab == (UnityEngine.Object) null))
      {
        bool isFinished = false;
        e = Prefab.GetComponent<FavorabilityRatingEffect>().Init(FavorabilityRatingEffect.AnimationType.SkillFrameRelease, this.BaseUnit, (System.Action) (() =>
        {
          Singleton<PopupManager>.GetInstance().dismiss(false);
          isFinished = true;
        }), false);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        Singleton<PopupManager>.GetInstance().open(Prefab, false, false, true, true, false, false, "SE_1006");
        Prefab.GetComponent<FavorabilityRatingEffect>().StartEffect();
        while (!isFinished)
          yield return (object) null;
        Prefab = (GameObject) null;
      }
    }
  }

  private IEnumerator ExtraSKillRemembereSequence()
  {
    if (this.isExtraSkillRemembere)
    {
      yield return (object) new WaitForSeconds(0.6f);
      GameObject Prefab = (GameObject) null;
      Future<GameObject> prefabF;
      IEnumerator e;
      if (this.BaseUnit.unit.IsSea)
      {
        prefabF = new ResourceObject("Animations/extraskill/FavorabilityRatingEffect").Load<GameObject>();
        e = prefabF.Wait();
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        Prefab = prefabF.Result.Clone(Singleton<CommonRoot>.GetInstance().LoadTmpObj.transform);
        prefabF = (Future<GameObject>) null;
      }
      else if (this.BaseUnit.unit.IsResonanceUnit)
      {
        prefabF = new ResourceObject("Animations/common_gear_skill/CommonGearSkillEffect").Load<GameObject>();
        e = prefabF.Wait();
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        Prefab = prefabF.Result.Clone(Singleton<CommonRoot>.GetInstance().LoadTmpObj.transform);
        prefabF = (Future<GameObject>) null;
      }
      if (!((UnityEngine.Object) Prefab == (UnityEngine.Object) null))
      {
        bool isFinished = false;
        e = Prefab.GetComponent<FavorabilityRatingEffect>().Init(FavorabilityRatingEffect.AnimationType.SkillRelease, this.BaseUnit, (System.Action) (() =>
        {
          Singleton<PopupManager>.GetInstance().dismiss(false);
          isFinished = true;
        }), !this.isFirstTimeGaugeMax);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        Singleton<PopupManager>.GetInstance().open(Prefab, false, false, true, true, false, false, "SE_1006");
        Prefab.GetComponent<FavorabilityRatingEffect>().StartEffect();
        while (!isFinished)
          yield return (object) null;
        Prefab = (GameObject) null;
      }
    }
  }

  private IEnumerator FinishSequence()
  {
    if ((UnityEngine.Object) this.parameterPanel != (UnityEngine.Object) null)
      this.parameterPanel.RemoveTween();
    if (this.mode == Unit00468Scene.Mode.UnitLumpTouta)
    {
      try
      {
        this.selectedMaterialPlayerUnits.RemoveAt(0);
        this.resultPlayerUnits.RemoveAt(0);
        this.otherInfos.RemoveAt(0);
        this.showPopupDatas.RemoveAt(0);
      }
      catch (ArgumentOutOfRangeException ex)
      {
      }
      if (this.selectedMaterialPlayerUnits.Count <= 0)
        Singleton<NGSceneManager>.GetInstance().changeScene("unit004_LumpTouta", false, (object[]) Array.Empty<object>());
      else
        Singleton<NGSceneManager>.GetInstance().changeScene("unit004_8_12", false, (object) this.selectedMaterialPlayerUnits, (object) this.resultPlayerUnits, (object) this.otherInfos, (object) this.showPopupDatas);
    }
    else if (this.mode != Unit00468Scene.Mode.Unit00420)
      Unit00484Scene.changeScene(false, this.BaseUnit, new PlayerUnit[5], this.mode);
    else
      Unit00420Scene.changeScene(false, this.BaseUnit, new PlayerUnit[5]);
    Singleton<NGSceneManager>.GetInstance().destroyScene("unit004_8_13");
    yield break;
  }

  private delegate IEnumerator Runner();
}
