﻿// Decompiled with JetBrains decompiler
// Type: Unit004OverkillersSlotUnitSelectMenu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;

public class Unit004OverkillersSlotUnitSelectMenu : UnitMenuBase
{
  private Unit004OverkillersSlotUnitSelectScene.Param param_;
  private bool isBaseEquipped_;
  private HashSet<int> excludeAnyEquippedIds_;
  private HashSet<int> excludeSameCharacterIds_;
  private HashSet<int> otherSlotUnitIds_;
  private const int CHARID_DYURIN = 2028;

  public IEnumerator initialize(Unit004OverkillersSlotUnitSelectScene.Param param)
  {
    Unit004OverkillersSlotUnitSelectMenu slotUnitSelectMenu = this;
    slotUnitSelectMenu.baseUnit = param.baseUnit;
    slotUnitSelectMenu.param_ = param;
    int id = param.baseUnit.id;
    UnitUnit unit1 = param.baseUnit.unit;
    int characterUnitCharacter = unit1.character_UnitCharacter;
    int sameCharacterId = unit1.same_character_id;
    PlayerUnit[] playerUnitArray = SMManager.Get<PlayerUnit[]>();
    List<PlayerUnit> lst = new List<PlayerUnit>(playerUnitArray.Length / 5);
    int selected_id;
    if (param.selectUnit != (PlayerUnit) null)
    {
      lst.Add(param.selectUnit);
      selected_id = param.selectUnit.id;
    }
    else
      selected_id = 0;
    slotUnitSelectMenu.isBaseEquipped_ = param.baseUnit.overkillers_base_id > 0;
    slotUnitSelectMenu.excludeAnyEquippedIds_ = new HashSet<int>();
    for (int index = 0; index < playerUnitArray.Length; ++index)
    {
      PlayerUnit playerUnit = playerUnitArray[index];
      if (id != playerUnit.id && (selected_id == 0 || selected_id != playerUnit.id))
      {
        UnitUnit unit2 = playerUnit.unit;
        if (unit2.character_UnitCharacter == 2028)
        {
          if (unit2.overkillers_parameter == 0 && !unit2.exist_overkillers_skill)
            continue;
        }
        else if (unit2.character_UnitCharacter != characterUnitCharacter || unit2.same_character_id == sameCharacterId || unit2.overkillers_parameter == 0)
          continue;
        if (playerUnit.isAnyOverkillersUnits)
          slotUnitSelectMenu.excludeAnyEquippedIds_.Add(playerUnit.id);
        lst.Add(playerUnit);
      }
    }
    slotUnitSelectMenu.excludeSameCharacterIds_ = new HashSet<int>();
    slotUnitSelectMenu.otherSlotUnitIds_ = new HashSet<int>();
    if (param.baseUnit.isAnyCacheOverkillersUnits)
    {
      for (int index = 0; index < param.baseUnit.cache_overkillers_units.Length; ++index)
      {
        if (index != param.slotNo && !(param.baseUnit.cache_overkillers_units[index] == (PlayerUnit) null))
        {
          slotUnitSelectMenu.excludeSameCharacterIds_.Add(param.baseUnit.cache_overkillers_units[index].unit.same_character_id);
          slotUnitSelectMenu.otherSlotUnitIds_.Add(param.baseUnit.cache_overkillers_units[index].id);
        }
      }
    }
    slotUnitSelectMenu.SetIconType(UnitMenuBase.IconType.Normal);
    yield return (object) slotUnitSelectMenu.Initialize();
    slotUnitSelectMenu.InitializeInfo((IEnumerable<PlayerUnit>) lst, (IEnumerable<PlayerMaterialUnit>) null, Persist.unit004OverkillersSlotUnitSortAndFilter, false, (uint) selected_id > 0U, true, true, false, (System.Action) null, 0);
    yield return (object) slotUnitSelectMenu.CreateUnitIcon();
    slotUnitSelectMenu.TxtNumber.SetTextLocalize(string.Format("{0}/{1}", (object) lst.Count, (object) Player.Current.max_units));
    slotUnitSelectMenu.lastReferenceUnitID = -1;
    slotUnitSelectMenu.InitializeEnd();
  }

  protected override IEnumerator CreateUnitIcon(
    int info_index,
    int unit_index,
    PlayerUnit baseUnit)
  {
    // ISSUE: reference to a compiler-generated field
    int num = this.\u003C\u003E1__state;
    Unit004OverkillersSlotUnitSelectMenu slotUnitSelectMenu = this;
    if (num != 0)
    {
      if (num != 1)
        return false;
      // ISSUE: reference to a compiler-generated field
      this.\u003C\u003E1__state = -1;
      slotUnitSelectMenu.postCreateUnitIcon(slotUnitSelectMenu.displayUnitInfos[info_index], slotUnitSelectMenu.allUnitIcons[unit_index]);
      return false;
    }
    // ISSUE: reference to a compiler-generated field
    this.\u003C\u003E1__state = -1;
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    this.\u003C\u003E2__current = (object) slotUnitSelectMenu.\u003C\u003En__0(info_index, unit_index, baseUnit);
    // ISSUE: reference to a compiler-generated field
    this.\u003C\u003E1__state = 1;
    return true;
  }

  protected override void CreateUnitIconCache(int info_index, int unit_index, PlayerUnit baseUnit)
  {
    base.CreateUnitIconCache(info_index, unit_index, baseUnit);
    this.postCreateUnitIcon(this.displayUnitInfos[info_index], this.allUnitIcons[unit_index]);
  }

  private void postCreateUnitIcon(UnitIconInfo dispInfo, UnitIconBase unitIcon)
  {
    if (dispInfo.removeButton)
    {
      unitIcon.onClick = (System.Action<UnitIconBase>) (x => this.onRemoveUnit());
      unitIcon.Gray = false;
    }
    else
    {
      unitIcon.onLongPress = (System.Action<UnitIconBase>) (x => Unit0042Scene.changeOverkillersUnitDetail(x.PlayerUnit, this.getUnits()));
      unitIcon.CanAwake = false;
      if (this.param_.selectUnit != (PlayerUnit) null && this.param_.selectUnit == unitIcon.PlayerUnit)
      {
        unitIcon.onClick = (System.Action<UnitIconBase>) (x => this.IbtnBack());
        unitIcon.Gray = false;
        unitIcon.Overkillers = true;
      }
      else if (this.isBaseEquipped_)
      {
        unitIcon.onClick = (System.Action<UnitIconBase>) (x => {});
        unitIcon.Gray = true;
        unitIcon.Overkillers = false;
      }
      else if (this.excludeAnyEquippedIds_.Contains(unitIcon.PlayerUnit.id))
      {
        unitIcon.onClick = (System.Action<UnitIconBase>) (x => {});
        unitIcon.Gray = true;
        unitIcon.Overkillers = true;
      }
      else if (this.excludeSameCharacterIds_.Contains(unitIcon.PlayerUnit.unit.same_character_id))
      {
        unitIcon.onClick = (System.Action<UnitIconBase>) (x => {});
        unitIcon.Gray = true;
        unitIcon.Overkillers = this.otherSlotUnitIds_.Contains(unitIcon.PlayerUnit.id);
      }
      else if (unitIcon.PlayerUnit.overkillers_base_id > 0)
      {
        unitIcon.onClick = (System.Action<UnitIconBase>) (x => {});
        unitIcon.Gray = true;
        unitIcon.Overkillers = true;
      }
      else
      {
        unitIcon.onClick = (System.Action<UnitIconBase>) (x => this.onSetUnit(x));
        unitIcon.Gray = false;
        unitIcon.Overkillers = false;
      }
      unitIcon.SetupDeckStatusBlink();
    }
  }

  protected override void Update()
  {
    base.Update();
    this.ScrollUpdate();
  }

  private void onRemoveUnit()
  {
    if (this.IsPushAndSet())
      return;
    this.StartCoroutine(this.doRemoveUnit());
  }

  private IEnumerator doRemoveUnit()
  {
    Unit004OverkillersSlotUnitSelectMenu slotUnitSelectMenu = this;
    Singleton<CommonRoot>.GetInstance().loadingMode = 1;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    Future<WebAPI.Response.UnitRemoveOverKillers> future = WebAPI.UnitRemoveOverKillers(slotUnitSelectMenu.param_.baseUnit.id, slotUnitSelectMenu.param_.slotNo, slotUnitSelectMenu.param_.selectUnit.id, new System.Action<WebAPI.Response.UserError>(slotUnitSelectMenu.errorWebAPI));
    yield return (object) future.Wait();
    if (future.Result != null)
    {
      yield return (object) GuildUtil.UpdateGuildDeck();
      Singleton<CommonRoot>.GetInstance().isLoading = false;
      slotUnitSelectMenu.param_.actSelected((PlayerUnit) null);
      slotUnitSelectMenu.backScene();
    }
  }

  private void onSetUnit(UnitIconBase icon)
  {
    if (this.IsPushAndSet())
      return;
    this.StartCoroutine(this.doSetUnit(icon.PlayerUnit));
  }

  private IEnumerator doSetUnit(PlayerUnit unit)
  {
    Unit004OverkillersSlotUnitSelectMenu slotUnitSelectMenu = this;
    if (slotUnitSelectMenu.checkOnceAlertOverkillersUnits())
    {
      int nSelect = 0;
      Consts instance = Consts.GetInstance();
      PopupCommonNoYes.Show(instance.POPUP_004_TITLE_ALERT_TEAMS_OVERKILLERS, instance.POPUP_004_MESSAGE_ALERT_TEAMS_OVERKILLERS, (System.Action) (() => nSelect = 1), (System.Action) (() => nSelect = 2), NGUIText.Alignment.Left, (string) null, NGUIText.Alignment.Left, false);
      while (nSelect == 0)
        yield return (object) null;
      if (nSelect == 2)
        yield break;
    }
    Singleton<CommonRoot>.GetInstance().loadingMode = 1;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    Future<WebAPI.Response.UnitSaveOverKillers> future = WebAPI.UnitSaveOverKillers(slotUnitSelectMenu.param_.baseUnit.id, slotUnitSelectMenu.param_.slotNo, unit.id, new System.Action<WebAPI.Response.UserError>(slotUnitSelectMenu.errorWebAPI));
    yield return (object) future.Wait();
    if (future.Result != null)
    {
      yield return (object) GuildUtil.UpdateGuildDeck();
      Singleton<CommonRoot>.GetInstance().isLoading = false;
      slotUnitSelectMenu.param_.actSelected(unit);
      slotUnitSelectMenu.backScene();
    }
  }

  private bool checkOnceAlertOverkillersUnits()
  {
    GuildRegistration guild = PlayerAffiliation.Current.guild;
    if (guild == null || string.IsNullOrEmpty(guild.guild_id))
      return false;
    Persist.GuildOverkillersAlertLog overkillersAlertLog;
    try
    {
      overkillersAlertLog = Persist.guildOverkillersAlertLog.Data;
    }
    catch
    {
      Persist.guildOverkillersAlertLog.Delete();
      overkillersAlertLog = new Persist.GuildOverkillersAlertLog();
      Persist.guildOverkillersAlertLog.Data = overkillersAlertLog;
    }
    string myId = Player.Current.id;
    if (Array.Find<GuildMembership>(guild.memberships, (Predicate<GuildMembership>) (x => x.player.player_id == myId)) == null)
      return false;
    int gvgId = guild.active_gvg_period_id.HasValue ? guild.active_gvg_period_id.Value : 0;
    if (overkillersAlertLog.guildID != guild.guild_id || overkillersAlertLog.gvgID != gvgId)
      overkillersAlertLog.reset(guild.guild_id, gvgId);
    int num = overkillersAlertLog.isAlertOverkillersUnits ? 1 : 0;
    if (num == 0)
      return num != 0;
    overkillersAlertLog.isAlertOverkillersUnits = false;
    Persist.guildOverkillersAlertLog.Flush();
    return num != 0;
  }

  private void errorWebAPI(WebAPI.Response.UserError e)
  {
    WebAPI.DefaultUserErrorCallback(e);
    Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
  }
}
