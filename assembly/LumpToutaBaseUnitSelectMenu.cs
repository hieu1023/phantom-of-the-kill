﻿// Decompiled with JetBrains decompiler
// Type: LumpToutaBaseUnitSelectMenu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class LumpToutaBaseUnitSelectMenu : UnitSelectMenuBase
{
  private List<PlayerUnit> selectedBasePlayerUnits = new List<PlayerUnit>();
  private List<List<UnitIconInfo>> selectedMaterialUnitIconInfos = new List<List<UnitIconInfo>>();
  private Dictionary<PlayerUnit, List<PlayerUnit>> allSamePlayerUnits = new Dictionary<PlayerUnit, List<PlayerUnit>>();
  private const int MAX_SELECT_BASE_UNIT = 10;
  [SerializeField]
  private UIPanel scrollPanel;
  [SerializeField]
  private UIWidget scrollBarWidget;
  [SerializeField]
  private UILabel useZeny;
  [SerializeField]
  private UILabel selectedCount;
  [SerializeField]
  private SpreadColorButton DecisionButton;

  public IEnumerator Init()
  {
    LumpToutaBaseUnitSelectMenu baseUnitSelectMenu = this;
    Singleton<CommonRoot>.GetInstance().ShowLoadingLayer(1, false);
    yield return (object) null;
    baseUnitSelectMenu.scrollPanel.bottomAnchor.absolute = 120;
    baseUnitSelectMenu.scrollBarWidget.bottomAnchor.absolute = 140;
    baseUnitSelectMenu.DecisionButton.isEnabled = false;
    baseUnitSelectMenu.isMaterial = true;
    IEnumerator e = baseUnitSelectMenu.Initialize();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    List<PlayerUnit> baseUnitPlayerList = new List<PlayerUnit>();
    e = baseUnitSelectMenu.CreateBasePlayerUnits(baseUnitPlayerList);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    baseUnitSelectMenu.SetIconType(UnitMenuBase.IconType.Normal);
    baseUnitSelectMenu.InitializeInfo((IEnumerable<PlayerUnit>) baseUnitPlayerList.ToArray(), (IEnumerable<PlayerMaterialUnit>) null, Persist.unit0048SortAndFilter, false, false, true, true, false, (System.Action) null, 0);
    e = baseUnitSelectMenu.CreateUnitIcon();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    baseUnitSelectMenu.InitializeEnd();
    Singleton<CommonRoot>.GetInstance().HideLoadingLayer();
  }

  private IEnumerator CreateBasePlayerUnits(List<PlayerUnit> baseUnitPlayerList)
  {
    LumpToutaBaseUnitSelectMenu baseUnitSelectMenu = this;
    float unityMax = (float) PlayerUnit.GetUnityValueMax();
    PlayerUnit[] baseUnits = ((IEnumerable<PlayerUnit>) SMManager.Get<PlayerUnit[]>()).Where<PlayerUnit>((Func<PlayerUnit, bool>) (x => x.unit.IsNormalUnit && (double) x.unityTotal < (double) unityMax)).ToArray<PlayerUnit>();
    int frameBalanceCount = 0;
    Dictionary<int, List<PlayerUnit>> unityMaterialUnits = new Dictionary<int, List<PlayerUnit>>();
    PlayerUnit[] playerUnitArray = baseUnits;
    int index;
    for (index = 0; index < playerUnitArray.Length; ++index)
    {
      PlayerUnit playerUnit = playerUnitArray[index];
      if (!baseUnitSelectMenu.IsExclusionUnitForLumpToutaMaterial(playerUnit))
      {
        if (!unityMaterialUnits.ContainsKey(playerUnit.unit.same_character_id))
          unityMaterialUnits.Add(playerUnit.unit.same_character_id, new List<PlayerUnit>());
        unityMaterialUnits[playerUnit.unit.same_character_id].Add(playerUnit);
      }
      ++frameBalanceCount;
      if (frameBalanceCount % 30 == 0)
        yield return (object) null;
    }
    playerUnitArray = (PlayerUnit[]) null;
    playerUnitArray = baseUnits;
    for (index = 0; index < playerUnitArray.Length; ++index)
    {
      PlayerUnit baseUnitKey = playerUnitArray[index];
      int sameCharacterId = baseUnitKey.unit.same_character_id;
      List<PlayerUnit> source = new List<PlayerUnit>();
      if (unityMaterialUnits.ContainsKey(sameCharacterId))
        source = unityMaterialUnits[sameCharacterId];
      baseUnitSelectMenu.allSamePlayerUnits.Add(baseUnitKey, source);
      if (source.Where<PlayerUnit>((Func<PlayerUnit, bool>) (x => x != baseUnitKey)).Count<PlayerUnit>() > 0)
        baseUnitPlayerList.Add(baseUnitKey);
      ++frameBalanceCount;
      if (frameBalanceCount % 200 == 0)
        yield return (object) null;
    }
    playerUnitArray = (PlayerUnit[]) null;
  }

  protected override void Select(UnitIconBase selectUnitIcon)
  {
    if (!selectUnitIcon.Selected && this.selectedBasePlayerUnits.Count >= 10)
      return;
    int sameCharacterId = selectUnitIcon.unit.same_character_id;
    List<UnitIconInfo> sameAllUnitInfos = new List<UnitIconInfo>();
    List<UnitIconBase> sameAllUnitIcons = new List<UnitIconBase>();
    foreach (UnitIconInfo allUnitInfo in this.allUnitInfos)
    {
      if (allUnitInfo.playerUnit != selectUnitIcon.PlayerUnit && allUnitInfo.unit.same_character_id == sameCharacterId)
      {
        sameAllUnitInfos.Add(allUnitInfo);
        foreach (UnitIconBase allUnitIcon in this.allUnitIcons)
        {
          if (allUnitIcon.PlayerUnit == allUnitInfo.playerUnit)
            sameAllUnitIcons.Add(allUnitIcon);
        }
      }
    }
    if (selectUnitIcon.Selected)
    {
      this.UnSelect(selectUnitIcon);
      this.LumpUnSelect(selectUnitIcon, sameAllUnitInfos, sameAllUnitIcons);
    }
    else
    {
      this.OnSelect(selectUnitIcon);
      this.LumpOnSelect(selectUnitIcon, sameAllUnitInfos, sameAllUnitIcons);
    }
    this.UpdateBottomInfo();
  }

  private void LumpOnSelect(
    UnitIconBase selectUnitIcon,
    List<UnitIconInfo> sameAllUnitInfos,
    List<UnitIconBase> sameAllUnitIcons)
  {
    this.selectedBasePlayerUnits.Add(selectUnitIcon.PlayerUnit);
    this.selectedMaterialUnitIconInfos.Add(sameAllUnitInfos);
    foreach (UnitIconInfo sameAllUnitInfo in sameAllUnitInfos)
      sameAllUnitInfo.select = 0;
    foreach (UnitIconBase sameAllUnitIcon in sameAllUnitIcons)
    {
      if (sameAllUnitIcon.Selected)
        this.UnSelect(sameAllUnitIcon);
      else
        this.OnSelect(sameAllUnitIcon);
      sameAllUnitIcon.HideCheckIcon();
    }
    if (this.selectedBasePlayerUnits.Count < 10)
      return;
    this.SelectWhiteAndNoSelectGray(true);
  }

  private void LumpUnSelect(
    UnitIconBase selectUnitIcon,
    List<UnitIconInfo> sameAllUnitInfos,
    List<UnitIconBase> sameAllUnitIcons)
  {
    int index1 = -1;
    for (int index2 = 0; index2 < this.selectedBasePlayerUnits.Count; ++index2)
    {
      if (this.selectedBasePlayerUnits[index2].unit.same_character_id == selectUnitIcon.unit.same_character_id)
      {
        index1 = index2;
        break;
      }
    }
    bool flag = false;
    if (this.selectedBasePlayerUnits.Count == 10)
      flag = true;
    this.selectedBasePlayerUnits.RemoveAt(index1);
    this.selectedMaterialUnitIconInfos.RemoveAt(index1);
    foreach (UnitIconInfo sameAllUnitInfo in sameAllUnitInfos)
      sameAllUnitInfo.select = -1;
    foreach (UnitIconBase sameAllUnitIcon in sameAllUnitIcons)
    {
      if (sameAllUnitIcon.Selected)
        this.UnSelect(sameAllUnitIcon);
      else
        this.OnSelect(sameAllUnitIcon);
    }
    if (!flag)
      return;
    this.SelectWhiteAndNoSelectGray(false);
  }

  private void SelectWhiteAndNoSelectGray(bool b)
  {
    List<PlayerUnit> playerUnitList = new List<PlayerUnit>();
    foreach (PlayerUnit selectedBasePlayerUnit in this.selectedBasePlayerUnits)
      playerUnitList.Add(selectedBasePlayerUnit);
    foreach (List<UnitIconInfo> materialUnitIconInfo in this.selectedMaterialUnitIconInfos)
    {
      foreach (UnitIconInfo unitIconInfo in materialUnitIconInfo)
        playerUnitList.Add(unitIconInfo.playerUnit);
    }
    foreach (UnitIconInfo allUnitInfo in this.allUnitInfos)
    {
      if (playerUnitList.Contains(allUnitInfo.playerUnit))
      {
        if ((UnityEngine.Object) allUnitInfo.icon != (UnityEngine.Object) null)
        {
          if (b)
            allUnitInfo.icon.Gray = false;
          else
            allUnitInfo.icon.Gray = true;
        }
      }
      else if ((UnityEngine.Object) allUnitInfo.icon != (UnityEngine.Object) null)
      {
        if (b)
          allUnitInfo.icon.Gray = true;
        else
          allUnitInfo.icon.Gray = false;
      }
    }
  }

  public override void OnSelect(UnitIconBase unitIcon)
  {
    unitIcon.SelectByCheckIcon(true);
    this.UnitInfoUpdates(unitIcon);
  }

  public override void UpdateSelectIcon()
  {
    foreach (UnitIconInfo selectedUnitIcon in this.selectedUnitIcons)
    {
      UnitIconInfo unitInfoDisplay = this.GetUnitInfoDisplay(selectedUnitIcon.playerUnit);
      if (unitInfoDisplay != null && (UnityEngine.Object) unitInfoDisplay.icon != (UnityEngine.Object) null)
      {
        if (!this.selectedBasePlayerUnits.Contains(selectedUnitIcon.playerUnit))
          unitInfoDisplay.icon.HideCheckIcon();
        else
          unitInfoDisplay.icon.SelectByCheckIcon(true);
      }
    }
  }

  protected override void CreateUnitIconAction(int info_index, int unit_index)
  {
    UnitIconBase allUnitIcon = this.allUnitIcons[unit_index];
    UnitIconInfo info = this.displayUnitInfos[info_index];
    if (this.selectedBasePlayerUnits.Count >= 10)
    {
      if (info.select == 0 && this.selectedBasePlayerUnits.Contains(allUnitIcon.PlayerUnit))
        allUnitIcon.SelectByCheckIcon(false);
      else if (info.select == 0)
      {
        allUnitIcon.SelectByCheckIcon(false);
        allUnitIcon.HideCheckIcon();
      }
      else
        allUnitIcon.Gray = true;
    }
    else if (info.select == 0 && this.selectedBasePlayerUnits.Contains(allUnitIcon.PlayerUnit))
      allUnitIcon.SelectByCheckIcon(true);
    else if (info.select == 0)
    {
      allUnitIcon.SelectByCheckIcon(true);
      allUnitIcon.HideCheckIcon();
    }
    else
      allUnitIcon.Gray = false;
    allUnitIcon.onClick = (System.Action<UnitIconBase>) (ui => this.Select(ui));
    allUnitIcon.onLongPress = (System.Action<UnitIconBase>) (x => Unit0042Scene.changeSceneEvolutionUnit(true, info.playerUnit, this.getUnits(), true, false, true));
  }

  public override bool SelectedUnitIsMax()
  {
    return false;
  }

  private void UpdateBottomInfo()
  {
    long useMoney = 0;
    float unityValueMax = (float) PlayerUnit.GetUnityValueMax();
    foreach (PlayerUnit selectedBasePlayerUnit in this.selectedBasePlayerUnits)
    {
      float unityTotal = selectedBasePlayerUnit.unityTotal;
      float num = 0.0f;
      List<PlayerUnit> playerUnitList = new List<PlayerUnit>();
      bool flag = false;
      foreach (PlayerUnit playerUnit in (IEnumerable<PlayerUnit>) this.allSamePlayerUnits[selectedBasePlayerUnit].OrderBy<PlayerUnit, int>((Func<PlayerUnit, int>) (x => x.unit.rarity.index)))
      {
        if (selectedBasePlayerUnit.id != playerUnit.id)
        {
          if (!flag)
          {
            if (playerUnitList.Count < 30)
            {
              if (!this.IsExclusionUnitForLumpToutaMaterial(playerUnit))
              {
                ++num;
                if ((double) num + (double) unityTotal >= (double) unityValueMax)
                  flag = true;
                playerUnitList.Add(playerUnit);
              }
            }
            else
              break;
          }
          else
            break;
        }
      }
      useMoney += CalcUnitCompose.priceCompose(selectedBasePlayerUnit, playerUnitList.ToArray());
    }
    if (useMoney >= SMManager.Get<Player>().money)
      this.useZeny.color = Color.red;
    else
      this.useZeny.color = Color.white;
    this.useZeny.SetTextLocalize(useMoney.ToString());
    if (this.selectedBasePlayerUnits.Count >= 10)
      this.selectedCount.color = Color.red;
    else
      this.selectedCount.color = Color.white;
    this.selectedCount.text = this.selectedBasePlayerUnits.Count.ToString() + string.Format("/{0}", (object) 10);
    this.SetDecisionButton(useMoney);
  }

  private void SetDecisionButton(long useMoney)
  {
    if (this.selectedBasePlayerUnits.Count <= 0)
      this.DecisionButton.isEnabled = false;
    else if (useMoney >= SMManager.Get<Player>().money)
      this.DecisionButton.isEnabled = false;
    else
      this.DecisionButton.isEnabled = true;
  }

  public override void IbtnYes()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<NGSceneManager>.GetInstance().changeScene("unit004_LumpTouta_Confirmation", true, (object) this.selectedBasePlayerUnits, (object) this.allSamePlayerUnits);
  }

  public override void IbtnClearS()
  {
    base.IbtnClearS();
    this.selectedBasePlayerUnits.Clear();
    this.selectedMaterialUnitIconInfos.Clear();
    this.UpdateBottomInfo();
  }
}
