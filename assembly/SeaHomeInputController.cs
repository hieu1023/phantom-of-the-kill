﻿// Decompiled with JetBrains decompiler
// Type: SeaHomeInputController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class SeaHomeInputController : MonoBehaviour
{
  [SerializeField]
  private SeaHomeCameraController cameraController;
  [SerializeField]
  private SeaHomeManager sceneObject;

  private void OnEnable()
  {
    UICamera.fallThrough = this.gameObject;
  }

  private void OnDisable()
  {
    UICamera.fallThrough = (GameObject) null;
  }

  private void OnClick()
  {
    if (UICamera.touchCount <= 0)
      return;
    this.sceneObject.SetTouchBipObject(this.hitObject("3DModels"));
  }

  private GameObject hitObject(string layer)
  {
    RaycastHit raycastHit = new RaycastHit();
    Ray ray = this.cameraController.mainCamera.ScreenPointToRay((Vector3) UICamera.lastTouchPosition);
    int num = 1 << LayerMask.NameToLayer(layer);
    ref RaycastHit local = ref raycastHit;
    int layerMask = num;
    return Physics.Raycast(ray, out local, 100f, layerMask) ? raycastHit.collider.gameObject : (GameObject) null;
  }
}
