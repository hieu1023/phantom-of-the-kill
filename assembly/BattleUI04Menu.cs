﻿// Decompiled with JetBrains decompiler
// Type: BattleUI04Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;
using UnityEngine;

public class BattleUI04Menu : BattleBackButtonMenuBase
{
  [SerializeField]
  private BattlePrepareCharacterInfoPlayer player;
  [SerializeField]
  private BattlePrepareCharacterInfoEnemy enemy;
  [SerializeField]
  private NGHorizontalScrollParts indicator;
  [SerializeField]
  private UIButton attackButton;
  private bool seEnable;
  private bool isInitialize;
  private AttackStatus[] attackStatus;
  private int backSkillCursor;
  private BL.UnitPosition attack;
  private BL.UnitPosition defense;
  private bool isComplited;

  public IEnumerator Init(BL.UnitPosition attack, BL.UnitPosition defense)
  {
    BattleUI04Menu battleUi04Menu = this;
    battleUi04Menu.seEnable = false;
    battleUi04Menu.attack = attack;
    battleUi04Menu.defense = defense;
    bool isFacility = attack.unit.isFacility || defense.unit.isFacility;
    battleUi04Menu.attackStatus = BattleFuncs.getAttackStatusArray(attack, defense, true, false, false);
    yield return (object) battleUi04Menu.player.Init(attack, battleUi04Menu.attackStatus, isFacility);
    AttackStatus[] attackStatusArray = BattleFuncs.getAttackStatusArray(defense, attack, false, false, false);
    yield return (object) battleUi04Menu.enemy.Init(defense, attackStatusArray, isFacility);
    battleUi04Menu.GetComponent<UIPanel>().SetDirty();
    battleUi04Menu.player.SetCompatibility(defense.unit.playerUnit);
    battleUi04Menu.enemy.SetCompatibility(attack.unit.playerUnit);
    battleUi04Menu.attackButton.isEnabled = (uint) battleUi04Menu.attackStatus.Length > 0U;
    battleUi04Menu.backSkillCursor = -1;
    battleUi04Menu.seEnable = true;
    battleUi04Menu.StartCoroutine(battleUi04Menu.WaitScrollSe());
    battleUi04Menu.isInitialize = true;
    battleUi04Menu.isComplited = false;
  }

  protected override void Update_Battle()
  {
    if (!this.isInitialize || this.indicator.selected == -1 || (this.backSkillCursor == this.indicator.selected || this.indicator.selected >= this.attackStatus.Length))
      return;
    this.player.setCurrentAttack(this.player.getAttackStatus(this.indicator.selected));
    this.backSkillCursor = this.indicator.selected;
  }

  public void onInitialized()
  {
    if (Singleton<NGBattleManager>.GetInstance().noDuelScene)
      return;
    Singleton<NGDuelDataManager>.GetInstance().StartBackGroundPreload(this.attack.unit, this.defense.unit);
  }

  public override void onBackButton()
  {
    if (this.isComplited)
      return;
    if (!Singleton<NGBattleManager>.GetInstance().noDuelScene)
    {
      Singleton<NGDuelDataManager>.GetInstance().StopBackGroundPreload();
      Singleton<NGDuelDataManager>.GetInstance().ClearOneTimeDuelCache();
    }
    this.backScene();
    this.isComplited = true;
  }

  public void onAttackButton()
  {
    if (this.isComplited)
      return;
    BL.UnitPosition current1 = this.player.getCurrent();
    BL.UnitPosition current2 = this.enemy.getCurrent();
    AttackStatus currentAttack = this.player.getCurrentAttack();
    if (this.attackStatus == null)
      return;
    if (!this.battleManager.useGameEngine)
    {
      this.battleManager.startDuel(BattleFuncs.calcDuel(currentAttack, current1, current2, false), (DuelEnvironment) null, false);
    }
    else
    {
      int attackOriginIndex = this.player.getCurrentAttackOriginIndex();
      this.battleManager.gameEngine.moveUnitWithAttack(current1, current2, currentAttack.isHeal, attackOriginIndex);
    }
    this.isComplited = true;
  }

  public void onEndScene()
  {
    this.eraseWithTween();
    this.seEnable = false;
    this.indicator.SeEnable = false;
    this.isComplited = false;
  }

  private IEnumerator WaitScrollSe()
  {
    yield return (object) new WaitForSeconds(0.3f);
    this.indicator.SeEnable = this.seEnable;
  }

  private void eraseWithTween()
  {
    if (!((Object) null != (Object) this.indicator))
      return;
    this.indicator.gameObject.GetComponent<TweenAlpha>().PlayReverse();
  }
}
