﻿// Decompiled with JetBrains decompiler
// Type: RaidBattlePreparationPopup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;
using System.Collections;
using UnityEngine;

public class RaidBattlePreparationPopup : MonoBehaviour
{
  [SerializeField]
  private RaidBattleSortiePopup sortiePopup;
  [SerializeField]
  private RaidBattleGuestSelectPopup guestSelectPopup;
  private PlayerUnit[] deckInfo;
  private PlayerUnit friend;
  private PlayerItem[] supply;
  private int[] usedUnitId;
  private IBattlePreparationPopup menu;
  private RaidBattlePreparationPopup.MODE mode;

  public IEnumerator InitializeAsync(
    IBattlePreparationPopup menu,
    RaidBattlePreparationPopup.MODE mode,
    GuildRaidPlayerHelpers[] helpers,
    string[] usedHelpers,
    string recommendCombat)
  {
    RaidBattlePreparationPopup parent = this;
    parent.menu = menu;
    parent.deckInfo = menu.GetPopupDeck();
    parent.friend = menu.GetPopupFriend();
    parent.supply = menu.GetPopupSupply();
    parent.usedUnitId = menu.GetPopupGrayUnitIds();
    yield return (object) parent.sortiePopup.InitializeAsync(parent, parent.deckInfo, parent.usedUnitId, parent.friend, parent.supply, recommendCombat);
    yield return (object) parent.guestSelectPopup.InitializeAsync(parent, helpers, usedHelpers);
    switch (mode)
    {
      case RaidBattlePreparationPopup.MODE.Sortie:
        parent.showSortie();
        break;
      case RaidBattlePreparationPopup.MODE.Guest:
        parent.showGuestSelect();
        break;
    }
  }

  public IEnumerator ReloadAsync()
  {
    this.deckInfo = this.menu.GetPopupDeck();
    this.friend = this.menu.GetPopupFriend();
    this.supply = this.menu.GetPopupSupply();
    this.usedUnitId = this.menu.GetPopupGrayUnitIds();
    if (this.mode == RaidBattlePreparationPopup.MODE.Sortie)
      yield return (object) this.sortiePopup.ReloadAsync(this.deckInfo, this.usedUnitId, this.friend, this.supply);
  }

  public void OnBackButton()
  {
    if (this.mode == RaidBattlePreparationPopup.MODE.Guest)
      this.showSortie();
    else
      this.menu.OnPopupClose();
  }

  public void OnGuestDecided(GvgCandidate friend)
  {
    this.friend = friend == null ? (PlayerUnit) null : friend.player_unit;
    GuildUtil.RaidFriend = friend;
    this.StartCoroutine(this.updateFriend());
  }

  private IEnumerator updateFriend()
  {
    yield return (object) this.sortiePopup.SetFriendUnit(this.friend);
    this.showSortie();
  }

  public void OnClickUnitIcon(PlayerUnit unit, bool isFriend)
  {
    if (isFriend)
      this.showGuestSelect();
    else
      this.menu.OnPopupUnitDetailOpen(unit, this.deckInfo, isFriend);
  }

  public void OnLongPressUnitIcon(PlayerUnit unit, bool isFriend)
  {
    if (unit == (PlayerUnit) null)
      return;
    PlayerUnit[] playerUnitArray;
    if (!isFriend)
      playerUnitArray = this.deckInfo;
    else
      playerUnitArray = new PlayerUnit[1]{ unit };
    PlayerUnit[] units = playerUnitArray;
    this.menu.OnPopupUnitDetailOpen(unit, units, isFriend);
  }

  public void OnAutoDeckEditButton()
  {
    this.menu.OnPopupAutoDeckEdit();
  }

  public void OnDeckEditButton()
  {
    this.menu.OnPopupDeckEditOpen(this.deckInfo);
  }

  public void OnGearEquipButton()
  {
    this.menu.OnPopupGearEquipOpen();
  }

  public void OnGearRepairButton()
  {
    this.menu.OnPopupGearRepairOpen();
  }

  public void OnSupplyEquipButton()
  {
    this.menu.OnPopupSupplyEquipOpen();
  }

  public void OnBattleConfigButton()
  {
    this.menu.OnPopupBattleConfigOpen();
  }

  public void OnSortieButton()
  {
    this.menu.OnPopupSortie();
  }

  private void showSortie()
  {
    this.guestSelectPopup.Hide();
    this.sortiePopup.gameObject.SetActive(true);
    this.sortiePopup.IsPush = false;
    this.mode = RaidBattlePreparationPopup.MODE.Sortie;
  }

  private void showGuestSelect()
  {
    this.sortiePopup.gameObject.SetActive(false);
    this.StartCoroutine(this.guestSelectPopup.Show());
    this.guestSelectPopup.IsPush = false;
    this.mode = RaidBattlePreparationPopup.MODE.Guest;
  }

  public enum MODE
  {
    Sortie,
    Guest,
  }
}
