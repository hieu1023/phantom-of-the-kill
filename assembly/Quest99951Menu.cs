﻿// Decompiled with JetBrains decompiler
// Type: Quest99951Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using UnityEngine;

public class Quest99951Menu : BackButtonMenuBase
{
  [SerializeField]
  protected UILabel TxtPopupdescripton01;
  [SerializeField]
  protected UILabel TxtPopupdescripton02;
  [SerializeField]
  protected UILabel TxtPopupdescripton03;
  [SerializeField]
  protected UILabel TxtTitle;
  protected Player player_data_;

  public virtual void SetText(int have_unit, int max_have_unit, Player player_data)
  {
    this.TxtPopupdescripton03.SetTextLocalize(Consts.GetInstance().GACHA_0065MENU_DESCRIPTION02 + "：[ff0000]" + have_unit.ToLocalizeNumberText() + "[-]/[ff0000]" + max_have_unit.ToLocalizeNumberText() + "[-]");
  }

  public void IbtnPopupCom()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<PopupManager>.GetInstance().onDismiss();
    Singleton<NGGameDataManager>.GetInstance().IsSea = false;
    Unit00468Scene.changeScene0048(true);
  }

  public void IbtnPopupDisjoint()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<PopupManager>.GetInstance().onDismiss();
    Singleton<NGGameDataManager>.GetInstance().IsSea = false;
    Unit00468Scene.changeScene00410(true, Unit00410Menu.FromType.AlertUnitOver);
  }

  public void IbtnPopupStorage()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<PopupManager>.GetInstance().onDismiss();
    Singleton<NGGameDataManager>.GetInstance().IsSea = false;
    Unit004StorageInScene.changeScene(true, true);
  }

  public void IbtnPopupExtend()
  {
    this.player_data_ = SMManager.Get<Player>();
    Singleton<PopupManager>.GetInstance().onDismiss();
    if (this.player_data_.CheckLimitMaxUnit())
      Singleton<PopupManager>.GetInstance().monitorCoroutine(PopupUtility._999_11_1());
    else
      Singleton<PopupManager>.GetInstance().monitorCoroutine(PopupUtility._007_14(0.0f));
  }

  public void IbtnNo()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<PopupManager>.GetInstance().onDismiss();
  }

  public override void onBackButton()
  {
    this.IbtnNo();
  }
}
