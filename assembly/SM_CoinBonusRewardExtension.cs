﻿// Decompiled with JetBrains decompiler
// Type: SM_CoinBonusRewardExtension
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using Gsc.Auth;
using SM;
using System;
using System.Collections.Generic;
using UniLinq;

public static class SM_CoinBonusRewardExtension
{
  public static IEnumerable<CoinBonusReward> GetActiveList(
    this CoinBonusReward[] self,
    string id)
  {
    IEnumerable<CoinBonusReward> source = ((IEnumerable<CoinBonusReward>) self).Where<CoinBonusReward>((Func<CoinBonusReward, bool>) (x => x.coin_product_id.platform == Device.Platform));
    if (source.Count<CoinBonusReward>() == 0)
      source = ((IEnumerable<CoinBonusReward>) self).Where<CoinBonusReward>((Func<CoinBonusReward, bool>) (x => x.coin_product_id.platform == "windows"));
    return source.Where<CoinBonusReward>((Func<CoinBonusReward, bool>) (x => x.coin_product_id.product_id == id));
  }
}
