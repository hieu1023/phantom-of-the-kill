﻿// Decompiled with JetBrains decompiler
// Type: PopupAppReview
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;
using UnityEngine;

public class PopupAppReview : BackButtonMenuBase
{
  public static bool Show(MonoBehaviour m)
  {
    if (!PopupAppReview.IsShow())
      return false;
    m.StartCoroutine(PopupAppReview.ShowReviewPopup());
    Persist.appReview.Data.isShow = true;
    Persist.appReview.Flush();
    return true;
  }

  private static bool IsShow()
  {
    return !Persist.appReview.Data.isShow && PopupAppReview.IsOSEnable();
  }

  private static bool IsOSEnable()
  {
    return false;
  }

  private static IEnumerator ShowReviewPopup()
  {
    Future<GameObject> f = new ResourceObject("Prefabs/popup/popup_Review__anim_popup01").Load<GameObject>();
    IEnumerator e = f.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Singleton<PopupManager>.GetInstance().open(f.Result, false, false, false, true, false, false, "SE_1006");
  }

  public void OnReview()
  {
    if (this.IsPushAndSet())
      return;
    Debug.LogWarningFormat("This OS→{0} is not app review supported", (object) SystemInfo.operatingSystem);
    Singleton<PopupManager>.GetInstance().onDismiss();
  }

  public void OnContact()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<PopupManager>.GetInstance().onDismiss();
    Help0154Scene.ChangeScene(true);
  }

  public override void onBackButton()
  {
    this.IbtnRetrun();
  }

  private void IbtnRetrun()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<PopupManager>.GetInstance().onDismiss();
  }
}
