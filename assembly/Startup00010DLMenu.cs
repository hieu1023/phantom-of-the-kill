﻿// Decompiled with JetBrains decompiler
// Type: Startup00010DLMenu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UniLinq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Startup00010DLMenu : MonoBehaviour
{
  private bool isArrowBtn = true;
  [SerializeField]
  private NGWrapScrollParts scroll;
  [SerializeField]
  private UILabel TxtPercentage;
  [SerializeField]
  private NGTweenGaugeScale gaugeScale;
  [SerializeField]
  private TweenAlpha fade;
  [SerializeField]
  private GameObject DownLoadVar;
  private int count;
  private bool prevDragging;
  private bool isScrolling;
  private GameObject tipsLabelObject;
  private StartupDownLoad.State state;

  public void onStartSceneAsync()
  {
    this.fade.PlayForward();
    this.gaugeScale.setValue(0, 100, false, -1f, -1f);
    StartupDownLoad.StartDownload(true, true);
  }

  public void Update()
  {
    float f = Mathf.Clamp((float) StartupDownLoad.progressNum() / (float) StartupDownLoad.progressDem() * 100f, 0.0f, 100f);
    this.TxtPercentage.SetTextLocalize(((int) f).ToString() + "％");
    this.gaugeScale.setValue(Mathf.FloorToInt(f), 100, false, -1f, -1f);
    this.state = StartupDownLoad.state;
    switch (this.state)
    {
      case StartupDownLoad.State.Processing:
        if ((int) StartupDownLoad.progressNum() == 0)
          break;
        this.DLStart();
        break;
      case StartupDownLoad.State.Completed:
        this.DLComplete();
        break;
    }
  }

  private void DLStart()
  {
    this.DownLoadVar.SetActive(true);
  }

  private void DLComplete()
  {
    this.StartCoroutine(this.SceneChange());
  }

  private IEnumerator SceneChange()
  {
    yield return (object) new WaitForSeconds(0.1f);
    SceneManager.LoadScene("main");
  }

  public void IbtnLeftArrow()
  {
    if (!this.isArrowBtn)
      return;
    this.isArrowBtn = false;
    this.isScrolling = true;
    this.count = this.scroll.selected - 1;
    if (this.count < 0)
      this.count = this.scroll.GetContentChildren().Count<GameObject>() - 1;
    this.scroll.centerOnChild.onFinished = (SpringPanel.OnFinished) (() =>
    {
      this.isArrowBtn = true;
      this.isScrolling = false;
    });
    this.scroll.setItemPosition(this.count);
  }

  public void IbtnRightArrow()
  {
    if (!this.isArrowBtn)
      return;
    this.isArrowBtn = false;
    this.isScrolling = true;
    this.count = this.scroll.selected + 1;
    if (this.count >= this.scroll.GetContentChildren().Count<GameObject>())
      this.count = 0;
    this.scroll.centerOnChild.onFinished = (SpringPanel.OnFinished) (() =>
    {
      this.isArrowBtn = true;
      this.isScrolling = false;
    });
    this.scroll.setItemPosition(this.count);
  }
}
