﻿// Decompiled with JetBrains decompiler
// Type: UIScrollViewExtension
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public static class UIScrollViewExtension
{
  public static void ResetHorizontalCenter(this UIScrollView self, float v)
  {
    if ((Object) self == (Object) null)
      return;
    UIPanel component = self.GetComponent<UIPanel>();
    if ((Object) component == (Object) null)
      return;
    self.DisableSpring();
    Transform transform = self.transform;
    Vector3 localPosition = transform.localPosition;
    Vector4 baseClipRegion = component.baseClipRegion;
    Vector2 clipOffset = component.clipOffset;
    if (self.movement == UIScrollView.Movement.Horizontal)
    {
      localPosition.x = -v;
      transform.localPosition = localPosition;
      clipOffset.x = v - baseClipRegion.x;
    }
    component.clipOffset = clipOffset;
  }
}
