﻿// Decompiled with JetBrains decompiler
// Type: Shop0074Scroll
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Shop0074Scroll : MonoBehaviour
{
  [SerializeField]
  private GameObject soldout;
  [SerializeField]
  private GameObject shortage;
  [SerializeField]
  private GameObject buy;
  [SerializeField]
  private UILabel quantityLabel;
  [SerializeField]
  private GameObject timerBase;
  [SerializeField]
  private UILabel timerLabel;
  [SerializeField]
  private UILabel piceLabel;
  [SerializeField]
  private UILabel priceLabel;
  [SerializeField]
  private UILabel ownLabel;
  [SerializeField]
  private UILabel titleLabel;
  [SerializeField]
  private UILabel descriptionLabel;
  [SerializeField]
  private BoxCollider ibtnDetailCollider;
  [SerializeField]
  private GameObject[] medalIconObject;
  [SerializeField]
  private GameObject dirThum;
  private GameObject unitIconObj;
  private GameObject itemIconObj;
  private GameObject uniqueIconObj;
  private PlayerQuestKey key;
  private PlayerSeasonTicket sTicket;
  private PlayerGachaTicket gTicket;
  private SM.SelectTicket uTicket;
  private PlayerSelectTicketSummary playerUnitTicket;
  private PlayerGuildFacility facility;
  private PlayerUnitTypeTicket uTypeTicket;
  private GameObject linkTarget;
  private System.Action<long> _onPurchasedHolding;
  private Func<IEnumerator> _onPurchased;
  private ShopArticleListMenu _menu;
  private int _playerItemQuantity;
  private int _playerUnitQuantity;
  private const int QUEST_KEY_MAX = 9999;

  public PlayerShopArticle _playerShopArticle { get; set; }

  public Shop _shop { get; set; }

  public bool IsSoldOut { get; set; }

  public IEnumerator Init(
    PlayerShopArticle playerShopArticle,
    Shop shop,
    ShopArticleListMenu menu,
    Func<IEnumerator> onPurchased,
    System.Action<long> onPurchasedHolding,
    GameObject unitIconPrefab,
    GameObject itemIconPrefab,
    GameObject uniqueIconPrefab)
  {
    this._menu = menu;
    this._playerShopArticle = playerShopArticle;
    this._shop = shop;
    int entity_id = this._playerShopArticle.article.ShopContents[0].entity_id;
    MasterDataTable.CommonRewardType entityType = this._playerShopArticle.article.ShopContents[0].entity_type;
    this._playerItemQuantity = 0;
    this._playerItemQuantity += SMManager.Get<PlayerItem[]>().AmountHavingTargetItem(entity_id, entityType);
    this._playerItemQuantity += SMManager.Get<PlayerMaterialGear[]>().AmountHavingTargetItem(entity_id);
    this._playerUnitQuantity = 0;
    if (MasterData.UnitUnit.ContainsKey(entity_id))
    {
      if (MasterData.UnitUnit[entity_id].IsMaterialUnit)
      {
        PlayerMaterialUnit playerMaterialUnit = ((IEnumerable<PlayerMaterialUnit>) SMManager.Get<PlayerMaterialUnit[]>()).FirstOrDefault<PlayerMaterialUnit>((Func<PlayerMaterialUnit, bool>) (x => x._unit == entity_id));
        if (playerMaterialUnit != null)
          this._playerUnitQuantity = playerMaterialUnit.quantity;
      }
      else
        this._playerUnitQuantity = SMManager.Get<PlayerUnit[]>().AmountHavingTargetUnit(entity_id, entityType);
    }
    this._onPurchasedHolding = onPurchasedHolding;
    this._onPurchased = onPurchased;
    this.key = this._playerShopArticle.article.ShopContents[0].entity_type == MasterDataTable.CommonRewardType.quest_key ? ((IEnumerable<PlayerQuestKey>) SMManager.Get<PlayerQuestKey[]>()).Where<PlayerQuestKey>((Func<PlayerQuestKey, bool>) (x => x.quest_key_id == this._playerShopArticle.article.ShopContents[0].entity_id)).FirstOrDefault<PlayerQuestKey>() : (PlayerQuestKey) null;
    long holding = 0;
    if (this._menu.currencyType == ShopArticleListMenu.CurrencyType.Zeny)
      holding = SMManager.Get<Player>().money;
    else if (this._menu.currencyType == ShopArticleListMenu.CurrencyType.BattleMedal)
      holding = (long) SMManager.Get<Player>().battle_medal;
    else if (this._menu.currencyType == ShopArticleListMenu.CurrencyType.RareMedal)
      holding = (long) SMManager.Get<Player>().medal;
    else if (this._menu.currencyType == ShopArticleListMenu.CurrencyType.TowerMedal)
      holding = TowerUtil.TowerPlayer != null ? (long) TowerUtil.TowerPlayer.tower_medal : 0L;
    else if (this._menu.currencyType == ShopArticleListMenu.CurrencyType.GuildMedal)
      holding = PlayerAffiliation.Current != null ? (long) PlayerAffiliation.Current.guild_medal : 0L;
    else if (this._menu.currencyType == ShopArticleListMenu.CurrencyType.RaidMedal)
      holding = (long) SMManager.Get<Player>().raid_medal;
    this.ArticleLimitValue(this._playerShopArticle.article, this._playerShopArticle, holding);
    this.titleLabel.SetTextLocalize(this._playerShopArticle.article.name);
    this.descriptionLabel.SetTextLocalize(this._playerShopArticle.article.description);
    ShopContent content = this._playerShopArticle.article.ShopContents[0];
    if ((UnityEngine.Object) this.ibtnDetailCollider != (UnityEngine.Object) null)
      this.ibtnDetailCollider.enabled = true;
    if ((UnityEngine.Object) this.unitIconObj != (UnityEngine.Object) null)
    {
      UnityEngine.Object.Destroy((UnityEngine.Object) this.unitIconObj);
      this.unitIconObj = (GameObject) null;
    }
    if ((UnityEngine.Object) this.itemIconObj != (UnityEngine.Object) null)
    {
      UnityEngine.Object.Destroy((UnityEngine.Object) this.itemIconObj);
      this.itemIconObj = (GameObject) null;
    }
    if ((UnityEngine.Object) this.uniqueIconObj != (UnityEngine.Object) null)
    {
      UnityEngine.Object.Destroy((UnityEngine.Object) this.uniqueIconObj);
      this.uniqueIconObj = (GameObject) null;
    }
    int quantity = 0;
    if ((UnityEngine.Object) this.ibtnDetailCollider != (UnityEngine.Object) null)
      this.ibtnDetailCollider.enabled = true;
    IEnumerator e;
    switch (content.entity_type)
    {
      case MasterDataTable.CommonRewardType.unit:
      case MasterDataTable.CommonRewardType.material_unit:
        this.unitIconObj = UnityEngine.Object.Instantiate<GameObject>(unitIconPrefab);
        this.unitIconObj.SetParent(this.dirThum);
        quantity = this._playerUnitQuantity;
        UnitUnit unit = (UnitUnit) null;
        if (MasterData.UnitUnit.TryGetValue(content.entity_id, out unit))
        {
          e = this.unitIconObj.GetComponent<UnitIcon>().SetUnit(unit, unit.GetElement(), false);
          while (e.MoveNext())
            yield return e.Current;
          e = (IEnumerator) null;
        }
        else
          this.unitIconObj.GetComponent<UnitIcon>().SetEmpty();
        this.unitIconObj.SetActive(true);
        this.unitIconObj.transform.localPosition = new Vector3(-239f, 0.0f, 0.0f);
        break;
      case MasterDataTable.CommonRewardType.supply:
        this.itemIconObj = UnityEngine.Object.Instantiate<GameObject>(itemIconPrefab);
        this.itemIconObj.SetParent(this.dirThum);
        quantity = this._playerItemQuantity;
        e = this.itemIconObj.GetComponent<ItemIcon>().InitByShopContent(content);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        this.itemIconObj.gameObject.SetActive(true);
        this.itemIconObj.transform.localPosition = new Vector3(-239f, -10f, 0.0f);
        break;
      case MasterDataTable.CommonRewardType.gear:
      case MasterDataTable.CommonRewardType.material_gear:
      case MasterDataTable.CommonRewardType.gear_body:
        this.itemIconObj = UnityEngine.Object.Instantiate<GameObject>(itemIconPrefab);
        this.itemIconObj.SetParent(this.dirThum);
        quantity = this._playerItemQuantity;
        e = this.itemIconObj.GetComponent<ItemIcon>().InitByShopContent(content);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        this.itemIconObj.gameObject.SetActive(true);
        this.itemIconObj.transform.localPosition = new Vector3(-239f, 0.0f, 0.0f);
        break;
      default:
        this.uniqueIconObj = UnityEngine.Object.Instantiate<GameObject>(uniqueIconPrefab);
        this.uniqueIconObj.SetParent(this.dirThum);
        UniqueIcons component = this.uniqueIconObj.GetComponent<UniqueIcons>();
        component.LabelActivated = false;
        this.uniqueIconObj.SetActive(true);
        quantity = -1;
        this.uniqueIconObj.transform.localPosition = new Vector3(-239f, -10f, 0.0f);
        switch (content.entity_type)
        {
          case MasterDataTable.CommonRewardType.money:
            e = component.SetZeny(0);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            break;
          case MasterDataTable.CommonRewardType.player_exp:
            e = component.SetPlayerExp(0);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            break;
          case MasterDataTable.CommonRewardType.unit_exp:
            e = component.SetUnitExp(0);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            break;
          case MasterDataTable.CommonRewardType.coin:
            e = component.SetKiseki(0);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            break;
          case MasterDataTable.CommonRewardType.recover:
            e = component.SetApRecover(0);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            break;
          case MasterDataTable.CommonRewardType.max_unit:
            e = component.SetMaxUnit(0);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            break;
          case MasterDataTable.CommonRewardType.max_item:
            e = component.SetMaxItem(0);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            break;
          case MasterDataTable.CommonRewardType.medal:
            e = component.SetMedal(0);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            break;
          case MasterDataTable.CommonRewardType.friend_point:
            e = component.SetPoint(0);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            break;
          case MasterDataTable.CommonRewardType.emblem:
            e = component.SetEmblem();
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            break;
          case MasterDataTable.CommonRewardType.battle_medal:
            e = component.SetBattleMedal(0);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            break;
          case MasterDataTable.CommonRewardType.cp_recover:
            e = component.SetCpRecover(0);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            break;
          case MasterDataTable.CommonRewardType.quest_key:
            quantity = this.key == null ? -1 : this.key.quantity;
            e = component.SetKey(content.entity_id, 0);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            break;
          case MasterDataTable.CommonRewardType.gacha_ticket:
            this.gTicket = ((IEnumerable<PlayerGachaTicket>) SMManager.Get<Player>().gacha_tickets).FirstOrDefault<PlayerGachaTicket>((Func<PlayerGachaTicket, bool>) (x => x.ticket.ID == content.entity_id));
            quantity = this.gTicket == null ? -1 : this.gTicket.quantity;
            if ((UnityEngine.Object) this.ibtnDetailCollider != (UnityEngine.Object) null)
              this.ibtnDetailCollider.enabled = false;
            e = component.SetGachaTicket(0, content.entity_id);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            break;
          case MasterDataTable.CommonRewardType.season_ticket:
            this.sTicket = ((IEnumerable<PlayerSeasonTicket>) SMManager.Get<PlayerSeasonTicket[]>()).FirstOrDefault<PlayerSeasonTicket>((Func<PlayerSeasonTicket, bool>) (x => x.season_ticket_id == content.entity_id));
            quantity = this.sTicket == null ? -1 : this.sTicket.quantity;
            e = component.SetSeasonTicket(0, content.entity_id);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            break;
          case MasterDataTable.CommonRewardType.mp_recover:
            e = component.SetMpRecover(0);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            break;
          case MasterDataTable.CommonRewardType.unit_ticket:
            this.playerUnitTicket = ((IEnumerable<PlayerSelectTicketSummary>) SMManager.Get<PlayerSelectTicketSummary[]>()).FirstOrDefault<PlayerSelectTicketSummary>((Func<PlayerSelectTicketSummary, bool>) (x => x.ticket_id == content.entity_id));
            this.uTicket = ((IEnumerable<SM.SelectTicket>) SMManager.Get<SM.SelectTicket[]>()).FirstOrDefault<SM.SelectTicket>((Func<SM.SelectTicket, bool>) (x => x.id == content.entity_id));
            quantity = this.uTicket == null ? -1 : this.playerUnitTicket.quantity;
            if ((UnityEngine.Object) this.ibtnDetailCollider != (UnityEngine.Object) null)
              this.ibtnDetailCollider.enabled = false;
            e = component.SetKillersTicket(content.entity_id, 0);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            break;
          case MasterDataTable.CommonRewardType.stamp:
            e = component.SetStamp(0);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            break;
          case MasterDataTable.CommonRewardType.awake_skill:
            quantity = ((IEnumerable<PlayerAwakeSkill>) SMManager.Get<PlayerAwakeSkill[]>()).Count<PlayerAwakeSkill>((Func<PlayerAwakeSkill, bool>) (x => x.skill_id == content.entity_id));
            e = component.SetAwakeSkill(content.entity_id);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            break;
          case MasterDataTable.CommonRewardType.guild_town:
            e = component.SetGuildMap(content.entity_id);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            break;
          case MasterDataTable.CommonRewardType.guild_facility:
            int id = 0;
            this.facility = ((IEnumerable<PlayerGuildFacility>) SMManager.Get<PlayerGuildFacility[]>()).FirstOrDefault<PlayerGuildFacility>((Func<PlayerGuildFacility, bool>) (x => x.master.ID == content.entity_id));
            if (this.facility != null)
            {
              quantity = this.facility.hasnum;
              id = this.facility.unit.ID;
            }
            else
            {
              quantity = 0;
              FacilityLevel facilityLevel = ((IEnumerable<FacilityLevel>) MasterData.FacilityLevelList).FirstOrDefault<FacilityLevel>((Func<FacilityLevel, bool>) (x => x.level == 1 && x.facility_MapFacility == content.entity_id));
              if (facilityLevel != null)
                id = facilityLevel.unit.ID;
            }
            if (id != 0)
            {
              e = component.SetGuildFacility(id);
              while (e.MoveNext())
                yield return e.Current;
              e = (IEnumerator) null;
              break;
            }
            break;
          case MasterDataTable.CommonRewardType.reincarnation_type_ticket:
            this.uTypeTicket = ((IEnumerable<PlayerUnitTypeTicket>) SMManager.Get<PlayerUnitTypeTicket[]>()).FirstOrDefault<PlayerUnitTypeTicket>((Func<PlayerUnitTypeTicket, bool>) (x => x.ticket_id == content.entity_id));
            quantity = this.uTypeTicket == null ? -1 : this.uTypeTicket.quantity;
            if ((UnityEngine.Object) this.ibtnDetailCollider != (UnityEngine.Object) null)
              this.ibtnDetailCollider.enabled = false;
            e = component.SetReincarnationTypeTicket(content.entity_id, 0);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            break;
          case MasterDataTable.CommonRewardType.challenge_point:
            e = component.SetItemIconCommonImage();
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            break;
        }
        break;
    }
    if (this._menu.currencyType == ShopArticleListMenu.CurrencyType.Zeny)
      this.piceLabel.SetTextLocalize(Consts.GetInstance().SHOP_0074_CURRENCY_TYPE_ZENY);
    else
      this.piceLabel.SetTextLocalize(Consts.GetInstance().SHOP_0074_CURRENCY_TYPE_DEFAULT);
    this.ownLabel.SetTextLocalize(quantity < 0 ? "-" : quantity.ToString());
    ((IEnumerable<GameObject>) this.medalIconObject).ToggleOnceEx((int) this._menu.currencyType);
  }

  public void ArticleLimitValue(
    ShopArticle shopArticle,
    PlayerShopArticle playerArticle,
    long holding)
  {
    bool flag = shopArticle.limit.HasValue && playerArticle.limit.Value <= 0 || shopArticle.daily_limit.HasValue && playerArticle.limit.Value <= 0;
    this.IsSoldOut = flag;
    if (flag)
    {
      this.soldout.SetActive(true);
      this.shortage.SetActive(false);
      this.buy.SetActive(false);
    }
    else if (holding < (long) shopArticle.price)
    {
      this.soldout.SetActive(false);
      this.shortage.SetActive(true);
      this.buy.SetActive(false);
    }
    else
    {
      this.shortage.SetActive(false);
      this.soldout.SetActive(false);
      this.buy.SetActive(true);
    }
    if (this._playerShopArticle.article.end_at.HasValue)
      ((MonoBehaviour) this._menu).StartCoroutine(this.UpdateServerTime());
    if ((this._playerShopArticle.article.limit.HasValue || this._playerShopArticle.article.daily_limit.HasValue) && this._playerShopArticle.limit.Value > 0)
    {
      this.quantityLabel.gameObject.SetActive(true);
      this.quantityLabel.SetTextLocalize(string.Format(Consts.GetInstance().SHOP_0074_SCROLL_ARTICLE_LIMIT_VALUE, (object) this._playerShopArticle.limit.Value));
    }
    else
      this.quantityLabel.gameObject.SetActive(false);
    this.priceLabel.SetTextLocalize(this._playerShopArticle.article.price.ToString());
  }

  private IEnumerator openPopup0076()
  {
    Shop0074Scroll shop0074Scroll = this;
    Player player = SMManager.Get<Player>();
    TowerPlayer towerPlayer = TowerUtil.TowerPlayer;
    PlayerAffiliation playerAffiliation = PlayerAffiliation.Current;
    Future<GameObject> prefab0076F = Res.Prefabs.popup.popup_007_6.Load<GameObject>();
    IEnumerator e = prefab0076F.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    GameObject prefab = prefab0076F.Result.Clone((Transform) null);
    GameObject gameObject = Singleton<PopupManager>.GetInstance().open(prefab, false, false, true, true, false, false, "SE_1006");
    int? quantityLimit = new int?();
    int playerQuantity;
    switch (shop0074Scroll._playerShopArticle.article.ShopContents[0].entity_type)
    {
      case MasterDataTable.CommonRewardType.unit:
      case MasterDataTable.CommonRewardType.material_unit:
        playerQuantity = shop0074Scroll._playerUnitQuantity;
        prefab.GetComponent<Shop0076Menu>().InitObject(shop0074Scroll.unitIconObj.gameObject);
        break;
      case MasterDataTable.CommonRewardType.supply:
      case MasterDataTable.CommonRewardType.gear:
      case MasterDataTable.CommonRewardType.material_gear:
      case MasterDataTable.CommonRewardType.gear_body:
        playerQuantity = shop0074Scroll._playerItemQuantity;
        prefab.GetComponent<Shop0076Menu>().InitObject(shop0074Scroll.itemIconObj.gameObject);
        break;
      case MasterDataTable.CommonRewardType.quest_key:
        quantityLimit = new int?(9999);
        PlayerQuestKey key = shop0074Scroll.key;
        playerQuantity = key != null ? key.quantity : 0;
        prefab.GetComponent<Shop0076Menu>().InitObject(shop0074Scroll.uniqueIconObj.gameObject);
        break;
      case MasterDataTable.CommonRewardType.gacha_ticket:
        playerQuantity = shop0074Scroll.gTicket == null ? 0 : shop0074Scroll.gTicket.quantity;
        prefab.GetComponent<Shop0076Menu>().InitObject(shop0074Scroll.uniqueIconObj.gameObject);
        break;
      case MasterDataTable.CommonRewardType.season_ticket:
        playerQuantity = shop0074Scroll.sTicket == null ? 0 : shop0074Scroll.sTicket.quantity;
        prefab.GetComponent<Shop0076Menu>().InitObject(shop0074Scroll.uniqueIconObj.gameObject);
        break;
      case MasterDataTable.CommonRewardType.unit_ticket:
        playerQuantity = shop0074Scroll.playerUnitTicket == null ? 0 : shop0074Scroll.playerUnitTicket.quantity;
        prefab.GetComponent<Shop0076Menu>().InitObject(shop0074Scroll.uniqueIconObj.gameObject);
        break;
      case MasterDataTable.CommonRewardType.awake_skill:
        // ISSUE: reference to a compiler-generated method
        playerQuantity = ((IEnumerable<PlayerAwakeSkill>) SMManager.Get<PlayerAwakeSkill[]>()).Count<PlayerAwakeSkill>(new Func<PlayerAwakeSkill, bool>(shop0074Scroll.\u003CopenPopup0076\u003Eb__45_0));
        prefab.GetComponent<Shop0076Menu>().InitObject(shop0074Scroll.uniqueIconObj.gameObject);
        break;
      case MasterDataTable.CommonRewardType.guild_facility:
        // ISSUE: reference to a compiler-generated method
        PlayerGuildFacility playerGuildFacility = ((IEnumerable<PlayerGuildFacility>) SMManager.Get<PlayerGuildFacility[]>()).FirstOrDefault<PlayerGuildFacility>(new Func<PlayerGuildFacility, bool>(shop0074Scroll.\u003CopenPopup0076\u003Eb__45_1));
        playerQuantity = playerGuildFacility != null ? playerGuildFacility.hasnum : 0;
        prefab.GetComponent<Shop0076Menu>().InitObject(shop0074Scroll.uniqueIconObj.gameObject);
        break;
      case MasterDataTable.CommonRewardType.reincarnation_type_ticket:
        playerQuantity = shop0074Scroll.uTypeTicket == null ? 0 : shop0074Scroll.uTypeTicket.quantity;
        prefab.GetComponent<Shop0076Menu>().InitObject(shop0074Scroll.uniqueIconObj.gameObject);
        break;
      case MasterDataTable.CommonRewardType.challenge_point:
        playerQuantity = Singleton<NGGameDataManager>.GetInstance().challenge_point;
        quantityLimit = new int?(Singleton<NGGameDataManager>.GetInstance().challenge_point_max);
        prefab.GetComponent<Shop0076Menu>().InitObject(shop0074Scroll.uniqueIconObj.gameObject);
        break;
      default:
        playerQuantity = 0;
        prefab.GetComponent<Shop0076Menu>().InitObject(shop0074Scroll.uniqueIconObj.gameObject);
        break;
    }
    shop0074Scroll.StartCoroutine(gameObject.GetComponent<Shop0076Menu>().Init(shop0074Scroll._playerShopArticle, shop0074Scroll._playerShopArticle.article, player, towerPlayer, playerAffiliation, playerQuantity, shop0074Scroll._menu.ScrollList, shop0074Scroll._shop.articles, shop0074Scroll._onPurchased, shop0074Scroll._onPurchasedHolding, quantityLimit));
  }

  private bool AddForStack()
  {
    foreach (PlayerItem playerItem in SMManager.Get<PlayerItem[]>())
    {
      if (playerItem.entity_id == this._playerShopArticle.article.ShopContents[0].entity_id && !playerItem.ForBattle && playerItem.quantity < 99)
        return true;
    }
    foreach (PlayerMaterialGear playerMaterialGear in SMManager.Get<PlayerMaterialGear[]>())
    {
      if (playerMaterialGear.gear_id == this._playerShopArticle.article.ShopContents[0].entity_id && playerMaterialGear.ForBattle && playerMaterialGear.quantity < 99)
        return true;
    }
    return false;
  }

  private void ButtonAction()
  {
    Player player = SMManager.Get<Player>();
    if (this._playerShopArticle.article.ShopContents[0].entity_type == MasterDataTable.CommonRewardType.supply || this._playerShopArticle.article.ShopContents[0].entity_type == MasterDataTable.CommonRewardType.material_gear || (this._playerShopArticle.article.ShopContents[0].entity_type == MasterDataTable.CommonRewardType.gear_body || this._playerShopArticle.article.ShopContents[0].entity_type == MasterDataTable.CommonRewardType.material_unit))
      this.StartCoroutine(this.openPopup0076());
    else if (this._playerShopArticle.article.ShopContents[0].entity_type == MasterDataTable.CommonRewardType.gear)
    {
      if (MasterData.GearGear.ContainsKey(this._playerShopArticle.article.ShopContents[0].entity_id) && MasterData.GearGear[this._playerShopArticle.article.ShopContents[0].entity_id].isMaterial())
        this.StartCoroutine(this.openPopup0076());
      else if (player.CheckMaxItem())
        this.StartCoroutine(PopupUtility._999_6_1(true));
      else if (player.CheckMaxReisou())
        this.StartCoroutine(PopupUtility.popupMaxReisou());
      else
        this.StartCoroutine(this.openPopup0076());
    }
    else if (this._playerShopArticle.article.ShopContents[0].entity_type == MasterDataTable.CommonRewardType.stamp)
      this.StartCoroutine(this.openPopup0076());
    else if (this._playerShopArticle.article.ShopContents[0].entity_type == MasterDataTable.CommonRewardType.unit)
    {
      if (MasterData.UnitUnit.ContainsKey(this._playerShopArticle.article.ShopContents[0].entity_id) && MasterData.UnitUnit[this._playerShopArticle.article.ShopContents[0].entity_id].IsMaterialUnit)
        this.StartCoroutine(this.openPopup0076());
      else if (player.CheckMaxUnit())
        this.StartCoroutine(PopupUtility._999_5_1());
      else
        this.StartCoroutine(this.openPopup0076());
    }
    else if (this._playerShopArticle.article.ShopContents[0].entity_type == MasterDataTable.CommonRewardType.quest_key)
    {
      if (this.key.max_quantity <= (this.key == null ? 0 : this.key.quantity))
        this.StartCoroutine(PopupUtility._999_15(MasterData.QuestkeyQuestkey[this.key.quest_key_id].name));
      else
        this.StartCoroutine(this.openPopup0076());
    }
    else if (this._playerShopArticle.article.ShopContents[0].entity_type == MasterDataTable.CommonRewardType.season_ticket)
    {
      if (this.sTicket != null && this.sTicket.max_quantity <= this.sTicket.quantity)
        this.StartCoroutine(PopupUtility._999_15(MasterData.SeasonTicketSeasonTicket[this.sTicket.season_ticket_id].name));
      else
        this.StartCoroutine(this.openPopup0076());
    }
    else if (this._playerShopArticle.article.ShopContents[0].entity_type == MasterDataTable.CommonRewardType.guild_facility)
    {
      if (this.facility != null && this.facility.hasnum >= this._playerShopArticle.article.ShopContents[0].upper_limit_count)
        this.StartCoroutine(PopupCommon.Show(Consts.GetInstance().POPUP_GUILD_SHOP_LIMIT_OVER_TITLE, Consts.GetInstance().POPUP_GUILD_SHOP_LIMIT_OVER_DESC, (System.Action) null));
      else
        this.StartCoroutine(this.openPopup0076());
    }
    else if (this._playerShopArticle.article.ShopContents[0].entity_type == MasterDataTable.CommonRewardType.reincarnation_type_ticket)
    {
      if (this.uTypeTicket != null && this._playerShopArticle.article.ShopContents[0].upper_limit_check && this.uTypeTicket.quantity >= this._playerShopArticle.article.ShopContents[0].upper_limit_count)
        this.StartCoroutine(PopupCommon.Show(Consts.GetInstance().POPUP_GUILD_SHOP_LIMIT_OVER_TITLE, Consts.GetInstance().POPUP_GUILD_SHOP_LIMIT_OVER_DESC, (System.Action) null));
      else
        this.StartCoroutine(this.openPopup0076());
    }
    else if (this._playerShopArticle.article.ShopContents[0].entity_type == MasterDataTable.CommonRewardType.challenge_point)
    {
      NGGameDataManager instance = Singleton<NGGameDataManager>.GetInstance();
      if (instance.challenge_point >= instance.challenge_point_max)
        this.StartCoroutine(PopupUtility._999_15(this._playerShopArticle.article.name));
      else
        this.StartCoroutine(this.openPopup0076());
    }
    else
      this.StartCoroutine(this.openPopup0076());
  }

  public void onBuy()
  {
    if (this._menu.IsPushAndSet())
      return;
    this.ButtonAction();
  }

  public void onChange()
  {
    if (this._menu.IsPushAndSet())
      return;
    this.ButtonAction();
  }

  public void onDetail()
  {
    switch (this._playerShopArticle.article.ShopContents[0].entity_type)
    {
      case MasterDataTable.CommonRewardType.gacha_ticket:
        break;
      case MasterDataTable.CommonRewardType.unit_ticket:
        break;
      case MasterDataTable.CommonRewardType.stamp:
        break;
      case MasterDataTable.CommonRewardType.challenge_point:
        break;
      default:
        if (this._menu.IsPushAndSet())
          break;
        this.StartCoroutine(this.setDetailPopup());
        break;
    }
  }

  private IEnumerator setDetailPopup()
  {
    Shop0074Scroll shop0074Scroll = this;
    if (shop0074Scroll._playerShopArticle.article.ShopContents[0].entity_type == MasterDataTable.CommonRewardType.guild_town)
      shop0074Scroll.StartCoroutine(shop0074Scroll.setMapDetailPopup());
    else if (shop0074Scroll._playerShopArticle.article.ShopContents[0].entity_type == MasterDataTable.CommonRewardType.guild_facility)
    {
      shop0074Scroll.StartCoroutine(shop0074Scroll.setFacilityDetailPopup());
    }
    else
    {
      GameObject popup = Singleton<PopupManager>.GetInstance().open(shop0074Scroll._menu.DetailPopup, false, false, false, true, false, false, "SE_1006");
      popup.SetActive(false);
      IEnumerator e = popup.GetComponent<Shop00742Menu>().Init(shop0074Scroll._playerShopArticle.article.ShopContents[0].entity_type, shop0074Scroll._playerShopArticle);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      popup.SetActive(true);
    }
  }

  private IEnumerator setMapDetailPopup()
  {
    if (!((UnityEngine.Object) this._menu.MapDetailPopup == (UnityEngine.Object) null))
    {
      GameObject popup = Singleton<PopupManager>.GetInstance().open(this._menu.MapDetailPopup, false, false, false, true, false, false, "SE_1006");
      popup.SetActive(false);
      IEnumerator e = popup.GetComponent<PopupMapDetailMenu>().InitializeAsync(this._playerShopArticle.article.ShopContents[0]);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      popup.SetActive(true);
    }
  }

  private IEnumerator setFacilityDetailPopup()
  {
    if (!((UnityEngine.Object) this._menu.FacilityDetailPopup == (UnityEngine.Object) null))
    {
      GameObject popup = Singleton<PopupManager>.GetInstance().open(this._menu.FacilityDetailPopup, false, false, false, true, false, false, "SE_1006");
      popup.SetActive(false);
      IEnumerator e = popup.GetComponent<PopupFacilityDetailMenu>().InitializeAsync(this._playerShopArticle.article.ShopContents[0].entity_id);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      popup.SetActive(true);
    }
  }

  private IEnumerator UpdateServerTime()
  {
    this.timerBase.SetActive(true);
    IEnumerator e = ServerTime.WaitSync();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.timerLabel.SetTextLocalize(Consts.Format(Consts.GetInstance().SHOP_0074_SCROLL_UPDATE_SERVER_TIME, (IDictionary) new Hashtable()
    {
      {
        (object) "time",
        (object) (this._playerShopArticle.article.end_at.Value - ServerTime.NowAppTime()).DisplayString()
      }
    }));
    this.timerLabel.gameObject.SetActive(true);
  }
}
