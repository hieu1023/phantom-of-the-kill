﻿// Decompiled with JetBrains decompiler
// Type: Bugu0059Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Bugu0059Scene : NGSceneBase
{
  private Bugu0059Menu menu;
  private bool resetFlag;

  public static void changeScene(bool stack, GameCore.ItemInfo itemInfo)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("bugu005_9", (stack ? 1 : 0) != 0, (object) itemInfo, null);
  }

  public static void changeScene(bool stack, GameCore.ItemInfo itemInfo, List<InventoryItem> materials)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("bugu005_9", (stack ? 1 : 0) != 0, (object) itemInfo, (object) materials);
  }

  public override IEnumerator onInitSceneAsync()
  {
    Bugu0059Scene bugu0059Scene = this;
    bugu0059Scene.menu = bugu0059Scene.menuBase as Bugu0059Menu;
    IEnumerator e = bugu0059Scene.menu.onInitAsync();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Future<GameObject> bgF = Res.Prefabs.BackGround.ComposeBackground.Load<GameObject>();
    e = bgF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    bugu0059Scene.backgroundPrefab = bgF.Result;
  }

  public IEnumerator onStartSceneAsync(GameCore.ItemInfo itemInfo, List<InventoryItem> materials)
  {
    Bugu0059Scene scene = this;
    WebAPI.Response.ItemGearDrillingConfirm response = (WebAPI.Response.ItemGearDrillingConfirm) null;
    if (scene.resetFlag)
    {
      PlayerItem playerItem = ((IEnumerable<PlayerItem>) SMManager.Get<PlayerItem[]>()).FirstOrDefault<PlayerItem>((Func<PlayerItem, bool>) (x => x.id == itemInfo.itemID));
      itemInfo = new GameCore.ItemInfo(playerItem);
      materials = (List<InventoryItem>) null;
      scene.resetFlag = false;
      if (playerItem.gear_level_limit >= playerItem.gear_level_limit_max && playerItem.gear_level_limit <= playerItem.gear_level)
      {
        scene.menu.callBackScene();
        yield break;
      }
    }
    IEnumerator e;
    if (materials != null && materials.Count > 0)
    {
      Future<WebAPI.Response.ItemGearDrillingConfirm> feature = WebAPI.ItemGearDrillingConfirm(itemInfo.itemID, materials.ToGearId().ToArray(), materials.ToMaterialId().ToArray(), (System.Action<WebAPI.Response.UserError>) null);
      e = feature.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      response = feature.Result;
      feature = (Future<WebAPI.Response.ItemGearDrillingConfirm>) null;
    }
    e = scene.menu.onStartAsync(itemInfo, materials, response, scene, response != null);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public void SetResetFlag()
  {
    this.resetFlag = true;
  }
}
