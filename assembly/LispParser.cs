﻿// Decompiled with JetBrains decompiler
// Type: LispParser
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore.LispCore;

public class LispParser : IScriptParser
{
  private SExpReader reader;

  public LispParser(SExpReader r)
  {
    this.reader = r;
  }

  public Cons parse(string text)
  {
    return this.reader.parse(text, true) as Cons;
  }
}
