﻿// Decompiled with JetBrains decompiler
// Type: SheetGachaStart
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

public class SheetGachaStart : MonoBehaviour
{
  public float delayTime;

  public IEnumerator Init(System.Action act)
  {
    yield return (object) new WaitForSeconds(this.delayTime);
    if (act != null)
      act();
  }
}
