﻿// Decompiled with JetBrains decompiler
// Type: Battle01712aMenu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using System.Collections;
using UnityEngine;

public class Battle01712aMenu : NGBattleMenuBase
{
  [SerializeField]
  private UILabel txt_name;
  [SerializeField]
  private UILabel txt_balloon;

  private void Awake()
  {
    foreach (Component componentsInChild in this.GetComponentsInChildren<Battle01712aCharaView>(true))
      componentsInChild.gameObject.SetActive(false);
  }

  private IEnumerator doSetSprite(UnitUnit unit, int job_id, int? metamorId)
  {
    Battle01712aMenu battle01712aMenu = this;
    Future<UnityEngine.Sprite> f = unit.LoadSpriteLarge(!metamorId.HasValue || metamorId.Value == 0 ? job_id : metamorId.Value, 1f);
    IEnumerator e = f.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    UnityEngine.Sprite result = f.Result;
    foreach (Battle01712aCharaView componentsInChild in battle01712aMenu.GetComponentsInChildren<Battle01712aCharaView>(true))
    {
      componentsInChild.setSprite(result);
      componentsInChild.gameObject.SetActive(true);
    }
  }

  public void setUnit(BL.Unit unit)
  {
    this.setText(this.txt_name, unit.unit.name);
    if ((Object) this.txt_balloon != (Object) null)
      this.setText(this.txt_balloon, unit.unit.unitVoicePattern.dead_message);
    this.StartCoroutine(this.doSetSprite(unit.unit, unit.playerUnit.job_id, unit.metamorphosis?.metamorphosis_id));
    NGSoundManager instance = Singleton<NGSoundManager>.GetInstance();
    if (!((Object) this.txt_balloon != (Object) null))
      return;
    instance.playSE("SE_2022", false, 0.0f, -1);
    instance.playVoiceByID(unit.unit.unitVoicePattern, 76, -1, 0.0f);
  }
}
