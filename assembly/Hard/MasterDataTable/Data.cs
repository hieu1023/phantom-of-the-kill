﻿// Decompiled with JetBrains decompiler
// Type: Hard.MasterDataTable.Data
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;

namespace Hard.MasterDataTable
{
  public static class Data
  {
    public static readonly UnitRegressionUnitType[] UnitRegressionUnitTypes = new UnitRegressionUnitType[4]
    {
      new UnitRegressionUnitType()
      {
        unit_id = 103013,
        unit_type = UnitTypeEnum.kouki,
        target_type = UnitTypeEnum.maki
      },
      new UnitRegressionUnitType()
      {
        unit_id = 502213,
        unit_type = UnitTypeEnum.maki,
        target_type = UnitTypeEnum.kouki
      },
      new UnitRegressionUnitType()
      {
        unit_id = 601513,
        unit_type = UnitTypeEnum.maki,
        target_type = UnitTypeEnum.kouki
      },
      new UnitRegressionUnitType()
      {
        unit_id = 302213,
        unit_type = UnitTypeEnum.kouki,
        target_type = UnitTypeEnum.maki
      }
    };
  }
}
