﻿// Decompiled with JetBrains decompiler
// Type: Hard.MasterDataTable.UnitRegressionUnitType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using SM;

namespace Hard.MasterDataTable
{
  public struct UnitRegressionUnitType
  {
    public int unit_id;
    public UnitTypeEnum unit_type;
    public UnitTypeEnum target_type;

    public bool IsMatch(PlayerUnit pu)
    {
      return this.unit_id == pu._unit && this.unit_type == (UnitTypeEnum) pu._unit_type;
    }
  }
}
