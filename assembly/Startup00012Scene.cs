﻿// Decompiled with JetBrains decompiler
// Type: Startup00012Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Startup00012Scene : NGSceneBase
{
  [SerializeField]
  private Startup00012Menu menu;

  public IEnumerator onStartSceneAsync(int info_id)
  {
    OfficialInformationArticle info = ((IEnumerable<OfficialInformationArticle>) Singleton<NGGameDataManager>.GetInstance().officialInfos).FirstOrDefault<OfficialInformationArticle>((Func<OfficialInformationArticle, bool>) (x => x.id == info_id));
    IEnumerator e;
    if (info == null)
    {
      e = this.onStartSceneAsync();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    else
    {
      e = this.menu.InitSceneAsync(info);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      e = this.menu.InitAsync();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      Singleton<CommonRoot>.GetInstance().isLoading = false;
    }
  }

  public IEnumerator onStartSceneAsync()
  {
    OfficialInformationArticle[] officialInfos = Singleton<NGGameDataManager>.GetInstance().officialInfos;
    int index = 0;
    if (index < officialInfos.Length)
    {
      IEnumerator e = this.onStartSceneAsync(officialInfos[index]);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
  }

  public IEnumerator onStartSceneAsync(OfficialInformationArticle info)
  {
    IEnumerator e = this.menu.InitSceneAsync(info);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    e = this.menu.InitAsync();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Singleton<CommonRoot>.GetInstance().isLoading = false;
  }

  public override void onEndScene()
  {
    DetailController.Release();
  }
}
