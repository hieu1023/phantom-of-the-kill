﻿// Decompiled with JetBrains decompiler
// Type: JobChangeData.DefValues
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace JobChangeData
{
  public static class DefValues
  {
    public static readonly int NUM_CHANGETYPE = Enum.GetNames(typeof (ChangeType)).Length;
    public static readonly int NUM_MATERIALSLOT = 5;
  }
}
