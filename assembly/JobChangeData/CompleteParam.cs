﻿// Decompiled with JetBrains decompiler
// Type: JobChangeData.CompleteParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;

namespace JobChangeData
{
  public class CompleteParam
  {
    public PlayerUnit before_ { get; private set; }

    public PlayerUnit after_ { get; private set; }

    public PlayerMaterialUnit[] materials_ { get; private set; }

    public CompleteParam(
      PlayerUnit beforeUnit,
      PlayerMaterialUnit[] materials,
      PlayerUnit afterUnit)
    {
      this.before_ = beforeUnit;
      this.after_ = afterUnit;
      this.materials_ = materials;
    }
  }
}
