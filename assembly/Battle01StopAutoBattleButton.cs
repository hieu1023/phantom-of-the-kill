﻿// Decompiled with JetBrains decompiler
// Type: Battle01StopAutoBattleButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;
using UnityEngine;

public class Battle01StopAutoBattleButton : NGBattleMenuBase
{
  private GameObject popupPrefab;

  protected override IEnumerator Start_Original()
  {
    Future<GameObject> f = Res.Prefabs.popup.popup_017_19_2__anim_popup01.Load<GameObject>();
    IEnumerator e = f.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.popupPrefab = f.Result;
  }

  public void onClick()
  {
    if (!this.battleManager.isBattleEnable || this.env.core.phaseState.state != BL.Phase.player && this.env.core.phaseState.state != BL.Phase.enemy && this.env.core.phaseState.state != BL.Phase.neutral)
      return;
    this.env.core.isAutoBattle.value = false;
  }
}
