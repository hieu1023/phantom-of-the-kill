﻿// Decompiled with JetBrains decompiler
// Type: Battle02StatusScrollParts
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections.Generic;
using UnityEngine;

public class Battle02StatusScrollParts : BattleMonoBehaviour
{
  private int forcsId = -1;
  [SerializeField]
  private SelectParts mColumnHeader;
  [SerializeField]
  private NGxScroll scrollParts;
  [SerializeField]
  public GameObject[] chilObject;

  public void initParts(GameObject prefab, int forcsId)
  {
    if (this.forcsId == forcsId)
      return;
    this.forcsId = forcsId;
    ((IEnumerable<GameObject>) this.chilObject).ForEach<GameObject>((System.Action<GameObject>) (o => o.SetActive(true)));
    List<BL.Unit> unitList;
    switch (forcsId)
    {
      case 0:
        unitList = this.env.core.playerUnits.value;
        break;
      case 1:
        unitList = this.env.core.enemyUnits.value;
        break;
      case 2:
        unitList = this.env.core.neutralUnits.value;
        break;
      default:
        return;
    }
    this.scrollParts.Clear();
    foreach (BL.Unit unit in unitList)
    {
      if (unit.playerUnit.spawn_turn <= this.env.core.phaseState.turnCount)
      {
        GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(prefab);
        Battle02MenuBase component = gameObject.GetComponent<Battle02MenuBase>();
        component.setUnit(unit);
        component.UpdateData();
        this.scrollParts.Add(gameObject, false);
      }
    }
    this.transform.localPosition = new Vector3(this.transform.localPosition.x + 100000f, this.transform.localPosition.y, this.transform.localPosition.z);
    this.chilObject[0].transform.localPosition = new Vector3(this.chilObject[0].transform.localPosition.x - 100000f, this.chilObject[0].transform.localPosition.y, this.chilObject[0].transform.localPosition.z);
    this.gameObject.SetActive(true);
    this.transform.parent.gameObject.SetActive(true);
    this.scrollParts.ResolvePosition();
    this.mColumnHeader.setValueNonTween(forcsId);
  }
}
