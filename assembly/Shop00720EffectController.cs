﻿// Decompiled with JetBrains decompiler
// Type: Shop00720EffectController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Shop00720EffectController : EffectController
{
  public string[] textureNameList_1;
  public string[] textureNameList_2;
  public string[] textureNameList_3;
  public int stopTextureId_1;
  public int stopTextureId_2;
  public int stopTextureId_3;
  public int[] transitionPlanList;
  public int rarity;
  public bool loadState;
  [SerializeField]
  private GameObject gb_started;
  private SlotDebug slot_script;
  [SerializeField]
  private Camera camera2D;
  private bool isCutInHiding;
  private Shop00720EffectController.CutinType cutinType;
  [SerializeField]
  public DuelCutin duelCutin;
  [SerializeField]
  private GameObject[] bugsCutinInitOff;
  [SerializeField]
  public DuelCutin duelCutinMale;
  [SerializeField]
  private GameObject[] bugsCutinMaleInitOff;
  [SerializeField]
  private GameObject duelCutinNewObj;
  private DuelCutin duelCutinNew;
  private GameObject AnimationUnitIconPrefab;
  private GameObject AnimationItemIconPrefab;
  [SerializeField]
  private GameObject new_eft_;
  [SerializeField]
  private List<GetSumContainerList> renpatu_obj_;
  [SerializeField]
  private List<AnimationUnitIcon> renpatu_unit_;
  [SerializeField]
  private List<AnimationItemIcon> renpatu_item_;

  public SlotDebug Slot_script
  {
    get
    {
      return this.slot_script;
    }
  }

  private void Awake()
  {
    this.slot_script = this.gb_started.GetComponent<SlotDebug>();
    this.StartCoroutine(this.slot_script.Init());
    this.SlotInit();
  }

  public void Bet()
  {
    Debug.Log((object) nameof (Bet));
    this.isCutInHiding = false;
    if (this.slot_script.isReady)
    {
      this.slot_script.SlotStart();
    }
    else
    {
      if (!this.slot_script.isEnd)
        return;
      this.SlotInit();
    }
  }

  public void SlotInit()
  {
    this.slot_script.SlotInit();
  }

  public void Skip()
  {
    Debug.Log((object) nameof (Skip));
    this.slot_script.Skip();
  }

  public IEnumerator Renpatu(
    WebAPI.Response.SlotS001MedalPayResult[] result_data)
  {
    Shop00720EffectController effectController = this;
    int num = result_data.Length - 1;
    foreach (GetSumContainerList sumContainerList in effectController.renpatu_obj_)
    {
      if ((UnityEngine.Object) sumContainerList != (UnityEngine.Object) null)
        sumContainerList.gameObject.SetActive(num == 0);
      --num;
    }
    GetSumContainerList renpatu_obj_last = effectController.renpatu_obj_[result_data.Length - 1];
    effectController.new_eft_.SetActive(false);
    Future<GameObject> fAnimationItemIcon;
    IEnumerator e;
    if ((UnityEngine.Object) effectController.AnimationItemIconPrefab == (UnityEngine.Object) null)
    {
      fAnimationItemIcon = Res.Prefabs.ArmorRepair.AnimationItemIcon.Load<GameObject>();
      e = fAnimationItemIcon.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      effectController.AnimationItemIconPrefab = fAnimationItemIcon.Result;
      fAnimationItemIcon = (Future<GameObject>) null;
    }
    if ((UnityEngine.Object) effectController.AnimationUnitIconPrefab == (UnityEngine.Object) null)
    {
      fAnimationItemIcon = Res.Prefabs.ArmorRepair.AnimationUnitIcon.Load<GameObject>();
      e = fAnimationItemIcon.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      effectController.AnimationUnitIconPrefab = fAnimationItemIcon.Result;
      fAnimationItemIcon = (Future<GameObject>) null;
    }
    int high_rarity = 1;
    for (int i = 0; i < result_data.Length; ++i)
    {
      WebAPI.Response.SlotS001MedalPayResult targetRusult = result_data[i];
      CommonRewardType crt = new CommonRewardType(targetRusult.reward_type_id, targetRusult.reward_result_id, targetRusult.reward_result_quantity, targetRusult.is_new, false);
      UnitUnit unit = crt.unitUnit;
      GearGear gear = crt.gearGear;
      SupplySupply supply = crt.supplySupply;
      renpatu_obj_last.renpatu_unit_light_[i].gameObject.SetActive(false);
      renpatu_obj_last.renpatu_unit_light2_[i].gameObject.SetActive(false);
      renpatu_obj_last.renpatu_item_light_[i].gameObject.SetActive(false);
      renpatu_obj_last.renpatu_item_light2_[i].gameObject.SetActive(false);
      renpatu_obj_last.renpatu_item_other_light_[i].gameObject.SetActive(false);
      renpatu_obj_last.renpatu_item_other_light2_[i].gameObject.SetActive(false);
      int rarityindex = ((IEnumerable<SlotS001MedalRarity>) MasterData.SlotS001MedalRarityList).SingleOrDefault<SlotS001MedalRarity>((Func<SlotS001MedalRarity, bool>) (sd => sd.ID == targetRusult.rarity_id)).index;
      if (high_rarity < rarityindex)
        high_rarity = rarityindex;
      renpatu_obj_last.renpatu_trans_[i].gameObject.SetActive(true);
      Shop00720EffectRarity component = renpatu_obj_last.renpatu_trans_[i].GetComponent<Shop00720EffectRarity>();
      component.Init();
      component.setRarity(rarityindex);
      if ((UnityEngine.Object) effectController.renpatu_unit_[i] != (UnityEngine.Object) null)
        effectController.renpatu_unit_[i].gameObject.SetActive(false);
      if ((UnityEngine.Object) effectController.renpatu_item_[i] != (UnityEngine.Object) null)
        effectController.renpatu_item_[i].gameObject.SetActive(false);
      renpatu_obj_last.renpatu_mesh_[i].gameObject.SetActive(true);
      if (unit != null)
      {
        e = effectController.SetTextureUnitThum(unit.ID, renpatu_obj_last.renpatu_mesh_[i]);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        e = effectController.SetTextureUnitThum(unit.ID, renpatu_obj_last.renpatu_mesh_blur_[i]);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        effectController.renpatu_unit_[i] = effectController.SetCloneUnitIcon(effectController.renpatu_unit_[i], renpatu_obj_last.renpatu_trans_[i], effectController.AnimationUnitIconPrefab, unit, targetRusult.is_new);
      }
      else if (gear != null)
      {
        e = effectController.SetTextureGearThum(gear.ID, renpatu_obj_last.renpatu_mesh_[i]);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        e = effectController.SetTextureGearThum(gear.ID, renpatu_obj_last.renpatu_mesh_blur_[i]);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        bool isWeaponMaterial = crt.type_ == 35;
        effectController.renpatu_item_[i] = effectController.SetCloneItemIcon(effectController.renpatu_item_[i], renpatu_obj_last.renpatu_trans_[i], effectController.AnimationItemIconPrefab, gear, isWeaponMaterial, targetRusult.is_new);
      }
      else if (supply != null)
      {
        e = effectController.SetTextureSupplyThum(supply.ID, renpatu_obj_last.renpatu_mesh_[i]);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        e = effectController.SetTextureSupplyThum(supply.ID, renpatu_obj_last.renpatu_mesh_blur_[i]);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        effectController.renpatu_item_[i] = effectController.SetCloneSupplyIcon(effectController.renpatu_item_[i], renpatu_obj_last.renpatu_trans_[i], effectController.AnimationItemIconPrefab);
        effectController.SetRealityItemThumnaile(rarityindex, result_data, i, supply);
      }
      else
      {
        e = effectController.SetTextureItemThum(crt, renpatu_obj_last.renpatu_mesh_[i], renpatu_obj_last.renpatu_mesh_[i].gameObject);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        e = effectController.SetTextureItemThum(crt, renpatu_obj_last.renpatu_mesh_blur_[i], (GameObject) null);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        effectController.renpatu_item_[i] = effectController.SetCloneSupplyIcon(effectController.renpatu_item_[i], renpatu_obj_last.renpatu_trans_[i], effectController.AnimationItemIconPrefab);
        effectController.SetRealityItemThumnaile(rarityindex, result_data, i, supply);
      }
      crt = (CommonRewardType) null;
      unit = (UnitUnit) null;
      gear = (GearGear) null;
      supply = (SupplySupply) null;
    }
    renpatu_obj_last.renpatu_trans_[0].GetComponent<Shop00720EffectRarity>().setSe(Shop00720EffectRarity.SeName(high_rarity));
  }

  private void SetRealityUnitThumnaile(
    int realityIndex,
    WebAPI.Response.SlotS001MedalPayResult[] result_data,
    int index)
  {
    GetSumContainerList sumContainerList = this.renpatu_obj_[result_data.Length - 1];
    switch (realityIndex)
    {
      case 0:
      case 1:
      case 2:
        sumContainerList.renpatu_mesh_blur_[index].enabled = false;
        break;
      case 4:
        sumContainerList.renpatu_unit_light_[index].gameObject.SetActive(true);
        break;
      case 5:
        sumContainerList.renpatu_unit_light2_[index].gameObject.SetActive(true);
        break;
    }
  }

  private void SetRealityItemThumnaile(
    int realityIndex,
    WebAPI.Response.SlotS001MedalPayResult[] result_data,
    int index,
    SupplySupply supply)
  {
    GetSumContainerList sumContainerList = this.renpatu_obj_[result_data.Length - 1];
    switch (realityIndex)
    {
      case 0:
      case 1:
      case 2:
        sumContainerList.renpatu_mesh_blur_[index].enabled = false;
        break;
      case 4:
        if (supply != null)
        {
          sumContainerList.renpatu_item_light_[index].gameObject.SetActive(true);
          break;
        }
        sumContainerList.renpatu_item_other_light_[index].gameObject.SetActive(true);
        break;
      case 5:
        if (supply != null)
        {
          sumContainerList.renpatu_item_light2_[index].gameObject.SetActive(true);
          break;
        }
        sumContainerList.renpatu_item_other_light2_[index].gameObject.SetActive(true);
        break;
    }
  }

  protected override AnimationUnitIcon SetCloneUnitIcon(
    AnimationUnitIcon icon,
    Transform trans,
    GameObject obj,
    UnitUnit unit,
    bool new_flag = false)
  {
    if ((UnityEngine.Object) icon != (UnityEngine.Object) null && (UnityEngine.Object) icon.gameObject.transform.parent != (UnityEngine.Object) trans.transform)
    {
      UnityEngine.Object.Destroy((UnityEngine.Object) icon.gameObject);
      icon = (AnimationUnitIcon) null;
    }
    if ((UnityEngine.Object) icon == (UnityEngine.Object) null)
      icon = obj.Clone(trans.transform).GetComponent<AnimationUnitIcon>();
    else
      icon.gameObject.SetActive(true);
    icon.transform.position = new Vector3(trans.transform.position.x, trans.transform.position.y, trans.transform.position.z);
    icon.transform.localRotation = Quaternion.Euler(0.0f, -180f, 0.0f);
    icon.gameObject.name = "AnimationUnitIcon";
    icon.Set(unit, new_flag);
    this.SetLayer(icon.gameObject.transform, trans.gameObject.layer);
    return icon;
  }

  private AnimationItemIcon SetCloneItemIcon(
    AnimationItemIcon icon,
    Transform trans,
    GameObject obj,
    GearGear item,
    bool isWeaponMaterial,
    bool new_flag = false)
  {
    if ((UnityEngine.Object) icon != (UnityEngine.Object) null && (UnityEngine.Object) icon.gameObject.transform.parent != (UnityEngine.Object) trans.transform)
    {
      UnityEngine.Object.Destroy((UnityEngine.Object) icon.gameObject);
      icon = (AnimationItemIcon) null;
    }
    if ((UnityEngine.Object) icon == (UnityEngine.Object) null)
      icon = obj.Clone(trans.transform).GetComponent<AnimationItemIcon>();
    else
      icon.gameObject.SetActive(true);
    icon.transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);
    icon.transform.localRotation = Quaternion.Euler(0.0f, -180f, 0.0f);
    icon.gameObject.name = "AnimationItemIcon";
    icon.Set(item, isWeaponMaterial, new_flag);
    this.SetLayer(icon.gameObject.transform, trans.gameObject.layer);
    return icon;
  }

  private AnimationItemIcon SetCloneSupplyIcon(
    AnimationItemIcon icon,
    Transform trans,
    GameObject obj)
  {
    if ((UnityEngine.Object) icon != (UnityEngine.Object) null && (UnityEngine.Object) icon.gameObject.transform.parent != (UnityEngine.Object) trans.transform)
    {
      UnityEngine.Object.Destroy((UnityEngine.Object) icon.gameObject);
      icon = (AnimationItemIcon) null;
    }
    if ((UnityEngine.Object) icon == (UnityEngine.Object) null)
      icon = obj.Clone(trans.transform).GetComponent<AnimationItemIcon>();
    else
      icon.gameObject.SetActive(true);
    icon.transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);
    icon.transform.localRotation = Quaternion.Euler(0.0f, -180f, 0.0f);
    icon.gameObject.name = nameof (SetCloneSupplyIcon);
    icon.SetSimpleMode();
    this.SetLayer(icon.gameObject.transform, trans.gameObject.layer);
    return icon;
  }

  private IEnumerator SetTextureItemThum(
    CommonRewardType crt,
    MeshRenderer mesh_renderer,
    GameObject rootObj = null)
  {
    Shop00720EffectController effectController = this;
    Future<UnityEngine.Sprite> sprite;
    switch (crt.type_)
    {
      case 4:
        sprite = Res.Icons.Item_Icon_Zeny.Load<UnityEngine.Sprite>();
        break;
      case 10:
        sprite = Res.Icons.Item_Icon_Kiseki.Load<UnityEngine.Sprite>();
        break;
      case 14:
        sprite = Res.Icons.Item_Icon_Medal.Load<UnityEngine.Sprite>();
        break;
      case 15:
        sprite = Res.Icons.Item_Icon_Point.Load<UnityEngine.Sprite>();
        break;
      case 17:
        sprite = Res.Icons.Item_Icon_BattleMedal.Load<UnityEngine.Sprite>();
        break;
      case 19:
        sprite = MasterData.QuestkeyQuestkey[crt.id_].LoadSpriteThumbnail();
        break;
      case 20:
        sprite = MasterData.GachaTicket[crt.id_].LoadSpriteF();
        break;
      case 21:
        sprite = MasterData.SeasonTicketSeasonTicket[crt.id_].LoadThumneilF();
        break;
      case 25:
        sprite = Res.Icons.Item_Icon_Common.Load<UnityEngine.Sprite>();
        break;
      case 29:
        sprite = MasterData.BattleskillSkill[crt.id_].LoadBattleSkillIcon((BattleFuncs.InvestSkill) null);
        break;
      default:
        if (!((UnityEngine.Object) rootObj != (UnityEngine.Object) null))
        {
          yield break;
        }
        else
        {
          rootObj.SetActive(true);
          yield break;
        }
    }
    IEnumerator e = effectController.SetTexture(sprite, mesh_renderer);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  private IEnumerator CutInInitializeOld(PlayerUnit unit)
  {
    IEnumerator e;
    if (unit.unit.character.gender == UnitGender.male)
    {
      this.cutinType = Shop00720EffectController.CutinType.Male;
      e = this.duelCutinMale.Initialize<PlayerUnit>(unit);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    else
    {
      this.cutinType = Shop00720EffectController.CutinType.Female;
      e = this.duelCutin.Initialize<PlayerUnit>(unit);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
  }

  public IEnumerator CutInInitialize(PlayerUnit unit)
  {
    this.duelCutin.gameObject.SetActive(false);
    this.duelCutinMale.gameObject.SetActive(false);
    this.duelCutinNewObj.SetActive(false);
    IEnumerator e;
    if (((IEnumerable<UnitGroup>) MasterData.UnitGroupList).FirstOrDefault<UnitGroup>((Func<UnitGroup, bool>) (x => x.unit_id == unit.unit.ID)) != null)
    {
      this.cutinType = Shop00720EffectController.CutinType.New;
      if ((UnityEngine.Object) this.duelCutinNew == (UnityEngine.Object) null)
      {
        Future<GameObject> fs = (Future<GameObject>) null;
        UnitCutinInfo cutinInfo = unit.CutinInfo;
        if (!string.IsNullOrEmpty(cutinInfo.prefab_name))
        {
          fs = new ResourceObject("Animations/battle_cutin/" + cutinInfo.prefab_name).Load<GameObject>();
          if (fs != null)
          {
            e = fs.Wait();
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            this.duelCutinNew = fs.Result.Clone(this.duelCutinNewObj.transform).GetComponent<DuelCutin>();
            if ((UnityEngine.Object) this.duelCutinNew != (UnityEngine.Object) null)
            {
              e = this.duelCutinNew.Initialize<PlayerUnit>(unit);
              while (e.MoveNext())
                yield return e.Current;
              e = (IEnumerator) null;
            }
            else
            {
              e = this.CutInInitializeOld(unit);
              while (e.MoveNext())
                yield return e.Current;
              e = (IEnumerator) null;
            }
          }
          else
          {
            e = this.CutInInitializeOld(unit);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
          }
        }
        else
        {
          e = this.CutInInitializeOld(unit);
          while (e.MoveNext())
            yield return e.Current;
          e = (IEnumerator) null;
        }
        fs = (Future<GameObject>) null;
      }
    }
    else
    {
      e = this.CutInInitializeOld(unit);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
  }

  public void PlayCutin()
  {
    switch (this.cutinType)
    {
      case Shop00720EffectController.CutinType.Male:
        foreach (GameObject gameObject in this.bugsCutinMaleInitOff)
          gameObject.SetActive(false);
        this.duelCutinMale.gameObject.SetActive(true);
        this.duelCutinMale.PlaySkillCutin(DuelCutin.PlayMode.FIRST_PERSON);
        break;
      case Shop00720EffectController.CutinType.Female:
        foreach (GameObject gameObject in this.bugsCutinInitOff)
          gameObject.SetActive(false);
        this.duelCutin.gameObject.SetActive(true);
        this.duelCutin.PlaySkillCutin(DuelCutin.PlayMode.FIRST_PERSON);
        break;
      case Shop00720EffectController.CutinType.New:
        if (this.isCutInHiding)
          break;
        this.camera2D.gameObject.SetActive(false);
        this.duelCutinNewObj.SetActive(true);
        this.duelCutinNew.gameObject.SetActive(true);
        this.duelCutinNew.PlaySkillCutin(DuelCutin.PlayMode.FIRST_PERSON);
        this.duelCutinNew.CameraCutin.transform.localPosition = new Vector3(0.0f, 0.0f, 10f);
        break;
    }
  }

  public void HideCutin()
  {
    switch (this.cutinType)
    {
      case Shop00720EffectController.CutinType.Male:
        this.duelCutinMale.gameObject.SetActive(false);
        this.duelCutinMale.gameObject.GetComponent<Animator>().ResetTrigger("Start");
        break;
      case Shop00720EffectController.CutinType.Female:
        this.duelCutin.gameObject.SetActive(false);
        this.duelCutin.gameObject.GetComponent<Animator>().ResetTrigger("Start");
        break;
      case Shop00720EffectController.CutinType.New:
        this.isCutInHiding = true;
        this.duelCutinNewObj.SetActive(false);
        this.duelCutinNew.gameObject.SetActive(false);
        this.duelCutinNew.gameObject.GetComponent<Animator>().ResetTrigger("Start");
        this.camera2D.gameObject.SetActive(true);
        break;
    }
  }

  private enum CutinType
  {
    Male,
    Female,
    New,
  }
}
