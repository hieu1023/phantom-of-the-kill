﻿// Decompiled with JetBrains decompiler
// Type: Help0154Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using DeviceKit;
using GameCore;
using SM;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Help0154Menu : BackButtonMenuBase
{
  [SerializeField]
  protected UILabel TxtPopuptitle;
  [SerializeField]
  protected UILabel TxtPopuptitle2;
  [SerializeField]
  protected UILabel TxtREADME;
  [SerializeField]
  private NGxScroll text_scroll;
  [SerializeField]
  protected UILabel m_supportMailAddress;
  private string player_id;
  private string short_id;

  public virtual void Foreground()
  {
  }

  public virtual void VScrollBar()
  {
  }

  public void InitContact(Player player)
  {
    this.player_id = player.id;
    this.short_id = player.short_id;
    this.text_scroll.ResolvePosition();
    this.text_scroll.ResolvePosition();
  }

  public void LaunchMailer()
  {
    App.LaunchMailer(Consts.GetInstance().HELP_CONTACT_ADDRESS, Consts.Format(Consts.GetInstance().HELP_CONTACT_TITLE, (IDictionary) new Dictionary<string, string>()
    {
      {
        "short_id",
        this.short_id
      }
    }), Consts.Format(Consts.GetInstance().HELP_CONTACT_MAIL_BODY, (IDictionary) new Dictionary<string, string>()
    {
      {
        "ver",
        Revision.DLCVersion
      },
      {
        "player_id",
        this.player_id
      },
      {
        "short_id",
        this.short_id
      },
      {
        "agent",
        string.Format("{0}/{1}/{2}/{3}/{4}", (object) Gsc.Device.DeviceInfo.DeviceModel, (object) Gsc.Device.DeviceInfo.DeviceVendor, (object) Gsc.Device.DeviceInfo.OperatingSystem, (object) Gsc.Device.DeviceInfo.ProcessorType, (object) Gsc.Device.DeviceInfo.SystemMemorySize)
      }
    }).Replace("\n", "%0A"));
  }

  public virtual void IbtnPopupClose()
  {
    if (this.IsPushAndSet())
      return;
    this.backScene();
  }

  public void IbtnBack()
  {
    if (this.IsPushAndSet())
      return;
    this.backScene();
  }

  public override void onBackButton()
  {
    this.IbtnBack();
  }
}
