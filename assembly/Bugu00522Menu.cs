﻿// Decompiled with JetBrains decompiler
// Type: Bugu00522Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using SM;
using System;
using System.Collections.Generic;
using UniLinq;

public class Bugu00522Menu : Bugu005SelectItemListMenuBase
{
  public override Persist<Persist.ItemSortAndFilterInfo> GetPersist()
  {
    return Persist.bugu0052BuildupBaseSortAndFilter;
  }

  protected override List<PlayerItem> GetItemList()
  {
    return ((IEnumerable<PlayerItem>) SMManager.Get<PlayerItem[]>()).Where<PlayerItem>((Func<PlayerItem, bool>) (x => x.gear != null && !x.isExchangable() && x.gear.kind.Enum != GearKindEnum.accessories)).ToList<PlayerItem>();
  }

  protected override void SelectItemProc(GameCore.ItemInfo item)
  {
    Bugu0058Scene.changeScene(true, item);
  }
}
