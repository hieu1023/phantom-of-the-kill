﻿// Decompiled with JetBrains decompiler
// Type: Quest00217Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[AddComponentMenu("Scenes/QuestExtra/TopMenu")]
public class Quest00217Menu : BackButtonMenuBase
{
  private static readonly TimeSpan biginnerSpn = new TimeSpan(168, 0, 0);
  private static int beforeCategoryId = -1;
  private Dictionary<int, Quest00217TabPage> categories = new Dictionary<int, Quest00217TabPage>();
  private Dictionary<int, Description> dicHuntingDescriptions_ = new Dictionary<int, Description>();
  private Dictionary<int, QuestScoreCampaignProgress> dicScoreCampaigns_ = new Dictionary<int, QuestScoreCampaignProgress>();
  [SerializeField]
  private Quest00217TabPage[] tabPages = new Quest00217TabPage[4];
  private List<Coroutine> loadingCoroutineList = new List<Coroutine>();
  private const int CATEGORY_METALKEY = 1;
  private const int CATEGORY_LIMITED = 2;
  private const int CATEGORY_BEGINNER = 4;
  [SerializeField]
  protected UILabel TxtTitle;
  public UIScrollView[] scrollviews;
  public GameObject topDragScroll_;
  private DateTime serverTime;
  private PlayerQuestGate[] keyQuestsGate;
  private List<UIDragScrollView> pauseDragScrollViews_;
  private PlayerExtraQuestS[] extraData;
  private DateTime playerCreatedAt;
  private bool isPageLoading;
  private const int maxDefaultSetupBanner = 4;
  private int setupBannerCount;

  private void OnDestroy()
  {
    NGSceneManager instance = Singleton<NGSceneManager>.GetInstance();
    if ((UnityEngine.Object) instance != (UnityEngine.Object) null)
    {
      foreach (Coroutine loadingCoroutine in this.loadingCoroutineList)
      {
        if (loadingCoroutine != null)
          instance.StopCoroutine(loadingCoroutine);
      }
    }
    this.loadingCoroutineList.Clear();
  }

  public IEnumerator Init(
    PlayerExtraQuestS[] ExtraData,
    int[] Categories,
    int[] Emphasis,
    QuestExtraTimetableNotice[] Notices,
    DateTime player_created_at,
    int activeTabIndex)
  {
    this.extraData = ((IEnumerable<PlayerExtraQuestS>) ExtraData).CheckMasterData().ToArray<PlayerExtraQuestS>();
    this.playerCreatedAt = player_created_at;
    this.TxtTitle.SetText(this.GetTitle());
    this.categories.Clear();
    this.setupBannerCount = 0;
    GameObject prefab = (GameObject) null;
    GameObject prefabHunting = (GameObject) null;
    GameObject prefabTower = (GameObject) null;
    PlayerExtraQuestS[] extraQuests = ((IEnumerable<PlayerExtraQuestS>) this.extraData).Where<PlayerExtraQuestS>((Func<PlayerExtraQuestS, bool>) (x => x.extra_quest_area == 1 || x.extra_quest_area == 3)).Root(true);
    this.keyQuestsGate = SMManager.Get<PlayerQuestGate[]>();
    bool isExistKeyQuests = ((IEnumerable<PlayerQuestGate>) this.keyQuestsGate).SelectMany<PlayerQuestGate, int>((Func<PlayerQuestGate, IEnumerable<int>>) (x => (IEnumerable<int>) x.quest_ids)).Intersect<int>(((IEnumerable<PlayerExtraQuestS>) this.extraData).Select<PlayerExtraQuestS, int>((Func<PlayerExtraQuestS, int>) (x => x._quest_extra_s))).Any<int>();
    this.createScrollItemsCategory(this.extraData, Categories);
    IEnumerator e = ServerTime.WaitSync();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.serverTime = ServerTime.NowAppTimeAddDelta();
    SM.TowerPeriod[] towerPeriod = ((IEnumerable<SM.TowerPeriod>) SMManager.Get<SM.TowerPeriod[]>()).OrderBy<SM.TowerPeriod, int>((Func<SM.TowerPeriod, int>) (x => x.priority)).ToArray<SM.TowerPeriod>();
    EventInfo[] eventInfo = ((IEnumerable<EventInfo>) SMManager.Get<EventInfo[]>()).OrderBy<EventInfo, int>((Func<EventInfo, int>) (x => x.Period() != null ? x.Period().priority : int.MaxValue)).ToArray<EventInfo>();
    int questTotal = extraQuests.Length + (eventInfo != null ? eventInfo.Length : 0) + (towerPeriod != null ? towerPeriod.Length : 0);
    int towerCnt = 0;
    int extraQuestCnt = 0;
    int eventQuestCnt = 0;
    List<Quest00217Menu.BannerClass> bannerList = new List<Quest00217Menu.BannerClass>();
    if (isExistKeyQuests)
    {
      e = this.Create_Transitionbutton("metalkey", 1);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    HashSet<int> emphasisHashlist = new HashSet<int>((IEnumerable<int>) Emphasis);
    for (int i = 0; i < questTotal; ++i)
    {
      PlayerExtraQuestS extraQ = extraQuests.Length > extraQuestCnt ? extraQuests[extraQuestCnt] : (PlayerExtraQuestS) null;
      EventInfo eventQ = eventInfo == null || eventInfo.Length <= eventQuestCnt ? (EventInfo) null : eventInfo[eventQuestCnt];
      SM.TowerPeriod towerQ = towerPeriod == null || towerPeriod.Length <= towerCnt ? (SM.TowerPeriod) null : towerPeriod[towerCnt];
      bannerList.Clear();
      if (extraQ != null && this.isQuestConditionEffective(extraQ.quest_extra_s.quest_m))
        bannerList.Add(new Quest00217Menu.BannerClass(extraQ.quest_extra_s.quest_m.priority, Quest00217Menu.BannerType.Extra));
      if (eventQ != null)
        bannerList.Add(new Quest00217Menu.BannerClass(eventQ.Period() != null ? eventQ.Period().priority : int.MaxValue, Quest00217Menu.BannerType.Event));
      if (towerQ != null)
        bannerList.Add(new Quest00217Menu.BannerClass(towerQ.priority, Quest00217Menu.BannerType.Tower));
      Quest00217Menu.BannerClass bannerClass = bannerList.OrderBy<Quest00217Menu.BannerClass, int>((Func<Quest00217Menu.BannerClass, int>) (x => x.priority)).ThenBy<Quest00217Menu.BannerClass, Quest00217Menu.BannerType>((Func<Quest00217Menu.BannerClass, Quest00217Menu.BannerType>) (x => x.type)).FirstOrDefault<Quest00217Menu.BannerClass>();
      Future<GameObject> ScrollPrefab;
      if (bannerClass != null)
      {
        switch (bannerClass.type)
        {
          case Quest00217Menu.BannerType.Extra:
            if ((UnityEngine.Object) prefab == (UnityEngine.Object) null)
            {
              ScrollPrefab = Res.Prefabs.quest002_17.scroll.Load<GameObject>();
              e = ScrollPrefab.Wait();
              while (e.MoveNext())
                yield return e.Current;
              e = (IEnumerator) null;
              prefab = ScrollPrefab.Result;
              ScrollPrefab = (Future<GameObject>) null;
            }
            e = this.InitLoopScrolls(extraQ, this.extraData, prefab, emphasisHashlist, Notices);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            ++extraQuestCnt;
            break;
          case Quest00217Menu.BannerType.Event:
            if ((UnityEngine.Object) prefabHunting == (UnityEngine.Object) null)
            {
              ScrollPrefab = Res.Prefabs.quest002_17.scroll_hunting.Load<GameObject>();
              e = ScrollPrefab.Wait();
              while (e.MoveNext())
                yield return e.Current;
              e = (IEnumerator) null;
              prefabHunting = ScrollPrefab.Result;
              ScrollPrefab = (Future<GameObject>) null;
            }
            e = this.CreatePunitiveExpeditionEventButton(eventQ, prefabHunting);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            ++eventQuestCnt;
            break;
          case Quest00217Menu.BannerType.Tower:
            if ((UnityEngine.Object) prefabTower == (UnityEngine.Object) null)
            {
              ScrollPrefab = new ResourceObject("Prefabs/quest002_17/scroll_tower").Load<GameObject>();
              e = ScrollPrefab.Wait();
              while (e.MoveNext())
                yield return e.Current;
              e = (IEnumerator) null;
              prefabTower = ScrollPrefab.Result;
              ScrollPrefab = (Future<GameObject>) null;
            }
            e = this.CreateTowerButton(towerQ, prefabTower);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            ++towerCnt;
            break;
        }
      }
      else
      {
        ++extraQuestCnt;
        ++eventQuestCnt;
        ++towerCnt;
      }
      extraQ = (PlayerExtraQuestS) null;
      eventQ = (EventInfo) null;
      towerQ = (SM.TowerPeriod) null;
    }
    int categoryId = activeTabIndex;
    if (activeTabIndex <= 0)
      categoryId = !this.isOpenedAnyKeyQuest(this.extraData) ? (!(this.serverTime - this.playerCreatedAt < Quest00217Menu.biginnerSpn) ? (Quest00217Menu.beforeCategoryId <= 0 ? 2 : Quest00217Menu.beforeCategoryId) : 4) : 1;
    yield return (object) this.SetupCategory(categoryId);
    yield return (object) new WaitForEndOfFrame();
  }

  private bool isOpenedAnyKeyQuest(PlayerExtraQuestS[] ExtraData)
  {
    Func<PlayerExtraQuestS[], int, int?> GetLid = (Func<PlayerExtraQuestS[], int, int?>) ((ex, id_s) => Array.Find<PlayerExtraQuestS>(ex, (Predicate<PlayerExtraQuestS>) (fd => fd._quest_extra_s == id_s))?.quest_extra_s.quest_l_QuestExtraL);
    return ((IEnumerable<PlayerQuestGate>) this.keyQuestsGate).Where<PlayerQuestGate>((Func<PlayerQuestGate, bool>) (x => ((IEnumerable<int>) x.quest_ids).Any<int>((Func<int, bool>) (y => ((IEnumerable<PlayerExtraQuestS>) ExtraData).Any<PlayerExtraQuestS>((Func<PlayerExtraQuestS, bool>) (z => z._quest_extra_s == y)))))).Where<PlayerQuestGate>((Func<PlayerQuestGate, bool>) (w =>
    {
      IEnumerable<int> idGateS = ((IEnumerable<PlayerQuestGate>) this.keyQuestsGate).SelectMany<PlayerQuestGate, int>((Func<PlayerQuestGate, IEnumerable<int>>) (s => (IEnumerable<int>) s.quest_ids));
      int? nullable = GetLid(ExtraData, w.quest_ids[0]);
      return nullable.HasValue && !((IEnumerable<PlayerExtraQuestS>) ((IEnumerable<PlayerExtraQuestS>) ExtraData).M(nullable.Value, true)).Where<PlayerExtraQuestS>((Func<PlayerExtraQuestS, bool>) (extra_s => !idGateS.Contains<int>(extra_s._quest_extra_s))).Any<PlayerExtraQuestS>();
    })).Any<PlayerQuestGate>((Func<PlayerQuestGate, bool>) (quest => quest.in_progress));
  }

  private bool isQuestConditionEffective(QuestExtraM m)
  {
    QuestExtraReleaseConditionsPlayer conditionsPlayer = ((IEnumerable<QuestExtraReleaseConditionsPlayer>) MasterData.QuestExtraReleaseConditionsPlayerList).FirstOrDefault<QuestExtraReleaseConditionsPlayer>((Func<QuestExtraReleaseConditionsPlayer, bool>) (x => x.quest_m == m));
    if (conditionsPlayer == null)
      return true;
    int level = SMManager.Observe<Player>().Value.level;
    string comparisonOperator = conditionsPlayer.comparison_operator;
    if (!(comparisonOperator == "<="))
    {
      if (comparisonOperator == ">=")
        return true;
    }
    else
    {
      int num = level;
      int? playerLevel = conditionsPlayer.player_level;
      int valueOrDefault = playerLevel.GetValueOrDefault();
      if (num <= valueOrDefault & playerLevel.HasValue)
        return true;
    }
    return false;
  }

  private void createScrollItemsCategory(PlayerExtraQuestS[] extraData, int[] sortedCategory)
  {
    foreach (QuestExtraCategory questExtraCategory in ((IEnumerable<int>) sortedCategory).Select<int, QuestExtraCategory>((Func<int, QuestExtraCategory>) (i => this.getQuestCategory(i))).ToList<QuestExtraCategory>())
    {
      Quest00217TabPage quest00217TabPage = questExtraCategory.ID <= this.tabPages.Length ? this.tabPages[questExtraCategory.ID - 1] : this.tabPages[1];
      quest00217TabPage.Init(this, questExtraCategory.ID);
      this.categories.Add(questExtraCategory.ID, quest00217TabPage);
    }
  }

  private QuestExtraCategory getQuestCategory(int id)
  {
    QuestExtraCategory questExtraCategory = (QuestExtraCategory) null;
    if (MasterData.QuestExtraCategory.TryGetValue(id, out questExtraCategory))
      return questExtraCategory;
    return new QuestExtraCategory()
    {
      ID = id,
      name = string.Format("Category:{0}", (object) id)
    };
  }

  private IEnumerator InitLoopScrolls(
    PlayerExtraQuestS extraData,
    PlayerExtraQuestS[] ExtraData,
    GameObject prefab,
    HashSet<int> Emphasis,
    QuestExtraTimetableNotice[] Notices)
  {
    List<QuestExtraTimetableNotice> list = ((IEnumerable<QuestExtraTimetableNotice>) Notices).ToList<QuestExtraTimetableNotice>();
    QuestExtraS questExtraS = extraData.quest_extra_s;
    Quest00217Scroll.Parameter parameter = new Quest00217Scroll.Parameter();
    parameter.isNew = true;
    QuestExtraLL questLL;
    if ((questLL = extraData.quest_ll) != null)
    {
      parameter.seek = QuestExtra.SeekType.LL;
      if (questLL.description.HasValue)
        parameter.descriptions = ((IEnumerable<QuestExtraDescription>) MasterData.QuestExtraDescriptionList).Where<QuestExtraDescription>((Func<QuestExtraDescription, bool>) (qd => qd.descriptionID == questLL.description.Value)).ToArray<QuestExtraDescription>();
      QuestExtra.getStatusLL(questLL, ExtraData, Emphasis, out parameter.isNew, out parameter.isClear, out parameter.isHighlighting);
    }
    else if (extraData.seek_type == PlayerExtraQuestS.SeekType.L)
    {
      QuestExtraL questL = questExtraS.quest_l;
      if (questL.description.HasValue)
        parameter.descriptions = ((IEnumerable<QuestExtraDescription>) MasterData.QuestExtraDescriptionList).Where<QuestExtraDescription>((Func<QuestExtraDescription, bool>) (qd => qd.descriptionID == questL.description.Value)).ToArray<QuestExtraDescription>();
      QuestExtra.getStatusL(questL.ID, ExtraData, Emphasis, out parameter.isNew, out parameter.isClear, out parameter.isHighlighting);
    }
    else
    {
      QuestExtraM questM = questExtraS.quest_m;
      if (questM.description.HasValue)
        parameter.descriptions = ((IEnumerable<QuestExtraDescription>) MasterData.QuestExtraDescriptionList).Where<QuestExtraDescription>((Func<QuestExtraDescription, bool>) (qd => qd.descriptionID == questM.description.Value)).ToArray<QuestExtraDescription>();
      QuestExtra.getStatusM(questM.ID, ExtraData, Emphasis, out parameter.isNew, out parameter.isClear, out parameter.isHighlighting);
    }
    Predicate<QuestExtraTimetableNotice> match = (Predicate<QuestExtraTimetableNotice>) (n => n._quest_extra_s == extraData._quest_extra_s);
    QuestExtraTimetableNotice extraTimetableNotice = list.Find(match);
    if (extraTimetableNotice != null && extraTimetableNotice.start_at.HasValue)
    {
      parameter.isNotice = true;
      parameter.startTime = extraTimetableNotice.start_at;
    }
    parameter.extra = extraData;
    int id = parameter.extra.top_category.ID;
    if (PerformanceConfig.GetInstance().IsTuningEventTopSetting)
    {
      this.categories[id].AddRequest(parameter, prefab);
    }
    else
    {
      IEnumerator e = this.ScrollInit(this.categories[id], parameter, prefab);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
  }

  public IEnumerator UpdateTime()
  {
    IEnumerator e = ServerTime.WaitSync();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.serverTime = ServerTime.NowAppTimeAddDelta();
    foreach (Quest00217TabPage tabPage in this.tabPages)
      tabPage.UpdateTime(this.serverTime);
  }

  private void checkChangeDescriptionScoreCampaing(Quest00217Scroll cntl, int id)
  {
    QuestScoreCampaignProgress qscp;
    if (this.dicScoreCampaigns_.TryGetValue(id, out qscp))
    {
      if (this.IsPushAndSet())
        return;
      this.pauseDragScrollView();
      Quest00228Scene.ChangeScene(qscp, true);
    }
    else
      cntl.setEffectNoDescription();
  }

  private bool checkHasDescriptionScoreCampaing(int Lid)
  {
    if (!this.dicScoreCampaigns_.Any<KeyValuePair<int, QuestScoreCampaignProgress>>())
      this.dicScoreCampaigns_ = ((IEnumerable<QuestScoreCampaignProgress>) SMManager.Get<QuestScoreCampaignProgress[]>()).Distinct<QuestScoreCampaignProgress>((IEqualityComparer<QuestScoreCampaignProgress>) new LambdaEqualityComparer<QuestScoreCampaignProgress>((Func<QuestScoreCampaignProgress, QuestScoreCampaignProgress, bool>) ((a, b) => a.quest_extra_l == b.quest_extra_l))).ToDictionary<QuestScoreCampaignProgress, int>((Func<QuestScoreCampaignProgress, int>) (x => x.quest_extra_l));
    return this.dicScoreCampaigns_.ContainsKey(Lid);
  }

  public virtual void Foreground()
  {
  }

  public virtual void IbtnBack()
  {
    if (this.IsPushAndSet())
      return;
    this.backScene();
  }

  public override void onBackButton()
  {
    this.IbtnBack();
  }

  public virtual void IbtnEvent()
  {
  }

  public virtual void VScrollBar()
  {
  }

  private IEnumerator Create_Transitionbutton(string name, int categoryId)
  {
    string path = string.Format("Prefabs/quest002_17/{0}", (object) name);
    if (!Singleton<ResourceManager>.GetInstance().Contains(path))
    {
      Debug.LogWarning((object) "don't exit path");
    }
    else
    {
      Future<GameObject> prefabF = Singleton<ResourceManager>.GetInstance().Load<GameObject>(path, 1f);
      IEnumerator e = prefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      if (PerformanceConfig.GetInstance().IsTuningEventTopSetting)
      {
        this.categories[categoryId].AddRequest(prefabF.Result);
      }
      else
      {
        GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(prefabF.Result);
        this.categories[categoryId].AddItem(gameObject);
      }
    }
  }

  private IEnumerator CreatePunitiveExpeditionEventButton(
    EventInfo info,
    GameObject prefab)
  {
    int categoryId = info.category_id;
    if (PerformanceConfig.GetInstance().IsTuningEventTopSetting)
    {
      this.categories[categoryId].AddRequest(info, prefab);
    }
    else
    {
      IEnumerator e = this.coInitScrcollHunting(this.categories[categoryId], info, prefab);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
  }

  private IEnumerator coChangeHuntinDescription(
    Quest00217ScrollHunting cntl,
    EventInfo info)
  {
    Quest00217Menu quest00217Menu = this;
    quest00217Menu.pauseDragScrollView();
    Description description;
    if (!quest00217Menu.dicHuntingDescriptions_.TryGetValue(info.period_id, out description))
    {
      Future<WebAPI.Response.EventTop> request = WebAPI.EventTop(info.period_id, (System.Action<WebAPI.Response.UserError>) (e =>
      {
        WebAPI.DefaultUserErrorCallback(e);
        Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
      }));
      IEnumerator e1 = request.Wait();
      while (e1.MoveNext())
        yield return e1.Current;
      e1 = (IEnumerator) null;
      WebAPI.Response.EventTop result = request.Result;
      if (result == null)
      {
        yield break;
      }
      else
      {
        description = result.description;
        quest00217Menu.dicHuntingDescriptions_.Add(info.period_id, description);
        request = (Future<WebAPI.Response.EventTop>) null;
      }
    }
    if (description == null)
    {
      cntl.setEffectNoDescription();
      quest00217Menu.IsPush = false;
    }
    else
      Quest00228Scene.ChangeScene(description, true);
  }

  private IEnumerator CreateTowerButton(SM.TowerPeriod info, GameObject prefab)
  {
    int categoryId = info.category_id;
    if (PerformanceConfig.GetInstance().IsTuningEventTopSetting)
    {
      this.categories[categoryId].AddRequest(info, prefab);
    }
    else
    {
      IEnumerator e = this.coInitScrollTower(this.categories[categoryId], info, prefab);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
  }

  private void OnDisable()
  {
    this.resumeDragScrollView();
  }

  private string GetTitle()
  {
    return Consts.GetInstance().QUEST_00217_NORMAL_TITLE;
  }

  private void pauseDragScrollView()
  {
    this.resumeDragScrollView();
    this.pauseDragScrollViews_ = new List<UIDragScrollView>();
    foreach (UIDragScrollView componentsInChild in this.topDragScroll_.GetComponentsInChildren<UIDragScrollView>())
    {
      if (componentsInChild.enabled)
      {
        this.pauseDragScrollViews_.Add(componentsInChild);
        componentsInChild.enabled = false;
      }
    }
    foreach (UIScrollView scrollview in this.scrollviews)
      scrollview.Press(false);
  }

  private void resumeDragScrollView()
  {
    if (this.pauseDragScrollViews_ == null || !this.pauseDragScrollViews_.Any<UIDragScrollView>())
      return;
    foreach (Behaviour pauseDragScrollView in this.pauseDragScrollViews_)
      pauseDragScrollView.enabled = true;
    this.pauseDragScrollViews_.Clear();
  }

  public void OnPushPageTab(int index)
  {
    this.setPageTabActive(index);
  }

  private void setPageTabActive(int index)
  {
    if (PerformanceConfig.GetInstance().IsTuningEventTopSetting)
    {
      if (this.isPageLoading)
        return;
      this.isPageLoading = true;
      this.loadingCoroutineList.Add(this.StartCoroutine(this.SetupCategory(index)));
    }
    else
    {
      foreach (KeyValuePair<int, Quest00217TabPage> category in this.categories)
      {
        if (category.Key == index)
          category.Value.SetPageActive(true);
        else
          category.Value.SetPageActive(false);
      }
    }
  }

  private IEnumerator SetupCategory(int categoryId)
  {
    foreach (KeyValuePair<int, Quest00217TabPage> category in this.categories)
      category.Value.SetPageActive(false);
    Quest00217TabPage page = this.categories[categoryId];
    page.SetButtonActive(true);
    yield return (object) null;
    this.setupBannerCount = 0;
    foreach (Quest00217TabPage.RequestParam request in page.RequestList)
    {
      IEnumerator e = this.SetupBanner(page, request);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      ++this.setupBannerCount;
    }
    page.RequestList.Clear();
    page.SetPageActive(true);
    Quest00217Menu.beforeCategoryId = categoryId;
    this.isPageLoading = false;
  }

  private IEnumerator SetupBanner(
    Quest00217TabPage page,
    Quest00217TabPage.RequestParam request)
  {
    IEnumerator e;
    if (request.ScrollEventParam != null)
    {
      e = this.ScrollInit(page, request.ScrollEventParam, request.Prefab);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    else if (request.ScrollHuntingParam != null)
    {
      e = this.coInitScrcollHunting(page, request.ScrollHuntingParam, request.Prefab);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    else if (request.ScrollTowerParam != null)
    {
      e = this.coInitScrollTower(page, request.ScrollTowerParam, request.Prefab);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    else if ((UnityEngine.Object) request.Prefab != (UnityEngine.Object) null)
      yield return (object) this.coInitScrollDirect(page, request.Prefab);
  }

  public virtual IEnumerator ScrollInit(
    Quest00217TabPage page,
    Quest00217Scroll.Parameter param,
    GameObject prefab)
  {
    Quest00217Menu quest00217Menu = this;
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(prefab);
    page.AddItem(gameObject);
    Quest00217Scroll qsi = gameObject.GetComponent<Quest00217Scroll>();
    if (PerformanceConfig.GetInstance().IsTuningEventTopSetting && quest00217Menu.setupBannerCount >= 4)
    {
      qsi.Setup(param, quest00217Menu.serverTime);
      Coroutine coroutine = quest00217Menu.StartCoroutine(qsi.SetAndCreate_BannerSprite());
      quest00217Menu.loadingCoroutineList.Add(coroutine);
    }
    else
    {
      IEnumerator e = qsi.InitScroll(param, quest00217Menu.serverTime);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    LongPressFloatButton btnFormation = qsi.BtnFormation as LongPressFloatButton;
    if (param.descriptions != null && ((IEnumerable<QuestExtraDescription>) param.descriptions).Any<QuestExtraDescription>())
    {
      qsi.setActiveHasDescriptions(true);
      EventDelegate.Set(btnFormation.onLongPress_, (EventDelegate.Callback) (() =>
      {
        if (this.IsPushAndSet())
          return;
        this.pauseDragScrollView();
        Quest00228Scene.ChangeScene(param.descriptions, true);
      }));
    }
    else
    {
      int questL = param.extra.quest_extra_s.quest_l_QuestExtraL;
      qsi.setActiveHasDescriptions(quest00217Menu.checkHasDescriptionScoreCampaing(questL));
      EventDelegate.Set(btnFormation.onLongPress_, (EventDelegate.Callback) (() => this.checkChangeDescriptionScoreCampaing(qsi, questL)));
    }
  }

  private IEnumerator coInitScrcollHunting(
    Quest00217TabPage page,
    EventInfo info,
    GameObject prefab)
  {
    Quest00217Menu quest00217Menu = this;
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(prefab);
    Quest00217Scroll.Parameter parameter = new Quest00217Scroll.Parameter();
    parameter.extra = (PlayerExtraQuestS) null;
    parameter.eventInfo = info;
    parameter.towerInfo = (SM.TowerPeriod) null;
    parameter.isNew = false;
    parameter.isClear = false;
    parameter.isNotice = false;
    parameter.startTime = new DateTime?();
    parameter.isHighlighting = false;
    page.AddItem(gameObject);
    Quest00217ScrollHunting qsh = gameObject.GetComponent<Quest00217ScrollHunting>();
    if (PerformanceConfig.GetInstance().IsTuningEventTopSetting && quest00217Menu.setupBannerCount >= 4)
    {
      qsh.Setup(parameter, quest00217Menu.serverTime);
      Coroutine coroutine = quest00217Menu.StartCoroutine(qsh.SetAndCreate_BannerSprite());
      quest00217Menu.loadingCoroutineList.Add(coroutine);
    }
    else
    {
      IEnumerator e = qsh.InitScroll(parameter, quest00217Menu.serverTime);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    EventDelegate.Set((qsh.BtnFormation as LongPressFloatButton).onLongPress_, (EventDelegate.Callback) (() =>
    {
      if (this.IsPushAndSet())
        return;
      this.StartCoroutine(this.coChangeHuntinDescription(qsh, info));
    }));
  }

  private IEnumerator coInitScrollTower(
    Quest00217TabPage page,
    SM.TowerPeriod info,
    GameObject prefab)
  {
    Quest00217Menu quest00217Menu = this;
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(prefab);
    Quest00217Scroll.Parameter parameter = new Quest00217Scroll.Parameter();
    parameter.extra = (PlayerExtraQuestS) null;
    parameter.eventInfo = (EventInfo) null;
    parameter.towerInfo = info;
    parameter.isNew = false;
    parameter.isClear = false;
    parameter.isNotice = false;
    parameter.startTime = new DateTime?();
    parameter.isHighlighting = false;
    page.AddItem(gameObject);
    Quest00217ScrollTower qsh = gameObject.GetComponent<Quest00217ScrollTower>();
    if (PerformanceConfig.GetInstance().IsTuningEventTopSetting && quest00217Menu.setupBannerCount >= 4)
    {
      qsh.Setup(parameter, quest00217Menu.serverTime);
      Coroutine coroutine = quest00217Menu.StartCoroutine(qsh.SetAndCreate_BannerSprite());
      quest00217Menu.loadingCoroutineList.Add(coroutine);
    }
    else
    {
      IEnumerator e = qsh.InitScroll(parameter, quest00217Menu.serverTime);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    // ISSUE: reference to a compiler-generated method
    EventDelegate.Set((qsh.BtnFormation as LongPressFloatButton).onLongPress_, new EventDelegate.Callback(quest00217Menu.\u003CcoInitScrollTower\u003Eb__52_0));
  }

  private IEnumerator coInitScrollDirect(Quest00217TabPage page, GameObject prefab)
  {
    page.AddItem(UnityEngine.Object.Instantiate<GameObject>(prefab));
    yield break;
  }

  public enum BannerType
  {
    None,
    Extra,
    Event,
    Tower,
  }

  public class BannerClass
  {
    public int priority;
    public Quest00217Menu.BannerType type;

    public BannerClass(int priority, Quest00217Menu.BannerType type)
    {
      this.priority = priority;
      this.type = type;
    }
  }
}
