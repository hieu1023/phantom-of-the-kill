﻿// Decompiled with JetBrains decompiler
// Type: DetailMenuScrollView05
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using SM;
using UnityEngine;

public class DetailMenuScrollView05 : DetailMenuScrollViewBase
{
  [SerializeField]
  protected UILabel TxtIntroduction;

  public override bool Init(PlayerUnit playerUnit)
  {
    this.gameObject.SetActive(true);
    this.Set(playerUnit.unit);
    return true;
  }

  public void Set(UnitUnit unit)
  {
    this.TxtIntroduction.SetTextLocalize(unit.description);
  }
}
