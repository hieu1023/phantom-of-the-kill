﻿// Decompiled with JetBrains decompiler
// Type: LumpToutaResultMenu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LumpToutaResultMenu : MonoBehaviour
{
  private Queue<IEnumerator> QueueOnTouchToNext = new Queue<IEnumerator>();
  [SerializeField]
  private UIGrid grid;
  private bool isBlockTouch;

  public IEnumerator Init(
    List<Unit004832Menu.ResultPlayerUnit> resultPlayerUnits,
    int incrementMedal,
    GainTrustResult[] gainTrustResults,
    UnlockQuest[] unlockQuests)
  {
    Future<GameObject> prefabF = new ResourceObject("Prefabs/unit004_LumpTouta/dir_Unit_Result").Load<GameObject>();
    IEnumerator e = prefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    GameObject unitResultPrefab = prefabF.Result;
    prefabF = Res.Prefabs.UnitIcon.normal.Load<GameObject>();
    e = prefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    GameObject normalPrefab = prefabF.Result;
    if (resultPlayerUnits.Count != gainTrustResults.Length)
      Debug.LogError((object) "resultPlayerUnits.Count != gainTrustResults.Length Not Same Count!!");
    for (int i = 0; i < resultPlayerUnits.Count; ++i)
    {
      yield return (object) unitResultPrefab.CloneAndGetComponent<UnitResult>(this.grid.transform).Init(normalPrefab, resultPlayerUnits[i]);
      if (gainTrustResults[i].has_new_player_awake_skill)
        this.QueueOnTouchToNext.Enqueue(this.ExtraSKillRememberSequence(resultPlayerUnits[i]));
    }
    this.grid.repositionNow = true;
    if (unlockQuests != null && unlockQuests.Length != 0)
      this.QueueOnTouchToNext.Enqueue(this.CharacterQuestStoryPopup(unlockQuests));
    if (incrementMedal > 0)
      this.QueueOnTouchToNext.Enqueue(this.RareMedalGetPopup(incrementMedal));
    this.QueueOnTouchToNext.Enqueue(this.changeSceneLumpTouta());
  }

  public void OnTouchToNext()
  {
    if (this.isBlockTouch)
      return;
    this.StartCoroutine(this.QueueOnTouchToNext.Dequeue());
  }

  private IEnumerator ExtraSKillRememberSequence(
    Unit004832Menu.ResultPlayerUnit resultPlayerUnit)
  {
    this.isBlockTouch = true;
    GameObject Prefab = (GameObject) null;
    Future<GameObject> prefabF;
    IEnumerator e;
    if (resultPlayerUnit.afterPlayerUnit.unit.IsSea)
    {
      prefabF = new ResourceObject("Animations/extraskill/FavorabilityRatingEffect").Load<GameObject>();
      e = prefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      Prefab = prefabF.Result.Clone(Singleton<CommonRoot>.GetInstance().LoadTmpObj.transform);
      prefabF = (Future<GameObject>) null;
    }
    else if (resultPlayerUnit.afterPlayerUnit.unit.IsResonanceUnit)
    {
      prefabF = new ResourceObject("Animations/common_gear_skill/CommonGearSkillEffect").Load<GameObject>();
      e = prefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      Prefab = prefabF.Result.Clone(Singleton<CommonRoot>.GetInstance().LoadTmpObj.transform);
      prefabF = (Future<GameObject>) null;
    }
    if (!((UnityEngine.Object) Prefab == (UnityEngine.Object) null))
    {
      bool isFinished = false;
      bool flag = (double) resultPlayerUnit.beforePlayerUnit.trust_rate < (double) Consts.GetInstance().TRUST_RATE_LEVEL_SIZE && (double) resultPlayerUnit.afterPlayerUnit.trust_rate >= (double) Consts.GetInstance().TRUST_RATE_LEVEL_SIZE;
      FavorabilityRatingEffect script = Prefab.GetComponent<FavorabilityRatingEffect>();
      e = script.Init(FavorabilityRatingEffect.AnimationType.SkillRelease, resultPlayerUnit.afterPlayerUnit, (System.Action) (() =>
      {
        Singleton<PopupManager>.GetInstance().dismiss(false);
        isFinished = true;
      }), !flag);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      Singleton<PopupManager>.GetInstance().open(Prefab, false, false, true, true, false, false, "SE_1006");
      script.StartEffect();
      while (!isFinished)
        yield return (object) null;
      this.isBlockTouch = false;
    }
  }

  private IEnumerator CharacterQuestStoryPopup(UnlockQuest[] unlockQuests)
  {
    this.isBlockTouch = true;
    Future<GameObject> prefab = Res.Prefabs.battle.popup_020_11_2__anim_popup01.Load<GameObject>();
    IEnumerator e = prefab.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    UnlockQuest[] unlockQuestArray = unlockQuests;
    for (int index = 0; index < unlockQuestArray.Length; ++index)
    {
      QuestCharacterS quest = MasterData.QuestCharacterS[unlockQuestArray[index].quest_s_id];
      Singleton<NGSoundManager>.GetInstance().playSE("SE_1028", false, 0.8f, -1);
      Battle020112Menu o = Singleton<PopupManager>.GetInstance().open(prefab.Result, false, false, false, true, false, false, "SE_1006").GetComponent<Battle020112Menu>();
      yield return (object) o.Init(quest, false);
      bool isFinished = false;
      o.SetCallback((System.Action) (() => isFinished = true));
      while (!isFinished)
        yield return (object) null;
      o = (Battle020112Menu) null;
    }
    unlockQuestArray = (UnlockQuest[]) null;
    this.isBlockTouch = false;
  }

  private IEnumerator RareMedalGetPopup(int increment_medal)
  {
    this.isBlockTouch = true;
    Future<GameObject> prefabf = Res.Prefabs.popup.popup_004_8_13__anim_popup01.Load<GameObject>();
    IEnumerator e = prefabf.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Popup004813Menu component = Singleton<PopupManager>.GetInstance().openAlert(prefabf.Result, false, false, (EventDelegate) null, false, true, false, true).GetComponent<Popup004813Menu>();
    component.SetText(increment_medal);
    bool isFinished = false;
    component.SetIbtnOKCallback((System.Action) (() => isFinished = true));
    while (!isFinished)
      yield return (object) null;
    this.isBlockTouch = false;
  }

  private IEnumerator changeSceneLumpTouta()
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("unit004_LumpTouta", false, (object[]) Array.Empty<object>());
    Singleton<NGSceneManager>.GetInstance().destroyScene("unit004_LumpTouta_Result");
    yield break;
  }
}
