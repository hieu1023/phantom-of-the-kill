﻿// Decompiled with JetBrains decompiler
// Type: Startup00063Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using DeviceKit;
using GameCore;
using SM;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Startup00063Scene : MonoBehaviour
{
  [SerializeField]
  private UIRoot uiRoot;

  private void Awake()
  {
    ModalWindow.setupRootPanel(this.uiRoot);
  }

  public void Mail()
  {
    Player player = SMManager.Get<Player>();
    string helpContactAddress = Consts.GetInstance().HELP_CONTACT_ADDRESS;
    string helpContactTitle = Consts.GetInstance().HELP_CONTACT_TITLE;
    string str = string.Format("{0}/{1}/{2}/{3}/{4}", (object) Gsc.Device.DeviceInfo.DeviceModel, (object) Gsc.Device.DeviceInfo.DeviceVendor, (object) Gsc.Device.DeviceInfo.OperatingSystem, (object) Gsc.Device.DeviceInfo.ProcessorType, (object) Gsc.Device.DeviceInfo.SystemMemorySize);
    string body = Consts.Format(Consts.GetInstance().HELP_CONTACT_MAIL_BODY, (IDictionary) new Dictionary<string, string>()
    {
      {
        "ver",
        Revision.DLCVersion
      },
      {
        "id",
        player.ToString()
      },
      {
        "agent",
        str
      }
    });
    App.LaunchMailer(helpContactAddress, helpContactTitle, body);
  }
}
