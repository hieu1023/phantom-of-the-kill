﻿// Decompiled with JetBrains decompiler
// Type: MapEditPopupConfirmExchangeMap
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapEditPopupConfirmExchangeMap : MonoBehaviour
{
  [SerializeField]
  private UIButton btnOK_;
  [SerializeField]
  private UIButton btnNG_;
  [SerializeField]
  private UILabel txtName_;
  [SerializeField]
  private UILabel txtCost_;
  [SerializeField]
  private UILabel txtDescription_;
  [SerializeField]
  private GuildTownMapScroll mapCheck_;

  public static IEnumerator show(PlayerGuildTown town, System.Action eventOK, System.Action eventNG)
  {
    Future<GameObject> ldprefab = MapEdit.Prefabs.popup_confirm_exchange_map.Load<GameObject>();
    IEnumerator e = ldprefab.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if ((UnityEngine.Object) ldprefab.Result == (UnityEngine.Object) null)
    {
      eventNG();
    }
    else
    {
      GameObject go = Singleton<PopupManager>.GetInstance().open(ldprefab.Result, false, false, false, true, true, true, "SE_1006");
      MapEditPopupConfirmExchangeMap popup = go.GetComponent<MapEditPopupConfirmExchangeMap>();
      e = popup.initialize(town);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      bool ispressed = false;
      EventDelegate.Set(popup.btnOK_.onClick, (EventDelegate.Callback) (() =>
      {
        if (ispressed)
          return;
        ispressed = true;
        eventOK();
      }));
      EventDelegate.Set(popup.btnNG_.onClick, (EventDelegate.Callback) (() =>
      {
        if (ispressed)
          return;
        ispressed = true;
        eventNG();
      }));
      Singleton<PopupManager>.GetInstance().startOpenAnime(go, false);
      while (!ispressed)
        yield return (object) null;
      Singleton<PopupManager>.GetInstance().dismiss(false);
      while (go.activeSelf)
        yield return (object) null;
    }
  }

  private IEnumerator initialize(PlayerGuildTown town)
  {
    MapTown master = town.master;
    this.txtName_.SetTextLocalize(master.name);
    this.txtDescription_.SetTextLocalize(master.description);
    this.txtCost_.SetTextLocalize(master.cost_capacity);
    IEnumerator e = this.mapCheck_.InitializeAsync(master.stage_id, (List<Tuple<int, int>>) null);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }
}
