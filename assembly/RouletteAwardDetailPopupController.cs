﻿// Decompiled with JetBrains decompiler
// Type: RouletteAwardDetailPopupController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RouletteAwardDetailPopupController : MonoBehaviour
{
  [SerializeField]
  private UIGrid awardItemContainer;
  [SerializeField]
  private UIScrollView awardItemScrollView;
  private RouletteMenu menu;

  public IEnumerator Init(
    RouletteMenu menu,
    List<RouletteR001FreeDeckEntity> awardDataList,
    GameObject awardItemPrefab)
  {
    this.menu = menu;
    for (int i = 0; i < awardDataList.Count; ++i)
    {
      IEnumerator e = awardItemPrefab.Clone(this.awardItemContainer.transform).GetComponent<RouletteAwardDetailPopupItemController>().Init(awardDataList[i]);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    this.awardItemContainer.Reposition();
    yield return (object) new WaitForEndOfFrame();
    this.awardItemScrollView.ResetPosition();
  }

  public void OnTapBackButton()
  {
    this.menu.isDisplayingAwardDetailPopup = false;
    Singleton<PopupManager>.GetInstance().dismiss(false);
  }
}
