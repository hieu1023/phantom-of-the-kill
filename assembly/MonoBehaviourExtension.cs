﻿// Decompiled with JetBrains decompiler
// Type: MonoBehaviourExtension
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

public static class MonoBehaviourExtension
{
  public static CoroutineData<T> StartCoroutine<T>(
    this MonoBehaviour behaviour,
    IEnumerator coroutine)
  {
    return CoroutineData<T>.Start(behaviour, coroutine);
  }

  public static CoroutineData<T> StartCoroutine<T>(
    this MonoBehaviour behaviour,
    MonitorCoroutine<T> coroutine)
  {
    return CoroutineData<T>.Start(behaviour, coroutine);
  }
}
