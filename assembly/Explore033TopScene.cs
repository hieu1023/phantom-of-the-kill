﻿// Decompiled with JetBrains decompiler
// Type: Explore033TopScene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;
using UnityEngine;

public class Explore033TopScene : NGSceneBase
{
  private Explore033TopMenu menu;

  public static void changeScene(bool stack = true, bool serverSync = true)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("explore033_Top", (stack ? 1 : 0) != 0, (object) serverSync);
  }

  public override IEnumerator onInitSceneAsync()
  {
    Explore033TopScene explore033TopScene = this;
    ExploreDataManager dataManager = Singleton<ExploreDataManager>.GetInstance();
    yield return (object) dataManager.LoadSuspendData(false, false);
    if (dataManager.IsNotRegisteredDeck())
    {
      bool isError = false;
      yield return (object) dataManager.RegistrationInitialDeck((System.Action) (() => isError = true));
      if (isError)
      {
        Singleton<ExploreSceneManager>.GetInstance().SetReloadDirty();
        Singleton<NGSceneManager>.GetInstance().ChangeErrorPage();
        yield break;
      }
    }
    yield return (object) dataManager.LoadSuspendData(true, false);
    explore033TopScene.menu = explore033TopScene.menuBase as Explore033TopMenu;
    yield return (object) explore033TopScene.menu.InitAsync();
    Singleton<NGDuelDataManager>.GetInstance().Init();
    yield return (object) Singleton<NGDuelDataManager>.GetInstance().PreloadCommonDuelEffect();
  }

  public IEnumerator onStartSceneAsync(bool serverSync)
  {
    Singleton<CommonRoot>.GetInstance().SetFooterEnable(true);
    Singleton<CommonRoot>.GetInstance().GetNormalHeaderComponent().PlayEntryAnime();
    ExploreSceneManager exploreMgr = Singleton<ExploreSceneManager>.GetInstance();
    ExploreDataManager data = Singleton<ExploreDataManager>.GetInstance();
    yield return (object) exploreMgr.OnStartExploreSceneAsync();
    yield return (object) data.ResumeExplore();
    if (serverSync && !exploreMgr.ReloadDirty)
    {
      bool saveFailed = false;
      yield return (object) data.SaveSuspendData((System.Action) (() => saveFailed = true), false);
      if (saveFailed)
      {
        data.InitReopenPopupState();
        exploreMgr.SetReloadDirty();
        Singleton<NGSceneManager>.GetInstance().ChangeErrorPage();
        yield break;
      }
    }
    Singleton<ExploreLotteryCore>.GetInstance().SetTaskReloadDirty();
    data.UpdateRankingViewInfo();
    yield return (object) this.menu.onStartSceneAsync();
  }

  public void onStartScene(bool serverSync)
  {
    this.setupExploreRenderSetting(true);
    Singleton<NGSceneManager>.GetInstance().waitSceneAction((System.Action) (() =>
    {
      ExploreDataManager instance = Singleton<ExploreDataManager>.GetInstance();
      if (instance.IsRankingPeriodFinished)
        Singleton<ExploreSceneManager>.GetInstanceOrNull()?.Pause(false);
      else if (instance.IsReopenPopup)
      {
        this.StartCoroutine(this.menu.ReopenPopup());
      }
      else
      {
        Singleton<CommonRoot>.GetInstance().isLoading = false;
        Singleton<ExploreSceneManager>.GetInstanceOrNull()?.Pause(false);
      }
    }));
  }

  public IEnumerator onBackSceneAsync(bool serverSync)
  {
    Singleton<CommonRoot>.GetInstance().isActiveBackground = false;
    if (Singleton<ExploreSceneManager>.GetInstance().ReloadDirty)
      Singleton<CommonRoot>.GetInstance().isLoading = true;
    yield return (object) this.menu.onBackSceneAsync();
  }

  public void onBackScene(bool serverSync)
  {
    this.setupExploreRenderSetting(true);
    Singleton<NGSceneManager>.GetInstance().waitSceneAction((System.Action) (() =>
    {
      Singleton<ExploreSceneManager>.GetInstance().OnBackExploreScene();
      ExploreDataManager instance = Singleton<ExploreDataManager>.GetInstance();
      if (instance.IsRankingPeriodFinished || Singleton<ExploreSceneManager>.GetInstance().ReloadDirty)
        Singleton<ExploreSceneManager>.GetInstance().Pause(false);
      else if (instance.IsReopenPopup)
      {
        this.StartCoroutine(this.menu.ReopenPopup());
      }
      else
      {
        Singleton<CommonRoot>.GetInstance().isLoading = false;
        Singleton<ExploreSceneManager>.GetInstance().Pause(false);
      }
    }));
  }

  public override void onEndScene()
  {
    Singleton<ExploreSceneManager>.GetInstance().OnEndExploreScene();
  }

  public override IEnumerator onDestroySceneAsync()
  {
    this.setupExploreRenderSetting(false);
    ExploreSceneManager instance = Singleton<ExploreSceneManager>.GetInstance();
    ExploreDataManager dataMgr = Singleton<ExploreDataManager>.GetInstance();
    instance.OnDestoryExploreScene();
    bool saveFailed = false;
    if (!instance.ReloadDirty && !dataMgr.IsRankingAcceptanceFinish && !Singleton<NGGameDataManager>.GetInstance().IsEarth)
      yield return (object) dataMgr.SaveSuspendData((System.Action) (() => saveFailed = true), false);
    Singleton<NGDuelDataManager>.GetInstance().Init();
    ExploreSceneManager.DestroyInstance();
    dataMgr.ClearCache();
    yield return (object) base.onDestroySceneAsync();
    if (saveFailed)
    {
      dataMgr.InitReopenPopupState();
      Singleton<NGSceneManager>.GetInstance().ChangeErrorPage();
    }
  }

  private void setupExploreRenderSetting(bool enable)
  {
    if (enable)
    {
      RenderSettings.ambientLight = Consts.GetInstance().UI3DMODEL_AMBIENT_COLOR;
      RenderSettings.fog = true;
      RenderSettings.fogMode = FogMode.Linear;
      RenderSettings.fogStartDistance = 120f;
      RenderSettings.fogEndDistance = 180f;
    }
    else
      RenderSettings.fog = false;
  }
}
