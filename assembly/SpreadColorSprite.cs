﻿// Decompiled with JetBrains decompiler
// Type: SpreadColorSprite
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[ExecuteInEditMode]
[AddComponentMenu("NGUI/UI/NGUI SpreadColorSprite")]
public class SpreadColorSprite : UISprite
{
  private UIWidget[] objs;

  protected override void OnInit()
  {
    this.objs = this.gameObject.GetComponentsInChildren<UIWidget>(true);
    base.OnInit();
  }

  public override void Invalidate(bool includeChildren)
  {
    if (this.objs != null)
    {
      foreach (UIWidget uiWidget in this.objs)
        uiWidget.color = this.color;
    }
    base.Invalidate(includeChildren);
  }
}
