﻿// Decompiled with JetBrains decompiler
// Type: Title0241SortPopup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

public class Title0241SortPopup : DisplayOrderSortPopup
{
  public void Init(System.Action sortAction)
  {
    this.Init(Persist.emblemSortCategory.Data.category, sortAction);
  }

  public override void IbtnOK()
  {
    this.SaveData();
    base.IbtnOK();
  }

  public override void SaveData()
  {
    base.SaveData();
    Persist.emblemSortCategory.Data.category = this.nowCategory;
    Persist.emblemSortCategory.Flush();
  }
}
