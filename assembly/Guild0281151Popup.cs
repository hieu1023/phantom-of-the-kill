﻿// Decompiled with JetBrains decompiler
// Type: Guild0281151Popup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;
using UnityEngine;

public class Guild0281151Popup : BackButtonMenuBase
{
  [SerializeField]
  private UILabel popupTitle;
  [SerializeField]
  private UILabel popupDesc;

  public void Initialize()
  {
    this.popupTitle.SetTextLocalize(Consts.Format(Consts.GetInstance().POPUP_028_1_1_5_TITLE, (IDictionary) null));
    this.popupDesc.SetTextLocalize(Consts.Format(Consts.GetInstance().POPUP_028_1_1_5_1_DESC, (IDictionary) null));
  }

  public override void onBackButton()
  {
    Singleton<PopupManager>.GetInstance().closeAll(false);
  }
}
