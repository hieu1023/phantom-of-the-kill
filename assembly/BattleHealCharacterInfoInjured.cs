﻿// Decompiled with JetBrains decompiler
// Type: BattleHealCharacterInfoInjured
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;
using UnityEngine;

public class BattleHealCharacterInfoInjured : BattleHealCharacterInfoBase
{
  [SerializeField]
  protected UILabel hpNumberAfter;
  [SerializeField]
  protected UILabel hpNumberBefore;
  [SerializeField]
  private UI2DSprite[] buffSprites;

  public override IEnumerator Init(BL.UnitPosition up, AttackStatus[] attacks)
  {
    if (up == null)
    {
      Debug.LogWarning((object) "unit is null");
    }
    else
    {
      IEnumerator e = base.Init(up, attacks);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.hpNumberBefore.SetTextLocalize(up.unit.parameter.Hp);
    }
  }

  public void setCurrentHP(int healHP)
  {
    BL.Unit unit = this.currentUnit.unit;
    int n = Mathf.Min(healHP + unit.hp, unit.parameter.Hp);
    this.setHPNumbers(n.ToString());
    this.hpBar.setValue(unit.hp, unit.parameter.Hp, false, -1f, -1f);
    this.consumeBar.setValue(n, unit.parameter.Hp, false, -1f, -1f);
  }

  private new void setHPNumbers(string hp)
  {
    this.hpNumberAfter.SetTextLocalize(hp);
  }

  protected override ResourceObject maskResource()
  {
    return Res.GUI._009_3_sozai.mask_Chara_R;
  }
}
