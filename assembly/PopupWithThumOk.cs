﻿// Decompiled with JetBrains decompiler
// Type: PopupWithThumOk
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using System.Collections;
using UnityEngine;

public class PopupWithThumOk : PopupCommon
{
  [SerializeField]
  private CreateIconObject createIcon;

  public IEnumerator Initialize(
    string title,
    string message,
    MasterDataTable.CommonRewardType rewardType,
    int rewardId,
    int quantity = 0,
    System.Action callback = null)
  {
    PopupWithThumOk popupWithThumOk = this;
    popupWithThumOk.transform.localScale = Vector3.zero;
    popupWithThumOk.Init(title, message, callback);
    IEnumerator e = popupWithThumOk.createIcon.CreateThumbnail(rewardType, rewardId, quantity, true, false, new CommonQuestType?(), false);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }
}
