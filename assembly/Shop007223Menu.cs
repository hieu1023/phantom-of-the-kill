﻿// Decompiled with JetBrains decompiler
// Type: Shop007223Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;
using UnityEngine;

public class Shop007223Menu : BackButtonMenuBase
{
  [SerializeField]
  private UILabel txtProductName;
  [SerializeField]
  private UILabel txtOverlimitTxt;
  private Shop00722Menu menu0722;
  private bool giveMessage;

  public void Init(PlayerShopArticle article, Shop00722Menu menu, bool giveMessage)
  {
    this.menu0722 = menu;
    this.giveMessage = giveMessage;
    this.txtProductName.SetTextLocalize(article.article.name);
    this.txtOverlimitTxt.gameObject.SetActive(giveMessage);
  }

  public virtual void IbtnPopupYes()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<PopupManager>.GetInstance().onDismiss();
    this.menu0722.UpdateInfo();
  }

  public override void onBackButton()
  {
    this.IbtnPopupYes();
  }
}
