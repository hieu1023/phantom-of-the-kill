﻿// Decompiled with JetBrains decompiler
// Type: TextTips
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class TextTips : CommonTips
{
  private List<GameObject> blinkGroup = new List<GameObject>();
  private const uint CREATE_WORD_PARTS_COUNT = 6;
  [SerializeField]
  private UI2DSprite slcBgSprite;
  [SerializeField]
  private Transform textTipsPosition;
  [SerializeField]
  private Transform backgroundTipsPosition;
  [SerializeField]
  private NGxBlinkExNext blink;
  [SerializeField]
  private GameObject slcLineDeco;
  [SerializeField]
  private GameObject slcLineDeco2;
  [SerializeField]
  private GameObject blackS;
  private List<int> makeIdList;
  private GameObject textTipsPrefab;
  private GameObject backgroundTipsPrefab;
  private GameObject backgroundTips;
  private UnityEngine.Sprite backgroundTipsSprite;

  protected override void Awake()
  {
    this.blink.StopAllCoroutines();
    this.blink.enabled = false;
    base.Awake();
    this.makeIdList = this.GetValidDataIdListAtRandom();
    List<int> makeIdList = this.makeIdList;
  }

  protected override IEnumerator Start()
  {
    // ISSUE: reference to a compiler-generated field
    int num = this.\u003C\u003E1__state;
    TextTips textTips = this;
    if (num != 0)
    {
      if (num != 1)
        return false;
      // ISSUE: reference to a compiler-generated field
      this.\u003C\u003E1__state = -1;
      return false;
    }
    // ISSUE: reference to a compiler-generated field
    this.\u003C\u003E1__state = -1;
    // ISSUE: reference to a compiler-generated field
    this.\u003C\u003E2__current = (object) textTips.StartCoroutine(textTips.Init());
    // ISSUE: reference to a compiler-generated field
    this.\u003C\u003E1__state = 1;
    return true;
  }

  private void Update()
  {
    if (Singleton<PopupManager>.GetInstance().ModalWindowIsOpen || !Input.GetMouseButtonDown(0) || !this.blink.isActiveAndEnabled)
      return;
    this.blink.BlinkToNextElement();
  }

  private IEnumerator Init()
  {
    IEnumerator e = this.LoadTipsPrefabs();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  private IEnumerator LoadTipsPrefabs()
  {
    Future<UnityEngine.Sprite> slcBgF = Singleton<ResourceManager>.GetInstance().LoadOrNull<UnityEngine.Sprite>("TipsLoading/slc_LoadingImage_tips");
    IEnumerator e = slcBgF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Future<GameObject> textTipsPrefabf = Res.Prefabs.TipsLoading.dir_txt_tips.Load<GameObject>();
    e = textTipsPrefabf.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.textTipsPrefab = textTipsPrefabf.Result;
    Future<GameObject> backgroundTipsPrefabf = Res.Prefabs.TipsLoading.slc_SlideShow_bg.Load<GameObject>();
    e = backgroundTipsPrefabf.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.backgroundTipsPrefab = backgroundTipsPrefabf.Result;
    TipsTextTips tipsTextTips = (TipsTextTips) null;
    if (MasterData.TipsTextTips.TryGetValue(this.makeIdList[0], out tipsTextTips))
    {
      Future<UnityEngine.Sprite> bgSpriteF = Singleton<ResourceManager>.GetInstance().LoadOrNull<UnityEngine.Sprite>("TipsLoading/" + tipsTextTips.image_name);
      e = bgSpriteF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.backgroundTips = UnityEngine.Object.Instantiate<GameObject>(this.backgroundTipsPrefab, this.backgroundTipsPosition);
      this.backgroundTips.GetComponent<UI2DSprite>().sprite2D = bgSpriteF.Result;
      this.backgroundTips.gameObject.SetActive(true);
      bgSpriteF = (Future<UnityEngine.Sprite>) null;
    }
    this.slcBgSprite.sprite2D = slcBgF.Result;
    this.slcLineDeco.SetActive(true);
    this.slcLineDeco2.SetActive(true);
    this.blackS.SetActive(true);
    TipsTextTips tips = (TipsTextTips) null;
    for (int index = 0; index < this.makeIdList.Count; ++index)
    {
      if (MasterData.TipsTextTips.TryGetValue(this.makeIdList[index], out tips))
      {
        GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.textTipsPrefab, this.textTipsPosition);
        gameObject.GetComponent<TextTipsPrefab>().Init(tips);
        this.blinkGroup.Add(gameObject);
      }
    }
    if (this.blinkGroup.Count > 1)
    {
      this.blink.StopAllCoroutines();
      this.blink.SetChildren(this.blinkGroup.ToArray());
      this.blink.enabled = true;
    }
  }

  private List<int> GetValidDataIdListAtRandom()
  {
    List<TipsTextTips> list1 = ((IEnumerable<TipsTextTips>) MasterData.TipsTextTipsList).Where<TipsTextTips>((Func<TipsTextTips, bool>) (tips => tips.enable && tips.image_name != "")).ToList<TipsTextTips>();
    TipsTextTips mainTips = list1[UnityEngine.Random.Range(0, list1.Count)];
    List<int> list2 = ((IEnumerable<TipsTextTips>) MasterData.TipsTextTipsList).Where<TipsTextTips>((Func<TipsTextTips, bool>) (tips => tips.enable && tips.image_name == mainTips.image_name)).Select<TipsTextTips, int>((Func<TipsTextTips, int>) (tips => tips.ID)).ToList<int>();
    if (list2 == null)
      return (List<int>) null;
    int capacity = Mathf.Min(6, list2.Count);
    if (capacity < 1)
      return (List<int>) null;
    List<int> intList = new List<int>(capacity);
    for (int index1 = 0; index1 < capacity; ++index1)
    {
      int index2 = UnityEngine.Random.Range(0, list2.Count);
      intList.Add(list2[index2]);
      list2.RemoveAt(index2);
    }
    return intList;
  }
}
