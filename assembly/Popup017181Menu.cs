﻿// Decompiled with JetBrains decompiler
// Type: Popup017181Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using UnityEngine;

public class Popup017181Menu : BattleBackButtonMenuBase
{
  [SerializeField]
  private UILabel label;

  public void Awake()
  {
    NGBattleManager instance = Singleton<NGBattleManager>.GetInstance();
    if (instance.isPvp || instance.isPvnpc)
      this.label.SetTextLocalize(Consts.GetInstance().BATTLE_ESCAPE_POPUP_PVP);
    else if (instance.isGvg && instance.battleInfo.battleId.Equals(string.Empty))
      this.label.SetTextLocalize(string.Empty);
    else
      this.label.SetTextLocalize(Consts.GetInstance().BATTLE_ESCAPE_POPUP);
  }

  public void IbtnYes()
  {
    this.battleManager.getController<BattleAIController>().stopAIAction();
    this.battleManager.getManager<BattleTimeManager>().setPhaseState(BL.Phase.surrender, true);
    this.battleManager.isRetire = true;
    this.battleManager.popupCloseAll(false);
  }

  public override void onBackButton()
  {
    this.IbtnNo();
  }

  public void IbtnNo()
  {
    this.battleManager.popupDismiss(false, false);
  }
}
