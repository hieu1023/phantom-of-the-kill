﻿// Decompiled with JetBrains decompiler
// Type: DeckOrganization.DeckPosition
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

namespace DeckOrganization
{
  public class DeckPosition
  {
    public int index_ { get; private set; }

    public Unit unit_ { get; private set; }

    public DeckPosition(int index)
    {
      this.index_ = index;
      this.unit_ = (Unit) null;
    }

    public void setUnit(Unit unit)
    {
      if (this.unit_ != null)
        this.unit_.setIndex(-1);
      this.unit_ = unit;
      unit?.setIndex(this.index_);
    }
  }
}
