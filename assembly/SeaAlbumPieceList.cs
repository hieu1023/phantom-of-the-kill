﻿// Decompiled with JetBrains decompiler
// Type: SeaAlbumPieceList
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using SM;
using UnityEngine;

public class SeaAlbumPieceList : MonoBehaviour
{
  [SerializeField]
  private UILabel txtPiece;
  [SerializeField]
  private UILabel txtProgress;
  [SerializeField]
  private GameObject acheiveObj;

  public void Init(PlayerAlbumPiece playerPiece, SeaAlbumPiece piece)
  {
    this.txtPiece.SetTextLocalize(piece.name);
    this.txtProgress.SetTextLocalize(playerPiece.count.ToString() + "/" + (object) piece.count);
    this.acheiveObj.SetActive(playerPiece.count == piece.count);
  }
}
