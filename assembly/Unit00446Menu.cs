﻿// Decompiled with JetBrains decompiler
// Type: Unit00446Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using System.Collections;
using UnityEngine;

public class Unit00446Menu : BackButtonMenuBase
{
  [SerializeField]
  protected UILabel TxtBattleTitle;
  public UI2DSprite GearSpriteObject;
  public UI2DSprite rarityStars;
  public UILabel TxtTitle;

  public IEnumerator SetSprite(GearGear targetgear)
  {
    Future<UnityEngine.Sprite> spriteF = targetgear.LoadSpriteBasic(1f);
    IEnumerator e = spriteF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.GearSpriteObject.sprite2D = spriteF.Result;
    this.GearSpriteObject.width = Mathf.FloorToInt(spriteF.Result.textureRect.width);
    this.GearSpriteObject.height = Mathf.FloorToInt(spriteF.Result.textureRect.height);
    RarityIcon.SetRarity(targetgear, this.rarityStars);
    this.TxtTitle.SetTextLocalize(targetgear.name);
  }

  public void changeScene()
  {
    if (this.IsPushAndSet())
      return;
    this.backScene();
  }

  public virtual void IbtnBattleBack()
  {
  }

  public override void onBackButton()
  {
    this.changeScene();
  }
}
