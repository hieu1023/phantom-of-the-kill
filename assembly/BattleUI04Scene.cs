﻿// Decompiled with JetBrains decompiler
// Type: BattleUI04Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class BattleUI04Scene : BattleSceneBase
{
  [SerializeField]
  private BattleUI04Menu menu;

  public IEnumerator onStartSceneAsync(BL.UnitPosition attack, BL.UnitPosition defense)
  {
    BattleUI04Scene battleUi04Scene = this;
    Stopwatch sw = new Stopwatch();
    sw.Start();
    IEnumerator e = battleUi04Scene.menu.Init(attack, defense);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Singleton<CommonRoot>.GetInstance().isActiveBackground3DCamera = false;
    e = battleUi04Scene.gameObject.GetComponent<AttachSEtoButtonOnBattle>().AddSEtoButton();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    sw.Stop();
    Debug.LogWarning((object) ("BattleUI04Scene:onInitSceneAsync/" + (object) sw.ElapsedMilliseconds));
  }

  public override void onSceneInitialized()
  {
    base.onSceneInitialized();
    this.menu.onInitialized();
  }

  public override void onEndScene()
  {
    this.menu.onEndScene();
    Singleton<CommonRoot>.GetInstance().isActiveBackground3DCamera = true;
  }
}
