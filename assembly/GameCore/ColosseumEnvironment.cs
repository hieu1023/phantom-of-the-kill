﻿// Decompiled with JetBrains decompiler
// Type: GameCore.ColosseumEnvironment
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;
using System.Collections.Generic;

namespace GameCore
{
  public class ColosseumEnvironment
  {
    public string colosseumTransactionID;
    public Dictionary<int, BL.Unit> playerUnitDict;
    public Dictionary<int, BL.Unit> opponentUnitDict;
    public Dictionary<int, PlayerItem> playerGearDict;
    public Dictionary<int, PlayerItem> playerGearDict2;
    public Dictionary<int, PlayerItem> playerReisouDict;
    public Dictionary<int, PlayerItem> playerReisouDict2;
    public Dictionary<int, PlayerItem> opponentGearDict;
    public Dictionary<int, PlayerItem> opponentGearDict2;
    public Dictionary<int, PlayerItem> opponentReisouDict;
    public Dictionary<int, PlayerItem> opponentReisouDict2;
    public Bonus[] bonusList;
    public string today;
  }
}
