﻿// Decompiled with JetBrains decompiler
// Type: GameCore.TurnOtherHp
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

namespace GameCore
{
  public class TurnOtherHp
  {
    public int hp;
    public List<BL.SkillEffect> consumeSkillEffect;

    public TurnOtherHp(int hp)
    {
      this.hp = hp;
      this.consumeSkillEffect = new List<BL.SkillEffect>();
    }
  }
}
