﻿// Decompiled with JetBrains decompiler
// Type: GameCore.LambdaEqualityComparer`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace GameCore
{
  public class LambdaEqualityComparer<T> : IEqualityComparer<T>
  {
    private readonly Func<T, T, bool> _lambdaComparer;
    private readonly Func<T, int> _lambdaHash;

    public LambdaEqualityComparer(Func<T, T, bool> lambdaComparer)
      : this(lambdaComparer, (Func<T, int>) (o => 0))
    {
    }

    public LambdaEqualityComparer(Func<T, T, bool> lambdaComparer, Func<T, int> lambdaHash)
    {
      if (lambdaComparer == null)
        throw new ArgumentNullException(nameof (lambdaComparer));
      if (lambdaHash == null)
        throw new ArgumentNullException(nameof (lambdaHash));
      this._lambdaComparer = lambdaComparer;
      this._lambdaHash = lambdaHash;
    }

    public bool Equals(T x, T y)
    {
      return this._lambdaComparer(x, y);
    }

    public int GetHashCode(T obj)
    {
      return this._lambdaHash(obj);
    }
  }
}
