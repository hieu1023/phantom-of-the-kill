﻿// Decompiled with JetBrains decompiler
// Type: GameCore.RecoverySkill
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

namespace GameCore
{
  public class RecoverySkill
  {
    public int id;
    public int level;
    public int? remain;
    public int useTurn;

    public RecoverySkill(BL.Skill skill)
    {
      this.id = skill.id;
      this.level = skill.level;
      this.remain = skill.remain;
      this.useTurn = skill.useTurn;
    }
  }
}
