﻿// Decompiled with JetBrains decompiler
// Type: GameCore.RecoveryType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

namespace GameCore
{
  public class RecoveryType
  {
    public RecoveryUnit[] units;
    public int remainTurn;
    public int[] points;
    public int absoluteTurnCount;
    public int turnCount;
  }
}
