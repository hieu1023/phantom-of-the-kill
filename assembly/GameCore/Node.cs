﻿// Decompiled with JetBrains decompiler
// Type: GameCore.Node
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace GameCore
{
  public class Node : INode
  {
    public readonly string Text;
    public readonly float Value;
    public readonly bool isFloat;

    public Node(string text)
    {
      this.Text = text;
      this.isFloat = float.TryParse(text, out this.Value);
    }

    public float Eval(Func<string, float> convert)
    {
      return this.isFloat ? this.Value : convert(this.Text);
    }
  }
}
