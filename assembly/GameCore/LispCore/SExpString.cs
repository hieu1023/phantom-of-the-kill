﻿// Decompiled with JetBrains decompiler
// Type: GameCore.LispCore.SExpString
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace GameCore.LispCore
{
  [Serializable]
  public class SExpString
  {
    public string strValue;

    public SExpString(char[] s)
    {
      this.strValue = new string(s);
    }

    public SExpString(string s)
    {
      this.strValue = s;
    }

    public override string ToString()
    {
      return "\"" + this.strValue + "\"";
    }

    public override bool Equals(object obj)
    {
      return obj != null && obj is SExpString sexpString && sexpString.strValue == this.strValue;
    }

    public bool Equals(SExpString s)
    {
      return s != null && s.strValue == this.strValue;
    }

    public override int GetHashCode()
    {
      return this.strValue.GetHashCode();
    }
  }
}
