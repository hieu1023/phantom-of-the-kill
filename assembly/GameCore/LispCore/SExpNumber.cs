﻿// Decompiled with JetBrains decompiler
// Type: GameCore.LispCore.SExpNumber
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace GameCore.LispCore
{
  [Serializable]
  public class SExpNumber
  {
    private Dictionary<Decimal, Decimal?> numberDic;

    public SExpNumber(Dictionary<Decimal, Decimal?> dic)
    {
      if (dic == null)
        this.numberDic = new Dictionary<Decimal, Decimal?>();
      else
        this.numberDic = dic;
    }

    public Decimal? numberObject(Decimal d)
    {
      if (!this.numberDic.ContainsKey(d))
        this.numberDic[d] = new Decimal?(d);
      return this.numberDic[d];
    }

    public Decimal? numberObject(float f)
    {
      return this.numberObject((Decimal) f);
    }

    public Decimal? numberObject(int i)
    {
      return this.numberObject((Decimal) i);
    }
  }
}
