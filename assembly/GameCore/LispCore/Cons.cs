﻿// Decompiled with JetBrains decompiler
// Type: GameCore.LispCore.Cons
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace GameCore.LispCore
{
  [Serializable]
  public class Cons
  {
    public object car;
    public object cdr;

    public override string ToString()
    {
      string str = this.car == null ? "nil" : this.car.ToString();
      if (SExp.consp_(this.cdr))
        return "(" + str + " " + this.cdr.ToString().Substring(1);
      if (this.cdr == null)
        return "(" + str + ")";
      return "(" + str + " . " + this.cdr + ")";
    }
  }
}
