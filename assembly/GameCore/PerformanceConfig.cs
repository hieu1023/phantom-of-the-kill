﻿// Decompiled with JetBrains decompiler
// Type: GameCore.PerformanceConfig
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using UnityEngine;

namespace GameCore
{
  public class PerformanceConfig
  {
    public static float LoadingBlanceTime = 0.2f;
    public static bool IsEnablePerformanceTest;
    public static bool IsForceTest;
    public static float CreateSceneTime;
    private const int tuningNotUseDeepCopyId = 1;
    private const int tuningNotAutoSaveBeforeDuelId = 2;
    private const int tuningEventTopSettingId = 3;
    private const int tuningTitleToHomeId = 4;
    private const int tuningCommonTextureLoaded = 5;
    private const int tuningGachaInitializeId = 7;
    private const int tuningMissionInitializeId = 8;
    private const int enableWebAutoRetryId = 10;
    private bool isSetupSpeck;
    private bool isLowMemory;
    private bool isHiMemory;
    private static PerformanceConfig instance;

    public static PerformanceConfig GetInstance()
    {
      if (PerformanceConfig.instance == null)
        PerformanceConfig.instance = new PerformanceConfig();
      return PerformanceConfig.instance;
    }

    public bool IsNotUseDeepCopy
    {
      get
      {
        AppSetupTuning appSetupTuning = MasterData.AppSetupTuning[1];
        return appSetupTuning != null && appSetupTuning.IsEnable;
      }
    }

    public bool IsNotAutoSaveBeforeDuel
    {
      get
      {
        return !MasterData.AppSetupTuning.ContainsKey(2) || MasterData.AppSetupTuning[2].IsEnable;
      }
    }

    public bool IsTuningEventTopSetting
    {
      get
      {
        return !MasterData.AppSetupTuning.ContainsKey(3) || MasterData.AppSetupTuning[3].IsEnable;
      }
    }

    public bool IsTuningTitleToHome
    {
      get
      {
        return !MasterData.AppSetupTuning.ContainsKey(4) || MasterData.AppSetupTuning[4].IsEnable;
      }
    }

    public bool IsTuningCommonTextureLoaded
    {
      get
      {
        if (!this.IsHiMemory)
          return false;
        return !MasterData.AppSetupTuning.ContainsKey(5) || MasterData.AppSetupTuning[5].IsEnable;
      }
    }

    public bool IsTuningGachaInitialize
    {
      get
      {
        return !MasterData.AppSetupTuning.ContainsKey(7) || MasterData.AppSetupTuning[7].IsEnable;
      }
    }

    public bool IsTuningMissionInitialize
    {
      get
      {
        return !MasterData.AppSetupTuning.ContainsKey(8) || MasterData.AppSetupTuning[8].IsEnable;
      }
    }

    public bool IsLowMemory
    {
      get
      {
        if (!this.isSetupSpeck)
          this.SetupSpeck();
        return this.isLowMemory;
      }
    }

    public bool IsHiMemory
    {
      get
      {
        if (!this.isSetupSpeck)
          this.SetupSpeck();
        return this.isHiMemory;
      }
    }

    private void SetupSpeck()
    {
      this.isLowMemory = SystemInfo.systemMemorySize < 2048;
      this.isHiMemory = SystemInfo.systemMemorySize >= this.GetHiSpecMemorySize();
      this.isSetupSpeck = true;
    }

    private int GetHiSpecMemorySize()
    {
      return 4096;
    }

    public bool EnableWebAutoRetry
    {
      get
      {
        AppSetupTuning appSetupTuning = (AppSetupTuning) null;
        return MasterData.AppSetupTuning.TryGetValue(10, out appSetupTuning) && appSetupTuning.IsEnable;
      }
    }
  }
}
