﻿// Decompiled with JetBrains decompiler
// Type: GameCore.TypeInfoCache
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

namespace GameCore
{
  internal static class TypeInfoCache
  {
    private static Dictionary<string, TypeInfo> cache = new Dictionary<string, TypeInfo>();

    public static void Register(string key, TypeInfo value)
    {
      lock (TypeInfoCache.cache)
      {
        if (TypeInfoCache.cache.Count > 1000)
          TypeInfoCache.cache.Clear();
        TypeInfoCache.cache[key] = value;
      }
    }

    public static bool TryGetValue(string key, out TypeInfo value)
    {
      lock (TypeInfoCache.cache)
        return TypeInfoCache.cache.TryGetValue(key, out value);
    }
  }
}
