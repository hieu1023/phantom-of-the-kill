﻿// Decompiled with JetBrains decompiler
// Type: GameCore.Reward
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using System;
using UnityEngine;

namespace GameCore
{
  [Serializable]
  public class Reward
  {
    [SerializeField]
    private CommonRewardType type;
    [SerializeField]
    private int id;
    [SerializeField]
    private int quantity;

    public Reward(CommonRewardType type, int id, int quantity)
    {
      this.type = type;
      this.id = id;
      this.quantity = quantity;
    }

    public CommonRewardType Type
    {
      get
      {
        return this.type;
      }
    }

    public int Id
    {
      get
      {
        return this.id;
      }
    }

    public int Quantity
    {
      get
      {
        return this.quantity;
      }
    }
  }
}
