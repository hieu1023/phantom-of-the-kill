﻿// Decompiled with JetBrains decompiler
// Type: GameCore.BattleLogicInitializer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using AI.Logic;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;

namespace GameCore
{
  public class BattleLogicInitializer
  {
    private BL.DropData createFieldEvent(Reward reward)
    {
      return new BL.DropData() { reward = reward };
    }

    private Future<BL.Skill> createSkill(PlayerUnitSkills playerSkill)
    {
      return Future.Single<BL.Skill>(this.createSkillCommon(playerSkill.skill_id, playerSkill.level));
    }

    private Future<BL.Skill> createSkill(GearGearSkill gearSkill)
    {
      return Future.Single<BL.Skill>(this.createSkillCommon(gearSkill.skill_BattleskillSkill, gearSkill.skill_level));
    }

    private Future<BL.Skill> createSkill(PlayerAwakeSkill awakeSkill)
    {
      return Future.Single<BL.Skill>(this.createSkillCommon(awakeSkill.skill_id, awakeSkill.level));
    }

    private BL.Skill createSkillCommon(int skillId, int level)
    {
      BattleskillSkill battleskillSkill;
      if (!MasterData.BattleskillSkill.TryGetValue(skillId, out battleskillSkill))
      {
        Debug.LogError((object) ("Key not Found: MasterData.BattleskillSkill[" + (object) skillId + "]"));
        return (BL.Skill) null;
      }
      BL.Skill skill = new BL.Skill();
      skill.id = skillId;
      skill.level = level;
      skill.useTurn = battleskillSkill.charge_turn - (level - 1);
      if (battleskillSkill.max_use_count != 0 && skill.useTurn < battleskillSkill.max_use_count)
        skill.useTurn = battleskillSkill.max_use_count;
      skill.remain = battleskillSkill.use_count == 0 ? new int?() : new int?(battleskillSkill.use_count + (level - 1));
      skill.initSkillCounts();
      return skill;
    }

    private Future<BL.MagicBullet> createMagicBullet(PlayerUnitSkills playerSkill)
    {
      return Future.Single<BL.MagicBullet>(new BL.MagicBullet()
      {
        skillId = playerSkill.skill.ID
      });
    }

    private Future<BL.MagicBullet> createMagicBullet(GearGearSkill gearSkill)
    {
      return Future.Single<BL.MagicBullet>(new BL.MagicBullet()
      {
        skillId = gearSkill.skill.ID
      });
    }

    private Future<BL.MagicBullet> createMagicBullet(PlayerAwakeSkill awakeSkill)
    {
      return Future.Single<BL.MagicBullet>(new BL.MagicBullet()
      {
        skillId = awakeSkill.masterData.ID
      });
    }

    private Future<BL.MagicBullet> createMagicBullet(IAttackMethod attackMethod)
    {
      return Future.Single<BL.MagicBullet>(new BL.MagicBullet()
      {
        skillId = attackMethod.skill.ID,
        attackMethod = attackMethod
      });
    }

    private IEnumerator createFutureUnit(
      Promise<BL.Unit> promise,
      PlayerUnit pu,
      int index,
      bool friend,
      Future<BL.Skill> ougiF,
      Future<List<BL.Skill>> skillsF,
      Future<List<BL.MagicBullet>> magicBulletsF,
      Future<List<BL.Skill>> duelSkillsF,
      Future<List<BL.Skill>> gearCommandSkillsF,
      Future<List<BL.Skill>> gearDuelSkillsF,
      Future<List<BL.MagicBullet>> gearMagicBulletsF,
      Future<List<BL.Skill>> awakeCommandSkillsF,
      Future<List<BL.Skill>> awakeDuelSkillsF,
      Future<List<BL.MagicBullet>> awakeMagicBulletsF,
      Future<List<BL.MagicBullet>> optionMagicBulletsF)
    {
      IEnumerator e = ougiF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      e = skillsF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      e = magicBulletsF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      e = duelSkillsF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      e = gearCommandSkillsF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      e = gearDuelSkillsF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      e = gearMagicBulletsF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      e = awakeCommandSkillsF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      e = awakeDuelSkillsF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      e = awakeMagicBulletsF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      e = optionMagicBulletsF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      BL.Skill result1 = ougiF.Result;
      List<BL.Skill> result2 = skillsF.Result;
      List<BL.MagicBullet> result3 = magicBulletsF.Result;
      List<BL.Skill> result4 = duelSkillsF.Result;
      List<BL.Skill> result5 = gearCommandSkillsF.Result;
      List<BL.Skill> result6 = gearDuelSkillsF.Result;
      List<BL.MagicBullet> result7 = gearMagicBulletsF.Result;
      List<BL.Skill> result8 = awakeCommandSkillsF.Result;
      List<BL.Skill> result9 = awakeDuelSkillsF.Result;
      List<BL.MagicBullet> result10 = awakeMagicBulletsF.Result;
      List<BL.MagicBullet> result11 = optionMagicBulletsF.Result;
      BL.Weapon weapon = new BL.Weapon();
      weapon.gearId = pu.equippedWeaponGearOrInitial.ID;
      BL.Unit unit = new BL.Unit();
      unit.specificId = pu.id;
      unit.unitId = pu.unit.ID;
      unit.playerUnit = pu;
      unit.lv = pu.level;
      unit.spawnTurn = pu.spawn_turn;
      unit.weapon = weapon;
      unit.optionWeapons = ((IEnumerable<IAttackMethod>) pu.battleOptionAttacks).Where<IAttackMethod>((Func<IAttackMethod, bool>) (x => x.kind.Enum != GearKindEnum.magic)).Select<IAttackMethod, BL.Weapon>((Func<IAttackMethod, BL.Weapon>) (y => new BL.Weapon()
      {
        attackMethod = y
      })).ToArray<BL.Weapon>();
      unit.gearLeftHand = pu.isLeftHandWeapon;
      unit.gearDualWield = pu.isDualWieldWeapon;
      unit.AIMoveGroup = pu.ai_move_group;
      unit.AIMoveGroupOrder = pu.ai_move_group_order;
      unit.AIMoveTargetX = pu.ai_move_target_x;
      unit.AIMoveTargetY = pu.ai_move_target_y;
      unit.index = index;
      unit.friend = friend;
      unit.ougi = result1;
      unit.skills = result2.Concat<BL.Skill>((IEnumerable<BL.Skill>) result5).Concat<BL.Skill>((IEnumerable<BL.Skill>) result8).ToArray<BL.Skill>();
      unit.duelSkills = result9.Concat<BL.Skill>((IEnumerable<BL.Skill>) result4).Concat<BL.Skill>((IEnumerable<BL.Skill>) result6).OrderByDescending<BL.Skill, int>((Func<BL.Skill, int>) (x => x.skill.weight)).ThenBy<BL.Skill, int>((Func<BL.Skill, int>) (x => x.id)).ThenByDescending<BL.Skill, int>((Func<BL.Skill, int>) (x => x.level)).ToArray<BL.Skill>();
      unit.magicBullets = result3.Concat<BL.MagicBullet>((IEnumerable<BL.MagicBullet>) result7).Concat<BL.MagicBullet>((IEnumerable<BL.MagicBullet>) result10).Concat<BL.MagicBullet>((IEnumerable<BL.MagicBullet>) result11).ToArray<BL.MagicBullet>();
      foreach (BL.MagicBullet magicBullet in unit.magicBullets)
        magicBullet.setPrefabName(unit.job);
      unit.skillfull_shield = XorShift.Range(1, 5);
      unit.skillfull_weapon = XorShift.Range(1, 5);
      unit.isSpawned = pu.spawn_turn <= 0;
      unit.isEnable = unit.isSpawned;
      unit.mIsExecCompletedSkillEffect = false;
      promise.Result = unit;
    }

    public Future<BL.Unit> createUnitByPlayerUnit(PlayerUnit pu, int index, bool friend)
    {
      pu.resetOnceOverkillers();
      List<PlayerUnitSkills> list = ((IEnumerable<PlayerUnitSkills>) pu.skills).Concat<PlayerUnitSkills>((IEnumerable<PlayerUnitSkills>) pu.retrofitSkills).ToList<PlayerUnitSkills>();
      PlayerUnitSkills[] array1 = list.Where<PlayerUnitSkills>((Func<PlayerUnitSkills, bool>) (v => v.skill.skill_type == BattleskillSkillType.command)).ToArray<PlayerUnitSkills>();
      PlayerUnitSkills[] array2 = list.Where<PlayerUnitSkills>((Func<PlayerUnitSkills, bool>) (v => v.skill.skill_type == BattleskillSkillType.release)).ToArray<PlayerUnitSkills>();
      PlayerUnitSkills[] array3 = list.Where<PlayerUnitSkills>((Func<PlayerUnitSkills, bool>) (v => v.skill.skill_type == BattleskillSkillType.magic)).ToArray<PlayerUnitSkills>();
      PlayerUnitSkills[] array4 = list.Where<PlayerUnitSkills>((Func<PlayerUnitSkills, bool>) (v => v.skill.skill_type == BattleskillSkillType.duel)).ToArray<PlayerUnitSkills>();
      List<GearGearSkill> source1 = pu.equippedGear != (PlayerItem) null ? ((IEnumerable<GearGearSkill>) pu.equippedGear.skills).Where<GearGearSkill>((Func<GearGearSkill, bool>) (v => v.skill.skill_type == BattleskillSkillType.command)).ToList<GearGearSkill>() : new List<GearGearSkill>();
      List<GearGearSkill> source2 = pu.equippedGear != (PlayerItem) null ? ((IEnumerable<GearGearSkill>) pu.equippedGear.skills).Where<GearGearSkill>((Func<GearGearSkill, bool>) (v => v.skill.skill_type == BattleskillSkillType.duel)).ToList<GearGearSkill>() : new List<GearGearSkill>();
      List<GearGearSkill> source3 = pu.equippedGear != (PlayerItem) null ? ((IEnumerable<GearGearSkill>) pu.equippedGear.skills).Where<GearGearSkill>((Func<GearGearSkill, bool>) (v => v.skill.skill_type == BattleskillSkillType.magic)).ToList<GearGearSkill>() : new List<GearGearSkill>();
      if (pu.equippedGear2 != (PlayerItem) null)
      {
        source1.AddRange(((IEnumerable<GearGearSkill>) pu.equippedGear2.skills).Where<GearGearSkill>((Func<GearGearSkill, bool>) (v => v.skill.skill_type == BattleskillSkillType.command)));
        source2.AddRange(((IEnumerable<GearGearSkill>) pu.equippedGear2.skills).Where<GearGearSkill>((Func<GearGearSkill, bool>) (v => v.skill.skill_type == BattleskillSkillType.duel)));
        source3.AddRange(((IEnumerable<GearGearSkill>) pu.equippedGear2.skills).Where<GearGearSkill>((Func<GearGearSkill, bool>) (v => v.skill.skill_type == BattleskillSkillType.magic)));
      }
      if (pu.equippedReisou != (PlayerItem) null)
      {
        source1.AddRange(((IEnumerable<GearGearSkill>) pu.equippedReisou.skills).Where<GearGearSkill>((Func<GearGearSkill, bool>) (v => v.skill.skill_type == BattleskillSkillType.command)));
        source2.AddRange(((IEnumerable<GearGearSkill>) pu.equippedReisou.skills).Where<GearGearSkill>((Func<GearGearSkill, bool>) (v => v.skill.skill_type == BattleskillSkillType.duel)));
        source3.AddRange(((IEnumerable<GearGearSkill>) pu.equippedReisou.skills).Where<GearGearSkill>((Func<GearGearSkill, bool>) (v => v.skill.skill_type == BattleskillSkillType.magic)));
      }
      if (pu.equippedReisou2 != (PlayerItem) null)
      {
        source1.AddRange(((IEnumerable<GearGearSkill>) pu.equippedReisou2.skills).Where<GearGearSkill>((Func<GearGearSkill, bool>) (v => v.skill.skill_type == BattleskillSkillType.command)));
        source2.AddRange(((IEnumerable<GearGearSkill>) pu.equippedReisou2.skills).Where<GearGearSkill>((Func<GearGearSkill, bool>) (v => v.skill.skill_type == BattleskillSkillType.duel)));
        source3.AddRange(((IEnumerable<GearGearSkill>) pu.equippedReisou2.skills).Where<GearGearSkill>((Func<GearGearSkill, bool>) (v => v.skill.skill_type == BattleskillSkillType.magic)));
      }
      PlayerAwakeSkill[] playerAwakeSkillArray;
      if (pu.equippedExtraSkill == null)
        playerAwakeSkillArray = new PlayerAwakeSkill[0];
      else
        playerAwakeSkillArray = new PlayerAwakeSkill[1]
        {
          pu.equippedExtraSkill
        };
      PlayerAwakeSkill[] array5 = ((IEnumerable<PlayerAwakeSkill>) playerAwakeSkillArray).Where<PlayerAwakeSkill>((Func<PlayerAwakeSkill, bool>) (v => v.masterData.skill_type == BattleskillSkillType.command)).ToArray<PlayerAwakeSkill>();
      PlayerAwakeSkill[] array6 = ((IEnumerable<PlayerAwakeSkill>) playerAwakeSkillArray).Where<PlayerAwakeSkill>((Func<PlayerAwakeSkill, bool>) (v => v.masterData.skill_type == BattleskillSkillType.duel)).ToArray<PlayerAwakeSkill>();
      PlayerAwakeSkill[] array7 = ((IEnumerable<PlayerAwakeSkill>) playerAwakeSkillArray).Where<PlayerAwakeSkill>((Func<PlayerAwakeSkill, bool>) (v => v.masterData.skill_type == BattleskillSkillType.magic)).ToArray<PlayerAwakeSkill>();
      Future<BL.Skill> ougiF = array2.Length == 0 ? Future.Single<BL.Skill>((BL.Skill) null) : this.createSkill(((IEnumerable<PlayerUnitSkills>) array2).First<PlayerUnitSkills>());
      Future<List<BL.Skill>> skillsF = ((IEnumerable<PlayerUnitSkills>) array1).Select<PlayerUnitSkills, Future<BL.Skill>>((Func<PlayerUnitSkills, Future<BL.Skill>>) (v => this.createSkill(v))).Sequence<BL.Skill>();
      Future<List<BL.MagicBullet>> magicBulletsF = ((IEnumerable<PlayerUnitSkills>) array3).Select<PlayerUnitSkills, Future<BL.MagicBullet>>((Func<PlayerUnitSkills, Future<BL.MagicBullet>>) (v => this.createMagicBullet(v))).Sequence<BL.MagicBullet>();
      Future<List<BL.Skill>> duelSkillsF = ((IEnumerable<PlayerUnitSkills>) array4).Select<PlayerUnitSkills, Future<BL.Skill>>((Func<PlayerUnitSkills, Future<BL.Skill>>) (v => this.createSkill(v))).Sequence<BL.Skill>();
      Future<List<BL.Skill>> gearCommandSkillsF = source1.Select<GearGearSkill, Future<BL.Skill>>((Func<GearGearSkill, Future<BL.Skill>>) (v => this.createSkill(v))).Sequence<BL.Skill>();
      Future<List<BL.Skill>> gearDuelSkillsF = source2.Select<GearGearSkill, Future<BL.Skill>>((Func<GearGearSkill, Future<BL.Skill>>) (v => this.createSkill(v))).Sequence<BL.Skill>();
      Future<List<BL.MagicBullet>> gearMagicBulletsF = source3.Select<GearGearSkill, Future<BL.MagicBullet>>((Func<GearGearSkill, Future<BL.MagicBullet>>) (v => this.createMagicBullet(v))).Sequence<BL.MagicBullet>();
      Future<List<BL.Skill>> awakeCommandSkillsF = ((IEnumerable<PlayerAwakeSkill>) array5).Select<PlayerAwakeSkill, Future<BL.Skill>>((Func<PlayerAwakeSkill, Future<BL.Skill>>) (v => this.createSkill(v))).Sequence<BL.Skill>();
      Future<List<BL.Skill>> awakeDuelSkillsF = ((IEnumerable<PlayerAwakeSkill>) array6).Select<PlayerAwakeSkill, Future<BL.Skill>>((Func<PlayerAwakeSkill, Future<BL.Skill>>) (v => this.createSkill(v))).Sequence<BL.Skill>();
      Future<List<BL.MagicBullet>> awakeMagicBulletsF = ((IEnumerable<PlayerAwakeSkill>) array7).Select<PlayerAwakeSkill, Future<BL.MagicBullet>>((Func<PlayerAwakeSkill, Future<BL.MagicBullet>>) (v => this.createMagicBullet(v))).Sequence<BL.MagicBullet>();
      Future<List<BL.MagicBullet>> optionMagicBulletsF = ((IEnumerable<IAttackMethod>) pu.battleOptionAttacks).Where<IAttackMethod>((Func<IAttackMethod, bool>) (v => v.kind.Enum == GearKindEnum.magic)).Select<IAttackMethod, Future<BL.MagicBullet>>((Func<IAttackMethod, Future<BL.MagicBullet>>) (x => this.createMagicBullet(x))).Sequence<BL.MagicBullet>();
      return new Future<BL.Unit>((Func<Promise<BL.Unit>, IEnumerator>) (promise => this.createFutureUnit(promise, pu, index, friend, ougiF, skillsF, magicBulletsF, duelSkillsF, gearCommandSkillsF, gearDuelSkillsF, gearMagicBulletsF, awakeCommandSkillsF, awakeDuelSkillsF, awakeMagicBulletsF, optionMagicBulletsF)));
    }

    private float CalcIndicatorLevel(IEnumerable<PlayerUnit> playerUnits)
    {
      return !playerUnits.Any<PlayerUnit>() ? 0.0f : playerUnits.Where<PlayerUnit>((Func<PlayerUnit, bool>) (x => x != (PlayerUnit) null)).Select<PlayerUnit, float>((Func<PlayerUnit, float>) (x => (float) x.level * x.unit.rarity.indicator_level_rate)).OrderByDescending<float, float>((Func<float, float>) (x => x)).Max();
    }

    private void initializeStage(BattleInfo info, BL env)
    {
      BattleStage stage = info.stage;
      env.initializeFeild(info.stageId);
      if (env.condition == null)
        env.condition = new BL.Condition();
      env.condition.id = stage.victory_condition.ID;
      Dictionary<int, Tuple<int, Reward>> dropDic = new Dictionary<int, Tuple<int, Reward>>();
      if (info.PanelItems != null)
        dropDic = ((IEnumerable<Tuple<int, Reward>>) info.PanelItems).ToDictionary<Tuple<int, Reward>, int>((Func<Tuple<int, Reward>, int>) (v =>
        {
          BattleStagePanelEvent battleStagePanelEvent = MasterData.BattleStagePanelEvent[v.Item1];
          return battleStagePanelEvent.initial_coordinate_x - 1 << 16 | battleStagePanelEvent.initial_coordinate_y - 1;
        }));
      info.SplitFacilityFromEnemyIds();
      BattleVictoryAreaCondition[] winArea = env.condition.winAreaConditoin;
      BattleVictoryAreaCondition[] loseArea = env.condition.loseAreaConditoin;
      BattleReinforcement[] battleReinforcements = info.EnemyReinforcements;
      stage.ApplyLandforms((System.Action<int, int, BattleMapLandform>) ((x, y, landform) =>
      {
        int key = x << 16 | y;
        Tuple<int, Reward> tuple = dropDic == null || !dropDic.ContainsKey(key) ? (Tuple<int, Reward>) null : dropDic[key];
        BL.DropData fieldEvent = tuple == null ? (BL.DropData) null : this.createFieldEvent(tuple.Item2);
        env.setFeildPanel(landform.ID, y, x, landform.landform.ID, tuple == null ? 0 : tuple.Item1, fieldEvent, winArea, loseArea, battleReinforcements);
      }));
    }

    private IEnumerator createEnemyUnits(
      BattleInfo info,
      List<BL.Unit> enemies,
      List<BL.Unit> userEnemies,
      BL env)
    {
      BattleLogicInitializer logicInitializer1 = this;
      int i;
      Future<BL.Unit> future;
      IEnumerator e;
      for (i = 0; i < info.Enemies.Length; ++i)
      {
        BattleStageEnemy enemy = info.Enemies[i];
        BattleLogicInitializer logicInitializer2 = logicInitializer1;
        BattleStageEnemy enemy1 = enemy;
        BattleLogicInitializer logicInitializer3 = logicInitializer1;
        PlayerUnit[] playerUnits1 = info.deck.player_units;
        PlayerUnit[] playerUnitArray;
        if (info.helper == null)
          playerUnitArray = new PlayerUnit[0];
        else
          playerUnitArray = new PlayerUnit[1]
          {
            info.helper.leader_unit
          };
        IEnumerable<PlayerUnit> playerUnits2 = ((IEnumerable<PlayerUnit>) playerUnits1).Concat<PlayerUnit>((IEnumerable<PlayerUnit>) playerUnitArray);
        double num1 = (double) logicInitializer3.CalcIndicatorLevel(playerUnits2);
        XorShift random = env.random;
        int endlessLoopCount = info.raidEndlessLoopCount;
        int questSId = info.quest_s_id;
        int num2 = info.raidBossDamage.Key == enemy.ID ? 1 : 0;
        PlayerUnit pu = PlayerUnit.FromEnemy(enemy1, (float) num1, random, endlessLoopCount, questSId, num2 != 0);
        int index = i;
        future = logicInitializer2.createUnitByPlayerUnit(pu, index, false);
        e = future.Wait();
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        future.Result.dropMoney = enemy.money;
        enemies.Add(future.Result);
        if (info.quest_type == CommonQuestType.Tower)
          future.Result.playerUnit.tower_hitpoint_rate = !info.enemyHpRate.ContainsKey(future.Result.playerUnit.id) ? 100f : info.enemyHpRate[future.Result.playerUnit.id];
        enemy = (BattleStageEnemy) null;
        future = (Future<BL.Unit>) null;
      }
      foreach (Tuple<int, Reward> tuple in ((IEnumerable<Tuple<int, Reward>>) info.EnemyItems).Where<Tuple<int, Reward>>((Func<Tuple<int, Reward>, bool>) (ei => enemies.Any<BL.Unit>((Func<BL.Unit, bool>) (x => x.playerUnit.id == ei.Item1)))))
      {
        Tuple<int, Reward> t = tuple;
        enemies.Single<BL.Unit>((Func<BL.Unit, bool>) (x => x.playerUnit.id == t.Item1)).drop = new BL.DropData()
        {
          reward = t.Item2
        };
      }
      if (info.UserEnemies != null)
      {
        for (i = 0; i < info.UserEnemies.Length; ++i)
        {
          if (info.user_units.Length > i && info.user_units[i] != (PlayerUnit) null)
          {
            BattleStageUserUnit enemyUser = info.UserEnemies[i];
            PlayerUnit userUnit = info.user_units[enemyUser.deck_position - 1];
            if (!(userUnit == (PlayerUnit) null))
            {
              PlayerItem equippedGear = userUnit.FindEquippedGear(info.user_items);
              PlayerItem equippedGear2 = userUnit.FindEquippedGear2(info.user_items);
              PlayerItem reisou = (PlayerItem) null;
              PlayerItem reisou2 = (PlayerItem) null;
              userUnit.SetUserEnemyUnit(enemyUser, equippedGear, equippedGear2, reisou, reisou2, enemyUser.deck_position == 1);
              future = logicInitializer1.createUnitByPlayerUnit(userUnit, enemies.Count + i, false);
              e = future.Wait();
              while (e.MoveNext())
                yield return e.Current;
              e = (IEnumerator) null;
              future.Result.dropMoney = enemyUser.money;
              Tuple<int, Reward> tuple = ((IEnumerable<Tuple<int, Reward>>) info.UserEnemyItems).SingleOrDefault<Tuple<int, Reward>>((Func<Tuple<int, Reward>, bool>) (x => x.Item1 == enemyUser.ID));
              if (tuple != null)
                future.Result.drop = new BL.DropData()
                {
                  reward = tuple.Item2
                };
              userEnemies.Add(future.Result);
              future = (Future<BL.Unit>) null;
            }
          }
        }
      }
    }

    private IEnumerator createFacilityUnits(
      BattleInfo info,
      List<BL.Unit> facilities,
      BL env)
    {
      if (info.facility_units != null)
      {
        for (int i = 0; i < info.facility_units.Length; ++i)
        {
          Future<BL.Unit> future = this.createUnitByPlayerUnit(info.facility_units[i], i, false);
          IEnumerator e = future.Wait();
          while (e.MoveNext())
            yield return e.Current;
          e = (IEnumerator) null;
          BL.Unit result = future.Result;
          BL.Facility facility1 = new BL.Facility();
          facility1.thisForce = result.playerUnit.is_enemy ? BL.ForceID.enemy : BL.ForceID.player;
          MapFacility facility2 = result.unit.facility;
          facility1.isTarget = facility2.is_target;
          facility1.isPutOn = facility2.is_puton;
          facility1.isView = facility2.is_view;
          result.facility = facility1;
          result.AIMoveGroupOrder = 100;
          facilities.Add(result);
          if (info.quest_type == CommonQuestType.Tower)
            result.playerUnit.tower_hitpoint_rate = !info.enemyHpRate.ContainsKey(result.playerUnit.id) ? 100f : info.enemyHpRate[result.playerUnit.id];
          future = (Future<BL.Unit>) null;
        }
      }
    }

    public IEnumerator initializeWave(int wave, BattleInfo info, BL env)
    {
      if (info.isWave)
      {
        info.currentWave = env.currentWave = wave;
        IEnumerator e = Singleton<NGBattleManager>.GetInstance().initMasterData(info);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        List<BL.Unit> players = new List<BL.Unit>();
        List<BL.Unit> guests = new List<BL.Unit>();
        List<BL.Unit> friend = new List<BL.Unit>();
        List<BL.Unit> enemies = new List<BL.Unit>();
        List<BL.Unit> userEnemies = new List<BL.Unit>();
        foreach (BL.Unit unit in env.playerUnits.value)
        {
          foreach (BL.Unit invocationUnit in env.enemyUnits.value)
            unit.skillEffects.RemoveEffect(invocationUnit, env, (BL.ISkillEffectListUnit) unit);
          if (unit.playerUnit.is_gesut)
            guests.Add(unit);
          else if (unit.is_helper)
            friend.Add(unit);
          else
            players.Add(unit);
          unit.isEnable = false;
        }
        this.initializeStage(info, env);
        e = this.createEnemyUnits(info, enemies, userEnemies, env);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        if (env.enemyUnits == null)
          env.enemyUnits = new BL.ClassValue<List<BL.Unit>>(enemies.Concat<BL.Unit>((IEnumerable<BL.Unit>) userEnemies).ToList<BL.Unit>());
        else
          env.enemyUnits.value = enemies.Concat<BL.Unit>((IEnumerable<BL.Unit>) userEnemies).ToList<BL.Unit>();
        List<BL.Unit> facilities = new List<BL.Unit>();
        e = this.createFacilityUnits(info, facilities, env);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        if (env.facilityUnits == null)
          env.facilityUnits = new BL.ClassValue<List<BL.Unit>>(facilities);
        else
          env.facilityUnits.value = facilities;
        this.setLeaderSkills(env.playerUnits.value, env.enemyUnits.value, false, true);
        this.setLeaderSkills(env.enemyUnits.value, env.playerUnits.value, false, false);
        this.setAwakeSkills(env.playerUnits.value, env.enemyUnits.value, false, true);
        this.setAwakeSkills(env.enemyUnits.value, env.playerUnits.value, false, false);
        this.setPassiveSkills(env.playerUnits.value, env.enemyUnits.value, false, true);
        this.setPassiveSkills(env.enemyUnits.value, env.playerUnits.value, false, false);
        this.setGearSkills(env.playerUnits.value, env.enemyUnits.value, false, true);
        this.setGearSkills(env.enemyUnits.value, env.playerUnits.value, false, false);
        this.setFacilitySkills(env.facilityUnits.value, env, false, false);
        foreach (BL.Unit unit in env.enemyUnits.value)
        {
          unit.hp = unit.parameter.Hp;
          foreach (BL.MagicBullet magicBullet in unit.magicBullets)
            magicBullet.setAdditionalCost(unit.hp);
          unit.checkActionRangeBySetHp = true;
        }
        foreach (BL.Unit unit in env.facilityUnits.value)
          unit.hp = unit.parameter.Hp;
        foreach (BL.ISkillEffectListUnit beUnit in env.enemyUnits.value)
          BattleFuncs.createBattleSkillEffectParams(beUnit, (IEnumerable<BL.Unit>) env.enemyUnits.value);
        e = this.createUnitPositions(info, env, players, guests, friend.Where<BL.Unit>((Func<BL.Unit, bool>) (pu => pu != (BL.Unit) null)).ToList<BL.Unit>(), enemies, userEnemies, facilities);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        this.createFieldEffects(info.stage, env);
        env.setCurrentUnitWith((BL.Unit) null, (System.Action<BL.UnitPosition>) (_ => {}));
        env.setCurrentField(0, 0);
        env.phaseState.turnReset();
      }
    }

    private void setupStoryScript(BattleInfo battleInfo, BL env)
    {
      Tuple<StoryPlaybackTiming, int, object[]>[] tupleArray;
      if (battleInfo.isStoryEnable)
      {
        switch (battleInfo.quest_type)
        {
          case CommonQuestType.Story:
            tupleArray = ((IEnumerable<StoryPlaybackStoryDetail>) MasterData.QuestStoryS[battleInfo.quest_s_id].StoryDetails()).Select<StoryPlaybackStoryDetail, Tuple<StoryPlaybackTiming, int, object[]>>((Func<StoryPlaybackStoryDetail, Tuple<StoryPlaybackTiming, int, object[]>>) (x => x.toTuple())).ToArray<Tuple<StoryPlaybackTiming, int, object[]>>();
            break;
          case CommonQuestType.Character:
            tupleArray = ((IEnumerable<StoryPlaybackCharacterDetail>) MasterData.QuestCharacterS[battleInfo.quest_s_id].CharacterDetails()).Select<StoryPlaybackCharacterDetail, Tuple<StoryPlaybackTiming, int, object[]>>((Func<StoryPlaybackCharacterDetail, Tuple<StoryPlaybackTiming, int, object[]>>) (x => x.toTuple())).ToArray<Tuple<StoryPlaybackTiming, int, object[]>>();
            break;
          case CommonQuestType.Extra:
            tupleArray = ((IEnumerable<StoryPlaybackExtraDetail>) MasterData.QuestExtraS[battleInfo.quest_s_id].ExtraDetails()).Select<StoryPlaybackExtraDetail, Tuple<StoryPlaybackTiming, int, object[]>>((Func<StoryPlaybackExtraDetail, Tuple<StoryPlaybackTiming, int, object[]>>) (x => x.toTuple())).ToArray<Tuple<StoryPlaybackTiming, int, object[]>>();
            break;
          case CommonQuestType.Harmony:
            tupleArray = ((IEnumerable<StoryPlaybackHarmonyDetail>) MasterData.QuestHarmonyS[battleInfo.quest_s_id].HarmonyDetail()).Select<StoryPlaybackHarmonyDetail, Tuple<StoryPlaybackTiming, int, object[]>>((Func<StoryPlaybackHarmonyDetail, Tuple<StoryPlaybackTiming, int, object[]>>) (x => x.toTuple())).ToArray<Tuple<StoryPlaybackTiming, int, object[]>>();
            break;
          case CommonQuestType.Earth:
            tupleArray = ((IEnumerable<EarthQuestStoryPlayback>) MasterData.EarthQuestStoryPlaybackList).Where<EarthQuestStoryPlayback>((Func<EarthQuestStoryPlayback, bool>) (x => x.episode.ID == battleInfo.quest_s_id)).Select<EarthQuestStoryPlayback, Tuple<StoryPlaybackTiming, int, object[]>>((Func<EarthQuestStoryPlayback, Tuple<StoryPlaybackTiming, int, object[]>>) (x =>
            {
              int timing = (int) x.timing;
              int scriptId = x.script_id;
              object[] objArray;
              if (x.stage_enemy_BattleStageEnemy.HasValue)
                objArray = new object[2]
                {
                  (object) x.attack_timing_type,
                  (object) x.stage_enemy_BattleStageEnemy
                };
              else
                objArray = new object[0];
              return Tuple.Create<StoryPlaybackTiming, int, object[]>((StoryPlaybackTiming) timing, scriptId, objArray);
            })).ToArray<Tuple<StoryPlaybackTiming, int, object[]>>();
            break;
          case CommonQuestType.Tower:
            tupleArray = ((IEnumerable<TowerPlaybackStoryDetail>) MasterData.TowerPlaybackStoryDetailList).Where<TowerPlaybackStoryDetail>((Func<TowerPlaybackStoryDetail, bool>) (x => x.stage.stage_id == battleInfo.stageId)).Select<TowerPlaybackStoryDetail, Tuple<StoryPlaybackTiming, int, object[]>>((Func<TowerPlaybackStoryDetail, Tuple<StoryPlaybackTiming, int, object[]>>) (x => x.toTuple())).ToArray<Tuple<StoryPlaybackTiming, int, object[]>>();
            break;
          case CommonQuestType.Sea:
            tupleArray = ((IEnumerable<StoryPlaybackSeaDetail>) MasterData.QuestSeaS[battleInfo.quest_s_id].StoryDetails()).Select<StoryPlaybackSeaDetail, Tuple<StoryPlaybackTiming, int, object[]>>((Func<StoryPlaybackSeaDetail, Tuple<StoryPlaybackTiming, int, object[]>>) (x => x.toTuple())).ToArray<Tuple<StoryPlaybackTiming, int, object[]>>();
            break;
          case CommonQuestType.GuildRaid:
            tupleArray = ((IEnumerable<StoryPlaybackRaidDetail>) MasterData.StoryPlaybackRaidDetailList).Where<StoryPlaybackRaidDetail>((Func<StoryPlaybackRaidDetail, bool>) (x => x.stage_id == battleInfo.stageId && !Persist.raidStoryAlreadyRead.Data.isAlreadyRead(x.stage_id, x.script_id))).Select<StoryPlaybackRaidDetail, Tuple<StoryPlaybackTiming, int, object[]>>((Func<StoryPlaybackRaidDetail, Tuple<StoryPlaybackTiming, int, object[]>>) (x => x.toTuple())).ToArray<Tuple<StoryPlaybackTiming, int, object[]>>();
            break;
          default:
            Debug.LogError((object) "ここに来たらバグ");
            tupleArray = new Tuple<StoryPlaybackTiming, int, object[]>[0];
            break;
        }
      }
      else
        tupleArray = new Tuple<StoryPlaybackTiming, int, object[]>[0];
      Dictionary<StoryPlaybackTiming, BL.StoryType> dictionary = new Dictionary<StoryPlaybackTiming, BL.StoryType>()
      {
        {
          StoryPlaybackTiming.before_battle,
          BL.StoryType.battle_start
        },
        {
          StoryPlaybackTiming.located_player_unit,
          BL.StoryType.first_turn_start
        },
        {
          StoryPlaybackTiming.after_battle,
          BL.StoryType.battle_win
        },
        {
          StoryPlaybackTiming.duel_start,
          BL.StoryType.duel_start
        },
        {
          StoryPlaybackTiming.turn_start,
          BL.StoryType.turn_start
        },
        {
          StoryPlaybackTiming.in_area,
          BL.StoryType.unit_in_area
        },
        {
          StoryPlaybackTiming.defeat_player,
          BL.StoryType.defeat_player
        },
        {
          StoryPlaybackTiming.wave_clear,
          BL.StoryType.wave_clear
        }
      };
      List<BL.Story> v = new List<BL.Story>();
      foreach (Tuple<StoryPlaybackTiming, int, object[]> tuple in tupleArray)
        v.Add(new BL.Story(tuple.Item2, dictionary[tuple.Item1], tuple.Item3));
      env.storyList = new BL.ClassValue<List<BL.Story>>(v);
    }

    private void createFieldEffects(BattleStage stage, BL env)
    {
      List<BL.FieldEffect> v = new List<BL.FieldEffect>();
      Dictionary<BattleFieldEffectTiming, BL.FieldEffectType> dictionary = new Dictionary<BattleFieldEffectTiming, BL.FieldEffectType>()
      {
        {
          BattleFieldEffectTiming.before_battle,
          BL.FieldEffectType.battle_start
        },
        {
          BattleFieldEffectTiming.first_turn_start,
          BL.FieldEffectType.first_turn_start
        },
        {
          BattleFieldEffectTiming.turn_start,
          BL.FieldEffectType.turn_start
        },
        {
          BattleFieldEffectTiming.player_start,
          BL.FieldEffectType.player_start
        },
        {
          BattleFieldEffectTiming.neutral_start,
          BL.FieldEffectType.neutral_start
        },
        {
          BattleFieldEffectTiming.enemy_start,
          BL.FieldEffectType.enemy_start
        },
        {
          BattleFieldEffectTiming.stageclear,
          BL.FieldEffectType.stageclear
        },
        {
          BattleFieldEffectTiming.pvp_change_player,
          BL.FieldEffectType.pvp_change_player
        },
        {
          BattleFieldEffectTiming.pvp_change_enemy,
          BL.FieldEffectType.pvp_change_enemy
        }
      };
      foreach (BattleFieldEffectStage fieldEffectStage in stage.FieldEffectStages)
        v.Add(new BL.FieldEffect(fieldEffectStage.field_effect.ID, dictionary[fieldEffectStage.timing]));
      if (env.fieldEffectList == null)
        env.fieldEffectList = new BL.ClassValue<List<BL.FieldEffect>>(v);
      else
        env.fieldEffectList.value = v;
    }

    private BL.UnitPosition checkUnitPosition(BL.Unit u, BL env)
    {
      BL.UnitPosition unitPosition = (BL.UnitPosition) null;
      if (env.unitPositions != null && env.unitPositions.value != null)
        unitPosition = env.getUnitPosition(u);
      return unitPosition ?? new BL.UnitPosition();
    }

    private Tuple<List<BL.UnitPosition>, int> getGuestPosition(
      BattleInfo info,
      BL env,
      List<BL.Unit> guests,
      int positionId)
    {
      List<BL.UnitPosition> unitPositionList = new List<BL.UnitPosition>();
      if (info.quest_type == CommonQuestType.Earth || info.quest_type == CommonQuestType.EarthExtra)
        unitPositionList.AddRange(guests.Select<BL.Unit, BattleEarthStageGuest, BL.UnitPosition>((IEnumerable<BattleEarthStageGuest>) info.EarthGuests, (Func<BL.Unit, BattleEarthStageGuest, BL.UnitPosition>) ((a, b) =>
        {
          BL.UnitPosition unitPosition = this.checkUnitPosition(a, env);
          unitPosition.id = positionId++;
          unitPosition.unit = a;
          unitPosition.row = b.initial_coordinate_y - 1;
          unitPosition.column = b.initial_coordinate_x - 1;
          unitPosition.direction = b.initial_direction;
          unitPosition.resetOriginalPosition(env, false);
          return unitPosition;
        })).Where<BL.UnitPosition>((Func<BL.UnitPosition, bool>) (a => a != null)));
      else
        unitPositionList.AddRange(guests.Select<BL.Unit, BattleStageGuest, BL.UnitPosition>((IEnumerable<BattleStageGuest>) info.Guests, (Func<BL.Unit, BattleStageGuest, BL.UnitPosition>) ((a, b) =>
        {
          BL.UnitPosition unitPosition = this.checkUnitPosition(a, env);
          unitPosition.id = positionId++;
          unitPosition.unit = a;
          unitPosition.row = b.initial_coordinate_y(info.stageId) - 1;
          unitPosition.column = b.initial_coordinate_x(info.stageId) - 1;
          unitPosition.direction = b.initial_direction(info.stageId);
          unitPosition.resetOriginalPosition(env, false);
          return unitPosition;
        })).Where<BL.UnitPosition>((Func<BL.UnitPosition, bool>) (a => a != null)));
      return new Tuple<List<BL.UnitPosition>, int>(unitPositionList, positionId);
    }

    private IEnumerator createUnitPositions(
      BattleInfo info,
      BL env,
      List<BL.Unit> players,
      List<BL.Unit> guests,
      List<BL.Unit> friend,
      List<BL.Unit> enemies,
      List<BL.Unit> userEnemies,
      List<BL.Unit> facilities)
    {
      int unitPositionId = 0;
      BattleStage stage = info.stage;
      KeyValuePair<int, BattleStagePlayer>[] stagePlayers = MasterData.BattleStagePlayer.Where<KeyValuePair<int, BattleStagePlayer>>((Func<KeyValuePair<int, BattleStagePlayer>, bool>) (x => x.Value.stage_BattleStage == info.stageId)).ToArray<KeyValuePair<int, BattleStagePlayer>>();
      List<BL.UnitPosition> list1 = players.Select<BL.Unit, BattleStagePlayer, BL.UnitPosition>((IEnumerable<BattleStagePlayer>) stage.Players, (Func<BL.Unit, BattleStagePlayer, BL.UnitPosition>) ((a, b) =>
      {
        if (a == (BL.Unit) null)
          return (BL.UnitPosition) null;
        BL.UnitPosition unitPosition = this.checkUnitPosition(a, env);
        unitPosition.id = unitPositionId++;
        unitPosition.unit = a;
        unitPosition.row = stagePlayers[a.index].Value.initial_coordinate_y - 1;
        unitPosition.column = stagePlayers[a.index].Value.initial_coordinate_x - 1;
        unitPosition.direction = stagePlayers[a.index].Value.initial_direction;
        unitPosition.resetOriginalPosition(env, false);
        return unitPosition;
      })).Where<BL.UnitPosition>((Func<BL.UnitPosition, bool>) (a => a != null)).ToList<BL.UnitPosition>();
      Tuple<List<BL.UnitPosition>, int> guestPosition = this.getGuestPosition(info, env, guests, unitPositionId);
      List<BL.UnitPosition> unitPositionList1 = guestPosition.Item1;
      unitPositionId = guestPosition.Item2;
      List<BL.UnitPosition> unitPositionList2 = new List<BL.UnitPosition>();
      if (friend != null && friend.Count > 0)
      {
        BattleStagePlayer battleStagePlayer = MasterData.BattleStagePlayer.FirstOrDefault<KeyValuePair<int, BattleStagePlayer>>((Func<KeyValuePair<int, BattleStagePlayer>, bool>) (x => x.Value.stage.ID == info.stageId && x.Value.deck_position == Consts.GetInstance().DECK_POSITION_FRIEND)).Value;
        if (battleStagePlayer != null)
        {
          BL.UnitPosition unitPosition = this.checkUnitPosition(friend[0], env);
          unitPosition.id = unitPositionId++;
          unitPosition.unit = friend[0];
          unitPosition.row = battleStagePlayer.initial_coordinate_y - 1;
          unitPosition.column = battleStagePlayer.initial_coordinate_x - 1;
          unitPosition.direction = battleStagePlayer.initial_direction;
          unitPosition.resetOriginalPosition(env, false);
          unitPositionList2.Add(unitPosition);
        }
      }
      List<BL.UnitPosition> list2 = enemies.Select<BL.Unit, BattleStageEnemy, BL.UnitPosition>((IEnumerable<BattleStageEnemy>) info.Enemies, (Func<BL.Unit, BattleStageEnemy, BL.UnitPosition>) ((a, b) =>
      {
        BL.UnitPosition unitPosition = this.checkUnitPosition(a, env);
        unitPosition.id = unitPositionId++;
        unitPosition.unit = a;
        unitPosition.row = b.initial_coordinate_y - 1;
        unitPosition.column = b.initial_coordinate_x - 1;
        unitPosition.direction = b.initial_direction;
        unitPosition.resetOriginalPosition(env, false);
        return unitPosition;
      })).ToList<BL.UnitPosition>();
      List<BL.UnitPosition> list3 = userEnemies.Select<BL.Unit, BattleStageUserUnit, BL.UnitPosition>((IEnumerable<BattleStageUserUnit>) info.UserEnemies, (Func<BL.Unit, BattleStageUserUnit, BL.UnitPosition>) ((a, b) =>
      {
        BL.UnitPosition unitPosition = this.checkUnitPosition(a, env);
        unitPosition.id = unitPositionId++;
        unitPosition.unit = a;
        unitPosition.row = b.initial_coordinate_y - 1;
        unitPosition.column = b.initial_coordinate_x - 1;
        unitPosition.direction = b.initial_direction;
        unitPosition.resetOriginalPosition(env, false);
        return unitPosition;
      })).ToList<BL.UnitPosition>();
      List<BL.UnitPosition> list4 = facilities.Select<BL.Unit, Tuple<int, int>, BL.UnitPosition>((IEnumerable<Tuple<int, int>>) info.facility_coordinates, (Func<BL.Unit, Tuple<int, int>, BL.UnitPosition>) ((a, b) =>
      {
        BL.UnitPosition unitPosition = this.checkUnitPosition(a, env);
        unitPosition.id = unitPositionId++;
        unitPosition.unit = a;
        unitPosition.row = b.Item2 - 1;
        unitPosition.column = b.Item1 - 1;
        unitPosition.resetOriginalPosition(env, false);
        return unitPosition;
      })).ToList<BL.UnitPosition>();
      List<BL.UnitPosition> unitPositionList3 = unitPositionList1;
      List<BL.UnitPosition> list5 = list1.Concat<BL.UnitPosition>((IEnumerable<BL.UnitPosition>) unitPositionList3).Concat<BL.UnitPosition>((IEnumerable<BL.UnitPosition>) unitPositionList2).Concat<BL.UnitPosition>((IEnumerable<BL.UnitPosition>) list2).Concat<BL.UnitPosition>((IEnumerable<BL.UnitPosition>) list3).Concat<BL.UnitPosition>((IEnumerable<BL.UnitPosition>) list4).ToList<BL.UnitPosition>();
      if (env.unitPositions == null)
        env.unitPositions = new BL.ClassValue<List<BL.UnitPosition>>(list5);
      else
        env.unitPositions.value = list5;
      NGBattleAIManager manager = Singleton<NGBattleManager>.GetInstance().getManager<NGBattleAIManager>();
      if ((UnityEngine.Object) manager != (UnityEngine.Object) null)
      {
        if (manager.ai is LispAILogic ai)
        {
          foreach (BL.UnitPosition unitPosition in env.unitPositions.value)
          {
            IEnumerator e = unitPosition.unit.InitAIExtention(ai, (byte[]) null);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
          }
        }
        ai = (LispAILogic) null;
      }
    }

    private void setRangeSkills(
      List<BL.Unit> units,
      List<BL.Unit> targets,
      bool ownOnly,
      bool targetOnly,
      BL.Unit unit,
      BattleskillSkill skill,
      int level,
      int gearIndex = 0)
    {
      if (!targetOnly && skill.target_type == BattleskillTargetType.myself)
      {
        foreach (BattleskillEffect effect in skill.Effects)
          unit.skillEffects.Add(BL.SkillEffect.FromMasterData(unit, effect, skill, level, true, gearIndex), new bool?(), (BL.ISkillEffectListUnit) unit);
      }
      else if (!targetOnly && (skill.target_type == BattleskillTargetType.player_single || skill.target_type == BattleskillTargetType.player_range))
      {
        foreach (BL.Unit unit1 in units)
        {
          foreach (BattleskillEffect effect in skill.Effects)
            unit1.skillEffects.Add(BL.SkillEffect.FromMasterData(unit, effect, skill, level, true, gearIndex), new bool?(), (BL.ISkillEffectListUnit) unit1);
        }
      }
      else if (!ownOnly && (skill.target_type == BattleskillTargetType.enemy_single || skill.target_type == BattleskillTargetType.enemy_range))
      {
        foreach (BL.Unit target in targets)
        {
          foreach (BattleskillEffect effect in skill.Effects)
            target.skillEffects.Add(BL.SkillEffect.FromMasterData(unit, effect, skill, level, true, gearIndex), new bool?(), (BL.ISkillEffectListUnit) target);
        }
      }
      else
      {
        if (skill.target_type != BattleskillTargetType.complex_single && skill.target_type != BattleskillTargetType.complex_range)
          return;
        foreach (BattleskillEffect effect in skill.Effects)
        {
          if (effect.is_targer_enemy)
          {
            if (!ownOnly)
            {
              foreach (BL.Unit target in targets)
                target.skillEffects.Add(BL.SkillEffect.FromMasterData(unit, effect, skill, level, true, gearIndex), new bool?(), (BL.ISkillEffectListUnit) target);
            }
          }
          else if (!targetOnly)
          {
            foreach (BL.Unit unit1 in units)
              unit1.skillEffects.Add(BL.SkillEffect.FromMasterData(unit, effect, skill, level, true, gearIndex), new bool?(), (BL.ISkillEffectListUnit) unit1);
          }
        }
      }
    }

    private void setLeaderSkills(
      List<BL.Unit> units,
      List<BL.Unit> targets,
      bool ownOnly = false,
      bool targetOnly = false)
    {
      foreach (BL.Unit unit in units)
      {
        if (unit.is_leader || unit.friend || unit.playerUnit.is_enemy_leader || unit.playerUnit.is_gesut && unit.is_helper)
        {
          foreach (PlayerUnitLeader_skills leaderSkill in unit.playerUnit.leader_skills)
            this.setRangeSkills(units, targets, ownOnly, targetOnly, unit, leaderSkill.skill, leaderSkill.level, 0);
        }
      }
    }

    private void setPassiveSkillsCore(
      BL.Unit unit,
      List<BL.Unit> units,
      List<BL.Unit> targets,
      bool ownOnly,
      bool targetOnly)
    {
      foreach (PlayerUnitSkills passiveSkill in unit.playerUnit.passiveSkills)
      {
        if (!passiveSkill.skill.range_effect_passive_skill)
        {
          if (!targetOnly)
          {
            foreach (BattleskillEffect effect in passiveSkill.skill.Effects)
              unit.skillEffects.Add(BL.SkillEffect.FromMasterData(effect, passiveSkill.skill, passiveSkill.level, true, 0, (BL.Unit) null, 0, false, false, 0), new bool?(), (BL.ISkillEffectListUnit) unit);
          }
        }
        else
          this.setRangeSkills(units, targets, ownOnly, targetOnly, unit, passiveSkill.skill, passiveSkill.level, 0);
      }
    }

    private void setPassiveSkills(
      List<BL.Unit> units,
      List<BL.Unit> targets,
      bool ownOnly = false,
      bool targetOnly = false)
    {
      foreach (BL.Unit unit in units)
        this.setPassiveSkillsCore(unit, units, targets, ownOnly, targetOnly);
    }

    private void setGearSkills(
      List<BL.Unit> units,
      List<BL.Unit> targets,
      bool ownOnly = false,
      bool targetOnly = false)
    {
      System.Action<BL.Unit, PlayerItem, int> action = (System.Action<BL.Unit, PlayerItem, int>) ((unit, gear, gearIndex) =>
      {
        ((IEnumerable<GearGearSkill>) gear.skills).ToList<GearGearSkill>();
        foreach (GearGearSkill gearGearSkill in ((IEnumerable<GearGearSkill>) gear.skills).Where<GearGearSkill>((Func<GearGearSkill, bool>) (v => v.skill.skill_type == BattleskillSkillType.passive)).ToArray<GearGearSkill>())
        {
          if (!gearGearSkill.skill.range_effect_passive_skill)
          {
            if (!targetOnly)
            {
              foreach (BattleskillEffect effect in gearGearSkill.skill.Effects)
                unit.skillEffects.Add(BL.SkillEffect.FromMasterData(effect, gearGearSkill.skill, gearGearSkill.skill_level, true, gearIndex, (BL.Unit) null, 0, false, false, 0), new bool?(), (BL.ISkillEffectListUnit) unit);
            }
          }
          else
            this.setRangeSkills(units, targets, ownOnly, targetOnly, unit, gearGearSkill.skill, gearGearSkill.skill_level, gearIndex);
        }
      });
      foreach (BL.Unit unit in units)
      {
        PlayerItem equippedGear = unit.playerUnit.equippedGear;
        if (equippedGear != (PlayerItem) null)
          action(unit, equippedGear, 1);
        PlayerItem equippedGear2 = unit.playerUnit.equippedGear2;
        if (equippedGear2 != (PlayerItem) null)
          action(unit, equippedGear2, 2);
        PlayerItem equippedReisou = unit.playerUnit.equippedReisou;
        if (equippedReisou != (PlayerItem) null)
          action(unit, equippedReisou, 1);
        PlayerItem equippedReisou2 = unit.playerUnit.equippedReisou2;
        if (equippedReisou2 != (PlayerItem) null)
          action(unit, equippedReisou2, 2);
      }
    }

    private void setAwakeSkills(
      List<BL.Unit> units,
      List<BL.Unit> targets,
      bool ownOnly = false,
      bool targetOnly = false)
    {
      foreach (BL.Unit unit in units)
      {
        PlayerAwakeSkill[] playerAwakeSkillArray;
        if (unit.playerUnit.equippedExtraSkill == null)
          playerAwakeSkillArray = new PlayerAwakeSkill[0];
        else
          playerAwakeSkillArray = new PlayerAwakeSkill[1]
          {
            unit.playerUnit.equippedExtraSkill
          };
        foreach (PlayerAwakeSkill playerAwakeSkill in ((IEnumerable<PlayerAwakeSkill>) playerAwakeSkillArray).Where<PlayerAwakeSkill>((Func<PlayerAwakeSkill, bool>) (v => v.masterData.skill_type == BattleskillSkillType.passive)).ToArray<PlayerAwakeSkill>())
        {
          if (!playerAwakeSkill.masterData.range_effect_passive_skill)
          {
            if (!targetOnly)
            {
              foreach (BattleskillEffect effect in playerAwakeSkill.masterData.Effects)
                unit.skillEffects.Add(BL.SkillEffect.FromMasterData(effect, playerAwakeSkill.masterData, playerAwakeSkill.level, true, 0, (BL.Unit) null, 0, false, false, 0), new bool?(), (BL.ISkillEffectListUnit) unit);
            }
          }
          else
            this.setRangeSkills(units, targets, ownOnly, targetOnly, unit, playerAwakeSkill.masterData, playerAwakeSkill.level, 0);
        }
      }
    }

    private void setFacilitySkills(
      List<BL.Unit> facilities,
      BL env,
      bool ownOnly = false,
      bool targetOnly = false)
    {
      foreach (BL.Unit facility in facilities)
      {
        switch (env.getForceID(facility))
        {
          case BL.ForceID.player:
            this.setPassiveSkillsCore(facility, env.playerUnits.value, env.enemyUnits.value, ownOnly, targetOnly);
            continue;
          case BL.ForceID.enemy:
            this.setPassiveSkillsCore(facility, env.enemyUnits.value, env.playerUnits.value, ownOnly, targetOnly);
            continue;
          default:
            continue;
        }
      }
    }

    private void setBonusSkills(List<BL.Unit> units, BattleInfo info)
    {
      foreach (BL.Unit unit in units)
      {
        foreach (Bonus pvpBonus in info.pvp_bonus_list)
        {
          if (Bonus.IsEnableBonus(unit, pvpBonus, info.pvp_start_date))
          {
            foreach (BattleskillEffect effect in pvpBonus.skill.Effects)
              unit.skillEffects.Add(BL.SkillEffect.FromMasterData(effect, pvpBonus.skill, 1, true, 0, (BL.Unit) null, 0, false, false, 0), new bool?(), (BL.ISkillEffectListUnit) unit);
          }
        }
      }
    }

    private void setGvgBonusSkills(List<BL.Unit> units, GuildBaseBonusEffect[] bonus_list)
    {
      foreach (BL.Unit unit in units)
      {
        foreach (GuildBaseBonusEffect bonus in bonus_list)
        {
          foreach (BattleskillEffect effect in bonus.skill.Effects)
            unit.skillEffects.Add(BL.SkillEffect.FromMasterData(effect, bonus.skill, 1, true, 0, (BL.Unit) null, 0, false, false, 0), new bool?(), (BL.ISkillEffectListUnit) unit);
        }
      }
    }

    private void setUnitInitialHp(
      BattleInfo battleInfo,
      BL.ClassValue<List<BL.Unit>> units,
      bool isFacility = false)
    {
      foreach (BL.Unit unit in units.value)
      {
        int hp = unit.parameter.Hp;
        unit.hp = battleInfo.quest_type != CommonQuestType.Tower ? hp : TowerUtil.GetHp(hp, unit.playerUnit.TowerHpRate);
        unit.initialHp = unit.hp;
        if (!isFacility)
        {
          foreach (BL.MagicBullet magicBullet in unit.magicBullets)
            magicBullet.setAdditionalCost(hp);
          unit.checkActionRangeBySetHp = true;
        }
      }
    }

    private IEnumerator initializeData(BattleInfo battleInfo, BL env)
    {
      env.nextRandom();
      if (battleInfo.isWave)
        battleInfo.currentWave = env.currentWave;
      IEnumerator e = Singleton<NGBattleManager>.GetInstance().initMasterData(battleInfo);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.initializeStage(battleInfo, env);
      List<BL.Unit> players = new List<BL.Unit>();
      List<BL.Unit> guests = new List<BL.Unit>();
      List<BL.Unit> friend = new List<BL.Unit>();
      List<BL.Unit> enemies = new List<BL.Unit>();
      List<BL.Unit> userEnemies = new List<BL.Unit>();
      int playerCnt;
      Future<BL.Unit> future;
      if (!battleInfo.pvp && !battleInfo.gvg)
      {
        bool helperOverrWriteGuest = false;
        playerCnt = battleInfo.deck.player_units.Length;
        List<int> activeDeckPosition = new List<int>();
        for (int index = 0; index < playerCnt; ++index)
          activeDeckPosition.Add(1 + index);
        if (battleInfo.quest_type != CommonQuestType.Earth && battleInfo.quest_type != CommonQuestType.EarthExtra)
        {
          activeDeckPosition = battleInfo.quest_type != CommonQuestType.Tower ? MasterData.BattleStagePlayer.Where<KeyValuePair<int, BattleStagePlayer>>((Func<KeyValuePair<int, BattleStagePlayer>, bool>) (x => x.Value.stage_BattleStage == battleInfo.stageId && x.Value.deck_position != Consts.GetInstance().DECK_POSITION_FRIEND)).Select<KeyValuePair<int, BattleStagePlayer>, int>((Func<KeyValuePair<int, BattleStagePlayer>, int>) (x => x.Value.deck_position)).ToList<int>() : MasterData.BattleStagePlayer.Where<KeyValuePair<int, BattleStagePlayer>>((Func<KeyValuePair<int, BattleStagePlayer>, bool>) (x => x.Value.stage_BattleStage == battleInfo.stageId)).Select<KeyValuePair<int, BattleStagePlayer>, int>((Func<KeyValuePair<int, BattleStagePlayer>, int>) (x => x.Value.deck_position)).ToList<int>();
          activeDeckPosition = activeDeckPosition.Except<int>((IEnumerable<int>) ((IEnumerable<BattleStageGuest>) battleInfo.Guests).Select<BattleStageGuest, int>((Func<BattleStageGuest, int>) (x => x.deck_position)).ToList<int>()).ToList<int>();
          playerCnt = activeDeckPosition.Count;
        }
        PlayerCharacterIntimate[] characterIntimate = SMManager.Get<PlayerCharacterIntimate[]>();
        PlayerUnit[] myDeckUnit = playerCnt > 0 ? battleInfo.deck.player_units : (PlayerUnit[]) null;
        int i;
        for (i = 0; i < playerCnt; ++i)
        {
          PlayerUnit pu = myDeckUnit[activeDeckPosition[i] - 1];
          if (pu != (PlayerUnit) null)
          {
            pu.SetIntimateList(characterIntimate);
            future = this.createUnitByPlayerUnit(pu, activeDeckPosition[i] - 1, false);
            e = future.Wait();
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            players.Add(future.Result);
            future = (Future<BL.Unit>) null;
          }
          else
            players.Add((BL.Unit) null);
        }
        if (battleInfo.quest_type == CommonQuestType.Earth || battleInfo.quest_type == CommonQuestType.EarthExtra)
        {
          for (i = 0; i < battleInfo.EarthGuests.Length; ++i)
          {
            PlayerUnit pu = PlayerUnit.FromGuest(battleInfo.EarthGuests[i]);
            pu.SetIntimateList(characterIntimate);
            future = this.createUnitByPlayerUnit(pu, players.Count + i, false);
            e = future.Wait();
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            guests.Add(future.Result);
            future = (Future<BL.Unit>) null;
          }
        }
        else
        {
          for (i = 0; i < battleInfo.Guests.Length; ++i)
          {
            BattleStageGuest guest = battleInfo.Guests[i];
            PlayerUnit pu = PlayerUnit.FromGuest(guest);
            pu.SetIntimateList(characterIntimate);
            future = this.createUnitByPlayerUnit(pu, guest.deck_position - 1, false);
            e = future.Wait();
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            guests.Add(future.Result);
            if (guest.deck_position == Consts.GetInstance().DECK_POSITION_FRIEND)
              helperOverrWriteGuest = true;
            guest = (BattleStageGuest) null;
            future = (Future<BL.Unit>) null;
          }
        }
        if (!helperOverrWriteGuest && battleInfo.helper != null)
        {
          PlayerUnit leaderUnit = battleInfo.helper.leader_unit;
          leaderUnit.SetIntimateList(characterIntimate);
          leaderUnit.importOverkillersUnits(battleInfo.helper_overkillers, false);
          future = this.createUnitByPlayerUnit(leaderUnit, Consts.GetInstance().DECK_POSITION_FRIEND - 1, battleInfo.helper.enableLeaderSkill());
          e = future.Wait();
          while (e.MoveNext())
            yield return e.Current;
          e = (IEnumerator) null;
          friend.Add(future.Result);
          future = (Future<BL.Unit>) null;
        }
        e = this.createEnemyUnits(battleInfo, enemies, userEnemies, env);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        env.playerUnits = new BL.ClassValue<List<BL.Unit>>(players.Where<BL.Unit>((Func<BL.Unit, bool>) (pu => pu != (BL.Unit) null)).Concat<BL.Unit>((IEnumerable<BL.Unit>) guests).Concat<BL.Unit>((IEnumerable<BL.Unit>) friend).OrderBy<BL.Unit, int>((Func<BL.Unit, int>) (x => x.index)).ToList<BL.Unit>());
        env.neutralUnits = new BL.ClassValue<List<BL.Unit>>(new List<BL.Unit>());
        env.enemyUnits = new BL.ClassValue<List<BL.Unit>>(enemies.Concat<BL.Unit>((IEnumerable<BL.Unit>) userEnemies).ToList<BL.Unit>());
        activeDeckPosition = (List<int>) null;
        characterIntimate = (PlayerCharacterIntimate[]) null;
        myDeckUnit = (PlayerUnit[]) null;
      }
      else
      {
        for (playerCnt = 0; playerCnt < battleInfo.pvp_player_units.Length; ++playerCnt)
        {
          PlayerUnit pvpPlayerUnit = battleInfo.pvp_player_units[playerCnt];
          if (pvpPlayerUnit != (PlayerUnit) null)
          {
            pvpPlayerUnit.SetIntimateList(battleInfo.pvp_player_character_intimates);
            pvpPlayerUnit.primary_equipped_gear = pvpPlayerUnit.FindEquippedGear(battleInfo.pvp_player_items);
            pvpPlayerUnit.primary_equipped_gear2 = pvpPlayerUnit.FindEquippedGear2(battleInfo.pvp_player_items);
            pvpPlayerUnit.primary_equipped_reisou = pvpPlayerUnit.FindEquippedReisou(battleInfo.pvp_player_items, battleInfo.pvp_player_reisou_items);
            pvpPlayerUnit.primary_equipped_reisou2 = pvpPlayerUnit.FindEquippedReisou2(battleInfo.pvp_player_items, battleInfo.pvp_player_reisou_items);
            pvpPlayerUnit.primary_equipped_awake_skill = pvpPlayerUnit.FindEquippedExtraSkill(battleInfo.pvp_player_awake_skill);
            future = this.createUnitByPlayerUnit(pvpPlayerUnit, playerCnt, false);
            e = future.Wait();
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            players.Add(future.Result);
            future = (Future<BL.Unit>) null;
          }
          else
            players.Add((BL.Unit) null);
        }
        if (battleInfo.gvg_player_helpers != null && battleInfo.gvg_player_helpers.Length != 0)
        {
          PlayerUnit gvgPlayerHelper = battleInfo.gvg_player_helpers[0];
          if (gvgPlayerHelper != (PlayerUnit) null)
          {
            gvgPlayerHelper.SetIntimateList(new PlayerCharacterIntimate[0]);
            gvgPlayerHelper.primary_equipped_gear = gvgPlayerHelper.FindEquippedGear(battleInfo.gvg_player_helper_items);
            gvgPlayerHelper.primary_equipped_gear2 = gvgPlayerHelper.FindEquippedGear2(battleInfo.gvg_player_helper_items);
            gvgPlayerHelper.primary_equipped_reisou = gvgPlayerHelper.FindEquippedReisou(battleInfo.gvg_player_helper_items, battleInfo.gvg_player_helper_reisou_items);
            gvgPlayerHelper.primary_equipped_reisou2 = gvgPlayerHelper.FindEquippedReisou2(battleInfo.gvg_player_helper_items, battleInfo.gvg_player_helper_reisou_items);
            gvgPlayerHelper.primary_equipped_awake_skill = gvgPlayerHelper.FindEquippedExtraSkill(battleInfo.gvg_player_helper_awake_skill);
            future = this.createUnitByPlayerUnit(gvgPlayerHelper, Consts.GetInstance().DECK_POSITION_FRIEND - 1, true);
            e = future.Wait();
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            players.Add(future.Result);
            future = (Future<BL.Unit>) null;
          }
          else
            players.Add((BL.Unit) null);
        }
        for (playerCnt = 0; playerCnt < battleInfo.pvp_enemy_units.Length; ++playerCnt)
        {
          PlayerUnit pvpEnemyUnit = battleInfo.pvp_enemy_units[playerCnt];
          if (pvpEnemyUnit != (PlayerUnit) null)
          {
            pvpEnemyUnit.SetIntimateList(battleInfo.pvp_enemy_character_intimates);
            pvpEnemyUnit.primary_equipped_gear = pvpEnemyUnit.FindEquippedGear(battleInfo.pvp_enemy_items);
            pvpEnemyUnit.primary_equipped_gear2 = pvpEnemyUnit.FindEquippedGear2(battleInfo.pvp_enemy_items);
            pvpEnemyUnit.primary_equipped_reisou = pvpEnemyUnit.FindEquippedReisou(battleInfo.pvp_enemy_items, battleInfo.pvp_enemy_reisou_items);
            pvpEnemyUnit.primary_equipped_reisou2 = pvpEnemyUnit.FindEquippedReisou2(battleInfo.pvp_enemy_items, battleInfo.pvp_enemy_reisou_items);
            pvpEnemyUnit.primary_equipped_awake_skill = pvpEnemyUnit.FindEquippedExtraSkill(battleInfo.pvp_enemy_awake_skill);
            future = this.createUnitByPlayerUnit(pvpEnemyUnit, playerCnt, false);
            e = future.Wait();
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            enemies.Add(future.Result);
            future = (Future<BL.Unit>) null;
          }
          else
            enemies.Add((BL.Unit) null);
        }
        if (battleInfo.gvg_enemy_helpers != null && battleInfo.gvg_enemy_helpers.Length != 0)
        {
          PlayerUnit gvgEnemyHelper = battleInfo.gvg_enemy_helpers[0];
          if (gvgEnemyHelper != (PlayerUnit) null)
          {
            gvgEnemyHelper.SetIntimateList(new PlayerCharacterIntimate[0]);
            gvgEnemyHelper.primary_equipped_gear = gvgEnemyHelper.FindEquippedGear(battleInfo.gvg_enemy_helper_items);
            gvgEnemyHelper.primary_equipped_gear2 = gvgEnemyHelper.FindEquippedGear2(battleInfo.gvg_enemy_helper_items);
            gvgEnemyHelper.primary_equipped_reisou = gvgEnemyHelper.FindEquippedReisou(battleInfo.gvg_enemy_helper_items, battleInfo.gvg_enemy_helper_reisou_items);
            gvgEnemyHelper.primary_equipped_reisou2 = gvgEnemyHelper.FindEquippedReisou2(battleInfo.gvg_enemy_helper_items, battleInfo.gvg_enemy_helper_reisou_items);
            gvgEnemyHelper.primary_equipped_awake_skill = gvgEnemyHelper.FindEquippedExtraSkill(battleInfo.gvg_enemy_helper_awake_skill);
            future = this.createUnitByPlayerUnit(gvgEnemyHelper, Consts.GetInstance().DECK_POSITION_FRIEND - 1, true);
            e = future.Wait();
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            enemies.Add(future.Result);
            future = (Future<BL.Unit>) null;
          }
          else
            enemies.Add((BL.Unit) null);
        }
        env.playerUnits = new BL.ClassValue<List<BL.Unit>>(players.Where<BL.Unit>((Func<BL.Unit, bool>) (pu => pu != (BL.Unit) null)).ToList<BL.Unit>());
        env.neutralUnits = new BL.ClassValue<List<BL.Unit>>(new List<BL.Unit>());
        env.enemyUnits = new BL.ClassValue<List<BL.Unit>>(enemies.Where<BL.Unit>((Func<BL.Unit, bool>) (pu => pu != (BL.Unit) null)).ToList<BL.Unit>());
      }
      List<BL.Unit> facilities = new List<BL.Unit>();
      e = this.createFacilityUnits(battleInfo, facilities, env);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      env.facilityUnits = new BL.ClassValue<List<BL.Unit>>(facilities);
      this.setLeaderSkills(env.playerUnits.value, env.enemyUnits.value, false, false);
      this.setLeaderSkills(env.enemyUnits.value, env.playerUnits.value, false, false);
      this.setAwakeSkills(env.playerUnits.value, env.enemyUnits.value, false, false);
      this.setAwakeSkills(env.enemyUnits.value, env.playerUnits.value, false, false);
      this.setPassiveSkills(env.playerUnits.value, env.enemyUnits.value, false, false);
      this.setPassiveSkills(env.enemyUnits.value, env.playerUnits.value, false, false);
      this.setGearSkills(env.playerUnits.value, env.enemyUnits.value, false, false);
      this.setGearSkills(env.enemyUnits.value, env.playerUnits.value, false, false);
      this.setFacilitySkills(env.facilityUnits.value, env, false, false);
      if (battleInfo.pvp && battleInfo.pvp_bonus_list != null)
      {
        this.setBonusSkills(env.playerUnits.value, battleInfo);
        this.setBonusSkills(env.enemyUnits.value, battleInfo);
      }
      if (battleInfo.gvg)
      {
        if (battleInfo.gvg_player_base_bonus_list != null)
          this.setGvgBonusSkills(env.playerUnits.value, battleInfo.gvg_player_base_bonus_list);
        if (battleInfo.gvg_enemy_base_bonus_list != null)
          this.setGvgBonusSkills(env.enemyUnits.value, battleInfo.gvg_enemy_base_bonus_list);
      }
      this.setUnitInitialHp(battleInfo, env.playerUnits, false);
      this.setUnitInitialHp(battleInfo, env.neutralUnits, false);
      this.setUnitInitialHp(battleInfo, env.enemyUnits, false);
      this.setUnitInitialHp(battleInfo, env.facilityUnits, true);
      if (battleInfo.quest_type == CommonQuestType.GuildRaid)
      {
        BL.Unit unit = env.enemyUnits.value.First<BL.Unit>((Func<BL.Unit, bool>) (u => u.playerUnit.id == battleInfo.raidBossDamage.Key));
        unit.hp -= battleInfo.raidBossDamage.Value;
        unit.initialHp = unit.hp;
      }
      foreach (BL.ISkillEffectListUnit beUnit in env.playerUnits.value)
        BattleFuncs.createBattleSkillEffectParams(beUnit, (IEnumerable<BL.Unit>) env.playerUnits.value);
      foreach (BL.ISkillEffectListUnit beUnit in env.neutralUnits.value)
        BattleFuncs.createBattleSkillEffectParams(beUnit, (IEnumerable<BL.Unit>) env.neutralUnits.value);
      foreach (BL.ISkillEffectListUnit beUnit in env.enemyUnits.value)
        BattleFuncs.createBattleSkillEffectParams(beUnit, (IEnumerable<BL.Unit>) env.enemyUnits.value);
      if (!battleInfo.pvp && !battleInfo.gvg)
      {
        e = this.createUnitPositions(battleInfo, env, players, guests, friend, enemies, userEnemies, facilities);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
      }
      else
      {
        int unitPositionId = 0;
        BattleStage stage = battleInfo.stage;
        List<BL.UnitPosition> first;
        List<BL.UnitPosition> unitPositionList;
        if (battleInfo.pvp)
        {
          first = players.Select<BL.Unit, PvpStageFormation, BL.UnitPosition>(((IEnumerable<PvpStageFormation>) MasterData.PvpStageFormationList).Where<PvpStageFormation>((Func<PvpStageFormation, bool>) (x => x.stage.ID == stage.ID && x.player_order == 0)), (Func<BL.Unit, PvpStageFormation, BL.UnitPosition>) ((a, b) =>
          {
            if (a == (BL.Unit) null)
              return (BL.UnitPosition) null;
            BL.UnitPosition unitPosition = new BL.UnitPosition();
            unitPosition.id = unitPositionId++;
            unitPosition.unit = a;
            unitPosition.row = b.formation_y - 1;
            unitPosition.column = b.formation_x - 1;
            unitPosition.direction = b.initial_direction;
            unitPosition.resetOriginalPosition(env, false);
            return unitPosition;
          })).Where<BL.UnitPosition>((Func<BL.UnitPosition, bool>) (a => a != null)).ToList<BL.UnitPosition>();
          unitPositionList = enemies.Select<BL.Unit, PvpStageFormation, BL.UnitPosition>(((IEnumerable<PvpStageFormation>) MasterData.PvpStageFormationList).Where<PvpStageFormation>((Func<PvpStageFormation, bool>) (x => x.stage.ID == stage.ID && x.player_order == 1)), (Func<BL.Unit, PvpStageFormation, BL.UnitPosition>) ((a, b) =>
          {
            if (a == (BL.Unit) null)
              return (BL.UnitPosition) null;
            BL.UnitPosition unitPosition = new BL.UnitPosition();
            unitPosition.id = unitPositionId++;
            unitPosition.unit = a;
            unitPosition.row = b.formation_y - 1;
            unitPosition.column = b.formation_x - 1;
            unitPosition.direction = b.initial_direction;
            unitPosition.resetOriginalPosition(env, false);
            return unitPosition;
          })).Where<BL.UnitPosition>((Func<BL.UnitPosition, bool>) (a => a != null)).ToList<BL.UnitPosition>();
        }
        else if (battleInfo.gvg)
        {
          first = players.Select<BL.Unit, GvgStageFormation, BL.UnitPosition>(((IEnumerable<GvgStageFormation>) MasterData.GvgStageFormationList).Where<GvgStageFormation>((Func<GvgStageFormation, bool>) (x => x.stage.ID == stage.ID && x.player_order == 0)), (Func<BL.Unit, GvgStageFormation, BL.UnitPosition>) ((a, b) =>
          {
            if (a == (BL.Unit) null)
              return (BL.UnitPosition) null;
            BL.UnitPosition unitPosition = new BL.UnitPosition();
            unitPosition.id = unitPositionId++;
            unitPosition.unit = a;
            unitPosition.row = b.formation_y - 1;
            unitPosition.column = b.formation_x - 1;
            unitPosition.direction = b.initial_direction;
            unitPosition.resetOriginalPosition(env, false);
            return unitPosition;
          })).Where<BL.UnitPosition>((Func<BL.UnitPosition, bool>) (a => a != null)).ToList<BL.UnitPosition>();
          unitPositionList = enemies.Select<BL.Unit, GvgStageFormation, BL.UnitPosition>(((IEnumerable<GvgStageFormation>) MasterData.GvgStageFormationList).Where<GvgStageFormation>((Func<GvgStageFormation, bool>) (x => x.stage.ID == stage.ID && x.player_order == 1)), (Func<BL.Unit, GvgStageFormation, BL.UnitPosition>) ((a, b) =>
          {
            if (a == (BL.Unit) null)
              return (BL.UnitPosition) null;
            BL.UnitPosition unitPosition = new BL.UnitPosition();
            unitPosition.id = unitPositionId++;
            unitPosition.unit = a;
            unitPosition.row = b.formation_y - 1;
            unitPosition.column = b.formation_x - 1;
            unitPosition.direction = b.initial_direction;
            unitPosition.resetOriginalPosition(env, false);
            return unitPosition;
          })).Where<BL.UnitPosition>((Func<BL.UnitPosition, bool>) (a => a != null)).ToList<BL.UnitPosition>();
        }
        else
          first = unitPositionList = new List<BL.UnitPosition>();
        List<BL.UnitPosition> list1 = facilities.Select<BL.Unit, Tuple<int, int>, BL.UnitPosition>((IEnumerable<Tuple<int, int>>) battleInfo.facility_coordinates, (Func<BL.Unit, Tuple<int, int>, BL.UnitPosition>) ((a, b) =>
        {
          BL.UnitPosition unitPosition = this.checkUnitPosition(a, env);
          unitPosition.id = unitPositionId++;
          unitPosition.unit = a;
          unitPosition.row = b.Item2 - 1;
          unitPosition.column = b.Item1 - 1;
          unitPosition.resetOriginalPosition(env, false);
          return unitPosition;
        })).ToList<BL.UnitPosition>();
        List<BL.UnitPosition> list2 = first.Concat<BL.UnitPosition>((IEnumerable<BL.UnitPosition>) unitPositionList).Concat<BL.UnitPosition>((IEnumerable<BL.UnitPosition>) list1).ToList<BL.UnitPosition>();
        env.unitPositions = new BL.ClassValue<List<BL.UnitPosition>>(list2);
      }
      env.setCurrentUnitWith((BL.Unit) null, (System.Action<BL.UnitPosition>) (_ => {}));
      if (!battleInfo.pvp && !battleInfo.gvg)
      {
        List<BL.Item> list = ((IEnumerable<PlayerItem>) battleInfo.items).Select<PlayerItem, BL.Item>((Func<PlayerItem, BL.Item>) (pi => pi.toBLItem())).ToList<BL.Item>();
        env.itemList = new BL.ClassValue<List<BL.Item>>(list.ToList<BL.Item>());
        env.itemListInBattle = new BL.ClassValue<List<BL.Item>>(list.ToList<BL.Item>());
      }
      else
      {
        env.itemList = new BL.ClassValue<List<BL.Item>>(new List<BL.Item>());
        env.itemListInBattle = new BL.ClassValue<List<BL.Item>>(new List<BL.Item>());
      }
      env.sight.value = 1;
      this.setupStoryScript(battleInfo, env);
      this.createFieldEffects(battleInfo.stage, env);
      if (battleInfo.pvp)
        env.initializeIntimatePvp();
      else
        env.initializeIntimate();
    }

    public IEnumerator doStart(BattleInfo battleInfo, BL env)
    {
      IEnumerator e = this.initializeData(battleInfo, env);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      foreach (BL.Unit unit in env.playerUnits.value)
        unit.isPlayerControl = true;
    }
  }
}
