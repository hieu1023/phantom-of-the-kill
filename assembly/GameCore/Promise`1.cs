﻿// Decompiled with JetBrains decompiler
// Type: GameCore.Promise`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace GameCore
{
  public class Promise<T>
  {
    private bool hasResult;
    private T result;
    private Exception exception;

    public bool HasResult
    {
      get
      {
        return this.hasResult;
      }
    }

    public T Result
    {
      get
      {
        if (!this.hasResult)
          throw new Exception("result or exception is not assigned.");
        return this.result;
      }
      set
      {
        this.hasResult = true;
        this.result = value;
      }
    }

    public Exception Exception
    {
      get
      {
        if (!this.hasResult)
          throw new Exception("result or exception is not assigned.");
        return this.exception;
      }
      set
      {
        if (this.hasResult)
          throw new Exception("result or exception is already assigned.");
        this.hasResult = true;
        this.exception = value;
      }
    }

    public T ResultOrException
    {
      get
      {
        if (!this.hasResult)
          throw new Exception("result is not assigned.");
        if (this.exception != null)
          throw this.exception;
        return this.result;
      }
    }
  }
}
