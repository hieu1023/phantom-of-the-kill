﻿// Decompiled with JetBrains decompiler
// Type: GameCore.AttackStatus
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using System;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

namespace GameCore
{
  [Serializable]
  public class AttackStatus
  {
    public BL.MagicBullet magicBullet;
    public BL.Weapon weapon;
    public CommonElement overwriteElement;
    public List<CommonElement> attackElements;
    public bool isAbsoluteCounterAttack;
    public Judgement.BeforeDuelParameter duelParameter;
    public float elementAttackRate;
    public float elementDamageRate;
    public float attackElementDamageRate;
    public float attackClassificationRate;

    public bool isMagic { get; set; }

    public bool isHeal
    {
      get
      {
        return this.magicBullet != null && this.magicBullet.isHeal;
      }
    }

    public bool isDrain
    {
      get
      {
        return this.magicBullet != null && this.magicBullet.isDrain;
      }
    }

    public float drainRate
    {
      get
      {
        return this.magicBullet == null ? 0.0f : this.magicBullet.drainRate;
      }
    }

    public float attackRate { get; set; }

    public float normalDamageRate { get; set; }

    public int normalAttackCount { get; set; }

    public int attack
    {
      get
      {
        return NC.Clamp(1, int.MaxValue, (int) this.originalAttack);
      }
    }

    public float originalAttack
    {
      get
      {
        return !this.isMagic ? this.duelParameter.CalcPhysicalAttack(this.attackRate) : this.duelParameter.CalcMagicAttack(this.attackRate);
      }
    }

    public int heal_attack
    {
      get
      {
        return ((IEnumerable<BattleskillEffect>) this.magicBullet.skill.Effects).Any<BattleskillEffect>((Func<BattleskillEffect, bool>) (x => x.EffectLogic.Enum == BattleskillEffectLogicEnum.fix_heal)) ? ((IEnumerable<BattleskillEffect>) this.magicBullet.skill.Effects).SingleOrDefault<BattleskillEffect>((Func<BattleskillEffect, bool>) (x => x.EffectLogic.Enum == BattleskillEffectLogicEnum.fix_heal)).GetInt(BattleskillEffectLogicArgumentEnum.value) : NC.Clamp(1, int.MaxValue, this.duelParameter.attackerUnitParameter.MagicAttack);
      }
    }

    public int attackCount
    {
      get
      {
        return this.duelParameter.AttackCount;
      }
    }

    public float hit
    {
      get
      {
        return NC.Clampf(0.0f, 1f, (float) this.duelParameter.DisplayHit / 100f);
      }
    }

    public float critical
    {
      get
      {
        return NC.Clampf(0.0f, 1f, (float) this.duelParameter.DisplayCritical / 100f);
      }
    }

    public int dexerityDisplay()
    {
      return NC.Clamp(0, 100, this.duelParameter.DisplayHit);
    }

    public int criticalDisplay()
    {
      return NC.Clamp(0, 100, this.duelParameter.DisplayCritical);
    }

    public int normalDamage()
    {
      return this.attack * this.attackCount;
    }

    public int expectationDamage()
    {
      return (int) ((double) (this.attack * this.attackCount) * (double) this.hit);
    }

    public void calcElementAttackRate(
      BL.ISkillEffectListUnit attack,
      BL.ISkillEffectListUnit defense)
    {
      Tuple<float, CommonElement?, List<CommonElement>, float, float> elementAttackRate = BattleDuelSkill.getElementAttackRate(attack, this, defense);
      this.elementAttackRate = elementAttackRate.Item1;
      this.overwriteElement = !elementAttackRate.Item2.HasValue ? (CommonElement) 0 : elementAttackRate.Item2.Value;
      this.attackElements = elementAttackRate.Item3;
      this.elementDamageRate = elementAttackRate.Item4;
      this.attackElementDamageRate = elementAttackRate.Item5;
    }

    public CommonElement? GetOverwriteElement()
    {
      return this.overwriteElement == (CommonElement) 0 ? new CommonElement?() : new CommonElement?(this.overwriteElement);
    }

    public List<CommonElement> GetAttackElements()
    {
      return this.attackElements;
    }

    public void calcAttackClassificationRate(
      BL.ISkillEffectListUnit attack,
      BL.ISkillEffectListUnit defense)
    {
      this.attackClassificationRate = BattleFuncs.calcAttackClassificationRate(this, attack, defense);
    }

    public float realHit
    {
      get
      {
        return NC.Clampf(0.0f, 1f, this.hit + Mathf.Sin((float) ((1.0 - (double) this.hit) * 2.0 * 3.14159274101257)) / 10f);
      }
    }

    public bool calcHit(float value)
    {
      return (double) this.realHit >= (double) value;
    }

    public string ShowAttackStatus()
    {
      return string.Format("magic({0}) attack({1}) heal({2}) count({3}) hit({4}) critical({5})", this.magicBullet != null ? (object) (this.magicBullet.name + " : " + (object) this.magicBullet.cost) : (object) "-", (object) this.attack, (object) this.heal_attack, (object) this.attackCount, (object) this.dexerityDisplay(), (object) this.criticalDisplay());
    }
  }
}
