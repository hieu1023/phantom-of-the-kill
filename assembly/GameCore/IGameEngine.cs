﻿// Decompiled with JetBrains decompiler
// Type: GameCore.IGameEngine
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

namespace GameCore
{
  public interface IGameEngine
  {
    bool isDisposition { get; }

    bool isWaitAction { get; }

    int endPoint { get; }

    string playerName { get; }

    string enemyName { get; }

    int playerEmblem { get; }

    int enemyEmblem { get; }

    BL.StructValue<int> remainTurn { get; }

    BL.StructValue<int> timeLimit { get; }

    BL.StructValue<int> playerPoint { get; }

    BL.StructValue<int> enemyPoint { get; }

    BL.StructValue<bool> isPlayerWipedOut { get; }

    BL.StructValue<bool> isEnemyWipedOut { get; }

    HashSet<BL.Panel> formationPanel { get; }

    void startMain();

    void locateUnitsCompleted();

    void turnInitializeCompleted();

    void actionUnitCompleted();

    void wipedOutCompleted();

    void moveUnit(BL.UnitPosition up);

    void moveUnitWithAttack(
      BL.UnitPosition attack,
      BL.UnitPosition defense,
      bool isHeal,
      int attackStatusIndex);

    void moveUnitWithAttack(BL.Unit attack, BL.Unit defense, bool isHeal, int attackStatusIndex);

    void moveUnitWithSkill(
      BL.Unit unit,
      BL.Skill skill,
      List<BL.Unit> targets,
      List<BL.Panel> penels);

    void readyComplited();

    void applyDeadUnit(BL.Unit attack, BL.Unit defense);

    void deadReserveToPoint(bool isEnemy, bool needAnnihilateCheck);
  }
}
