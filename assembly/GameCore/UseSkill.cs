﻿// Decompiled with JetBrains decompiler
// Type: GameCore.UseSkill
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace GameCore
{
  public class UseSkill : INode, IParam<int>
  {
    public readonly Node ID;
    public readonly int iID;

    public UseSkill(Node id)
    {
      this.ID = (Node) null;
      if (!int.TryParse(id.Text, out this.iID))
        return;
      this.ID = id;
    }

    public float Eval(Func<string, float> convert)
    {
      return this.ID == null ? 0.0f : 1f;
    }

    public int getParam(Func<string, float> convert)
    {
      return this.iID;
    }
  }
}
