﻿// Decompiled with JetBrains decompiler
// Type: GameCore.Serialization.EasySerializer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.IO;
using System.Runtime.Serialization;
using UnityEngine;

namespace GameCore.Serialization
{
  public class EasySerializer
  {
    public static byte[] SerializeObjectToMemory(object serializableObject)
    {
      EasySerializer.SetEnvironmentVariables();
      System.Runtime.Serialization.Formatters.Binary.BinaryFormatter binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
      binaryFormatter.Binder = (SerializationBinder) new VersionDeserializationBinder();
      using (MemoryStream memoryStream = new MemoryStream())
      {
        binaryFormatter.Serialize((Stream) memoryStream, serializableObject);
        return memoryStream.GetBuffer();
      }
    }

    public static object DeserializeObjectFromMemory(byte[] buf)
    {
      EasySerializer.SetEnvironmentVariables();
      System.Runtime.Serialization.Formatters.Binary.BinaryFormatter binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
      binaryFormatter.Binder = (SerializationBinder) new VersionDeserializationBinder();
      using (Stream serializationStream = (Stream) new MemoryStream(buf))
        return binaryFormatter.Deserialize(serializationStream);
    }

    public static void SerializeObjectToFile(object serializableObject, string filePath)
    {
      EasySerializer.SetEnvironmentVariables();
      System.Runtime.Serialization.Formatters.Binary.BinaryFormatter binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
      binaryFormatter.Binder = (SerializationBinder) new VersionDeserializationBinder();
      using (Stream serializationStream = (Stream) File.Open(filePath, FileMode.Create))
      {
        try
        {
          binaryFormatter.Serialize(serializationStream, serializableObject);
        }
        catch (Exception ex)
        {
          Debug.LogError((object) ("SerializeObjectToFile_PUNK_DEBUG-14830_=_" + filePath + "_exeption_=_" + (object) ex));
        }
      }
    }

    public static object DeserializeObjectFromFile(string filePath)
    {
      if (!File.Exists(filePath))
        return (object) null;
      EasySerializer.SetEnvironmentVariables();
      System.Runtime.Serialization.Formatters.Binary.BinaryFormatter binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
      binaryFormatter.Binder = (SerializationBinder) new VersionDeserializationBinder();
      try
      {
        using (Stream serializationStream = (Stream) File.Open(filePath, FileMode.Open))
          return binaryFormatter.Deserialize(serializationStream);
      }
      catch (FileNotFoundException ex)
      {
        return (object) null;
      }
    }

    private static void SetEnvironmentVariables()
    {
    }
  }
}
