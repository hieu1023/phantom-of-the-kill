﻿// Decompiled with JetBrains decompiler
// Type: GameCore.Serialization.ReadWrite7BitEncodingExtension
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.IO;

namespace GameCore.Serialization
{
  public static class ReadWrite7BitEncodingExtension
  {
    public static void Write7BitInt(this BinaryWriter self, int value)
    {
      uint num;
      for (num = (uint) value; num >= 128U; num >>= 7)
        self.Write((byte) (num | 128U));
      self.Write((byte) num);
    }

    public static int Read7BitInt(this BinaryReader self)
    {
      int num1 = 0;
      int num2 = 0;
      while (num2 != 35)
      {
        byte num3 = self.ReadByte();
        num1 |= ((int) num3 & (int) sbyte.MaxValue) << num2;
        num2 += 7;
        if (((int) num3 & 128) == 0)
          return num1;
      }
      throw new FormatException("Bad7BitInt32");
    }
  }
}
