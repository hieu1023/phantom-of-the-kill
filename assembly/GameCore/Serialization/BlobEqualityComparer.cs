﻿// Decompiled with JetBrains decompiler
// Type: GameCore.Serialization.BlobEqualityComparer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

namespace GameCore.Serialization
{
  internal class BlobEqualityComparer : IEqualityComparer<object>
  {
    bool IEqualityComparer<object>.Equals(object x, object y)
    {
      if (x == null)
        return y == null;
      return x.GetType().IsValueType ? x.Equals(y) : x == y;
    }

    int IEqualityComparer<object>.GetHashCode(object obj)
    {
      return this.GetHashCode();
    }
  }
}
