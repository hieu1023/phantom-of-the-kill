﻿// Decompiled with JetBrains decompiler
// Type: GameCore.RecoveryUtility
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using System;
using System.Collections.Generic;

namespace GameCore
{
  public static class RecoveryUtility
  {
    public static void resetPosition(
      BL.UnitPosition up,
      int row,
      int column,
      BL env,
      bool noCountReset = false,
      bool countMoveDistance = false)
    {
      if (countMoveDistance)
      {
        int num1 = BL.fieldDistance(up.originalRow, up.originalColumn, up.row, up.column) + BL.fieldDistance(up.row, up.column, row, column);
        BL.Panel fieldPanel1 = env.getFieldPanel(up.originalRow, up.originalColumn);
        BL.Panel fieldPanel2 = env.getFieldPanel(up.row, up.column);
        up.usedMoveCost += env.getRouteCostNonCache(up, fieldPanel2, fieldPanel1, up.movePanels, up.completePanels);
        up.moveDistance += num1;
        foreach (BL.SkillEffect skillEffect1 in BattleFuncs.unitPositionToISkillEffectListUnit(up).skillEffects.All())
        {
          int? moveDistance = skillEffect1.moveDistance;
          if (moveDistance.HasValue)
          {
            BL.SkillEffect skillEffect2 = skillEffect1;
            moveDistance = skillEffect2.moveDistance;
            int num2 = num1;
            skillEffect2.moveDistance = moveDistance.HasValue ? new int?(moveDistance.GetValueOrDefault() + num2) : new int?();
          }
          else
            skillEffect1.moveDistance = new int?(num1);
        }
      }
      up.row = row;
      up.column = column;
      up.resetOriginalPosition(env, noCountReset);
    }

    public static void Apply(RecoveryType rt, BL env)
    {
      List<BL.UnitPosition> unitPositionList1 = new List<BL.UnitPosition>();
      List<BL.UnitPosition> unitPositionList2 = new List<BL.UnitPosition>();
      env.completedActionUnits.value = new List<BL.UnitPosition>();
      env.resetActionList(BL.ForceID.player);
      env.resetActionList(BL.ForceID.neutral);
      env.resetActionList(BL.ForceID.enemy);
      foreach (BL.UnitPosition unitPosition in env.unitPositions.value)
      {
        env.removeZocPanels((BL.ISkillEffectListUnit) unitPosition.unit, unitPosition.originalRow, unitPosition.originalColumn, false);
        unitPosition.removePanelSkillEffects(false);
      }
      foreach (RecoveryUnit unit in rt.units)
      {
        BL.UnitPosition up = BL.UnitPosition.FromNetwork(new int?(unit.unitPositionId), env);
        up.unit.hp = unit.hp;
        up.unit.setIsDead(unit.hp <= 0, env, 0, true, false);
        up.unit.deadTurn = new List<int>((IEnumerable<int>) unit.deadTurn);
        up.unit.deadCount = unit.deadCount;
        up.unit.deadCountExceptImmediateRebirth = unit.deadCountExceptImmediateRebirth;
        up.unit.pvpRespawnCount = unit.respawnCount;
        up.unit.skillEffects = new BL.SkillEffectList();
        foreach (RecoverySkillEffect skillEffect in unit.skillEffectList)
          up.unit.skillEffects.Add(BL.SkillEffect.FromRecovery(skillEffect, env), new bool?(), (BL.ISkillEffectListUnit) null);
        foreach (RecoverySkillEffectParam skillFixEffectParam in unit.skillFixEffectParams)
        {
          BL.SkillEffect effect = BL.SkillEffect.FromRecovery(skillFixEffectParam.skillEffect, env);
          up.unit.skillEffects.AddFixEffectParam(effect.effect.effect_logic.Enum, effect, (int) skillFixEffectParam.value);
        }
        foreach (RecoverySkillEffectParam ratioEffectParam in unit.skillRatioEffectParams)
        {
          BL.SkillEffect effect = BL.SkillEffect.FromRecovery(ratioEffectParam.skillEffect, env);
          up.unit.skillEffects.AddRatioEffectParam(effect.effect.effect_logic.Enum, effect, ratioEffectParam.value);
        }
        foreach (RecoverySkillEffect removedBaseSkillEffect in unit.removedBaseSkillEffects)
          up.unit.skillEffects.AddRemovedBaseSkillEffect(BL.SkillEffect.FromRecovery(removedBaseSkillEffect, env));
        foreach (RecoverySkillEffectParam removedFixEffectParam in unit.removedFixEffectParams)
        {
          BL.SkillEffect skillEffect = BL.SkillEffect.FromRecovery(removedFixEffectParam.skillEffect, env);
          up.unit.skillEffects.AddRemovedFixEffectParam(new Tuple<BattleskillEffectLogicEnum, BL.SkillEffect, int>(skillEffect.effect.effect_logic.Enum, skillEffect, (int) removedFixEffectParam.value));
        }
        foreach (RecoverySkillEffectParam ratioEffectParam in unit.removedRatioEffectParams)
        {
          BL.SkillEffect skillEffect = BL.SkillEffect.FromRecovery(ratioEffectParam.skillEffect, env);
          up.unit.skillEffects.AddRemovedRatioEffectParam(new Tuple<BattleskillEffectLogicEnum, BL.SkillEffect, float>(skillEffect.effect.effect_logic.Enum, skillEffect, ratioEffectParam.value));
        }
        foreach (int[] numArray in unit.duelSkillEffectIdInvokeCount)
          up.unit.skillEffects.SetDuelSkillEffectIdInvokeCount(numArray[0], numArray[1]);
        foreach (int[] numArray in unit.duelSkillIdInvokeCount)
          up.unit.skillEffects.SetDuelSkillIdInvokeCount(numArray[0], numArray[1]);
        foreach (int[] numArray in unit.duelSkillIdInvokeCount2)
          up.unit.skillEffects.SetDuelSkillIdInvokeCount2(numArray[0], numArray[1]);
        foreach (RecoverySkillEffect overwriteSkillEffect in unit.removedOverwriteSkillEffects)
          up.unit.skillEffects.AddRemovedOverwriteSkillEffect(BL.SkillEffect.FromRecovery(overwriteSkillEffect, env));
        up.unit.mIsExecCompletedSkillEffect = unit.isExecCompletedSkillEffect;
        foreach (RecoverySkillEffect transformationSkillEffect in unit.waitingTransformationSkillEffects)
          up.unit.skillEffects.AddWaitingTransformationSkillEffect(BL.SkillEffect.FromRecovery(transformationSkillEffect, env));
        RecoveryUtility.resetPosition(up, unit.row, unit.column, env, false, false);
        if (unit.completed)
        {
          if (!up.isCompleted)
            up.recoveryCompleteUnit();
        }
        else if (up.unit.isPlayerControl)
          unitPositionList1.Add(up);
        else
          unitPositionList2.Add(up);
        foreach (RecoverySkill skill1 in unit.skillList)
        {
          BL.Skill skill2 = (BL.Skill) null;
          if (up.unit.hasOugi && up.unit.ougi.id == skill1.id)
          {
            skill2 = up.unit.ougi;
          }
          else
          {
            foreach (BL.Skill skill3 in up.unit.skills)
            {
              if (skill3.id == skill1.id)
                skill2 = skill3;
            }
          }
          if (skill2 != null)
          {
            skill2.level = skill1.level;
            skill2.remain = skill1.remain;
            skill2.useTurn = skill1.useTurn;
          }
        }
      }
      env.playerActionUnits.value = unitPositionList1;
      env.enemyActionUnits.value = unitPositionList2;
    }
  }
}
