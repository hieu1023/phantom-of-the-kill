﻿// Decompiled with JetBrains decompiler
// Type: GameCore.BattleFuncs
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using SM;
using System;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

namespace GameCore
{
  public static class BattleFuncs
  {
    public static GameGlobalVariable<BL> environment = GameGlobalVariable<BL>.Null();
    public static BL.Unit[] ZeroArrayUnit = new BL.Unit[0];
    public static BL.ForceID[] ForceIDArrayNone = new BL.ForceID[1]
    {
      BL.ForceID.none
    };
    public static BL.ForceID[] ForceIDArrayPlayer = new BL.ForceID[1];
    public static BL.ForceID[] ForceIDArrayEnemy = new BL.ForceID[1]
    {
      BL.ForceID.enemy
    };
    public static BL.ForceID[] ForceIDArrayNeutral = new BL.ForceID[1]
    {
      BL.ForceID.neutral
    };
    public static BL.ForceID[] ForceIDArrayPlayerTarget = new BL.ForceID[1]
    {
      BL.ForceID.enemy
    };
    public static BL.ForceID[] ForceIDArrayEnemyTarget = new BL.ForceID[1];
    public static BL.ForceID[] ForceIDArrayNeutralTarget = new BL.ForceID[2]
    {
      BL.ForceID.player,
      BL.ForceID.enemy
    };
    private static BattleskillEffectLogicEnum[] DeckEveryGeSkillAddEnum = new BattleskillEffectLogicEnum[17]
    {
      BattleskillEffectLogicEnum.deck_every_ge_fix_strength,
      BattleskillEffectLogicEnum.deck_every_ge_fix_vitality,
      BattleskillEffectLogicEnum.deck_every_ge_fix_intelligence,
      BattleskillEffectLogicEnum.deck_every_ge_fix_mind,
      BattleskillEffectLogicEnum.deck_every_ge_fix_agility,
      BattleskillEffectLogicEnum.deck_every_ge_fix_dexterity,
      BattleskillEffectLogicEnum.deck_every_ge_fix_luck,
      BattleskillEffectLogicEnum.deck_every_ge_fix_move,
      BattleskillEffectLogicEnum.deck_every_ge_fix_physical_attack,
      BattleskillEffectLogicEnum.deck_every_ge_fix_magic_attack,
      BattleskillEffectLogicEnum.deck_every_ge_fix_physical_defense,
      BattleskillEffectLogicEnum.deck_every_ge_fix_magic_defense,
      BattleskillEffectLogicEnum.deck_every_ge_fix_hit,
      BattleskillEffectLogicEnum.deck_every_ge_fix_evasion,
      BattleskillEffectLogicEnum.deck_every_ge_fix_critical,
      BattleskillEffectLogicEnum.deck_every_ge_fix_critical_evasion,
      BattleskillEffectLogicEnum.deck_every_ge_fix_attack_speed
    };
    private static BattleskillEffectLogicEnum[] DeckEveryGeSkillMulEnum = new BattleskillEffectLogicEnum[17]
    {
      BattleskillEffectLogicEnum.deck_every_ge_ratio_strength,
      BattleskillEffectLogicEnum.deck_every_ge_ratio_vitality,
      BattleskillEffectLogicEnum.deck_every_ge_ratio_intelligence,
      BattleskillEffectLogicEnum.deck_every_ge_ratio_mind,
      BattleskillEffectLogicEnum.deck_every_ge_ratio_agility,
      BattleskillEffectLogicEnum.deck_every_ge_ratio_dexterity,
      BattleskillEffectLogicEnum.deck_every_ge_ratio_luck,
      BattleskillEffectLogicEnum.deck_every_ge_ratio_move,
      BattleskillEffectLogicEnum.deck_every_ge_ratio_physical_attack,
      BattleskillEffectLogicEnum.deck_every_ge_ratio_magic_attack,
      BattleskillEffectLogicEnum.deck_every_ge_ratio_physical_defense,
      BattleskillEffectLogicEnum.deck_every_ge_ratio_magic_defense,
      BattleskillEffectLogicEnum.deck_every_ge_ratio_hit,
      BattleskillEffectLogicEnum.deck_every_ge_ratio_evasion,
      BattleskillEffectLogicEnum.deck_every_ge_ratio_critical,
      BattleskillEffectLogicEnum.deck_every_ge_ratio_critical_evasion,
      BattleskillEffectLogicEnum.deck_every_ge_ratio_attack_speed
    };
    private static BattleskillEffectLogicEnum[] DeckEveryLeSkillAddEnum = new BattleskillEffectLogicEnum[17]
    {
      BattleskillEffectLogicEnum.deck_every_le_fix_strength,
      BattleskillEffectLogicEnum.deck_every_le_fix_vitality,
      BattleskillEffectLogicEnum.deck_every_le_fix_intelligence,
      BattleskillEffectLogicEnum.deck_every_le_fix_mind,
      BattleskillEffectLogicEnum.deck_every_le_fix_agility,
      BattleskillEffectLogicEnum.deck_every_le_fix_dexterity,
      BattleskillEffectLogicEnum.deck_every_le_fix_luck,
      BattleskillEffectLogicEnum.deck_every_le_fix_move,
      BattleskillEffectLogicEnum.deck_every_le_fix_physical_attack,
      BattleskillEffectLogicEnum.deck_every_le_fix_magic_attack,
      BattleskillEffectLogicEnum.deck_every_le_fix_physical_defense,
      BattleskillEffectLogicEnum.deck_every_le_fix_magic_defense,
      BattleskillEffectLogicEnum.deck_every_le_fix_hit,
      BattleskillEffectLogicEnum.deck_every_le_fix_evasion,
      BattleskillEffectLogicEnum.deck_every_le_fix_critical,
      BattleskillEffectLogicEnum.deck_every_le_fix_critical_evasion,
      BattleskillEffectLogicEnum.deck_every_le_fix_attack_speed
    };
    private static BattleskillEffectLogicEnum[] DeckEveryLeSkillMulEnum = new BattleskillEffectLogicEnum[17]
    {
      BattleskillEffectLogicEnum.deck_every_le_ratio_strength,
      BattleskillEffectLogicEnum.deck_every_le_ratio_vitality,
      BattleskillEffectLogicEnum.deck_every_le_ratio_intelligence,
      BattleskillEffectLogicEnum.deck_every_le_ratio_mind,
      BattleskillEffectLogicEnum.deck_every_le_ratio_agility,
      BattleskillEffectLogicEnum.deck_every_le_ratio_dexterity,
      BattleskillEffectLogicEnum.deck_every_le_ratio_luck,
      BattleskillEffectLogicEnum.deck_every_le_ratio_move,
      BattleskillEffectLogicEnum.deck_every_le_ratio_physical_attack,
      BattleskillEffectLogicEnum.deck_every_le_ratio_magic_attack,
      BattleskillEffectLogicEnum.deck_every_le_ratio_physical_defense,
      BattleskillEffectLogicEnum.deck_every_le_ratio_magic_defense,
      BattleskillEffectLogicEnum.deck_every_le_ratio_hit,
      BattleskillEffectLogicEnum.deck_every_le_ratio_evasion,
      BattleskillEffectLogicEnum.deck_every_le_ratio_critical,
      BattleskillEffectLogicEnum.deck_every_le_ratio_critical_evasion,
      BattleskillEffectLogicEnum.deck_every_le_ratio_attack_speed
    };
    private static BattleskillEffectLogicEnum[] DeckAnotherGeSkillAddEnum = new BattleskillEffectLogicEnum[17]
    {
      BattleskillEffectLogicEnum.deck_another_ge_fix_strength,
      BattleskillEffectLogicEnum.deck_another_ge_fix_vitality,
      BattleskillEffectLogicEnum.deck_another_ge_fix_intelligence,
      BattleskillEffectLogicEnum.deck_another_ge_fix_mind,
      BattleskillEffectLogicEnum.deck_another_ge_fix_agility,
      BattleskillEffectLogicEnum.deck_another_ge_fix_dexterity,
      BattleskillEffectLogicEnum.deck_another_ge_fix_luck,
      BattleskillEffectLogicEnum.deck_another_ge_fix_move,
      BattleskillEffectLogicEnum.deck_another_ge_fix_physical_attack,
      BattleskillEffectLogicEnum.deck_another_ge_fix_magic_attack,
      BattleskillEffectLogicEnum.deck_another_ge_fix_physical_defense,
      BattleskillEffectLogicEnum.deck_another_ge_fix_magic_defense,
      BattleskillEffectLogicEnum.deck_another_ge_fix_hit,
      BattleskillEffectLogicEnum.deck_another_ge_fix_evasion,
      BattleskillEffectLogicEnum.deck_another_ge_fix_critical,
      BattleskillEffectLogicEnum.deck_another_ge_fix_critical_evasion,
      BattleskillEffectLogicEnum.deck_another_ge_fix_attack_speed
    };
    private static BattleskillEffectLogicEnum[] DeckAnotherGeSkillMulEnum = new BattleskillEffectLogicEnum[17]
    {
      BattleskillEffectLogicEnum.deck_another_ge_ratio_strength,
      BattleskillEffectLogicEnum.deck_another_ge_ratio_vitality,
      BattleskillEffectLogicEnum.deck_another_ge_ratio_intelligence,
      BattleskillEffectLogicEnum.deck_another_ge_ratio_mind,
      BattleskillEffectLogicEnum.deck_another_ge_ratio_agility,
      BattleskillEffectLogicEnum.deck_another_ge_ratio_dexterity,
      BattleskillEffectLogicEnum.deck_another_ge_ratio_luck,
      BattleskillEffectLogicEnum.deck_another_ge_ratio_move,
      BattleskillEffectLogicEnum.deck_another_ge_ratio_physical_attack,
      BattleskillEffectLogicEnum.deck_another_ge_ratio_magic_attack,
      BattleskillEffectLogicEnum.deck_another_ge_ratio_physical_defense,
      BattleskillEffectLogicEnum.deck_another_ge_ratio_magic_defense,
      BattleskillEffectLogicEnum.deck_another_ge_ratio_hit,
      BattleskillEffectLogicEnum.deck_another_ge_ratio_evasion,
      BattleskillEffectLogicEnum.deck_another_ge_ratio_critical,
      BattleskillEffectLogicEnum.deck_another_ge_ratio_critical_evasion,
      BattleskillEffectLogicEnum.deck_another_ge_ratio_attack_speed
    };
    private static BattleskillEffectLogicEnum[] DeckAnotherLeSkillAddEnum = new BattleskillEffectLogicEnum[17]
    {
      BattleskillEffectLogicEnum.deck_another_le_fix_strength,
      BattleskillEffectLogicEnum.deck_another_le_fix_vitality,
      BattleskillEffectLogicEnum.deck_another_le_fix_intelligence,
      BattleskillEffectLogicEnum.deck_another_le_fix_mind,
      BattleskillEffectLogicEnum.deck_another_le_fix_agility,
      BattleskillEffectLogicEnum.deck_another_le_fix_dexterity,
      BattleskillEffectLogicEnum.deck_another_le_fix_luck,
      BattleskillEffectLogicEnum.deck_another_le_fix_move,
      BattleskillEffectLogicEnum.deck_another_le_fix_physical_attack,
      BattleskillEffectLogicEnum.deck_another_le_fix_magic_attack,
      BattleskillEffectLogicEnum.deck_another_le_fix_physical_defense,
      BattleskillEffectLogicEnum.deck_another_le_fix_magic_defense,
      BattleskillEffectLogicEnum.deck_another_le_fix_hit,
      BattleskillEffectLogicEnum.deck_another_le_fix_evasion,
      BattleskillEffectLogicEnum.deck_another_le_fix_critical,
      BattleskillEffectLogicEnum.deck_another_le_fix_critical_evasion,
      BattleskillEffectLogicEnum.deck_another_le_fix_attack_speed
    };
    private static BattleskillEffectLogicEnum[] DeckAnotherLeSkillMulEnum = new BattleskillEffectLogicEnum[17]
    {
      BattleskillEffectLogicEnum.deck_another_le_ratio_strength,
      BattleskillEffectLogicEnum.deck_another_le_ratio_vitality,
      BattleskillEffectLogicEnum.deck_another_le_ratio_intelligence,
      BattleskillEffectLogicEnum.deck_another_le_ratio_mind,
      BattleskillEffectLogicEnum.deck_another_le_ratio_agility,
      BattleskillEffectLogicEnum.deck_another_le_ratio_dexterity,
      BattleskillEffectLogicEnum.deck_another_le_ratio_luck,
      BattleskillEffectLogicEnum.deck_another_le_ratio_move,
      BattleskillEffectLogicEnum.deck_another_le_ratio_physical_attack,
      BattleskillEffectLogicEnum.deck_another_le_ratio_magic_attack,
      BattleskillEffectLogicEnum.deck_another_le_ratio_physical_defense,
      BattleskillEffectLogicEnum.deck_another_le_ratio_magic_defense,
      BattleskillEffectLogicEnum.deck_another_le_ratio_hit,
      BattleskillEffectLogicEnum.deck_another_le_ratio_evasion,
      BattleskillEffectLogicEnum.deck_another_le_ratio_critical,
      BattleskillEffectLogicEnum.deck_another_le_ratio_critical_evasion,
      BattleskillEffectLogicEnum.deck_another_le_ratio_attack_speed
    };
    private static BattleskillEffectLogicEnum[] DeckSameGeSkillAddEnum = new BattleskillEffectLogicEnum[17]
    {
      BattleskillEffectLogicEnum.deck_same_ge_fix_strength,
      BattleskillEffectLogicEnum.deck_same_ge_fix_vitality,
      BattleskillEffectLogicEnum.deck_same_ge_fix_intelligence,
      BattleskillEffectLogicEnum.deck_same_ge_fix_mind,
      BattleskillEffectLogicEnum.deck_same_ge_fix_agility,
      BattleskillEffectLogicEnum.deck_same_ge_fix_dexterity,
      BattleskillEffectLogicEnum.deck_same_ge_fix_luck,
      BattleskillEffectLogicEnum.deck_same_ge_fix_move,
      BattleskillEffectLogicEnum.deck_same_ge_fix_physical_attack,
      BattleskillEffectLogicEnum.deck_same_ge_fix_magic_attack,
      BattleskillEffectLogicEnum.deck_same_ge_fix_physical_defense,
      BattleskillEffectLogicEnum.deck_same_ge_fix_magic_defense,
      BattleskillEffectLogicEnum.deck_same_ge_fix_hit,
      BattleskillEffectLogicEnum.deck_same_ge_fix_evasion,
      BattleskillEffectLogicEnum.deck_same_ge_fix_critical,
      BattleskillEffectLogicEnum.deck_same_ge_fix_critical_evasion,
      BattleskillEffectLogicEnum.deck_same_ge_fix_attack_speed
    };
    private static BattleskillEffectLogicEnum[] DeckSameGeSkillMulEnum = new BattleskillEffectLogicEnum[17]
    {
      BattleskillEffectLogicEnum.deck_same_ge_ratio_strength,
      BattleskillEffectLogicEnum.deck_same_ge_ratio_vitality,
      BattleskillEffectLogicEnum.deck_same_ge_ratio_intelligence,
      BattleskillEffectLogicEnum.deck_same_ge_ratio_mind,
      BattleskillEffectLogicEnum.deck_same_ge_ratio_agility,
      BattleskillEffectLogicEnum.deck_same_ge_ratio_dexterity,
      BattleskillEffectLogicEnum.deck_same_ge_ratio_luck,
      BattleskillEffectLogicEnum.deck_same_ge_ratio_move,
      BattleskillEffectLogicEnum.deck_same_ge_ratio_physical_attack,
      BattleskillEffectLogicEnum.deck_same_ge_ratio_magic_attack,
      BattleskillEffectLogicEnum.deck_same_ge_ratio_physical_defense,
      BattleskillEffectLogicEnum.deck_same_ge_ratio_magic_defense,
      BattleskillEffectLogicEnum.deck_same_ge_ratio_hit,
      BattleskillEffectLogicEnum.deck_same_ge_ratio_evasion,
      BattleskillEffectLogicEnum.deck_same_ge_ratio_critical,
      BattleskillEffectLogicEnum.deck_same_ge_ratio_critical_evasion,
      BattleskillEffectLogicEnum.deck_same_ge_ratio_attack_speed
    };
    private static BattleskillEffectLogicEnum[] DeckSameLeSkillAddEnum = new BattleskillEffectLogicEnum[17]
    {
      BattleskillEffectLogicEnum.deck_same_le_fix_strength,
      BattleskillEffectLogicEnum.deck_same_le_fix_vitality,
      BattleskillEffectLogicEnum.deck_same_le_fix_intelligence,
      BattleskillEffectLogicEnum.deck_same_le_fix_mind,
      BattleskillEffectLogicEnum.deck_same_le_fix_agility,
      BattleskillEffectLogicEnum.deck_same_le_fix_dexterity,
      BattleskillEffectLogicEnum.deck_same_le_fix_luck,
      BattleskillEffectLogicEnum.deck_same_le_fix_move,
      BattleskillEffectLogicEnum.deck_same_le_fix_physical_attack,
      BattleskillEffectLogicEnum.deck_same_le_fix_magic_attack,
      BattleskillEffectLogicEnum.deck_same_le_fix_physical_defense,
      BattleskillEffectLogicEnum.deck_same_le_fix_magic_defense,
      BattleskillEffectLogicEnum.deck_same_le_fix_hit,
      BattleskillEffectLogicEnum.deck_same_le_fix_evasion,
      BattleskillEffectLogicEnum.deck_same_le_fix_critical,
      BattleskillEffectLogicEnum.deck_same_le_fix_critical_evasion,
      BattleskillEffectLogicEnum.deck_same_le_fix_attack_speed
    };
    private static BattleskillEffectLogicEnum[] DeckSameLeSkillMulEnum = new BattleskillEffectLogicEnum[17]
    {
      BattleskillEffectLogicEnum.deck_same_le_ratio_strength,
      BattleskillEffectLogicEnum.deck_same_le_ratio_vitality,
      BattleskillEffectLogicEnum.deck_same_le_ratio_intelligence,
      BattleskillEffectLogicEnum.deck_same_le_ratio_mind,
      BattleskillEffectLogicEnum.deck_same_le_ratio_agility,
      BattleskillEffectLogicEnum.deck_same_le_ratio_dexterity,
      BattleskillEffectLogicEnum.deck_same_le_ratio_luck,
      BattleskillEffectLogicEnum.deck_same_le_ratio_move,
      BattleskillEffectLogicEnum.deck_same_le_ratio_physical_attack,
      BattleskillEffectLogicEnum.deck_same_le_ratio_magic_attack,
      BattleskillEffectLogicEnum.deck_same_le_ratio_physical_defense,
      BattleskillEffectLogicEnum.deck_same_le_ratio_magic_defense,
      BattleskillEffectLogicEnum.deck_same_le_ratio_hit,
      BattleskillEffectLogicEnum.deck_same_le_ratio_evasion,
      BattleskillEffectLogicEnum.deck_same_le_ratio_critical,
      BattleskillEffectLogicEnum.deck_same_le_ratio_critical_evasion,
      BattleskillEffectLogicEnum.deck_same_le_ratio_attack_speed
    };
    private static BattleskillEffectLogicEnum[] DamageCutFixEnum = new BattleskillEffectLogicEnum[6]
    {
      BattleskillEffectLogicEnum.damage_cut_fix_duel,
      BattleskillEffectLogicEnum.damage_cut_fix_after_duel,
      BattleskillEffectLogicEnum.damage_cut_fix_ailment,
      BattleskillEffectLogicEnum.damage_cut_fix_landform,
      BattleskillEffectLogicEnum.damage_cut_fix_command,
      BattleskillEffectLogicEnum.damage_cut_fix_release
    };
    private static BattleskillEffectLogicEnum[] DamageCutRatioEnum = new BattleskillEffectLogicEnum[6]
    {
      BattleskillEffectLogicEnum.damage_cut_ratio_duel,
      BattleskillEffectLogicEnum.damage_cut_ratio_after_duel,
      BattleskillEffectLogicEnum.damage_cut_ratio_ailment,
      BattleskillEffectLogicEnum.damage_cut_ratio_landform,
      BattleskillEffectLogicEnum.damage_cut_ratio_command,
      BattleskillEffectLogicEnum.damage_cut_ratio_release
    };
    private static BattleskillEffectLogicEnum[] DamageCut2FixEnum = new BattleskillEffectLogicEnum[6]
    {
      BattleskillEffectLogicEnum.damage_cut2_fix_duel,
      BattleskillEffectLogicEnum.damage_cut2_fix_after_duel,
      BattleskillEffectLogicEnum.damage_cut2_fix_ailment,
      BattleskillEffectLogicEnum.damage_cut2_fix_landform,
      BattleskillEffectLogicEnum.damage_cut2_fix_command,
      BattleskillEffectLogicEnum.damage_cut2_fix_release
    };
    private static BattleskillEffectLogicEnum[] DamageCut2RatioEnum = new BattleskillEffectLogicEnum[6]
    {
      BattleskillEffectLogicEnum.damage_cut2_ratio_duel,
      BattleskillEffectLogicEnum.damage_cut2_ratio_after_duel,
      BattleskillEffectLogicEnum.damage_cut2_ratio_ailment,
      BattleskillEffectLogicEnum.damage_cut2_ratio_landform,
      BattleskillEffectLogicEnum.damage_cut2_ratio_command,
      BattleskillEffectLogicEnum.damage_cut2_ratio_release
    };
    private static BattleskillEffectLogicEnum[] charismaEffectsEnum = new BattleskillEffectLogicEnum[37]
    {
      BattleskillEffectLogicEnum.charisma_fix_strength,
      BattleskillEffectLogicEnum.charisma_fix_vitality,
      BattleskillEffectLogicEnum.charisma_fix_intelligence,
      BattleskillEffectLogicEnum.charisma_fix_mind,
      BattleskillEffectLogicEnum.charisma_fix_agility,
      BattleskillEffectLogicEnum.charisma_fix_dexterity,
      BattleskillEffectLogicEnum.charisma_fix_luck,
      BattleskillEffectLogicEnum.charisma_fix_move,
      BattleskillEffectLogicEnum.charisma_fix_physical_attack,
      BattleskillEffectLogicEnum.charisma_fix_magic_attack,
      BattleskillEffectLogicEnum.charisma_fix_physical_defense,
      BattleskillEffectLogicEnum.charisma_fix_magic_defense,
      BattleskillEffectLogicEnum.charisma_fix_hit,
      BattleskillEffectLogicEnum.charisma_fix_evasion,
      BattleskillEffectLogicEnum.charisma_fix_critical,
      BattleskillEffectLogicEnum.charisma_fix_critical_evasion,
      BattleskillEffectLogicEnum.charisma_fix_attack_speed,
      BattleskillEffectLogicEnum.charisma_ratio_strength,
      BattleskillEffectLogicEnum.charisma_ratio_vitality,
      BattleskillEffectLogicEnum.charisma_ratio_intelligence,
      BattleskillEffectLogicEnum.charisma_ratio_mind,
      BattleskillEffectLogicEnum.charisma_ratio_agility,
      BattleskillEffectLogicEnum.charisma_ratio_dexterity,
      BattleskillEffectLogicEnum.charisma_ratio_luck,
      BattleskillEffectLogicEnum.charisma_ratio_move,
      BattleskillEffectLogicEnum.charisma_ratio_physical_attack,
      BattleskillEffectLogicEnum.charisma_ratio_magic_attack,
      BattleskillEffectLogicEnum.charisma_ratio_physical_defense,
      BattleskillEffectLogicEnum.charisma_ratio_magic_defense,
      BattleskillEffectLogicEnum.charisma_ratio_hit,
      BattleskillEffectLogicEnum.charisma_ratio_evasion,
      BattleskillEffectLogicEnum.charisma_ratio_critical,
      BattleskillEffectLogicEnum.charisma_ratio_critical_evasion,
      BattleskillEffectLogicEnum.charisma_ratio_attack_speed,
      BattleskillEffectLogicEnum.charisma_clamp_hit,
      BattleskillEffectLogicEnum.charisma_damage_rate,
      BattleskillEffectLogicEnum.charisma_enemy_damage_rate
    };
    private static BattleskillEffectLogicEnum[] moveDistanceSkillEffectsEnum = new BattleskillEffectLogicEnum[34]
    {
      BattleskillEffectLogicEnum.move_distance_fix_strength,
      BattleskillEffectLogicEnum.move_distance_fix_vitality,
      BattleskillEffectLogicEnum.move_distance_fix_intelligence,
      BattleskillEffectLogicEnum.move_distance_fix_mind,
      BattleskillEffectLogicEnum.move_distance_fix_agility,
      BattleskillEffectLogicEnum.move_distance_fix_dexterity,
      BattleskillEffectLogicEnum.move_distance_fix_luck,
      BattleskillEffectLogicEnum.move_distance_fix_move,
      BattleskillEffectLogicEnum.move_distance_fix_physical_attack,
      BattleskillEffectLogicEnum.move_distance_fix_magic_attack,
      BattleskillEffectLogicEnum.move_distance_fix_physical_defense,
      BattleskillEffectLogicEnum.move_distance_fix_magic_defense,
      BattleskillEffectLogicEnum.move_distance_fix_hit,
      BattleskillEffectLogicEnum.move_distance_fix_evasion,
      BattleskillEffectLogicEnum.move_distance_fix_critical,
      BattleskillEffectLogicEnum.move_distance_fix_critical_evasion,
      BattleskillEffectLogicEnum.move_distance_fix_attack_speed,
      BattleskillEffectLogicEnum.move_distance_ratio_strength,
      BattleskillEffectLogicEnum.move_distance_ratio_vitality,
      BattleskillEffectLogicEnum.move_distance_ratio_intelligence,
      BattleskillEffectLogicEnum.move_distance_ratio_mind,
      BattleskillEffectLogicEnum.move_distance_ratio_agility,
      BattleskillEffectLogicEnum.move_distance_ratio_dexterity,
      BattleskillEffectLogicEnum.move_distance_ratio_luck,
      BattleskillEffectLogicEnum.move_distance_ratio_move,
      BattleskillEffectLogicEnum.move_distance_ratio_physical_attack,
      BattleskillEffectLogicEnum.move_distance_ratio_magic_attack,
      BattleskillEffectLogicEnum.move_distance_ratio_physical_defense,
      BattleskillEffectLogicEnum.move_distance_ratio_magic_defense,
      BattleskillEffectLogicEnum.move_distance_ratio_hit,
      BattleskillEffectLogicEnum.move_distance_ratio_evasion,
      BattleskillEffectLogicEnum.move_distance_ratio_critical,
      BattleskillEffectLogicEnum.move_distance_ratio_critical_evasion,
      BattleskillEffectLogicEnum.move_distance_ratio_attack_speed
    };
    private static BattleskillEffectLogicEnum[] rangeSkillEffectsEnum = new BattleskillEffectLogicEnum[34]
    {
      BattleskillEffectLogicEnum.cavalry_rush_fix_strength,
      BattleskillEffectLogicEnum.cavalry_rush_fix_vitality,
      BattleskillEffectLogicEnum.cavalry_rush_fix_intelligence,
      BattleskillEffectLogicEnum.cavalry_rush_fix_mind,
      BattleskillEffectLogicEnum.cavalry_rush_fix_agility,
      BattleskillEffectLogicEnum.cavalry_rush_fix_dexterity,
      BattleskillEffectLogicEnum.cavalry_rush_fix_luck,
      BattleskillEffectLogicEnum.cavalry_rush_fix_move,
      BattleskillEffectLogicEnum.cavalry_rush_fix_physical_attack,
      BattleskillEffectLogicEnum.cavalry_rush_fix_magic_attack,
      BattleskillEffectLogicEnum.cavalry_rush_fix_physical_defense,
      BattleskillEffectLogicEnum.cavalry_rush_fix_magic_defense,
      BattleskillEffectLogicEnum.cavalry_rush_fix_hit,
      BattleskillEffectLogicEnum.cavalry_rush_fix_evasion,
      BattleskillEffectLogicEnum.cavalry_rush_fix_critical,
      BattleskillEffectLogicEnum.cavalry_rush_fix_critical_evasion,
      BattleskillEffectLogicEnum.cavalry_rush_fix_attack_speed,
      BattleskillEffectLogicEnum.cavalry_rush_ratio_strength,
      BattleskillEffectLogicEnum.cavalry_rush_ratio_vitality,
      BattleskillEffectLogicEnum.cavalry_rush_ratio_intelligence,
      BattleskillEffectLogicEnum.cavalry_rush_ratio_mind,
      BattleskillEffectLogicEnum.cavalry_rush_ratio_agility,
      BattleskillEffectLogicEnum.cavalry_rush_ratio_dexterity,
      BattleskillEffectLogicEnum.cavalry_rush_ratio_luck,
      BattleskillEffectLogicEnum.cavalry_rush_ratio_move,
      BattleskillEffectLogicEnum.cavalry_rush_ratio_physical_attack,
      BattleskillEffectLogicEnum.cavalry_rush_ratio_magic_attack,
      BattleskillEffectLogicEnum.cavalry_rush_ratio_physical_defense,
      BattleskillEffectLogicEnum.cavalry_rush_ratio_magic_defense,
      BattleskillEffectLogicEnum.cavalry_rush_ratio_hit,
      BattleskillEffectLogicEnum.cavalry_rush_ratio_evasion,
      BattleskillEffectLogicEnum.cavalry_rush_ratio_critical,
      BattleskillEffectLogicEnum.cavalry_rush_ratio_critical_evasion,
      BattleskillEffectLogicEnum.cavalry_rush_ratio_attack_speed
    };
    private static BattleskillEffectLogicEnum[] deadCountSkillEffectsEnumPlayerForce = new BattleskillEffectLogicEnum[68]
    {
      BattleskillEffectLogicEnum.dead_count_player_fix_strength,
      BattleskillEffectLogicEnum.dead_count_player_fix_vitality,
      BattleskillEffectLogicEnum.dead_count_player_fix_intelligence,
      BattleskillEffectLogicEnum.dead_count_player_fix_mind,
      BattleskillEffectLogicEnum.dead_count_player_fix_agility,
      BattleskillEffectLogicEnum.dead_count_player_fix_dexterity,
      BattleskillEffectLogicEnum.dead_count_player_fix_luck,
      BattleskillEffectLogicEnum.dead_count_player_fix_move,
      BattleskillEffectLogicEnum.dead_count_player_fix_physical_attack,
      BattleskillEffectLogicEnum.dead_count_player_fix_magic_attack,
      BattleskillEffectLogicEnum.dead_count_player_fix_physical_defense,
      BattleskillEffectLogicEnum.dead_count_player_fix_magic_defense,
      BattleskillEffectLogicEnum.dead_count_player_fix_hit,
      BattleskillEffectLogicEnum.dead_count_player_fix_evasion,
      BattleskillEffectLogicEnum.dead_count_player_fix_critical,
      BattleskillEffectLogicEnum.dead_count_player_fix_critical_evasion,
      BattleskillEffectLogicEnum.dead_count_player_fix_attack_speed,
      BattleskillEffectLogicEnum.dead_count_player_ratio_strength,
      BattleskillEffectLogicEnum.dead_count_player_ratio_vitality,
      BattleskillEffectLogicEnum.dead_count_player_ratio_intelligence,
      BattleskillEffectLogicEnum.dead_count_player_ratio_mind,
      BattleskillEffectLogicEnum.dead_count_player_ratio_agility,
      BattleskillEffectLogicEnum.dead_count_player_ratio_dexterity,
      BattleskillEffectLogicEnum.dead_count_player_ratio_luck,
      BattleskillEffectLogicEnum.dead_count_player_ratio_move,
      BattleskillEffectLogicEnum.dead_count_player_ratio_physical_attack,
      BattleskillEffectLogicEnum.dead_count_player_ratio_magic_attack,
      BattleskillEffectLogicEnum.dead_count_player_ratio_physical_defense,
      BattleskillEffectLogicEnum.dead_count_player_ratio_magic_defense,
      BattleskillEffectLogicEnum.dead_count_player_ratio_hit,
      BattleskillEffectLogicEnum.dead_count_player_ratio_evasion,
      BattleskillEffectLogicEnum.dead_count_player_ratio_critical,
      BattleskillEffectLogicEnum.dead_count_player_ratio_critical_evasion,
      BattleskillEffectLogicEnum.dead_count_player_ratio_attack_speed,
      BattleskillEffectLogicEnum.dead_count_complex_fix_strength,
      BattleskillEffectLogicEnum.dead_count_complex_fix_vitality,
      BattleskillEffectLogicEnum.dead_count_complex_fix_intelligence,
      BattleskillEffectLogicEnum.dead_count_complex_fix_mind,
      BattleskillEffectLogicEnum.dead_count_complex_fix_agility,
      BattleskillEffectLogicEnum.dead_count_complex_fix_dexterity,
      BattleskillEffectLogicEnum.dead_count_complex_fix_luck,
      BattleskillEffectLogicEnum.dead_count_complex_fix_move,
      BattleskillEffectLogicEnum.dead_count_complex_fix_physical_attack,
      BattleskillEffectLogicEnum.dead_count_complex_fix_magic_attack,
      BattleskillEffectLogicEnum.dead_count_complex_fix_physical_defense,
      BattleskillEffectLogicEnum.dead_count_complex_fix_magic_defense,
      BattleskillEffectLogicEnum.dead_count_complex_fix_hit,
      BattleskillEffectLogicEnum.dead_count_complex_fix_evasion,
      BattleskillEffectLogicEnum.dead_count_complex_fix_critical,
      BattleskillEffectLogicEnum.dead_count_complex_fix_critical_evasion,
      BattleskillEffectLogicEnum.dead_count_complex_fix_attack_speed,
      BattleskillEffectLogicEnum.dead_count_complex_ratio_strength,
      BattleskillEffectLogicEnum.dead_count_complex_ratio_vitality,
      BattleskillEffectLogicEnum.dead_count_complex_ratio_intelligence,
      BattleskillEffectLogicEnum.dead_count_complex_ratio_mind,
      BattleskillEffectLogicEnum.dead_count_complex_ratio_agility,
      BattleskillEffectLogicEnum.dead_count_complex_ratio_dexterity,
      BattleskillEffectLogicEnum.dead_count_complex_ratio_luck,
      BattleskillEffectLogicEnum.dead_count_complex_ratio_move,
      BattleskillEffectLogicEnum.dead_count_complex_ratio_physical_attack,
      BattleskillEffectLogicEnum.dead_count_complex_ratio_magic_attack,
      BattleskillEffectLogicEnum.dead_count_complex_ratio_physical_defense,
      BattleskillEffectLogicEnum.dead_count_complex_ratio_magic_defense,
      BattleskillEffectLogicEnum.dead_count_complex_ratio_hit,
      BattleskillEffectLogicEnum.dead_count_complex_ratio_evasion,
      BattleskillEffectLogicEnum.dead_count_complex_ratio_critical,
      BattleskillEffectLogicEnum.dead_count_complex_ratio_critical_evasion,
      BattleskillEffectLogicEnum.dead_count_complex_ratio_attack_speed
    };
    private static BattleskillEffectLogicEnum[] deadCountSkillEffectsEnumEnemyForce = new BattleskillEffectLogicEnum[68]
    {
      BattleskillEffectLogicEnum.dead_count_enemy_fix_strength,
      BattleskillEffectLogicEnum.dead_count_enemy_fix_vitality,
      BattleskillEffectLogicEnum.dead_count_enemy_fix_intelligence,
      BattleskillEffectLogicEnum.dead_count_enemy_fix_mind,
      BattleskillEffectLogicEnum.dead_count_enemy_fix_agility,
      BattleskillEffectLogicEnum.dead_count_enemy_fix_dexterity,
      BattleskillEffectLogicEnum.dead_count_enemy_fix_luck,
      BattleskillEffectLogicEnum.dead_count_enemy_fix_move,
      BattleskillEffectLogicEnum.dead_count_enemy_fix_physical_attack,
      BattleskillEffectLogicEnum.dead_count_enemy_fix_magic_attack,
      BattleskillEffectLogicEnum.dead_count_enemy_fix_physical_defense,
      BattleskillEffectLogicEnum.dead_count_enemy_fix_magic_defense,
      BattleskillEffectLogicEnum.dead_count_enemy_fix_hit,
      BattleskillEffectLogicEnum.dead_count_enemy_fix_evasion,
      BattleskillEffectLogicEnum.dead_count_enemy_fix_critical,
      BattleskillEffectLogicEnum.dead_count_enemy_fix_critical_evasion,
      BattleskillEffectLogicEnum.dead_count_enemy_fix_attack_speed,
      BattleskillEffectLogicEnum.dead_count_enemy_ratio_strength,
      BattleskillEffectLogicEnum.dead_count_enemy_ratio_vitality,
      BattleskillEffectLogicEnum.dead_count_enemy_ratio_intelligence,
      BattleskillEffectLogicEnum.dead_count_enemy_ratio_mind,
      BattleskillEffectLogicEnum.dead_count_enemy_ratio_agility,
      BattleskillEffectLogicEnum.dead_count_enemy_ratio_dexterity,
      BattleskillEffectLogicEnum.dead_count_enemy_ratio_luck,
      BattleskillEffectLogicEnum.dead_count_enemy_ratio_move,
      BattleskillEffectLogicEnum.dead_count_enemy_ratio_physical_attack,
      BattleskillEffectLogicEnum.dead_count_enemy_ratio_magic_attack,
      BattleskillEffectLogicEnum.dead_count_enemy_ratio_physical_defense,
      BattleskillEffectLogicEnum.dead_count_enemy_ratio_magic_defense,
      BattleskillEffectLogicEnum.dead_count_enemy_ratio_hit,
      BattleskillEffectLogicEnum.dead_count_enemy_ratio_evasion,
      BattleskillEffectLogicEnum.dead_count_enemy_ratio_critical,
      BattleskillEffectLogicEnum.dead_count_enemy_ratio_critical_evasion,
      BattleskillEffectLogicEnum.dead_count_enemy_ratio_attack_speed,
      BattleskillEffectLogicEnum.dead_count_complex_fix_strength,
      BattleskillEffectLogicEnum.dead_count_complex_fix_vitality,
      BattleskillEffectLogicEnum.dead_count_complex_fix_intelligence,
      BattleskillEffectLogicEnum.dead_count_complex_fix_mind,
      BattleskillEffectLogicEnum.dead_count_complex_fix_agility,
      BattleskillEffectLogicEnum.dead_count_complex_fix_dexterity,
      BattleskillEffectLogicEnum.dead_count_complex_fix_luck,
      BattleskillEffectLogicEnum.dead_count_complex_fix_move,
      BattleskillEffectLogicEnum.dead_count_complex_fix_physical_attack,
      BattleskillEffectLogicEnum.dead_count_complex_fix_magic_attack,
      BattleskillEffectLogicEnum.dead_count_complex_fix_physical_defense,
      BattleskillEffectLogicEnum.dead_count_complex_fix_magic_defense,
      BattleskillEffectLogicEnum.dead_count_complex_fix_hit,
      BattleskillEffectLogicEnum.dead_count_complex_fix_evasion,
      BattleskillEffectLogicEnum.dead_count_complex_fix_critical,
      BattleskillEffectLogicEnum.dead_count_complex_fix_critical_evasion,
      BattleskillEffectLogicEnum.dead_count_complex_fix_attack_speed,
      BattleskillEffectLogicEnum.dead_count_complex_ratio_strength,
      BattleskillEffectLogicEnum.dead_count_complex_ratio_vitality,
      BattleskillEffectLogicEnum.dead_count_complex_ratio_intelligence,
      BattleskillEffectLogicEnum.dead_count_complex_ratio_mind,
      BattleskillEffectLogicEnum.dead_count_complex_ratio_agility,
      BattleskillEffectLogicEnum.dead_count_complex_ratio_dexterity,
      BattleskillEffectLogicEnum.dead_count_complex_ratio_luck,
      BattleskillEffectLogicEnum.dead_count_complex_ratio_move,
      BattleskillEffectLogicEnum.dead_count_complex_ratio_physical_attack,
      BattleskillEffectLogicEnum.dead_count_complex_ratio_magic_attack,
      BattleskillEffectLogicEnum.dead_count_complex_ratio_physical_defense,
      BattleskillEffectLogicEnum.dead_count_complex_ratio_magic_defense,
      BattleskillEffectLogicEnum.dead_count_complex_ratio_hit,
      BattleskillEffectLogicEnum.dead_count_complex_ratio_evasion,
      BattleskillEffectLogicEnum.dead_count_complex_ratio_critical,
      BattleskillEffectLogicEnum.dead_count_complex_ratio_critical_evasion,
      BattleskillEffectLogicEnum.dead_count_complex_ratio_attack_speed
    };
    private static BattleskillEffectLogicEnum[] onemanChargeEffectsEnum = new BattleskillEffectLogicEnum[34]
    {
      BattleskillEffectLogicEnum.oneman_charge_fix_strength,
      BattleskillEffectLogicEnum.oneman_charge_fix_vitality,
      BattleskillEffectLogicEnum.oneman_charge_fix_intelligence,
      BattleskillEffectLogicEnum.oneman_charge_fix_mind,
      BattleskillEffectLogicEnum.oneman_charge_fix_agility,
      BattleskillEffectLogicEnum.oneman_charge_fix_dexterity,
      BattleskillEffectLogicEnum.oneman_charge_fix_luck,
      BattleskillEffectLogicEnum.oneman_charge_fix_move,
      BattleskillEffectLogicEnum.oneman_charge_fix_physical_attack,
      BattleskillEffectLogicEnum.oneman_charge_fix_magic_attack,
      BattleskillEffectLogicEnum.oneman_charge_fix_physical_defense,
      BattleskillEffectLogicEnum.oneman_charge_fix_magic_defense,
      BattleskillEffectLogicEnum.oneman_charge_fix_hit,
      BattleskillEffectLogicEnum.oneman_charge_fix_evasion,
      BattleskillEffectLogicEnum.oneman_charge_fix_critical,
      BattleskillEffectLogicEnum.oneman_charge_fix_critical_evasion,
      BattleskillEffectLogicEnum.oneman_charge_ratio_strength,
      BattleskillEffectLogicEnum.oneman_charge_ratio_vitality,
      BattleskillEffectLogicEnum.oneman_charge_ratio_intelligence,
      BattleskillEffectLogicEnum.oneman_charge_ratio_mind,
      BattleskillEffectLogicEnum.oneman_charge_ratio_agility,
      BattleskillEffectLogicEnum.oneman_charge_ratio_dexterity,
      BattleskillEffectLogicEnum.oneman_charge_ratio_luck,
      BattleskillEffectLogicEnum.oneman_charge_ratio_move,
      BattleskillEffectLogicEnum.oneman_charge_ratio_physical_attack,
      BattleskillEffectLogicEnum.oneman_charge_ratio_magic_attack,
      BattleskillEffectLogicEnum.oneman_charge_ratio_physical_defense,
      BattleskillEffectLogicEnum.oneman_charge_ratio_magic_defense,
      BattleskillEffectLogicEnum.oneman_charge_ratio_hit,
      BattleskillEffectLogicEnum.oneman_charge_ratio_evasion,
      BattleskillEffectLogicEnum.oneman_charge_ratio_critical,
      BattleskillEffectLogicEnum.oneman_charge_ratio_critical_evasion,
      BattleskillEffectLogicEnum.oneman_charge_fix_attack_speed,
      BattleskillEffectLogicEnum.oneman_charge_ratio_attack_speed
    };
    private const int CriticalDamageRate = 3;
    private const int EDGE_MAX_COST = 10000;
    private const int costMax = 1000000000;
    private const float MulBaseValue = 10000f;

    public static bool useNeighbors(BL.Unit u, BL env_ = null)
    {
      if (env_ == null)
        env_ = BattleFuncs.env;
      return env_.battleInfo.pvp || env_.battleInfo.gvg || env_.battleInfo.isEarthMode || env_.playerUnits.value.Contains(u);
    }

    public static AttackStatus selectCounterAttackStatus(AttackStatus[] attacks)
    {
      return attacks.Length == 0 ? (AttackStatus) null : ((IEnumerable<AttackStatus>) attacks).Where<AttackStatus>((Func<AttackStatus, bool>) (x => x.magicBullet == null || x.magicBullet.isAttack)).OrderBy<AttackStatus, int>((Func<AttackStatus, int>) (x => x.magicBullet != null ? x.magicBullet.cost : 0)).ThenByDescending<AttackStatus, int>((Func<AttackStatus, int>) (x =>
      {
        float damageRate = x.duelParameter.DamageRate;
        x.duelParameter.DamageRate *= x.elementAttackRate * x.attackClassificationRate * x.normalDamageRate;
        int num = Mathf.Max(Mathf.FloorToInt(x.originalAttack), 1) * x.normalAttackCount;
        x.duelParameter.DamageRate = damageRate;
        return num;
      })).FirstOrDefault<AttackStatus>();
    }

    public static AttackStatus getCounterAttack(
      BL.ISkillEffectListUnit attack,
      BL.Panel attackPanel,
      BL.Unit[] attackNeighbors,
      BL.ISkillEffectListUnit defense,
      BL.Panel defensePanel,
      BL.Unit[] defenseNeighbors,
      bool isAttack,
      bool isHeal,
      int move_distance,
      int move_range,
      bool isAI = false,
      bool checkInvokeAbsoluteCounterAttack = false)
    {
      return BattleFuncs.selectCounterAttackStatus(BattleFuncs.getAttackStatusArray(defense, defensePanel, defenseNeighbors, attack, attackPanel, attackNeighbors, defense.hp, isAttack, isHeal, move_distance, move_range, isAI, false, checkInvokeAbsoluteCounterAttack));
    }

    public static bool isCounterAttack(
      BL.ISkillEffectListUnit attack,
      BL.Panel attackPanel,
      BL.Unit[] attackNeighbors,
      BL.ISkillEffectListUnit defense,
      BL.Panel defensePanel,
      BL.Unit[] defenseNeighbors,
      int defenseHp,
      bool isAI)
    {
      return (uint) BattleFuncs.getAttackStatusArray(defense, defensePanel, defenseNeighbors, attack, attackPanel, attackNeighbors, defenseHp, false, false, 0, -1, isAI, true, false).Length > 0U;
    }

    public static AttackStatus[] getAttackStatusArray(
      BL.ISkillEffectListUnit attack,
      BL.Panel attackPanel,
      BL.Unit[] attackNeighbors,
      BL.ISkillEffectListUnit defense,
      BL.Panel defensePanel,
      BL.Unit[] defenseNeighbors,
      int attackHp,
      bool isAttack,
      bool isHeal,
      int move_distance = 0,
      int move_range = -1,
      bool isAI = false,
      bool makeEmptyArray = false,
      bool checkInvokeAbsoluteCounterAttack = false)
    {
      if (attack.IsDontAction)
        return new AttackStatus[0];
      AttackStatus[] ret;
      if (BattleFuncs.env.getAttackStatusCache(attack, attackPanel, attackNeighbors, defense, defensePanel, defenseNeighbors, attackHp, isAttack, isHeal, move_distance, move_range, isAI, out ret))
        return ret;
      int distance = BL.fieldDistance(attackPanel, defensePanel);
      BL.Unit originalUnit = attack.originalUnit;
      BL.Unit.GearRange gearRange = attack.gearRange();
      Tuple<int, int> addRange = attackPanel.getEffectsAddRange(originalUnit);
      int num = (int) originalUnit.weapon.gear.kind.Enum;
      bool flag = false;
      bool magicWarriorFlag = originalUnit.unit.magic_warrior_flag;
      AttackStatus[] array1 = new AttackStatus[0];
      AttackStatus[] array2 = new AttackStatus[0];
      List<AttackStatus> first = new List<AttackStatus>(originalUnit.optionWeapons.Length);
      bool checkAbsolute = checkInvokeAbsoluteCounterAttack;
      while (true)
      {
        if (originalUnit.magicBullets.Length != 0)
        {
          flag = true;
          IEnumerable<BL.MagicBullet> source = ((IEnumerable<BL.MagicBullet>) originalUnit.magicBullets).Where<BL.MagicBullet>((Func<BL.MagicBullet, bool>) (x =>
          {
            if (x != null && x.isHeal == isHeal && attackHp > x.cost)
            {
              BL.Unit.MagicRange magicRange = attack.magicRange(x);
              if (checkAbsolute || NC.IsReach(magicRange.Min + addRange.Item1, magicRange.Max + addRange.Item2, distance))
                return true;
            }
            return false;
          }));
          if (!makeEmptyArray)
            array1 = source.Select<BL.MagicBullet, AttackStatus>((Func<BL.MagicBullet, AttackStatus>) (bullet => BattleFuncs.getAttackStatus(bullet, (BL.Weapon) null, attack, attackPanel, attackNeighbors, defense, defensePanel, defenseNeighbors, isAttack, isHeal, move_distance, move_range, isAI, checkInvokeAbsoluteCounterAttack))).ToArray<AttackStatus>();
          else
            Array.Resize<AttackStatus>(ref array1, source.Count<BL.MagicBullet>());
        }
        if (!isHeal && !flag | magicWarriorFlag && (checkAbsolute || NC.IsReach(gearRange.Min + addRange.Item1, gearRange.Max + addRange.Item2, distance)))
        {
          if (!makeEmptyArray)
            array2 = new AttackStatus[1]
            {
              BattleFuncs.getAttackStatus((BL.MagicBullet) null, (BL.Weapon) null, attack, attackPanel, attackNeighbors, defense, defensePanel, defenseNeighbors, isAttack, isHeal, move_distance, move_range, isAI, checkInvokeAbsoluteCounterAttack)
            };
          else
            Array.Resize<AttackStatus>(ref array2, 1);
        }
        if (!isHeal && originalUnit.optionWeapons.Length != 0)
        {
          for (int index = 0; index < originalUnit.optionWeapons.Length; ++index)
          {
            BL.Weapon optionWeapon = originalUnit.optionWeapons[index];
            IAttackMethod attackMethod = optionWeapon.attackMethod;
            int minRange = attackMethod.skill.min_range;
            int maxRange = attackMethod.skill.max_range;
            if (checkAbsolute || NC.IsReach(minRange + addRange.Item1, maxRange + addRange.Item2, distance))
              first.Add(BattleFuncs.getAttackStatus((BL.MagicBullet) null, optionWeapon, attack, attackPanel, attackNeighbors, defense, defensePanel, defenseNeighbors, isAttack, isHeal, move_distance, move_range, isAI, checkInvokeAbsoluteCounterAttack));
          }
        }
        ret = ((IEnumerable<AttackStatus>) array2).Concat<AttackStatus>(first.Concat<AttackStatus>((IEnumerable<AttackStatus>) array1)).ToArray<AttackStatus>();
        if (!isAttack)
        {
          if (!checkAbsolute)
          {
            if (ret.Length < 1 && ((IEnumerable<BattleskillEffect>) attack.originalUnit.absoluteCounterAttackEffects).Any<BattleskillEffect>((Func<BattleskillEffect, bool>) (effect => BattleFuncs.checkInvokeSkillEffect(BattleFuncs.PackedSkillEffect.Create(effect), attack, defense, new int?(), (Judgement.NonBattleParameter.FromPlayerUnitCache) null, (Judgement.NonBattleParameter.FromPlayerUnitCache) null, new int?(), new int?()) && !attack.IsDontUseSkill(effect.skill.ID))) && !BattleFuncs.isSkillsAndEffectsInvalid(attack, defense, (BL.SkillEffect) null))
              checkAbsolute = true;
            else
              goto label_27;
          }
          else
            break;
        }
        else
          goto label_27;
      }
      if (!makeEmptyArray)
      {
        foreach (AttackStatus attackStatus in ret)
          attackStatus.isAbsoluteCounterAttack = true;
      }
label_27:
      if (makeEmptyArray || ret.Length < 1)
        return ret;
      ret = !(isAttack & isHeal) ? ((IEnumerable<AttackStatus>) ret).OrderByDescending<AttackStatus, int>((Func<AttackStatus, int>) (status => status.attack)).ToArray<AttackStatus>() : ((IEnumerable<AttackStatus>) ret).OrderBy<AttackStatus, int>((Func<AttackStatus, int>) (status => status.magicBullet == null ? 0 : status.magicBullet.cost)).ToArray<AttackStatus>();
      return checkInvokeAbsoluteCounterAttack ? ret : BattleFuncs.env.setAttackStatusCache(attack, attackPanel, attackNeighbors, defense, defensePanel, defenseNeighbors, attackHp, isAttack, isHeal, move_distance, move_range, isAI, ret);
    }

    private static AttackStatus getAttackStatus(
      BL.MagicBullet magicBullet,
      BL.Weapon weapon,
      BL.ISkillEffectListUnit attack,
      BL.Panel attackPanel,
      BL.Unit[] attackNeighbors,
      BL.ISkillEffectListUnit defense,
      BL.Panel defensePanel,
      BL.Unit[] defenseNeighbors,
      bool isAttack,
      bool isHeal,
      int move_distance,
      int move_range,
      bool isAI,
      bool checkInvokeAbsoluteCounterAttack)
    {
      BL.ForceID forceId1 = BattleFuncs.env.getForceID(attack.originalUnit);
      BL.ForceID forceId2 = BattleFuncs.env.getForceID(defense.originalUnit);
      BL.ForceID[] targetForce1 = BattleFuncs.env.getTargetForce(attack.originalUnit, attack.IsCharm);
      BL.ForceID[] targetForce2 = BattleFuncs.env.getTargetForce(defense.originalUnit, defense.IsCharm);
      IEnumerable<BL.Unit> beAttackDeckUnits = (IEnumerable<BL.Unit>) BattleFuncs.env.forceUnits(forceId1).value;
      IEnumerable<BL.Unit> beAttackTargetDeckUnits = ((IEnumerable<BL.ForceID>) targetForce1).SelectMany<BL.ForceID, BL.Unit>((Func<BL.ForceID, IEnumerable<BL.Unit>>) (x => (IEnumerable<BL.Unit>) BattleFuncs.env.forceUnits(x).value));
      IEnumerable<BL.Unit> beDefenseDeckUnits;
      IEnumerable<BL.Unit> beDefenseTargetDeckUnits;
      if (forceId1 == forceId2)
      {
        beDefenseDeckUnits = beAttackDeckUnits;
        beDefenseTargetDeckUnits = beAttackTargetDeckUnits;
      }
      else
      {
        beDefenseDeckUnits = (IEnumerable<BL.Unit>) BattleFuncs.env.forceUnits(forceId2).value;
        beDefenseTargetDeckUnits = ((IEnumerable<BL.ForceID>) targetForce2).SelectMany<BL.ForceID, BL.Unit>((Func<BL.ForceID, IEnumerable<BL.Unit>>) (x => (IEnumerable<BL.Unit>) BattleFuncs.env.forceUnits(x).value));
      }
      bool flag;
      if (weapon != null)
        flag = false;
      else if (attack.originalUnit.unit.magic_warrior_flag)
      {
        flag = magicBullet != null;
      }
      else
      {
        GearGear gear = attack.originalUnit.weapon.gear;
        flag = (gear.attack_type == GearAttackType.none ? (int) attack.originalUnit.playerUnit.initial_gear.attack_type : (int) gear.attack_type) == 6;
      }
      Judgement.BeforeDuelParameter single = Judgement.BeforeDuelParameter.CreateSingle(attack, magicBullet, attackPanel.landform, attackNeighbors, defense, (BL.MagicBullet) null, defensePanel.landform, defenseNeighbors, isAttack, BL.fieldDistance(attackPanel, defensePanel), move_distance, move_range, attack.hp, defense.hp, targetForce1, targetForce2, attackPanel, defensePanel, beAttackDeckUnits, beDefenseDeckUnits, beAttackTargetDeckUnits, beDefenseTargetDeckUnits, isHeal, isAI, new bool?(flag), checkInvokeAbsoluteCounterAttack, weapon);
      float num = 1f;
      float attackDamageRate = attack.originalUnit.playerUnit.normalAttackDamageRate;
      AttackStatus attackStatus = new AttackStatus();
      attackStatus.duelParameter = single;
      attackStatus.isMagic = flag;
      attackStatus.magicBullet = magicBullet;
      attackStatus.weapon = weapon;
      attackStatus.attackRate = num;
      attackStatus.normalDamageRate = attackDamageRate;
      attackStatus.normalAttackCount = attack.originalUnit.playerUnit.normalAttackCount;
      attackStatus.calcElementAttackRate(attack, defense);
      attackStatus.calcAttackClassificationRate(attack, defense);
      return attackStatus;
    }

    public static void makeAttackStatusArgs(
      BL.UnitPosition attack,
      BL.UnitPosition defense,
      bool isAttack,
      bool isHeal,
      out BL.Unit[] attackNeighbors,
      out BL.Unit[] defenseNeighbors,
      out int move_distance,
      out int move_range,
      BL.Panel originalPanel = null,
      BL.Panel movePanel = null,
      bool isAI = false)
    {
      if (movePanel == null)
        movePanel = !isAttack ? BattleFuncs.env.getFieldPanel(defense, false) : BattleFuncs.env.getFieldPanel(attack, false);
      int row1;
      int column1;
      int row2;
      int column2;
      if (isAttack)
      {
        row1 = movePanel.row;
        column1 = movePanel.column;
        row2 = defense.row;
        column2 = defense.column;
      }
      else
      {
        row1 = attack.row;
        column1 = attack.column;
        row2 = movePanel.row;
        column2 = movePanel.column;
      }
      attackNeighbors = BattleFuncs.useNeighbors(attack.unit, (BL) null) ? BattleFuncs.getFourForceUnits(row1, column1, BattleFuncs.getForceIDArray(attack.unit, BattleFuncs.env), isAI).Select<BL.UnitPosition, BL.Unit>((Func<BL.UnitPosition, BL.Unit>) (x => x.unit)).ToArray<BL.Unit>() : BattleFuncs.ZeroArrayUnit;
      defenseNeighbors = BattleFuncs.useNeighbors(defense.unit, (BL) null) ? BattleFuncs.getFourForceUnits(row2, column2, BattleFuncs.getForceIDArray(defense.unit, BattleFuncs.env), isAI).Select<BL.UnitPosition, BL.Unit>((Func<BL.UnitPosition, BL.Unit>) (x => x.unit)).ToArray<BL.Unit>() : BattleFuncs.ZeroArrayUnit;
      if (isHeal)
      {
        move_distance = 0;
        move_range = -1;
      }
      else if (isAttack)
      {
        if (originalPanel == null)
          originalPanel = BattleFuncs.env.getFieldPanel(attack, true);
        move_distance = !BattleFuncs.hasEnabledMoveDistanceEffects((BL.ISkillEffectListUnit) attack.unit) ? 0 : BL.fieldDistance(originalPanel, BattleFuncs.env.getFieldPanel(defense, false)) - 1;
        if (BattleFuncs.hasEnabledRangeEffects((BL.ISkillEffectListUnit) attack.unit) || BattleFuncs.hasEnabledRangeEffects((BL.ISkillEffectListUnit) defense.unit))
          move_range = BL.fieldDistance(originalPanel, movePanel);
        else
          move_range = -1;
      }
      else
      {
        if (originalPanel == null)
          originalPanel = BattleFuncs.env.getFieldPanel(defense, true);
        move_distance = !BattleFuncs.hasEnabledMoveDistanceEffects((BL.ISkillEffectListUnit) defense.unit) ? 0 : BL.fieldDistance(originalPanel, BattleFuncs.env.getFieldPanel(attack, false)) - 1;
        if (BattleFuncs.hasEnabledRangeEffects((BL.ISkillEffectListUnit) defense.unit) || BattleFuncs.hasEnabledRangeEffects((BL.ISkillEffectListUnit) attack.unit))
          move_range = BL.fieldDistance(originalPanel, movePanel);
        else
          move_range = -1;
      }
    }

    public static AttackStatus[] getAttackStatusArray(
      BL.UnitPosition attack,
      BL.UnitPosition defense,
      bool isAttack,
      bool isHeal,
      bool isAI = false)
    {
      BL.Unit[] attackNeighbors;
      BL.Unit[] defenseNeighbors;
      int move_distance;
      int move_range;
      BattleFuncs.makeAttackStatusArgs(attack, defense, isAttack, isHeal, out attackNeighbors, out defenseNeighbors, out move_distance, out move_range, (BL.Panel) null, (BL.Panel) null, isAI);
      BL.ISkillEffectListUnit attack1 = attack is BL.ISkillEffectListUnit ? attack as BL.ISkillEffectListUnit : (BL.ISkillEffectListUnit) attack.unit;
      BL.ISkillEffectListUnit defense1 = defense is BL.ISkillEffectListUnit ? defense as BL.ISkillEffectListUnit : (BL.ISkillEffectListUnit) defense.unit;
      return BattleFuncs.getAttackStatusArray(attack1, BattleFuncs.env.getFieldPanel(attack, false), attackNeighbors, defense1, BattleFuncs.env.getFieldPanel(defense, false), defenseNeighbors, attack1.hp, isAttack, isHeal, move_distance, move_range, isAI, false, false);
    }

    public static DuelResult calcDuel(
      AttackStatus attackStatus,
      BL.ISkillEffectListUnit attack,
      BL.Panel attackPanel,
      BL.ISkillEffectListUnit defense,
      BL.Panel defensePanel,
      int move_distance,
      int move_range,
      bool isAI = false)
    {
      return attackStatus.isHeal ? BattleFuncs.calcDuelHeal(attackStatus, attack, attackPanel, defense, defensePanel, isAI) : BattleFuncs.calcDuelAttack(attackStatus, attack, attackPanel, defense, defensePanel, move_distance, move_range, isAI);
    }

    public static DuelResult calcDuelHeal(
      AttackStatus attackStatus,
      BL.ISkillEffectListUnit attack,
      BL.Panel attackPanel,
      BL.ISkillEffectListUnit defense,
      BL.Panel defensePanel,
      bool isAI = false)
    {
      DuelResult duelResult = new DuelResult()
      {
        moveUnit = attack.originalUnit,
        moveUnitIsCharm = attack.IsCharm,
        isColosseum = false,
        isPlayerAttack = attack.originalUnit.isPlayerControl,
        attack = attack.originalUnit,
        defense = defense.originalUnit,
        attackAttackStatus = attackStatus,
        defenseAttackStatus = (AttackStatus) null,
        turns = new BL.DuelTurn[0],
        attackDamage = attackStatus.magicBullet.cost,
        defenseDamage = -attackStatus.heal_attack,
        attackFromDamage = 0,
        defenseFromDamage = 0
      };
      duelResult.isDieAttack = duelResult.attackDamage >= attack.hp;
      duelResult.isDieDefense = duelResult.defenseDamage >= defense.hp;
      return duelResult;
    }

    public static DuelResult calcDuelAttack(
      AttackStatus attackStatus,
      BL.ISkillEffectListUnit attack,
      BL.Panel attackPanel,
      BL.ISkillEffectListUnit defense,
      BL.Panel defensePanel,
      int move_distance,
      int move_range,
      bool isAI = false)
    {
      DuelResult duelResult = new DuelResult();
      duelResult.isColosseum = false;
      duelResult.moveUnit = attack.originalUnit;
      duelResult.moveUnitIsCharm = attack.IsCharm;
      BL.Unit[] array1 = BattleFuncs.getFourForceUnits(attackPanel.row, attackPanel.column, BattleFuncs.getForceIDArray(attack.originalUnit, BattleFuncs.env), isAI).Select<BL.UnitPosition, BL.Unit>((Func<BL.UnitPosition, BL.Unit>) (x => x.unit)).ToArray<BL.Unit>();
      BL.Unit[] array2 = BattleFuncs.getFourForceUnits(defensePanel.row, defensePanel.column, BattleFuncs.getForceIDArray(defense.originalUnit, BattleFuncs.env), isAI).Select<BL.UnitPosition, BL.Unit>((Func<BL.UnitPosition, BL.Unit>) (x => x.unit)).ToArray<BL.Unit>();
      AttackStatus defenseAS = BattleFuncs.getCounterAttack(attack, attackPanel, array1, defense, defensePanel, array2, false, false, move_distance, move_range, isAI, false);
      BattleskillSkill battleskillSkill = (BattleskillSkill) null;
      if (defenseAS == null && !defense.IsDontAction && !BattleFuncs.isSkillsAndEffectsInvalid(defense, attack, (BL.SkillEffect) null))
      {
        List<Tuple<BattleskillEffect, int, BattleFuncs.PackedSkillEffect>> tupleList = new List<Tuple<BattleskillEffect, int, BattleFuncs.PackedSkillEffect>>();
        foreach (Tuple<BattleskillSkill, int, int> unitAndGearSkill in defense.originalUnit.unitAndGearSkills)
        {
          bool flag = false;
          foreach (BattleskillEffect effect in unitAndGearSkill.Item1.Effects)
          {
            if (effect.EffectLogic.Enum == BattleskillEffectLogicEnum.absolute_counter_attack && (double) effect.GetFloat(BattleskillEffectLogicArgumentEnum.percentage_invocation) < 200.0 && effect.checkLevel(unitAndGearSkill.Item2))
            {
              BattleFuncs.PackedSkillEffect pse = BattleFuncs.PackedSkillEffect.Create(effect);
              if (BattleFuncs.checkInvokeSkillEffect(pse, defense, attack, new int?(), (Judgement.NonBattleParameter.FromPlayerUnitCache) null, (Judgement.NonBattleParameter.FromPlayerUnitCache) null, new int?(), new int?()))
              {
                if (!flag)
                {
                  if (!defense.IsDontUseSkill(unitAndGearSkill.Item1.ID))
                    flag = true;
                  else
                    break;
                }
                tupleList.Add(Tuple.Create<BattleskillEffect, int, BattleFuncs.PackedSkillEffect>(effect, unitAndGearSkill.Item2, pse));
              }
            }
          }
        }
        if (tupleList.Count > 0)
        {
          AttackStatus counterAttack = BattleFuncs.getCounterAttack(attack, attackPanel, array1, defense, defensePanel, array2, false, false, move_distance, move_range, isAI, true);
          if (counterAttack != null)
          {
            foreach (Tuple<BattleskillEffect, int, BattleFuncs.PackedSkillEffect> tuple in tupleList)
            {
              BattleFuncs.PackedSkillEffect packedSkillEffect = tuple.Item3;
              if (BattleFuncs.isInvoke(defense, attack, attackStatus.duelParameter.defenderUnitParameter, attackStatus.duelParameter.attackerUnitParameter, counterAttack, attackStatus, tuple.Item2, tuple.Item1.GetFloat(BattleskillEffectLogicArgumentEnum.percentage_invocation), BattleFuncs.env.random, true, defense.hp, attack.hp, new int?(), new float?(), (BattleskillEffect) null, packedSkillEffect.HasKey(BattleskillEffectLogicArgumentEnum.base_invocation) ? packedSkillEffect.GetFloat(BattleskillEffectLogicArgumentEnum.base_invocation) : 0.0f, packedSkillEffect.HasKey(BattleskillEffectLogicArgumentEnum.invocation_skill_ratio) ? packedSkillEffect.GetFloat(BattleskillEffectLogicArgumentEnum.invocation_skill_ratio) : 1f, packedSkillEffect.HasKey(BattleskillEffectLogicArgumentEnum.invocation_luck_ratio) ? packedSkillEffect.GetFloat(BattleskillEffectLogicArgumentEnum.invocation_luck_ratio) : 1f, (List<BattleFuncs.InvalidSpecificSkillLogic>) null))
              {
                defenseAS = counterAttack;
                battleskillSkill = tuple.Item1.skill;
                break;
              }
            }
          }
        }
      }
      bool flag1 = defenseAS != null && defenseAS.isAbsoluteCounterAttack;
      Tuple<BattleskillSkill, int, int> tuple1 = (Tuple<BattleskillSkill, int, int>) null;
      if (!defense.IsDontAction && defenseAS != null && !BattleFuncs.isSkillsAndEffectsInvalid(defense, attack, (BL.SkillEffect) null))
      {
        tuple1 = ((IEnumerable<Tuple<BattleskillSkill, int, int>>) defense.originalUnit.unitAndGearSkills).Where<Tuple<BattleskillSkill, int, int>>((Func<Tuple<BattleskillSkill, int, int>, bool>) (x => !defense.IsDontUseSkill(x.Item1.ID) && BattleFuncs.CreatePackedSkillEffects(x.Item1, x.Item2).Any<BattleFuncs.PackedSkillEffect>((Func<BattleFuncs.PackedSkillEffect, bool>) (effect => effect.LogicEnum() == BattleskillEffectLogicEnum.ambush && effect.GetHasKeyEffect(BattleskillEffectLogicArgumentEnum.invoke_gamemode).isEnableGameMode(BattleskillInvokeGameModeEnum.quest) && (!effect.HasKey(BattleskillEffectLogicArgumentEnum.gear_kind_id) || effect.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) == 0 || effect.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) == defense.originalUnit.unit.kind.ID) && ((!effect.HasKey(BattleskillEffectLogicArgumentEnum.target_gear_kind_id) || effect.GetInt(BattleskillEffectLogicArgumentEnum.target_gear_kind_id) == 0 || effect.GetInt(BattleskillEffectLogicArgumentEnum.target_gear_kind_id) == attack.originalUnit.unit.kind.ID) && (!effect.HasKey(BattleskillEffectLogicArgumentEnum.element) || effect.GetInt(BattleskillEffectLogicArgumentEnum.element) == 0 || (CommonElement) effect.GetInt(BattleskillEffectLogicArgumentEnum.element) == defense.originalUnit.playerUnit.GetElement())) && ((!effect.HasKey(BattleskillEffectLogicArgumentEnum.target_element) || effect.GetInt(BattleskillEffectLogicArgumentEnum.target_element) == 0 || (CommonElement) effect.GetInt(BattleskillEffectLogicArgumentEnum.target_element) == attack.originalUnit.playerUnit.GetElement()) && (!effect.HasKey(BattleskillEffectLogicArgumentEnum.job_id) || effect.GetInt(BattleskillEffectLogicArgumentEnum.job_id) == 0 || effect.GetInt(BattleskillEffectLogicArgumentEnum.job_id) == defense.originalUnit.job.ID) && ((!effect.HasKey(BattleskillEffectLogicArgumentEnum.target_job_id) || effect.GetInt(BattleskillEffectLogicArgumentEnum.target_job_id) == 0 || effect.GetInt(BattleskillEffectLogicArgumentEnum.target_job_id) == attack.originalUnit.job.ID) && (!effect.HasKey(BattleskillEffectLogicArgumentEnum.family_id) || effect.GetInt(BattleskillEffectLogicArgumentEnum.family_id) == 0 || defense.originalUnit.playerUnit.HasFamily((UnitFamily) effect.GetInt(BattleskillEffectLogicArgumentEnum.family_id))))) && ((!effect.HasKey(BattleskillEffectLogicArgumentEnum.target_family_id) || effect.GetInt(BattleskillEffectLogicArgumentEnum.target_family_id) == 0 || attack.originalUnit.playerUnit.HasFamily((UnitFamily) effect.GetInt(BattleskillEffectLogicArgumentEnum.target_family_id))) && (!effect.HasKey(BattleskillEffectLogicArgumentEnum.attacker_attack_type) || effect.GetInt(BattleskillEffectLogicArgumentEnum.attacker_attack_type) == 0 || effect.GetInt(BattleskillEffectLogicArgumentEnum.attacker_attack_type) == 1 && !attackStatus.isMagic || effect.GetInt(BattleskillEffectLogicArgumentEnum.attacker_attack_type) == 2 && attackStatus.isMagic)) && (double) defense.hp <= ((double) effect.GetFloat(BattleskillEffectLogicArgumentEnum.percentage) * 100.0 + (double) (x.Item2 * 2)) / 100.0 * (double) defense.originalUnit.parameter.Hp)))).FirstOrDefault<Tuple<BattleskillSkill, int, int>>();
        if (tuple1 != null)
        {
          BL.ISkillEffectListUnit skillEffectListUnit = attack;
          attack = defense;
          defense = skillEffectListUnit;
          AttackStatus attackStatus1 = attackStatus;
          attackStatus = defenseAS;
          defenseAS = attackStatus1;
          BL.Panel panel = attackPanel;
          attackPanel = defensePanel;
          defensePanel = panel;
        }
      }
      duelResult.beforeAttakerAilmentEffectIDs = attack.skillEffects.Where(BattleskillSkillType.ailment, false).Select<BattleskillSkill, int>((Func<BattleskillSkill, int>) (x => x.ailment_effect.ID)).ToArray<int>();
      duelResult.beforeDefenderAilmentEffectIDs = defense.skillEffects.Where(BattleskillSkillType.ailment, false).Select<BattleskillSkill, int>((Func<BattleskillSkill, int>) (x => x.ailment_effect.ID)).ToArray<int>();
      duelResult.isPlayerAttack = attack.originalUnit.isPlayerControl;
      duelResult.attack = attack.originalUnit;
      duelResult.defense = defense.originalUnit;
      duelResult.attackAttackStatus = attackStatus;
      duelResult.defenseAttackStatus = defenseAS;
      duelResult.distance = BL.fieldDistance(attackPanel, defensePanel);
      BL.SkillEffectList skillEffects1 = attack.skillEffects;
      BL.SkillEffectList skillEffects2 = defense.skillEffects;
      if (PerformanceConfig.GetInstance().IsNotUseDeepCopy)
      {
        attack.setSkillEffects(attack.skillEffects.Clone());
        defense.setSkillEffects(defense.skillEffects.Clone());
      }
      else
      {
        attack.setSkillEffects(CopyUtil.DeepCopy<BL.SkillEffectList>(skillEffects1));
        defense.setSkillEffects(CopyUtil.DeepCopy<BL.SkillEffectList>(skillEffects2));
      }
      duelResult.turns = BattleFuncs.calcTurns(attack, attackStatus, attackPanel, defense, defenseAS, defensePanel, duelResult.distance, isAI, tuple1 != null);
      attack.setSkillEffects(skillEffects1);
      defense.setSkillEffects(skillEffects2);
      if (battleskillSkill != null)
      {
        BL.Skill[] skillArray1 = tuple1 == null ? duelResult.turns[0].invokeDefenderDuelSkills : duelResult.turns[0].invokeDuelSkills;
        BL.Skill[] skillArray2 = new BL.Skill[skillArray1.Length + 1];
        skillArray2[0] = new BL.Skill()
        {
          id = battleskillSkill.ID
        };
        for (int index = 0; index < skillArray1.Length; ++index)
          skillArray2[index + 1] = skillArray1[index];
        if (tuple1 == null)
          duelResult.turns[0].invokeDefenderDuelSkills = skillArray2;
        else
          duelResult.turns[0].invokeDuelSkills = skillArray2;
      }
      if (tuple1 != null)
      {
        BL.Skill[] invokeDuelSkills = duelResult.turns[0].invokeDuelSkills;
        BL.Skill[] skillArray = new BL.Skill[invokeDuelSkills.Length + 1];
        skillArray[0] = new BL.Skill()
        {
          id = tuple1.Item1.ID
        };
        for (int index = 0; index < invokeDuelSkills.Length; ++index)
          skillArray[index + 1] = invokeDuelSkills[index];
        duelResult.turns[0].invokeDuelSkills = skillArray;
      }
      foreach (BL.DuelTurn turn in duelResult.turns)
      {
        if (turn.isAtackker)
        {
          duelResult.defenseDamage += turn.damage;
          duelResult.defenseDamage -= turn.defenderDrainDamage;
          duelResult.attackDamage += turn.counterDamage;
          duelResult.attackDamage -= turn.drainDamage;
        }
        else
        {
          duelResult.attackDamage += turn.damage;
          duelResult.attackDamage -= turn.defenderDrainDamage;
          duelResult.defenseDamage += turn.counterDamage;
          duelResult.defenseDamage -= turn.drainDamage;
        }
      }
      duelResult.attackFromDamage = ((IEnumerable<BL.DuelTurn>) duelResult.turns).Sum<BL.DuelTurn>((Func<BL.DuelTurn, int>) (x => x.isAtackker ? 0 : x.damage));
      duelResult.defenseFromDamage = ((IEnumerable<BL.DuelTurn>) duelResult.turns).Sum<BL.DuelTurn>((Func<BL.DuelTurn, int>) (x => !x.isAtackker ? 0 : x.damage));
      duelResult.isDieAttack = duelResult.attackDamage >= attack.hp;
      duelResult.isDieDefense = duelResult.defenseDamage >= defense.hp;
      duelResult.distance = flag1 ? 1 : BL.fieldDistance(attackPanel, defensePanel);
      BL.Unit[] array3 = BattleFuncs.getFourForceUnits(attackPanel.row, attackPanel.column, BattleFuncs.getForceIDArray(attack.originalUnit, BattleFuncs.env), isAI).Select<BL.UnitPosition, BL.Unit>((Func<BL.UnitPosition, BL.Unit>) (x => x.unit)).ToArray<BL.Unit>();
      BattleFuncs.BeforeDuelDuelSupport beforeDuelDuelSupport1 = BattleFuncs.GetBeforeDuelDuelSupport(attack, defense, array3);
      duelResult.attackDuelSupport = beforeDuelDuelSupport1.duelSupport;
      duelResult.attackDuelSupportHitIncr = beforeDuelDuelSupport1.hitIncr;
      duelResult.attackDuelSupportEvasionIncr = beforeDuelDuelSupport1.evasionIncr;
      duelResult.attackDuelSupportCriticalIncr = beforeDuelDuelSupport1.criticalIncr;
      duelResult.attackDuelSupportCriticalEvasionIncr = beforeDuelDuelSupport1.criticalEvasionIncr;
      BL.Unit[] array4 = BattleFuncs.getFourForceUnits(defensePanel.row, defensePanel.column, BattleFuncs.getForceIDArray(defense.originalUnit, BattleFuncs.env), isAI).Select<BL.UnitPosition, BL.Unit>((Func<BL.UnitPosition, BL.Unit>) (x => x.unit)).ToArray<BL.Unit>();
      BattleFuncs.BeforeDuelDuelSupport beforeDuelDuelSupport2 = BattleFuncs.GetBeforeDuelDuelSupport(defense, attack, array4);
      duelResult.defenseDuelSupport = beforeDuelDuelSupport2.duelSupport;
      duelResult.defenseDuelSupportHitIncr = beforeDuelDuelSupport2.hitIncr;
      duelResult.defenseDuelSupportEvasionIncr = beforeDuelDuelSupport2.evasionIncr;
      duelResult.defenseDuelSupportCriticalIncr = beforeDuelDuelSupport2.criticalIncr;
      duelResult.defenseDuelSupportCriticalEvasionIncr = beforeDuelDuelSupport2.criticalEvasionIncr;
      GearKindCorrelations kindCorrelations1 = MasterData.UniqueGearKindCorrelationsBy(attack.originalUnit.unit.kind, defense.originalUnit.unit.kind);
      BL.DuelHistory duelHistory1 = new BL.DuelHistory()
      {
        inflictTotalDamage = duelResult.defenseFromDamage,
        sufferTotalDamage = duelResult.attackFromDamage,
        criticalCount = ((IEnumerable<BL.DuelTurn>) duelResult.turns).Count<BL.DuelTurn>((Func<BL.DuelTurn, bool>) (x => x.isAtackker && x.isCritical)),
        inflictMaxDamage = ((IEnumerable<BL.DuelTurn>) duelResult.turns).Max<BL.DuelTurn>((Func<BL.DuelTurn, int>) (x => !x.isAtackker ? 0 : x.damage)),
        weekElementAttackCount = ((IEnumerable<BL.DuelTurn>) duelResult.turns).Count<BL.DuelTurn>((Func<BL.DuelTurn, bool>) (x => x.isAtackker && (double) x.attackStatus.elementAttackRate > 1.0)),
        weekKindAttackCount = kindCorrelations1 == null || !kindCorrelations1.is_advantage ? 0 : ((IEnumerable<BL.DuelTurn>) duelResult.turns).Count<BL.DuelTurn>((Func<BL.DuelTurn, bool>) (x => x.isAtackker)),
        targetUnitID = defense.originalUnit.playerUnit.id
      };
      GearKindCorrelations kindCorrelations2 = MasterData.UniqueGearKindCorrelationsBy(defense.originalUnit.unit.kind, attack.originalUnit.unit.kind);
      BL.DuelHistory duelHistory2 = new BL.DuelHistory()
      {
        inflictTotalDamage = duelResult.attackFromDamage,
        sufferTotalDamage = duelResult.defenseFromDamage,
        criticalCount = ((IEnumerable<BL.DuelTurn>) duelResult.turns).Count<BL.DuelTurn>((Func<BL.DuelTurn, bool>) (x => !x.isAtackker && x.isCritical)),
        inflictMaxDamage = ((IEnumerable<BL.DuelTurn>) duelResult.turns).Max<BL.DuelTurn>((Func<BL.DuelTurn, int>) (x => x.isAtackker ? 0 : x.damage)),
        weekElementAttackCount = ((IEnumerable<BL.DuelTurn>) duelResult.turns).Count<BL.DuelTurn>((Func<BL.DuelTurn, bool>) (x => !x.isAtackker && (double) x.attackStatus.elementAttackRate > 1.0)),
        weekKindAttackCount = kindCorrelations2 == null || !kindCorrelations2.is_advantage ? 0 : ((IEnumerable<BL.DuelTurn>) duelResult.turns).Count<BL.DuelTurn>((Func<BL.DuelTurn, bool>) (x => !x.isAtackker)),
        targetUnitID = attack.originalUnit.playerUnit.id
      };
      List<BL.DuelHistory> duelHistoryList = new List<BL.DuelHistory>();
      if (attack.originalUnit.duelHistory != null)
        duelHistoryList.AddRange((IEnumerable<BL.DuelHistory>) attack.originalUnit.duelHistory);
      duelHistoryList.Add(duelHistory1);
      attack.originalUnit.duelHistory = duelHistoryList.ToArray();
      duelHistoryList.Clear();
      if (defense.originalUnit.duelHistory != null)
        duelHistoryList.AddRange((IEnumerable<BL.DuelHistory>) defense.originalUnit.duelHistory);
      duelHistoryList.Add(duelHistory2);
      defense.originalUnit.duelHistory = duelHistoryList.ToArray();
      return duelResult;
    }

    public static DuelResult calcDuel(
      AttackStatus attackStatus,
      BL.UnitPosition attack,
      BL.UnitPosition defense,
      bool isAI = false)
    {
      return BattleFuncs.calcDuel(attackStatus, attack is BL.ISkillEffectListUnit ? attack as BL.ISkillEffectListUnit : (BL.ISkillEffectListUnit) attack.unit, BattleFuncs.env.getFieldPanel(attack, false), defense is BL.ISkillEffectListUnit ? defense as BL.ISkillEffectListUnit : (BL.ISkillEffectListUnit) defense.unit, BattleFuncs.env.getFieldPanel(defense, false), BL.fieldDistance(BattleFuncs.env.getFieldPanel(attack, true), BattleFuncs.env.getFieldPanel(defense, false)) - 1, BL.fieldDistance(BattleFuncs.env.getFieldPanel(attack, true), BattleFuncs.env.getFieldPanel(attack, false)), isAI);
    }

    private static BL.DuelTurn[] calcTurns(
      BL.ISkillEffectListUnit attack,
      AttackStatus attackAS,
      BL.Panel attackPanel,
      BL.ISkillEffectListUnit defense,
      AttackStatus defenseAS,
      BL.Panel defensePanel,
      int distance,
      bool isAI,
      bool invokedAmbush)
    {
      List<BL.DuelTurn> turns = new List<BL.DuelTurn>();
      TurnHp hp = new TurnHp();
      hp.attackerHp = attack.hp;
      hp.defenderHp = defense.hp;
      hp.attackerIsDontAction = attack.IsDontAction;
      hp.defenderIsDontAction = defense.IsDontAction;
      hp.attackerIsDontEvasion = attack.IsDontEvasion;
      hp.defenderIsDontEvasion = defense.IsDontEvasion;
      hp.attackerIsDontUseSkill = attack.IsDontAction;
      hp.defenderIsDontUseSkill = defense.IsDontAction;
      hp.attackerCantOneMore = false;
      hp.defenderCantOneMore = false;
      hp.otherHp = new Dictionary<BL.ISkillEffectListUnit, TurnOtherHp>();
      if (BattleFuncs.isSkillsAndEffectsInvalid(attack, defense, (BL.SkillEffect) null))
        hp.attackerIsDontUseSkill = true;
      if (BattleFuncs.isSkillsAndEffectsInvalid(defense, attack, (BL.SkillEffect) null))
        hp.defenderIsDontUseSkill = true;
      int num = BattleFuncs.checkRushInvoke(attack, defense, attackAS, defenseAS, attack.hp, defense.hp, BattleFuncs.env.random, new int?()) ? 1 : 0;
      bool flag = BattleFuncs.checkRushInvoke(defense, attack, defenseAS, attackAS, defense.hp, attack.hp, BattleFuncs.env.random, new int?());
      System.Action action1 = (System.Action) (() =>
      {
        if (hp.attackerCantOneMore || !BattleFuncs.canOneMore(attackAS.duelParameter.attackerUnitParameter, attackAS.duelParameter.defenderUnitParameter, attack, defense, true, invokedAmbush, BattleFuncs.env.random, attackAS, defenseAS, hp.attackerHp, hp.defenderHp, new int?(), false, attackPanel, defensePanel))
          return;
        BattleFuncs.calcMultiAttack(turns, hp, true, attack, attackAS, attackPanel, defense, defenseAS, defensePanel, distance, isAI, invokedAmbush, true);
      });
      System.Action action2 = (System.Action) (() =>
      {
        if (defenseAS == null || hp.defenderCantOneMore || !BattleFuncs.canOneMore(defenseAS.duelParameter.attackerUnitParameter, defenseAS.duelParameter.defenderUnitParameter, defense, attack, false, invokedAmbush, BattleFuncs.env.random, defenseAS, attackAS, hp.defenderHp, hp.attackerHp, new int?(), false, defensePanel, attackPanel))
          return;
        BattleFuncs.calcMultiAttack(turns, hp, false, defense, defenseAS, defensePanel, attack, attackAS, attackPanel, distance, isAI, invokedAmbush, true);
      });
      BattleFuncs.calcMultiAttack(turns, hp, true, attack, attackAS, attackPanel, defense, defenseAS, defensePanel, distance, isAI, invokedAmbush, false);
      if (num != 0)
      {
        action1();
        action1 = (System.Action) null;
      }
      BattleFuncs.calcMultiAttack(turns, hp, false, defense, defenseAS, defensePanel, attack, attackAS, attackPanel, distance, isAI, invokedAmbush, false);
      if (flag)
      {
        action2();
        action2 = (System.Action) null;
      }
      if (action1 != null)
        action1();
      if (action2 != null)
        action2();
      return turns.ToArray();
    }

    private static void calcMultiAttack(
      List<BL.DuelTurn> turns,
      TurnHp hp,
      bool isAttacker,
      BL.ISkillEffectListUnit attack,
      AttackStatus attackStatus,
      BL.Panel attackPanel,
      BL.ISkillEffectListUnit defense,
      AttackStatus defenseStatus,
      BL.Panel defensePanel,
      int distance,
      bool isAI,
      bool invokedAmbush,
      bool isOneMoreAttack)
    {
      if (attackStatus == null || hp.isDieAttackerOrDefender() || (isAttacker ? (hp.attackerIsDontAction ? 1 : 0) : (hp.defenderIsDontAction ? 1 : 0)) != 0)
        return;
      List<BL.DuelTurn> duelTurnList = new List<BL.DuelTurn>();
      int num = BattleFuncs.attackCount(attack, defense);
      int normalAttackCount = attack.originalUnit.playerUnit.normalAttackCount;
      int attackedCount = 0;
      bool isInvalidAttackDuelSkill = false;
      for (; attackedCount < num; ++attackedCount)
        BattleFuncs.calcSingleAttack(turns.Concat<BL.DuelTurn>((IEnumerable<BL.DuelTurn>) duelTurnList).ToList<BL.DuelTurn>(), duelTurnList, hp, isAttacker, attack, attackStatus, attackPanel, defense, defenseStatus, defensePanel, distance, BattleFuncs.env.random, isAI, new int?(), invokedAmbush, false, defense.hp, isOneMoreAttack, attackedCount, isInvalidAttackDuelSkill, false);
      bool flag1 = BattleFuncs.IsInvokeDuelSkill(duelTurnList);
      if (normalAttackCount >= 2 && duelTurnList.Count > 0 && !(hp.isDieAttackerOrDefender() & flag1))
      {
        if (hp.isDieAttackerOrDefender())
          isInvalidAttackDuelSkill = true;
        for (int index = num * normalAttackCount; attackedCount < index; ++attackedCount)
          BattleFuncs.calcSingleAttack(turns.Concat<BL.DuelTurn>((IEnumerable<BL.DuelTurn>) duelTurnList).ToList<BL.DuelTurn>(), duelTurnList, hp, isAttacker, attack, attackStatus, attackPanel, defense, defenseStatus, defensePanel, distance, BattleFuncs.env.random, isAI, new int?(), invokedAmbush, false, defense.hp, isOneMoreAttack, attackedCount, isInvalidAttackDuelSkill, true);
      }
      bool flag2 = BattleFuncs.IsInvokeDuelSkill(duelTurnList);
      if (duelTurnList.Count <= 0)
        return;
      int count = duelTurnList.Count;
      foreach (BL.DuelTurn duelTurn in duelTurnList)
      {
        if (flag2)
        {
          duelTurn.attackCount = 1;
          duelTurn.isDualSingleAttack = true;
        }
        else
          duelTurn.attackCount = count;
        turns.Add(duelTurn);
        --count;
      }
    }

    private static bool IsInvokeDuelSkill(List<BL.DuelTurn> turns)
    {
      foreach (BL.DuelTurn turn in turns)
      {
        if (((IEnumerable<BL.Skill>) turn.invokeDuelSkills).Any<BL.Skill>((Func<BL.Skill, bool>) (x =>
        {
          BattleskillGenre? genre1 = x.genre1;
          BattleskillGenre battleskillGenre = BattleskillGenre.attack;
          return genre1.GetValueOrDefault() == battleskillGenre & genre1.HasValue;
        })))
          return true;
      }
      return false;
    }

    private static BattleFuncs.AttackDamageData calcAttackDamage(
      XorShift random,
      bool isAttacker,
      TurnHp hp,
      BL.ISkillEffectListUnit attack,
      AttackStatus attackStatus,
      BL.Panel attackPanel,
      BL.ISkillEffectListUnit defense,
      AttackStatus defenseStatus,
      BL.Panel defensePanel,
      int distance,
      bool isAI,
      int? colosseumTurn,
      BattleDuelSkill baseDuelSkill,
      bool invokedAmbush,
      bool invokedPrayer,
      float? invokeRate,
      float? criticalRate,
      int defenderDuelBeginHp,
      bool isOneMoreAttack,
      bool isBiattack,
      int biAttackNo = 0,
      bool isInvalidAttackDuelSkill = false)
    {
      bool isColosseum = colosseumTurn.HasValue;
      BattleFuncs.AttackDamageData ret = new BattleFuncs.AttackDamageData();
      int? nullable1 = new int?();
      float? nullable2 = new float?();
      float? nullable3 = new float?();
      float? nullable4 = new float?();
      List<BattleFuncs.InvalidSpecificSkillLogic> invalidSkillLogics = BattleFuncs.GetInvalidSkillsAndLogics((IEnumerable<BattleskillEffect>) baseDuelSkill.invokeAttackerSkillEffects);
      if (!colosseumTurn.HasValue)
      {
        foreach (BL.SkillEffect skillEffect in BattleFuncs.gearSkillEffectFilter(attack.originalUnit, attack.skillEffects.Where(BattleskillEffectLogicEnum.invalid_specific_skills_and_logics, (Func<BL.SkillEffect, bool>) (x =>
        {
          BattleskillEffect effect = x.effect;
          int num = effect.GetInt(BattleskillEffectLogicArgumentEnum.is_attack_nc);
          bool flag = invokedAmbush ? !isAttacker : isAttacker;
          return (num != 1 || flag) && (!(num == 2 & flag) && !BattleFuncs.isSealedSkillEffect(attack, x)) && (effect.GetInt(BattleskillEffectLogicArgumentEnum.range) == 0 || effect.GetInt(BattleskillEffectLogicArgumentEnum.range) == distance) && (BattleFuncs.checkInvokeSkillEffect(BattleFuncs.PackedSkillEffect.Create(x), attack, defense, colosseumTurn, (Judgement.NonBattleParameter.FromPlayerUnitCache) null, (Judgement.NonBattleParameter.FromPlayerUnitCache) null, new int?(isAttacker ? hp.attackerHp : hp.defenderHp), new int?(isAttacker ? hp.defenderHp : hp.attackerHp)) && !BattleFuncs.isEffectEnemyRangeAndInvalid(x, attack, defense)) && !BattleFuncs.isSkillsAndEffectsInvalid(attack, defense, (BL.SkillEffect) null);
        }))))
          invalidSkillLogics.Add(BattleFuncs.InvalidSpecificSkillLogic.Create(skillEffect.effect.GetInt(BattleskillEffectLogicArgumentEnum.invalid_skill_id), skillEffect.effect.GetInt(BattleskillEffectLogicArgumentEnum.invalid_logic_id), (object) skillEffect));
      }
      float num1 = baseDuelSkill.damageRate;
      if (baseDuelSkill.biAttackDamageRate != null)
        num1 *= baseDuelSkill.biAttackDamageRate[biAttackNo];
      float attackRate1 = baseDuelSkill.attackRate;
      float damageValue = baseDuelSkill.damageValue;
      int? fixDamage1 = baseDuelSkill.FixDamage;
      float drainRate = baseDuelSkill.drainRate;
      float downPhysicalRate = baseDuelSkill.defenseDownPhysicalRate;
      float defenseDownMagicRate = baseDuelSkill.defenseDownMagicRate;
      float? fixHit = baseDuelSkill.FixHit;
      int fixHitPriority = baseDuelSkill.FixHitPriority;
      float? fixCritical = baseDuelSkill.FixCritical;
      bool isChageAttackType = baseDuelSkill.isChageAttackType;
      float? percentageDamageRate = baseDuelSkill.PercentageDamageRate;
      int percentageDamageMax = baseDuelSkill.PercentageDamageMax;
      float drainRateRatio = baseDuelSkill.drainRateRatio;
      float physicalRateRatio = baseDuelSkill.defenseDownPhysicalRateRatio;
      float downMagicRateRatio = baseDuelSkill.defenseDownMagicRateRatio;
      if (isBiattack)
      {
        Func<BL.SkillEffect, float> getValue = (Func<BL.SkillEffect, float>) (effect =>
        {
          float num2 = 1f;
          for (int index = biAttackNo >= 5 ? 5 : biAttackNo + 1; index >= 1; --index)
          {
            BattleskillEffectLogicArgumentEnum logicArgumentEnum = BattleskillEffectLogic.GetLogicArgumentEnum("percentage_damage_s" + (object) index);
            if (logicArgumentEnum != BattleskillEffectLogicArgumentEnum.none)
            {
              float num3 = effect.effect.GetFloat(logicArgumentEnum);
              if ((double) num3 != 0.0)
              {
                num2 = num3;
                break;
              }
            }
          }
          return num2;
        });
        foreach (BL.SkillEffect skillEffect in BattleFuncs.gearSkillEffectFilter(defense.originalUnit, defense.skillEffects.Where(BattleskillEffectLogicEnum.resist_suisei, (Func<BL.SkillEffect, bool>) (x =>
        {
          BattleskillEffect effect = x.effect;
          BattleFuncs.PackedSkillEffect pse = BattleFuncs.PackedSkillEffect.Create(x);
          pse.SetIgnoreHeader(true);
          if (BattleFuncs.isSealedSkillEffect(defense, x) || effect.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) != 0 && effect.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) != defense.originalUnit.unit.kind.ID || (effect.GetInt(BattleskillEffectLogicArgumentEnum.target_gear_kind_id) != 0 && effect.GetInt(BattleskillEffectLogicArgumentEnum.target_gear_kind_id) != attack.originalUnit.unit.kind.ID || effect.GetInt(BattleskillEffectLogicArgumentEnum.element) != 0 && (CommonElement) effect.GetInt(BattleskillEffectLogicArgumentEnum.element) != defense.originalUnit.playerUnit.GetElement()) || (effect.GetInt(BattleskillEffectLogicArgumentEnum.target_element) != 0 && (CommonElement) effect.GetInt(BattleskillEffectLogicArgumentEnum.target_element) != attack.originalUnit.playerUnit.GetElement() || effect.GetInt(BattleskillEffectLogicArgumentEnum.target_job_id) != 0 && effect.GetInt(BattleskillEffectLogicArgumentEnum.target_job_id) != attack.originalUnit.job.ID) || (!BattleFuncs.checkInvokeSkillEffect(pse, defense, attack, colosseumTurn, (Judgement.NonBattleParameter.FromPlayerUnitCache) null, (Judgement.NonBattleParameter.FromPlayerUnitCache) null, new int?(isAttacker ? hp.defenderHp : hp.attackerHp), new int?(isAttacker ? hp.attackerHp : hp.defenderHp)) || BattleFuncs.isEffectEnemyRangeAndInvalid(x, defense, attack) || BattleFuncs.isSkillsAndEffectsInvalid(defense, attack, (BL.SkillEffect) null)))
            return false;
          Func<BattleFuncs.InvalidSpecificSkillLogic, bool> funcExtraCheck = (Func<BattleFuncs.InvalidSpecificSkillLogic, bool>) (issl =>
          {
            BattleskillEffect battleskillEffect = (BattleskillEffect) null;
            if (issl.param is BL.SkillEffect)
              battleskillEffect = (issl.param as BL.SkillEffect).effect;
            else if (issl.param is BattleskillEffect)
              battleskillEffect = issl.param as BattleskillEffect;
            if (battleskillEffect == null)
              return true;
            int num2 = battleskillEffect.HasKey(BattleskillEffectLogicArgumentEnum.condition) ? battleskillEffect.GetInt(BattleskillEffectLogicArgumentEnum.condition) : 0;
            if (num2 == 0)
              return true;
            float num3 = getValue(x);
            if (num2 == 1 && (double) num3 > 1.0)
              return true;
            return num2 == 2 && (double) num3 < 1.0;
          });
          return !BattleFuncs.checkInvalidEffect(x, invalidSkillLogics, funcExtraCheck);
        }))))
          num1 = (float) ((Decimal) num1 * (Decimal) getValue(skillEffect));
      }
      ret.invokeSkillExtraInfo = new List<string>();
      ret.damageShareUnit = new List<BL.ISkillEffectListUnit>();
      ret.damageShareDamage = new List<int>();
      ret.damageShareSkillEffect = new List<BL.UseSkillEffect>();
      ret.attackerUseSkillEffects = new List<BL.UseSkillEffect>();
      ret.defenderUseSkillEffects = new List<BL.UseSkillEffect>();
      ret.duelSkillProc = BattleDuelSkill.invokeDuelSkills(attack, attackStatus, attackPanel, defense, defenseStatus, defensePanel, distance, isAttacker ? hp.attackerHp : hp.defenderHp, isAttacker ? hp.defenderHp : hp.attackerHp, isAttacker ? hp.attackerIsDontUseSkill : hp.defenderIsDontUseSkill, isAttacker ? hp.defenderIsDontUseSkill : hp.attackerIsDontUseSkill, random, isAI, colosseumTurn, isBiattack, isAttacker, invokedAmbush, invokedPrayer, baseDuelSkill, invokeRate, defenderDuelBeginHp, isOneMoreAttack, isInvalidAttackDuelSkill, invalidSkillLogics);
      float? nullable5 = new float?();
      int maxDamage1 = 0;
      if (ret.duelSkillProc.PercentageDamageRate.HasValue)
      {
        nullable5 = ret.duelSkillProc.PercentageDamageRate;
        maxDamage1 = ret.duelSkillProc.PercentageDamageMax;
      }
      else if (percentageDamageRate.HasValue)
      {
        nullable5 = percentageDamageRate;
        maxDamage1 = percentageDamageMax;
      }
      BL.MagicBullet magicBullet = attackStatus.magicBullet;
      if (magicBullet != null && !nullable5.HasValue)
      {
        BattleskillEffect percentageDamage = magicBullet.percentageDamage;
        if (percentageDamage != null)
        {
          nullable5 = new float?(percentageDamage.GetFloat(BattleskillEffectLogicArgumentEnum.percentage));
          maxDamage1 = percentageDamage.HasKey(BattleskillEffectLogicArgumentEnum.max_value) ? percentageDamage.GetInt(BattleskillEffectLogicArgumentEnum.max_value) : 0;
        }
      }
      ret.hp = hp;
      float? nullable6 = ret.duelSkillProc.FixHit;
      float? nullable7;
      if (nullable6.HasValue || fixHit.HasValue)
      {
        int? hitMin = attackStatus.duelParameter.HitMin;
        float? nullable8;
        if (!hitMin.HasValue)
        {
          nullable6 = new float?();
          nullable8 = nullable6;
        }
        else
          nullable8 = new float?((float) hitMin.GetValueOrDefault() / 100f);
        float? nullable9 = nullable8;
        int? hitMax = attackStatus.duelParameter.HitMax;
        float? nullable10;
        if (!hitMax.HasValue)
        {
          nullable6 = new float?();
          nullable10 = nullable6;
        }
        else
          nullable10 = new float?((float) hitMax.GetValueOrDefault() / 100f);
        float? nullable11 = nullable10;
        nullable6 = ret.duelSkillProc.FixHit;
        float? nullable12 = nullable6.HasValue ? ret.duelSkillProc.FixHit : fixHit;
        if (fixHitPriority == 0 && nullable11.HasValue)
        {
          nullable6 = nullable12;
          float? nullable13 = nullable11;
          if ((double) nullable6.GetValueOrDefault() > (double) nullable13.GetValueOrDefault() & (nullable6.HasValue & nullable13.HasValue))
            nullable12 = nullable11;
        }
        if (nullable9.HasValue)
        {
          nullable7 = nullable12;
          nullable6 = nullable9;
          if ((double) nullable7.GetValueOrDefault() < (double) nullable6.GetValueOrDefault() & (nullable7.HasValue & nullable6.HasValue))
            nullable12 = nullable9;
        }
        ref BattleFuncs.AttackDamageData local = ref ret;
        nullable6 = nullable12;
        float num2 = random.NextFloat();
        int num3 = (double) nullable6.GetValueOrDefault() >= (double) num2 & nullable6.HasValue ? 1 : 0;
        local.isHit = num3 != 0;
      }
      else
        ret.isHit = attackStatus.calcHit(random.NextFloat());
      if (defense.IsDontEvasion || (isAttacker ? (hp.defenderIsDontEvasion ? 1 : 0) : (hp.attackerIsDontEvasion ? 1 : 0)) != 0)
        ret.isHit = true;
      float? nullable14 = criticalRate;
      if (fixCritical.HasValue)
      {
        if (nullable14.HasValue)
        {
          nullable6 = fixCritical;
          nullable7 = nullable14;
          if (!((double) nullable6.GetValueOrDefault() > (double) nullable7.GetValueOrDefault() & (nullable6.HasValue & nullable7.HasValue)))
            goto label_46;
        }
        nullable14 = fixCritical;
      }
label_46:
      if (attackStatus.criticalDisplay() > 0)
      {
        if (nullable14.HasValue)
        {
          double critical = (double) attackStatus.critical;
          nullable7 = nullable14;
          double valueOrDefault = (double) nullable7.GetValueOrDefault();
          if (!(critical > valueOrDefault & nullable7.HasValue))
            goto label_50;
        }
        nullable14 = new float?(attackStatus.critical);
      }
label_50:
      if (nullable14.HasValue)
      {
        int? criticalMin = attackStatus.duelParameter.CriticalMin;
        float? nullable8;
        if (!criticalMin.HasValue)
        {
          nullable7 = new float?();
          nullable8 = nullable7;
        }
        else
          nullable8 = new float?((float) criticalMin.GetValueOrDefault() / 100f);
        float? nullable9 = nullable8;
        int? criticalMax = attackStatus.duelParameter.CriticalMax;
        float? nullable10;
        if (!criticalMax.HasValue)
        {
          nullable7 = new float?();
          nullable10 = nullable7;
        }
        else
          nullable10 = new float?((float) criticalMax.GetValueOrDefault() / 100f);
        float? nullable11 = nullable10;
        if (nullable11.HasValue)
        {
          nullable7 = nullable14;
          nullable6 = nullable11;
          if ((double) nullable7.GetValueOrDefault() > (double) nullable6.GetValueOrDefault() & (nullable7.HasValue & nullable6.HasValue))
            nullable14 = nullable11;
        }
        if (nullable9.HasValue)
        {
          nullable6 = nullable14;
          nullable7 = nullable9;
          if ((double) nullable6.GetValueOrDefault() < (double) nullable7.GetValueOrDefault() & (nullable6.HasValue & nullable7.HasValue))
            nullable14 = nullable9;
        }
      }
      ref BattleFuncs.AttackDamageData local1 = ref ret;
      int num4;
      if (ret.isHit && nullable14.HasValue)
      {
        nullable7 = nullable14;
        float num2 = random.NextFloat();
        num4 = (double) nullable7.GetValueOrDefault() >= (double) num2 & nullable7.HasValue ? 1 : 0;
      }
      else
        num4 = 0;
      local1.isCritical = num4 != 0;
      ret.damage = 0;
      ret.dispDamage = 0;
      ret.realDamage = 0;
      ret.dispDrainDamage = 0;
      ret.drainDamage = 0;
      ret.counterDamage = 0;
      ret.defenderDispDrainDamage = 0;
      ret.defenderDrainDamage = 0;
      ret.duelSkillProc.invokeDefenderSkill(ret.isCritical);
      if (ret.duelSkillProc.isSuppressCritical || baseDuelSkill.isSuppressCritical)
        ret.isCritical = false;
      if (ret.duelSkillProc.attackerCantOneMore || baseDuelSkill.attackerCantOneMore)
      {
        if (isAttacker)
          hp.attackerCantOneMore = true;
        else
          hp.defenderCantOneMore = true;
      }
      if (biAttackNo == 0)
      {
        int hp1 = attack.originalUnit.parameter.Hp;
        ret.counterDamage += Mathf.CeilToInt((float) hp1 * baseDuelSkill.counterDamageHpPercentage) + Mathf.CeilToInt((float) hp1 * ret.duelSkillProc.counterDamageHpPercentage);
        ret.counterDamage += ret.duelSkillProc.counterDamageValue + baseDuelSkill.counterDamageValue;
      }
      System.Action action1 = (System.Action) (() =>
      {
        if (isAttacker)
        {
          ret.counterDamage = NC.Clamp(0, hp.attackerHp, ret.counterDamage);
          hp.attackerHp -= ret.counterDamage;
        }
        else
        {
          ret.counterDamage = NC.Clamp(0, hp.defenderHp, ret.counterDamage);
          hp.defenderHp -= ret.counterDamage;
        }
      });
      int attackerHp = hp.attackerHp;
      int defenderHp = hp.defenderHp;
      if (ret.isHit)
      {
        int physicalDefense = attackStatus.duelParameter.defenderUnitParameter.PhysicalDefense;
        int magicDefense = attackStatus.duelParameter.defenderUnitParameter.MagicDefense;
        float num2 = ret.duelSkillProc.defenseDownPhysicalRate * downPhysicalRate;
        float num3 = ret.duelSkillProc.defenseDownMagicRate * defenseDownMagicRate;
        float num5 = 1f - (1f - num2) * (ret.duelSkillProc.defenseDownPhysicalRateRatio * physicalRateRatio);
        float num6 = 1f - (1f - num3) * (ret.duelSkillProc.defenseDownMagicRateRatio * downMagicRateRatio);
        attackStatus.duelParameter.defenderUnitParameter.PhysicalDefense = (int) Mathf.Ceil((float) physicalDefense * num5);
        attackStatus.duelParameter.defenderUnitParameter.MagicDefense = (int) Mathf.Ceil((float) magicDefense * num6);
        float num7 = ret.duelSkillProc.damageRate * num1;
        List<BattleFuncs.SkillParam> skillParams1 = new List<BattleFuncs.SkillParam>();
        System.Action<BattleskillEffectLogicEnum> action2 = (System.Action<BattleskillEffectLogicEnum>) (logic =>
        {
          foreach (BL.SkillEffect effect in attack.skillEffects.Where(logic, closure_0 ?? (closure_0 = (Func<BL.SkillEffect, bool>) (x =>
          {
            BattleskillEffect effect = x.effect;
            int num2 = effect.GetInt(BattleskillEffectLogicArgumentEnum.attack_phase);
            if (!BattleFuncs.isSealedSkillEffect(attack, x))
            {
              switch (num2)
              {
                case 0:
                  if ((effect.GetInt(BattleskillEffectLogicArgumentEnum.target_element) == 0 || (CommonElement) effect.GetInt(BattleskillEffectLogicArgumentEnum.target_element) == defense.originalUnit.playerUnit.GetElement()) && (effect.GetInt(BattleskillEffectLogicArgumentEnum.element) == 0 || (CommonElement) effect.GetInt(BattleskillEffectLogicArgumentEnum.element) == attack.originalUnit.playerUnit.GetElement()) && (effect.GetInt(BattleskillEffectLogicArgumentEnum.target_gear_kind_id) == 0 || effect.GetInt(BattleskillEffectLogicArgumentEnum.target_gear_kind_id) == defense.originalUnit.unit.kind.ID) && ((!effect.HasKey(BattleskillEffectLogicArgumentEnum.gear_kind_id) || effect.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) == 0 || effect.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) == attack.originalUnit.unit.kind.ID) && (effect.GetInt(BattleskillEffectLogicArgumentEnum.target_family_id) == 0 || defense.originalUnit.playerUnit.HasFamily((UnitFamily) effect.GetInt(BattleskillEffectLogicArgumentEnum.target_family_id)))) && ((!effect.HasKey(BattleskillEffectLogicArgumentEnum.family_id) || effect.GetInt(BattleskillEffectLogicArgumentEnum.family_id) == 0 || attack.originalUnit.playerUnit.HasFamily((UnitFamily) effect.GetInt(BattleskillEffectLogicArgumentEnum.family_id))) && (!effect.HasKey(BattleskillEffectLogicArgumentEnum.skill_group_id) || effect.GetInt(BattleskillEffectLogicArgumentEnum.skill_group_id) == 0 || attack.originalUnit.unit.HasSkillGroupId(effect.GetInt(BattleskillEffectLogicArgumentEnum.skill_group_id))) && ((!effect.HasKey(BattleskillEffectLogicArgumentEnum.exclude_skill_group_id) || effect.GetInt(BattleskillEffectLogicArgumentEnum.exclude_skill_group_id) == 0 || !attack.originalUnit.unit.HasSkillGroupId(effect.GetInt(BattleskillEffectLogicArgumentEnum.exclude_skill_group_id))) && !BattleFuncs.isEffectEnemyRangeAndInvalid(x, attack, defense))))
                    return !BattleFuncs.isSkillsAndEffectsInvalid(attack, defense, (BL.SkillEffect) null);
                  break;
                case 1:
                  if (!isOneMoreAttack)
                    goto case 0;
                  else
                    goto default;
                default:
                  if (!(num2 == 2 & isOneMoreAttack))
                    break;
                  goto case 0;
              }
            }
            return false;
          }))))
            skillParams1.Add(BattleFuncs.SkillParam.Create(attack.originalUnit, effect));
        });
        action2(BattleskillEffectLogicEnum.damage_rate_one_more_attack);
        action2(BattleskillEffectLogicEnum.damage_rate_one_more_attack2);
        foreach (IGrouping<int, BattleFuncs.SkillParam> grouping in BattleFuncs.gearSkillParamFilter(skillParams1).GroupBy<BattleFuncs.SkillParam, int>((Func<BattleFuncs.SkillParam, int>) (x => x.effect.effectId)))
        {
          Decimal num8 = new Decimal(10, 0, 0, false, (byte) 1);
          bool flag = true;
          Decimal? nullable8 = new Decimal?();
          foreach (BattleFuncs.SkillParam skillParam in (IEnumerable<BattleFuncs.SkillParam>) grouping)
          {
            BL.SkillEffect effect = skillParam.effect;
            BL.ISkillEffectListUnit invoke = attack;
            BL.ISkillEffectListUnit target = defense;
            Judgement.BeforeDuelUnitParameter attackerUnitParameter = attackStatus.duelParameter.attackerUnitParameter;
            Judgement.BeforeDuelUnitParameter defenderUnitParameter = attackStatus.duelParameter.defenderUnitParameter;
            AttackStatus invokeAS = attackStatus;
            AttackStatus targetAS = defenseStatus;
            int baseSkillLevel = effect.baseSkillLevel;
            double num9 = (double) effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.percentage_invocation);
            XorShift random1 = random;
            int invokeHp = isAttacker ? hp.attackerHp : hp.defenderHp;
            int targetHp = isAttacker ? hp.defenderHp : hp.attackerHp;
            int? colosseumTurn1 = colosseumTurn;
            nullable7 = new float?();
            float? invokeRate1 = nullable7;
            if (BattleFuncs.isInvoke(invoke, target, attackerUnitParameter, defenderUnitParameter, invokeAS, targetAS, baseSkillLevel, (float) num9, random1, false, invokeHp, targetHp, colosseumTurn1, invokeRate1, (BattleskillEffect) null, 0.0f, 1f, 1f, (List<BattleFuncs.InvalidSpecificSkillLogic>) null))
            {
              Decimal num10 = (Decimal) effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.damage_percentage) + (Decimal) effect.baseSkillLevel * (Decimal) effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.skill_ratio);
              if (num10 < Decimal.Zero)
                num10 = new Decimal();
              num8 *= num10;
              if (flag)
              {
                flag = false;
              }
              else
              {
                if (!nullable8.HasValue)
                {
                  BattleFuncs.PackedSkillEffect packedSkillEffect = BattleFuncs.PackedSkillEffect.Create(effect);
                  nullable8 = new Decimal?(!packedSkillEffect.HasKey(BattleskillEffectLogicArgumentEnum.max_percentage) ? new Decimal(-1, -1, -1, false, (byte) 0) : (Decimal) packedSkillEffect.GetFloat(BattleskillEffectLogicArgumentEnum.max_percentage));
                }
                Decimal num11 = num8;
                Decimal? nullable9 = nullable8;
                Decimal valueOrDefault = nullable9.GetValueOrDefault();
                if (num11 > valueOrDefault & nullable9.HasValue)
                {
                  num8 = nullable8.Value;
                  break;
                }
              }
            }
          }
          num7 *= (float) num8;
        }
        if ((baseDuelSkill.attackerSkills.Length >= 1 ? 1 : (ret.duelSkillProc.attackerSkills.Length >= 1 ? 1 : 0)) != 0)
        {
          List<BattleFuncs.SkillParam> skillParams2 = new List<BattleFuncs.SkillParam>();
          System.Action<BL.ISkillEffectListUnit, BL.ISkillEffectListUnit, int> action3 = (System.Action<BL.ISkillEffectListUnit, BL.ISkillEffectListUnit, int>) ((effectUnit, targetUnit, effect_target) =>
          {
            if (BattleFuncs.isSkillsAndEffectsInvalid(effectUnit, targetUnit, (BL.SkillEffect) null))
              return;
            foreach (BL.SkillEffect effect in effectUnit.enabledSkillEffect(BattleskillEffectLogicEnum.duel_skill_damage_fluctuate_ratio).Where<BL.SkillEffect>((Func<BL.SkillEffect, bool>) (x =>
            {
              BattleskillEffect effect = x.effect;
              if (effect.GetInt(BattleskillEffectLogicArgumentEnum.effect_target) != effect_target || effect.HasKey(BattleskillEffectLogicArgumentEnum.skill_group_id) && effect.GetInt(BattleskillEffectLogicArgumentEnum.skill_group_id) != 0 && !effectUnit.originalUnit.unit.HasSkillGroupId(effect.GetInt(BattleskillEffectLogicArgumentEnum.skill_group_id)))
                return false;
              return !effect.HasKey(BattleskillEffectLogicArgumentEnum.exclude_skill_group_id) || effect.GetInt(BattleskillEffectLogicArgumentEnum.exclude_skill_group_id) == 0 || !effectUnit.originalUnit.unit.HasSkillGroupId(effect.GetInt(BattleskillEffectLogicArgumentEnum.exclude_skill_group_id));
            })))
            {
              if (!isBiattack)
                skillParams2.Add(BattleFuncs.SkillParam.CreateMul(effectUnit.originalUnit, effect, (float) ((Decimal) effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.damage_rate) + (Decimal) effect.baseSkillLevel * (Decimal) effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.skill_ratio)), (object) null, 0));
              else
                skillParams2.Add(BattleFuncs.SkillParam.CreateMul(effectUnit.originalUnit, effect, (float) ((Decimal) effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.damage_rate_suisei) + (Decimal) effect.baseSkillLevel * (Decimal) effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.skill_ratio_suisei)), (object) null, 0));
            }
          });
          action3(attack, defense, 0);
          action3(defense, attack, 1);
          num7 *= BattleFuncs.calcSkillParamMul(skillParams2, 1f);
        }
        if (ret.duelSkillProc.isInvokeCounterAttack)
          num7 *= ret.duelSkillProc.counterDamageRate;
        int? fixDamage2 = attackStatus.duelParameter.FixDamage;
        int? nullable10 = ret.duelSkillProc.FixDamage;
        if (nullable10.HasValue)
          attackStatus.duelParameter.FixDamage = ret.duelSkillProc.FixDamage;
        else if (fixDamage1.HasValue)
          attackStatus.duelParameter.FixDamage = fixDamage1;
        if (nullable5.HasValue)
        {
          int preHp = isAttacker ? hp.defenderHp : hp.attackerHp;
          attackStatus.duelParameter.FixDamage = new int?(BattleFuncs.calcPercentageDamage(preHp, nullable5.Value, maxDamage1));
        }
        int physicalAttack = attackStatus.duelParameter.attackerUnitParameter.PhysicalAttack;
        attackStatus.duelParameter.attackerUnitParameter.PhysicalAttack += (int) ((double) ret.duelSkillProc.damageValue + (double) damageValue);
        int magicAttack = attackStatus.duelParameter.attackerUnitParameter.MagicAttack;
        attackStatus.duelParameter.attackerUnitParameter.MagicAttack += (int) ((double) ret.duelSkillProc.damageValue + (double) damageValue);
        bool isMagic = attackStatus.isMagic;
        attackStatus.isMagic = ret.duelSkillProc.isChageAttackType | isChageAttackType ? !isMagic : isMagic;
        float attackRate2 = attackStatus.attackRate;
        attackStatus.attackRate = ret.duelSkillProc.attackRate * attackRate1;
        float damageRate = attackStatus.duelParameter.DamageRate;
        attackStatus.duelParameter.DamageRate *= num7;
        attackStatus.duelParameter.DamageRate *= BattleFuncs.calcAttackClassificationRate(attackStatus, attack, defense);
        attackStatus.duelParameter.DamageRate *= attack.originalUnit.playerUnit.normalAttackDamageRate;
        Decimal num12 = new Decimal(10, 0, 0, false, (byte) 1);
        if (ret.isCritical)
        {
          bool flag1 = false;
          for (int effectTarget = 0; effectTarget <= 1; effectTarget++)
          {
            Judgement.BeforeDuelUnitParameter invokeParameter;
            Judgement.BeforeDuelUnitParameter targetParameter;
            AttackStatus invokeAS;
            AttackStatus targetAS;
            BL.ISkillEffectListUnit effectUnit;
            BL.ISkillEffectListUnit targetUnit;
            int effectHp;
            int targetHp;
            if (effectTarget == 0)
            {
              effectUnit = attack;
              targetUnit = defense;
              invokeParameter = attackStatus.duelParameter.attackerUnitParameter;
              targetParameter = attackStatus.duelParameter.defenderUnitParameter;
              invokeAS = attackStatus;
              targetAS = defenseStatus;
              effectHp = isAttacker ? hp.attackerHp : hp.defenderHp;
              targetHp = isAttacker ? hp.defenderHp : hp.attackerHp;
            }
            else
            {
              effectUnit = defense;
              targetUnit = attack;
              invokeParameter = attackStatus.duelParameter.defenderUnitParameter;
              targetParameter = attackStatus.duelParameter.attackerUnitParameter;
              invokeAS = defenseStatus;
              targetAS = attackStatus;
              effectHp = isAttacker ? hp.defenderHp : hp.attackerHp;
              targetHp = isAttacker ? hp.attackerHp : hp.defenderHp;
            }
            List<BattleFuncs.SkillParam> skillParams = new List<BattleFuncs.SkillParam>();
            foreach (BL.SkillEffect effect in effectUnit.skillEffects.Where(BattleskillEffectLogicEnum.damage_rate_critical, (Func<BL.SkillEffect, bool>) (x => x.effect.GetInt(BattleskillEffectLogicArgumentEnum.effect_target) == effectTarget && !BattleFuncs.isSealedSkillEffect(effectUnit, x) && BattleFuncs.checkInvokeSkillEffect(BattleFuncs.PackedSkillEffect.Create(x), effectUnit, targetUnit, colosseumTurn, (Judgement.NonBattleParameter.FromPlayerUnitCache) null, (Judgement.NonBattleParameter.FromPlayerUnitCache) null, new int?(effectHp), new int?(targetHp)) && ((effectTarget == 0 || !BattleFuncs.isSkillsAndEffectsInvalid(defense, attack, (BL.SkillEffect) null)) && !BattleFuncs.isEffectEnemyRangeAndInvalid(x, attack, defense)) && !BattleFuncs.isSkillsAndEffectsInvalid(attack, defense, (BL.SkillEffect) null))))
              skillParams.Add(BattleFuncs.SkillParam.Create(effectUnit.originalUnit, effect));
            foreach (IGrouping<int, BattleFuncs.SkillParam> grouping in BattleFuncs.gearSkillParamFilter(skillParams).GroupBy<BattleFuncs.SkillParam, int>((Func<BattleFuncs.SkillParam, int>) (x => x.effect.effectId)))
            {
              Decimal num8 = new Decimal(10, 0, 0, false, (byte) 1);
              bool flag2 = true;
              Decimal? nullable8 = new Decimal?();
              foreach (BattleFuncs.SkillParam skillParam in (IEnumerable<BattleFuncs.SkillParam>) grouping)
              {
                BL.SkillEffect effect = skillParam.effect;
                if (BattleFuncs.isInvoke(effectUnit, targetUnit, invokeParameter, targetParameter, invokeAS, targetAS, effect.baseSkillLevel, effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.percentage_invocation), random, false, effectHp, targetHp, colosseumTurn, new float?(), (BattleskillEffect) null, effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.base_invocation), effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.invocation_skill_ratio), effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.invocation_luck_ratio), (List<BattleFuncs.InvalidSpecificSkillLogic>) null))
                {
                  Decimal num9 = (Decimal) effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.damage_percentage) + (Decimal) effect.baseSkillLevel * (Decimal) effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.skill_ratio);
                  if (num9 < Decimal.Zero)
                    num9 = new Decimal();
                  num8 *= num9;
                  if (flag2)
                  {
                    flag1 = true;
                    flag2 = false;
                  }
                  else
                  {
                    if (!nullable8.HasValue)
                    {
                      float num10 = effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.max_percentage);
                      nullable8 = new Decimal?((double) num10 == 0.0 ? new Decimal(-1, -1, -1, false, (byte) 0) : (Decimal) num10);
                    }
                    Decimal num11 = num8;
                    Decimal? nullable9 = nullable8;
                    Decimal valueOrDefault = nullable9.GetValueOrDefault();
                    if (num11 > valueOrDefault & nullable9.HasValue)
                    {
                      num8 = nullable8.Value;
                      break;
                    }
                  }
                }
              }
              num12 *= num8;
            }
          }
          if (!flag1)
            num12 = new Decimal(3);
        }
        float num13 = (float) ((Decimal) Mathf.Max(1f, attackStatus.originalAttack) * num12);
        attackStatus.duelParameter.attackerUnitParameter.PhysicalAttack = physicalAttack;
        attackStatus.duelParameter.attackerUnitParameter.MagicAttack = magicAttack;
        attackStatus.duelParameter.FixDamage = fixDamage2;
        attackStatus.duelParameter.defenderUnitParameter.PhysicalDefense = physicalDefense;
        attackStatus.duelParameter.defenderUnitParameter.MagicDefense = magicDefense;
        attackStatus.isMagic = isMagic;
        attackStatus.attackRate = attackRate2;
        attackStatus.duelParameter.DamageRate = damageRate;
        if ((double) num13 < 1.0)
          num13 = 1f;
        if (ret.duelSkillProc.isAbsoluteDefense || baseDuelSkill.isAbsoluteDefense)
        {
          ret.invokeSkillExtraInfo.Add("absolute_defense");
          num13 = 0.0f;
        }
        else
        {
          IEnumerable<BL.SkillEffect> skillEffects = defense.skillEffects.Where(BattleskillEffectLogicEnum.absolute_defense, (Func<BL.SkillEffect, bool>) (x =>
          {
            BattleskillEffect effect = x.effect;
            BattleFuncs.PackedSkillEffect pse = BattleFuncs.PackedSkillEffect.Create(x);
            pse.SetIgnoreHeader(true);
            return (!x.useRemain.HasValue || x.useRemain.Value >= 1) && (!BattleFuncs.isSealedSkillEffect(defense, x) && (effect.GetInt(BattleskillEffectLogicArgumentEnum.target_gear_kind_id) == 0 || effect.GetInt(BattleskillEffectLogicArgumentEnum.target_gear_kind_id) == attack.originalUnit.unit.kind.ID) && ((effect.GetInt(BattleskillEffectLogicArgumentEnum.element) == 0 || (CommonElement) effect.GetInt(BattleskillEffectLogicArgumentEnum.element) == defense.originalUnit.playerUnit.GetElement()) && (effect.GetInt(BattleskillEffectLogicArgumentEnum.target_element) == 0 || (CommonElement) effect.GetInt(BattleskillEffectLogicArgumentEnum.target_element) == attack.originalUnit.playerUnit.GetElement())) && ((effect.GetInt(BattleskillEffectLogicArgumentEnum.target_job_id) == 0 || effect.GetInt(BattleskillEffectLogicArgumentEnum.target_job_id) == attack.originalUnit.job.ID) && (effect.GetInt(BattleskillEffectLogicArgumentEnum.target_family_id) == 0 || attack.originalUnit.playerUnit.HasFamily((UnitFamily) effect.GetInt(BattleskillEffectLogicArgumentEnum.target_family_id))) && ((effect.GetInt(BattleskillEffectLogicArgumentEnum.character_id) == 0 || effect.GetInt(BattleskillEffectLogicArgumentEnum.character_id) == defense.originalUnit.unit.character.ID) && (effect.GetInt(BattleskillEffectLogicArgumentEnum.target_unit_id) == 0 || effect.GetInt(BattleskillEffectLogicArgumentEnum.target_unit_id) == attack.originalUnit.unit.ID))) && (effect.GetInt(BattleskillEffectLogicArgumentEnum.unit_id) == 0 || effect.GetInt(BattleskillEffectLogicArgumentEnum.unit_id) == defense.originalUnit.unit.ID)) && (BattleFuncs.checkInvokeSkillEffect(pse, defense, attack, colosseumTurn, (Judgement.NonBattleParameter.FromPlayerUnitCache) null, (Judgement.NonBattleParameter.FromPlayerUnitCache) null, new int?(isAttacker ? hp.defenderHp : hp.attackerHp), new int?(isAttacker ? hp.attackerHp : hp.defenderHp)) && !BattleFuncs.isEffectEnemyRangeAndInvalid(x, defense, attack) && !BattleFuncs.isSkillsAndEffectsInvalid(defense, attack, (BL.SkillEffect) null)) && !BattleFuncs.checkInvalidEffect(x, invalidSkillLogics, (Func<BattleFuncs.InvalidSpecificSkillLogic, bool>) null);
          }));
          IEnumerable<BL.SkillEffect> array = (IEnumerable<BL.SkillEffect>) BattleFuncs.gearSkillEffectFilter(defense.originalUnit, skillEffects).ToArray<BL.SkillEffect>();
          if (array.Any<BL.SkillEffect>())
          {
            foreach (BL.SkillEffect effect in array)
            {
              if (BattleFuncs.isInvoke(defense, attack, attackStatus.duelParameter.defenderUnitParameter, attackStatus.duelParameter.attackerUnitParameter, defenseStatus, attackStatus, effect.baseSkillLevel, effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.percentage_invocation), random, false, isAttacker ? hp.defenderHp : hp.attackerHp, isAttacker ? hp.attackerHp : hp.defenderHp, colosseumTurn, new float?(), (BattleskillEffect) null, 0.0f, 1f, 1f, (List<BattleFuncs.InvalidSpecificSkillLogic>) null))
              {
                ret.defenderUseSkillEffects.Add(BL.UseSkillEffect.Create(effect, BL.UseSkillEffect.Type.Decrement, 0.0f));
                ret.invokeSkillExtraInfo.Add("absolute_defense");
                nullable10 = effect.useRemain;
                if (nullable10.HasValue)
                {
                  nullable10 = effect.useRemain;
                  if (nullable10.Value >= 1)
                  {
                    BL.SkillEffect skillEffect = effect;
                    nullable10 = effect.useRemain;
                    int? nullable8 = nullable10.HasValue ? new int?(nullable10.GetValueOrDefault() - 1) : new int?();
                    skillEffect.useRemain = nullable8;
                  }
                }
                num13 = 0.0f;
                break;
              }
            }
          }
        }
        float num14 = (float) BattleFuncs.applyDamageCut(0, (int) num13, defense, attack, attackStatus.duelParameter.defenderUnitParameter, attackStatus.duelParameter.attackerUnitParameter, defenseStatus, attackStatus, random, isAttacker ? hp.defenderHp : hp.attackerHp, isAttacker ? hp.attackerHp : hp.defenderHp, colosseumTurn, defensePanel);
        if ((double) num14 >= 1.0)
        {
          IEnumerable<BL.SkillEffect> skillEffects = defense.skillEffects.Where(BattleskillEffectLogicEnum.damage_shield, (Func<BL.SkillEffect, bool>) (x =>
          {
            BattleskillEffect effect = x.effect;
            return (x.work == null || BattleFuncs.getShieldRemain(x, defense) >= 1) && !BattleFuncs.isSealedSkillEffect(defense, x) && (BattleFuncs.checkInvokeSkillEffect(BattleFuncs.PackedSkillEffect.Create(x), defense, attack, colosseumTurn, (Judgement.NonBattleParameter.FromPlayerUnitCache) null, (Judgement.NonBattleParameter.FromPlayerUnitCache) null, new int?(isAttacker ? hp.defenderHp : hp.attackerHp), new int?(isAttacker ? hp.attackerHp : hp.defenderHp)) && !BattleFuncs.isEffectEnemyRangeAndInvalid(x, defense, attack) && !BattleFuncs.isSkillsAndEffectsInvalid(defense, attack, (BL.SkillEffect) null)) && !BattleFuncs.checkInvalidEffect(x, invalidSkillLogics, (Func<BattleFuncs.InvalidSpecificSkillLogic, bool>) null);
          }));
          IEnumerable<BL.SkillEffect> array = (IEnumerable<BL.SkillEffect>) BattleFuncs.gearSkillEffectFilter(defense.originalUnit, skillEffects).ToArray<BL.SkillEffect>();
          if (array.Any<BL.SkillEffect>())
          {
            foreach (BL.SkillEffect skillEffect in (IEnumerable<BL.SkillEffect>) array.OrderByDescending<BL.SkillEffect, int>((Func<BL.SkillEffect, int>) (x => BattleFuncs.getShieldRemain(x, defense))).ThenBy<BL.SkillEffect, int>((Func<BL.SkillEffect, int>) (x => x.effectId)))
            {
              BL.UseSkillEffect useSkillEffect = BL.UseSkillEffect.Create(skillEffect, BL.UseSkillEffect.Type.None, 0.0f);
              if (skillEffect.work == null)
                skillEffect.work = new float[1];
              int num8 = (int) skillEffect.work[0];
              int shieldHp = BattleFuncs.getShieldHp(skillEffect, defense);
              int num9 = num8 + (int) num14;
              if (num9 > shieldHp)
              {
                num14 = (float) (num9 - shieldHp);
                num9 = shieldHp;
              }
              else
                num14 = 0.0f;
              skillEffect.work[0] = (float) num9;
              useSkillEffect.type = num9 >= shieldHp ? BL.UseSkillEffect.Type.Remove : BL.UseSkillEffect.Type.SetWork;
              useSkillEffect.work = skillEffect.work[0];
              ret.defenderUseSkillEffects.Add(useSkillEffect);
              if ((double) num14 <= 0.0)
              {
                ret.invokeSkillExtraInfo.Add("absolute_defense");
                break;
              }
            }
          }
        }
        ret.duelSkillProc.InvokeDamageSkill((int) num14);
        if (!invokedPrayer)
          invokedPrayer = ((IEnumerable<BL.Skill>) ret.duelSkillProc.defenderSkills).Any<BL.Skill>((Func<BL.Skill, bool>) (x => ((IEnumerable<BattleskillEffect>) x.skill.Effects).Any<BattleskillEffect>((Func<BattleskillEffect, bool>) (y => y.EffectLogic.Enum == BattleskillEffectLogicEnum.prayer))));
        if (!invokedPrayer)
        {
          int num8 = isAttacker ? hp.defenderHp : hp.attackerHp;
          if ((double) num14 >= (double) num8 && defenderDuelBeginHp >= 2)
          {
            IEnumerable<BL.SkillEffect> skillEffects = defense.skillEffects.Where(BattleskillEffectLogicEnum.passive_prayer, (Func<BL.SkillEffect, bool>) (x =>
            {
              BattleskillEffect effect = x.effect;
              return (!x.useRemain.HasValue || x.useRemain.Value >= 1) && (!BattleFuncs.isSealedSkillEffect(defense, x) && (effect.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) == 0 || effect.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) == defense.originalUnit.unit.kind.ID) && ((effect.GetInt(BattleskillEffectLogicArgumentEnum.target_gear_kind_id) == 0 || effect.GetInt(BattleskillEffectLogicArgumentEnum.target_gear_kind_id) == attack.originalUnit.unit.kind.ID) && (effect.GetInt(BattleskillEffectLogicArgumentEnum.element) == 0 || (CommonElement) effect.GetInt(BattleskillEffectLogicArgumentEnum.element) == defense.originalUnit.playerUnit.GetElement())) && ((effect.GetInt(BattleskillEffectLogicArgumentEnum.target_element) == 0 || (CommonElement) effect.GetInt(BattleskillEffectLogicArgumentEnum.target_element) == attack.originalUnit.playerUnit.GetElement()) && (effect.GetInt(BattleskillEffectLogicArgumentEnum.job_id) == 0 || effect.GetInt(BattleskillEffectLogicArgumentEnum.job_id) == defense.originalUnit.job.ID) && ((effect.GetInt(BattleskillEffectLogicArgumentEnum.target_job_id) == 0 || effect.GetInt(BattleskillEffectLogicArgumentEnum.target_job_id) == attack.originalUnit.job.ID) && (effect.GetInt(BattleskillEffectLogicArgumentEnum.family_id) == 0 || defense.originalUnit.playerUnit.HasFamily((UnitFamily) effect.GetInt(BattleskillEffectLogicArgumentEnum.family_id))))) && ((effect.GetInt(BattleskillEffectLogicArgumentEnum.target_family_id) == 0 || attack.originalUnit.playerUnit.HasFamily((UnitFamily) effect.GetInt(BattleskillEffectLogicArgumentEnum.target_family_id))) && (!effect.HasKey(BattleskillEffectLogicArgumentEnum.invoke_gamemode) || BattleFuncs.checkInvokeGamemode(effect.GetInt(BattleskillEffectLogicArgumentEnum.invoke_gamemode), isColosseum)) && (!BattleFuncs.isEffectEnemyRangeAndInvalid(x, defense, attack) && !BattleFuncs.isSkillsAndEffectsInvalid(defense, attack, (BL.SkillEffect) null)))) && !BattleFuncs.checkInvalidEffect(x, invalidSkillLogics, (Func<BattleFuncs.InvalidSpecificSkillLogic, bool>) null);
            }));
            IEnumerable<BL.SkillEffect> array = (IEnumerable<BL.SkillEffect>) BattleFuncs.gearSkillEffectFilter(defense.originalUnit, skillEffects).ToArray<BL.SkillEffect>();
            if (array.Any<BL.SkillEffect>())
            {
              foreach (BL.SkillEffect skillEffect in array)
              {
                BattleFuncs.PackedSkillEffect packedSkillEffect = BattleFuncs.PackedSkillEffect.Create(skillEffect);
                if (BattleFuncs.isInvoke(defense, attack, attackStatus.duelParameter.defenderUnitParameter, attackStatus.duelParameter.attackerUnitParameter, defenseStatus, attackStatus, skillEffect.baseSkillLevel, skillEffect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.percentage_invocation), random, false, isAttacker ? hp.defenderHp : hp.attackerHp, isAttacker ? hp.attackerHp : hp.defenderHp, colosseumTurn, new float?(), skillEffect.effect, packedSkillEffect.HasKey(BattleskillEffectLogicArgumentEnum.base_invocation) ? packedSkillEffect.GetFloat(BattleskillEffectLogicArgumentEnum.base_invocation) : 0.0f, packedSkillEffect.HasKey(BattleskillEffectLogicArgumentEnum.invocation_skill_ratio) ? packedSkillEffect.GetFloat(BattleskillEffectLogicArgumentEnum.invocation_skill_ratio) : 1f, packedSkillEffect.HasKey(BattleskillEffectLogicArgumentEnum.invocation_luck_ratio) ? packedSkillEffect.GetFloat(BattleskillEffectLogicArgumentEnum.invocation_luck_ratio) : 1f, (List<BattleFuncs.InvalidSpecificSkillLogic>) null))
                {
                  ret.defenderUseSkillEffects.Add(BL.UseSkillEffect.Create(skillEffect, BL.UseSkillEffect.Type.Decrement, 0.0f));
                  invokedPrayer = true;
                  break;
                }
              }
            }
          }
        }
        if (invokedPrayer)
        {
          int num8 = isAttacker ? hp.defenderHp : hp.attackerHp;
          if ((double) num14 >= (double) num8)
            num14 = (float) (num8 - 1);
        }
        if ((double) num14 >= 1.0 && !isColosseum)
        {
          BL.ISkillEffectListUnit[] array1 = BattleFuncs.getForceUnits(BattleFuncs.env.getForceID(defense.originalUnit), isAI, true, false).ToArray<BL.ISkillEffectListUnit>();
          Dictionary<BL.ISkillEffectListUnit, Tuple<BL.SkillEffect, int>> source = new Dictionary<BL.ISkillEffectListUnit, Tuple<BL.SkillEffect, int>>();
          foreach (BL.ISkillEffectListUnit skillEffectListUnit in array1)
          {
            BL.ISkillEffectListUnit unit = skillEffectListUnit;
            if (unit != defense)
            {
              if (!ret.hp.otherHp.ContainsKey(unit))
                ret.hp.otherHp[unit] = new TurnOtherHp(unit.hp);
              if (ret.hp.otherHp[unit].hp > 0)
              {
                List<BL.SkillEffect> list1 = unit.skillEffects.Where(BattleskillEffectLogicEnum.damage_share, (Func<BL.SkillEffect, bool>) (x =>
                {
                  if (x.useRemain.HasValue)
                  {
                    int num2 = ret.hp.otherHp[unit].consumeSkillEffect.Count<BL.SkillEffect>((Func<BL.SkillEffect, bool>) (y => y == x));
                    if (x.useRemain.Value <= num2)
                      return false;
                  }
                  BattleskillEffect effect = x.effect;
                  Tuple<int, int> unitCell1 = BattleFuncs.getUnitCell(defense.originalUnit, isAI, false);
                  Tuple<int, int> unitCell2 = BattleFuncs.getUnitCell(unit.originalUnit, isAI, false);
                  int num3 = BL.fieldDistance(unitCell1.Item1, unitCell1.Item2, unitCell2.Item1, unitCell2.Item2);
                  if (num3 < effect.GetInt(BattleskillEffectLogicArgumentEnum.min_range) || num3 > effect.GetInt(BattleskillEffectLogicArgumentEnum.max_range) || BattleFuncs.isSealedSkillEffect(unit, x) || (effect.GetInt(BattleskillEffectLogicArgumentEnum.target_gear_kind_id) != 0 && effect.GetInt(BattleskillEffectLogicArgumentEnum.target_gear_kind_id) != defense.originalUnit.unit.kind.ID || effect.GetInt(BattleskillEffectLogicArgumentEnum.target_family_id) != 0 && !defense.originalUnit.playerUnit.HasFamily((UnitFamily) effect.GetInt(BattleskillEffectLogicArgumentEnum.target_family_id))))
                    return false;
                  return effect.GetInt(BattleskillEffectLogicArgumentEnum.target_element) == 0 || (CommonElement) effect.GetInt(BattleskillEffectLogicArgumentEnum.target_element) == defense.originalUnit.playerUnit.GetElement();
                })).ToList<BL.SkillEffect>();
                List<BL.SkillEffect> list2 = BattleFuncs.gearSkillEffectFilter(unit.originalUnit, (IEnumerable<BL.SkillEffect>) list1).ToList<BL.SkillEffect>();
                if (list2.Count > 0)
                {
                  BL.SkillEffect skillEffect1 = (BL.SkillEffect) null;
                  int num8 = int.MinValue;
                  foreach (BL.SkillEffect skillEffect2 in list2)
                  {
                    int num9 = 0 + (skillEffect2.effect.GetInt(BattleskillEffectLogicArgumentEnum.value) + skillEffect2.baseSkillLevel * skillEffect2.effect.GetInt(BattleskillEffectLogicArgumentEnum.value_skill_ratio)) + Mathf.CeilToInt((float) ((Decimal) (int) num14 * (Decimal) (skillEffect2.effect.GetFloat(BattleskillEffectLogicArgumentEnum.percentage) + (float) skillEffect2.baseSkillLevel * skillEffect2.effect.GetFloat(BattleskillEffectLogicArgumentEnum.percentage_skill_ratio))));
                    if (num9 > num8)
                    {
                      num8 = num9;
                      skillEffect1 = skillEffect2;
                    }
                  }
                  source[unit] = Tuple.Create<BL.SkillEffect, int>(skillEffect1, num8);
                }
              }
            }
          }
          if (source.Count > 0)
          {
            int maxDamage = source.Values.Max<Tuple<BL.SkillEffect, int>>((Func<Tuple<BL.SkillEffect, int>, int>) (x => x.Item2));
            KeyValuePair<BL.ISkillEffectListUnit, Tuple<BL.SkillEffect, int>>[] array2 = source.Where<KeyValuePair<BL.ISkillEffectListUnit, Tuple<BL.SkillEffect, int>>>((Func<KeyValuePair<BL.ISkillEffectListUnit, Tuple<BL.SkillEffect, int>>, bool>) (x => x.Value.Item2 == maxDamage)).ToArray<KeyValuePair<BL.ISkillEffectListUnit, Tuple<BL.SkillEffect, int>>>();
            BL.ISkillEffectListUnit key;
            BL.SkillEffect skillEffect;
            int num8;
            if (array2.Length == 1)
            {
              key = array2[0].Key;
              skillEffect = array2[0].Value.Item1;
              num8 = array2[0].Value.Item2;
            }
            else
            {
              Tuple<int, int> unitCell = BattleFuncs.getUnitCell(defense.originalUnit, isAI, false);
              List<Tuple<int, int>> list = ((IEnumerable<KeyValuePair<BL.ISkillEffectListUnit, Tuple<BL.SkillEffect, int>>>) array2).Select<KeyValuePair<BL.ISkillEffectListUnit, Tuple<BL.SkillEffect, int>>, Tuple<int, int>>((Func<KeyValuePair<BL.ISkillEffectListUnit, Tuple<BL.SkillEffect, int>>, Tuple<int, int>>) (x => BattleFuncs.getUnitCell(x.Key.originalUnit, isAI, false))).ToList<Tuple<int, int>>();
              int num9 = unitCell.Item2;
              int num10 = unitCell.Item1;
              int index1 = -1;
              for (int index2 = 1; index2 <= 50; ++index2)
              {
                int num11 = num10 + index2;
                int num15 = num9;
                for (int index3 = 0; index3 < index2; ++index3)
                {
                  index1 = list.IndexOf(Tuple.Create<int, int>(num11, num15));
                  if (index1 == -1)
                  {
                    --num11;
                    ++num15;
                  }
                  else
                    break;
                }
                if (index1 == -1)
                {
                  for (int index3 = 0; index3 < index2; ++index3)
                  {
                    index1 = list.IndexOf(Tuple.Create<int, int>(num11, num15));
                    if (index1 == -1)
                    {
                      --num11;
                      --num15;
                    }
                    else
                      break;
                  }
                  if (index1 == -1)
                  {
                    for (int index3 = 0; index3 < index2; ++index3)
                    {
                      index1 = list.IndexOf(Tuple.Create<int, int>(num11, num15));
                      if (index1 == -1)
                      {
                        ++num11;
                        --num15;
                      }
                      else
                        break;
                    }
                    if (index1 == -1)
                    {
                      for (int index3 = 0; index3 < index2; ++index3)
                      {
                        index1 = list.IndexOf(Tuple.Create<int, int>(num11, num15));
                        if (index1 == -1)
                        {
                          ++num11;
                          ++num15;
                        }
                        else
                          break;
                      }
                      if (index1 != -1)
                        break;
                    }
                    else
                      break;
                  }
                  else
                    break;
                }
                else
                  break;
              }
              if (index1 == -1)
                index1 = 0;
              key = array2[index1].Key;
              skillEffect = array2[index1].Value.Item1;
              num8 = array2[index1].Value.Item2;
            }
            BattleFuncs.PackedSkillEffect packedSkillEffect = BattleFuncs.PackedSkillEffect.Create(skillEffect);
            if (num8 < 0)
              num8 = 0;
            else if (num8 > (int) num14)
              num8 = (int) num14;
            int num16 = num8;
            int num17 = (int) num14 - num8;
            float num18 = !packedSkillEffect.HasKey(BattleskillEffectLogicArgumentEnum.damage_percentage) ? 1f : packedSkillEffect.GetFloat(BattleskillEffectLogicArgumentEnum.damage_percentage);
            float num19 = !packedSkillEffect.HasKey(BattleskillEffectLogicArgumentEnum.target_damage_percentage) ? 1f : packedSkillEffect.GetFloat(BattleskillEffectLogicArgumentEnum.target_damage_percentage);
            int num20 = Mathf.CeilToInt((float) ((Decimal) num16 * (Decimal) num18));
            int num21 = Mathf.CeilToInt((float) ((Decimal) num17 * (Decimal) num19));
            if (num20 < 0)
              num20 = 0;
            if (num21 < 0)
              num21 = 0;
            ret.hp.otherHp[key].hp -= num20;
            num14 = (float) num21;
            ret.hp.otherHp[key].consumeSkillEffect.Add(skillEffect);
            ret.damageShareUnit.Add(key);
            ret.damageShareDamage.Add(num20);
            ret.damageShareSkillEffect.Add(BL.UseSkillEffect.Create(skillEffect, BL.UseSkillEffect.Type.None, 0.0f));
          }
        }
        ret.damage = NC.Clamp(0, isAttacker ? hp.defenderHp : hp.attackerHp, (int) num14);
        ret.dispDamage = NC.Clamp(0, 99999999, (int) num14);
        ret.realDamage = (int) num14;
        if (ret.realDamage < 0)
          ret.realDamage = 0;
        if (ret.duelSkillProc.isInvokeCounterAttack)
        {
          float num8 = num14 * ret.duelSkillProc.counterAttackRate;
          ret.counterDamage += (int) num8;
        }
        action1();
        if ((isAttacker ? (!hp.defenderIsDontAction ? 1 : 0) : (!hp.attackerIsDontAction ? 1 : 0)) != 0)
        {
          Func<BL.SkillEffect, bool> f = (Func<BL.SkillEffect, bool>) (x =>
          {
            BattleskillEffect effect = x.effect;
            BattleFuncs.PackedSkillEffect pse = BattleFuncs.PackedSkillEffect.Create(x);
            pse.SetIgnoreHeader(true);
            if (!BattleFuncs.isSealedSkillEffect(defense, x) && (effect.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) == 0 || effect.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) == defense.originalUnit.unit.kind.ID) && ((effect.GetInt(BattleskillEffectLogicArgumentEnum.target_gear_kind_id) == 0 || effect.GetInt(BattleskillEffectLogicArgumentEnum.target_gear_kind_id) == attack.originalUnit.unit.kind.ID) && (effect.GetInt(BattleskillEffectLogicArgumentEnum.element) == 0 || (CommonElement) effect.GetInt(BattleskillEffectLogicArgumentEnum.element) == defense.originalUnit.playerUnit.GetElement())) && ((effect.GetInt(BattleskillEffectLogicArgumentEnum.target_element) == 0 || (CommonElement) effect.GetInt(BattleskillEffectLogicArgumentEnum.target_element) == attack.originalUnit.playerUnit.GetElement()) && (!effect.HasKey(BattleskillEffectLogicArgumentEnum.job_id) || effect.GetInt(BattleskillEffectLogicArgumentEnum.job_id) == 0 || effect.GetInt(BattleskillEffectLogicArgumentEnum.job_id) == defense.originalUnit.job.ID)) && ((!effect.HasKey(BattleskillEffectLogicArgumentEnum.target_job_id) || effect.GetInt(BattleskillEffectLogicArgumentEnum.target_job_id) == 0 || effect.GetInt(BattleskillEffectLogicArgumentEnum.target_job_id) == attack.originalUnit.job.ID) && ((effect.GetInt(BattleskillEffectLogicArgumentEnum.family_id) == 0 || defense.originalUnit.playerUnit.HasFamily((UnitFamily) effect.GetInt(BattleskillEffectLogicArgumentEnum.family_id))) && (effect.GetInt(BattleskillEffectLogicArgumentEnum.target_family_id) == 0 || attack.originalUnit.playerUnit.HasFamily((UnitFamily) effect.GetInt(BattleskillEffectLogicArgumentEnum.target_family_id)))) && ((!effect.HasKey(BattleskillEffectLogicArgumentEnum.skill_group_id) || effect.GetInt(BattleskillEffectLogicArgumentEnum.skill_group_id) == 0 || defense.originalUnit.unit.HasSkillGroupId(effect.GetInt(BattleskillEffectLogicArgumentEnum.skill_group_id))) && (!effect.HasKey(BattleskillEffectLogicArgumentEnum.exclude_skill_group_id) || effect.GetInt(BattleskillEffectLogicArgumentEnum.exclude_skill_group_id) == 0 || !defense.originalUnit.unit.HasSkillGroupId(effect.GetInt(BattleskillEffectLogicArgumentEnum.exclude_skill_group_id))))) && (BattleFuncs.checkInvokeSkillEffect(pse, defense, attack, colosseumTurn, (Judgement.NonBattleParameter.FromPlayerUnitCache) null, (Judgement.NonBattleParameter.FromPlayerUnitCache) null, new int?(isAttacker ? hp.defenderHp : hp.attackerHp), new int?(isAttacker ? hp.attackerHp : hp.defenderHp)) && !BattleFuncs.isEffectEnemyRangeAndInvalid(x, defense, attack) && !BattleFuncs.isSkillsAndEffectsInvalid(defense, attack, (BL.SkillEffect) null)))
            {
              bool flag = false;
              float percentage_invocation = 0.0f;
              float base_invocation = 0.0f;
              float invocation_skill_ratio = 1f;
              float invocation_luck_ratio = 1f;
              if (pse.HasKey(BattleskillEffectLogicArgumentEnum.percentage_invocation))
              {
                flag = true;
                percentage_invocation = pse.GetFloat(BattleskillEffectLogicArgumentEnum.percentage_invocation);
              }
              if (pse.HasKey(BattleskillEffectLogicArgumentEnum.base_invocation))
              {
                flag = true;
                base_invocation = pse.GetFloat(BattleskillEffectLogicArgumentEnum.base_invocation);
              }
              if (pse.HasKey(BattleskillEffectLogicArgumentEnum.invocation_skill_ratio))
              {
                flag = true;
                invocation_skill_ratio = pse.GetFloat(BattleskillEffectLogicArgumentEnum.invocation_skill_ratio);
              }
              if (pse.HasKey(BattleskillEffectLogicArgumentEnum.invocation_luck_ratio))
              {
                flag = true;
                invocation_luck_ratio = pse.GetFloat(BattleskillEffectLogicArgumentEnum.invocation_luck_ratio);
              }
              if (!flag || BattleFuncs.isInvoke(defense, attack, attackStatus.duelParameter.defenderUnitParameter, attackStatus.duelParameter.attackerUnitParameter, defenseStatus, attackStatus, x.baseSkillLevel, percentage_invocation, random, false, isAttacker ? hp.defenderHp : hp.attackerHp, isAttacker ? hp.attackerHp : hp.defenderHp, colosseumTurn, new float?(), (BattleskillEffect) null, base_invocation, invocation_skill_ratio, invocation_luck_ratio, (List<BattleFuncs.InvalidSpecificSkillLogic>) null))
                return true;
            }
            return false;
          });
          BL.SkillEffect[] array1 = defense.skillEffects.Where(BattleskillEffectLogicEnum.damage_drain, f).ToArray<BL.SkillEffect>();
          BL.SkillEffect[] array2 = defense.skillEffects.Where(BattleskillEffectLogicEnum.damage_drain2, f).ToArray<BL.SkillEffect>();
          if (array1.Length != 0 || array2.Length != 0)
          {
            List<BattleFuncs.SkillParam> skillParams = new List<BattleFuncs.SkillParam>();
            int num8 = (isAttacker ? hp.defenderHp : hp.attackerHp) - ret.damage;
            foreach (BL.SkillEffect effect in array1)
            {
              if (defense.CanHeal(effect.baseSkill.skill_type))
              {
                Decimal num9 = (Decimal) (effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.percentage_drain) + (float) effect.baseSkillLevel * effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.skill_ratio));
                skillParams.Add(BattleFuncs.SkillParam.CreateAdd(defense.originalUnit, effect, (float) (int) ((Decimal) ret.damage * num9), (object) null, 0));
              }
            }
            if (num8 >= 1)
            {
              foreach (BL.SkillEffect effect in array2)
              {
                if (defense.CanHeal(effect.baseSkill.skill_type))
                {
                  Decimal num9 = (Decimal) (effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.percentage_drain) + (float) effect.baseSkillLevel * effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.skill_ratio));
                  skillParams.Add(BattleFuncs.SkillParam.CreateAdd(defense.originalUnit, effect, (float) (int) ((Decimal) ret.damage * num9), (object) null, 0));
                }
              }
            }
            ret.defenderDispDrainDamage = BattleFuncs.calcSkillParamAdd(skillParams);
            ret.defenderDrainDamage = Mathf.Min(defense.originalUnit.parameter.Hp, num8 + ret.defenderDispDrainDamage) - num8;
          }
        }
        ret.dispDrainDamage = 0;
        ret.drainDamage = 0;
        int hp1 = attack.originalUnit.parameter.Hp;
        float num22 = ret.duelSkillProc.attackerSkills.Length >= 1 || baseDuelSkill.attackerSkills.Length >= 1 ? ret.duelSkillProc.drainRateRatio * drainRateRatio : 1f;
        if ((double) ret.duelSkillProc.drainRate > 0.0)
        {
          if (attack.CanHeal(BattleskillSkillType.duel))
          {
            ret.dispDrainDamage = Mathf.CeilToInt((float) ret.damage * ret.duelSkillProc.drainRate * num22);
            ret.drainDamage = NC.Clamp(0, hp1, ret.dispDrainDamage);
          }
        }
        else if ((double) drainRate > 0.0)
        {
          if (attack.CanHeal(BattleskillSkillType.duel))
          {
            ret.dispDrainDamage = Mathf.CeilToInt((float) ret.damage * drainRate * num22);
            ret.drainDamage = NC.Clamp(0, hp1, ret.dispDrainDamage);
          }
        }
        else if (attackStatus.isDrain && attack.CanHeal(BattleskillSkillType.magic))
        {
          ret.dispDrainDamage = Mathf.CeilToInt((float) ret.damage * attackStatus.drainRate * num22);
          ret.drainDamage = NC.Clamp(0, hp1, ret.dispDrainDamage);
        }
        if (isAttacker)
        {
          ret.hp.defenderHp -= ret.damage;
          ret.hp.defenderHp += ret.defenderDrainDamage;
          int num8 = Mathf.Min(hp1, hp.attackerHp + ret.drainDamage);
          if (ret.dispDrainDamage > 0)
            ret.drainDamage = Mathf.Max(0, num8 - hp.attackerHp);
          ret.hp.attackerHp = num8;
        }
        else
        {
          ret.hp.attackerHp -= ret.damage;
          ret.hp.attackerHp += ret.defenderDrainDamage;
          int num8 = Mathf.Min(hp1, hp.defenderHp + ret.drainDamage);
          if (ret.dispDrainDamage > 0)
            ret.drainDamage = Mathf.Max(0, num8 - hp.defenderHp);
          hp.defenderHp = num8;
        }
      }
      else
      {
        ret.duelSkillProc.InvokeDamageSkill(0);
        action1();
      }
      System.Action<IEnumerable<BL.SkillEffect>, BL.ISkillEffectListUnit, BL.ISkillEffectListUnit, int, int, bool> action4 = (System.Action<IEnumerable<BL.SkillEffect>, BL.ISkillEffectListUnit, BL.ISkillEffectListUnit, int, int, bool>) ((efs, effectUnit, targetUnit, effectUnitHp, targetUnitHp, isDefenseEffect) =>
      {
        foreach (BL.SkillEffect headerEffect in BattleFuncs.gearSkillEffectFilter(effectUnit.originalUnit, efs.Where<BL.SkillEffect>((Func<BL.SkillEffect, bool>) (x =>
        {
          BattleskillEffect effect = x.effect;
          int num2 = effect.GetInt(BattleskillEffectLogicArgumentEnum.attack_phase);
          BattleFuncs.PackedSkillEffect pse = BattleFuncs.PackedSkillEffect.Create(x);
          pse.SetIgnoreHeader(true);
          int num3 = pse.HasKey(BattleskillEffectLogicArgumentEnum.is_attack_nc) ? pse.GetInt(BattleskillEffectLogicArgumentEnum.is_attack_nc) : 0;
          bool flag = isDefenseEffect ? (invokedAmbush ? isAttacker : !isAttacker) : (invokedAmbush ? !isAttacker : isAttacker);
          if (num3 == 1 && !flag || (num3 == 2 & flag || BattleFuncs.isSealedSkillEffect(effectUnit, x)))
            return false;
          switch (num2)
          {
            case 0:
              if ((effect.GetInt(BattleskillEffectLogicArgumentEnum.target_element) == 0 || (CommonElement) effect.GetInt(BattleskillEffectLogicArgumentEnum.target_element) == targetUnit.originalUnit.playerUnit.GetElement()) && (effect.GetInt(BattleskillEffectLogicArgumentEnum.element) == 0 || (CommonElement) effect.GetInt(BattleskillEffectLogicArgumentEnum.element) == effectUnit.originalUnit.playerUnit.GetElement()) && ((effect.GetInt(BattleskillEffectLogicArgumentEnum.target_gear_kind_id) == 0 || effect.GetInt(BattleskillEffectLogicArgumentEnum.target_gear_kind_id) == targetUnit.originalUnit.unit.kind.ID) && (effect.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) == 0 || effect.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) == effectUnit.originalUnit.unit.kind.ID)) && ((effect.GetInt(BattleskillEffectLogicArgumentEnum.target_family_id) == 0 || targetUnit.originalUnit.playerUnit.HasFamily((UnitFamily) effect.GetInt(BattleskillEffectLogicArgumentEnum.target_family_id))) && (BattleFuncs.checkInvokeSkillEffect(pse, effectUnit, targetUnit, colosseumTurn, (Judgement.NonBattleParameter.FromPlayerUnitCache) null, (Judgement.NonBattleParameter.FromPlayerUnitCache) null, new int?(effectUnitHp), new int?(targetUnitHp)) && !BattleFuncs.isEffectEnemyRangeAndInvalid(x, effectUnit, targetUnit))))
                return !BattleFuncs.isSkillsAndEffectsInvalid(effectUnit, targetUnit, x);
              break;
            case 1:
              if (!isOneMoreAttack)
                goto case 0;
              else
                goto default;
            default:
              if (!(num2 == 2 & isOneMoreAttack))
                break;
              goto case 0;
          }
          return false;
        }))))
        {
          int index = headerEffect.effect.GetInt(BattleskillEffectLogicArgumentEnum.skill_id);
          bool isUnconditional;
          if (index < 0)
          {
            isUnconditional = true;
            index = -index;
          }
          else
            isUnconditional = false;
          if (index != 0 && MasterData.BattleskillSkill.ContainsKey(index))
          {
            float lottery = headerEffect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.percentage_invest) + headerEffect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.percentage_invest_levelup) * (float) headerEffect.baseSkillLevel;
            BattleFuncs.PackedSkillEffect packedSkillEffect = BattleFuncs.PackedSkillEffect.Create(headerEffect);
            int onceInvestFlag = packedSkillEffect.HasKey(BattleskillEffectLogicArgumentEnum.once_invest) ? packedSkillEffect.GetInt(BattleskillEffectLogicArgumentEnum.once_invest) : 0;
            BattleskillSkill battleskillSkill = MasterData.BattleskillSkill[index];
            foreach (BL.ISkillEffectListUnit skillEffectListUnit in BattleFuncs.getInvestUnit(effectUnit, targetUnit, index, headerEffect.effect.GetInt(BattleskillEffectLogicArgumentEnum.range_type), isAI, isColosseum))
            {
              if (battleskillSkill.skill_type == BattleskillSkillType.ailment)
              {
                if (BattleFuncs.isAilmentInvest(lottery, index, skillEffectListUnit, random, isColosseum))
                  ret.duelSkillProc.addInvestSkills(skillEffectListUnit, BattleFuncs.ailmentInvest(index, skillEffectListUnit), effectUnit, headerEffect.baseSkillId, true, isUnconditional, 0, onceInvestFlag);
              }
              else if ((double) lottery >= (double) random.NextFloat())
                ret.duelSkillProc.addInvestSkills(skillEffectListUnit, new BL.Skill[1]
                {
                  new BL.Skill() { id = index, level = 1 }
                }, effectUnit, headerEffect.baseSkillId, false, (isUnconditional ? 1 : 0) != 0, 0, onceInvestFlag);
            }
          }
        }
      });
      action4(attack.skillEffects.Where(BattleskillEffectLogicEnum.passive_invest_passive).Concat<BL.SkillEffect>(BattleFuncs.getAttackMethodExtraSkillEffects(attackStatus, BattleskillEffectLogicEnum.passive_invest_passive)), attack, defense, isAttacker ? attackerHp : defenderHp, isAttacker ? defenderHp : attackerHp, false);
      action4(defense.skillEffects.Where(BattleskillEffectLogicEnum.passive_invest_passive2), defense, attack, isAttacker ? defenderHp : attackerHp, isAttacker ? attackerHp : defenderHp, true);
      return ret;
    }

    public static void calcSingleAttack(
      List<BL.DuelTurn> turns,
      List<BL.DuelTurn> turnsTemp,
      TurnHp hp,
      bool isAttacker,
      BL.ISkillEffectListUnit attack,
      AttackStatus attackStatus,
      BL.Panel attackPanel,
      BL.ISkillEffectListUnit defense,
      AttackStatus defenseStatus,
      BL.Panel defensePanel,
      int distance,
      XorShift random,
      bool isAI,
      int? colosseumTurn,
      bool invokedAmbush,
      bool invokedPrayer,
      int defenseHp,
      bool isOneMoreAttack,
      int attackedCount,
      bool isInvalidAttackDuelSkill,
      bool isEnableOverKill)
    {
      if (!isEnableOverKill && hp.isDieAttackerOrDefender())
        return;
      BL.MagicBullet magicBullet = attackStatus.magicBullet;
      int num1 = attackedCount > 0 || magicBullet == null ? 0 : magicBullet.cost;
      if (magicBullet != null && (isAttacker ? hp.attackerHp : hp.defenderHp) <= num1)
        return;
      if (!invokedPrayer)
      {
        invokedPrayer = BattleFuncs.isInvokedDefenderSkillLogic((IEnumerable<BL.DuelTurn>) turns, BattleskillEffectLogicEnum.prayer, new bool?(isAttacker));
        if (!invokedPrayer)
          invokedPrayer = BattleFuncs.isInvokedDefenderConsumeSkillLogic((IEnumerable<BL.DuelTurn>) turns, BattleskillEffectLogicEnum.passive_prayer, new bool?(isAttacker));
      }
      bool hasValue = colosseumTurn.HasValue;
      IEnumerable<BattleskillSkill> battleskillSkills = defense.skillEffects.Where(BattleskillSkillType.ailment, false);
      IEnumerable<BattleskillSkill> second = attack.skillEffects.Where(BattleskillSkillType.ailment, false);
      BL.DuelTurn turn = new BL.DuelTurn();
      turn.isAtackker = isAttacker;
      turn.attackStatus = attackStatus;
      turn.attackerStatus = (AttackStatus) null;
      turn.defenderStatus = (AttackStatus) null;
      turn.counterDamage = magicBullet != null ? num1 : 0;
      turn.skillIds = ((IEnumerable<BL.Skill>) attack.originalUnit.skills).Select<BL.Skill, int>((Func<BL.Skill, int>) (x => x.id)).ToArray<int>();
      if (isAttacker)
        hp.attackerHp -= turn.counterDamage;
      else
        hp.defenderHp -= turn.counterDamage;
      float? criticalRate = new float?();
      float? invokeRate = new float?();
      List<BL.Skill> first = new List<BL.Skill>();
      if ((isAttacker ? (hp.attackerIsDontUseSkill ? 1 : 0) : (hp.defenderIsDontUseSkill ? 1 : 0)) == 0)
      {
        IEnumerable<Tuple<BattleskillSkill, int, int>> source = ((IEnumerable<Tuple<BattleskillSkill, int, int>>) attack.originalUnit.unitAndGearSkills).Where<Tuple<BattleskillSkill, int, int>>((Func<Tuple<BattleskillSkill, int, int>, bool>) (skill => ((IEnumerable<BattleskillEffect>) skill.Item1.Effects).Any<BattleskillEffect>((Func<BattleskillEffect, bool>) (effect => effect.EffectLogic.Enum == BattleskillEffectLogicEnum.fierce_rival && !attack.IsDontUseSkill(skill.Item1.ID)))));
        if (source.Any<Tuple<BattleskillSkill, int, int>>() && !BattleFuncs.isSkillsAndEffectsInvalid(attack, defense, (BL.SkillEffect) null))
        {
          BL.DuelTurn duelTurn = turns.LastOrDefault<BL.DuelTurn>();
          foreach (Tuple<BattleskillSkill, int, int> tuple in source)
          {
            bool flag = false;
            foreach (BattleskillEffect battleskillEffect in ((IEnumerable<BattleskillEffect>) tuple.Item1.Effects).Where<BattleskillEffect>((Func<BattleskillEffect, bool>) (x => x.EffectLogic.Enum == BattleskillEffectLogicEnum.fierce_rival)))
            {
              int num2 = battleskillEffect.GetInt(BattleskillEffectLogicArgumentEnum.invoke_skill);
              int num3 = battleskillEffect.GetInt(BattleskillEffectLogicArgumentEnum.invoke_critical);
              int num4 = battleskillEffect.GetInt(BattleskillEffectLogicArgumentEnum.invoke_turn);
              flag = battleskillEffect.checkLevel(tuple.Item2) && (!battleskillEffect.HasKey(BattleskillEffectLogicArgumentEnum.gear_kind_id) || battleskillEffect.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) == 0 || battleskillEffect.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) == attack.originalUnit.unit.kind.ID) && ((!battleskillEffect.HasKey(BattleskillEffectLogicArgumentEnum.target_gear_kind_id) || battleskillEffect.GetInt(BattleskillEffectLogicArgumentEnum.target_gear_kind_id) == 0 || battleskillEffect.GetInt(BattleskillEffectLogicArgumentEnum.target_gear_kind_id) == defense.originalUnit.unit.kind.ID) && (!battleskillEffect.HasKey(BattleskillEffectLogicArgumentEnum.element) || battleskillEffect.GetInt(BattleskillEffectLogicArgumentEnum.element) == 0 || (CommonElement) battleskillEffect.GetInt(BattleskillEffectLogicArgumentEnum.element) == attack.originalUnit.playerUnit.GetElement())) && (!battleskillEffect.HasKey(BattleskillEffectLogicArgumentEnum.target_element) || battleskillEffect.GetInt(BattleskillEffectLogicArgumentEnum.target_element) == 0 || (CommonElement) battleskillEffect.GetInt(BattleskillEffectLogicArgumentEnum.target_element) == defense.originalUnit.playerUnit.GetElement()) && (num4 == 0 || num4 == 1 && duelTurn != null && isAttacker != duelTurn.isAtackker || num4 == 2 && duelTurn != null && isAttacker == duelTurn.isAtackker) && (num2 == 0 || num2 == 1 && duelTurn != null && ((IEnumerable<BL.Skill>) duelTurn.invokeDuelSkills).Any<BL.Skill>((Func<BL.Skill, bool>) (x =>
              {
                BattleskillGenre? genre1 = x.genre1;
                BattleskillGenre battleskillGenre = BattleskillGenre.attack;
                return genre1.GetValueOrDefault() == battleskillGenre & genre1.HasValue;
              })) || num2 == 2 && duelTurn != null && !((IEnumerable<BL.Skill>) duelTurn.invokeDuelSkills).Any<BL.Skill>((Func<BL.Skill, bool>) (x =>
              {
                BattleskillGenre? genre1 = x.genre1;
                BattleskillGenre battleskillGenre = BattleskillGenre.attack;
                return genre1.GetValueOrDefault() == battleskillGenre & genre1.HasValue;
              }))) && (num3 == 0 || num3 == 1 && duelTurn != null && duelTurn.isCritical || num3 == 2 && duelTurn != null && !duelTurn.isCritical) && BattleFuncs.isInvoke(attack, defense, attackStatus.duelParameter.attackerUnitParameter, attackStatus.duelParameter.defenderUnitParameter, attackStatus, defenseStatus, tuple.Item2, battleskillEffect.GetFloat(BattleskillEffectLogicArgumentEnum.percentage_invocation), random, true, isAttacker ? hp.defenderHp : hp.attackerHp, isAttacker ? hp.attackerHp : hp.defenderHp, colosseumTurn, new float?(), (BattleskillEffect) null, 0.0f, 1f, 1f, (List<BattleFuncs.InvalidSpecificSkillLogic>) null);
              if (flag)
              {
                float num5;
                if ((double) (num5 = battleskillEffect.GetFloat(BattleskillEffectLogicArgumentEnum.counter_critical_rate)) != 0.0 && !BattleFuncs.isCriticalGuardEnable(defense, attack))
                  criticalRate = new float?(num5);
                float num6;
                if ((double) (num6 = battleskillEffect.GetFloat(BattleskillEffectLogicArgumentEnum.counter_skill_rate)) != 0.0)
                  invokeRate = new float?(num6);
                first.Add(new BL.Skill()
                {
                  id = tuple.Item1.ID
                });
                break;
              }
            }
            if (flag)
              break;
          }
        }
      }
      List<BL.ISkillEffectListUnit> turnInvestUnit = new List<BL.ISkillEffectListUnit>();
      List<int> turnInvestSkillIds = new List<int>();
      List<BL.ISkillEffectListUnit> turnInvestFrom = new List<BL.ISkillEffectListUnit>();
      List<int> turnInvestFromSkillIds = new List<int>();
      BL.Skill[] skills = (BL.Skill[]) null;
      BattleDuelSkill battleDuelSkill1 = BattleDuelSkill.invokeBiAttackSkills(attack, attackStatus, attackPanel, defense, defenseStatus, defensePanel, distance, isAttacker ? hp.attackerHp : hp.defenderHp, isAttacker ? hp.defenderHp : hp.attackerHp, isAttacker ? hp.attackerIsDontUseSkill : hp.defenderIsDontUseSkill, isAttacker ? hp.defenderIsDontUseSkill : hp.attackerIsDontUseSkill, random, isAI, colosseumTurn, isAttacker, invokedAmbush, invokeRate, isOneMoreAttack, isInvalidAttackDuelSkill);
      turn.invokeAttackerDuelSkillEffectIds = new List<int>();
      turn.invokeDefenderDuelSkillEffectIds = new List<int>();
      if (battleDuelSkill1.attackCount > 1)
      {
        List<BL.SuiseiResult> source1 = new List<BL.SuiseiResult>();
        List<BL.Skill> source2 = new List<BL.Skill>();
        List<BL.Skill> source3 = new List<BL.Skill>();
        List<BL.Skill> skillList1 = new List<BL.Skill>();
        List<BL.Skill> skillList2 = new List<BL.Skill>();
        List<BL.Skill> givePassiveSkills = new List<BL.Skill>();
        List<BL.UseSkillEffect> useSkillEffectList1 = new List<BL.UseSkillEffect>();
        List<BL.UseSkillEffect> useSkillEffectList2 = new List<BL.UseSkillEffect>();
        turn.invokeSkillExtraInfo = new List<string>();
        turn.damageShareUnit = new List<BL.ISkillEffectListUnit>();
        turn.damageShareDamage = new List<int>();
        turn.damageShareSkillEffect = new List<BL.UseSkillEffect>();
        for (int index = 0; index < battleDuelSkill1.attackCount; ++index)
        {
          BattleFuncs.AttackDamageData attackDamageData = BattleFuncs.calcAttackDamage(random, isAttacker, hp, attack, attackStatus, attackPanel, defense, defenseStatus, defensePanel, distance, isAI, colosseumTurn, battleDuelSkill1, invokedAmbush, invokedPrayer, invokeRate, criticalRate, defenseHp, isOneMoreAttack, true, index, isInvalidAttackDuelSkill);
          if (!invokedPrayer)
          {
            invokedPrayer = ((IEnumerable<BL.Skill>) attackDamageData.duelSkillProc.defenderSkills).Any<BL.Skill>((Func<BL.Skill, bool>) (x => ((IEnumerable<BattleskillEffect>) x.skill.Effects).Any<BattleskillEffect>((Func<BattleskillEffect, bool>) (y => y.EffectLogic.Enum == BattleskillEffectLogicEnum.prayer))));
            if (!invokedPrayer)
              invokedPrayer = attackDamageData.defenderUseSkillEffects.Any<BL.UseSkillEffect>((Func<BL.UseSkillEffect, bool>) (x => MasterData.BattleskillEffect[x.effectEffectId].EffectLogic.Enum == BattleskillEffectLogicEnum.passive_prayer));
          }
          useSkillEffectList1.AddRange((IEnumerable<BL.UseSkillEffect>) attackDamageData.attackerUseSkillEffects);
          useSkillEffectList2.AddRange((IEnumerable<BL.UseSkillEffect>) attackDamageData.defenderUseSkillEffects);
          turn.invokeSkillExtraInfo.AddRange((IEnumerable<string>) attackDamageData.invokeSkillExtraInfo);
          turn.damageShareUnit.AddRange((IEnumerable<BL.ISkillEffectListUnit>) attackDamageData.damageShareUnit);
          turn.damageShareDamage.AddRange((IEnumerable<int>) attackDamageData.damageShareDamage);
          turn.damageShareSkillEffect.AddRange((IEnumerable<BL.UseSkillEffect>) attackDamageData.damageShareSkillEffect);
          BattleFuncs.recordInvokeDuelSkillEffectIds(attack, defense, turn, attackDamageData.isHit, attackDamageData.duelSkillProc.invokeAttackerDuelSkillEffectIds, attackDamageData.duelSkillProc.invokeDefenderDuelSkillEffectIds);
          turn.counterDamage += attackDamageData.counterDamage;
          BL.SuiseiResult suiseiResult = new BL.SuiseiResult();
          suiseiResult.damage = attackDamageData.damage;
          suiseiResult.dispDamage = attackDamageData.dispDamage;
          suiseiResult.realDamage = attackDamageData.realDamage;
          suiseiResult.invokeDuelSkills = ((IEnumerable<BL.Skill>) attackDamageData.duelSkillProc.attackerSkills).Concat<BL.Skill>((IEnumerable<BL.Skill>) attackDamageData.duelSkillProc.attackerElementSkills).ToArray<BL.Skill>();
          suiseiResult.invokeDefenderDuelSkills = ((IEnumerable<BL.Skill>) attackDamageData.duelSkillProc.defenderSkills).Concat<BL.Skill>((IEnumerable<BL.Skill>) attackDamageData.duelSkillProc.defenderElementSkills).ToArray<BL.Skill>();
          suiseiResult.invokeSkillExtraInfo = attackDamageData.invokeSkillExtraInfo;
          suiseiResult.drainDamage = attackDamageData.drainDamage;
          suiseiResult.dispDrainDamage = attackDamageData.dispDrainDamage;
          suiseiResult.isCritical = attackDamageData.isCritical;
          suiseiResult.isHit = attackDamageData.isHit;
          suiseiResult.defenderDrainDamage = attackDamageData.defenderDrainDamage;
          suiseiResult.defenderDispDrainDamage = attackDamageData.defenderDispDrainDamage;
          source1.Add(suiseiResult);
          source2.AddRange((IEnumerable<BL.Skill>) suiseiResult.invokeDefenderDuelSkills);
          source3.AddRange((IEnumerable<BL.Skill>) suiseiResult.invokeDuelSkills);
          BattleFuncs.assortOneAttackInvest(attack, defense, battleDuelSkill1, attackDamageData.duelSkillProc, turnInvestUnit, turnInvestSkillIds, turnInvestFrom, turnInvestFromSkillIds, givePassiveSkills, skillList2, skillList1, suiseiResult.isHit, index, turns);
        }
        turn.suiseiResults = source1;
        turn.isHit = source1.Any<BL.SuiseiResult>((Func<BL.SuiseiResult, bool>) (x => x.isHit));
        turn.isCritical = source1.Any<BL.SuiseiResult>((Func<BL.SuiseiResult, bool>) (x => x.isCritical));
        turn.invokeDuelSkills = first.Concat<BL.Skill>((IEnumerable<BL.Skill>) battleDuelSkill1.attackerSkills).Concat<BL.Skill>(source3.GroupBy<BL.Skill, int>((Func<BL.Skill, int>) (x => x.id)).Select<IGrouping<int, BL.Skill>, BL.Skill>((Func<IGrouping<int, BL.Skill>, BL.Skill>) (x => x.FirstOrDefault<BL.Skill>()))).ToArray<BL.Skill>();
        turn.invokeDefenderDuelSkills = ((IEnumerable<BL.Skill>) battleDuelSkill1.defenderSkills).Concat<BL.Skill>(source2.GroupBy<BL.Skill, int>((Func<BL.Skill, int>) (x => x.id)).Select<IGrouping<int, BL.Skill>, BL.Skill>((Func<IGrouping<int, BL.Skill>, BL.Skill>) (x => x.FirstOrDefault<BL.Skill>()))).ToArray<BL.Skill>();
        turn.damage = source1.Sum<BL.SuiseiResult>((Func<BL.SuiseiResult, int>) (x => x.damage));
        turn.dispDamage = source1.Sum<BL.SuiseiResult>((Func<BL.SuiseiResult, int>) (x => x.dispDamage));
        turn.realDamage = source1.Sum<BL.SuiseiResult>((Func<BL.SuiseiResult, int>) (x => x.realDamage));
        turn.drainDamage = source1.Sum<BL.SuiseiResult>((Func<BL.SuiseiResult, int>) (x => x.drainDamage));
        turn.dispDrainDamage = source1.Sum<BL.SuiseiResult>((Func<BL.SuiseiResult, int>) (x => x.dispDrainDamage));
        turn.defenderDrainDamage = source1.Sum<BL.SuiseiResult>((Func<BL.SuiseiResult, int>) (x => x.defenderDrainDamage));
        turn.defenderDispDrainDamage = source1.Sum<BL.SuiseiResult>((Func<BL.SuiseiResult, int>) (x => x.defenderDispDrainDamage));
        turn.attackerRestHp = hp.attackerHp;
        turn.defenderRestHp = hp.defenderHp;
        turn.attackerUseSkillEffects = useSkillEffectList1;
        turn.defenderUseSkillEffects = useSkillEffectList2;
        if (skillList1.Count > 0)
          turn.invokeAilmentSkills = skillList1.GroupBy<BL.Skill, int>((Func<BL.Skill, int>) (x => x.id)).Select<IGrouping<int, BL.Skill>, BL.Skill>((Func<IGrouping<int, BL.Skill>, BL.Skill>) (x => x.FirstOrDefault<BL.Skill>())).ToArray<BL.Skill>();
        if (skillList2.Count > 0)
          skills = skillList2.GroupBy<BL.Skill, int>((Func<BL.Skill, int>) (x => x.id)).Select<IGrouping<int, BL.Skill>, BL.Skill>((Func<IGrouping<int, BL.Skill>, BL.Skill>) (x => x.FirstOrDefault<BL.Skill>())).ToArray<BL.Skill>();
        if (givePassiveSkills.Count > 0)
          turn.invokeGiveSkills = givePassiveSkills.ToArray();
      }
      else
      {
        BattleFuncs.AttackDamageData attackDamageData = BattleFuncs.calcAttackDamage(random, isAttacker, hp, attack, attackStatus, attackPanel, defense, defenseStatus, defensePanel, distance, isAI, colosseumTurn, battleDuelSkill1, invokedAmbush, invokedPrayer, invokeRate, criticalRate, defenseHp, isOneMoreAttack, false, 0, isInvalidAttackDuelSkill);
        turn.counterDamage += attackDamageData.counterDamage;
        turn.isHit = attackDamageData.isHit;
        turn.isCritical = attackDamageData.isCritical;
        turn.invokeDuelSkills = first.Concat<BL.Skill>((IEnumerable<BL.Skill>) battleDuelSkill1.attackerSkills).Concat<BL.Skill>((IEnumerable<BL.Skill>) attackDamageData.duelSkillProc.attackerSkills).Concat<BL.Skill>((IEnumerable<BL.Skill>) attackDamageData.duelSkillProc.attackerElementSkills).ToArray<BL.Skill>();
        turn.invokeDefenderDuelSkills = ((IEnumerable<BL.Skill>) battleDuelSkill1.defenderSkills).Concat<BL.Skill>((IEnumerable<BL.Skill>) attackDamageData.duelSkillProc.defenderSkills).Concat<BL.Skill>((IEnumerable<BL.Skill>) attackDamageData.duelSkillProc.defenderElementSkills).ToArray<BL.Skill>();
        turn.damage = attackDamageData.damage;
        turn.dispDamage = attackDamageData.dispDamage;
        turn.realDamage = attackDamageData.realDamage;
        turn.drainDamage = attackDamageData.drainDamage;
        turn.dispDrainDamage = attackDamageData.dispDrainDamage;
        turn.defenderDrainDamage = attackDamageData.defenderDrainDamage;
        turn.defenderDispDrainDamage = attackDamageData.defenderDispDrainDamage;
        turn.attackerRestHp = attackDamageData.hp.attackerHp;
        turn.defenderRestHp = attackDamageData.hp.defenderHp;
        turn.attackerUseSkillEffects = attackDamageData.attackerUseSkillEffects;
        turn.defenderUseSkillEffects = attackDamageData.defenderUseSkillEffects;
        turn.invokeSkillExtraInfo = attackDamageData.invokeSkillExtraInfo;
        turn.damageShareUnit = attackDamageData.damageShareUnit;
        turn.damageShareDamage = attackDamageData.damageShareDamage;
        turn.damageShareSkillEffect = attackDamageData.damageShareSkillEffect;
        BattleFuncs.recordInvokeDuelSkillEffectIds(attack, defense, turn, turn.isHit, attackDamageData.duelSkillProc.invokeAttackerDuelSkillEffectIds, attackDamageData.duelSkillProc.invokeDefenderDuelSkillEffectIds);
        List<BL.Skill> givePassiveSkills = new List<BL.Skill>();
        List<BL.Skill> giveAttackerAilmentSkills = new List<BL.Skill>();
        List<BL.Skill> giveDefenderAilmentSkills = new List<BL.Skill>();
        BattleFuncs.assortOneAttackInvest(attack, defense, battleDuelSkill1, attackDamageData.duelSkillProc, turnInvestUnit, turnInvestSkillIds, turnInvestFrom, turnInvestFromSkillIds, givePassiveSkills, giveAttackerAilmentSkills, giveDefenderAilmentSkills, turn.isHit, 0, turns);
        if (giveDefenderAilmentSkills.Count > 0)
          turn.invokeAilmentSkills = giveDefenderAilmentSkills.ToArray();
        if (giveAttackerAilmentSkills.Count > 0)
          skills = giveAttackerAilmentSkills.ToArray();
        if (givePassiveSkills.Count > 0)
          turn.invokeGiveSkills = givePassiveSkills.ToArray();
      }
      BattleFuncs.recordInvokeDuelSkillEffectIds(attack, defense, turn, turn.isHit, battleDuelSkill1.invokeAttackerDuelSkillEffectIds, battleDuelSkill1.invokeDefenderDuelSkillEffectIds);
      turn.attackerCombiUnit = battleDuelSkill1.attackerCombiUnit;
      if (!hasValue)
      {
        BattleDuelSkill battleDuelSkill2 = BattleDuelSkill.invokeAilmentSkills(attack, attackStatus, defense, turn.isHit, isAttacker ? hp.attackerIsDontUseSkill : hp.defenderIsDontUseSkill, random, isAI, colosseumTurn);
        List<BL.Skill> skillList1 = new List<BL.Skill>();
        if (turn.isHit)
        {
          BL.Skill[] investSkills = battleDuelSkill2.getInvestSkills(defense, true, false, 0, (List<BL.DuelTurn>) null, (List<BL.ISkillEffectListUnit>) null, (List<int>) null, (List<BL.ISkillEffectListUnit>) null);
          if (investSkills != null)
            skillList1.AddRange((IEnumerable<BL.Skill>) investSkills);
        }
        BL.Skill[] investSkills1 = battleDuelSkill2.getInvestSkills(defense, true, true, 0, (List<BL.DuelTurn>) null, (List<BL.ISkillEffectListUnit>) null, (List<int>) null, (List<BL.ISkillEffectListUnit>) null);
        if (investSkills1 != null)
          skillList1.AddRange((IEnumerable<BL.Skill>) investSkills1);
        if (skillList1.Count > 0)
          turn.invokeAilmentSkills = turn.invokeAilmentSkills != null ? ((IEnumerable<BL.Skill>) turn.invokeAilmentSkills).Concat<BL.Skill>((IEnumerable<BL.Skill>) skillList1.ToArray()).ToArray<BL.Skill>() : skillList1.ToArray();
        List<BL.Skill> skillList2 = new List<BL.Skill>();
        if (turn.isHit)
        {
          BL.Skill[] investSkills2 = battleDuelSkill2.getInvestSkills(attack, true, false, 0, (List<BL.DuelTurn>) null, (List<BL.ISkillEffectListUnit>) null, (List<int>) null, (List<BL.ISkillEffectListUnit>) null);
          if (investSkills2 != null)
            skillList2.AddRange((IEnumerable<BL.Skill>) investSkills2);
        }
        BL.Skill[] investSkills3 = battleDuelSkill2.getInvestSkills(attack, true, true, 0, (List<BL.DuelTurn>) null, (List<BL.ISkillEffectListUnit>) null, (List<int>) null, (List<BL.ISkillEffectListUnit>) null);
        if (investSkills3 != null)
          skillList2.AddRange((IEnumerable<BL.Skill>) investSkills3);
        if (skillList2.Count > 0)
          skills = skills != null ? ((IEnumerable<BL.Skill>) skills).Concat<BL.Skill>((IEnumerable<BL.Skill>) skillList2.ToArray()).ToArray<BL.Skill>() : skillList2.ToArray();
        if (turn.isHit)
        {
          Tuple<BL.ISkillEffectListUnit[], int[], BL.ISkillEffectListUnit[], int[]> allInvestList = battleDuelSkill2.getAllInvestList(true, false, 0, (List<BL.DuelTurn>) null, (List<BL.ISkillEffectListUnit>) null, (List<int>) null, (List<BL.ISkillEffectListUnit>) null);
          if (allInvestList != null)
          {
            turnInvestUnit.AddRange((IEnumerable<BL.ISkillEffectListUnit>) allInvestList.Item1);
            turnInvestSkillIds.AddRange((IEnumerable<int>) allInvestList.Item2);
            turnInvestFrom.AddRange((IEnumerable<BL.ISkillEffectListUnit>) allInvestList.Item3);
            turnInvestFromSkillIds.AddRange((IEnumerable<int>) allInvestList.Item4);
          }
        }
        Tuple<BL.ISkillEffectListUnit[], int[], BL.ISkillEffectListUnit[], int[]> allInvestList1 = battleDuelSkill2.getAllInvestList(true, true, 0, (List<BL.DuelTurn>) null, (List<BL.ISkillEffectListUnit>) null, (List<int>) null, (List<BL.ISkillEffectListUnit>) null);
        if (allInvestList1 != null)
        {
          turnInvestUnit.AddRange((IEnumerable<BL.ISkillEffectListUnit>) allInvestList1.Item1);
          turnInvestSkillIds.AddRange((IEnumerable<int>) allInvestList1.Item2);
          turnInvestFrom.AddRange((IEnumerable<BL.ISkillEffectListUnit>) allInvestList1.Item3);
          turnInvestFromSkillIds.AddRange((IEnumerable<int>) allInvestList1.Item4);
        }
        turn.invokeDuelSkills = ((IEnumerable<BL.Skill>) turn.invokeDuelSkills).Concat<BL.Skill>((IEnumerable<BL.Skill>) battleDuelSkill2.attackerSkills).ToArray<BL.Skill>();
        if (turn.invokeAilmentSkills != null)
        {
          if (turn.dispDamage >= 1)
            battleskillSkills = battleskillSkills.Where<BattleskillSkill>((Func<BattleskillSkill, bool>) (x => ((IEnumerable<BattleskillEffect>) x.Effects).Any<BattleskillEffect>((Func<BattleskillEffect, bool>) (y => y.EffectLogic.Enum != BattleskillEffectLogicEnum.sleep))));
          IEnumerable<BattleskillSkill> source = ((IEnumerable<BL.Skill>) turn.invokeAilmentSkills).Select<BL.Skill, BattleskillSkill>((Func<BL.Skill, BattleskillSkill>) (x => x.skill)).Except<BattleskillSkill>(battleskillSkills);
          turn.ailmentEffects = source.Select<BattleskillSkill, BattleskillAilmentEffect>((Func<BattleskillSkill, BattleskillAilmentEffect>) (x => x.ailment_effect)).ToArray<BattleskillAilmentEffect>();
          if (isAttacker)
          {
            hp.defenderIsDontAction |= BL.Skill.HasDontActionEffect(turn.invokeAilmentSkills);
            hp.defenderIsDontEvasion |= BL.Skill.HasDontEvasionEffect(turn.invokeAilmentSkills);
            hp.defenderIsDontUseSkill |= BL.Skill.HasDontActionEffect(turn.invokeAilmentSkills);
          }
          else
          {
            hp.attackerIsDontAction |= BL.Skill.HasDontActionEffect(turn.invokeAilmentSkills);
            hp.attackerIsDontEvasion |= BL.Skill.HasDontEvasionEffect(turn.invokeAilmentSkills);
            hp.attackerIsDontUseSkill |= BL.Skill.HasDontActionEffect(turn.invokeAilmentSkills);
          }
        }
        if (skills != null)
        {
          IEnumerable<BattleskillSkill> source = ((IEnumerable<BL.Skill>) skills).Select<BL.Skill, BattleskillSkill>((Func<BL.Skill, BattleskillSkill>) (x => x.skill)).Except<BattleskillSkill>(second);
          turn.attackerAilmentEffects = source.Select<BattleskillSkill, BattleskillAilmentEffect>((Func<BattleskillSkill, BattleskillAilmentEffect>) (x => x.ailment_effect)).ToArray<BattleskillAilmentEffect>();
          if (!isAttacker)
          {
            hp.defenderIsDontAction |= BL.Skill.HasDontActionEffect(skills);
            hp.defenderIsDontEvasion |= BL.Skill.HasDontEvasionEffect(skills);
            hp.defenderIsDontUseSkill |= BL.Skill.HasDontActionEffect(skills);
          }
          else
          {
            hp.attackerIsDontAction |= BL.Skill.HasDontActionEffect(skills);
            hp.attackerIsDontEvasion |= BL.Skill.HasDontEvasionEffect(skills);
            hp.attackerIsDontUseSkill |= BL.Skill.HasDontActionEffect(skills);
          }
        }
      }
      turn.investUnit = turnInvestUnit.ToArray();
      turn.investSkillIds = turnInvestSkillIds.ToArray();
      turn.investFrom = turnInvestFrom.ToArray();
      turn.investFromSkillIds = turnInvestFromSkillIds.ToArray();
      foreach (var data in ((IEnumerable<BL.ISkillEffectListUnit>) turn.investUnit).Select((unit, index) => new
      {
        unit = unit,
        index = index
      }))
      {
        BattleskillEffectLogicEnum[] battleskillEffectLogicEnumArray = new BattleskillEffectLogicEnum[3]
        {
          BattleskillEffectLogicEnum.white_night,
          BattleskillEffectLogicEnum.seal,
          BattleskillEffectLogicEnum.heal_impossible
        };
        if (data.unit == attack || data.unit == defense)
        {
          BL.Skill skill = new BL.Skill()
          {
            id = turn.investSkillIds[data.index]
          };
          if (!hasValue || skill.skill.skill_type != BattleskillSkillType.ailment)
          {
            if (skill.skill.target_type == BattleskillTargetType.complex_single || skill.skill.target_type == BattleskillTargetType.complex_range)
            {
              foreach (BattleskillEffect effect in skill.skill.Effects)
              {
                if (((IEnumerable<BattleskillEffectLogicEnum>) battleskillEffectLogicEnumArray).Contains<BattleskillEffectLogicEnum>(effect.EffectLogic.Enum))
                {
                  if (!effect.is_targer_enemy)
                  {
                    if (data.unit == attack)
                      data.unit.skillEffects.Add(BL.SkillEffect.FromMasterData(effect, skill.skill, 1, false, 0, (BL.Unit) null, 0, false, false, 0), new bool?(), (BL.ISkillEffectListUnit) null);
                  }
                  else if (data.unit == defense)
                    data.unit.skillEffects.Add(BL.SkillEffect.FromMasterData(effect, skill.skill, 1, false, 0, (BL.Unit) null, 0, false, false, 0), new bool?(), (BL.ISkillEffectListUnit) null);
                }
              }
            }
            else
            {
              foreach (BattleskillEffect effect in skill.skill.Effects)
              {
                if (((IEnumerable<BattleskillEffectLogicEnum>) battleskillEffectLogicEnumArray).Contains<BattleskillEffectLogicEnum>(effect.EffectLogic.Enum))
                  data.unit.skillEffects.Add(BL.SkillEffect.FromMasterData(effect, skill.skill, 1, false, 0, (BL.Unit) null, 0, false, false, 0), new bool?(), (BL.ISkillEffectListUnit) null);
              }
            }
          }
        }
      }
      turnsTemp.Add(turn);
    }

    public static int attackCount(BL.ISkillEffectListUnit attack, BL.ISkillEffectListUnit defense)
    {
      if (BattleFuncs.isSkillsAndEffectsInvalid(attack, defense, (BL.SkillEffect) null))
        return 1;
      int num = BattleFuncs.gearSkillEffectFilter(attack.originalUnit, attack.skillEffects.Where(BattleskillEffectLogicEnum.multiple_attack, (Func<BL.SkillEffect, bool>) (x =>
      {
        BattleskillEffect effect = x.effect;
        if (effect.HasKey(BattleskillEffectLogicArgumentEnum.gear_kind_id) && effect.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) != 0 && effect.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) != attack.originalUnit.unit.kind.ID || effect.HasKey(BattleskillEffectLogicArgumentEnum.target_gear_kind_id) && effect.GetInt(BattleskillEffectLogicArgumentEnum.target_gear_kind_id) != 0 && effect.GetInt(BattleskillEffectLogicArgumentEnum.target_gear_kind_id) != defense.originalUnit.unit.kind.ID || (effect.HasKey(BattleskillEffectLogicArgumentEnum.element) && effect.GetInt(BattleskillEffectLogicArgumentEnum.element) != 0 && (CommonElement) effect.GetInt(BattleskillEffectLogicArgumentEnum.element) != attack.originalUnit.playerUnit.GetElement() || effect.HasKey(BattleskillEffectLogicArgumentEnum.target_element) && effect.GetInt(BattleskillEffectLogicArgumentEnum.target_element) != 0 && (CommonElement) effect.GetInt(BattleskillEffectLogicArgumentEnum.target_element) != defense.originalUnit.playerUnit.GetElement()) || (effect.HasKey(BattleskillEffectLogicArgumentEnum.job_id) && effect.GetInt(BattleskillEffectLogicArgumentEnum.job_id) != 0 && effect.GetInt(BattleskillEffectLogicArgumentEnum.job_id) != attack.originalUnit.job.ID || effect.HasKey(BattleskillEffectLogicArgumentEnum.target_job_id) && effect.GetInt(BattleskillEffectLogicArgumentEnum.target_job_id) != 0 && effect.GetInt(BattleskillEffectLogicArgumentEnum.target_job_id) != defense.originalUnit.job.ID || effect.HasKey(BattleskillEffectLogicArgumentEnum.family_id) && effect.GetInt(BattleskillEffectLogicArgumentEnum.family_id) != 0 && !attack.originalUnit.playerUnit.HasFamily((UnitFamily) effect.GetInt(BattleskillEffectLogicArgumentEnum.family_id))))
          return false;
        return !effect.HasKey(BattleskillEffectLogicArgumentEnum.target_family_id) || effect.GetInt(BattleskillEffectLogicArgumentEnum.target_family_id) == 0 || defense.originalUnit.playerUnit.HasFamily((UnitFamily) effect.GetInt(BattleskillEffectLogicArgumentEnum.target_family_id));
      }))).Sum<BL.SkillEffect>((Func<BL.SkillEffect, int>) (x => x.effect.GetInt(BattleskillEffectLogicArgumentEnum.value)));
      return num <= 0 ? 1 : num;
    }

    public static bool canOneMore(
      Judgement.BeforeDuelUnitParameter attack,
      Judgement.BeforeDuelUnitParameter defense,
      BL.ISkillEffectListUnit myself,
      BL.ISkillEffectListUnit enemy,
      bool isAttacker,
      bool isInvokedAmbush = false,
      XorShift random = null,
      AttackStatus attackStatus = null,
      AttackStatus defenseStatus = null,
      int myselfHp = 0,
      int enemyHp = 0,
      int? colosseumTurn = null,
      bool colosseumIsSample = false,
      BL.Panel attackPanel = null,
      BL.Panel defensePanel = null)
    {
      Func<BL.ISkillEffectListUnit, BL.ISkillEffectListUnit, int, BL.ISkillEffectListUnit, BL.ISkillEffectListUnit, bool> func = (Func<BL.ISkillEffectListUnit, BL.ISkillEffectListUnit, int, BL.ISkillEffectListUnit, BL.ISkillEffectListUnit, bool>) ((effectUnit, targetUnit, effect_target, unit, target) => effectUnit.skillEffects.Where(BattleskillEffectLogicEnum.defensive_formation, (Func<BL.SkillEffect, bool>) (x =>
      {
        BattleskillEffect effect = x.effect;
        if ((double) effect.GetFloat(BattleskillEffectLogicArgumentEnum.effect_target) != (double) effect_target || BattleFuncs.isSealedSkillEffect(effectUnit, x) || effect.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) != 0 && effect.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) != effectUnit.originalUnit.unit.kind.ID || (effect.GetInt(BattleskillEffectLogicArgumentEnum.target_gear_kind_id) != 0 && effect.GetInt(BattleskillEffectLogicArgumentEnum.target_gear_kind_id) != targetUnit.originalUnit.unit.kind.ID || effect.GetInt(BattleskillEffectLogicArgumentEnum.element) != 0 && (CommonElement) effect.GetInt(BattleskillEffectLogicArgumentEnum.element) != effectUnit.originalUnit.playerUnit.GetElement()) || (effect.GetInt(BattleskillEffectLogicArgumentEnum.target_element) != 0 && (CommonElement) effect.GetInt(BattleskillEffectLogicArgumentEnum.target_element) != targetUnit.originalUnit.playerUnit.GetElement() || effect.GetInt(BattleskillEffectLogicArgumentEnum.job_id) != 0 && effect.GetInt(BattleskillEffectLogicArgumentEnum.job_id) != effectUnit.originalUnit.job.ID || (effect.GetInt(BattleskillEffectLogicArgumentEnum.target_job_id) != 0 && effect.GetInt(BattleskillEffectLogicArgumentEnum.target_job_id) != targetUnit.originalUnit.job.ID || effect.GetInt(BattleskillEffectLogicArgumentEnum.family_id) != 0 && !effectUnit.originalUnit.playerUnit.HasFamily((UnitFamily) effect.GetInt(BattleskillEffectLogicArgumentEnum.family_id)))) || (effect.GetInt(BattleskillEffectLogicArgumentEnum.target_family_id) != 0 && !targetUnit.originalUnit.playerUnit.HasFamily((UnitFamily) effect.GetInt(BattleskillEffectLogicArgumentEnum.target_family_id)) || effectUnit == target && BattleFuncs.isSkillsAndEffectsInvalid(target, unit, (BL.SkillEffect) null) || (BattleFuncs.isEffectEnemyRangeAndInvalid(x, unit, target) || BattleFuncs.isSkillsAndEffectsInvalid(unit, target, (BL.SkillEffect) null))))
          return false;
        BattleFuncs.PackedSkillEffect packedSkillEffect = BattleFuncs.PackedSkillEffect.Create(x);
        BattleLandform landform = (BattleLandform) null;
        if (effect_target == 0 && attackPanel != null)
          landform = attackPanel.landform;
        else if (effect_target == 1 && defensePanel != null)
          landform = defensePanel.landform;
        if (!packedSkillEffect.CheckLandTag(landform))
          return false;
        if (!packedSkillEffect.HasKey(BattleskillEffectLogicArgumentEnum.percentage_invocation))
          return true;
        float percentage_invocation = packedSkillEffect.GetFloat(BattleskillEffectLogicArgumentEnum.percentage_invocation);
        if ((double) percentage_invocation >= 200.0)
          return true;
        return random != null && BattleFuncs.isInvoke(effectUnit, targetUnit, effect_target == 0 ? attack : defense, effect_target == 0 ? defense : attack, effect_target == 0 ? attackStatus : defenseStatus, effect_target == 0 ? defenseStatus : attackStatus, x.baseSkillLevel, percentage_invocation, random, false, effect_target == 0 ? myselfHp : enemyHp, effect_target == 0 ? enemyHp : myselfHp, colosseumTurn, new float?(), (BattleskillEffect) null, packedSkillEffect.HasKey(BattleskillEffectLogicArgumentEnum.base_invocation) ? packedSkillEffect.GetFloat(BattleskillEffectLogicArgumentEnum.base_invocation) : 0.0f, packedSkillEffect.HasKey(BattleskillEffectLogicArgumentEnum.invocation_skill_ratio) ? packedSkillEffect.GetFloat(BattleskillEffectLogicArgumentEnum.invocation_skill_ratio) : 1f, packedSkillEffect.HasKey(BattleskillEffectLogicArgumentEnum.invocation_luck_ratio) ? packedSkillEffect.GetFloat(BattleskillEffectLogicArgumentEnum.invocation_luck_ratio) : 1f, (List<BattleFuncs.InvalidSpecificSkillLogic>) null);
      })).Any<BL.SkillEffect>());
      bool flag1 = attack.AttackSpeed - defense.AttackSpeed >= 5;
      if (!flag1)
      {
        IEnumerable<BL.SkillEffect> source = BattleFuncs.gearSkillEffectFilter(myself.originalUnit, myself.skillEffects.Where(BattleskillEffectLogicEnum.absolute_one_more_attack, (Func<BL.SkillEffect, bool>) (x =>
        {
          BattleskillEffect effect = x.effect;
          if (!colosseumIsSample)
          {
            bool flag2 = isInvokedAmbush ? !isAttacker : isAttacker;
            if (effect.GetInt(BattleskillEffectLogicArgumentEnum.is_attack) == 1 && !flag2 || effect.GetInt(BattleskillEffectLogicArgumentEnum.is_attack) == 2 & flag2)
              return false;
          }
          else if (effect.GetInt(BattleskillEffectLogicArgumentEnum.is_attack) != 0)
            return false;
          return !BattleFuncs.isSealedSkillEffect(myself, x) && (effect.GetInt(BattleskillEffectLogicArgumentEnum.target_element) == 0 || (CommonElement) effect.GetInt(BattleskillEffectLogicArgumentEnum.target_element) == enemy.originalUnit.playerUnit.GetElement()) && ((effect.GetInt(BattleskillEffectLogicArgumentEnum.element) == 0 || (CommonElement) effect.GetInt(BattleskillEffectLogicArgumentEnum.element) == myself.originalUnit.playerUnit.GetElement()) && (effect.GetInt(BattleskillEffectLogicArgumentEnum.target_gear_kind_id) == 0 || effect.GetInt(BattleskillEffectLogicArgumentEnum.target_gear_kind_id) == enemy.originalUnit.unit.kind.ID)) && ((effect.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) == 0 || effect.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) == myself.originalUnit.unit.kind.ID) && (effect.GetInt(BattleskillEffectLogicArgumentEnum.target_family_id) == 0 || enemy.originalUnit.playerUnit.HasFamily((UnitFamily) effect.GetInt(BattleskillEffectLogicArgumentEnum.target_family_id))) && (effect.GetInt(BattleskillEffectLogicArgumentEnum.family_id) == 0 || myself.originalUnit.playerUnit.HasFamily((UnitFamily) effect.GetInt(BattleskillEffectLogicArgumentEnum.family_id)))) && (BattleFuncs.PackedSkillEffect.Create(x).CheckLandTag(attackPanel != null ? attackPanel.landform : (BattleLandform) null) && !BattleFuncs.isEffectEnemyRangeAndInvalid(x, myself, enemy)) && !BattleFuncs.isSkillsAndEffectsInvalid(myself, enemy, (BL.SkillEffect) null);
        })));
        flag1 |= source.Any<BL.SkillEffect>((Func<BL.SkillEffect, bool>) (x =>
        {
          BattleskillEffect effect = x.effect;
          float percentage_invocation = x.effect.GetFloat(BattleskillEffectLogicArgumentEnum.percentage_invocation);
          if ((double) percentage_invocation >= 200.0)
            return true;
          if (random == null)
            return false;
          BattleFuncs.PackedSkillEffect packedSkillEffect = BattleFuncs.PackedSkillEffect.Create(x);
          return BattleFuncs.isInvoke(myself, enemy, attack, defense, attackStatus, defenseStatus, x.baseSkillLevel, percentage_invocation, random, false, myselfHp, enemyHp, colosseumTurn, new float?(), (BattleskillEffect) null, packedSkillEffect.HasKey(BattleskillEffectLogicArgumentEnum.base_invocation) ? packedSkillEffect.GetFloat(BattleskillEffectLogicArgumentEnum.base_invocation) : 0.0f, packedSkillEffect.HasKey(BattleskillEffectLogicArgumentEnum.invocation_skill_ratio) ? packedSkillEffect.GetFloat(BattleskillEffectLogicArgumentEnum.invocation_skill_ratio) : 1f, packedSkillEffect.HasKey(BattleskillEffectLogicArgumentEnum.invocation_luck_ratio) ? packedSkillEffect.GetFloat(BattleskillEffectLogicArgumentEnum.invocation_luck_ratio) : 1f, (List<BattleFuncs.InvalidSpecificSkillLogic>) null);
        }));
      }
      return flag1 && !func(myself, enemy, 0, myself, enemy) && !func(enemy, myself, 1, myself, enemy);
    }

    public static int calcPercentageDamage(int preHp, float percentageDamageRate, int maxDamage)
    {
      int num;
      if (preHp <= 1)
      {
        num = preHp;
      }
      else
      {
        num = Mathf.CeilToInt((float) ((Decimal) preHp * (Decimal) percentageDamageRate));
        if (maxDamage != 0 && num > maxDamage)
          num = maxDamage;
        if (preHp - num < 1)
          num = preHp - 1;
      }
      return num;
    }

    public static bool isInvokedDefenderSkillLogic(
      IEnumerable<BL.DuelTurn> turns,
      BattleskillEffectLogicEnum skillLogic,
      bool? isAttacker = null)
    {
      return turns.Any<BL.DuelTurn>((Func<BL.DuelTurn, bool>) (x =>
      {
        if (isAttacker.HasValue)
        {
          int num1 = x.isAtackker ? 1 : 0;
          bool? nullable = isAttacker;
          int num2 = nullable.GetValueOrDefault() ? 1 : 0;
          if (!(num1 == num2 & nullable.HasValue))
            return false;
        }
        return ((IEnumerable<BL.Skill>) x.invokeDefenderDuelSkills).Any<BL.Skill>((Func<BL.Skill, bool>) (y => ((IEnumerable<BattleskillEffect>) y.skill.Effects).Any<BattleskillEffect>((Func<BattleskillEffect, bool>) (z => z.EffectLogic.Enum == skillLogic))));
      }));
    }

    public static bool isInvokedDefenderConsumeSkillLogic(
      IEnumerable<BL.DuelTurn> turns,
      BattleskillEffectLogicEnum skillLogic,
      bool? isAttacker = null)
    {
      return turns.Any<BL.DuelTurn>((Func<BL.DuelTurn, bool>) (x =>
      {
        if (isAttacker.HasValue)
        {
          int num1 = x.isAtackker ? 1 : 0;
          bool? nullable = isAttacker;
          int num2 = nullable.GetValueOrDefault() ? 1 : 0;
          if (!(num1 == num2 & nullable.HasValue))
            return false;
        }
        return x.defenderUseSkillEffects.Any<BL.UseSkillEffect>((Func<BL.UseSkillEffect, bool>) (y => MasterData.BattleskillEffect[y.effectEffectId].EffectLogic.Enum == skillLogic));
      }));
    }

    private static void assortOneAttackInvest(
      BL.ISkillEffectListUnit attack,
      BL.ISkillEffectListUnit defense,
      BattleDuelSkill biAttackSkills,
      BattleDuelSkill duelSkillProc,
      List<BL.ISkillEffectListUnit> turnInvestUnit,
      List<int> turnInvestSkillIds,
      List<BL.ISkillEffectListUnit> turnInvestFrom,
      List<int> turnInvestFromSkillIds,
      List<BL.Skill> givePassiveSkills,
      List<BL.Skill> giveAttackerAilmentSkills,
      List<BL.Skill> giveDefenderAilmentSkills,
      bool isHit,
      int attackNo,
      List<BL.DuelTurn> turns)
    {
      List<BL.ISkillEffectListUnit> turnInvestUnitBk = new List<BL.ISkillEffectListUnit>((IEnumerable<BL.ISkillEffectListUnit>) turnInvestUnit);
      List<int> turnInvestSkillIdsBk = new List<int>((IEnumerable<int>) turnInvestSkillIds);
      List<BL.ISkillEffectListUnit> turnInvestFromBk = new List<BL.ISkillEffectListUnit>((IEnumerable<BL.ISkillEffectListUnit>) turnInvestFrom);
      System.Action<bool> action = (System.Action<bool>) (isUnconditional =>
      {
        BL.Skill[] investSkills1 = biAttackSkills.getInvestSkills(attack, true, isUnconditional, attackNo, turns, turnInvestUnitBk, turnInvestSkillIdsBk, turnInvestFromBk);
        if (investSkills1 != null)
          giveAttackerAilmentSkills.AddRange((IEnumerable<BL.Skill>) investSkills1);
        BL.Skill[] investSkills2 = duelSkillProc.getInvestSkills(attack, true, isUnconditional, 0, turns, turnInvestUnitBk, turnInvestSkillIdsBk, turnInvestFromBk);
        if (investSkills2 != null)
          giveAttackerAilmentSkills.AddRange((IEnumerable<BL.Skill>) investSkills2);
        BL.Skill[] investSkills3 = biAttackSkills.getInvestSkills(defense, true, isUnconditional, attackNo, turns, turnInvestUnitBk, turnInvestSkillIdsBk, turnInvestFromBk);
        if (investSkills3 != null)
          giveDefenderAilmentSkills.AddRange((IEnumerable<BL.Skill>) investSkills3);
        BL.Skill[] investSkills4 = duelSkillProc.getInvestSkills(defense, true, isUnconditional, 0, turns, turnInvestUnitBk, turnInvestSkillIdsBk, turnInvestFromBk);
        if (investSkills4 != null)
          giveDefenderAilmentSkills.AddRange((IEnumerable<BL.Skill>) investSkills4);
        BL.Skill[] investSkills5 = biAttackSkills.getInvestSkills(attack, false, isUnconditional, attackNo, turns, turnInvestUnitBk, turnInvestSkillIdsBk, turnInvestFromBk);
        if (investSkills5 != null)
          givePassiveSkills.AddRange((IEnumerable<BL.Skill>) investSkills5);
        BL.Skill[] investSkills6 = duelSkillProc.getInvestSkills(attack, false, isUnconditional, 0, turns, turnInvestUnitBk, turnInvestSkillIdsBk, turnInvestFromBk);
        if (investSkills6 != null)
          givePassiveSkills.AddRange((IEnumerable<BL.Skill>) investSkills6);
        BL.Skill[] investSkills7 = biAttackSkills.getInvestSkills(defense, false, isUnconditional, attackNo, turns, turnInvestUnitBk, turnInvestSkillIdsBk, turnInvestFromBk);
        if (investSkills7 != null)
          givePassiveSkills.AddRange((IEnumerable<BL.Skill>) investSkills7);
        BL.Skill[] investSkills8 = duelSkillProc.getInvestSkills(defense, false, isUnconditional, 0, turns, turnInvestUnitBk, turnInvestSkillIdsBk, turnInvestFromBk);
        if (investSkills8 != null)
          givePassiveSkills.AddRange((IEnumerable<BL.Skill>) investSkills8);
        Tuple<BL.ISkillEffectListUnit[], int[], BL.ISkillEffectListUnit[], int[]> allInvestList1 = biAttackSkills.getAllInvestList(true, isUnconditional, attackNo, turns, turnInvestUnitBk, turnInvestSkillIdsBk, turnInvestFromBk);
        if (allInvestList1 != null)
        {
          turnInvestUnit.AddRange((IEnumerable<BL.ISkillEffectListUnit>) allInvestList1.Item1);
          turnInvestSkillIds.AddRange((IEnumerable<int>) allInvestList1.Item2);
          turnInvestFrom.AddRange((IEnumerable<BL.ISkillEffectListUnit>) allInvestList1.Item3);
          turnInvestFromSkillIds.AddRange((IEnumerable<int>) allInvestList1.Item4);
        }
        Tuple<BL.ISkillEffectListUnit[], int[], BL.ISkillEffectListUnit[], int[]> allInvestList2 = duelSkillProc.getAllInvestList(true, isUnconditional, 0, turns, turnInvestUnitBk, turnInvestSkillIdsBk, turnInvestFromBk);
        if (allInvestList2 != null)
        {
          turnInvestUnit.AddRange((IEnumerable<BL.ISkillEffectListUnit>) allInvestList2.Item1);
          turnInvestSkillIds.AddRange((IEnumerable<int>) allInvestList2.Item2);
          turnInvestFrom.AddRange((IEnumerable<BL.ISkillEffectListUnit>) allInvestList2.Item3);
          turnInvestFromSkillIds.AddRange((IEnumerable<int>) allInvestList2.Item4);
        }
        Tuple<BL.ISkillEffectListUnit[], int[], BL.ISkillEffectListUnit[], int[]> allInvestList3 = biAttackSkills.getAllInvestList(false, isUnconditional, attackNo, turns, turnInvestUnitBk, turnInvestSkillIdsBk, turnInvestFromBk);
        if (allInvestList3 != null)
        {
          turnInvestUnit.AddRange((IEnumerable<BL.ISkillEffectListUnit>) allInvestList3.Item1);
          turnInvestSkillIds.AddRange((IEnumerable<int>) allInvestList3.Item2);
          turnInvestFrom.AddRange((IEnumerable<BL.ISkillEffectListUnit>) allInvestList3.Item3);
          turnInvestFromSkillIds.AddRange((IEnumerable<int>) allInvestList3.Item4);
        }
        Tuple<BL.ISkillEffectListUnit[], int[], BL.ISkillEffectListUnit[], int[]> allInvestList4 = duelSkillProc.getAllInvestList(false, isUnconditional, 0, turns, turnInvestUnitBk, turnInvestSkillIdsBk, turnInvestFromBk);
        if (allInvestList4 == null)
          return;
        turnInvestUnit.AddRange((IEnumerable<BL.ISkillEffectListUnit>) allInvestList4.Item1);
        turnInvestSkillIds.AddRange((IEnumerable<int>) allInvestList4.Item2);
        turnInvestFrom.AddRange((IEnumerable<BL.ISkillEffectListUnit>) allInvestList4.Item3);
        turnInvestFromSkillIds.AddRange((IEnumerable<int>) allInvestList4.Item4);
      });
      if (isHit)
        action(false);
      action(true);
    }

    private static void recordInvokeDuelSkillEffectIds(
      BL.ISkillEffectListUnit attack,
      BL.ISkillEffectListUnit defense,
      BL.DuelTurn turn,
      bool isHit,
      List<int> invokeAttackerDuelSkillEffectIds,
      List<int> invokeDefenderDuelSkillEffectIds)
    {
      turn.invokeAttackerDuelSkillEffectIds.AddRange((IEnumerable<int>) invokeAttackerDuelSkillEffectIds);
      foreach (int duelSkillEffectId in invokeAttackerDuelSkillEffectIds)
        attack.skillEffects.AddDuelSkillEffectIdInvokeCount(duelSkillEffectId, 1);
      if (isHit)
      {
        turn.invokeDefenderDuelSkillEffectIds.AddRange((IEnumerable<int>) invokeDefenderDuelSkillEffectIds);
        foreach (int duelSkillEffectId in invokeDefenderDuelSkillEffectIds)
          defense.skillEffects.AddDuelSkillEffectIdInvokeCount(duelSkillEffectId, 1);
      }
      else
      {
        foreach (int duelSkillEffectId in invokeDefenderDuelSkillEffectIds)
        {
          if (MasterData.BattleskillEffect.ContainsKey(duelSkillEffectId))
          {
            BattleskillGenre? genre1 = MasterData.BattleskillEffect[duelSkillEffectId].skill.genre1;
            BattleskillGenre battleskillGenre = BattleskillGenre.defense;
            if (!(genre1.GetValueOrDefault() == battleskillGenre & genre1.HasValue))
            {
              turn.invokeDefenderDuelSkillEffectIds.Add(duelSkillEffectId);
              defense.skillEffects.AddDuelSkillEffectIdInvokeCount(duelSkillEffectId, 1);
            }
          }
        }
      }
    }

    private static BL env
    {
      get
      {
        return BattleFuncs.environment.Get();
      }
    }

    public static BL.ForceID getForceID(BL.Unit unit)
    {
      return BattleFuncs.env.getForceID(unit);
    }

    public static BL.ForceID[] getForceIDArray(BL.ForceID id)
    {
      switch (id)
      {
        case BL.ForceID.player:
          return BattleFuncs.ForceIDArrayPlayer;
        case BL.ForceID.neutral:
          return BattleFuncs.ForceIDArrayNeutral;
        case BL.ForceID.enemy:
          return BattleFuncs.ForceIDArrayEnemy;
        default:
          return BattleFuncs.ForceIDArrayNone;
      }
    }

    public static BL.ForceID[] getForceIDArray(BL.Unit unit, BL _env)
    {
      return BattleFuncs.getForceIDArray(_env.getForceID(unit));
    }

    public static void setAttributePanels(
      IEnumerable<BL.Panel> panels,
      BL.PanelAttribute attribute,
      bool unset)
    {
      if (panels == null)
        return;
      foreach (BL.Panel panel in panels)
      {
        if (unset)
          panel.unsetAttribute(attribute);
        else
          panel.setAttribute(attribute);
      }
    }

    private static BL.Panel panelAdd(
      BL.Panel panel,
      int movement,
      HashSet<BL.Panel> pwl,
      HashSet<BL.Panel> limit)
    {
      if (limit != null && !limit.Contains(panel))
        return panel;
      panel.workMovement = movement;
      if (!pwl.Contains(panel))
        pwl.Add(panel);
      return panel;
    }

    private static bool checkMoveOK(
      int row,
      int column,
      int movement,
      BL.Unit unit,
      HashSet<BL.Panel> pwl,
      HashSet<BL.Panel> limit,
      bool isAI,
      bool isRebirth,
      bool enabledIgnoreMveCost)
    {
      BL.Panel fieldPanel = BattleFuncs.env.getFieldPanel(row, column);
      if (fieldPanel == null || limit != null && !limit.Contains(fieldPanel))
        return false;
      if (pwl.Contains(fieldPanel))
        return movement > fieldPanel.workMovement;
      return (isAI ? (BattleFuncs.env.isMoveOKAI(fieldPanel, unit, isRebirth, enabledIgnoreMveCost, movement) ? 1 : 0) : (BattleFuncs.env.isMoveOK(fieldPanel, unit, isRebirth, enabledIgnoreMveCost, movement) ? 1 : 0)) != 0;
    }

    private static void createMovePanelsFour(
      int row,
      int column,
      int movement,
      BL.Unit unit,
      HashSet<BL.Panel> pwl,
      HashSet<BL.Panel> limit,
      Queue<Tuple<int, int, int>> queue,
      bool isAI,
      bool isRebirth,
      bool enabledIgnoreMoveCost)
    {
      if (movement < 1)
        return;
      if (BattleFuncs.checkMoveOK(row - 1, column, movement, unit, pwl, limit, isAI, isRebirth, enabledIgnoreMoveCost))
        BattleFuncs.createMovePanelsSub(row - 1, column, movement, unit, pwl, limit, queue, enabledIgnoreMoveCost, isAI);
      if (BattleFuncs.checkMoveOK(row + 1, column, movement, unit, pwl, limit, isAI, isRebirth, enabledIgnoreMoveCost))
        BattleFuncs.createMovePanelsSub(row + 1, column, movement, unit, pwl, limit, queue, enabledIgnoreMoveCost, isAI);
      if (BattleFuncs.checkMoveOK(row, column - 1, movement, unit, pwl, limit, isAI, isRebirth, enabledIgnoreMoveCost))
        BattleFuncs.createMovePanelsSub(row, column - 1, movement, unit, pwl, limit, queue, enabledIgnoreMoveCost, isAI);
      if (!BattleFuncs.checkMoveOK(row, column + 1, movement, unit, pwl, limit, isAI, isRebirth, enabledIgnoreMoveCost))
        return;
      BattleFuncs.createMovePanelsSub(row, column + 1, movement, unit, pwl, limit, queue, enabledIgnoreMoveCost, isAI);
    }

    private static void createMovePanelsSub(
      int row,
      int column,
      int movement,
      BL.Unit unit,
      HashSet<BL.Panel> pwl,
      HashSet<BL.Panel> limit,
      Queue<Tuple<int, int, int>> queue,
      bool enabledIgnoreMoveCost,
      bool isAI)
    {
      BL.Panel fieldPanel = BattleFuncs.env.getFieldPanel(row, column);
      BattleFuncs.panelAdd(fieldPanel, movement, pwl, limit);
      if (fieldPanel.zocCheckp(unit, isAI, BattleFuncs.env))
        return;
      queue.Enqueue(Tuple.Create<int, int, int>(row, column, movement - BattleFuncs.getMoveCost(fieldPanel, unit, enabledIgnoreMoveCost)));
    }

    public static HashSet<BL.Panel> createMovePanels(
      int row,
      int column,
      int movement,
      BL.Unit unit,
      HashSet<BL.Panel> limit = null,
      bool isAI = false,
      bool isRebirth = false)
    {
      if (!isRebirth && unit.HasEnabledSkillEffect(BattleskillEffectLogicEnum.slip_thru))
        isRebirth = true;
      HashSet<BL.Panel> pwl = new HashSet<BL.Panel>();
      Queue<Tuple<int, int, int>> queue = new Queue<Tuple<int, int, int>>();
      bool enabledIgnoreMoveCost = unit.HasEnabledSkillEffect(BattleskillEffectLogicEnum.ignore_move_cost);
      BL.Unit unit1 = unit.enableCache();
      try
      {
        BattleFuncs.panelAdd(BattleFuncs.env.getFieldPanel(row, column), movement, pwl, limit);
        BattleFuncs.createMovePanelsFour(row, column, movement, unit1, pwl, limit, queue, isAI, isRebirth, enabledIgnoreMoveCost);
        while (queue.Count != 0)
        {
          Tuple<int, int, int> tuple = queue.Dequeue();
          BattleFuncs.createMovePanelsFour(tuple.Item1, tuple.Item2, tuple.Item3, unit1, pwl, limit, queue, isAI, isRebirth, enabledIgnoreMoveCost);
        }
      }
      finally
      {
        if ((object) unit1 != null)
          unit1.Dispose();
      }
      return pwl;
    }

    public static HashSet<BL.Panel> createMovePanels(BL.UnitPosition up)
    {
      return BattleFuncs.createMovePanels(up.originalRow, up.originalColumn, up.moveCost, up.unit, (HashSet<BL.Panel>) null, false, false);
    }

    public static BL.Panel getPanel(BL.UnitPosition up)
    {
      return BattleFuncs.env.getFieldPanel(up.originalRow, up.originalColumn);
    }

    public static BL.Panel getPanel(int row, int column)
    {
      return BattleFuncs.env.getFieldPanel(row, column);
    }

    private static void addEdge(
      int row,
      int column,
      IEnumerable<BL.Panel> panels,
      List<BattleFuncs.AsterEdge> edges,
      BL.Unit unit,
      int cost)
    {
      int to = 0;
      foreach (BL.Panel panel in panels)
      {
        if (row == panel.row && column == panel.column)
          edges.Add(new BattleFuncs.AsterEdge(to, cost));
        ++to;
      }
    }

    private static BattleFuncs.AsterEdge[] createEdges(
      BL.Panel panel,
      IEnumerable<BL.Panel> panels,
      BL.Unit unit,
      int moveCache,
      bool enabledIgnoreMoveCost)
    {
      List<BattleFuncs.AsterEdge> edges = new List<BattleFuncs.AsterEdge>();
      int moveCost = BattleFuncs.getMoveCost(panel, unit, enabledIgnoreMoveCost);
      int cost = moveCost > moveCache ? 10000 : moveCost;
      BattleFuncs.addEdge(panel.row - 1, panel.column, panels, edges, unit, cost);
      BattleFuncs.addEdge(panel.row + 1, panel.column, panels, edges, unit, cost);
      BattleFuncs.addEdge(panel.row, panel.column - 1, panels, edges, unit, cost);
      BattleFuncs.addEdge(panel.row, panel.column + 1, panels, edges, unit, cost);
      return edges.ToArray();
    }

    public static BattleFuncs.AsterNode[] createNodes(
      IEnumerable<BL.Panel> panels,
      BL.Unit unit,
      BL.Panel start,
      BL.Panel goal,
      out int startIdx,
      out int goalIdx,
      bool enabledIgnoreMoveCost)
    {
      List<BattleFuncs.AsterNode> asterNodeList = new List<BattleFuncs.AsterNode>();
      int no = 0;
      startIdx = goalIdx = -1;
      int moveCost = BattleFuncs.env.getUnitPosition(unit).moveCost;
      foreach (BL.Panel panel in panels)
      {
        asterNodeList.Add(new BattleFuncs.AsterNode(no, panel, BattleFuncs.createEdges(panel, panels, unit, moveCost, enabledIgnoreMoveCost)));
        if (panel == start)
          startIdx = no;
        if (panel == goal)
          goalIdx = no;
        ++no;
      }
      return asterNodeList.ToArray();
    }

    private static void aster(BattleFuncs.AsterNode[] nodes, int goal, int start)
    {
      BattleFuncs.AsterNode node1 = nodes[goal];
      BattleFuncs.AsterNode node2 = nodes[start];
      HashSet<BattleFuncs.AsterNode> asterNodeSet1 = new HashSet<BattleFuncs.AsterNode>();
      HashSet<BattleFuncs.AsterNode> asterNodeSet2 = new HashSet<BattleFuncs.AsterNode>();
      asterNodeSet1.Add(node2);
      node2.cost = BattleFuncs.heuristic(node1, node2);
      while (asterNodeSet1.Count != 0)
      {
        BattleFuncs.AsterNode n = (BattleFuncs.AsterNode) null;
        int num1 = 1000000000;
        foreach (BattleFuncs.AsterNode asterNode in asterNodeSet1)
        {
          if (asterNode.cost < num1)
          {
            n = asterNode;
            num1 = n.cost;
          }
        }
        if (n == node1)
          break;
        asterNodeSet2.Add(n);
        asterNodeSet1.Remove(n);
        for (int index = 0; index < n.edges.Length; ++index)
        {
          BattleFuncs.AsterNode node3 = nodes[n.edges[index].to];
          int num2 = n.cost - BattleFuncs.heuristic(node1, n);
          int num3 = BattleFuncs.heuristic(node1, node3);
          int cost = n.edges[index].cost;
          int num4 = num3;
          int num5 = num2 + num4 + cost;
          if (asterNodeSet1.Contains(node3))
          {
            if (num5 < node3.cost)
            {
              node3.cost = num5;
              node3.from = n.no;
            }
          }
          else if (asterNodeSet2.Contains(node3))
          {
            if (num5 < node3.cost)
            {
              node3.cost = num5;
              node3.from = n.no;
              asterNodeSet1.Add(node3);
              asterNodeSet2.Remove(node3);
            }
          }
          else
          {
            node3.cost = num5;
            node3.from = n.no;
            asterNodeSet1.Add(node3);
          }
        }
      }
    }

    private static int heuristic(BattleFuncs.AsterNode goal, BattleFuncs.AsterNode n)
    {
      int row1 = goal.panel.row;
      int column1 = goal.panel.column;
      int row2 = n.panel.row;
      int column2 = n.panel.column;
      return (row1 > row2 ? row1 - row2 : row2 - row1) + (column1 > column2 ? column1 - column2 : column2 - column1);
    }

    public static void getNodesStartAndGoal(
      BattleFuncs.AsterNode[] nodes,
      BL.Panel start,
      BL.Panel goal,
      out int startIdx,
      out int goalIdx)
    {
      startIdx = goalIdx = -1;
      for (int index = 0; index < nodes.Length; ++index)
      {
        if (nodes[index].panel == goal)
          goalIdx = index;
        if (nodes[index].panel == start)
          startIdx = index;
        if (startIdx != -1 && goalIdx != -1)
          break;
      }
    }

    public static List<BL.Panel> createRouteWithCost(
      BL.Unit unit,
      BattleFuncs.AsterNode[] nodes,
      int startIdx,
      int goalIdx,
      out int cost,
      bool enabledIgnoreMoveCost)
    {
      cost = 0;
      if (startIdx == -1 || goalIdx == -1)
        return new List<BL.Panel>();
      BattleFuncs.aster(nodes, goalIdx, startIdx);
      List<BL.Panel> panelList = new List<BL.Panel>();
      BattleFuncs.AsterNode node;
      for (int index = goalIdx; index != startIdx; index = node.from)
      {
        node = nodes[index];
        cost += BattleFuncs.getMoveCost(node.panel, unit, enabledIgnoreMoveCost);
        panelList.Add(node.panel);
      }
      panelList.Add(nodes[startIdx].panel);
      return panelList;
    }

    private static bool checkTargetp(
      BL.ISkillEffectListUnit su,
      BL.ForceID[] forceIds,
      BL.Unit.TargetAttribute ta,
      bool nonFacility = false)
    {
      return su.checkTargetAttribute(ta) && (!nonFacility || !su.originalUnit.isFacility) && ((IEnumerable<BL.ForceID>) forceIds).Contains<BL.ForceID>(BattleFuncs.env.getForceID(su.originalUnit));
    }

    public static List<BL.UnitPosition> getTargets(
      int r,
      int c,
      int[] range,
      BL.ForceID[] forceIds,
      BL.Unit.TargetAttribute ta,
      bool isAI = false,
      bool originalTarget = false,
      bool isDead = false,
      bool nonFacility = false,
      List<BL.Unit> searchTargets = null)
    {
      List<BL.UnitPosition> unitPositionList = new List<BL.UnitPosition>();
      if (range.Length < 1)
        return unitPositionList;
      Func<bool, BL.ISkillEffectListUnit, int, int, bool> func = (Func<bool, BL.ISkillEffectListUnit, int, int, bool>) ((dead, su, row, col) =>
      {
        if (isDead != dead || !BattleFuncs.checkTargetp(su, forceIds, ta, nonFacility))
          return false;
        int num = BL.fieldDistance(r, c, row, col);
        return num >= range[0] && num <= range[1] && (searchTargets == null || searchTargets.Contains(su.originalUnit));
      });
      if (isAI)
      {
        foreach (BL.AIUnit aiUnit in BattleFuncs.env.aiUnitPositions.value)
        {
          int num1 = originalTarget ? aiUnit.originalRow : aiUnit.row;
          int num2 = originalTarget ? aiUnit.originalColumn : aiUnit.column;
          if (func(aiUnit.isDead, (BL.ISkillEffectListUnit) aiUnit, num1, num2))
            unitPositionList.Add((BL.UnitPosition) aiUnit);
        }
      }
      else
      {
        foreach (BL.UnitPosition unitPosition in BattleFuncs.env.unitPositions.value)
        {
          if (unitPosition.unit.isEnable)
          {
            int num1 = originalTarget ? unitPosition.originalRow : unitPosition.row;
            int num2 = originalTarget ? unitPosition.originalColumn : unitPosition.column;
            if (func(unitPosition.unit.isDead, (BL.ISkillEffectListUnit) unitPosition.unit, num1, num2))
              unitPositionList.Add(unitPosition);
          }
        }
      }
      return unitPositionList;
    }

    public static Tuple<int, int> getUnitCell(BL.Unit unit, bool isAI = false, bool isOriginal = false)
    {
      int num1;
      int num2;
      if (isAI)
      {
        BL.AIUnit aiUnit = BattleFuncs.env.getAIUnit(unit);
        if (isOriginal)
        {
          num1 = aiUnit.unitPosition.originalRow;
          num2 = aiUnit.unitPosition.originalColumn;
        }
        else
        {
          num1 = aiUnit.row;
          num2 = aiUnit.column;
        }
      }
      else
      {
        BL.UnitPosition unitPosition = BattleFuncs.env.getUnitPosition(unit);
        if (isOriginal)
        {
          num1 = unitPosition.originalRow;
          num2 = unitPosition.originalColumn;
        }
        else
        {
          num1 = unitPosition.row;
          num2 = unitPosition.column;
        }
      }
      return new Tuple<int, int>(num1, num2);
    }

    public static List<BL.UnitPosition> getTargets(
      BL.Unit unit,
      int[] range,
      BL.ForceID[] forceIds,
      BL.Unit.TargetAttribute ta,
      bool isAI = false,
      bool isMineOriginal = false,
      bool isTargetsOriginal = false,
      bool nonFacility = false)
    {
      Tuple<int, int> unitCell = BattleFuncs.getUnitCell(unit, isAI, isMineOriginal);
      return BattleFuncs.getTargets(unitCell.Item1, unitCell.Item2, range, forceIds, ta, isAI, false, false, nonFacility, (List<BL.Unit>) null);
    }

    public static List<BL.UnitPosition> getAttackTargets(
      BL.UnitPosition up,
      bool isOriginal = false,
      bool isAI = false,
      bool nonFacility = false)
    {
      if (up == null || up.unit == (BL.Unit) null)
        return new List<BL.UnitPosition>();
      int r = isOriginal ? up.originalRow : up.row;
      int num1 = isOriginal ? up.originalColumn : up.column;
      BL.ISkillEffectListUnit iskillEffectListUnit = BattleFuncs.unitPositionToISkillEffectListUnit(up);
      int c = num1;
      int[] attackRange = iskillEffectListUnit.attackRange;
      BL.ForceID[] targetForce = BattleFuncs.env.getTargetForce(up.unit, iskillEffectListUnit.IsCharm);
      int num2 = isAI ? 1 : 0;
      int num3 = nonFacility ? 1 : 0;
      List<BL.Unit> provokeUnits = BattleFuncs.getProvokeUnits(iskillEffectListUnit);
      return BattleFuncs.getTargets(r, c, attackRange, targetForce, BL.Unit.TargetAttribute.attack, num2 != 0, false, false, num3 != 0, provokeUnits);
    }

    public static List<BL.UnitPosition> getHealTargets(
      BL.UnitPosition up,
      bool isOriginal = false,
      bool isAI = false,
      bool nonFacility = false)
    {
      if (up == null || up.unit == (BL.Unit) null)
        return new List<BL.UnitPosition>();
      int r = isOriginal ? up.originalRow : up.row;
      int num1 = isOriginal ? up.originalColumn : up.column;
      BL.ISkillEffectListUnit iskillEffectListUnit = BattleFuncs.unitPositionToISkillEffectListUnit(up);
      int c = num1;
      int[] healRange = iskillEffectListUnit.healRange;
      BL.ForceID[] forceIdArray = BattleFuncs.getForceIDArray(up.unit, BattleFuncs.env);
      int num2 = isAI ? 1 : 0;
      int num3 = nonFacility ? 1 : 0;
      List<BL.Unit> provokeUnits = BattleFuncs.getProvokeUnits(iskillEffectListUnit);
      return BattleFuncs.getTargets(r, c, healRange, forceIdArray, BL.Unit.TargetAttribute.heal, num2 != 0, false, false, num3 != 0, provokeUnits);
    }

    public static HashSet<BL.Panel> getTargetPanels(
      int row,
      int column,
      int[] range,
      BL.ForceID[] forceIds,
      BL.Unit.TargetAttribute ta,
      bool isAI = false,
      BL.UnitPosition myUP = null,
      bool nonFacility = false,
      bool isDead = false,
      List<BL.Unit> searchTargets = null)
    {
      bool flag1 = myUP != null;
      int r = row;
      int c = column;
      int[] range1 = range;
      BL.ForceID[] forceIds1 = forceIds;
      int num1 = (int) ta;
      int num2 = isAI ? 1 : 0;
      int num3 = flag1 ? 1 : 0;
      bool flag2 = nonFacility;
      int num4 = isDead ? 1 : 0;
      int num5 = flag2 ? 1 : 0;
      List<BL.Unit> searchTargets1 = searchTargets;
      List<BL.UnitPosition> targets = BattleFuncs.getTargets(r, c, range1, forceIds1, (BL.Unit.TargetAttribute) num1, num2 != 0, num3 != 0, num4 != 0, num5 != 0, searchTargets1);
      HashSet<BL.Panel> panelSet = new HashSet<BL.Panel>();
      foreach (BL.UnitPosition unitPosition in targets)
      {
        if (flag1)
        {
          if (unitPosition != myUP)
            panelSet.Add(BattleFuncs.env.getFieldPanel(unitPosition.originalRow, unitPosition.originalColumn));
        }
        else
          panelSet.Add(BattleFuncs.env.getFieldPanel(unitPosition.row, unitPosition.column));
      }
      return panelSet;
    }

    public static HashSet<BL.Panel> getAttackTargetPanels(
      BL.UnitPosition up,
      bool isOriginal = false,
      bool isAI = false)
    {
      if (up.unit == (BL.Unit) null)
        return new HashSet<BL.Panel>();
      if (up.unit.IsDontAction)
        return new HashSet<BL.Panel>();
      BL.ISkillEffectListUnit iskillEffectListUnit = BattleFuncs.unitPositionToISkillEffectListUnit(up);
      int row = isOriginal ? up.originalRow : up.row;
      int column = isOriginal ? up.originalColumn : up.column;
      Tuple<int, int> effectsAddRange = BattleFuncs.env.getFieldPanel(row, column).getEffectsAddRange(up.unit);
      int[] attackRange = iskillEffectListUnit.attackRange;
      int[] range = new int[2]
      {
        attackRange[0] + effectsAddRange.Item1,
        attackRange[1] + effectsAddRange.Item2
      };
      return BattleFuncs.getTargetPanels(row, column, range, BattleFuncs.env.getTargetForce(up.unit, iskillEffectListUnit.IsCharm), BL.Unit.TargetAttribute.attack, isAI, (BL.UnitPosition) null, false, false, BattleFuncs.getProvokeUnits(iskillEffectListUnit));
    }

    public static HashSet<BL.Panel> getHealTargetPanels(
      BL.UnitPosition up,
      bool isOriginal = false,
      bool isAI = false)
    {
      if (up.unit == (BL.Unit) null)
        return new HashSet<BL.Panel>();
      if (up.unit.IsDontAction)
        return new HashSet<BL.Panel>();
      int row = isOriginal ? up.originalRow : up.row;
      int num1 = isOriginal ? up.originalColumn : up.column;
      BL.ISkillEffectListUnit iskillEffectListUnit = BattleFuncs.unitPositionToISkillEffectListUnit(up);
      int column = num1;
      int[] healRange = iskillEffectListUnit.healRange;
      BL.ForceID[] forceIdArray = BattleFuncs.getForceIDArray(up.unit, BattleFuncs.env);
      int num2 = isAI ? 1 : 0;
      BL.UnitPosition myUP = up;
      List<BL.Unit> provokeUnits = BattleFuncs.getProvokeUnits(iskillEffectListUnit);
      return BattleFuncs.getTargetPanels(row, column, healRange, forceIdArray, BL.Unit.TargetAttribute.heal, num2 != 0, myUP, false, false, provokeUnits);
    }

    public static List<BL.Panel> getRangePanels(int row, int column, int[] range)
    {
      if (range.Length == 0)
        return new List<BL.Panel>();
      BL env = BattleFuncs.env;
      List<BL.Panel> panelList = new List<BL.Panel>();
      for (int r2 = -range[1]; r2 <= range[1]; ++r2)
      {
        for (int c2 = -range[1]; c2 <= range[1]; ++c2)
        {
          BL.Panel fieldPanel = env.getFieldPanel(row + r2, column + c2);
          if (fieldPanel != null)
          {
            int num = BL.fieldDistance(0, 0, r2, c2);
            if (num >= range[0] && num <= range[1])
              panelList.Add(fieldPanel);
          }
        }
      }
      return panelList;
    }

    public static HashSet<BL.Panel> allMoveActionRangePanels_(
      BL.UnitPosition up,
      HashSet<BL.Panel> completePanels = null,
      bool isAI = false,
      bool isHeal = false,
      BL.Skill skill = null,
      HashSet<BL.Panel> positionPanels = null)
    {
      HashSet<BL.Panel> panelSet = new HashSet<BL.Panel>();
      positionPanels?.Clear();
      if (completePanels == null)
        completePanels = up.completePanels;
      BL.ForceID[] forceIds = (BL.ForceID[]) null;
      int[] range = new int[2];
      int[] numArray = (int[]) null;
      bool isDead = false;
      bool nonFacility = false;
      BL.Unit.TargetAttribute ta;
      if (skill != null)
      {
        if (skill.targetType == BattleskillTargetType.myself)
        {
          foreach (BL.Panel completePanel in completePanels)
          {
            if (!panelSet.Contains(completePanel))
              panelSet.Add(completePanel);
          }
          if (positionPanels != null)
          {
            foreach (BL.Panel completePanel in completePanels)
            {
              if (!positionPanels.Contains(completePanel))
                positionPanels.Add(completePanel);
            }
          }
          return panelSet;
        }
        ta = skill.targetAttribute;
        range = skill.range;
        if (positionPanels != null)
        {
          nonFacility = skill.nonFacility;
          isDead = skill.isDeadTargetOnly;
          forceIds = skill.getTargetForceIDs(BattleFuncs.env, (BL.ISkillEffectListUnit) up.unit);
        }
      }
      else if (isHeal)
      {
        ta = BL.Unit.TargetAttribute.heal;
        range = BattleFuncs.unitPositionToISkillEffectListUnit(up).healRange;
        if (positionPanels != null)
          forceIds = BattleFuncs.getForceIDArray(up.unit, BattleFuncs.env);
      }
      else
      {
        ta = BL.Unit.TargetAttribute.attack;
        BL.ISkillEffectListUnit iskillEffectListUnit = BattleFuncs.unitPositionToISkillEffectListUnit(up);
        numArray = iskillEffectListUnit.attackRange;
        if (positionPanels != null)
          forceIds = BattleFuncs.env.getTargetForce(up.unit, iskillEffectListUnit.IsCharm);
      }
      using (HashSet<BL.Panel>.Enumerator enumerator = completePanels.GetEnumerator())
      {
label_50:
        while (enumerator.MoveNext())
        {
          BL.Panel current = enumerator.Current;
          if (skill == null && ta == BL.Unit.TargetAttribute.attack)
          {
            Tuple<int, int> effectsAddRange = current.getEffectsAddRange(up.unit);
            range[0] = numArray[0] + effectsAddRange.Item1;
            range[1] = numArray[1] + effectsAddRange.Item2;
          }
          List<BL.Panel> rangePanels = BattleFuncs.getRangePanels(current.row, current.column, range);
          foreach (BL.Panel panel in rangePanels)
            panelSet.Add(panel);
          if (positionPanels != null)
          {
            foreach (BL.Panel panel in rangePanels)
            {
              if (isDead)
              {
                BL.UnitPosition[] unitPositionArray = isAI ? BattleFuncs.env.getFieldUnitsAI(panel, false, isDead) : BattleFuncs.env.getFieldUnits(panel, false, isDead);
                if (unitPositionArray != null)
                {
                  foreach (BL.UnitPosition unitPosition in unitPositionArray)
                  {
                    if (BattleFuncs.checkTargetp(unitPosition is BL.ISkillEffectListUnit ? unitPosition as BL.ISkillEffectListUnit : (BL.ISkillEffectListUnit) unitPosition.unit, forceIds, ta, nonFacility))
                    {
                      positionPanels.Add(current);
                      goto label_50;
                    }
                  }
                }
              }
              else
              {
                BL.UnitPosition unitPosition = isAI ? BattleFuncs.env.getFieldUnitAI(panel.row, panel.column, false) : BattleFuncs.env.getFieldUnit(panel.row, panel.column, false);
                if (unitPosition != null && (BattleFuncs.checkTargetp(isAI ? unitPosition as BL.ISkillEffectListUnit : (BL.ISkillEffectListUnit) unitPosition.unit, forceIds, ta, nonFacility) && (!isHeal || up.id != unitPosition.id)))
                {
                  positionPanels.Add(current);
                  break;
                }
              }
            }
          }
        }
      }
      return panelSet;
    }

    public static HashSet<BL.Panel> createDangerPanels<T>(IEnumerable<T> units) where T : BL.UnitPosition
    {
      HashSet<BL.Panel> panelSet = new HashSet<BL.Panel>();
      foreach (T unit in units)
      {
        foreach (BL.Panel actionRangePanel in unit.allMoveActionRangePanels)
          panelSet.Add(actionRangePanel);
      }
      return panelSet;
    }

    public static HashSet<BL.Panel> createDangerPanels(BL.ForceID byForce)
    {
      return BattleFuncs.createDangerPanels<BL.UnitPosition>((IEnumerable<BL.UnitPosition>) BattleFuncs.env.getActionUnits(byForce).value);
    }

    public static HashSet<BL.Panel> createDangerPanels(BL.ForceID[] byForces)
    {
      HashSet<BL.Panel> panelSet = new HashSet<BL.Panel>();
      foreach (BL.ForceID byForce in byForces)
      {
        foreach (BL.Panel dangerPanel in BattleFuncs.createDangerPanels(byForce))
          panelSet.Add(dangerPanel);
      }
      return panelSet;
    }

    public static HashSet<BL.Panel> moveCompletePanels_(
      HashSet<BL.Panel> panels,
      BL.Unit unit,
      bool isAI = false,
      bool isOriginal = true)
    {
      HashSet<BL.Panel> panelSet = new HashSet<BL.Panel>();
      foreach (BL.Panel panel in panels)
      {
        BL.UnitPosition unitPosition = isAI ? BattleFuncs.env.getFieldUnitAI(panel, isOriginal) : BattleFuncs.env.getFieldUnit(panel, isOriginal);
        if (unitPosition == null || unitPosition.unit == unit || unitPosition.unit.isPutOn)
          panelSet.Add(panel);
      }
      return panelSet;
    }

    public static HashSet<BL.Panel> targetAttackPanels(
      HashSet<BL.Panel> panels,
      BL.Unit unit,
      BL.ForceID[] targetForce,
      bool isAI = false,
      bool nonFacility = false)
    {
      HashSet<BL.Panel> panelSet = new HashSet<BL.Panel>();
      foreach (BL.Panel panel in panels)
      {
        if (BattleFuncs.getTargets(panel.row, panel.column, unit.attackRange, targetForce, BL.Unit.TargetAttribute.attack, isAI, false, false, nonFacility, (List<BL.Unit>) null).Count > 0)
          panelSet.Add(panel);
      }
      return panelSet;
    }

    public static List<BL.UnitPosition> getFourForceUnits(
      int row,
      int column,
      BL.ForceID[] targetForce,
      bool isAI = false)
    {
      List<BL.UnitPosition> unitPositionList = new List<BL.UnitPosition>();
      BL.UnitPosition unitPosition1 = BattleFuncs.env.fieldForceUnit(row + 1, column, targetForce, isAI);
      if (unitPosition1 != null)
        unitPositionList.Add(unitPosition1);
      BL.UnitPosition unitPosition2 = BattleFuncs.env.fieldForceUnit(row - 1, column, targetForce, isAI);
      if (unitPosition2 != null)
        unitPositionList.Add(unitPosition2);
      BL.UnitPosition unitPosition3 = BattleFuncs.env.fieldForceUnit(row, column - 1, targetForce, isAI);
      if (unitPosition3 != null)
        unitPositionList.Add(unitPosition3);
      BL.UnitPosition unitPosition4 = BattleFuncs.env.fieldForceUnit(row, column + 1, targetForce, isAI);
      if (unitPosition4 != null)
        unitPositionList.Add(unitPosition4);
      return unitPositionList;
    }

    public static List<BL.UnitPosition> getForceUnitsWithinRange(
      int row,
      int column,
      int range,
      BL.ForceID[] targetForce,
      bool isAI = false)
    {
      List<BL.UnitPosition> unitPositionList = new List<BL.UnitPosition>();
      BL env = BattleFuncs.env;
      for (int index1 = -range; index1 <= range; ++index1)
      {
        int num1 = row + index1;
        for (int index2 = -range; index2 <= range; ++index2)
        {
          int num2 = column + index2;
          if (BL.fieldDistance(row, column, num1, num2) <= range)
          {
            BL.UnitPosition unitPosition = env.fieldForceUnit(num1, num2, targetForce, isAI);
            if (unitPosition != null)
              unitPositionList.Add(unitPosition);
          }
        }
      }
      return unitPositionList;
    }

    public static List<BL.UnitPosition> getNeighbors(BL.UnitPosition up, bool isAI = false)
    {
      return BattleFuncs.getFourForceUnits(up.row, up.column, BattleFuncs.getForceIDArray(up.unit, BattleFuncs.env), isAI);
    }

    public static int getMoveCost(
      BL.Panel panel,
      UnitMoveType moveType,
      bool enabledIgnoreMoveCost)
    {
      int moveCost = panel.landform.GetIncr(moveType).move_cost;
      return enabledIgnoreMoveCost && moveCost <= 10 ? 1 : moveCost;
    }

    public static int getMoveCost(BL.Panel panel, BL.Unit unit, bool enabledIgnoreMoveCost)
    {
      return BattleFuncs.getMoveCost(panel, unit.job.move_type, enabledIgnoreMoveCost);
    }

    public static IEnumerable<BL.SkillEffect> getAilmentResistEffects(
      int skillId,
      BL.ISkillEffectListUnit target,
      bool isColosseum = false)
    {
      if (MasterData.BattleskillSkill.ContainsKey(skillId))
      {
        BattleskillSkill skill = MasterData.BattleskillSkill[skillId];
        IEnumerable<BL.SkillEffect> source = target.skillEffects.Where(BattleskillEffectLogicEnum.change_invest_skilleffect);
        foreach (BL.SkillEffect skillEffect in BattleFuncs.gearSkillEffectFilter(target.originalUnit, source.Where<BL.SkillEffect>((Func<BL.SkillEffect, bool>) (x =>
        {
          BattleskillEffect effect1 = x.effect;
          int logic = effect1.GetInt(BattleskillEffectLogicArgumentEnum.effect_logic);
          int ailmentGroupId = effect1.HasKey(BattleskillEffectLogicArgumentEnum.ailment_group_id) ? effect1.GetInt(BattleskillEffectLogicArgumentEnum.ailment_group_id) : 0;
          if (!((IEnumerable<BattleskillEffect>) skill.Effects).Any<BattleskillEffect>((Func<BattleskillEffect, bool>) (effect =>
          {
            if (effect.EffectLogic.Enum != (BattleskillEffectLogicEnum) logic)
              return false;
            BattleFuncs.PackedSkillEffect packedSkillEffect = BattleFuncs.PackedSkillEffect.Create(effect);
            return (packedSkillEffect.HasKey(BattleskillEffectLogicArgumentEnum.ailment_group_id) ? packedSkillEffect.GetInt(BattleskillEffectLogicArgumentEnum.ailment_group_id) : 0) == ailmentGroupId;
          })))
            return false;
          BattleFuncs.PackedSkillEffect packedSkillEffect1 = BattleFuncs.PackedSkillEffect.Create(x);
          if (packedSkillEffect1.HasKey(BattleskillEffectLogicArgumentEnum.land_tag1))
          {
            if (isColosseum)
              return false;
            BL.UnitPosition unitPosition = BattleFuncs.iSkillEffectListUnitToUnitPosition(target);
            BattleLandform landform = BattleFuncs.getPanel(unitPosition.row, unitPosition.column)?.landform;
            if (!packedSkillEffect1.CheckLandTag(landform))
              return false;
          }
          return true;
        }))))
          yield return skillEffect;
      }
    }

    public static bool hasPerfectAilmentResist(IEnumerable<BL.SkillEffect> resistEffects)
    {
      return resistEffects.Any<BL.SkillEffect>((Func<BL.SkillEffect, bool>) (x => x.effect.HasKey(BattleskillEffectLogicArgumentEnum.is_perfect_guard) && x.effect.GetInt(BattleskillEffectLogicArgumentEnum.is_perfect_guard) == 1));
    }

    public static bool isAilmentInvest(
      float lottery,
      int skillID,
      BL.ISkillEffectListUnit target,
      XorShift random,
      bool isColosseum)
    {
      if (MasterData.BattleskillSkill.ContainsKey(skillID))
      {
        lottery = Mathf.Min(1f, lottery);
        BL.SkillEffect[] array = BattleFuncs.getAilmentResistEffects(skillID, target, isColosseum).ToArray<BL.SkillEffect>();
        if (BattleFuncs.hasPerfectAilmentResist((IEnumerable<BL.SkillEffect>) array))
          return false;
        float num = ((IEnumerable<BL.SkillEffect>) array).Select<BL.SkillEffect, float>((Func<BL.SkillEffect, float>) (x => x.effect.GetFloat(BattleskillEffectLogicArgumentEnum.percentage_change) + x.effect.GetFloat(BattleskillEffectLogicArgumentEnum.percentage_change_levelup) * (float) x.baseSkillLevel)).Sum();
        lottery *= Mathf.Max(0.0f, 1f - num);
        if ((double) lottery >= (double) random.NextFloat())
          return true;
      }
      return false;
    }

    public static BL.Skill[] ailmentInvest(int skill_id, BL.ISkillEffectListUnit target)
    {
      if (!MasterData.BattleskillSkill.ContainsKey(skill_id))
        return (BL.Skill[]) null;
      BattleskillSkill battleskillSkill = MasterData.BattleskillSkill[skill_id];
      if (((IEnumerable<BattleskillEffect>) battleskillSkill.Effects).All<BattleskillEffect>((Func<BattleskillEffect, bool>) (x => x.EffectLogic.Enum == BattleskillEffectLogicEnum.seal)))
      {
        IEnumerable<int> target_skills = ((IEnumerable<BattleskillEffect>) battleskillSkill.Effects).Select<BattleskillEffect, int>((Func<BattleskillEffect, int>) (x => x.GetInt(BattleskillEffectLogicArgumentEnum.skill_id)));
        if (!target_skills.Contains<int>(0) && !((IEnumerable<Tuple<BattleskillSkill, int, int>>) target.originalUnit.unitAndGearSkills).Any<Tuple<BattleskillSkill, int, int>>((Func<Tuple<BattleskillSkill, int, int>, bool>) (x => target_skills.Contains<int>(x.Item1.ID))))
          return (BL.Skill[]) null;
      }
      return new BL.Skill[1]
      {
        new BL.Skill() { id = skill_id }
      };
    }

    public static BL.ISkillEffectListUnit unitPositionToISkillEffectListUnit(BL.UnitPosition up)
    {
      return !(up is BL.ISkillEffectListUnit) ? (BL.ISkillEffectListUnit) up.unit : up as BL.ISkillEffectListUnit;
    }

    public static BL.UnitPosition iSkillEffectListUnitToUnitPosition(BL.ISkillEffectListUnit unit)
    {
      if (unit is BL.UnitPosition)
        return unit as BL.UnitPosition;
      if (BattleFuncs.env == null)
        return (BL.UnitPosition) null;
      return BattleFuncs.env.unitPositions != null && BattleFuncs.env.unitPositions.value != null ? BattleFuncs.env.getUnitPosition(unit.originalUnit) : (BL.UnitPosition) null;
    }

    public static BL.ISkillEffectListUnit unitToISkillEffectListUnit(BL.Unit unit, bool isAI)
    {
      BL.ISkillEffectListUnit skillEffectListUnit = (BL.ISkillEffectListUnit) null;
      if (isAI)
        skillEffectListUnit = (BL.ISkillEffectListUnit) BattleFuncs.env.getAIUnit(unit);
      return skillEffectListUnit ?? (BL.ISkillEffectListUnit) unit;
    }

    public static bool isSkillsAndEffectsInvalid(
      BL.ISkillEffectListUnit myself,
      BL.ISkillEffectListUnit enemy,
      BL.SkillEffect ef = null)
    {
      if (ef != null && ef.isAttackMethod)
        return false;
      Func<BL.ISkillEffectListUnit, BL.ISkillEffectListUnit, int, bool> func = (Func<BL.ISkillEffectListUnit, BL.ISkillEffectListUnit, int, bool>) ((unit, target, effect_target) => unit.skillEffects.Where(BattleskillEffectLogicEnum.invalid_skills_and_effects, (Func<BL.SkillEffect, bool>) (x =>
      {
        BattleskillEffect effect = x.effect;
        if ((double) effect.GetFloat(BattleskillEffectLogicArgumentEnum.effect_target) != (double) effect_target || BattleFuncs.isSealedSkillEffect(unit, x) || effect.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) != 0 && effect.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) != unit.originalUnit.unit.kind.ID || (effect.GetInt(BattleskillEffectLogicArgumentEnum.target_gear_kind_id) != 0 && effect.GetInt(BattleskillEffectLogicArgumentEnum.target_gear_kind_id) != target.originalUnit.unit.kind.ID || effect.GetInt(BattleskillEffectLogicArgumentEnum.element) != 0 && (CommonElement) effect.GetInt(BattleskillEffectLogicArgumentEnum.element) != unit.originalUnit.playerUnit.GetElement()) || (effect.GetInt(BattleskillEffectLogicArgumentEnum.target_element) != 0 && (CommonElement) effect.GetInt(BattleskillEffectLogicArgumentEnum.target_element) != target.originalUnit.playerUnit.GetElement() || effect.GetInt(BattleskillEffectLogicArgumentEnum.job_id) != 0 && effect.GetInt(BattleskillEffectLogicArgumentEnum.job_id) != unit.originalUnit.job.ID || (effect.GetInt(BattleskillEffectLogicArgumentEnum.target_job_id) != 0 && effect.GetInt(BattleskillEffectLogicArgumentEnum.target_job_id) != target.originalUnit.job.ID || effect.GetInt(BattleskillEffectLogicArgumentEnum.family_id) != 0 && !unit.originalUnit.playerUnit.HasFamily((UnitFamily) effect.GetInt(BattleskillEffectLogicArgumentEnum.family_id)))))
          return false;
        return effect.GetInt(BattleskillEffectLogicArgumentEnum.target_family_id) == 0 || target.originalUnit.playerUnit.HasFamily((UnitFamily) effect.GetInt(BattleskillEffectLogicArgumentEnum.target_family_id));
      })).Any<BL.SkillEffect>());
      return func(myself, enemy, 0) || func(enemy, myself, 1);
    }

    private static bool checkEnabledSkillsAndEffectsInvalid(
      BattleskillEffect x,
      BL.ISkillEffectListUnit unit)
    {
      BattleskillEffect battleskillEffect = x;
      if (battleskillEffect.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) != 0 && battleskillEffect.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) != unit.originalUnit.unit.kind.ID || battleskillEffect.GetInt(BattleskillEffectLogicArgumentEnum.element) != 0 && (CommonElement) battleskillEffect.GetInt(BattleskillEffectLogicArgumentEnum.element) != unit.originalUnit.playerUnit.GetElement() || battleskillEffect.GetInt(BattleskillEffectLogicArgumentEnum.job_id) != 0 && battleskillEffect.GetInt(BattleskillEffectLogicArgumentEnum.job_id) != unit.originalUnit.job.ID)
        return false;
      return battleskillEffect.GetInt(BattleskillEffectLogicArgumentEnum.family_id) == 0 || unit.originalUnit.playerUnit.HasFamily((UnitFamily) battleskillEffect.GetInt(BattleskillEffectLogicArgumentEnum.family_id));
    }

    private static IEnumerable<BL.SkillEffect> GetEnabledLandBlessingBuffDebuff(
      BL.SkillEffectList self,
      BattleskillEffectLogicEnum e,
      BL.ISkillEffectListUnit unit,
      BattleLandform landform)
    {
      return self.Where(e, (Func<BL.SkillEffect, bool>) (effect => !BattleFuncs.isSealedSkillEffect(unit, effect) && effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.effect_target) == 0 && (effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.landform_id) == 0 || effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.landform_id) == landform.baseID) && ((effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) == 0 || effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) == unit.originalUnit.unit.kind.ID) && effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.target_gear_kind_id) == 0) && ((effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.element) == 0 || (CommonElement) effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.element) == unit.originalUnit.playerUnit.GetElement()) && effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.target_element) == 0 && (effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.job_id) == 0 || effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.job_id) == unit.originalUnit.job.ID)) && effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.target_job_id) == 0));
    }

    private static IEnumerable<BL.SkillEffect> GetEnabledLandBlessingBuffDebuff(
      BL.SkillEffectList self,
      BattleskillEffectLogicEnum e,
      BL.ISkillEffectListUnit effectUnit,
      BL.ISkillEffectListUnit targetUnit,
      BL.ISkillEffectListUnit unit,
      BL.ISkillEffectListUnit target,
      BattleLandform landform,
      BattleLandformEffectPhase phase,
      int effectTarget)
    {
      return self.Where(e, (Func<BL.SkillEffect, bool>) (effect =>
      {
        if (BattleFuncs.isSealedSkillEffect(effectUnit, effect) || target != null && effectTarget != effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.effect_target) || phase != (BattleLandformEffectPhase) 0 && effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.phase) != 0 && (BattleLandformEffectPhase) effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.phase) != phase || (effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.landform_id) != 0 && effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.landform_id) != landform.baseID || effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) != 0 && effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) != effectUnit.originalUnit.unit.kind.ID || target != null && effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.target_gear_kind_id) != 0 && effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.target_gear_kind_id) != targetUnit.originalUnit.unit.kind.ID) || (effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.element) != 0 && (CommonElement) effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.element) != effectUnit.originalUnit.playerUnit.GetElement() || target != null && effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.target_element) != 0 && (CommonElement) effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.target_element) != targetUnit.originalUnit.playerUnit.GetElement() || (effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.job_id) != 0 && effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.job_id) != effectUnit.originalUnit.job.ID || target != null && effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.target_job_id) != 0 && effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.target_job_id) != targetUnit.originalUnit.job.ID)) || (target != null && effectUnit == target && BattleFuncs.isSkillsAndEffectsInvalid(target, unit, (BL.SkillEffect) null) || target != null && BattleFuncs.isEffectEnemyRangeAndInvalid(effect, unit, target)))
          return false;
        return target == null || !BattleFuncs.isSkillsAndEffectsInvalid(unit, target, (BL.SkillEffect) null);
      }));
    }

    private static bool CheckEnabledLandBlessingBuffDebuff(
      BattleskillEffect effect,
      BL.ISkillEffectListUnit unit)
    {
      if (effect.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) != 0 && effect.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) != unit.originalUnit.unit.kind.ID || effect.GetInt(BattleskillEffectLogicArgumentEnum.element) != 0 && (CommonElement) effect.GetInt(BattleskillEffectLogicArgumentEnum.element) != unit.originalUnit.playerUnit.GetElement())
        return false;
      return effect.GetInt(BattleskillEffectLogicArgumentEnum.job_id) == 0 || effect.GetInt(BattleskillEffectLogicArgumentEnum.job_id) == unit.originalUnit.job.ID;
    }

    public static void GetLandBlessingSkillAdd(
      List<BattleFuncs.SkillParam> skillParams,
      BL.ISkillEffectListUnit beUnit,
      BattleskillEffectLogicEnum fix_logic,
      BattleLandform landform)
    {
      foreach (BL.SkillEffect effect in BattleFuncs.GetEnabledLandBlessingBuffDebuff(beUnit.skillEffects, fix_logic, beUnit, landform))
        skillParams.Add(BattleFuncs.SkillParam.CreateAdd(beUnit.originalUnit, effect, (float) (effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.value) + effect.baseSkillLevel * effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.skill_ratio)), (object) null, 0));
    }

    public static void GetLandBlessingSkillAdd(
      List<BattleFuncs.SkillParam> skillParams,
      BL.ISkillEffectListUnit beUnit,
      BL.ISkillEffectListUnit beTarget,
      BattleskillEffectLogicEnum fix_logic,
      BattleLandform landform,
      BattleLandformEffectPhase phase)
    {
      foreach (BL.SkillEffect effect in BattleFuncs.GetEnabledLandBlessingBuffDebuff(beUnit.skillEffects, fix_logic, beUnit, beTarget, beUnit, beTarget, landform, phase, 0))
        skillParams.Add(BattleFuncs.SkillParam.CreateAdd(beUnit.originalUnit, effect, (float) (effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.value) + effect.baseSkillLevel * effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.skill_ratio)), (object) null, 0));
      if (beTarget == null || beUnit.originalUnit.isFacility)
        return;
      foreach (BL.SkillEffect effect in BattleFuncs.GetEnabledLandBlessingBuffDebuff(beTarget.skillEffects, fix_logic, beTarget, beUnit, beUnit, beTarget, landform, phase, 1))
        skillParams.Add(BattleFuncs.SkillParam.CreateAdd(beTarget.originalUnit, effect, (float) (effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.value) + effect.baseSkillLevel * effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.skill_ratio)), (object) null, 0));
    }

    public static void GetLandBlessingSkillMul(
      List<BattleFuncs.SkillParam> skillParams,
      BL.ISkillEffectListUnit beUnit,
      BattleskillEffectLogicEnum ratio_logic,
      BattleLandform landform)
    {
      foreach (BL.SkillEffect effect in BattleFuncs.GetEnabledLandBlessingBuffDebuff(beUnit.skillEffects, ratio_logic, beUnit, landform))
        skillParams.Add(BattleFuncs.SkillParam.CreateMul(beUnit.originalUnit, effect, effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.percentage) + (float) effect.baseSkillLevel * effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.skill_ratio), (object) null, 0));
    }

    public static void GetLandBlessingSkillMul(
      List<BattleFuncs.SkillParam> skillParams,
      BL.ISkillEffectListUnit beUnit,
      BL.ISkillEffectListUnit beTarget,
      BattleskillEffectLogicEnum ratio_logic,
      BattleLandform landform,
      BattleLandformEffectPhase phase)
    {
      foreach (BL.SkillEffect effect in BattleFuncs.GetEnabledLandBlessingBuffDebuff(beUnit.skillEffects, ratio_logic, beUnit, beTarget, beUnit, beTarget, landform, phase, 0))
        skillParams.Add(BattleFuncs.SkillParam.CreateMul(beUnit.originalUnit, effect, effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.percentage) + (float) effect.baseSkillLevel * effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.skill_ratio), (object) null, 0));
      if (beTarget == null || beUnit.originalUnit.isFacility)
        return;
      foreach (BL.SkillEffect effect in BattleFuncs.GetEnabledLandBlessingBuffDebuff(beTarget.skillEffects, ratio_logic, beTarget, beUnit, beUnit, beTarget, landform, phase, 1))
        skillParams.Add(BattleFuncs.SkillParam.CreateMul(beUnit.originalUnit, effect, effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.percentage) + (float) effect.baseSkillLevel * effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.skill_ratio), (object) null, 0));
    }

    private static IEnumerable<BL.SkillEffect> GetEnabledDuelSupportBuffDebuff(
      BL.SkillEffectList self,
      BattleskillEffectLogicEnum e,
      BL.ISkillEffectListUnit effectUnit,
      BL.ISkillEffectListUnit targetUnit,
      int effectTarget,
      BL.ISkillEffectListUnit unit,
      BL.ISkillEffectListUnit target)
    {
      return self.Where(e, (Func<BL.SkillEffect, bool>) (effect =>
      {
        BattleskillEffect effect1 = effect.effect;
        return !BattleFuncs.isSealedSkillEffect(effectUnit, effect) && effectTarget == effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.effect_target) && (!effect1.HasKey(BattleskillEffectLogicArgumentEnum.gear_kind_id) || effect1.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) == 0 || effect1.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) == effectUnit.originalUnit.unit.kind.ID) && ((!effect1.HasKey(BattleskillEffectLogicArgumentEnum.target_gear_kind_id) || effect1.GetInt(BattleskillEffectLogicArgumentEnum.target_gear_kind_id) == 0 || effect1.GetInt(BattleskillEffectLogicArgumentEnum.target_gear_kind_id) == targetUnit.originalUnit.unit.kind.ID) && (!effect1.HasKey(BattleskillEffectLogicArgumentEnum.element) || effect1.GetInt(BattleskillEffectLogicArgumentEnum.element) == 0 || (CommonElement) effect1.GetInt(BattleskillEffectLogicArgumentEnum.element) == effectUnit.originalUnit.playerUnit.GetElement())) && ((!effect1.HasKey(BattleskillEffectLogicArgumentEnum.target_element) || effect1.GetInt(BattleskillEffectLogicArgumentEnum.target_element) == 0 || (CommonElement) effect1.GetInt(BattleskillEffectLogicArgumentEnum.target_element) == targetUnit.originalUnit.playerUnit.GetElement()) && ((effectUnit != target || !BattleFuncs.isSkillsAndEffectsInvalid(target, unit, (BL.SkillEffect) null)) && !BattleFuncs.isEffectEnemyRangeAndInvalid(effect, unit, target))) && !BattleFuncs.isSkillsAndEffectsInvalid(unit, target, (BL.SkillEffect) null);
      }));
    }

    private static bool CheckEnabledDuelSupportBuffDebuff(
      BattleskillEffect effect,
      BL.ISkillEffectListUnit effectUnit)
    {
      BattleskillEffect battleskillEffect = effect;
      if (battleskillEffect.HasKey(BattleskillEffectLogicArgumentEnum.gear_kind_id) && battleskillEffect.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) != 0 && battleskillEffect.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) != effectUnit.originalUnit.unit.kind.ID)
        return false;
      return !battleskillEffect.HasKey(BattleskillEffectLogicArgumentEnum.element) || battleskillEffect.GetInt(BattleskillEffectLogicArgumentEnum.element) == 0 || (CommonElement) battleskillEffect.GetInt(BattleskillEffectLogicArgumentEnum.element) == effectUnit.originalUnit.playerUnit.GetElement();
    }

    private static IEnumerable<BL.SkillEffect> GetDuelSupportSkillEffects(
      BL.ISkillEffectListUnit effectUnit,
      BL.ISkillEffectListUnit targetUnit,
      BL.ISkillEffectListUnit beUnit,
      BL.ISkillEffectListUnit beTarget,
      BattleskillEffectLogicEnum logic,
      BL.Unit[] neighborUnits,
      int target)
    {
      foreach (BL.SkillEffect skillEffect in BattleFuncs.GetEnabledDuelSupportBuffDebuff(effectUnit.skillEffects, logic, effectUnit, targetUnit, target, beUnit, beTarget))
      {
        BL.SkillEffect effect = skillEffect;
        BattleFuncs.PackedSkillEffect pse = BattleFuncs.PackedSkillEffect.Create(effect);
        int count = ((IEnumerable<BL.Unit>) neighborUnits).Count<BL.Unit>((Func<BL.Unit, bool>) (x =>
        {
          if (effect.effect.HasKey(BattleskillEffectLogicArgumentEnum.character_id) && effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.character_id) != 0 && effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.character_id) != x.unit.character.ID || effect.effect.HasKey(BattleskillEffectLogicArgumentEnum.same_character_id) && effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.same_character_id) != 0 && effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.same_character_id) != x.unit.same_character_id || (effect.effect.HasKey(BattleskillEffectLogicArgumentEnum.unit_id) && effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.unit_id) != 0 && effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.unit_id) != x.unit.ID || effect.effect.HasKey(BattleskillEffectLogicArgumentEnum.effect_element) && effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.effect_element) != 0 && (CommonElement) effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.effect_element) != x.playerUnit.GetElement()) || (effect.effect.HasKey(BattleskillEffectLogicArgumentEnum.effect_gear_kind_id) && effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.effect_gear_kind_id) != 0 && effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.effect_gear_kind_id) != x.unit.kind.ID || effect.effect.HasKey(BattleskillEffectLogicArgumentEnum.effect_family_id) && effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.effect_family_id) != 0 && !x.playerUnit.HasFamily((UnitFamily) effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.effect_family_id))))
            return false;
          return !pse.HasKey(BattleskillEffectLogicArgumentEnum.effect_skill_group_id) || pse.GetInt(BattleskillEffectLogicArgumentEnum.effect_skill_group_id) == 0 || x.unit.HasSkillGroupId(pse.GetInt(BattleskillEffectLogicArgumentEnum.effect_skill_group_id));
        }));
        if (count != 0)
        {
          if (count > effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.max_target_count) && effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.max_target_count) != 0)
            count = effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.max_target_count);
          for (int i = 0; i < count; ++i)
            yield return effect;
        }
      }
    }

    public static void GetDuelSupportSkillAdd(
      List<BattleFuncs.SkillParam> skillParams,
      BL.ISkillEffectListUnit beUnit,
      BL.ISkillEffectListUnit beTarget,
      BattleskillEffectLogicEnum fix_logic,
      BL.Unit[] neighborUnits)
    {
      foreach (BL.SkillEffect supportSkillEffect in BattleFuncs.GetDuelSupportSkillEffects(beUnit, beTarget, beUnit, beTarget, fix_logic, neighborUnits, 0))
        skillParams.Add(BattleFuncs.SkillParam.CreateAdd(beUnit.originalUnit, supportSkillEffect, (float) (supportSkillEffect.effect.GetInt(BattleskillEffectLogicArgumentEnum.value) + supportSkillEffect.baseSkillLevel * supportSkillEffect.effect.GetInt(BattleskillEffectLogicArgumentEnum.skill_ratio)), (object) null, 0));
      foreach (BL.SkillEffect supportSkillEffect in BattleFuncs.GetDuelSupportSkillEffects(beTarget, beUnit, beUnit, beTarget, fix_logic, neighborUnits, 1))
        skillParams.Add(BattleFuncs.SkillParam.CreateAdd(beTarget.originalUnit, supportSkillEffect, (float) (supportSkillEffect.effect.GetInt(BattleskillEffectLogicArgumentEnum.value) + supportSkillEffect.baseSkillLevel * supportSkillEffect.effect.GetInt(BattleskillEffectLogicArgumentEnum.skill_ratio)), (object) null, 0));
    }

    public static void GetDuelSupportSkillMul(
      List<BattleFuncs.SkillParam> skillParams,
      BL.ISkillEffectListUnit beUnit,
      BL.ISkillEffectListUnit beTarget,
      BattleskillEffectLogicEnum ratio_logic,
      BL.Unit[] neighborUnits)
    {
      foreach (BL.SkillEffect supportSkillEffect in BattleFuncs.GetDuelSupportSkillEffects(beUnit, beTarget, beUnit, beTarget, ratio_logic, neighborUnits, 0))
        skillParams.Add(BattleFuncs.SkillParam.CreateMul(beUnit.originalUnit, supportSkillEffect, supportSkillEffect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.percentage) + (float) supportSkillEffect.baseSkillLevel * supportSkillEffect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.skill_ratio), (object) null, 0));
      foreach (BL.SkillEffect supportSkillEffect in BattleFuncs.GetDuelSupportSkillEffects(beTarget, beUnit, beUnit, beTarget, ratio_logic, neighborUnits, 1))
        skillParams.Add(BattleFuncs.SkillParam.CreateMul(beTarget.originalUnit, supportSkillEffect, supportSkillEffect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.percentage) + (float) supportSkillEffect.baseSkillLevel * supportSkillEffect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.skill_ratio), (object) null, 0));
    }

    public static BattleFuncs.BeforeDuelDuelSupport GetBeforeDuelDuelSupport(
      BL.ISkillEffectListUnit beUnit,
      BL.ISkillEffectListUnit beTarget,
      BL.Unit[] neighborUnits)
    {
      BattleFuncs.BeforeDuelDuelSupport beforeDuelDuelSupport = new BattleFuncs.BeforeDuelDuelSupport();
      beforeDuelDuelSupport.duelSupport = beUnit.originalUnit.playerUnit.GetIntimateDuelSupport(((IEnumerable<BL.Unit>) neighborUnits).Select<BL.Unit, PlayerUnit>((Func<BL.Unit, PlayerUnit>) (x => x.playerUnit)).ToArray<PlayerUnit>());
      List<BattleFuncs.SkillParam> skillParams1 = new List<BattleFuncs.SkillParam>();
      List<BattleFuncs.SkillParam> skillParams2 = new List<BattleFuncs.SkillParam>();
      List<BattleFuncs.SkillParam> skillParams3 = new List<BattleFuncs.SkillParam>();
      List<BattleFuncs.SkillParam> skillParams4 = new List<BattleFuncs.SkillParam>();
      BattleFuncs.GetDuelSupportSkillAdd(skillParams1, beUnit, beTarget, BattleskillEffectLogicEnum.duel_support_fix_hit, neighborUnits);
      BattleFuncs.GetDuelSupportSkillAdd(skillParams1, beUnit, beTarget, BattleskillEffectLogicEnum.duel_support2_fix_hit, neighborUnits);
      BattleFuncs.GetDuelSupportSkillAdd(skillParams2, beUnit, beTarget, BattleskillEffectLogicEnum.duel_support_fix_evasion, neighborUnits);
      BattleFuncs.GetDuelSupportSkillAdd(skillParams2, beUnit, beTarget, BattleskillEffectLogicEnum.duel_support2_fix_evasion, neighborUnits);
      BattleFuncs.GetDuelSupportSkillAdd(skillParams3, beUnit, beTarget, BattleskillEffectLogicEnum.duel_support_fix_critical, neighborUnits);
      BattleFuncs.GetDuelSupportSkillAdd(skillParams3, beUnit, beTarget, BattleskillEffectLogicEnum.duel_support2_fix_critical, neighborUnits);
      BattleFuncs.GetDuelSupportSkillAdd(skillParams4, beUnit, beTarget, BattleskillEffectLogicEnum.duel_support_fix_critical_evasion, neighborUnits);
      BattleFuncs.GetDuelSupportSkillAdd(skillParams4, beUnit, beTarget, BattleskillEffectLogicEnum.duel_support2_fix_critical_evasion, neighborUnits);
      BattleFuncs.GetDuelSupportSkillMul(skillParams1, beUnit, beTarget, BattleskillEffectLogicEnum.duel_support_ratio_hit, neighborUnits);
      BattleFuncs.GetDuelSupportSkillMul(skillParams1, beUnit, beTarget, BattleskillEffectLogicEnum.duel_support2_ratio_hit, neighborUnits);
      BattleFuncs.GetDuelSupportSkillMul(skillParams2, beUnit, beTarget, BattleskillEffectLogicEnum.duel_support_ratio_evasion, neighborUnits);
      BattleFuncs.GetDuelSupportSkillMul(skillParams2, beUnit, beTarget, BattleskillEffectLogicEnum.duel_support2_ratio_evasion, neighborUnits);
      BattleFuncs.GetDuelSupportSkillMul(skillParams3, beUnit, beTarget, BattleskillEffectLogicEnum.duel_support_ratio_critical, neighborUnits);
      BattleFuncs.GetDuelSupportSkillMul(skillParams3, beUnit, beTarget, BattleskillEffectLogicEnum.duel_support2_ratio_critical, neighborUnits);
      BattleFuncs.GetDuelSupportSkillMul(skillParams4, beUnit, beTarget, BattleskillEffectLogicEnum.duel_support_ratio_critical_evasion, neighborUnits);
      BattleFuncs.GetDuelSupportSkillMul(skillParams4, beUnit, beTarget, BattleskillEffectLogicEnum.duel_support2_ratio_critical_evasion, neighborUnits);
      int num1 = BattleFuncs.calcSkillParamAdd(skillParams1);
      int num2 = BattleFuncs.calcSkillParamAdd(skillParams2);
      int num3 = BattleFuncs.calcSkillParamAdd(skillParams3);
      int num4 = BattleFuncs.calcSkillParamAdd(skillParams4);
      float num5 = BattleFuncs.calcSkillParamMul(skillParams1, 1f);
      float num6 = BattleFuncs.calcSkillParamMul(skillParams2, 1f);
      float num7 = BattleFuncs.calcSkillParamMul(skillParams3, 1f);
      float num8 = BattleFuncs.calcSkillParamMul(skillParams4, 1f);
      beforeDuelDuelSupport.hit = Mathf.CeilToInt((float) beforeDuelDuelSupport.duelSupport.hit * num5) + num1;
      beforeDuelDuelSupport.evasion = Mathf.CeilToInt((float) beforeDuelDuelSupport.duelSupport.evasion * num6) + num2;
      beforeDuelDuelSupport.critical = Mathf.CeilToInt((float) beforeDuelDuelSupport.duelSupport.critical * num7) + num3;
      beforeDuelDuelSupport.criticalEvasion = Mathf.CeilToInt((float) beforeDuelDuelSupport.duelSupport.critical_evasion * num8) + num4;
      beforeDuelDuelSupport.hitIncr = beforeDuelDuelSupport.hit - beforeDuelDuelSupport.duelSupport.hit;
      beforeDuelDuelSupport.evasionIncr = beforeDuelDuelSupport.evasion - beforeDuelDuelSupport.duelSupport.evasion;
      beforeDuelDuelSupport.criticalIncr = beforeDuelDuelSupport.critical - beforeDuelDuelSupport.duelSupport.critical;
      beforeDuelDuelSupport.criticalEvasionIncr = beforeDuelDuelSupport.criticalEvasion - beforeDuelDuelSupport.duelSupport.critical_evasion;
      return beforeDuelDuelSupport;
    }

    public static BL.PhaseState getPhaseState()
    {
      return BattleFuncs.env != null ? BattleFuncs.env.phaseState : (BL.PhaseState) null;
    }

    public static List<BattleFuncs.InvalidSpecificSkillLogic> GetInvalidSkillsAndLogics(
      IEnumerable<BattleskillEffect> effects)
    {
      List<BattleFuncs.InvalidSpecificSkillLogic> specificSkillLogicList = new List<BattleFuncs.InvalidSpecificSkillLogic>();
      if (effects == null || !effects.Any<BattleskillEffect>((Func<BattleskillEffect, bool>) (x => x.HasKey(BattleskillEffectLogicArgumentEnum.gda_percentage_invocation) || x.HasKey(BattleskillEffectLogicArgumentEnum.gdd_percentage_invocation))))
        return specificSkillLogicList;
      foreach (BattleskillEffect effect in effects)
      {
        bool flag1 = effect.HasKey(BattleskillEffectLogicArgumentEnum.invalid_skill_id);
        bool flag2 = effect.HasKey(BattleskillEffectLogicArgumentEnum.invalid_logic_id);
        if (flag1 | flag2)
        {
          int skillId = flag1 ? effect.GetInt(BattleskillEffectLogicArgumentEnum.invalid_skill_id) : 0;
          int logicId = flag2 ? effect.GetInt(BattleskillEffectLogicArgumentEnum.invalid_logic_id) : 0;
          specificSkillLogicList.Add(BattleFuncs.InvalidSpecificSkillLogic.Create(skillId, logicId, (object) effect));
        }
      }
      return specificSkillLogicList;
    }

    public static bool checkInvalidEffect(
      BattleskillEffect effect,
      List<BattleFuncs.InvalidSpecificSkillLogic> invalidSkillLogics,
      Func<BattleFuncs.InvalidSpecificSkillLogic, bool> funcExtraCheck = null)
    {
      return invalidSkillLogics != null && invalidSkillLogics.Any<BattleFuncs.InvalidSpecificSkillLogic>((Func<BattleFuncs.InvalidSpecificSkillLogic, bool>) (x =>
      {
        if (x.skillId != 0 && x.skillId != effect.skill.ID || x.logicId != 0 && (BattleskillEffectLogicEnum) x.logicId != effect.EffectLogic.Enum)
          return false;
        return funcExtraCheck == null || funcExtraCheck(x);
      }));
    }

    public static bool checkInvalidEffect(
      BL.SkillEffect effect,
      List<BattleFuncs.InvalidSpecificSkillLogic> invalidSkillLogics,
      Func<BattleFuncs.InvalidSpecificSkillLogic, bool> funcExtraCheck = null)
    {
      return BattleFuncs.checkInvalidEffect(effect.effect, invalidSkillLogics, funcExtraCheck);
    }

    public static bool isInvoke(
      BL.ISkillEffectListUnit invoke,
      BL.ISkillEffectListUnit target,
      Judgement.BeforeDuelUnitParameter invokeParameter,
      Judgement.BeforeDuelUnitParameter targetParameter,
      AttackStatus invokeAS,
      AttackStatus targetAS,
      int skill_level,
      float percentage_invocation,
      XorShift random,
      bool isDuelSkill,
      int invokeHp,
      int targetHp,
      int? colosseumTurn,
      float? invokeRate = null,
      BattleskillEffect effect = null,
      float base_invocation = 0.0f,
      float invocation_skill_ratio = 1f,
      float invocation_luck_ratio = 1f,
      List<BattleFuncs.InvalidSpecificSkillLogic> invalidSkillLogics = null)
    {
      int num1 = invokeParameter.Dexterity;
      if (effect != null)
      {
        switch (effect.EffectLogic.Enum)
        {
          case BattleskillEffectLogicEnum.prayer:
          case BattleskillEffectLogicEnum.passive_prayer:
            num1 = invokeParameter.Luck;
            break;
        }
      }
      int num2 = targetParameter.Luck;
      if (num2 < 0)
        num2 = 0;
      float num3 = (float) ((double) num1 * (double) percentage_invocation + (double) base_invocation + (double) skill_level * (double) invocation_skill_ratio - (double) num2 * (double) invocation_luck_ratio);
      float? nullable1;
      if (invokeRate.HasValue)
      {
        double num4 = (double) num3;
        float? nullable2 = invokeRate;
        float num5 = 100f;
        nullable1 = nullable2.HasValue ? new float?(nullable2.GetValueOrDefault() * num5) : new float?();
        double valueOrDefault = (double) nullable1.GetValueOrDefault();
        if (num4 < valueOrDefault & nullable1.HasValue)
          num3 = invokeRate.Value * 100f;
      }
      float num6 = num3 + BattleFuncs.getWhiteNightRate(invoke, target, invokeAS, targetAS, invalidSkillLogics, isDuelSkill, invokeHp, targetHp, colosseumTurn) * 100f;
      float? nullable3 = new float?();
      float? nullable4 = new float?();
      Func<BL.SkillEffect, BL.ISkillEffectListUnit, BL.ISkillEffectListUnit, AttackStatus, int, bool> checkConditions = (Func<BL.SkillEffect, BL.ISkillEffectListUnit, BL.ISkillEffectListUnit, AttackStatus, int, bool>) ((skillEffect, effInvoker, effTarget, attackStatus, effect_target) =>
      {
        BattleFuncs.PackedSkillEffect packedSkillEffect = BattleFuncs.PackedSkillEffect.Create(skillEffect);
        if (!packedSkillEffect.HasKey(BattleskillEffectLogicArgumentEnum.effect_target) && effect_target != 0 || packedSkillEffect.HasKey(BattleskillEffectLogicArgumentEnum.effect_target) && packedSkillEffect.GetInt(BattleskillEffectLogicArgumentEnum.effect_target) != effect_target)
          return false;
        if (packedSkillEffect.HasKey(BattleskillEffectLogicArgumentEnum.attack_type))
        {
          int num4 = packedSkillEffect.GetInt(BattleskillEffectLogicArgumentEnum.attack_type);
          if (num4 == 1 && (attackStatus == null || attackStatus.isMagic) || num4 == 2 && (attackStatus == null || !attackStatus.isMagic))
            return false;
        }
        BattleLandform landform = !colosseumTurn.HasValue ? BattleFuncs.env.getFieldPanel(BattleFuncs.iSkillEffectListUnitToUnitPosition(effInvoker), false).landform : (BattleLandform) null;
        return (!packedSkillEffect.HasKey(BattleskillEffectLogicArgumentEnum.gear_kind_id) || packedSkillEffect.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) == 0 || packedSkillEffect.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) == effInvoker.originalUnit.unit.kind.ID) && (!packedSkillEffect.HasKey(BattleskillEffectLogicArgumentEnum.target_gear_kind_id) || packedSkillEffect.GetInt(BattleskillEffectLogicArgumentEnum.target_gear_kind_id) == 0 || packedSkillEffect.GetInt(BattleskillEffectLogicArgumentEnum.target_gear_kind_id) == effTarget.originalUnit.unit.kind.ID) && ((!packedSkillEffect.HasKey(BattleskillEffectLogicArgumentEnum.element) || packedSkillEffect.GetInt(BattleskillEffectLogicArgumentEnum.element) == 0 || (CommonElement) packedSkillEffect.GetInt(BattleskillEffectLogicArgumentEnum.element) == effInvoker.originalUnit.playerUnit.GetElement()) && (!packedSkillEffect.HasKey(BattleskillEffectLogicArgumentEnum.target_element) || packedSkillEffect.GetInt(BattleskillEffectLogicArgumentEnum.target_element) == 0 || (CommonElement) packedSkillEffect.GetInt(BattleskillEffectLogicArgumentEnum.target_element) == effTarget.originalUnit.playerUnit.GetElement())) && ((!packedSkillEffect.HasKey(BattleskillEffectLogicArgumentEnum.job_id) || packedSkillEffect.GetInt(BattleskillEffectLogicArgumentEnum.job_id) == 0 || packedSkillEffect.GetInt(BattleskillEffectLogicArgumentEnum.job_id) == effInvoker.originalUnit.job.ID) && (!packedSkillEffect.HasKey(BattleskillEffectLogicArgumentEnum.target_job_id) || packedSkillEffect.GetInt(BattleskillEffectLogicArgumentEnum.target_job_id) == 0 || packedSkillEffect.GetInt(BattleskillEffectLogicArgumentEnum.target_job_id) == effTarget.originalUnit.job.ID)) && packedSkillEffect.CheckLandTag(landform);
      });
      foreach (BL.SkillEffect skillEffect in BattleFuncs.gearSkillEffectFilter(invoke.originalUnit, invoke.enabledSkillEffect(BattleskillEffectLogicEnum.clamp_invoke_duel_skill).Where<BL.SkillEffect>((Func<BL.SkillEffect, bool>) (x => !BattleFuncs.checkInvalidEffect(x, invalidSkillLogics, (Func<BattleFuncs.InvalidSpecificSkillLogic, bool>) null) && checkConditions(x, invoke, target, invokeAS, 0)))).Concat<BL.SkillEffect>(BattleFuncs.gearSkillEffectFilter(target.originalUnit, target.enabledSkillEffect(BattleskillEffectLogicEnum.clamp_invoke_duel_skill).Where<BL.SkillEffect>((Func<BL.SkillEffect, bool>) (x => !BattleFuncs.checkInvalidEffect(x, invalidSkillLogics, (Func<BattleFuncs.InvalidSpecificSkillLogic, bool>) null) && checkConditions(x, target, invoke, targetAS, 1))))))
      {
        float num4 = (float) (((Decimal) skillEffect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.min_percentage) + (Decimal) skillEffect.baseSkillLevel * (Decimal) skillEffect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.min_skill_ratio)) * new Decimal(100));
        if (nullable3.HasValue)
        {
          double num5 = (double) num4;
          nullable1 = nullable3;
          double valueOrDefault = (double) nullable1.GetValueOrDefault();
          if (!(num5 > valueOrDefault & nullable1.HasValue))
            goto label_13;
        }
        nullable3 = new float?(num4);
label_13:
        float num7 = (float) (((Decimal) skillEffect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.max_percentage) + (Decimal) skillEffect.baseSkillLevel * (Decimal) skillEffect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.max_skill_ratio)) * new Decimal(100));
        if (nullable4.HasValue)
        {
          double num5 = (double) num7;
          nullable1 = nullable4;
          double valueOrDefault = (double) nullable1.GetValueOrDefault();
          if (!(num5 < valueOrDefault & nullable1.HasValue))
            continue;
        }
        nullable4 = new float?(num7);
      }
      if (nullable4.HasValue)
      {
        double num4 = (double) num6;
        float? nullable2 = nullable4;
        double valueOrDefault = (double) nullable2.GetValueOrDefault();
        if (num4 > valueOrDefault & nullable2.HasValue)
          num6 = nullable4.Value;
      }
      if (nullable3.HasValue)
      {
        double num4 = (double) num6;
        float? nullable2 = nullable3;
        double valueOrDefault = (double) nullable2.GetValueOrDefault();
        if (num4 < valueOrDefault & nullable2.HasValue)
          num6 = nullable3.Value;
      }
      return (double) num6 >= (double) random.NextFloat() * 100.0;
    }

    private static float getWhiteNightRate(
      BL.ISkillEffectListUnit myself,
      BL.ISkillEffectListUnit enemy,
      AttackStatus myselfAS,
      AttackStatus enemyAS,
      List<BattleFuncs.InvalidSpecificSkillLogic> invalidSkillLogics,
      bool isDuelSkill,
      int myselfHp,
      int enemyHp,
      int? colosseumTurn)
    {
      List<BattleFuncs.SkillParam> skillParams = new List<BattleFuncs.SkillParam>();
      Judgement.NonBattleParameter.FromPlayerUnitCache myselfNbpCache = new Judgement.NonBattleParameter.FromPlayerUnitCache(myself.originalUnit.playerUnit);
      Judgement.NonBattleParameter.FromPlayerUnitCache enemyNbpCache = new Judgement.NonBattleParameter.FromPlayerUnitCache(enemy.originalUnit.playerUnit);
      System.Action<BattleskillEffectLogicEnum, BL.ISkillEffectListUnit, BL.ISkillEffectListUnit, int> action = (System.Action<BattleskillEffectLogicEnum, BL.ISkillEffectListUnit, BL.ISkillEffectListUnit, int>) ((logic, effectUnit, targetUnit, effect_target) =>
      {
        AttackStatus attackStatus;
        int effectHp;
        Judgement.NonBattleParameter.FromPlayerUnitCache effectNbpCache;
        Judgement.NonBattleParameter.FromPlayerUnitCache targetNbpCache;
        int targetHp;
        if (effect_target == 0)
        {
          attackStatus = myselfAS;
          effectNbpCache = myselfNbpCache;
          targetNbpCache = enemyNbpCache;
          effectHp = myselfHp;
          targetHp = enemyHp;
        }
        else
        {
          attackStatus = enemyAS;
          effectNbpCache = enemyNbpCache;
          targetNbpCache = myselfNbpCache;
          effectHp = enemyHp;
          targetHp = myselfHp;
        }
        foreach (BL.SkillEffect effect in effectUnit.skillEffects.Where(logic, (Func<BL.SkillEffect, bool>) (x =>
        {
          if (BattleFuncs.checkInvalidEffect(x, invalidSkillLogics, (Func<BattleFuncs.InvalidSpecificSkillLogic, bool>) null))
            return false;
          BattleFuncs.PackedSkillEffect pse = BattleFuncs.PackedSkillEffect.Create(x);
          if (pse.HasKey(BattleskillEffectLogicArgumentEnum.attack_type))
          {
            int num = pse.GetInt(BattleskillEffectLogicArgumentEnum.attack_type);
            if (num == 1 && (attackStatus == null || attackStatus.isMagic) || num == 2 && (attackStatus == null || !attackStatus.isMagic))
              return false;
          }
          if (pse.HasKey(BattleskillEffectLogicArgumentEnum.min_hp_percentage))
          {
            Decimal num1 = (Decimal) effectHp / (Decimal) effectUnit.originalUnit.parameter.Hp;
            float num2 = pse.GetFloat(BattleskillEffectLogicArgumentEnum.min_hp_percentage);
            float num3 = pse.GetFloat(BattleskillEffectLogicArgumentEnum.max_hp_percentage);
            if ((double) num2 != 0.0 && num1 < (Decimal) num2 || (double) num3 != 0.0 && num1 >= (Decimal) num3)
              return false;
          }
          if ((double) pse.GetFloat(BattleskillEffectLogicArgumentEnum.effect_target) != (double) effect_target || BattleFuncs.isSealedSkillEffect(effectUnit, x))
            return false;
          if (logic == BattleskillEffectLogicEnum.white_night3)
          {
            pse.SetIgnoreHeader(true);
            if (!BattleFuncs.checkInvokeSkillEffect(pse, effectUnit, targetUnit, colosseumTurn, effectNbpCache, targetNbpCache, new int?(effectHp), new int?(targetHp)))
              return false;
            int paramDiffValue = BattleFuncs.GetParamDiffValue(x.effect.GetInt(BattleskillEffectLogicArgumentEnum.param_type), effectNbpCache, effectHp);
            int num = BattleFuncs.GetParamDiffValue(x.effect.GetInt(BattleskillEffectLogicArgumentEnum.target_param_type), targetNbpCache, targetHp) - paramDiffValue;
            if (num < x.effect.GetInt(BattleskillEffectLogicArgumentEnum.min_value) || num > x.effect.GetInt(BattleskillEffectLogicArgumentEnum.max_value))
              return false;
          }
          else if ((pse.HasKey(BattleskillEffectLogicArgumentEnum.gear_kind_id) && pse.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) != 0 && pse.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) != effectUnit.originalUnit.unit.kind.ID || pse.HasKey(BattleskillEffectLogicArgumentEnum.target_gear_kind_id) && pse.GetInt(BattleskillEffectLogicArgumentEnum.target_gear_kind_id) != 0 && pse.GetInt(BattleskillEffectLogicArgumentEnum.target_gear_kind_id) != targetUnit.originalUnit.unit.kind.ID || (pse.HasKey(BattleskillEffectLogicArgumentEnum.element) && pse.GetInt(BattleskillEffectLogicArgumentEnum.element) != 0 && (CommonElement) pse.GetInt(BattleskillEffectLogicArgumentEnum.element) != effectUnit.originalUnit.playerUnit.GetElement() || pse.HasKey(BattleskillEffectLogicArgumentEnum.target_element) && pse.GetInt(BattleskillEffectLogicArgumentEnum.target_element) != 0 && (CommonElement) pse.GetInt(BattleskillEffectLogicArgumentEnum.target_element) != targetUnit.originalUnit.playerUnit.GetElement()) || (pse.HasKey(BattleskillEffectLogicArgumentEnum.job_id) && pse.GetInt(BattleskillEffectLogicArgumentEnum.job_id) != 0 && pse.GetInt(BattleskillEffectLogicArgumentEnum.job_id) != effectUnit.originalUnit.job.ID || pse.HasKey(BattleskillEffectLogicArgumentEnum.target_job_id) && pse.GetInt(BattleskillEffectLogicArgumentEnum.target_job_id) != 0 && pse.GetInt(BattleskillEffectLogicArgumentEnum.target_job_id) != targetUnit.originalUnit.job.ID || pse.HasKey(BattleskillEffectLogicArgumentEnum.family_id) && pse.GetInt(BattleskillEffectLogicArgumentEnum.family_id) != 0 && !effectUnit.originalUnit.playerUnit.HasFamily((UnitFamily) pse.GetInt(BattleskillEffectLogicArgumentEnum.family_id))) ? 0 : (!pse.HasKey(BattleskillEffectLogicArgumentEnum.target_family_id) || pse.GetInt(BattleskillEffectLogicArgumentEnum.target_family_id) == 0 ? 1 : (targetUnit.originalUnit.playerUnit.HasFamily((UnitFamily) pse.GetInt(BattleskillEffectLogicArgumentEnum.target_family_id)) ? 1 : 0))) == 0)
            return false;
          return (effectUnit != enemy || !BattleFuncs.isSkillsAndEffectsInvalid(enemy, myself, (BL.SkillEffect) null)) && !BattleFuncs.isEffectEnemyRangeAndInvalid(x, myself, enemy) && !BattleFuncs.isSkillsAndEffectsInvalid(myself, enemy, (BL.SkillEffect) null);
        })))
          skillParams.Add(BattleFuncs.SkillParam.CreateAdd(effectUnit.originalUnit, effect, effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.percentage) * (float) effect.baseSkillLevel, (object) null, 0));
      });
      action(BattleskillEffectLogicEnum.white_night, myself, enemy, 0);
      action(BattleskillEffectLogicEnum.white_night, enemy, myself, 1);
      if (isDuelSkill)
      {
        action(BattleskillEffectLogicEnum.white_night2, myself, enemy, 0);
        action(BattleskillEffectLogicEnum.white_night2, enemy, myself, 1);
        action(BattleskillEffectLogicEnum.white_night3, myself, enemy, 0);
        action(BattleskillEffectLogicEnum.white_night3, enemy, myself, 1);
      }
      return BattleFuncs.calcSkillParamAddSingle(skillParams);
    }

    private static bool checkEnabledWhiteNight(
      BattleskillEffect x,
      BL.ISkillEffectListUnit effectUnit)
    {
      BattleskillEffect battleskillEffect = x;
      if (battleskillEffect.HasKey(BattleskillEffectLogicArgumentEnum.gear_kind_id) && battleskillEffect.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) != 0 && battleskillEffect.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) != effectUnit.originalUnit.unit.kind.ID || battleskillEffect.HasKey(BattleskillEffectLogicArgumentEnum.element) && battleskillEffect.GetInt(BattleskillEffectLogicArgumentEnum.element) != 0 && (CommonElement) battleskillEffect.GetInt(BattleskillEffectLogicArgumentEnum.element) != effectUnit.originalUnit.playerUnit.GetElement() || battleskillEffect.HasKey(BattleskillEffectLogicArgumentEnum.job_id) && battleskillEffect.GetInt(BattleskillEffectLogicArgumentEnum.job_id) != 0 && battleskillEffect.GetInt(BattleskillEffectLogicArgumentEnum.job_id) != effectUnit.originalUnit.job.ID)
        return false;
      return !battleskillEffect.HasKey(BattleskillEffectLogicArgumentEnum.family_id) || battleskillEffect.GetInt(BattleskillEffectLogicArgumentEnum.family_id) == 0 || effectUnit.originalUnit.playerUnit.HasFamily((UnitFamily) battleskillEffect.GetInt(BattleskillEffectLogicArgumentEnum.family_id));
    }

    private static bool checkEnabledWhiteNight3(
      BattleskillEffect x,
      BL.ISkillEffectListUnit effectUnit)
    {
      return BattleFuncs.checkInvokeSkillEffectSelf(BattleFuncs.PackedSkillEffect.Create(x), effectUnit, (Judgement.NonBattleParameter.FromPlayerUnitCache) null, new int?(), true);
    }

    public static IEnumerable<Tuple<BattleskillSkill, int, int>> getUnitAndGearSkills(
      BL.Unit unit)
    {
      PlayerAwakeSkill[] playerAwakeSkillArray;
      if (unit.playerUnit.equippedExtraSkill == null)
        playerAwakeSkillArray = new PlayerAwakeSkill[0];
      else
        playerAwakeSkillArray = new PlayerAwakeSkill[1]
        {
          unit.playerUnit.equippedExtraSkill
        };
      IEnumerable<Tuple<BattleskillSkill, int, int>> tuples = ((IEnumerable<PlayerAwakeSkill>) playerAwakeSkillArray).Select<PlayerAwakeSkill, Tuple<BattleskillSkill, int, int>>((Func<PlayerAwakeSkill, Tuple<BattleskillSkill, int, int>>) (x => new Tuple<BattleskillSkill, int, int>(x.masterData, x.level, 0))).Concat<Tuple<BattleskillSkill, int, int>>(((IEnumerable<PlayerUnitSkills>) unit.playerUnit.skills).Select<PlayerUnitSkills, Tuple<BattleskillSkill, int, int>>((Func<PlayerUnitSkills, Tuple<BattleskillSkill, int, int>>) (x => new Tuple<BattleskillSkill, int, int>(x.skill, x.level, 0))));
      if (unit.playerUnit.equippedGear != (PlayerItem) null)
        tuples = tuples.Concat<Tuple<BattleskillSkill, int, int>>(((IEnumerable<GearGearSkill>) unit.playerUnit.equippedGear.skills).Select<GearGearSkill, Tuple<BattleskillSkill, int, int>>((Func<GearGearSkill, Tuple<BattleskillSkill, int, int>>) (x => new Tuple<BattleskillSkill, int, int>(x.skill, x.skill_level, 1))));
      if (unit.playerUnit.equippedGear2 != (PlayerItem) null)
        tuples = tuples.Concat<Tuple<BattleskillSkill, int, int>>(((IEnumerable<GearGearSkill>) unit.playerUnit.equippedGear2.skills).Select<GearGearSkill, Tuple<BattleskillSkill, int, int>>((Func<GearGearSkill, Tuple<BattleskillSkill, int, int>>) (x => new Tuple<BattleskillSkill, int, int>(x.skill, x.skill_level, 2))));
      if (unit.playerUnit.equippedReisou != (PlayerItem) null)
        tuples = tuples.Concat<Tuple<BattleskillSkill, int, int>>(((IEnumerable<GearGearSkill>) unit.playerUnit.equippedReisou.skills).Select<GearGearSkill, Tuple<BattleskillSkill, int, int>>((Func<GearGearSkill, Tuple<BattleskillSkill, int, int>>) (x => new Tuple<BattleskillSkill, int, int>(x.skill, x.skill_level, 1))));
      if (unit.playerUnit.equippedReisou2 != (PlayerItem) null)
        tuples = tuples.Concat<Tuple<BattleskillSkill, int, int>>(((IEnumerable<GearGearSkill>) unit.playerUnit.equippedReisou2.skills).Select<GearGearSkill, Tuple<BattleskillSkill, int, int>>((Func<GearGearSkill, Tuple<BattleskillSkill, int, int>>) (x => new Tuple<BattleskillSkill, int, int>(x.skill, x.skill_level, 1))));
      if (unit.playerUnit.equippedOverkillersSkills != null)
        tuples = tuples.Concat<Tuple<BattleskillSkill, int, int>>(((IEnumerable<PlayerUnitSkills>) unit.playerUnit.equippedOverkillersSkills).Select<PlayerUnitSkills, Tuple<BattleskillSkill, int, int>>((Func<PlayerUnitSkills, Tuple<BattleskillSkill, int, int>>) (x => new Tuple<BattleskillSkill, int, int>(x.skill, x.level, 0))));
      return (IEnumerable<Tuple<BattleskillSkill, int, int>>) tuples.OrderByDescending<Tuple<BattleskillSkill, int, int>, int>((Func<Tuple<BattleskillSkill, int, int>, int>) (x => x.Item1.weight)).ThenBy<Tuple<BattleskillSkill, int, int>, int>((Func<Tuple<BattleskillSkill, int, int>, int>) (x => x.Item1.ID)).ThenByDescending<Tuple<BattleskillSkill, int, int>, int>((Func<Tuple<BattleskillSkill, int, int>, int>) (x => x.Item2));
    }

    public static bool isCriticalGuardEnable(
      BL.ISkillEffectListUnit myself,
      BL.ISkillEffectListUnit enemy)
    {
      return myself.enabledSkillEffect(BattleskillEffectLogicEnum.critical_guard).Where<BL.SkillEffect>((Func<BL.SkillEffect, bool>) (x =>
      {
        BattleskillEffect effect = x.effect;
        if (effect.HasKey(BattleskillEffectLogicArgumentEnum.gear_kind_id) && effect.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) != 0 && effect.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) != myself.originalUnit.unit.kind.ID || effect.HasKey(BattleskillEffectLogicArgumentEnum.target_gear_kind_id) && effect.GetInt(BattleskillEffectLogicArgumentEnum.target_gear_kind_id) != 0 && effect.GetInt(BattleskillEffectLogicArgumentEnum.target_gear_kind_id) != enemy.originalUnit.unit.kind.ID || (effect.HasKey(BattleskillEffectLogicArgumentEnum.element) && effect.GetInt(BattleskillEffectLogicArgumentEnum.element) != 0 && (CommonElement) effect.GetInt(BattleskillEffectLogicArgumentEnum.element) != myself.originalUnit.playerUnit.GetElement() || effect.HasKey(BattleskillEffectLogicArgumentEnum.target_element) && effect.GetInt(BattleskillEffectLogicArgumentEnum.target_element) != 0 && (CommonElement) effect.GetInt(BattleskillEffectLogicArgumentEnum.target_element) != enemy.originalUnit.playerUnit.GetElement()) || (effect.HasKey(BattleskillEffectLogicArgumentEnum.job_id) && effect.GetInt(BattleskillEffectLogicArgumentEnum.job_id) != 0 && effect.GetInt(BattleskillEffectLogicArgumentEnum.job_id) != myself.originalUnit.job.ID || effect.HasKey(BattleskillEffectLogicArgumentEnum.target_job_id) && effect.GetInt(BattleskillEffectLogicArgumentEnum.target_job_id) != 0 && effect.GetInt(BattleskillEffectLogicArgumentEnum.target_job_id) != enemy.originalUnit.job.ID || effect.HasKey(BattleskillEffectLogicArgumentEnum.family_id) && effect.GetInt(BattleskillEffectLogicArgumentEnum.family_id) != 0 && !myself.originalUnit.playerUnit.HasFamily((UnitFamily) effect.GetInt(BattleskillEffectLogicArgumentEnum.family_id))))
          return false;
        return !effect.HasKey(BattleskillEffectLogicArgumentEnum.target_family_id) || effect.GetInt(BattleskillEffectLogicArgumentEnum.target_family_id) == 0 || enemy.originalUnit.playerUnit.HasFamily((UnitFamily) effect.GetInt(BattleskillEffectLogicArgumentEnum.target_family_id));
      })).Any<BL.SkillEffect>() && !BattleFuncs.isSkillsAndEffectsInvalid(myself, enemy, (BL.SkillEffect) null);
    }

    private static bool checkEnabledCriticalGuard(
      BattleskillEffect x,
      BL.ISkillEffectListUnit myself)
    {
      BattleskillEffect battleskillEffect = x;
      if (battleskillEffect.HasKey(BattleskillEffectLogicArgumentEnum.gear_kind_id) && battleskillEffect.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) != 0 && battleskillEffect.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) != myself.originalUnit.unit.kind.ID || battleskillEffect.HasKey(BattleskillEffectLogicArgumentEnum.element) && battleskillEffect.GetInt(BattleskillEffectLogicArgumentEnum.element) != 0 && (CommonElement) battleskillEffect.GetInt(BattleskillEffectLogicArgumentEnum.element) != myself.originalUnit.playerUnit.GetElement() || battleskillEffect.HasKey(BattleskillEffectLogicArgumentEnum.job_id) && battleskillEffect.GetInt(BattleskillEffectLogicArgumentEnum.job_id) != 0 && battleskillEffect.GetInt(BattleskillEffectLogicArgumentEnum.job_id) != myself.originalUnit.job.ID)
        return false;
      return !battleskillEffect.HasKey(BattleskillEffectLogicArgumentEnum.family_id) || battleskillEffect.GetInt(BattleskillEffectLogicArgumentEnum.family_id) == 0 || myself.originalUnit.playerUnit.HasFamily((UnitFamily) battleskillEffect.GetInt(BattleskillEffectLogicArgumentEnum.family_id));
    }

    public static bool isEffectEnemyRangeAndInvalid(
      BL.SkillEffect effect,
      BL.ISkillEffectListUnit myself,
      BL.ISkillEffectListUnit enemy)
    {
      return !effect.isAttackMethod && effect.unit != (BL.Unit) null && (effect.unit.index == enemy.originalUnit.index && effect.unit.isPlayerForce == enemy.originalUnit.isPlayerForce) && (effect.effect.skill.skill_type == BattleskillSkillType.leader || effect.effect.skill.skill_type == BattleskillSkillType.passive && effect.effect.skill.range_effect_passive_skill) && BattleFuncs.isSkillsAndEffectsInvalid(enemy, myself, (BL.SkillEffect) null);
    }

    public static void consumeSkillEffects(
      BL.DuelTurn[] turns,
      BL.ISkillEffectListUnit atk,
      BL.ISkillEffectListUnit def)
    {
      System.Action<BL.SkillEffectList, BL.UseSkillEffect> action = (System.Action<BL.SkillEffectList, BL.UseSkillEffect>) ((skillEffects, useEffect) =>
      {
        IEnumerable<BL.SkillEffect> source = skillEffects.All().Where<BL.SkillEffect>((Func<BL.SkillEffect, bool>) (x =>
        {
          if (x.effectId == useEffect.effectEffectId && x.baseSkillLevel == useEffect.effectBaseSkillLevel)
          {
            if (x.turnRemain.HasValue || useEffect.effectTurnRemain != -1)
            {
              int? turnRemain = x.turnRemain;
              int effectTurnRemain = useEffect.effectTurnRemain;
              if (!(turnRemain.GetValueOrDefault() == effectTurnRemain & turnRemain.HasValue))
                goto label_9;
            }
            if (x.unit == (BL.Unit) null && useEffect.effectUnitIndex == -1 || x.unit != (BL.Unit) null && useEffect.effectUnitIndex != -1 && (x.unit.index == useEffect.effectUnitIndex && x.unit.isPlayerForce == useEffect.effectUnitIsPlayerControl))
            {
              if (x.work == null && (double) useEffect.effectWork == 0.0)
                return true;
              return x.work != null && (double) useEffect.effectWork != 0.0 && (double) x.work[0] == (double) useEffect.effectWork;
            }
          }
label_9:
          return false;
        }));
        BL.SkillEffect skillEffect1 = useEffect.type != BL.UseSkillEffect.Type.Decrement ? source.FirstOrDefault<BL.SkillEffect>() : source.FirstOrDefault<BL.SkillEffect>((Func<BL.SkillEffect, bool>) (x =>
        {
          int? useRemain = x.useRemain;
          int num = 1;
          return useRemain.GetValueOrDefault() >= num & useRemain.HasValue;
        }));
        if (skillEffect1 == null)
          return;
        switch (useEffect.type)
        {
          case BL.UseSkillEffect.Type.Remove:
            skillEffect1.useRemain = new int?(0);
            if ((double) useEffect.work == 0.0)
              break;
            skillEffect1.work = new float[1]
            {
              useEffect.work
            };
            break;
          case BL.UseSkillEffect.Type.SetWork:
            skillEffect1.work = new float[1]
            {
              useEffect.work
            };
            break;
          case BL.UseSkillEffect.Type.Decrement:
            if (!skillEffect1.useRemain.HasValue)
              break;
            int? useRemain1 = skillEffect1.useRemain;
            int num1 = 1;
            if (!(useRemain1.GetValueOrDefault() >= num1 & useRemain1.HasValue))
              break;
            BL.SkillEffect skillEffect2 = skillEffect1;
            useRemain1 = skillEffect2.useRemain;
            int? nullable = useRemain1;
            skillEffect2.useRemain = nullable.HasValue ? new int?(nullable.GetValueOrDefault() - 1) : new int?();
            break;
        }
      });
      foreach (BL.DuelTurn turn in turns)
      {
        List<int> duelSkillEffectIds1;
        List<int> duelSkillEffectIds2;
        List<BL.UseSkillEffect> useSkillEffectList1;
        List<BL.UseSkillEffect> useSkillEffectList2;
        if (turn.isAtackker)
        {
          duelSkillEffectIds1 = turn.invokeAttackerDuelSkillEffectIds;
          duelSkillEffectIds2 = turn.invokeDefenderDuelSkillEffectIds;
          useSkillEffectList1 = turn.attackerUseSkillEffects;
          useSkillEffectList2 = turn.defenderUseSkillEffects;
        }
        else
        {
          duelSkillEffectIds1 = turn.invokeDefenderDuelSkillEffectIds;
          duelSkillEffectIds2 = turn.invokeAttackerDuelSkillEffectIds;
          useSkillEffectList1 = turn.defenderUseSkillEffects;
          useSkillEffectList2 = turn.attackerUseSkillEffects;
        }
        if (duelSkillEffectIds1 != null)
        {
          foreach (int skillEffectId in duelSkillEffectIds1)
            atk.skillEffects.AddDuelSkillEffectIdInvokeCount(skillEffectId, 1);
        }
        if (duelSkillEffectIds2 != null)
        {
          foreach (int skillEffectId in duelSkillEffectIds2)
            def.skillEffects.AddDuelSkillEffectIdInvokeCount(skillEffectId, 1);
        }
        if (useSkillEffectList1 != null)
        {
          foreach (BL.UseSkillEffect useSkillEffect in useSkillEffectList1)
            action(atk.skillEffects, useSkillEffect);
        }
        if (useSkillEffectList2 != null)
        {
          foreach (BL.UseSkillEffect useSkillEffect in useSkillEffectList2)
            action(def.skillEffects, useSkillEffect);
        }
      }
    }

    public static BL getEnv()
    {
      return BattleFuncs.env;
    }

    public static BL.ForceID[] getTargetForce(BL.Unit unit, bool isCharm)
    {
      return BattleFuncs.env.getTargetForce(unit, isCharm);
    }

    public static BL.ClassValue<List<BL.Unit>> forceUnits(BL.ForceID forceId)
    {
      return BattleFuncs.env.forceUnits(forceId);
    }

    public static BL.UnitPosition getUnitPosition(BL.ISkillEffectListUnit unit)
    {
      if (BattleFuncs.env == null)
        return (BL.UnitPosition) null;
      return BattleFuncs.env.unitPositions != null && BattleFuncs.env.unitPositions.value != null ? BattleFuncs.env.getUnitPosition(unit.originalUnit) : (BL.UnitPosition) null;
    }

    public static BL.UnitPosition[] getFieldUnitsAI(int row, int column)
    {
      List<BL.UnitPosition> unitPositionList = new List<BL.UnitPosition>();
      foreach (BL.AIUnit aiUnit in BattleFuncs.env.aiUnitPositions.value)
      {
        if (aiUnit.unit.isEnable && !aiUnit.unit.isDead && (aiUnit.row == row && aiUnit.column == column) && !aiUnit.isDead)
          unitPositionList.Add((BL.UnitPosition) aiUnit);
      }
      return unitPositionList.Count <= 0 ? (BL.UnitPosition[]) null : unitPositionList.ToArray();
    }

    public static IEnumerable<BL.ISkillEffectListUnit> getInvestUnit(
      BL.ISkillEffectListUnit myself,
      BL.ISkillEffectListUnit enemy,
      int skillId,
      int rangeType,
      bool isAI,
      bool isColosseum)
    {
      BattleskillSkill battleskillSkill = MasterData.BattleskillSkill[skillId];
      if (isColosseum)
      {
        List<BL.ISkillEffectListUnit> skillEffectListUnitList = new List<BL.ISkillEffectListUnit>();
        switch (rangeType)
        {
          case 0:
            switch (battleskillSkill.target_type)
            {
              case BattleskillTargetType.myself:
              case BattleskillTargetType.player_single:
                skillEffectListUnitList.Add(myself);
                break;
              case BattleskillTargetType.player_range:
                if (battleskillSkill.min_range <= 0)
                {
                  skillEffectListUnitList.Add(myself);
                  break;
                }
                break;
              case BattleskillTargetType.enemy_single:
                skillEffectListUnitList.Add(enemy);
                break;
              case BattleskillTargetType.enemy_range:
                if (battleskillSkill.min_range <= 1 && battleskillSkill.max_range >= 1)
                {
                  skillEffectListUnitList.Add(enemy);
                  break;
                }
                break;
              case BattleskillTargetType.complex_single:
                if (((IEnumerable<BattleskillEffect>) battleskillSkill.Effects).Any<BattleskillEffect>((Func<BattleskillEffect, bool>) (x => !x.is_targer_enemy)))
                  skillEffectListUnitList.Add(myself);
                if (((IEnumerable<BattleskillEffect>) battleskillSkill.Effects).Any<BattleskillEffect>((Func<BattleskillEffect, bool>) (x => x.is_targer_enemy)))
                {
                  skillEffectListUnitList.Add(enemy);
                  break;
                }
                break;
              case BattleskillTargetType.complex_range:
                if (((IEnumerable<BattleskillEffect>) battleskillSkill.Effects).Any<BattleskillEffect>((Func<BattleskillEffect, bool>) (x => !x.is_targer_enemy)) && battleskillSkill.min_range <= 0)
                  skillEffectListUnitList.Add(myself);
                if (((IEnumerable<BattleskillEffect>) battleskillSkill.Effects).Any<BattleskillEffect>((Func<BattleskillEffect, bool>) (x => x.is_targer_enemy)) && battleskillSkill.min_range <= 1 && battleskillSkill.max_range >= 1)
                {
                  skillEffectListUnitList.Add(enemy);
                  break;
                }
                break;
            }
            break;
          case 1:
            switch (battleskillSkill.target_type)
            {
              case BattleskillTargetType.myself:
              case BattleskillTargetType.player_range:
              case BattleskillTargetType.player_single:
                if (battleskillSkill.min_range <= 0)
                {
                  skillEffectListUnitList.Add(myself);
                  break;
                }
                break;
              case BattleskillTargetType.enemy_single:
              case BattleskillTargetType.enemy_range:
                if (battleskillSkill.min_range <= 1 && battleskillSkill.max_range >= 1)
                {
                  skillEffectListUnitList.Add(enemy);
                  break;
                }
                break;
              case BattleskillTargetType.complex_single:
              case BattleskillTargetType.complex_range:
                if (((IEnumerable<BattleskillEffect>) battleskillSkill.Effects).Any<BattleskillEffect>((Func<BattleskillEffect, bool>) (x => !x.is_targer_enemy)) && battleskillSkill.min_range <= 0)
                  skillEffectListUnitList.Add(myself);
                if (((IEnumerable<BattleskillEffect>) battleskillSkill.Effects).Any<BattleskillEffect>((Func<BattleskillEffect, bool>) (x => x.is_targer_enemy)) && battleskillSkill.min_range <= 1 && battleskillSkill.max_range >= 1)
                {
                  skillEffectListUnitList.Add(enemy);
                  break;
                }
                break;
            }
            break;
          case 2:
            switch (battleskillSkill.target_type)
            {
              case BattleskillTargetType.myself:
              case BattleskillTargetType.player_range:
              case BattleskillTargetType.player_single:
                if (battleskillSkill.min_range <= 1 && battleskillSkill.max_range >= 1)
                {
                  skillEffectListUnitList.Add(myself);
                  break;
                }
                break;
              case BattleskillTargetType.enemy_single:
              case BattleskillTargetType.enemy_range:
                if (battleskillSkill.min_range <= 0)
                {
                  skillEffectListUnitList.Add(enemy);
                  break;
                }
                break;
              case BattleskillTargetType.complex_single:
              case BattleskillTargetType.complex_range:
                if (((IEnumerable<BattleskillEffect>) battleskillSkill.Effects).Any<BattleskillEffect>((Func<BattleskillEffect, bool>) (x => !x.is_targer_enemy)) && battleskillSkill.min_range <= 1 && battleskillSkill.max_range >= 1)
                  skillEffectListUnitList.Add(myself);
                if (((IEnumerable<BattleskillEffect>) battleskillSkill.Effects).Any<BattleskillEffect>((Func<BattleskillEffect, bool>) (x => x.is_targer_enemy)) && battleskillSkill.min_range <= 0)
                {
                  skillEffectListUnitList.Add(enemy);
                  break;
                }
                break;
            }
            break;
        }
        return (IEnumerable<BL.ISkillEffectListUnit>) skillEffectListUnitList.ToArray();
      }
      List<BL.UnitPosition> source = new List<BL.UnitPosition>();
      BL.ISkillEffectListUnit skillEffectListUnit;
      switch (rangeType)
      {
        case 0:
          switch (battleskillSkill.target_type)
          {
            case BattleskillTargetType.myself:
            case BattleskillTargetType.player_single:
              source = BattleFuncs.getRangeTargets(myself, new int[2], new BL.ForceID[1]
              {
                BattleFuncs.getForceID(myself.originalUnit)
              }, true).ToList<BL.UnitPosition>();
              goto label_65;
            case BattleskillTargetType.player_range:
              source = BattleFuncs.getRangeTargets(myself, new int[2]
              {
                battleskillSkill.min_range,
                battleskillSkill.max_range
              }, new BL.ForceID[1]
              {
                BattleFuncs.getForceID(myself.originalUnit)
              }, true).ToList<BL.UnitPosition>();
              goto label_65;
            case BattleskillTargetType.enemy_single:
              source = BattleFuncs.getRangeTargets(enemy, new int[2], new BL.ForceID[1]
              {
                BattleFuncs.getForceID(enemy.originalUnit)
              }, true).ToList<BL.UnitPosition>();
              goto label_65;
            case BattleskillTargetType.enemy_range:
              source = BattleFuncs.getRangeTargets(myself, new int[2]
              {
                battleskillSkill.min_range,
                battleskillSkill.max_range
              }, new BL.ForceID[1]
              {
                BattleFuncs.getForceID(enemy.originalUnit)
              }, true).ToList<BL.UnitPosition>();
              goto label_65;
            case BattleskillTargetType.complex_single:
              if (((IEnumerable<BattleskillEffect>) battleskillSkill.Effects).Any<BattleskillEffect>((Func<BattleskillEffect, bool>) (x => !x.is_targer_enemy)))
                source.AddRange(BattleFuncs.getRangeTargets(myself, new int[2], new BL.ForceID[1]
                {
                  BattleFuncs.getForceID(myself.originalUnit)
                }, true));
              if (((IEnumerable<BattleskillEffect>) battleskillSkill.Effects).Any<BattleskillEffect>((Func<BattleskillEffect, bool>) (x => x.is_targer_enemy)))
              {
                source.AddRange(BattleFuncs.getRangeTargets(enemy, new int[2], new BL.ForceID[1]
                {
                  BattleFuncs.getForceID(enemy.originalUnit)
                }, true));
                goto label_65;
              }
              else
                goto label_65;
            case BattleskillTargetType.complex_range:
              if (((IEnumerable<BattleskillEffect>) battleskillSkill.Effects).Any<BattleskillEffect>((Func<BattleskillEffect, bool>) (x => !x.is_targer_enemy)))
                source.AddRange(BattleFuncs.getRangeTargets(myself, new int[2]
                {
                  battleskillSkill.min_range,
                  battleskillSkill.max_range
                }, new BL.ForceID[1]
                {
                  BattleFuncs.getForceID(myself.originalUnit)
                }, true));
              if (((IEnumerable<BattleskillEffect>) battleskillSkill.Effects).Any<BattleskillEffect>((Func<BattleskillEffect, bool>) (x => x.is_targer_enemy)))
              {
                source.AddRange(BattleFuncs.getRangeTargets(myself, new int[2]
                {
                  battleskillSkill.min_range,
                  battleskillSkill.max_range
                }, new BL.ForceID[1]
                {
                  BattleFuncs.getForceID(enemy.originalUnit)
                }, true));
                goto label_65;
              }
              else
                goto label_65;
            default:
              goto label_65;
          }
        case 1:
          skillEffectListUnit = myself;
          break;
        case 2:
          skillEffectListUnit = enemy;
          break;
        default:
label_65:
          return source.Select<BL.UnitPosition, BL.ISkillEffectListUnit>((Func<BL.UnitPosition, BL.ISkillEffectListUnit>) (x => BattleFuncs.unitPositionToISkillEffectListUnit(x)));
      }
      BL.ISkillEffectListUnit unit = skillEffectListUnit;
      int[] range = new int[2]
      {
        battleskillSkill.min_range,
        battleskillSkill.max_range
      };
      switch (battleskillSkill.target_type)
      {
        case BattleskillTargetType.myself:
        case BattleskillTargetType.player_single:
          IEnumerable<BL.UnitPosition> rangeTargets1 = BattleFuncs.getRangeTargets(unit, range, new BL.ForceID[1]
          {
            BattleFuncs.getForceID(myself.originalUnit)
          }, true);
          source.AddRange(rangeTargets1.Where<BL.UnitPosition>((Func<BL.UnitPosition, bool>) (x => BattleFuncs.unitPositionToISkillEffectListUnit(x) == myself)));
          goto label_65;
        case BattleskillTargetType.player_range:
          source = BattleFuncs.getRangeTargets(unit, range, new BL.ForceID[1]
          {
            BattleFuncs.getForceID(myself.originalUnit)
          }, true).ToList<BL.UnitPosition>();
          goto label_65;
        case BattleskillTargetType.enemy_single:
          IEnumerable<BL.UnitPosition> rangeTargets2 = BattleFuncs.getRangeTargets(unit, range, new BL.ForceID[1]
          {
            BattleFuncs.getForceID(enemy.originalUnit)
          }, true);
          source.AddRange(rangeTargets2.Where<BL.UnitPosition>((Func<BL.UnitPosition, bool>) (x => BattleFuncs.unitPositionToISkillEffectListUnit(x) == enemy)));
          goto label_65;
        case BattleskillTargetType.enemy_range:
          source = BattleFuncs.getRangeTargets(unit, range, new BL.ForceID[1]
          {
            BattleFuncs.getForceID(enemy.originalUnit)
          }, true).ToList<BL.UnitPosition>();
          goto label_65;
        case BattleskillTargetType.complex_single:
          if (((IEnumerable<BattleskillEffect>) battleskillSkill.Effects).Any<BattleskillEffect>((Func<BattleskillEffect, bool>) (x => !x.is_targer_enemy)))
          {
            IEnumerable<BL.UnitPosition> rangeTargets3 = BattleFuncs.getRangeTargets(unit, range, new BL.ForceID[1]
            {
              BattleFuncs.getForceID(myself.originalUnit)
            }, true);
            source.AddRange(rangeTargets3.Where<BL.UnitPosition>((Func<BL.UnitPosition, bool>) (x => BattleFuncs.unitPositionToISkillEffectListUnit(x) == myself)));
          }
          if (((IEnumerable<BattleskillEffect>) battleskillSkill.Effects).Any<BattleskillEffect>((Func<BattleskillEffect, bool>) (x => x.is_targer_enemy)))
          {
            IEnumerable<BL.UnitPosition> rangeTargets3 = BattleFuncs.getRangeTargets(unit, range, new BL.ForceID[1]
            {
              BattleFuncs.getForceID(enemy.originalUnit)
            }, true);
            source.AddRange(rangeTargets3.Where<BL.UnitPosition>((Func<BL.UnitPosition, bool>) (x => BattleFuncs.unitPositionToISkillEffectListUnit(x) == enemy)));
            goto label_65;
          }
          else
            goto label_65;
        case BattleskillTargetType.complex_range:
          if (((IEnumerable<BattleskillEffect>) battleskillSkill.Effects).Any<BattleskillEffect>((Func<BattleskillEffect, bool>) (x => !x.is_targer_enemy)))
            source.AddRange(BattleFuncs.getRangeTargets(unit, range, new BL.ForceID[1]
            {
              BattleFuncs.getForceID(myself.originalUnit)
            }, true));
          if (((IEnumerable<BattleskillEffect>) battleskillSkill.Effects).Any<BattleskillEffect>((Func<BattleskillEffect, bool>) (x => x.is_targer_enemy)))
          {
            source.AddRange(BattleFuncs.getRangeTargets(unit, range, new BL.ForceID[1]
            {
              BattleFuncs.getForceID(enemy.originalUnit)
            }, true));
            goto label_65;
          }
          else
            goto label_65;
        default:
          goto label_65;
      }
    }

    private static IEnumerable<BL.SkillEffect> GetEnabledDeckEveryBuffDebuff(
      BL.SkillEffectList self,
      BattleskillEffectLogicEnum e,
      BL.ISkillEffectListUnit unit,
      IEnumerable<BL.Unit> deckUnits,
      Func<int, int, bool> condFunc)
    {
      return self.WhereAndGroupBy(e, (Func<IEnumerable<BL.SkillEffect>, BL.SkillEffect>) (x => x.OrderBy<BL.SkillEffect, int>((Func<BL.SkillEffect, int>) (effect => effect.effectId)).FirstOrDefault<BL.SkillEffect>()), (Func<IGrouping<int, BL.SkillEffect>, IEnumerable<BL.SkillEffect>>) (skills => skills.Where<BL.SkillEffect>((Func<BL.SkillEffect, bool>) (effect =>
      {
        int num1 = effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.type);
        Dictionary<int, int> dictionary = new Dictionary<int, int>();
        for (int index1 = 1; index1 <= 7; ++index1)
        {
          BattleskillEffectLogicArgumentEnum logicArgumentEnum = BattleskillEffectLogic.GetLogicArgumentEnum("kind_count" + (object) index1);
          if (logicArgumentEnum != BattleskillEffectLogicArgumentEnum.none)
          {
            int num2 = effect.effect.GetInt(logicArgumentEnum);
            if (num2 != 0)
            {
              int index2 = num2 / 10;
              int num3 = num2 % 10;
              dictionary[index2] = num3;
            }
          }
        }
        foreach (int key in dictionary.Keys)
        {
          int kind = key;
          int num2 = 0;
          switch (num1)
          {
            case 0:
              num2 = deckUnits.Count<BL.Unit>((Func<BL.Unit, bool>) (x => x.originalUnit.unit.kind.ID == kind));
              break;
            case 1:
              num2 = deckUnits.Count<BL.Unit>((Func<BL.Unit, bool>) (x => x.originalUnit.playerUnit.GetElement() == (CommonElement) kind));
              break;
            case 2:
              num2 = deckUnits.Count<BL.Unit>((Func<BL.Unit, bool>) (x => x.originalUnit.job.ID == kind));
              break;
            case 3:
              num2 = deckUnits.Count<BL.Unit>((Func<BL.Unit, bool>) (x => x.originalUnit.playerUnit.HasFamily((UnitFamily) kind)));
              break;
            case 4:
              num2 = deckUnits.Count<BL.Unit>((Func<BL.Unit, bool>) (x => x.unit.character.ID == kind));
              break;
            case 5:
              num2 = deckUnits.Count<BL.Unit>((Func<BL.Unit, bool>) (x => x.unit.same_character_id == kind));
              break;
            case 6:
              num2 = deckUnits.Count<BL.Unit>((Func<BL.Unit, bool>) (x => x.unit.ID == kind));
              break;
            case 7:
              num2 = deckUnits.Count<BL.Unit>((Func<BL.Unit, bool>) (x => x.originalUnit.unitGroup != null && x.originalUnit.unitGroup.group_large_category_id.ID == kind));
              break;
            case 8:
              num2 = deckUnits.Count<BL.Unit>((Func<BL.Unit, bool>) (x => x.originalUnit.unitGroup != null && x.originalUnit.unitGroup.group_small_category_id.ID == kind));
              break;
            case 9:
              num2 = deckUnits.Count<BL.Unit>((Func<BL.Unit, bool>) (x =>
              {
                if (x.originalUnit.unitGroup == null)
                  return false;
                return x.originalUnit.unitGroup.group_clothing_category_id.ID == kind || x.originalUnit.unitGroup.group_clothing_category_id_2.ID == kind;
              }));
              break;
            case 10:
              num2 = deckUnits.Count<BL.Unit>((Func<BL.Unit, bool>) (x => x.originalUnit.unitGroup != null && x.originalUnit.unitGroup.group_generation_category_id.ID == kind));
              break;
            case 11:
              num2 = deckUnits.Count<BL.Unit>((Func<BL.Unit, bool>) (x => x.originalUnit.unit.HasSkillGroupId(kind)));
              break;
          }
          if (!condFunc(num2, dictionary[kind]))
            return false;
        }
        return true;
      }))));
    }

    private static IEnumerable<BL.SkillEffect> GetEnabledDeckEveryGeBuffDebuff(
      BL.SkillEffectList self,
      BattleskillEffectLogicEnum e,
      BL.ISkillEffectListUnit unit,
      IEnumerable<BL.Unit> deckUnits)
    {
      return BattleFuncs.GetEnabledDeckEveryBuffDebuff(self, e, unit, deckUnits, (Func<int, int, bool>) ((deckCount, condCount) => deckCount >= condCount));
    }

    private static IEnumerable<BL.SkillEffect> GetEnabledDeckEveryLeBuffDebuff(
      BL.SkillEffectList self,
      BattleskillEffectLogicEnum e,
      BL.ISkillEffectListUnit unit,
      IEnumerable<BL.Unit> deckUnits)
    {
      return BattleFuncs.GetEnabledDeckEveryBuffDebuff(self, e, unit, deckUnits, (Func<int, int, bool>) ((deckCount, condCount) => deckCount <= condCount));
    }

    private static IEnumerable<BL.SkillEffect> GetEnabledDeckSameAnotherBuffDebuff(
      BL.SkillEffectList self,
      BattleskillEffectLogicEnum e,
      BL.ISkillEffectListUnit unit,
      IEnumerable<BL.Unit> deckUnits,
      Func<IEnumerable<int>, int, bool> condFunc)
    {
      return self.WhereAndGroupBy(e, (Func<IEnumerable<BL.SkillEffect>, BL.SkillEffect>) (x => x.OrderBy<BL.SkillEffect, int>((Func<BL.SkillEffect, int>) (effect => effect.effectId)).FirstOrDefault<BL.SkillEffect>()), (Func<IGrouping<int, BL.SkillEffect>, IEnumerable<BL.SkillEffect>>) (skills => skills.Where<BL.SkillEffect>((Func<BL.SkillEffect, bool>) (effect =>
      {
        int num1 = effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.kind_count);
        IEnumerable<int> source = (IEnumerable<int>) null;
        List<BattleskillEffect> effects = BattleFuncs.PackedSkillEffect.Create(effect).GetEffects();
        if (effects.Skip<BattleskillEffect>(1).Any<BattleskillEffect>((Func<BattleskillEffect, bool>) (x => x.HasKey(BattleskillEffectLogicArgumentEnum.type))))
        {
          BL.Unit[] array = deckUnits.Where<BL.Unit>((Func<BL.Unit, bool>) (x =>
          {
            foreach (BattleskillEffect battleskillEffect in effects)
            {
              int num2 = battleskillEffect.GetInt(BattleskillEffectLogicArgumentEnum.type);
              int typeId = battleskillEffect.HasKey(BattleskillEffectLogicArgumentEnum.type_id) ? battleskillEffect.GetInt(BattleskillEffectLogicArgumentEnum.type_id) : 0;
              bool flag = false;
              switch (num2)
              {
                case 0:
                  if (typeId == 0 || x.originalUnit.unit.kind.ID == typeId)
                  {
                    flag = true;
                    break;
                  }
                  break;
                case 1:
                  if (typeId == 0 || x.originalUnit.playerUnit.GetElement() == (CommonElement) typeId)
                  {
                    flag = true;
                    break;
                  }
                  break;
                case 2:
                  if (typeId == 0 || x.originalUnit.job.ID == typeId)
                  {
                    flag = true;
                    break;
                  }
                  break;
                case 3:
                  if (typeId == 0 || x.originalUnit.playerUnit.HasFamily((UnitFamily) typeId))
                  {
                    flag = true;
                    break;
                  }
                  break;
                case 4:
                  if (typeId == 0 || x.unit.character.ID == typeId)
                  {
                    flag = true;
                    break;
                  }
                  break;
                case 5:
                  if (typeId == 0 || x.unit.same_character_id == typeId)
                  {
                    flag = true;
                    break;
                  }
                  break;
                case 6:
                  if (typeId == 0 || x.unit.ID == typeId)
                  {
                    flag = true;
                    break;
                  }
                  break;
                case 7:
                  if (x.originalUnit.unitGroup != null && (typeId == 0 || x.originalUnit.unitGroup.group_large_category_id.ID == typeId))
                  {
                    flag = true;
                    break;
                  }
                  break;
                case 8:
                  if (x.originalUnit.unitGroup != null && (typeId == 0 || x.originalUnit.unitGroup.group_small_category_id.ID == typeId))
                  {
                    flag = true;
                    break;
                  }
                  break;
                case 9:
                  if (x.originalUnit.unitGroup != null && (typeId == 0 || x.originalUnit.unitGroup.group_clothing_category_id.ID == typeId || x.originalUnit.unitGroup.group_clothing_category_id_2.ID == typeId))
                  {
                    flag = true;
                    break;
                  }
                  break;
                case 10:
                  if (x.originalUnit.unitGroup != null && (typeId == 0 || x.originalUnit.unitGroup.group_generation_category_id.ID == typeId))
                  {
                    flag = true;
                    break;
                  }
                  break;
                case 11:
                  if (typeId == 0 || ((IEnumerable<int>) x.originalUnit.unit.SkillGroupIds).Any<int>((Func<int, bool>) (y => y == typeId)))
                  {
                    flag = true;
                    break;
                  }
                  break;
                case 12:
                  if (typeId == 0 || x.originalUnit.playerUnit.unit_type.ID == typeId)
                  {
                    flag = true;
                    break;
                  }
                  break;
              }
              if (!flag)
                return false;
            }
            return true;
          })).ToArray<BL.Unit>();
          if (array.Length >= 1)
            source = (IEnumerable<int>) new int[1]
            {
              array.Length
            };
        }
        else
        {
          int num2 = effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.type);
          int typeId = effect.effect.HasKey(BattleskillEffectLogicArgumentEnum.type_id) ? effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.type_id) : 0;
          switch (num2)
          {
            case 0:
              source = deckUnits.Where<BL.Unit>((Func<BL.Unit, bool>) (x => typeId == 0 || x.originalUnit.unit.kind.ID == typeId)).GroupBy<BL.Unit, int>((Func<BL.Unit, int>) (x => x.originalUnit.unit.kind.ID)).Select<IGrouping<int, BL.Unit>, int>((Func<IGrouping<int, BL.Unit>, int>) (x => x.Count<BL.Unit>()));
              break;
            case 1:
              source = deckUnits.Where<BL.Unit>((Func<BL.Unit, bool>) (x => typeId == 0 || x.originalUnit.playerUnit.GetElement() == (CommonElement) typeId)).GroupBy<BL.Unit, int>((Func<BL.Unit, int>) (x => (int) x.originalUnit.playerUnit.GetElement())).Select<IGrouping<int, BL.Unit>, int>((Func<IGrouping<int, BL.Unit>, int>) (x => x.Count<BL.Unit>()));
              break;
            case 2:
              source = deckUnits.Where<BL.Unit>((Func<BL.Unit, bool>) (x => typeId == 0 || x.originalUnit.job.ID == typeId)).GroupBy<BL.Unit, int>((Func<BL.Unit, int>) (x => x.originalUnit.job.ID)).Select<IGrouping<int, BL.Unit>, int>((Func<IGrouping<int, BL.Unit>, int>) (x => x.Count<BL.Unit>()));
              break;
            case 3:
              source = deckUnits.SelectMany<BL.Unit, UnitFamily>((Func<BL.Unit, IEnumerable<UnitFamily>>) (x => (IEnumerable<UnitFamily>) x.originalUnit.playerUnit.Families)).Where<UnitFamily>((Func<UnitFamily, bool>) (x => typeId == 0 || x == (UnitFamily) typeId)).GroupBy<UnitFamily, UnitFamily>((Func<UnitFamily, UnitFamily>) (x => x)).Select<IGrouping<UnitFamily, UnitFamily>, int>((Func<IGrouping<UnitFamily, UnitFamily>, int>) (x => x.Count<UnitFamily>()));
              break;
            case 4:
              source = deckUnits.Where<BL.Unit>((Func<BL.Unit, bool>) (x => typeId == 0 || x.unit.character.ID == typeId)).GroupBy<BL.Unit, int>((Func<BL.Unit, int>) (x => x.unit.character.ID)).Select<IGrouping<int, BL.Unit>, int>((Func<IGrouping<int, BL.Unit>, int>) (x => x.Count<BL.Unit>()));
              break;
            case 5:
              source = deckUnits.Where<BL.Unit>((Func<BL.Unit, bool>) (x => typeId == 0 || x.unit.same_character_id == typeId)).GroupBy<BL.Unit, int>((Func<BL.Unit, int>) (x => x.unit.same_character_id)).Select<IGrouping<int, BL.Unit>, int>((Func<IGrouping<int, BL.Unit>, int>) (x => x.Count<BL.Unit>()));
              break;
            case 6:
              source = deckUnits.Where<BL.Unit>((Func<BL.Unit, bool>) (x => typeId == 0 || x.unit.ID == typeId)).GroupBy<BL.Unit, int>((Func<BL.Unit, int>) (x => x.unit.ID)).Select<IGrouping<int, BL.Unit>, int>((Func<IGrouping<int, BL.Unit>, int>) (x => x.Count<BL.Unit>()));
              break;
            case 7:
              source = deckUnits.Where<BL.Unit>((Func<BL.Unit, bool>) (x =>
              {
                if (x.originalUnit.unitGroup == null)
                  return false;
                return typeId == 0 || x.originalUnit.unitGroup.group_large_category_id.ID == typeId;
              })).GroupBy<BL.Unit, int>((Func<BL.Unit, int>) (x => x.originalUnit.unitGroup.group_large_category_id.ID)).Select<IGrouping<int, BL.Unit>, int>((Func<IGrouping<int, BL.Unit>, int>) (x => x.Count<BL.Unit>()));
              break;
            case 8:
              source = deckUnits.Where<BL.Unit>((Func<BL.Unit, bool>) (x =>
              {
                if (x.originalUnit.unitGroup == null)
                  return false;
                return typeId == 0 || x.originalUnit.unitGroup.group_small_category_id.ID == typeId;
              })).GroupBy<BL.Unit, int>((Func<BL.Unit, int>) (x => x.originalUnit.unitGroup.group_small_category_id.ID)).Select<IGrouping<int, BL.Unit>, int>((Func<IGrouping<int, BL.Unit>, int>) (x => x.Count<BL.Unit>()));
              break;
            case 9:
              source = deckUnits.Where<BL.Unit>((Func<BL.Unit, bool>) (x =>
              {
                if (x.originalUnit.unitGroup == null)
                  return false;
                return typeId == 0 || x.originalUnit.unitGroup.group_clothing_category_id.ID == typeId || x.originalUnit.unitGroup.group_clothing_category_id_2.ID == typeId;
              })).Select<BL.Unit, List<int>>((Func<BL.Unit, List<int>>) (x =>
              {
                List<int> intList = new List<int>();
                if (typeId == 0 || x.originalUnit.unitGroup.group_clothing_category_id.ID == typeId)
                  intList.Add(x.originalUnit.unitGroup.group_clothing_category_id.ID);
                if (typeId == 0 || x.originalUnit.unitGroup.group_clothing_category_id_2.ID == typeId)
                  intList.Add(x.originalUnit.unitGroup.group_clothing_category_id_2.ID);
                return intList;
              })).SelectMany<List<int>, int>((Func<List<int>, IEnumerable<int>>) (x => (IEnumerable<int>) x)).GroupBy<int, int>((Func<int, int>) (x => x)).Select<IGrouping<int, int>, int>((Func<IGrouping<int, int>, int>) (x => x.Count<int>()));
              break;
            case 10:
              source = deckUnits.Where<BL.Unit>((Func<BL.Unit, bool>) (x =>
              {
                if (x.originalUnit.unitGroup == null)
                  return false;
                return typeId == 0 || x.originalUnit.unitGroup.group_generation_category_id.ID == typeId;
              })).GroupBy<BL.Unit, int>((Func<BL.Unit, int>) (x => x.originalUnit.unitGroup.group_generation_category_id.ID)).Select<IGrouping<int, BL.Unit>, int>((Func<IGrouping<int, BL.Unit>, int>) (x => x.Count<BL.Unit>()));
              break;
            case 11:
              source = deckUnits.SelectMany<BL.Unit, int>((Func<BL.Unit, IEnumerable<int>>) (x => (IEnumerable<int>) x.originalUnit.unit.SkillGroupIds)).Where<int>((Func<int, bool>) (x => typeId == 0 || x == typeId)).GroupBy<int, int>((Func<int, int>) (x => x)).Select<IGrouping<int, int>, int>((Func<IGrouping<int, int>, int>) (x => x.Count<int>()));
              break;
            case 12:
              source = deckUnits.Where<BL.Unit>((Func<BL.Unit, bool>) (x => typeId == 0 || x.originalUnit.playerUnit.unit_type.ID == typeId)).GroupBy<BL.Unit, int>((Func<BL.Unit, int>) (x => x.originalUnit.playerUnit.unit_type.ID)).Select<IGrouping<int, BL.Unit>, int>((Func<IGrouping<int, BL.Unit>, int>) (x => x.Count<BL.Unit>()));
              break;
          }
        }
        return source != null && source.Any<int>() && condFunc(source, num1);
      }))));
    }

    private static IEnumerable<BL.SkillEffect> GetEnabledDeckAnotherGeBuffDebuff(
      BL.SkillEffectList self,
      BattleskillEffectLogicEnum e,
      BL.ISkillEffectListUnit unit,
      IEnumerable<BL.Unit> deckUnits)
    {
      return BattleFuncs.GetEnabledDeckSameAnotherBuffDebuff(self, e, unit, deckUnits, (Func<IEnumerable<int>, int, bool>) ((count, kindCount) => count.Count<int>() >= kindCount));
    }

    private static IEnumerable<BL.SkillEffect> GetEnabledDeckAnotherLeBuffDebuff(
      BL.SkillEffectList self,
      BattleskillEffectLogicEnum e,
      BL.ISkillEffectListUnit unit,
      IEnumerable<BL.Unit> deckUnits)
    {
      return BattleFuncs.GetEnabledDeckSameAnotherBuffDebuff(self, e, unit, deckUnits, (Func<IEnumerable<int>, int, bool>) ((count, kindCount) => count.Count<int>() <= kindCount));
    }

    private static IEnumerable<BL.SkillEffect> GetEnabledDeckSameGeBuffDebuff(
      BL.SkillEffectList self,
      BattleskillEffectLogicEnum e,
      BL.ISkillEffectListUnit unit,
      IEnumerable<BL.Unit> deckUnits)
    {
      return BattleFuncs.GetEnabledDeckSameAnotherBuffDebuff(self, e, unit, deckUnits, (Func<IEnumerable<int>, int, bool>) ((count, kindCount) => count.OrderByDescending<int, int>((Func<int, int>) (x => x)).First<int>() >= kindCount));
    }

    private static IEnumerable<BL.SkillEffect> GetEnabledDeckSameLeBuffDebuff(
      BL.SkillEffectList self,
      BattleskillEffectLogicEnum e,
      BL.ISkillEffectListUnit unit,
      IEnumerable<BL.Unit> deckUnits)
    {
      return BattleFuncs.GetEnabledDeckSameAnotherBuffDebuff(self, e, unit, deckUnits, (Func<IEnumerable<int>, int, bool>) ((count, kindCount) => count.OrderByDescending<int, int>((Func<int, int>) (x => x)).First<int>() <= kindCount));
    }

    private static void GetDeckEveryGeSkillAdd(
      BL.ISkillEffectListUnit beUnit,
      BattleskillEffectLogicEnum fix_logic,
      IEnumerable<BL.Unit> deckUnits)
    {
      foreach (BL.SkillEffect effect in BattleFuncs.GetEnabledDeckEveryGeBuffDebuff(beUnit.skillEffects, fix_logic, beUnit, deckUnits))
        beUnit.skillEffects.AddFixEffectParam(fix_logic, effect, effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.value) + effect.baseSkillLevel * effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.skill_ratio));
    }

    private static void GetDeckEveryGeSkillMul(
      BL.ISkillEffectListUnit beUnit,
      BattleskillEffectLogicEnum ratio_logic,
      IEnumerable<BL.Unit> deckUnits)
    {
      foreach (BL.SkillEffect effect in BattleFuncs.GetEnabledDeckEveryGeBuffDebuff(beUnit.skillEffects, ratio_logic, beUnit, deckUnits))
        beUnit.skillEffects.AddRatioEffectParam(ratio_logic, effect, effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.percentage) + (float) effect.baseSkillLevel * effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.skill_ratio));
    }

    private static void GetDeckEveryLeSkillAdd(
      BL.ISkillEffectListUnit beUnit,
      BattleskillEffectLogicEnum fix_logic,
      IEnumerable<BL.Unit> deckUnits)
    {
      foreach (BL.SkillEffect effect in BattleFuncs.GetEnabledDeckEveryLeBuffDebuff(beUnit.skillEffects, fix_logic, beUnit, deckUnits))
        beUnit.skillEffects.AddFixEffectParam(fix_logic, effect, effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.value) + effect.baseSkillLevel * effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.skill_ratio));
    }

    private static void GetDeckEveryLeSkillMul(
      BL.ISkillEffectListUnit beUnit,
      BattleskillEffectLogicEnum ratio_logic,
      IEnumerable<BL.Unit> deckUnits)
    {
      foreach (BL.SkillEffect effect in BattleFuncs.GetEnabledDeckEveryLeBuffDebuff(beUnit.skillEffects, ratio_logic, beUnit, deckUnits))
        beUnit.skillEffects.AddRatioEffectParam(ratio_logic, effect, effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.percentage) + (float) effect.baseSkillLevel * effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.skill_ratio));
    }

    private static void GetDeckAnotherGeSkillAdd(
      BL.ISkillEffectListUnit beUnit,
      BattleskillEffectLogicEnum fix_logic,
      IEnumerable<BL.Unit> deckUnits)
    {
      foreach (BL.SkillEffect effect in BattleFuncs.GetEnabledDeckAnotherGeBuffDebuff(beUnit.skillEffects, fix_logic, beUnit, deckUnits))
        beUnit.skillEffects.AddFixEffectParam(fix_logic, effect, effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.value) + effect.baseSkillLevel * effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.skill_ratio));
    }

    private static void GetDeckAnotherGeSkillMul(
      BL.ISkillEffectListUnit beUnit,
      BattleskillEffectLogicEnum ratio_logic,
      IEnumerable<BL.Unit> deckUnits)
    {
      foreach (BL.SkillEffect effect in BattleFuncs.GetEnabledDeckAnotherGeBuffDebuff(beUnit.skillEffects, ratio_logic, beUnit, deckUnits))
        beUnit.skillEffects.AddRatioEffectParam(ratio_logic, effect, effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.percentage) + (float) effect.baseSkillLevel * effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.skill_ratio));
    }

    private static void GetDeckAnotherLeSkillAdd(
      BL.ISkillEffectListUnit beUnit,
      BattleskillEffectLogicEnum fix_logic,
      IEnumerable<BL.Unit> deckUnits)
    {
      foreach (BL.SkillEffect effect in BattleFuncs.GetEnabledDeckAnotherLeBuffDebuff(beUnit.skillEffects, fix_logic, beUnit, deckUnits))
        beUnit.skillEffects.AddFixEffectParam(fix_logic, effect, effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.value) + effect.baseSkillLevel * effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.skill_ratio));
    }

    private static void GetDeckAnotherLeSkillMul(
      BL.ISkillEffectListUnit beUnit,
      BattleskillEffectLogicEnum ratio_logic,
      IEnumerable<BL.Unit> deckUnits)
    {
      foreach (BL.SkillEffect effect in BattleFuncs.GetEnabledDeckAnotherLeBuffDebuff(beUnit.skillEffects, ratio_logic, beUnit, deckUnits))
        beUnit.skillEffects.AddRatioEffectParam(ratio_logic, effect, effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.percentage) + (float) effect.baseSkillLevel * effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.skill_ratio));
    }

    private static void GetDeckSameGeSkillAdd(
      BL.ISkillEffectListUnit beUnit,
      BattleskillEffectLogicEnum fix_logic,
      IEnumerable<BL.Unit> deckUnits)
    {
      foreach (BL.SkillEffect effect in BattleFuncs.GetEnabledDeckSameGeBuffDebuff(beUnit.skillEffects, fix_logic, beUnit, deckUnits))
        beUnit.skillEffects.AddFixEffectParam(fix_logic, effect, effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.value) + effect.baseSkillLevel * effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.skill_ratio));
    }

    private static void GetDeckSameGeSkillMul(
      BL.ISkillEffectListUnit beUnit,
      BattleskillEffectLogicEnum ratio_logic,
      IEnumerable<BL.Unit> deckUnits)
    {
      foreach (BL.SkillEffect effect in BattleFuncs.GetEnabledDeckSameGeBuffDebuff(beUnit.skillEffects, ratio_logic, beUnit, deckUnits))
        beUnit.skillEffects.AddRatioEffectParam(ratio_logic, effect, effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.percentage) + (float) effect.baseSkillLevel * effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.skill_ratio));
    }

    private static void GetDeckSameLeSkillAdd(
      BL.ISkillEffectListUnit beUnit,
      BattleskillEffectLogicEnum fix_logic,
      IEnumerable<BL.Unit> deckUnits)
    {
      foreach (BL.SkillEffect effect in BattleFuncs.GetEnabledDeckSameLeBuffDebuff(beUnit.skillEffects, fix_logic, beUnit, deckUnits))
        beUnit.skillEffects.AddFixEffectParam(fix_logic, effect, effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.value) + effect.baseSkillLevel * effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.skill_ratio));
    }

    private static void GetDeckSameLeSkillMul(
      BL.ISkillEffectListUnit beUnit,
      BattleskillEffectLogicEnum ratio_logic,
      IEnumerable<BL.Unit> deckUnits)
    {
      foreach (BL.SkillEffect effect in BattleFuncs.GetEnabledDeckSameLeBuffDebuff(beUnit.skillEffects, ratio_logic, beUnit, deckUnits))
        beUnit.skillEffects.AddRatioEffectParam(ratio_logic, effect, effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.percentage) + (float) effect.baseSkillLevel * effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.skill_ratio));
    }

    public static void createBattleSkillEffectParams(
      BL.ISkillEffectListUnit beUnit,
      IEnumerable<BL.Unit> deckUnits)
    {
      bool flag = false;
      foreach (BattleskillEffectLogicEnum battleskillEffectLogicEnum in BattleFuncs.DeckEveryGeSkillAddEnum)
      {
        BattleFuncs.GetDeckEveryGeSkillAdd(beUnit, battleskillEffectLogicEnum, deckUnits);
        flag |= beUnit.skillEffects.RemoveEffect(beUnit.skillEffects.Where(battleskillEffectLogicEnum).Except<BL.SkillEffect>(beUnit.skillEffects.GetFixEffectParams(battleskillEffectLogicEnum).Select<Tuple<BL.SkillEffect, int>, BL.SkillEffect>((Func<Tuple<BL.SkillEffect, int>, BL.SkillEffect>) (x => x.Item1))));
      }
      foreach (BattleskillEffectLogicEnum battleskillEffectLogicEnum in BattleFuncs.DeckEveryGeSkillMulEnum)
      {
        BattleFuncs.GetDeckEveryGeSkillMul(beUnit, battleskillEffectLogicEnum, deckUnits);
        flag |= beUnit.skillEffects.RemoveEffect(beUnit.skillEffects.Where(battleskillEffectLogicEnum).Except<BL.SkillEffect>(beUnit.skillEffects.GetRatioEffectParams(battleskillEffectLogicEnum).Select<Tuple<BL.SkillEffect, float>, BL.SkillEffect>((Func<Tuple<BL.SkillEffect, float>, BL.SkillEffect>) (x => x.Item1))));
      }
      foreach (BattleskillEffectLogicEnum battleskillEffectLogicEnum in BattleFuncs.DeckEveryLeSkillAddEnum)
      {
        BattleFuncs.GetDeckEveryLeSkillAdd(beUnit, battleskillEffectLogicEnum, deckUnits);
        flag |= beUnit.skillEffects.RemoveEffect(beUnit.skillEffects.Where(battleskillEffectLogicEnum).Except<BL.SkillEffect>(beUnit.skillEffects.GetFixEffectParams(battleskillEffectLogicEnum).Select<Tuple<BL.SkillEffect, int>, BL.SkillEffect>((Func<Tuple<BL.SkillEffect, int>, BL.SkillEffect>) (x => x.Item1))));
      }
      foreach (BattleskillEffectLogicEnum battleskillEffectLogicEnum in BattleFuncs.DeckEveryLeSkillMulEnum)
      {
        BattleFuncs.GetDeckEveryLeSkillMul(beUnit, battleskillEffectLogicEnum, deckUnits);
        flag |= beUnit.skillEffects.RemoveEffect(beUnit.skillEffects.Where(battleskillEffectLogicEnum).Except<BL.SkillEffect>(beUnit.skillEffects.GetRatioEffectParams(battleskillEffectLogicEnum).Select<Tuple<BL.SkillEffect, float>, BL.SkillEffect>((Func<Tuple<BL.SkillEffect, float>, BL.SkillEffect>) (x => x.Item1))));
      }
      foreach (BattleskillEffectLogicEnum battleskillEffectLogicEnum in BattleFuncs.DeckAnotherGeSkillAddEnum)
      {
        BattleFuncs.GetDeckAnotherGeSkillAdd(beUnit, battleskillEffectLogicEnum, deckUnits);
        flag |= beUnit.skillEffects.RemoveEffect(beUnit.skillEffects.Where(battleskillEffectLogicEnum).Except<BL.SkillEffect>(beUnit.skillEffects.GetFixEffectParams(battleskillEffectLogicEnum).Select<Tuple<BL.SkillEffect, int>, BL.SkillEffect>((Func<Tuple<BL.SkillEffect, int>, BL.SkillEffect>) (x => x.Item1))));
      }
      foreach (BattleskillEffectLogicEnum battleskillEffectLogicEnum in BattleFuncs.DeckAnotherGeSkillMulEnum)
      {
        BattleFuncs.GetDeckAnotherGeSkillMul(beUnit, battleskillEffectLogicEnum, deckUnits);
        flag |= beUnit.skillEffects.RemoveEffect(beUnit.skillEffects.Where(battleskillEffectLogicEnum).Except<BL.SkillEffect>(beUnit.skillEffects.GetRatioEffectParams(battleskillEffectLogicEnum).Select<Tuple<BL.SkillEffect, float>, BL.SkillEffect>((Func<Tuple<BL.SkillEffect, float>, BL.SkillEffect>) (x => x.Item1))));
      }
      foreach (BattleskillEffectLogicEnum battleskillEffectLogicEnum in BattleFuncs.DeckAnotherLeSkillAddEnum)
      {
        BattleFuncs.GetDeckAnotherLeSkillAdd(beUnit, battleskillEffectLogicEnum, deckUnits);
        flag |= beUnit.skillEffects.RemoveEffect(beUnit.skillEffects.Where(battleskillEffectLogicEnum).Except<BL.SkillEffect>(beUnit.skillEffects.GetFixEffectParams(battleskillEffectLogicEnum).Select<Tuple<BL.SkillEffect, int>, BL.SkillEffect>((Func<Tuple<BL.SkillEffect, int>, BL.SkillEffect>) (x => x.Item1))));
      }
      foreach (BattleskillEffectLogicEnum battleskillEffectLogicEnum in BattleFuncs.DeckAnotherLeSkillMulEnum)
      {
        BattleFuncs.GetDeckAnotherLeSkillMul(beUnit, battleskillEffectLogicEnum, deckUnits);
        flag |= beUnit.skillEffects.RemoveEffect(beUnit.skillEffects.Where(battleskillEffectLogicEnum).Except<BL.SkillEffect>(beUnit.skillEffects.GetRatioEffectParams(battleskillEffectLogicEnum).Select<Tuple<BL.SkillEffect, float>, BL.SkillEffect>((Func<Tuple<BL.SkillEffect, float>, BL.SkillEffect>) (x => x.Item1))));
      }
      foreach (BattleskillEffectLogicEnum battleskillEffectLogicEnum in BattleFuncs.DeckSameGeSkillAddEnum)
      {
        BattleFuncs.GetDeckSameGeSkillAdd(beUnit, battleskillEffectLogicEnum, deckUnits);
        flag |= beUnit.skillEffects.RemoveEffect(beUnit.skillEffects.Where(battleskillEffectLogicEnum).Except<BL.SkillEffect>(beUnit.skillEffects.GetFixEffectParams(battleskillEffectLogicEnum).Select<Tuple<BL.SkillEffect, int>, BL.SkillEffect>((Func<Tuple<BL.SkillEffect, int>, BL.SkillEffect>) (x => x.Item1))));
      }
      foreach (BattleskillEffectLogicEnum battleskillEffectLogicEnum in BattleFuncs.DeckSameGeSkillMulEnum)
      {
        BattleFuncs.GetDeckSameGeSkillMul(beUnit, battleskillEffectLogicEnum, deckUnits);
        flag |= beUnit.skillEffects.RemoveEffect(beUnit.skillEffects.Where(battleskillEffectLogicEnum).Except<BL.SkillEffect>(beUnit.skillEffects.GetRatioEffectParams(battleskillEffectLogicEnum).Select<Tuple<BL.SkillEffect, float>, BL.SkillEffect>((Func<Tuple<BL.SkillEffect, float>, BL.SkillEffect>) (x => x.Item1))));
      }
      foreach (BattleskillEffectLogicEnum battleskillEffectLogicEnum in BattleFuncs.DeckSameLeSkillAddEnum)
      {
        BattleFuncs.GetDeckSameLeSkillAdd(beUnit, battleskillEffectLogicEnum, deckUnits);
        flag |= beUnit.skillEffects.RemoveEffect(beUnit.skillEffects.Where(battleskillEffectLogicEnum).Except<BL.SkillEffect>(beUnit.skillEffects.GetFixEffectParams(battleskillEffectLogicEnum).Select<Tuple<BL.SkillEffect, int>, BL.SkillEffect>((Func<Tuple<BL.SkillEffect, int>, BL.SkillEffect>) (x => x.Item1))));
      }
      foreach (BattleskillEffectLogicEnum battleskillEffectLogicEnum in BattleFuncs.DeckSameLeSkillMulEnum)
      {
        BattleFuncs.GetDeckSameLeSkillMul(beUnit, battleskillEffectLogicEnum, deckUnits);
        flag |= beUnit.skillEffects.RemoveEffect(beUnit.skillEffects.Where(battleskillEffectLogicEnum).Except<BL.SkillEffect>(beUnit.skillEffects.GetRatioEffectParams(battleskillEffectLogicEnum).Select<Tuple<BL.SkillEffect, float>, BL.SkillEffect>((Func<Tuple<BL.SkillEffect, float>, BL.SkillEffect>) (x => x.Item1))));
      }
      if (flag)
        beUnit.skillEffects.ClearCache();
      beUnit.skillEffects.ResetTransformationSkillEffects(0);
    }

    public static bool isSealedSkillEffect(BL.ISkillEffectListUnit unit, BL.SkillEffect effect)
    {
      if (effect.isAttackMethod)
        return false;
      BL.Unit unit1 = effect.unit;
      if (unit1 != (BL.Unit) null)
      {
        BattleskillSkill skill = effect.effect.skill;
        if (skill.skill_type == BattleskillSkillType.leader || skill.skill_type == BattleskillSkillType.passive && skill.range_effect_passive_skill)
        {
          if (unit is BL.AIUnit)
          {
            foreach (BL.AIUnit aiUnit in BattleFuncs.env.aiUnitPositions.value)
            {
              if (aiUnit.originalUnit.index == unit1.index && aiUnit.originalUnit.isPlayerForce == unit1.isPlayerForce)
                return aiUnit.IsDontUseSkillEffect(effect);
            }
          }
          return unit1.IsDontUseSkillEffect(effect);
        }
      }
      return unit.IsDontUseSkillEffect(effect);
    }

    public static IEnumerable<BL.ISkillEffectListUnit> getForceUnits(
      BL.ForceID forceId,
      bool isAI,
      bool isEnableOnly,
      bool includeFacilities = false)
    {
      if (isAI)
      {
        IEnumerable<BL.AIUnit> source = (IEnumerable<BL.AIUnit>) BattleFuncs.env.aiUnitPositions.value;
        if (isEnableOnly)
          source = source.Where<BL.AIUnit>((Func<BL.AIUnit, bool>) (x => !x.isDead));
        foreach (BL.ISkillEffectListUnit skillEffectListUnit in source.Where<BL.AIUnit>((Func<BL.AIUnit, bool>) (x =>
        {
          if (BattleFuncs.getForceID(x.originalUnit) != forceId)
            return false;
          return includeFacilities || !x.originalUnit.isFacility;
        })))
          yield return skillEffectListUnit;
      }
      else
      {
        IEnumerable<BL.Unit> units = (IEnumerable<BL.Unit>) BattleFuncs.env.getForceUnitList(forceId);
        if (includeFacilities)
          units = units.Concat<BL.Unit>(BattleFuncs.env.facilityUnits.value.Where<BL.Unit>((Func<BL.Unit, bool>) (x => x.facility.thisForce == forceId)));
        if (isEnableOnly)
          units = units.Where<BL.Unit>((Func<BL.Unit, bool>) (x => !x.isDead && x.hp > 0 && x.isEnable));
        foreach (BL.ISkillEffectListUnit skillEffectListUnit in units)
          yield return skillEffectListUnit;
      }
    }

    public static IEnumerable<BL.ISkillEffectListUnit> getAllUnits(
      bool isAI,
      bool isEnableOnly,
      bool includeFacilities = false)
    {
      return ((IEnumerable<BL.ForceID>) new BL.ForceID[3]
      {
        BL.ForceID.player,
        BL.ForceID.enemy,
        BL.ForceID.neutral
      }).SelectMany<BL.ForceID, BL.ISkillEffectListUnit>((Func<BL.ForceID, IEnumerable<BL.ISkillEffectListUnit>>) (x => BattleFuncs.getForceUnits(x, isAI, isEnableOnly, includeFacilities)));
    }

    public static IEnumerable<BL.UnitPosition> getRangeTargets(
      BL.ISkillEffectListUnit unit,
      int[] range,
      BL.ForceID[] forceIds,
      bool nonFacility)
    {
      BL.UnitPosition up = BattleFuncs.iSkillEffectListUnitToUnitPosition(unit);
      bool isAI = up is BL.ISkillEffectListUnit;
      return range[1] == 999 ? ((IEnumerable<BL.ForceID>) forceIds).SelectMany<BL.ForceID, BL.ISkillEffectListUnit>((Func<BL.ForceID, IEnumerable<BL.ISkillEffectListUnit>>) (x => BattleFuncs.getForceUnits(x, isAI, true, !nonFacility))).Select<BL.ISkillEffectListUnit, BL.UnitPosition>((Func<BL.ISkillEffectListUnit, BL.UnitPosition>) (x => BattleFuncs.iSkillEffectListUnitToUnitPosition(x))).Where<BL.UnitPosition>((Func<BL.UnitPosition, bool>) (u => range[0] == 0 || u != up)) : (IEnumerable<BL.UnitPosition>) BattleFuncs.getTargets(up.row, up.column, range, forceIds, BL.Unit.TargetAttribute.all, isAI, false, false, nonFacility, (List<BL.Unit>) null);
    }

    private static IEnumerable<BL.SkillEffect> GetEnabledFieldDamageFluctuateBuffDebuff(
      BL.SkillEffectList self,
      BattleskillEffectLogicEnum e,
      BL.ISkillEffectListUnit unit,
      BL.ISkillEffectListUnit target,
      int range)
    {
      return self.WhereAndGroupBy(e, (Func<IEnumerable<BL.SkillEffect>, BL.SkillEffect>) (x => x.OrderBy<BL.SkillEffect, int>((Func<BL.SkillEffect, int>) (effect => Mathf.Abs(range - effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.range)))).ThenBy<BL.SkillEffect, int>((Func<BL.SkillEffect, int>) (effect => effect.effectId)).FirstOrDefault<BL.SkillEffect>()), (Func<IGrouping<int, BL.SkillEffect>, IEnumerable<BL.SkillEffect>>) (skills => skills.Where<BL.SkillEffect>((Func<BL.SkillEffect, bool>) (effect =>
      {
        if (BattleFuncs.isSealedSkillEffect(unit, effect) || effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.unit_id) != 0 && effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.unit_id) != unit.originalUnit.unit.ID || (effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.target_gear_kind_id) != 0 && effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.target_gear_kind_id) != target.originalUnit.unit.kind.ID || effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.target_element) != 0 && (CommonElement) effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.target_element) != target.originalUnit.playerUnit.GetElement()) || (effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.target_job_id) != 0 && effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.target_job_id) != target.originalUnit.job.ID || effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.target_family_id) != 0 && !target.originalUnit.playerUnit.HasFamily((UnitFamily) effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.target_family_id)) || effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.target_character_id) != 0 && effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.target_character_id) != target.originalUnit.unit.character.ID))
          return false;
        return effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.target_unit_id) == 0 || effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.target_unit_id) == target.originalUnit.unit.ID;
      }))));
    }

    private static bool CheckEnabledFieldDamageFluctuateBuffDebuff(
      BattleskillEffect effect,
      BL.ISkillEffectListUnit unit)
    {
      return effect.GetInt(BattleskillEffectLogicArgumentEnum.unit_id) == 0 || effect.GetInt(BattleskillEffectLogicArgumentEnum.unit_id) == unit.originalUnit.unit.ID;
    }

    private static void GetFieldDamageFluctuateSkillAdd(
      List<BattleFuncs.SkillParam> skillParams,
      BL.ISkillEffectListUnit beUnit,
      BL.ISkillEffectListUnit beTarget,
      int range)
    {
      foreach (BL.SkillEffect effect in BattleFuncs.GetEnabledFieldDamageFluctuateBuffDebuff(beUnit.skillEffects, BattleskillEffectLogicEnum.after_duel_damage_fluctuate_fix, beUnit, beTarget, range))
        skillParams.Add(BattleFuncs.SkillParam.CreateAdd(beUnit.originalUnit, effect, (float) (effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.damage_value) + effect.baseSkillLevel * effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.skill_ratio)), (object) null, 0));
    }

    private static void GetFieldDamageFluctuateSkillMul(
      List<BattleFuncs.SkillParam> skillParams,
      BL.ISkillEffectListUnit beUnit,
      BL.ISkillEffectListUnit beTarget,
      int range)
    {
      foreach (BL.SkillEffect effect in BattleFuncs.GetEnabledFieldDamageFluctuateBuffDebuff(beUnit.skillEffects, BattleskillEffectLogicEnum.after_duel_damage_fluctuate_ratio, beUnit, beTarget, range))
        skillParams.Add(BattleFuncs.SkillParam.CreateMul(beUnit.originalUnit, effect, (float) ((Decimal) effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.damage_percentage) + (Decimal) effect.baseSkillLevel * (Decimal) effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.skill_ratio)), (object) null, 0));
    }

    public static int applyFieldDamageFluctuate(
      BL.ISkillEffectListUnit atk,
      BL.ISkillEffectListUnit def,
      BL.ISkillEffectListUnit invokeUnit,
      BL.ISkillEffectListUnit damageUnit,
      BattleskillEffect effect,
      int damage)
    {
      return BattleFuncs.applyFieldDamageFluctuate(atk, def, invokeUnit, damageUnit, effect.GetInt(BattleskillEffectLogicArgumentEnum.is_range_from_enemy) == 0, damage);
    }

    public static int applyFieldDamageFluctuate(
      BL.ISkillEffectListUnit atk,
      BL.ISkillEffectListUnit def,
      BL.ISkillEffectListUnit invokeUnit,
      BL.ISkillEffectListUnit damageUnit,
      bool isRangeFromEnemy,
      int damage)
    {
      if (damage < 0)
        return damage;
      BL.ISkillEffectListUnit beTarget = BattleFuncs.env.getForceID(atk.originalUnit) != BattleFuncs.env.getForceID(damageUnit.originalUnit) ? atk : def;
      BL.ISkillEffectListUnit unit = isRangeFromEnemy ? (atk == invokeUnit ? def : atk) : invokeUnit;
      BL.UnitPosition unitPosition = BattleFuncs.iSkillEffectListUnitToUnitPosition(damageUnit);
      int range = BL.fieldDistance(unitPosition, BattleFuncs.iSkillEffectListUnitToUnitPosition(unit));
      List<BattleFuncs.SkillParam> skillParams = new List<BattleFuncs.SkillParam>();
      BattleFuncs.GetFieldDamageFluctuateSkillAdd(skillParams, damageUnit, beTarget, range);
      BattleFuncs.GetFieldDamageFluctuateSkillMul(skillParams, damageUnit, beTarget, range);
      damage = Mathf.FloorToInt((float) ((Decimal) damage * (Decimal) BattleFuncs.calcSkillParamMul(skillParams, 1f))) + BattleFuncs.calcSkillParamAdd(skillParams);
      if (damage < 0)
        damage = 0;
      damage = BattleFuncs.applyDamageCut(1, damage, damageUnit, beTarget, (Judgement.BeforeDuelUnitParameter) null, (Judgement.BeforeDuelUnitParameter) null, (AttackStatus) null, (AttackStatus) null, (XorShift) null, 0, 0, new int?(), BattleFuncs.getPanel(unitPosition.row, unitPosition.column));
      return damage;
    }

    private static IEnumerable<BL.SkillEffect> GetEnabledDamageCutBuffDebuff(
      BL.SkillEffectList self,
      BattleskillEffectLogicEnum e,
      BL.ISkillEffectListUnit unit,
      BL.ISkillEffectListUnit target,
      BL.Panel panel)
    {
      return self.Where(e, (Func<BL.SkillEffect, bool>) (effect =>
      {
        BattleskillEffect effect1 = effect.effect;
        if (BattleFuncs.isSealedSkillEffect(unit, effect) || target != null && effect1.HasKey(BattleskillEffectLogicArgumentEnum.target_element) && (effect1.GetInt(BattleskillEffectLogicArgumentEnum.target_element) != 0 && (CommonElement) effect1.GetInt(BattleskillEffectLogicArgumentEnum.target_element) != target.originalUnit.playerUnit.GetElement()) || (target != null && effect1.HasKey(BattleskillEffectLogicArgumentEnum.target_gear_kind_id) && (effect1.GetInt(BattleskillEffectLogicArgumentEnum.target_gear_kind_id) != 0 && effect1.GetInt(BattleskillEffectLogicArgumentEnum.target_gear_kind_id) != target.originalUnit.unit.kind.ID) || target != null && effect1.HasKey(BattleskillEffectLogicArgumentEnum.target_family_id) && (effect1.GetInt(BattleskillEffectLogicArgumentEnum.target_family_id) != 0 && !target.originalUnit.playerUnit.HasFamily((UnitFamily) effect1.GetInt(BattleskillEffectLogicArgumentEnum.target_family_id)))) || (!BattleFuncs.PackedSkillEffect.Create(effect).CheckLandTag(panel != null ? panel.landform : (BattleLandform) null) || target != null && BattleFuncs.isEffectEnemyRangeAndInvalid(effect, unit, target)))
          return false;
        return target == null || !BattleFuncs.isSkillsAndEffectsInvalid(unit, target, (BL.SkillEffect) null);
      }));
    }

    private static bool CheckEnabledDamageCutBuffDebuff(
      BattleskillEffect effect,
      BL.ISkillEffectListUnit unit)
    {
      return true;
    }

    public static int applyDamageCut(
      int type,
      int damage,
      BL.ISkillEffectListUnit beUnit,
      BL.ISkillEffectListUnit beTarget = null,
      Judgement.BeforeDuelUnitParameter invokeParameter = null,
      Judgement.BeforeDuelUnitParameter targetParameter = null,
      AttackStatus invokeAS = null,
      AttackStatus targetAS = null,
      XorShift random = null,
      int invokeHp = 0,
      int targetHp = 0,
      int? colosseumTurn = null,
      BL.Panel invokePanel = null)
    {
      if (damage <= 0)
        return damage;
      bool checkInvoke = type == 0;
      Func<BL.SkillEffect, bool> func = (Func<BL.SkillEffect, bool>) (effect => checkInvoke && !BattleFuncs.isInvoke(beUnit, beTarget, invokeParameter, targetParameter, invokeAS, targetAS, effect.baseSkillLevel, effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.percentage_invocation), random, false, invokeHp, targetHp, colosseumTurn, new float?(), (BattleskillEffect) null, effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.base_invocation), effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.invocation_skill_ratio), effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.invocation_luck_ratio), (List<BattleFuncs.InvalidSpecificSkillLogic>) null));
      List<BattleFuncs.SkillParam> skillParams1 = new List<BattleFuncs.SkillParam>();
      foreach (BL.SkillEffect effect in BattleFuncs.GetEnabledDamageCutBuffDebuff(beUnit.skillEffects, BattleFuncs.DamageCutFixEnum[type], beUnit, beTarget, invokePanel))
      {
        int num = effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.border_value) + effect.baseSkillLevel * effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.skill_ratio);
        skillParams1.Add(BattleFuncs.SkillParam.CreateParam(beUnit.originalUnit, effect, (object) num, 0));
      }
      foreach (BL.SkillEffect effect in BattleFuncs.GetEnabledDamageCutBuffDebuff(beUnit.skillEffects, BattleFuncs.DamageCutRatioEnum[type], beUnit, beTarget, invokePanel))
      {
        float num1 = effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.border_percentage) + (float) effect.baseSkillLevel * effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.skill_ratio);
        int num2 = Mathf.CeilToInt((float) beUnit.originalUnit.parameter.Hp * num1);
        skillParams1.Add(BattleFuncs.SkillParam.CreateParam(beUnit.originalUnit, effect, (object) num2, 0));
      }
      int num3 = int.MinValue;
      foreach (BattleFuncs.SkillParam skillParam in BattleFuncs.gearSkillParamFilter(skillParams1))
      {
        if (!func(skillParam.effect))
        {
          int num1 = (int) skillParam.param;
          if (num1 > num3)
            num3 = num1;
        }
      }
      if (num3 != int.MinValue && damage < num3)
        damage = 0;
      List<BattleFuncs.SkillParam> skillParams2 = new List<BattleFuncs.SkillParam>();
      foreach (BL.SkillEffect effect in BattleFuncs.GetEnabledDamageCutBuffDebuff(beUnit.skillEffects, BattleFuncs.DamageCut2FixEnum[type], beUnit, beTarget, invokePanel))
      {
        if (!func(effect))
          skillParams2.Add(BattleFuncs.SkillParam.CreateAdd(beUnit.originalUnit, effect, (float) (effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.border_value) + effect.baseSkillLevel * effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.skill_ratio)), (object) null, 0));
      }
      foreach (BL.SkillEffect effect in BattleFuncs.GetEnabledDamageCutBuffDebuff(beUnit.skillEffects, BattleFuncs.DamageCut2RatioEnum[type], beUnit, beTarget, invokePanel))
      {
        if (!func(effect))
        {
          float num1 = effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.border_percentage) + (float) effect.baseSkillLevel * effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.skill_ratio);
          skillParams2.Add(BattleFuncs.SkillParam.CreateAdd(beUnit.originalUnit, effect, (float) Mathf.CeilToInt((float) beUnit.originalUnit.parameter.Hp * num1), (object) null, 0));
        }
      }
      damage -= BattleFuncs.calcSkillParamAdd(skillParams2);
      if (damage < 0)
        damage = 0;
      return damage;
    }

    public static List<Tuple<int, int, int>> getPenetratePosition(
      int myRow,
      int myColumn,
      int tgtRow,
      int tgtColumn,
      int penetrateCount,
      bool getAllPath = false)
    {
      List<Tuple<int, int, int>> res = new List<Tuple<int, int, int>>();
      int num1 = tgtColumn - myColumn;
      int num2 = tgtRow - myRow;
      int r = penetrateCount;
      int num3 = Mathf.Abs(num1);
      int num4 = Mathf.Abs(num2);
      int distance = num3 + num4;
      if (distance == 0)
        return res;
      int num5 = num1 < 0 ? -1 : 1;
      int num6 = num2 < 0 ? -1 : 1;
      int x = 0;
      int y = 0;
      bool flag = false;
      List<Tuple<int, int>> paths = getAllPath ? new List<Tuple<int, int>>() : (List<Tuple<int, int>>) null;
      System.Action action = (System.Action) (() =>
      {
        if (Mathf.Abs(x) + Mathf.Abs(y) == distance)
        {
          flag = true;
          if (!getAllPath)
            return;
          foreach (var data in paths.Select((p, index) => new
          {
            row = p.Item1,
            column = p.Item2,
            index = index
          }))
            res.Add(new Tuple<int, int, int>(data.row, data.column, data.index - paths.Count));
          res.Add(new Tuple<int, int, int>(tgtRow, tgtColumn, 0));
        }
        else if (!flag)
        {
          if (!getAllPath)
            return;
          paths.Add(Tuple.Create<int, int>(y + myRow, x + myColumn));
        }
        else
        {
          --r;
          res.Add(new Tuple<int, int, int>(y + myRow, x + myColumn, penetrateCount - r));
        }
      });
      if (num3 >= num4)
      {
        int num7 = -num3;
        while (!flag || r > 0)
        {
          x += num5;
          num7 += 2 * num4;
          if (num7 >= 0)
          {
            y += num6;
            num7 -= 2 * num3;
          }
          action();
        }
      }
      else
      {
        int num7 = -num4;
        while (!flag || r > 0)
        {
          y += num6;
          num7 += 2 * num3;
          if (num7 >= 0)
          {
            x += num5;
            num7 -= 2 * num4;
          }
          action();
        }
      }
      return res;
    }

    public static IEnumerable<BattleFuncs.PackedSkillEffect> CreatePackedSkillEffects(
      BattleskillSkill skill,
      int level)
    {
      BattleskillEffect[] battleskillEffectArray = skill.Effects;
      for (int index = 0; index < battleskillEffectArray.Length; ++index)
      {
        BattleskillEffect headerEffect = battleskillEffectArray[index];
        if (!headerEffect.EffectLogic.HasTag(BattleskillEffectTag.ext_arg) && headerEffect.checkLevel(level))
          yield return BattleFuncs.PackedSkillEffect.Create(headerEffect);
      }
      battleskillEffectArray = (BattleskillEffect[]) null;
    }

    public static int calcEquippedGearWeight(
      GearGear initialGear,
      PlayerItem equippedGear,
      PlayerItem equippedGear2)
    {
      if (equippedGear2 != (PlayerItem) null && equippedGear2.gear == null)
        equippedGear2 = (PlayerItem) null;
      if (equippedGear == (PlayerItem) null && equippedGear2 == (PlayerItem) null)
        return initialGear == null ? 0 : initialGear.weight;
      if (equippedGear != (PlayerItem) null && equippedGear2 == (PlayerItem) null)
        return equippedGear.gear.weight;
      return equippedGear == (PlayerItem) null && equippedGear2 != (PlayerItem) null ? equippedGear2.gear.weight : equippedGear.gear.weight + equippedGear2.gear.weight;
    }

    public static bool isGearEquipped(PlayerUnit playerUnit, int kindId)
    {
      if (kindId == 0 || kindId == playerUnit.equippedGearOrInitial.kind_GearKind)
        return true;
      return playerUnit.equippedGear2 != (PlayerItem) null && kindId == playerUnit.equippedGear2.gear.kind_GearKind;
    }

    public static bool isGearModelEquipped(PlayerUnit playerUnit, int kindId)
    {
      if (kindId == 0 || kindId == playerUnit.equippedGearOrInitial.model_kind_GearModelKind)
        return true;
      return playerUnit.equippedGear2 != (PlayerItem) null && kindId == playerUnit.equippedGear2.gear.model_kind_GearModelKind;
    }

    public static bool isBonusSkillId(int skillId)
    {
      return skillId >= 900100000 && skillId <= 913999999;
    }

    public static BL.Unit getEffectUnit(BL.SkillEffect effect, BL.Unit hasUnit)
    {
      if (effect.unit != (BL.Unit) null)
      {
        BattleskillSkill skill = effect.effect.skill;
        if (skill.skill_type == BattleskillSkillType.leader || skill.skill_type == BattleskillSkillType.passive && skill.range_effect_passive_skill)
          return effect.unit;
      }
      return effect.parentUnit != (BL.Unit) null ? effect.parentUnit : hasUnit;
    }

    private static int getSkillParamAdd(
      BattleFuncs.SkillParam sp,
      BattleFuncs.BuffDebuffSwapState swapState)
    {
      int num = (int) sp.addParam.Value;
      if (swapState == null || num == 0)
        return num;
      return num > 0 ? (!swapState.isBuffSwap(sp.effect) ? num : -num) : (!swapState.isDebuffSwap(sp.effect) ? num : -num);
    }

    private static float getSkillParamMul(
      BattleFuncs.SkillParam sp,
      BattleFuncs.BuffDebuffSwapState swapState)
    {
      float num = sp.mulParam.Value;
      if (swapState == null || (double) num == 1.0)
        return num;
      if ((double) num > 1.0)
        return !swapState.isBuffSwap(sp.effect) ? num : (float) (new Decimal(10, 0, 0, false, (byte) 1) / (Decimal) num);
      if (!swapState.isDebuffSwap(sp.effect))
        return num;
      if ((double) num < 0.100000001490116)
        num = 0.1f;
      return (float) (new Decimal(10, 0, 0, false, (byte) 1) / (Decimal) num);
    }

    private static IEnumerable<BattleFuncs.SkillParam> pileSkillParamFilter(
      IEnumerable<BattleFuncs.SkillParam> skillParams,
      double baseParam,
      float baseMul,
      float extraAdd)
    {
      bool exist123 = false;
      bool exist4 = false;
      foreach (BattleFuncs.SkillParam skillParam in skillParams)
      {
        if (skillParam.param == null)
          yield return skillParam;
        else if ((int) skillParam.param == 4)
          exist4 = true;
        else
          exist123 = true;
      }
      BattleFuncs.SkillParam minSp;
      if (exist4)
      {
        double num1;
        double num2 = num1 = baseParam * 10000.0 * (double) baseMul / 10000.0 + (double) extraAdd;
        BattleFuncs.SkillParam skillParam1 = (BattleFuncs.SkillParam) null;
        minSp = (BattleFuncs.SkillParam) null;
        foreach (BattleFuncs.SkillParam skillParam2 in skillParams.Where<BattleFuncs.SkillParam>((Func<BattleFuncs.SkillParam, bool>) (x => x.param != null && (int) x.param == 4)))
        {
          float num3 = 1f;
          int num4 = 0;
          if (skillParam2.addParam.HasValue)
            num4 += (int) skillParam2.addParam.Value;
          else if (skillParam2.mulParam.HasValue)
            num3 *= skillParam2.mulParam.Value;
          double num5 = baseParam * 10000.0 * (double) baseMul * (double) num3 / 10000.0 + (double) num4 + (double) extraAdd;
          if (num5 > num1)
          {
            skillParam1 = skillParam2;
            num1 = num5;
          }
          else if (num5 < num2)
          {
            minSp = skillParam2;
            num2 = num5;
          }
        }
        if (skillParam1 != null)
          yield return skillParam1;
        if (minSp != null)
          yield return minSp;
        minSp = (BattleFuncs.SkillParam) null;
      }
      if (exist123)
      {
        foreach (IGrouping<BattleskillSkillType, BattleFuncs.SkillParam> grouping in skillParams.Where<BattleFuncs.SkillParam>((Func<BattleFuncs.SkillParam, bool>) (x => x.param != null && (int) x.param != 4)).GroupBy<BattleFuncs.SkillParam, BattleskillSkillType>((Func<BattleFuncs.SkillParam, BattleskillSkillType>) (x => x.effect.baseSkill.skill_type)))
        {
          IGrouping<BattleskillSkillType, BattleFuncs.SkillParam> spGroup = grouping;
          float num1 = 1f;
          int num2 = 0;
          float num3 = 1f;
          int num4 = 0;
          foreach (BattleFuncs.SkillParam skillParam in (IEnumerable<BattleFuncs.SkillParam>) spGroup)
          {
            if ((int) skillParam.param != 3)
            {
              if (skillParam.addParam.HasValue)
              {
                int num5 = (int) skillParam.addParam.Value;
                if (num5 > 0)
                  num4 += num5;
                else if (num5 < 0)
                  num2 += num5;
              }
              else if (skillParam.mulParam.HasValue)
              {
                float num5 = skillParam.mulParam.Value;
                if ((double) num5 > 1.0)
                  num3 *= num5;
                else if ((double) num5 < 1.0)
                  num1 *= num5;
              }
            }
          }
          double num6 = baseParam * 10000.0 * (double) baseMul * (double) num1 / 10000.0 + (double) num2 + (double) extraAdd;
          double num7 = baseParam * 10000.0 * (double) baseMul * (double) num3 / 10000.0 + (double) num4 + (double) extraAdd;
          BattleFuncs.SkillParam skillParam1 = (BattleFuncs.SkillParam) null;
          minSp = (BattleFuncs.SkillParam) null;
          foreach (BattleFuncs.SkillParam skillParam2 in (IEnumerable<BattleFuncs.SkillParam>) spGroup)
          {
            if ((int) skillParam2.param == 3)
            {
              float num5 = 1f;
              int num8 = 0;
              if (skillParam2.addParam.HasValue)
                num8 += (int) skillParam2.addParam.Value;
              else if (skillParam2.mulParam.HasValue)
                num5 *= skillParam2.mulParam.Value;
              double num9 = baseParam * 10000.0 * (double) baseMul * (double) num5 / 10000.0 + (double) num8 + (double) extraAdd;
              if (num9 > num7)
              {
                skillParam1 = skillParam2;
                num7 = num9;
              }
              else if (num9 < num6)
              {
                minSp = skillParam2;
                num6 = num9;
              }
            }
          }
          if (skillParam1 != null)
          {
            yield return skillParam1;
          }
          else
          {
            foreach (BattleFuncs.SkillParam skillParam2 in (IEnumerable<BattleFuncs.SkillParam>) spGroup)
            {
              if ((int) skillParam2.param != 3)
              {
                if (skillParam2.addParam.HasValue)
                {
                  if ((int) skillParam2.addParam.Value > 0)
                    yield return skillParam2;
                }
                else if (skillParam2.mulParam.HasValue && (double) skillParam2.mulParam.Value > 1.0)
                  yield return skillParam2;
              }
            }
          }
          if (minSp != null)
          {
            yield return minSp;
          }
          else
          {
            foreach (BattleFuncs.SkillParam skillParam2 in (IEnumerable<BattleFuncs.SkillParam>) spGroup)
            {
              if ((int) skillParam2.param != 3)
              {
                if (skillParam2.addParam.HasValue)
                {
                  if ((int) skillParam2.addParam.Value < 0)
                    yield return skillParam2;
                }
                else if (skillParam2.mulParam.HasValue && (double) skillParam2.mulParam.Value < 1.0)
                  yield return skillParam2;
              }
            }
          }
          minSp = (BattleFuncs.SkillParam) null;
          spGroup = (IGrouping<BattleskillSkillType, BattleFuncs.SkillParam>) null;
        }
      }
    }

    private static double calcSkillParamSub(
      IEnumerable<BattleFuncs.SkillParam> skillParams,
      double baseParam,
      float baseMul,
      float extraAdd,
      BattleFuncs.BuffDebuffSwapState swapState = null)
    {
      Decimal num1 = (Decimal) baseMul;
      int num2 = 0;
      List<BattleFuncs.SkillParam> source1 = (List<BattleFuncs.SkillParam>) null;
      foreach (BattleFuncs.SkillParam skillParam in skillParams)
      {
        if (skillParam.effect.gearIndex == 0)
        {
          if (skillParam.addParam.HasValue)
            num2 += BattleFuncs.getSkillParamAdd(skillParam, swapState);
          if (skillParam.mulParam.HasValue)
            num1 *= (Decimal) BattleFuncs.getSkillParamMul(skillParam, swapState);
        }
        else
        {
          if (source1 == null)
            source1 = new List<BattleFuncs.SkillParam>();
          source1.Add(skillParam);
        }
      }
      if (source1 != null)
      {
        foreach (IGrouping<\u003C\u003Ef__AnonymousType2<int, bool>, BattleFuncs.SkillParam> source2 in source1.GroupBy(x => new
        {
          index = x.effectUnit.index,
          isPlayerForce = x.effectUnit.isPlayerForce
        }))
        {
          Decimal num3 = (Decimal) baseParam * (Decimal) baseMul + (Decimal) extraAdd;
          Decimal num4 = new Decimal(-1, -1, -1, false, (byte) 0);
          Decimal num5 = new Decimal(10, 0, 0, false, (byte) 1);
          int num6 = 0;
          Decimal num7 = new Decimal(-1, -1, -1, true, (byte) 0);
          Decimal num8 = new Decimal(10, 0, 0, false, (byte) 1);
          int num9 = 0;
          foreach (IGrouping<int, BattleFuncs.SkillParam> grouping in source2.GroupBy<BattleFuncs.SkillParam, int>((Func<BattleFuncs.SkillParam, int>) (x => x.effect.gearIndex)))
          {
            Decimal num10 = new Decimal(10, 0, 0, false, (byte) 1);
            int num11 = 0;
            foreach (BattleFuncs.SkillParam sp in (IEnumerable<BattleFuncs.SkillParam>) grouping)
            {
              if (sp.addParam.HasValue)
                num11 += BattleFuncs.getSkillParamAdd(sp, swapState);
              if (sp.mulParam.HasValue)
                num10 *= (Decimal) BattleFuncs.getSkillParamMul(sp, swapState);
            }
            Decimal num12 = (Decimal) baseParam * (Decimal) baseMul * num10 + (Decimal) num11 + (Decimal) extraAdd;
            if (num12 > num3)
            {
              if (num12 > num7)
              {
                num7 = num12;
                num8 = num10;
                num9 = num11;
              }
            }
            else if (num12 < num3 && num12 < num4)
            {
              num4 = num12;
              num5 = num10;
              num6 = num11;
            }
          }
          num1 *= num5 * num8;
          num2 += num6 + num9;
        }
      }
      return (double) ((Decimal) baseParam * num1 + (Decimal) num2 + (Decimal) extraAdd);
    }

    public static double calcSkillParam(
      IEnumerable<BattleFuncs.SkillParam> skillParams,
      double baseParam,
      float baseMul = 1f,
      float extraAdd = 0.0f,
      BattleFuncs.BuffDebuffSwapState swapState = null)
    {
      return BattleFuncs.calcSkillParamSub(BattleFuncs.pileSkillParamFilter(skillParams, baseParam, baseMul, extraAdd), baseParam, baseMul, extraAdd, swapState);
    }

    public static Tuple<float, float> calcSkillParam2(
      List<BattleFuncs.SkillParam> skillParams,
      float baseParam,
      float baseMul,
      float extraAdd,
      List<BattleFuncs.SkillParam> skillParams2,
      float baseParam2,
      float baseMul2,
      float extraAdd2,
      BattleFuncs.BuffDebuffSwapState swapState = null)
    {
      BattleFuncs.SkillParam[] array1 = BattleFuncs.pileSkillParamFilter((IEnumerable<BattleFuncs.SkillParam>) skillParams, (double) baseParam, baseMul, extraAdd).ToArray<BattleFuncs.SkillParam>();
      float num1 = (float) BattleFuncs.calcSkillParamSub((IEnumerable<BattleFuncs.SkillParam>) array1, (double) baseParam, baseMul, extraAdd, swapState);
      BattleFuncs.SkillParam[] array2 = BattleFuncs.pileSkillParamFilter((IEnumerable<BattleFuncs.SkillParam>) skillParams2, (double) (int) num1 + (double) baseParam2, baseMul2, extraAdd2).ToArray<BattleFuncs.SkillParam>();
      BattleFuncs.SkillParam[] array3 = ((IEnumerable<BattleFuncs.SkillParam>) array1).Where<BattleFuncs.SkillParam>((Func<BattleFuncs.SkillParam, bool>) (x => (uint) x.effect.gearIndex > 0U)).ToArray<BattleFuncs.SkillParam>();
      BattleFuncs.SkillParam[] array4 = ((IEnumerable<BattleFuncs.SkillParam>) array2).Where<BattleFuncs.SkillParam>((Func<BattleFuncs.SkillParam, bool>) (x => (uint) x.effect.gearIndex > 0U)).ToArray<BattleFuncs.SkillParam>();
      float num2;
      float num3;
      if (array3.Length == 0 && array4.Length == 0)
      {
        num2 = num1;
        num3 = (float) BattleFuncs.calcSkillParamSub((IEnumerable<BattleFuncs.SkillParam>) array2, (double) (int) num2 + (double) baseParam2, baseMul2, extraAdd2, swapState);
      }
      else
      {
        Decimal num4 = (Decimal) baseMul;
        int num5 = 0;
        Decimal num6 = (Decimal) baseMul2;
        int num7 = 0;
        foreach (BattleFuncs.SkillParam sp in ((IEnumerable<BattleFuncs.SkillParam>) array1).Where<BattleFuncs.SkillParam>((Func<BattleFuncs.SkillParam, bool>) (x => x.effect.gearIndex == 0)))
        {
          if (sp.addParam.HasValue)
            num5 += BattleFuncs.getSkillParamAdd(sp, swapState);
          if (sp.mulParam.HasValue)
            num4 *= (Decimal) BattleFuncs.getSkillParamMul(sp, swapState);
        }
        foreach (BattleFuncs.SkillParam sp in ((IEnumerable<BattleFuncs.SkillParam>) array2).Where<BattleFuncs.SkillParam>((Func<BattleFuncs.SkillParam, bool>) (x => x.effect.gearIndex == 0)))
        {
          if (sp.addParam.HasValue)
            num7 += BattleFuncs.getSkillParamAdd(sp, swapState);
          if (sp.mulParam.HasValue)
            num6 *= (Decimal) BattleFuncs.getSkillParamMul(sp, swapState);
        }
        Decimal num8 = ((Decimal) (int) ((Decimal) baseParam * (Decimal) baseMul + (Decimal) extraAdd) + (Decimal) baseParam2) * (Decimal) baseMul2 + (Decimal) extraAdd2;
        foreach (var data in ((IEnumerable<BattleFuncs.SkillParam>) array3).Concat<BattleFuncs.SkillParam>((IEnumerable<BattleFuncs.SkillParam>) array4).Select(x => new
        {
          index = x.effectUnit.index,
          isPlayerForce = x.effectUnit.isPlayerForce
        }).Distinct())
        {
          var unitKey = data;
          BattleFuncs.SkillParam[] array5 = ((IEnumerable<BattleFuncs.SkillParam>) array3).Where<BattleFuncs.SkillParam>((Func<BattleFuncs.SkillParam, bool>) (x => new
          {
            index = x.effectUnit.index,
            isPlayerForce = x.effectUnit.isPlayerForce
          }.Equals((object) unitKey))).ToArray<BattleFuncs.SkillParam>();
          BattleFuncs.SkillParam[] array6 = ((IEnumerable<BattleFuncs.SkillParam>) array4).Where<BattleFuncs.SkillParam>((Func<BattleFuncs.SkillParam, bool>) (x => new
          {
            index = x.effectUnit.index,
            isPlayerForce = x.effectUnit.isPlayerForce
          }.Equals((object) unitKey))).ToArray<BattleFuncs.SkillParam>();
          IEnumerable<int> ints = ((IEnumerable<BattleFuncs.SkillParam>) array5).Concat<BattleFuncs.SkillParam>((IEnumerable<BattleFuncs.SkillParam>) array6).Select<BattleFuncs.SkillParam, int>((Func<BattleFuncs.SkillParam, int>) (x => x.effect.gearIndex)).Distinct<int>();
          Decimal num9 = new Decimal(-1, -1, -1, false, (byte) 0);
          Decimal num10 = new Decimal(10, 0, 0, false, (byte) 1);
          int num11 = 0;
          Decimal num12 = new Decimal(10, 0, 0, false, (byte) 1);
          int num13 = 0;
          Decimal num14 = new Decimal(-1, -1, -1, true, (byte) 0);
          Decimal num15 = new Decimal(10, 0, 0, false, (byte) 1);
          int num16 = 0;
          Decimal num17 = new Decimal(10, 0, 0, false, (byte) 1);
          int num18 = 0;
          foreach (int num19 in ints)
          {
            int gearIndex = num19;
            BattleFuncs.SkillParam[] array7 = ((IEnumerable<BattleFuncs.SkillParam>) array5).Where<BattleFuncs.SkillParam>((Func<BattleFuncs.SkillParam, bool>) (x => x.effect.gearIndex == gearIndex)).ToArray<BattleFuncs.SkillParam>();
            BattleFuncs.SkillParam[] array8 = ((IEnumerable<BattleFuncs.SkillParam>) array6).Where<BattleFuncs.SkillParam>((Func<BattleFuncs.SkillParam, bool>) (x => x.effect.gearIndex == gearIndex)).ToArray<BattleFuncs.SkillParam>();
            Decimal num20 = new Decimal(10, 0, 0, false, (byte) 1);
            int num21 = 0;
            foreach (BattleFuncs.SkillParam sp in array7)
            {
              if (sp.addParam.HasValue)
                num21 += BattleFuncs.getSkillParamAdd(sp, swapState);
              if (sp.mulParam.HasValue)
                num20 *= (Decimal) BattleFuncs.getSkillParamMul(sp, swapState);
            }
            Decimal num22 = new Decimal(10, 0, 0, false, (byte) 1);
            int num23 = 0;
            foreach (BattleFuncs.SkillParam sp in array8)
            {
              if (sp.addParam.HasValue)
                num23 += BattleFuncs.getSkillParamAdd(sp, swapState);
              if (sp.mulParam.HasValue)
                num22 *= (Decimal) BattleFuncs.getSkillParamMul(sp, swapState);
            }
            Decimal num24 = ((Decimal) (int) ((Decimal) baseParam * (Decimal) baseMul * num20 + (Decimal) num21 + (Decimal) extraAdd) + (Decimal) baseParam2) * (Decimal) baseMul2 * num22 + (Decimal) num23 + (Decimal) extraAdd2;
            if (num24 > num8)
            {
              if (num24 > num14)
              {
                num14 = num24;
                num15 = num20;
                num16 = num21;
                num17 = num22;
                num18 = num23;
              }
            }
            else if (num24 < num8 && num24 < num9)
            {
              num9 = num24;
              num10 = num20;
              num11 = num21;
              num17 = num22;
              num18 = num23;
            }
          }
          num4 *= num10 * num15;
          num5 += num11 + num16;
          num6 *= num12 * num17;
          num7 += num13 + num18;
        }
        num2 = (float) ((Decimal) baseParam * num4 + (Decimal) num5 + (Decimal) extraAdd);
        num3 = (float) (((Decimal) (int) num2 + (Decimal) baseParam2) * num6 + (Decimal) num7 + (Decimal) extraAdd2);
      }
      return Tuple.Create<float, float>(num2, num3);
    }

    public static float calcSkillParamMul(List<BattleFuncs.SkillParam> skillParams, float baseMul = 1f)
    {
      float num = 10000f * baseMul;
      foreach (BattleFuncs.SkillParam skillParam in BattleFuncs.gearSkillParamFilter(skillParams))
      {
        if (skillParam.mulParam.HasValue)
          num *= skillParam.mulParam.Value;
      }
      return num / 10000f;
    }

    public static int calcSkillParamAdd(List<BattleFuncs.SkillParam> skillParams)
    {
      int num = 0;
      foreach (BattleFuncs.SkillParam skillParam in BattleFuncs.gearSkillParamFilter(skillParams))
      {
        if (skillParam.addParam.HasValue)
          num += (int) skillParam.addParam.Value;
      }
      return num;
    }

    public static float calcSkillParamAddSingle(List<BattleFuncs.SkillParam> skillParams)
    {
      Decimal num = new Decimal();
      foreach (BattleFuncs.SkillParam skillParam in BattleFuncs.gearSkillParamFilter(skillParams))
      {
        if (skillParam.addParam.HasValue)
          num += (Decimal) skillParam.addParam.Value;
      }
      return (float) num;
    }

    public static IEnumerable<BL.SkillEffect> gearSkillEffectFilter(
      BL.Unit hasUnit,
      IEnumerable<BL.SkillEffect> skillEffects)
    {
      HashSet<Tuple<int, bool>> existGearOne = (HashSet<Tuple<int, bool>>) null;
      foreach (BL.SkillEffect skillEffect in skillEffects)
      {
        if (skillEffect.gearIndex == 0)
          yield return skillEffect;
        else if (skillEffect.gearIndex == 1)
        {
          if (existGearOne == null)
            existGearOne = new HashSet<Tuple<int, bool>>();
          BL.Unit effectUnit = BattleFuncs.getEffectUnit(skillEffect, hasUnit);
          existGearOne.Add(Tuple.Create<int, bool>(effectUnit.index, effectUnit.isPlayerForce));
          yield return skillEffect;
        }
        else
        {
          if (existGearOne == null)
            existGearOne = new HashSet<Tuple<int, bool>>();
          BL.Unit effectUnit1 = BattleFuncs.getEffectUnit(skillEffect, hasUnit);
          Tuple<int, bool> unitKey = Tuple.Create<int, bool>(effectUnit1.index, effectUnit1.isPlayerForce);
          if (!existGearOne.Contains(unitKey))
          {
            if (skillEffects.Any<BL.SkillEffect>((Func<BL.SkillEffect, bool>) (x =>
            {
              if (x.gearIndex != 1)
                return false;
              BL.Unit effectUnit = BattleFuncs.getEffectUnit(x, hasUnit);
              return Tuple.Create<int, bool>(effectUnit.index, effectUnit.isPlayerForce).Equals((object) unitKey);
            })))
              existGearOne.Add(unitKey);
            else
              yield return skillEffect;
          }
        }
      }
    }

    public static IEnumerable<BattleFuncs.SkillParam> gearSkillParamFilter(
      List<BattleFuncs.SkillParam> skillParams)
    {
      HashSet<Tuple<int, bool>> existGearOne = (HashSet<Tuple<int, bool>>) null;
      foreach (BattleFuncs.SkillParam skillParam in skillParams)
      {
        BL.SkillEffect effect = skillParam.effect;
        if (effect.gearIndex == 0)
          yield return skillParam;
        else if (effect.gearIndex == 1)
        {
          if (existGearOne == null)
            existGearOne = new HashSet<Tuple<int, bool>>();
          BL.Unit effectUnit = BattleFuncs.getEffectUnit(effect, skillParam.hasUnit);
          existGearOne.Add(Tuple.Create<int, bool>(effectUnit.index, effectUnit.isPlayerForce));
          yield return skillParam;
        }
        else
        {
          if (existGearOne == null)
            existGearOne = new HashSet<Tuple<int, bool>>();
          BL.Unit effectUnit = BattleFuncs.getEffectUnit(effect, skillParam.hasUnit);
          Tuple<int, bool> unitKey = Tuple.Create<int, bool>(effectUnit.index, effectUnit.isPlayerForce);
          if (!existGearOne.Contains(unitKey))
          {
            if (skillParams.Any<BattleFuncs.SkillParam>((Func<BattleFuncs.SkillParam, bool>) (x => x.effect.gearIndex == 1 && Tuple.Create<int, bool>(x.effectUnit.index, x.effectUnit.isPlayerForce).Equals((object) unitKey))))
              existGearOne.Add(unitKey);
            else
              yield return skillParam;
          }
        }
      }
    }

    public static IEnumerable<BattleFuncs.SkillParam> gearSkillParamCategoryFilter(
      List<BattleFuncs.SkillParam> skillParams)
    {
      Dictionary<int, HashSet<Tuple<int, bool>>> existGearOne = new Dictionary<int, HashSet<Tuple<int, bool>>>();
      foreach (BattleFuncs.SkillParam skillParam in skillParams)
      {
        BattleFuncs.SkillParam sp = skillParam;
        BL.SkillEffect effect = sp.effect;
        if (effect.gearIndex == 0)
          yield return sp;
        else if (effect.gearIndex == 1)
        {
          if (!existGearOne.ContainsKey(sp.category))
            existGearOne[sp.category] = new HashSet<Tuple<int, bool>>();
          BL.Unit effectUnit = BattleFuncs.getEffectUnit(effect, sp.hasUnit);
          existGearOne[sp.category].Add(Tuple.Create<int, bool>(effectUnit.index, effectUnit.isPlayerForce));
          yield return sp;
        }
        else
        {
          if (!existGearOne.ContainsKey(sp.category))
            existGearOne[sp.category] = new HashSet<Tuple<int, bool>>();
          BL.Unit effectUnit = BattleFuncs.getEffectUnit(effect, sp.hasUnit);
          Tuple<int, bool> unitKey = Tuple.Create<int, bool>(effectUnit.index, effectUnit.isPlayerForce);
          if (!existGearOne[sp.category].Contains(unitKey))
          {
            if (skillParams.Any<BattleFuncs.SkillParam>((Func<BattleFuncs.SkillParam, bool>) (x => x.effect.gearIndex == 1 && x.category == sp.category && Tuple.Create<int, bool>(x.effectUnit.index, x.effectUnit.isPlayerForce).Equals((object) unitKey))))
              existGearOne[sp.category].Add(unitKey);
            else
              yield return sp;
          }
        }
      }
    }

    public static bool isResetPositionOK(
      BL.Unit target,
      int row,
      int column,
      int moveCost,
      IEnumerable<BL.Unit> ignoreUnits,
      bool isAI = false)
    {
      BL.Panel fieldPanel = BattleFuncs.env.getFieldPanel(row, column);
      if (fieldPanel == null || !BattleFuncs.env.isMoveOKPanel(fieldPanel, target, false, moveCost))
        return false;
      BL.UnitPosition[] unitPositionArray = isAI ? BattleFuncs.getFieldUnitsAI(row, column) : BattleFuncs.env.getFieldUnits(row, column, false, false);
      if (unitPositionArray != null)
      {
        foreach (BL.UnitPosition unitPosition in unitPositionArray)
        {
          if (!ignoreUnits.Contains<BL.Unit>(unitPosition.unit) && !unitPosition.unit.isPutOn)
            return false;
        }
      }
      return true;
    }

    public static int[] getAttractDelta(BL.UnitPosition up, BL.UnitPosition tup)
    {
      int num1 = tup.row - up.row;
      int num2 = tup.column - up.column;
      int num3 = 0;
      int num4 = 0;
      if (num1 > 0 && num2 >= 0)
      {
        if (Mathf.Abs(num1) >= Mathf.Abs(num2))
          --num4;
        else
          --num3;
      }
      else if (num1 <= 0 && num2 > 0)
      {
        if (Mathf.Abs(num2) >= Mathf.Abs(num1))
          --num3;
        else
          ++num4;
      }
      else if (num1 < 0 && num2 <= 0)
      {
        if (Mathf.Abs(num1) >= Mathf.Abs(num2))
          ++num4;
        else
          ++num3;
      }
      else if (Mathf.Abs(num2) >= Mathf.Abs(num1))
        ++num3;
      else
        --num4;
      return new int[2]{ num4, num3 };
    }

    public static bool getShiftBreakPosition(
      BL.ISkillEffectListUnit useUnit,
      BL.ISkillEffectListUnit targetUnit,
      int range,
      out int posY,
      out int posX)
    {
      BL.UnitPosition unitPosition1 = BattleFuncs.iSkillEffectListUnitToUnitPosition(useUnit);
      BL.UnitPosition unitPosition2 = BattleFuncs.iSkillEffectListUnitToUnitPosition(targetUnit);
      int num1 = unitPosition1.row - unitPosition2.row;
      int num2 = unitPosition1.column - unitPosition2.column;
      int num3 = num1 <= 0 || num2 < 0 ? (num1 > 0 || num2 <= 0 ? (num1 >= 0 || num2 > 0 ? (Mathf.Abs(num2) < Mathf.Abs(num1) ? 0 : 3) : (Mathf.Abs(num1) < Mathf.Abs(num2) ? 3 : 2)) : (Mathf.Abs(num2) < Mathf.Abs(num1) ? 2 : 1)) : (Mathf.Abs(num1) < Mathf.Abs(num2) ? 1 : 0);
      int y = unitPosition2.row;
      int x = unitPosition2.column;
      switch (num3)
      {
        case 0:
          y += range;
          break;
        case 1:
          x += range;
          break;
        case 2:
          y -= range;
          break;
        default:
          x -= range;
          break;
      }
      BL.Unit[] ignoreUnits = new BL.Unit[1]
      {
        useUnit.originalUnit
      };
      int moveCost = unitPosition1.unit.parameter.Move;
      bool found = false;
      Func<bool> func = (Func<bool>) (() =>
      {
        if (!BattleFuncs.isResetPositionOK(useUnit.originalUnit, y, x, moveCost, (IEnumerable<BL.Unit>) ignoreUnits, false))
          return false;
        found = true;
        return true;
      });
      for (int index1 = 0; index1 < 4; ++index1)
      {
        switch (num3)
        {
          case 0:
            for (int index2 = 0; index2 < range && !func(); ++index2)
            {
              y--;
              x++;
            }
            break;
          case 1:
            for (int index2 = 0; index2 < range && !func(); ++index2)
            {
              y--;
              x--;
            }
            break;
          case 2:
            for (int index2 = 0; index2 < range && !func(); ++index2)
            {
              y++;
              x--;
            }
            break;
          case 3:
            for (int index2 = 0; index2 < range && !func(); ++index2)
            {
              y++;
              x++;
            }
            break;
        }
        if (!found)
          num3 = (num3 + 1) % 4;
        else
          break;
      }
      posY = y;
      posX = x;
      return found;
    }

    public static bool canUseImmediateSkillEffect(
      BattleskillEffect effect,
      BL.ISkillEffectListUnit unit,
      BL.ISkillEffectListUnit useUnit = null,
      BL.ISkillEffectListUnit firstTarget = null)
    {
      switch (effect.EffectLogic.Enum)
      {
        case BattleskillEffectLogicEnum.fix_heal:
        case BattleskillEffectLogicEnum.ratio_heal:
        case BattleskillEffectLogicEnum.fix_lv_heal:
        case BattleskillEffectLogicEnum.ratio_lv_heal:
          NGBattleManager instance = Singleton<NGBattleManager>.GetInstance();
          if (useUnit == null || !instance.isRaidBoss(unit.originalUnit) || BattleFuncs.env.getForceID(useUnit.originalUnit) == BattleFuncs.env.getForceID(unit.originalUnit))
            return true;
          break;
        case BattleskillEffectLogicEnum.invest_skilleffect_im:
          int index = effect.GetInt(BattleskillEffectLogicArgumentEnum.skill_id);
          if (index != 0 && MasterData.BattleskillSkill.ContainsKey(index))
          {
            if (!BattleFuncs.hasPerfectAilmentResist(BattleFuncs.getAilmentResistEffects(index, unit, false)))
            {
              BattleskillSkill battleskillSkill = MasterData.BattleskillSkill[index];
              List<BL.SkillEffect> rangeSkills = (List<BL.SkillEffect>) null;
              foreach (BattleskillEffect effect1 in battleskillSkill.Effects)
              {
                switch (effect1.EffectLogic.Enum)
                {
                  case BattleskillEffectLogicEnum.seal:
                    Func<int, bool> func1 = closure_0 ?? (closure_0 = (Func<int, bool>) (sid => ((IEnumerable<BL.Skill>) unit.originalUnit.duelSkills).Any<BL.Skill>((Func<BL.Skill, bool>) (x => (sid == 0 || x.skill.ID == sid) && ((IEnumerable<BattleskillEffect>) x.skill.Effects).Any<BattleskillEffect>((Func<BattleskillEffect, bool>) (y => y.EffectLogic.Enum != BattleskillEffectLogicEnum.invest_element && y.EffectLogic.Enum != BattleskillEffectLogicEnum.effect_element))))));
                    Func<int, int, int, int, bool> func2 = (Func<int, int, int, int, bool>) ((sid, stype, sinvest, slogic) =>
                    {
                      if (rangeSkills == null)
                        rangeSkills = BattleFuncs.getAllUnits(unit is BL.AIUnit, false, false).SelectMany<BL.ISkillEffectListUnit, BL.SkillEffect>(closure_2 ?? (closure_2 = (Func<BL.ISkillEffectListUnit, IEnumerable<BL.SkillEffect>>) (u => u.skillEffects.All().Where<BL.SkillEffect>((Func<BL.SkillEffect, bool>) (x => x.unit != (BL.Unit) null && (x.baseSkill.skill_type == BattleskillSkillType.leader || x.baseSkill.skill_type == BattleskillSkillType.passive && x.baseSkill.range_effect_passive_skill) && x.unit.index == unit.originalUnit.index && x.unit.isPlayerForce == unit.originalUnit.isPlayerForce))))).ToList<BL.SkillEffect>();
                      return unit.skillEffects.All().Concat<BL.SkillEffect>((IEnumerable<BL.SkillEffect>) rangeSkills).Any<BL.SkillEffect>((Func<BL.SkillEffect, bool>) (x =>
                      {
                        if (x.baseSkill.skill_type == BattleskillSkillType.ailment || x.unit != (BL.Unit) null && (x.baseSkill.skill_type == BattleskillSkillType.leader || x.baseSkill.skill_type == BattleskillSkillType.passive && x.baseSkill.range_effect_passive_skill) && (x.unit.index != unit.originalUnit.index || x.unit.isPlayerForce != unit.originalUnit.isPlayerForce) || (sid != 0 && x.baseSkillId != sid || stype != 0 && (BattleskillSkillType) stype != x.baseSkill.skill_type || sinvest != 0 && (sinvest != 1 || x.isBaseSkill) && (sinvest != 2 || !x.isBaseSkill)))
                          return false;
                        return slogic == 0 || slogic == x.effect.EffectLogic.ID;
                      }));
                    });
                    int num1 = effect1.HasKey(BattleskillEffectLogicArgumentEnum.invest_type) ? effect1.GetInt(BattleskillEffectLogicArgumentEnum.invest_type) : 0;
                    int num2 = effect1.GetInt(BattleskillEffectLogicArgumentEnum.skill_id);
                    int num3 = effect1.GetInt(BattleskillEffectLogicArgumentEnum.skill_type);
                    int num4 = effect1.HasKey(BattleskillEffectLogicArgumentEnum.logic_id) ? effect1.GetInt(BattleskillEffectLogicArgumentEnum.logic_id) : 0;
                    switch (num3)
                    {
                      case 0:
                        if (func2(num2, 1, 0, num4) || func2(num2, 2, 0, num4) || (func2(num2, 3, num1, num4) || func2(num2, 6, 0, num4)) || (func2(num2, 7, 0, num4) || num4 == 0 && func1(num2)))
                          return true;
                        continue;
                      case 1:
                      case 2:
                      case 3:
                      case 6:
                      case 7:
                        if (func2(num2, num3, num1, num4))
                          return true;
                        continue;
                      case 4:
                        if (func1(num2))
                          return true;
                        continue;
                      default:
                        continue;
                    }
                  case BattleskillEffectLogicEnum.do_not_use_command:
                    if (unit.skills.Length >= 1)
                    {
                      int sid = effect1.GetInt(BattleskillEffectLogicArgumentEnum.skill_id);
                      if (sid == 0 || ((IEnumerable<BL.Skill>) unit.skills).Any<BL.Skill>((Func<BL.Skill, bool>) (x => x.id == sid)))
                        return true;
                      break;
                    }
                    break;
                  case BattleskillEffectLogicEnum.do_not_use_ougi:
                    if (unit.hasOugi)
                    {
                      int num5 = effect1.GetInt(BattleskillEffectLogicArgumentEnum.skill_id);
                      if (num5 == 0 || unit.ougi.id == num5)
                        return true;
                      break;
                    }
                    break;
                  default:
                    return true;
                }
              }
              break;
            }
            break;
          }
          break;
        case BattleskillEffectLogicEnum.attract:
          if (useUnit != null && (effect.GetInt(BattleskillEffectLogicArgumentEnum.target_gear_kind_id) == 0 || effect.GetInt(BattleskillEffectLogicArgumentEnum.target_gear_kind_id) == unit.originalUnit.unit.kind.ID) && ((effect.GetInt(BattleskillEffectLogicArgumentEnum.target_family_id) == 0 || unit.originalUnit.playerUnit.HasFamily((UnitFamily) effect.GetInt(BattleskillEffectLogicArgumentEnum.target_family_id))) && (effect.GetInt(BattleskillEffectLogicArgumentEnum.target_element) == 0 || (CommonElement) effect.GetInt(BattleskillEffectLogicArgumentEnum.target_element) == unit.originalUnit.playerUnit.GetElement())))
          {
            BL.UnitPosition unitPosition1 = BattleFuncs.iSkillEffectListUnitToUnitPosition(useUnit);
            BL.UnitPosition unitPosition2 = BattleFuncs.iSkillEffectListUnitToUnitPosition(unit);
            int[] attractDelta = BattleFuncs.getAttractDelta(unitPosition1, unitPosition2);
            BL.Unit[] unitArray = new BL.Unit[2]
            {
              useUnit.originalUnit,
              unit.originalUnit
            };
            if (BattleFuncs.isResetPositionOK(useUnit.originalUnit, unitPosition1.row + attractDelta[0], unitPosition1.column + attractDelta[1], unitPosition1.unit.parameter.Move, (IEnumerable<BL.Unit>) unitArray, false) && BattleFuncs.isResetPositionOK(unit.originalUnit, unitPosition2.row + attractDelta[0], unitPosition2.column + attractDelta[1], unitPosition2.unit.parameter.Move, (IEnumerable<BL.Unit>) unitArray, false))
              return true;
            break;
          }
          break;
        case BattleskillEffectLogicEnum.shift_break:
          if (useUnit != null)
          {
            if (firstTarget != null)
              unit = firstTarget;
            if ((effect.GetInt(BattleskillEffectLogicArgumentEnum.target_gear_kind_id) == 0 || effect.GetInt(BattleskillEffectLogicArgumentEnum.target_gear_kind_id) == unit.originalUnit.unit.kind.ID) && (effect.GetInt(BattleskillEffectLogicArgumentEnum.target_family_id) == 0 || unit.originalUnit.playerUnit.HasFamily((UnitFamily) effect.GetInt(BattleskillEffectLogicArgumentEnum.target_family_id))) && ((effect.GetInt(BattleskillEffectLogicArgumentEnum.target_element) == 0 || (CommonElement) effect.GetInt(BattleskillEffectLogicArgumentEnum.target_element) == unit.originalUnit.playerUnit.GetElement()) && BattleFuncs.getShiftBreakPosition(useUnit, unit, effect.GetInt(BattleskillEffectLogicArgumentEnum.range), out int _, out int _)))
              return true;
            break;
          }
          break;
        case BattleskillEffectLogicEnum.keep_away:
          if (useUnit != null && (effect.GetInt(BattleskillEffectLogicArgumentEnum.target_gear_kind_id) == 0 || effect.GetInt(BattleskillEffectLogicArgumentEnum.target_gear_kind_id) == unit.originalUnit.unit.kind.ID) && ((effect.GetInt(BattleskillEffectLogicArgumentEnum.target_family_id) == 0 || unit.originalUnit.unit.HasFamily((UnitFamily) effect.GetInt(BattleskillEffectLogicArgumentEnum.target_family_id))) && (effect.GetInt(BattleskillEffectLogicArgumentEnum.target_element) == 0 || (CommonElement) effect.GetInt(BattleskillEffectLogicArgumentEnum.target_element) == unit.originalUnit.playerUnit.GetElement())))
          {
            BL.UnitPosition unitPosition1 = BattleFuncs.iSkillEffectListUnitToUnitPosition(useUnit);
            BL.UnitPosition unitPosition2 = BattleFuncs.iSkillEffectListUnitToUnitPosition(unit);
            int penetrateCount = effect.GetInt(BattleskillEffectLogicArgumentEnum.range);
            List<Tuple<int, int, int>> penetratePosition = BattleFuncs.getPenetratePosition(unitPosition1.row, unitPosition1.column, unitPosition2.row, unitPosition2.column, penetrateCount, false);
            BL.Unit[] unitArray = new BL.Unit[0];
            using (List<Tuple<int, int, int>>.Enumerator enumerator = penetratePosition.GetEnumerator())
            {
              while (enumerator.MoveNext())
              {
                Tuple<int, int, int> current = enumerator.Current;
                if (BattleFuncs.isResetPositionOK(unit.originalUnit, current.Item1, current.Item2, unitPosition2.unit.parameter.Move, (IEnumerable<BL.Unit>) unitArray, false))
                  return true;
              }
              break;
            }
          }
          else
            break;
        case BattleskillEffectLogicEnum.dance:
          if (useUnit != null && BattleFuncs.checkInvokeSkillEffect(BattleFuncs.PackedSkillEffect.Create(effect), useUnit, unit, new int?(), (Judgement.NonBattleParameter.FromPlayerUnitCache) null, (Judgement.NonBattleParameter.FromPlayerUnitCache) null, new int?(), new int?()))
          {
            if (!(useUnit is BL.AIUnit))
            {
              if (BattleFuncs.env.getActionUnits(BattleFuncs.iSkillEffectListUnitToUnitPosition(unit)) == null)
                return true;
              break;
            }
            if (!((IEnumerable<BL.ISkillEffectListUnit>) BattleFuncs.env.aiActionUnits.value).Contains<BL.ISkillEffectListUnit>(unit))
              return true;
            break;
          }
          break;
        case BattleskillEffectLogicEnum.changing:
          if (useUnit != null && BattleFuncs.checkInvokeSkillEffect(BattleFuncs.PackedSkillEffect.Create(effect), useUnit, unit, new int?(), (Judgement.NonBattleParameter.FromPlayerUnitCache) null, (Judgement.NonBattleParameter.FromPlayerUnitCache) null, new int?(), new int?()))
          {
            BL.UnitPosition unitPosition1 = BattleFuncs.iSkillEffectListUnitToUnitPosition(useUnit);
            BL.UnitPosition unitPosition2 = BattleFuncs.iSkillEffectListUnitToUnitPosition(unit);
            BL.Unit[] unitArray = new BL.Unit[2]
            {
              useUnit.originalUnit,
              unit.originalUnit
            };
            if (BattleFuncs.isResetPositionOK(useUnit.originalUnit, unitPosition2.row, unitPosition2.column, unitPosition1.unit.parameter.Move, (IEnumerable<BL.Unit>) unitArray, false) && BattleFuncs.isResetPositionOK(unit.originalUnit, unitPosition1.row, unitPosition1.column, unitPosition2.unit.parameter.Move, (IEnumerable<BL.Unit>) unitArray, false))
              return true;
            break;
          }
          break;
        case BattleskillEffectLogicEnum.steal:
          if (useUnit != null && (BattleFuncs.checkInvokeSkillEffect(BattleFuncs.PackedSkillEffect.Create(effect), useUnit, unit, new int?(), (Judgement.NonBattleParameter.FromPlayerUnitCache) null, (Judgement.NonBattleParameter.FromPlayerUnitCache) null, new int?(), new int?()) && BattleFuncs.calcStealEffectParam(unit, effect, out int _)))
            return true;
          break;
        case BattleskillEffectLogicEnum.transformation:
          if (useUnit != null && useUnit.transformationGroupId != effect.GetInt(BattleskillEffectLogicArgumentEnum.transformation_group_id))
            return true;
          break;
        default:
          return true;
      }
      return false;
    }

    public static bool canUseSkillToPanel(
      BattleskillSkill skill,
      int level,
      BL.Panel panel,
      BL.ISkillEffectListUnit useUnit)
    {
      BattleskillEffect[] array = ((IEnumerable<BattleskillEffect>) skill.Effects).Where<BattleskillEffect>((Func<BattleskillEffect, bool>) (x => x.EffectLogic.Enum != BattleskillEffectLogicEnum.hp_consume && x.EffectLogic.Enum != BattleskillEffectLogicEnum.skill_chain && (x.EffectLogic.Enum != BattleskillEffectLogicEnum.change_skill_range && x.EffectLogic.Enum != BattleskillEffectLogicEnum.change_skill_use_count) && (x.EffectLogic.Enum != BattleskillEffectLogicEnum.random_choice && !x.EffectLogic.HasTag(BattleskillEffectTag.ext_arg)) && x.checkLevel(level))).ToArray<BattleskillEffect>();
      if (array.Length != 0)
      {
        foreach (BattleskillEffect battleskillEffect in array)
        {
          if (battleskillEffect.EffectLogic.Enum != BattleskillEffectLogicEnum.map_shift || useUnit == null)
            return false;
          BL.UnitPosition unitPosition = BattleFuncs.iSkillEffectListUnitToUnitPosition(useUnit);
          if (!BattleFuncs.isResetPositionOK(useUnit.originalUnit, panel.row, panel.column, unitPosition.unit.parameter.Move, (IEnumerable<BL.Unit>) new BL.Unit[1]
          {
            useUnit.originalUnit
          }, false))
            return false;
        }
        return true;
      }
      foreach (Tuple<BattleskillSkill, IEnumerable<BL.ISkillEffectListUnit>> chainSkillTarget in BattleFuncs.getChainSkillTargets(skill, level, (BL.ISkillEffectListUnit) null, panel, useUnit, (Tuple<int, int>) null))
      {
        Tuple<BattleskillSkill, IEnumerable<BL.ISkillEffectListUnit>> st = chainSkillTarget;
        if (st.Item2.Any<BL.ISkillEffectListUnit>((Func<BL.ISkillEffectListUnit, bool>) (x => x.skillEffects.CanUseSkillOne(st.Item1, level, x, BattleFuncs.env, useUnit) == 0)))
          return true;
      }
      return false;
    }

    public static IEnumerable<Tuple<BattleskillSkill, IEnumerable<BL.ISkillEffectListUnit>>> getChainSkillTargets(
      BattleskillSkill skill,
      int level,
      BL.ISkillEffectListUnit unit,
      BL.Panel panel,
      BL.ISkillEffectListUnit useUnit,
      Tuple<int, int> usePosition = null)
    {
      if (useUnit != null)
      {
        bool isAI = useUnit is BL.AIUnit;
        Queue<BattleskillEffect> chainSkillEffect = new Queue<BattleskillEffect>();
        System.Action<BattleskillSkill> func = (System.Action<BattleskillSkill>) (s =>
        {
          foreach (BattleskillEffect battleskillEffect in ((IEnumerable<BattleskillEffect>) s.Effects).Where<BattleskillEffect>((Func<BattleskillEffect, bool>) (x => x.EffectLogic.Enum == BattleskillEffectLogicEnum.skill_chain && x.checkLevel(level))))
            chainSkillEffect.Enqueue(battleskillEffect);
        });
        func(skill);
        while (chainSkillEffect.Count > 0)
        {
          BattleskillEffect headerEffect = chainSkillEffect.Dequeue();
          if (BattleFuncs.checkInvokeSkillEffect(BattleFuncs.PackedSkillEffect.Create(headerEffect), useUnit, unit, new int?(), (Judgement.NonBattleParameter.FromPlayerUnitCache) null, (Judgement.NonBattleParameter.FromPlayerUnitCache) null, new int?(), new int?()))
          {
            int key = headerEffect.GetInt(BattleskillEffectLogicArgumentEnum.skill_id);
            if (key != 0 && MasterData.BattleskillSkill.ContainsKey(key))
            {
              skill = MasterData.BattleskillSkill[key];
              BL.Skill skill1 = new BL.Skill()
              {
                id = key
              };
              int excluding_slanting = headerEffect.GetInt(BattleskillEffectLogicArgumentEnum.excluding_slanting);
              if (excluding_slanting == 3 || excluding_slanting == 4)
              {
                BL.UnitPosition unitPosition1 = BattleFuncs.iSkillEffectListUnitToUnitPosition(useUnit);
                List<Tuple<int, int>> tupleList = new List<Tuple<int, int>>();
                if (excluding_slanting == 3)
                {
                  int row;
                  int column;
                  if (panel != null)
                  {
                    row = panel.row;
                    column = panel.column;
                  }
                  else if (unit != null)
                  {
                    BL.UnitPosition unitPosition2 = BattleFuncs.iSkillEffectListUnitToUnitPosition(unit);
                    row = unitPosition2.row;
                    column = unitPosition2.column;
                  }
                  else
                    continue;
                  int minRange = skill.min_range;
                  int maxRange = skill.max_range;
                  List<Tuple<int, int, int>> penetratePosition = BattleFuncs.getPenetratePosition(unitPosition1.row, unitPosition1.column, row, column, maxRange, true);
                  if (headerEffect.GetInt(BattleskillEffectLogicArgumentEnum.is_range_from_target) == 0)
                  {
                    for (int index = 0; index < maxRange; ++index)
                      tupleList.Add(Tuple.Create<int, int>(penetratePosition[index].Item1, penetratePosition[index].Item2));
                  }
                  else
                  {
                    foreach (Tuple<int, int, int> tuple in penetratePosition)
                    {
                      if (tuple.Item3 <= maxRange && tuple.Item3 >= minRange)
                        tupleList.Add(Tuple.Create<int, int>(tuple.Item1, tuple.Item2));
                    }
                  }
                }
                else if (usePosition != null)
                {
                  List<Tuple<int, int, int>> penetratePosition = BattleFuncs.getPenetratePosition(usePosition.Item1, usePosition.Item2, unitPosition1.row, unitPosition1.column, 0, true);
                  for (int index = 0; index < penetratePosition.Count; ++index)
                  {
                    if (penetratePosition[index].Item3 < 0)
                      tupleList.Add(Tuple.Create<int, int>(penetratePosition[index].Item1, penetratePosition[index].Item2));
                  }
                }
                else
                  continue;
                List<BL.ISkillEffectListUnit> skillEffectListUnitList = new List<BL.ISkillEffectListUnit>();
                foreach (Tuple<int, int> tuple in tupleList)
                {
                  BL.UnitPosition[] unitPositionArray = isAI ? BattleFuncs.getFieldUnitsAI(tuple.Item1, tuple.Item2) : BattleFuncs.env.getFieldUnits(tuple.Item1, tuple.Item2, false, false);
                  if (unitPositionArray != null)
                  {
                    BL.ForceID[] targetForceIds = skill1.getTargetForceIDs(BattleFuncs.env, (BL.ISkillEffectListUnit) useUnit.originalUnit);
                    foreach (BL.UnitPosition up in unitPositionArray)
                    {
                      BL.ISkillEffectListUnit iskillEffectListUnit = BattleFuncs.unitPositionToISkillEffectListUnit(up);
                      if (BattleFuncs.checkTargetp(iskillEffectListUnit, targetForceIds, skill1.targetAttribute, skill1.nonFacility))
                        skillEffectListUnitList.Add(iskillEffectListUnit);
                    }
                  }
                }
                yield return Tuple.Create<BattleskillSkill, IEnumerable<BL.ISkillEffectListUnit>>(skill, (IEnumerable<BL.ISkillEffectListUnit>) skillEffectListUnitList);
              }
              else
              {
                int row;
                int column;
                if (headerEffect.GetInt(BattleskillEffectLogicArgumentEnum.is_range_from_target) == 0)
                {
                  BL.UnitPosition unitPosition = BattleFuncs.iSkillEffectListUnitToUnitPosition(useUnit);
                  row = unitPosition.row;
                  column = unitPosition.column;
                }
                else if (panel != null)
                {
                  row = panel.row;
                  column = panel.column;
                }
                else if (unit != null)
                {
                  BL.UnitPosition unitPosition = BattleFuncs.iSkillEffectListUnitToUnitPosition(unit);
                  row = unitPosition.row;
                  column = unitPosition.column;
                }
                else
                  continue;
                yield return Tuple.Create<BattleskillSkill, IEnumerable<BL.ISkillEffectListUnit>>(skill, BattleFuncs.env.getSkillTargetUnits(useUnit.originalUnit, row, column, skill1, isAI).Where<BL.UnitPosition>((Func<BL.UnitPosition, bool>) (tup =>
                {
                  switch (excluding_slanting)
                  {
                    case 1:
                      if (tup.row != row && tup.column != column)
                        return false;
                      break;
                    case 2:
                      if (Mathf.Abs(tup.row - row) != Mathf.Abs(tup.column - column))
                        return false;
                      break;
                  }
                  return true;
                })).Select<BL.UnitPosition, BL.ISkillEffectListUnit>((Func<BL.UnitPosition, BL.ISkillEffectListUnit>) (x => BattleFuncs.unitPositionToISkillEffectListUnit(x))));
              }
              func(skill);
            }
          }
        }
      }
    }

    public static IEnumerable<BattleFuncs.InvestSkill> getInvestSkills(
      BL.UnitPosition up)
    {
      BL.ISkillEffectListUnit unit = BattleFuncs.unitPositionToISkillEffectListUnit(up);
      IEnumerable<\u003C\u003Ef__AnonymousType3<BL.SkillEffect, BL.Unit, bool>> first = unit.skillEffects.All().Where<BL.SkillEffect>((Func<BL.SkillEffect, bool>) (effect =>
      {
        if (effect.isDontDisplay)
          return false;
        if (effect.baseSkill.skill_type == BattleskillSkillType.ailment)
          return true;
        return (!effect.isBaseSkill || effect.baseSkill.skill_type != BattleskillSkillType.passive || effect.baseSkill.range_effect_passive_skill) && BattleFuncs.checkEffectLogicEnabled(effect, unit);
      })).Select(x => new
      {
        effect = x,
        fromUnit = x.investUnit,
        isSeal = BattleFuncs.isSealedSkillEffect(unit, x)
      });
      IEnumerable<\u003C\u003Ef__AnonymousType3<BL.SkillEffect, BL.Unit, bool>> datas = unit.skillEffects.GetAllFixEffectParams().Where<Tuple<BattleskillEffectLogicEnum, BL.SkillEffect, int>>((Func<Tuple<BattleskillEffectLogicEnum, BL.SkillEffect, int>, bool>) (param =>
      {
        BL.SkillEffect skillEffect = param.Item2;
        return !skillEffect.isBaseSkill || skillEffect.baseSkill.skill_type != BattleskillSkillType.passive || skillEffect.baseSkill.range_effect_passive_skill;
      })).Select(x => new
      {
        effect = x.Item2,
        fromUnit = x.Item2.investUnit,
        isSeal = BattleFuncs.isSealedSkillEffect(unit, x.Item2)
      });
      IEnumerable<\u003C\u003Ef__AnonymousType3<BL.SkillEffect, BL.Unit, bool>> second1 = unit.skillEffects.GetAllRatioEffectParams().Where<Tuple<BattleskillEffectLogicEnum, BL.SkillEffect, float>>((Func<Tuple<BattleskillEffectLogicEnum, BL.SkillEffect, float>, bool>) (param =>
      {
        BL.SkillEffect skillEffect = param.Item2;
        return !skillEffect.isBaseSkill || skillEffect.baseSkill.skill_type != BattleskillSkillType.passive || skillEffect.baseSkill.range_effect_passive_skill;
      })).Select(x => new
      {
        effect = x.Item2,
        fromUnit = x.Item2.investUnit,
        isSeal = BattleFuncs.isSealedSkillEffect(unit, x.Item2)
      });
      IEnumerable<\u003C\u003Ef__AnonymousType3<BL.SkillEffect, BL.Unit, bool>> second2 = datas;
      foreach (IGrouping<\u003C\u003Ef__AnonymousType4<int, int, bool>, \u003C\u003Ef__AnonymousType3<BL.SkillEffect, BL.Unit, bool>> grouping in first.Concat(second2).Concat(second1).GroupBy(x =>
      {
        int num;
        bool flag;
        if (x.fromUnit != (BL.Unit) null)
        {
          num = x.fromUnit.index;
          flag = x.fromUnit.isPlayerForce;
        }
        else
        {
          num = -1;
          flag = false;
        }
        BL.SkillEffect effect = x.effect;
        return new
        {
          skillId = effect.investSkillId == 0 ? effect.baseSkillId : effect.investSkillId,
          index = num,
          isPlayerControl = flag
        };
      }))
      {
        BL.SkillEffect skillEffect = (BL.SkillEffect) null;
        BL.Unit unit1 = (BL.Unit) null;
        int? nullable1 = new int?();
        foreach (var data in grouping)
        {
          BL.SkillEffect effect = data.effect;
          if (skillEffect == null)
          {
            skillEffect = effect;
            unit1 = data.fromUnit;
          }
          int? nullable2 = effect.baseSkill.skill_type != BattleskillSkillType.ailment ? effect.turnRemain : effect.executeRemain;
          if (nullable2.HasValue)
          {
            if (nullable1.HasValue)
            {
              int? nullable3 = nullable2;
              int? nullable4 = nullable1;
              if (!(nullable3.GetValueOrDefault() > nullable4.GetValueOrDefault() & (nullable3.HasValue & nullable4.HasValue)))
                continue;
            }
            nullable1 = nullable2;
          }
        }
        if (skillEffect != null)
        {
          int skillId = grouping.Key.skillId;
          if (MasterData.BattleskillSkill.ContainsKey(skillId))
            yield return new BattleFuncs.InvestSkill()
            {
              skill = MasterData.BattleskillSkill[skillId],
              turnRemain = nullable1,
              fromEnemy = unit1 != (BL.Unit) null && BattleFuncs.env.getForceID(unit1) != BattleFuncs.env.getForceID(unit.originalUnit),
              fromFriend = unit1 != (BL.Unit) null && unit1.friend,
              isEnemyIcon = unit1 != (BL.Unit) null && BattleFuncs.env.getForceID(unit1) == BL.ForceID.enemy
            };
        }
      }
    }

    private static bool checkEffectLogicEnabled(BL.SkillEffect effect, BL.ISkillEffectListUnit unit)
    {
      int id = effect.effect.EffectLogic.ID;
      try
      {
        if (effect.useRemain.HasValue && effect.useRemain.Value <= 0)
          return false;
        if (id == 1001338)
        {
          if (effect.work != null)
          {
            if (BattleFuncs.getShieldRemain(effect, unit) <= 0)
              return false;
          }
        }
      }
      catch (Exception ex)
      {
      }
      return BattleFuncs.checkPassiveEffectEnable(effect.effect, unit) == 1;
    }

    public static int checkPassiveEffectEnable(
      BattleskillEffect effect,
      BL.ISkillEffectListUnit unit)
    {
      int id = effect.EffectLogic.ID;
      bool? nullable = new bool?();
      try
      {
        switch (effect.EffectLogic.opt_test3)
        {
          case 0:
            if (effect.EffectLogic.opt_test1 != 0)
            {
              nullable = new bool?(Judgement.CheckEnabledBuffDebuff(effect, unit.originalUnit, BattleskillInvokeGameModeEnum.quest));
              break;
            }
            break;
          case 1:
            nullable = new bool?(Judgement.CheckEnabledEquipGearBuffDebuff(effect, unit));
            break;
          case 2:
            nullable = new bool?(Judgement.CheckEnabledRangeBuffDebuff(effect, unit));
            break;
          case 3:
            nullable = new bool?(Judgement.CheckEnabledRangeBuffDebuff(effect, unit));
            break;
          case 4:
          case 5:
            nullable = new bool?(Judgement.CheckEnabledHpBuffDebuff(effect, unit));
            break;
          case 6:
            nullable = new bool?(Judgement.CheckEnabledTargetCountBuffDebuff(effect, unit));
            break;
          case 7:
          case 2044:
          case 2045:
          case 2046:
            nullable = new bool?(Judgement.CheckEnabledCharismaBuffDebuff(effect, unit));
            break;
          case 8:
            nullable = new bool?(Judgement.CheckEnabledCavalryRushBuffDebuff(effect, unit));
            break;
          case 9:
            nullable = new bool?(Judgement.CheckEnabledRaidMissionBuffDebuff(effect, unit));
            break;
          case 10:
            nullable = new bool?(Judgement.CheckEnabledExtremeOfForceBuffDebuff(effect, unit));
            break;
          case 11:
            nullable = new bool?(Judgement.CheckEnabledOnemanChargeBuffDebuff(effect, unit));
            break;
          case 12:
          case 13:
            nullable = new bool?(Judgement.CheckEnabledInOutSideBattleBuffDebuff(effect, unit));
            break;
          case 14:
            nullable = new bool?(Judgement.CheckEnabledEvenIllusionBuffDebuff(effect, unit));
            break;
          case 15:
            nullable = new bool?(Judgement.CheckEnabledSpecificUnitBuffDebuff(effect, unit));
            break;
          case 16:
            nullable = new bool?(Judgement.CheckEnabledUnitRarityBuffDebuff(effect, unit));
            break;
          case 17:
          case 18:
          case 19:
            nullable = new bool?(Judgement.CheckEnabledDeadCountBuffDebuff(effect, unit, BattleskillInvokeGameModeEnum.quest));
            break;
          case 20:
            nullable = new bool?(Judgement.CheckEnabledBuffDebuff2(effect, unit.originalUnit, BattleskillInvokeGameModeEnum.quest));
            break;
          case 21:
          case 22:
            nullable = new bool?(Judgement.CheckEnabledSpecificGroupBuffDebuff(effect, unit));
            break;
          case 23:
            nullable = new bool?(Judgement.CheckEnabledSpecificSkillGroupBuffDebuff(effect, unit));
            break;
          case 24:
            nullable = new bool?(Judgement.CheckEnabledEnemyBuffDebuff(effect, unit, BattleskillInvokeGameModeEnum.quest));
            break;
          case 25:
          case 26:
          case 27:
          case 28:
            nullable = new bool?(Judgement.CheckEnabledParamDiffBuffDebuff(effect, unit));
            break;
          case 29:
            nullable = new bool?(Judgement.CheckEnabledBuffDebuff3(effect, unit));
            break;
          case 30:
            nullable = new bool?(Judgement.CheckEnabledBuffDebuff4(effect, unit));
            break;
          case 31:
            nullable = new bool?(Judgement.CheckEnabledAttackClassBuffDebuff(effect, unit));
            break;
          case 32:
            nullable = new bool?(Judgement.CheckEnabledAttackElementBuffDebuff(effect, unit));
            break;
          case 33:
            nullable = new bool?(Judgement.CheckEnabledInvestLogicBuffDebuff(effect, unit));
            break;
          case 34:
            nullable = new bool?(Judgement.CheckEnabledEnemyInvestLogicBuffDebuff(effect, unit));
            break;
          case 2000:
            nullable = new bool?(BattleFuncs.checkEnabledCriticalGuard(effect, unit));
            break;
          case 2001:
            BattleskillEffect battleskillEffect1 = effect;
            nullable = new bool?((!battleskillEffect1.HasKey(BattleskillEffectLogicArgumentEnum.gear_kind_id) || battleskillEffect1.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) == 0 || battleskillEffect1.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) == unit.originalUnit.unit.kind.ID) && (!battleskillEffect1.HasKey(BattleskillEffectLogicArgumentEnum.element) || battleskillEffect1.GetInt(BattleskillEffectLogicArgumentEnum.element) == 0 || (CommonElement) battleskillEffect1.GetInt(BattleskillEffectLogicArgumentEnum.element) == unit.originalUnit.playerUnit.GetElement()) && (!battleskillEffect1.HasKey(BattleskillEffectLogicArgumentEnum.job_id) || battleskillEffect1.GetInt(BattleskillEffectLogicArgumentEnum.job_id) == 0 || battleskillEffect1.GetInt(BattleskillEffectLogicArgumentEnum.job_id) == unit.originalUnit.job.ID) && (!battleskillEffect1.HasKey(BattleskillEffectLogicArgumentEnum.family_id) || battleskillEffect1.GetInt(BattleskillEffectLogicArgumentEnum.family_id) == 0 || unit.originalUnit.playerUnit.HasFamily((UnitFamily) battleskillEffect1.GetInt(BattleskillEffectLogicArgumentEnum.family_id))));
            break;
          case 2002:
            int num1 = effect.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id);
            nullable = new bool?(num1 == 0 || num1 == unit.originalUnit.weapon.gear.kind.ID);
            break;
          case 2003:
          case 2004:
            nullable = new bool?(BattleFuncs.checkEnabledWhiteNight(effect, unit));
            break;
          case 2005:
            nullable = new bool?(BattleFuncs.checkEnabledWhiteNight3(effect, unit));
            break;
          case 2006:
            int num2 = !effect.HasKey(BattleskillEffectLogicArgumentEnum.element) ? 0 : effect.GetInt(BattleskillEffectLogicArgumentEnum.element);
            nullable = new bool?(num2 == 0 || (CommonElement) num2 == unit.originalUnit.playerUnit.GetElement());
            break;
          case 2007:
            BattleskillEffect battleskillEffect2 = effect;
            nullable = new bool?((!battleskillEffect2.HasKey(BattleskillEffectLogicArgumentEnum.gear_kind_id) || battleskillEffect2.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) == 0 || battleskillEffect2.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) == unit.originalUnit.unit.kind.ID) && (!battleskillEffect2.HasKey(BattleskillEffectLogicArgumentEnum.element) || battleskillEffect2.GetInt(BattleskillEffectLogicArgumentEnum.element) == 0 || (CommonElement) battleskillEffect2.GetInt(BattleskillEffectLogicArgumentEnum.element) == unit.originalUnit.playerUnit.GetElement()) && (!battleskillEffect2.HasKey(BattleskillEffectLogicArgumentEnum.job_id) || battleskillEffect2.GetInt(BattleskillEffectLogicArgumentEnum.job_id) == 0 || battleskillEffect2.GetInt(BattleskillEffectLogicArgumentEnum.job_id) == unit.originalUnit.job.ID));
            break;
          case 2008:
            BattleskillEffect battleskillEffect3 = effect;
            nullable = new bool?((!battleskillEffect3.HasKey(BattleskillEffectLogicArgumentEnum.gear_kind_id) || battleskillEffect3.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) == 0 || battleskillEffect3.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) == unit.originalUnit.unit.kind.ID) && (!battleskillEffect3.HasKey(BattleskillEffectLogicArgumentEnum.element) || battleskillEffect3.GetInt(BattleskillEffectLogicArgumentEnum.element) == 0 || (CommonElement) battleskillEffect3.GetInt(BattleskillEffectLogicArgumentEnum.element) == unit.originalUnit.playerUnit.GetElement()) && (!battleskillEffect3.HasKey(BattleskillEffectLogicArgumentEnum.job_id) || battleskillEffect3.GetInt(BattleskillEffectLogicArgumentEnum.job_id) == 0 || battleskillEffect3.GetInt(BattleskillEffectLogicArgumentEnum.job_id) == unit.originalUnit.job.ID));
            break;
          case 2009:
            nullable = new bool?(BattleFuncs.checkEnabledSkillsAndEffectsInvalid(effect, unit));
            break;
          case 2010:
            nullable = new bool?(BattleFuncs.CheckEnabledLandBlessingBuffDebuff(effect, unit));
            break;
          case 2011:
          case 2012:
            nullable = new bool?(BattleFuncs.CheckEnabledDuelSupportBuffDebuff(effect, unit));
            break;
          case 2013:
            BattleskillEffect battleskillEffect4 = effect;
            nullable = new bool?((battleskillEffect4.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) == 0 || battleskillEffect4.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) == unit.originalUnit.unit.kind.ID) && (battleskillEffect4.GetInt(BattleskillEffectLogicArgumentEnum.element) == 0 || (CommonElement) battleskillEffect4.GetInt(BattleskillEffectLogicArgumentEnum.element) == unit.originalUnit.playerUnit.GetElement()) && (battleskillEffect4.GetInt(BattleskillEffectLogicArgumentEnum.job_id) == 0 || battleskillEffect4.GetInt(BattleskillEffectLogicArgumentEnum.job_id) == unit.originalUnit.job.ID) && (battleskillEffect4.GetInt(BattleskillEffectLogicArgumentEnum.family_id) == 0 || unit.originalUnit.playerUnit.HasFamily((UnitFamily) battleskillEffect4.GetInt(BattleskillEffectLogicArgumentEnum.family_id))));
            break;
          case 2014:
          case 2015:
            BattleskillEffect battleskillEffect5 = effect;
            BattleFuncs.PackedSkillEffect pse1 = BattleFuncs.PackedSkillEffect.Create(effect);
            pse1.SetIgnoreHeader(true);
            nullable = new bool?((battleskillEffect5.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) == 0 || battleskillEffect5.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) == unit.originalUnit.unit.kind.ID) && (battleskillEffect5.GetInt(BattleskillEffectLogicArgumentEnum.element) == 0 || (CommonElement) battleskillEffect5.GetInt(BattleskillEffectLogicArgumentEnum.element) == unit.originalUnit.playerUnit.GetElement()) && (!battleskillEffect5.HasKey(BattleskillEffectLogicArgumentEnum.job_id) || battleskillEffect5.GetInt(BattleskillEffectLogicArgumentEnum.job_id) == 0 || battleskillEffect5.GetInt(BattleskillEffectLogicArgumentEnum.job_id) == unit.originalUnit.job.ID) && ((battleskillEffect5.GetInt(BattleskillEffectLogicArgumentEnum.family_id) == 0 || unit.originalUnit.playerUnit.HasFamily((UnitFamily) battleskillEffect5.GetInt(BattleskillEffectLogicArgumentEnum.family_id))) && (!battleskillEffect5.HasKey(BattleskillEffectLogicArgumentEnum.skill_group_id) || battleskillEffect5.GetInt(BattleskillEffectLogicArgumentEnum.skill_group_id) == 0 || unit.originalUnit.unit.HasSkillGroupId(battleskillEffect5.GetInt(BattleskillEffectLogicArgumentEnum.skill_group_id)))) && (!battleskillEffect5.HasKey(BattleskillEffectLogicArgumentEnum.exclude_skill_group_id) || battleskillEffect5.GetInt(BattleskillEffectLogicArgumentEnum.exclude_skill_group_id) == 0 || !unit.originalUnit.unit.HasSkillGroupId(battleskillEffect5.GetInt(BattleskillEffectLogicArgumentEnum.exclude_skill_group_id))) && BattleFuncs.checkInvokeSkillEffectSelf(pse1, unit, (Judgement.NonBattleParameter.FromPlayerUnitCache) null, new int?(), true));
            break;
          case 2016:
            BattleskillEffect battleskillEffect6 = effect;
            nullable = new bool?((battleskillEffect6.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) == 0 || battleskillEffect6.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) == unit.originalUnit.unit.kind.ID) && (battleskillEffect6.GetInt(BattleskillEffectLogicArgumentEnum.element) == 0 || (CommonElement) battleskillEffect6.GetInt(BattleskillEffectLogicArgumentEnum.element) == unit.originalUnit.playerUnit.GetElement()) && ((battleskillEffect6.GetInt(BattleskillEffectLogicArgumentEnum.job_id) == 0 || battleskillEffect6.GetInt(BattleskillEffectLogicArgumentEnum.job_id) == unit.originalUnit.job.ID) && (battleskillEffect6.GetInt(BattleskillEffectLogicArgumentEnum.family_id) == 0 || unit.originalUnit.playerUnit.HasFamily((UnitFamily) battleskillEffect6.GetInt(BattleskillEffectLogicArgumentEnum.family_id)))) && (!battleskillEffect6.HasKey(BattleskillEffectLogicArgumentEnum.invoke_gamemode) || BattleFuncs.checkInvokeGamemode(battleskillEffect6.GetInt(BattleskillEffectLogicArgumentEnum.invoke_gamemode), false)));
            break;
          case 2017:
            BattleskillEffect battleskillEffect7 = effect;
            nullable = new bool?((battleskillEffect7.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) == 0 || battleskillEffect7.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) == unit.originalUnit.unit.kind.ID) && (battleskillEffect7.GetInt(BattleskillEffectLogicArgumentEnum.element) == 0 || (CommonElement) battleskillEffect7.GetInt(BattleskillEffectLogicArgumentEnum.element) == unit.originalUnit.playerUnit.GetElement()) && (battleskillEffect7.GetInt(BattleskillEffectLogicArgumentEnum.job_id) == 0 || battleskillEffect7.GetInt(BattleskillEffectLogicArgumentEnum.job_id) == unit.originalUnit.job.ID) && (battleskillEffect7.GetInt(BattleskillEffectLogicArgumentEnum.family_id) == 0 || unit.originalUnit.playerUnit.HasFamily((UnitFamily) battleskillEffect7.GetInt(BattleskillEffectLogicArgumentEnum.family_id))));
            break;
          case 2018:
            BattleskillEffect battleskillEffect8 = effect;
            BattleFuncs.PackedSkillEffect packedSkillEffect = BattleFuncs.PackedSkillEffect.Create(effect);
            nullable = new bool?((battleskillEffect8.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) == 0 || battleskillEffect8.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) == unit.originalUnit.unit.kind.ID) && (battleskillEffect8.GetInt(BattleskillEffectLogicArgumentEnum.element) == 0 || (CommonElement) battleskillEffect8.GetInt(BattleskillEffectLogicArgumentEnum.element) == unit.originalUnit.playerUnit.GetElement()) && (battleskillEffect8.GetInt(BattleskillEffectLogicArgumentEnum.job_id) == 0 || battleskillEffect8.GetInt(BattleskillEffectLogicArgumentEnum.job_id) == unit.originalUnit.job.ID) && (!packedSkillEffect.HasKey(BattleskillEffectLogicArgumentEnum.exclude_skill_group_id) || packedSkillEffect.GetInt(BattleskillEffectLogicArgumentEnum.exclude_skill_group_id) == 0 || !unit.originalUnit.unit.HasSkillGroupId(packedSkillEffect.GetInt(BattleskillEffectLogicArgumentEnum.exclude_skill_group_id))) && (!packedSkillEffect.HasKey(BattleskillEffectLogicArgumentEnum.group_large_id) || packedSkillEffect.GetInt(BattleskillEffectLogicArgumentEnum.group_large_id) == 0 || unit.originalUnit.unitGroup != null && packedSkillEffect.GetInt(BattleskillEffectLogicArgumentEnum.group_large_id) == unit.originalUnit.unitGroup.group_large_category_id.ID));
            break;
          case 2019:
            BattleskillEffect battleskillEffect9 = effect;
            BattleFuncs.PackedSkillEffect pse2 = BattleFuncs.PackedSkillEffect.Create(effect);
            pse2.SetIgnoreHeader(true);
            nullable = new bool?((battleskillEffect9.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) == 0 || battleskillEffect9.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) == unit.originalUnit.unit.kind.ID) && (battleskillEffect9.GetInt(BattleskillEffectLogicArgumentEnum.element) == 0 || (CommonElement) battleskillEffect9.GetInt(BattleskillEffectLogicArgumentEnum.element) == unit.originalUnit.playerUnit.GetElement()) && BattleFuncs.checkInvokeSkillEffectSelf(pse2, unit, (Judgement.NonBattleParameter.FromPlayerUnitCache) null, new int?(), true));
            break;
          case 2020:
          case 2021:
            BattleskillEffect battleskillEffect10 = effect;
            nullable = new bool?((battleskillEffect10.GetInt(BattleskillEffectLogicArgumentEnum.element) == 0 || (CommonElement) battleskillEffect10.GetInt(BattleskillEffectLogicArgumentEnum.element) == unit.originalUnit.playerUnit.GetElement()) && (!battleskillEffect10.HasKey(BattleskillEffectLogicArgumentEnum.gear_kind_id) || battleskillEffect10.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) == 0 || battleskillEffect10.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) == unit.originalUnit.unit.kind.ID) && ((!battleskillEffect10.HasKey(BattleskillEffectLogicArgumentEnum.family_id) || battleskillEffect10.GetInt(BattleskillEffectLogicArgumentEnum.family_id) == 0 || unit.originalUnit.playerUnit.HasFamily((UnitFamily) battleskillEffect10.GetInt(BattleskillEffectLogicArgumentEnum.family_id))) && (!battleskillEffect10.HasKey(BattleskillEffectLogicArgumentEnum.skill_group_id) || battleskillEffect10.GetInt(BattleskillEffectLogicArgumentEnum.skill_group_id) == 0 || unit.originalUnit.unit.HasSkillGroupId(battleskillEffect10.GetInt(BattleskillEffectLogicArgumentEnum.skill_group_id)))) && (!battleskillEffect10.HasKey(BattleskillEffectLogicArgumentEnum.exclude_skill_group_id) || battleskillEffect10.GetInt(BattleskillEffectLogicArgumentEnum.exclude_skill_group_id) == 0 || !unit.originalUnit.unit.HasSkillGroupId(battleskillEffect10.GetInt(BattleskillEffectLogicArgumentEnum.exclude_skill_group_id))));
            break;
          case 2022:
          case 2048:
            BattleskillEffect battleskillEffect11 = effect;
            BattleFuncs.PackedSkillEffect pse3 = BattleFuncs.PackedSkillEffect.Create(effect);
            pse3.SetIgnoreHeader(true);
            nullable = new bool?((battleskillEffect11.GetInt(BattleskillEffectLogicArgumentEnum.element) == 0 || (CommonElement) battleskillEffect11.GetInt(BattleskillEffectLogicArgumentEnum.element) == unit.originalUnit.playerUnit.GetElement()) && (battleskillEffect11.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) == 0 || battleskillEffect11.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) == unit.originalUnit.unit.kind.ID) && BattleFuncs.checkInvokeSkillEffectSelf(pse3, unit, (Judgement.NonBattleParameter.FromPlayerUnitCache) null, new int?(), true));
            break;
          case 2023:
          case 2024:
            nullable = new bool?(BattleFuncs.CheckEnabledFieldDamageFluctuateBuffDebuff(effect, unit));
            break;
          case 2025:
            BattleskillEffect battleskillEffect12 = effect;
            BattleFuncs.PackedSkillEffect pse4 = BattleFuncs.PackedSkillEffect.Create(effect);
            pse4.SetIgnoreHeader(true);
            nullable = new bool?((battleskillEffect12.GetInt(BattleskillEffectLogicArgumentEnum.element) == 0 || (CommonElement) battleskillEffect12.GetInt(BattleskillEffectLogicArgumentEnum.element) == unit.originalUnit.playerUnit.GetElement()) && (battleskillEffect12.GetInt(BattleskillEffectLogicArgumentEnum.character_id) == 0 || battleskillEffect12.GetInt(BattleskillEffectLogicArgumentEnum.character_id) == unit.originalUnit.unit.character.ID) && (battleskillEffect12.GetInt(BattleskillEffectLogicArgumentEnum.unit_id) == 0 || battleskillEffect12.GetInt(BattleskillEffectLogicArgumentEnum.unit_id) == unit.originalUnit.unit.ID) && BattleFuncs.checkInvokeSkillEffectSelf(pse4, unit, (Judgement.NonBattleParameter.FromPlayerUnitCache) null, new int?(), true));
            break;
          case 2026:
          case 2027:
            nullable = new bool?(BattleFuncs.CheckEnabledDamageCutBuffDebuff(effect, unit));
            break;
          case 2028:
            nullable = new bool?(true);
            break;
          case 2029:
            BattleskillEffect battleskillEffect13 = effect;
            nullable = new bool?((!battleskillEffect13.HasKey(BattleskillEffectLogicArgumentEnum.skill_group_id) || battleskillEffect13.GetInt(BattleskillEffectLogicArgumentEnum.skill_group_id) == 0 || unit.originalUnit.unit.HasSkillGroupId(battleskillEffect13.GetInt(BattleskillEffectLogicArgumentEnum.skill_group_id))) && (!battleskillEffect13.HasKey(BattleskillEffectLogicArgumentEnum.exclude_skill_group_id) || battleskillEffect13.GetInt(BattleskillEffectLogicArgumentEnum.exclude_skill_group_id) == 0 || !unit.originalUnit.unit.HasSkillGroupId(battleskillEffect13.GetInt(BattleskillEffectLogicArgumentEnum.exclude_skill_group_id))));
            break;
          case 2030:
            nullable = new bool?(true);
            break;
          case 2031:
            BattleskillEffect battleskillEffect14 = effect;
            nullable = new bool?((battleskillEffect14.GetInt(BattleskillEffectLogicArgumentEnum.element) == 0 || (CommonElement) battleskillEffect14.GetInt(BattleskillEffectLogicArgumentEnum.element) == unit.originalUnit.playerUnit.GetElement()) && (battleskillEffect14.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) == 0 || battleskillEffect14.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) == unit.originalUnit.unit.kind.ID) && (battleskillEffect14.GetInt(BattleskillEffectLogicArgumentEnum.family_id) == 0 || unit.originalUnit.playerUnit.HasFamily((UnitFamily) battleskillEffect14.GetInt(BattleskillEffectLogicArgumentEnum.family_id))));
            break;
          case 2032:
            BattleskillEffect battleskillEffect15 = effect;
            nullable = new bool?((battleskillEffect15.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) == 0 || battleskillEffect15.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) == unit.originalUnit.unit.kind.ID) && (battleskillEffect15.GetInt(BattleskillEffectLogicArgumentEnum.element) == 0 || (CommonElement) battleskillEffect15.GetInt(BattleskillEffectLogicArgumentEnum.element) == unit.originalUnit.playerUnit.GetElement()) && ((battleskillEffect15.GetInt(BattleskillEffectLogicArgumentEnum.family_id) == 0 || unit.originalUnit.playerUnit.HasFamily((UnitFamily) battleskillEffect15.GetInt(BattleskillEffectLogicArgumentEnum.family_id))) && (battleskillEffect15.GetInt(BattleskillEffectLogicArgumentEnum.skill_group_id) == 0 || unit.originalUnit.unit.HasSkillGroupId(battleskillEffect15.GetInt(BattleskillEffectLogicArgumentEnum.skill_group_id)))) && (battleskillEffect15.GetInt(BattleskillEffectLogicArgumentEnum.exclude_skill_group_id) == 0 || !unit.originalUnit.unit.HasSkillGroupId(battleskillEffect15.GetInt(BattleskillEffectLogicArgumentEnum.exclude_skill_group_id))));
            break;
          case 2033:
          case 2034:
          case 2047:
            nullable = new bool?(BattleFuncs.checkInvokeSkillEffectSelf(BattleFuncs.PackedSkillEffect.Create(effect), unit, (Judgement.NonBattleParameter.FromPlayerUnitCache) null, new int?(), true));
            break;
          case 2035:
            nullable = new bool?(true);
            break;
          case 2036:
            nullable = new bool?(BattleFuncs.checkInvokeSkillEffectSelf(BattleFuncs.PackedSkillEffect.Create(effect), unit, (Judgement.NonBattleParameter.FromPlayerUnitCache) null, new int?(), true));
            break;
          case 2037:
            BattleFuncs.PackedSkillEffect pse5 = BattleFuncs.PackedSkillEffect.Create(effect);
            pse5.SetIgnoreHeader(true);
            nullable = new bool?(BattleFuncs.checkInvokeSkillEffectSelf(pse5, unit, (Judgement.NonBattleParameter.FromPlayerUnitCache) null, new int?(), true));
            break;
          case 2038:
            BattleskillEffect battleskillEffect16 = effect;
            nullable = new bool?((!battleskillEffect16.HasKey(BattleskillEffectLogicArgumentEnum.gear_kind_id) || battleskillEffect16.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) == 0 || battleskillEffect16.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) == unit.originalUnit.unit.kind.ID) && (!battleskillEffect16.HasKey(BattleskillEffectLogicArgumentEnum.element) || battleskillEffect16.GetInt(BattleskillEffectLogicArgumentEnum.element) == 0 || (CommonElement) battleskillEffect16.GetInt(BattleskillEffectLogicArgumentEnum.element) == unit.originalUnit.playerUnit.GetElement()));
            break;
          case 2039:
            nullable = new bool?(BattleFuncs.checkInvokeSkillEffectSelf(BattleFuncs.PackedSkillEffect.Create(effect), unit, (Judgement.NonBattleParameter.FromPlayerUnitCache) null, new int?(), true));
            break;
          case 2040:
          case 2041:
          case 2042:
          case 2043:
            BattleskillEffect battleskillEffect17 = effect;
            nullable = new bool?((battleskillEffect17.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) == 0 || battleskillEffect17.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) == unit.originalUnit.unit.kind.ID) && (battleskillEffect17.GetInt(BattleskillEffectLogicArgumentEnum.element) == 0 || (CommonElement) battleskillEffect17.GetInt(BattleskillEffectLogicArgumentEnum.element) == unit.originalUnit.playerUnit.GetElement()) && (battleskillEffect17.GetInt(BattleskillEffectLogicArgumentEnum.family_id) == 0 || unit.originalUnit.playerUnit.HasFamily((UnitFamily) battleskillEffect17.GetInt(BattleskillEffectLogicArgumentEnum.family_id))));
            break;
          case 2049:
            nullable = new bool?(BattleFuncs.checkInvokeSkillEffectSelf(BattleFuncs.PackedSkillEffect.Create(effect), unit, (Judgement.NonBattleParameter.FromPlayerUnitCache) null, new int?(), true));
            break;
          case 2050:
            nullable = new bool?(BattleFuncs.checkInvokeSkillEffectSelf(BattleFuncs.PackedSkillEffect.Create(effect), unit, (Judgement.NonBattleParameter.FromPlayerUnitCache) null, new int?(), true));
            break;
          case 2051:
            nullable = new bool?(BattleFuncs.checkInvokeSkillEffectSelf(BattleFuncs.PackedSkillEffect.Create(effect), unit, (Judgement.NonBattleParameter.FromPlayerUnitCache) null, new int?(), true));
            break;
          case 2052:
            nullable = new bool?(BattleFuncs.checkInvokeSkillEffectSelf(BattleFuncs.PackedSkillEffect.Create(effect), unit, (Judgement.NonBattleParameter.FromPlayerUnitCache) null, new int?(), true));
            break;
          case 9999:
            nullable = new bool?(true);
            break;
        }
      }
      catch (Exception ex)
      {
      }
      if (!nullable.HasValue)
        return 2;
      return !nullable.Value ? 0 : 1;
    }

    public static bool checkInvokeSkillEffectCommon(
      BattleFuncs.PackedSkillEffect pse,
      int? colosseumTurn = null)
    {
      if (pse.HasKey(BattleskillEffectLogicArgumentEnum.invoke_gamemode) && !BattleFuncs.checkInvokeGamemode(pse.GetInt(BattleskillEffectLogicArgumentEnum.invoke_gamemode), colosseumTurn.HasValue))
        return false;
      int absoluteTurnCount;
      if (!colosseumTurn.HasValue)
      {
        BL.PhaseState phaseState = BattleFuncs.getPhaseState();
        if (phaseState == null)
          return false;
        absoluteTurnCount = phaseState.absoluteTurnCount;
      }
      else
        absoluteTurnCount = colosseumTurn.Value;
      int num1 = 0;
      if (pse.HasKey(BattleskillEffectLogicArgumentEnum.start_turn))
      {
        num1 = pse.GetInt(BattleskillEffectLogicArgumentEnum.start_turn);
        if (num1 != 0 && absoluteTurnCount < num1)
          return false;
      }
      if (pse.HasKey(BattleskillEffectLogicArgumentEnum.end_turn))
      {
        int num2 = pse.GetInt(BattleskillEffectLogicArgumentEnum.end_turn);
        if (num2 != 0 && absoluteTurnCount >= num2)
          return false;
      }
      if (pse.HasKey(BattleskillEffectLogicArgumentEnum.turn_cycle))
      {
        int num2 = pse.GetInt(BattleskillEffectLogicArgumentEnum.turn_cycle);
        if (num2 != 0 && (absoluteTurnCount - num1) % num2 != 0)
          return false;
      }
      int num3 = absoluteTurnCount - (pse.skillEffect != null ? pse.skillEffect.investTurn : 0);
      int num4 = 0;
      if (pse.HasKey(BattleskillEffectLogicArgumentEnum.start_turn2))
      {
        num4 = pse.GetInt(BattleskillEffectLogicArgumentEnum.start_turn2);
        if (num4 != 0 && num3 < num4)
          return false;
      }
      if (pse.HasKey(BattleskillEffectLogicArgumentEnum.turn_cycle2))
      {
        int num2 = pse.GetInt(BattleskillEffectLogicArgumentEnum.turn_cycle2);
        if (num2 != 0 && (num3 - num4) % num2 != 0)
          return false;
      }
      return true;
    }

    public static bool checkInvokeSkillEffectSelf(
      BattleFuncs.PackedSkillEffect pse,
      BL.ISkillEffectListUnit unit,
      Judgement.NonBattleParameter.FromPlayerUnitCache unitNbpCache = null,
      int? unitHp = null,
      bool dontCheckParamDiff = false)
    {
      if (pse.HasKey(BattleskillEffectLogicArgumentEnum.equip_gear_king_id) && !BattleFuncs.isGearEquipped(unit.originalUnit.playerUnit, pse.GetInt(BattleskillEffectLogicArgumentEnum.equip_gear_king_id)))
        return false;
      if (pse.HasKey(BattleskillEffectLogicArgumentEnum.gear_kind_id))
      {
        int num = pse.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id);
        if (num != 0 && num != unit.originalUnit.unit.kind.ID)
          return false;
      }
      if (pse.HasKey(BattleskillEffectLogicArgumentEnum.exclude_gear_kind_id))
      {
        int num = pse.GetInt(BattleskillEffectLogicArgumentEnum.exclude_gear_kind_id);
        if (num != 0 && num == unit.originalUnit.unit.kind.ID)
          return false;
      }
      if (pse.HasKey(BattleskillEffectLogicArgumentEnum.element))
      {
        int num = pse.GetInt(BattleskillEffectLogicArgumentEnum.element);
        if (num != 0 && (CommonElement) num != unit.originalUnit.playerUnit.GetElement())
          return false;
      }
      if (pse.HasKey(BattleskillEffectLogicArgumentEnum.exclude_element))
      {
        int num = pse.GetInt(BattleskillEffectLogicArgumentEnum.exclude_element);
        if (num != 0 && (CommonElement) num == unit.originalUnit.playerUnit.GetElement())
          return false;
      }
      if (pse.HasKey(BattleskillEffectLogicArgumentEnum.job_id))
      {
        int num = pse.GetInt(BattleskillEffectLogicArgumentEnum.job_id);
        if (num != 0 && num != unit.originalUnit.job.ID)
          return false;
      }
      if (pse.HasKey(BattleskillEffectLogicArgumentEnum.exclude_job_id))
      {
        int num = pse.GetInt(BattleskillEffectLogicArgumentEnum.exclude_job_id);
        if (num != 0 && num == unit.originalUnit.job.ID)
          return false;
      }
      if (pse.HasKey(BattleskillEffectLogicArgumentEnum.family_id))
      {
        int num = pse.GetInt(BattleskillEffectLogicArgumentEnum.family_id);
        if (num != 0 && !unit.originalUnit.playerUnit.HasFamily((UnitFamily) num))
          return false;
      }
      if (pse.HasKey(BattleskillEffectLogicArgumentEnum.exclude_family_id))
      {
        int num = pse.GetInt(BattleskillEffectLogicArgumentEnum.exclude_family_id);
        if (num != 0 && unit.originalUnit.playerUnit.HasFamily((UnitFamily) num))
          return false;
      }
      if (pse.HasKey(BattleskillEffectLogicArgumentEnum.group_large_id))
      {
        int num = pse.GetInt(BattleskillEffectLogicArgumentEnum.group_large_id);
        if (num != 0 && (unit.originalUnit.unitGroup == null || num != unit.originalUnit.unitGroup.group_large_category_id.ID))
          return false;
      }
      if (pse.HasKey(BattleskillEffectLogicArgumentEnum.group_small_id))
      {
        int num = pse.GetInt(BattleskillEffectLogicArgumentEnum.group_small_id);
        if (num != 0 && (unit.originalUnit.unitGroup == null || num != unit.originalUnit.unitGroup.group_small_category_id.ID))
          return false;
      }
      if (pse.HasKey(BattleskillEffectLogicArgumentEnum.group_clothing_id))
      {
        int num = pse.GetInt(BattleskillEffectLogicArgumentEnum.group_clothing_id);
        if (num != 0 && (unit.originalUnit.unitGroup == null || num != unit.originalUnit.unitGroup.group_clothing_category_id.ID && num != unit.originalUnit.unitGroup.group_clothing_category_id_2.ID))
          return false;
      }
      if (pse.HasKey(BattleskillEffectLogicArgumentEnum.group_generation_id))
      {
        int num = pse.GetInt(BattleskillEffectLogicArgumentEnum.group_generation_id);
        if (num != 0 && (unit.originalUnit.unitGroup == null || num != unit.originalUnit.unitGroup.group_generation_category_id.ID))
          return false;
      }
      if (pse.HasKey(BattleskillEffectLogicArgumentEnum.skill_group_id))
      {
        int skillGroupId = pse.GetInt(BattleskillEffectLogicArgumentEnum.skill_group_id);
        if (skillGroupId != 0 && !unit.originalUnit.unit.HasSkillGroupId(skillGroupId))
          return false;
      }
      if (pse.HasKey(BattleskillEffectLogicArgumentEnum.exclude_skill_group_id))
      {
        int skillGroupId = pse.GetInt(BattleskillEffectLogicArgumentEnum.exclude_skill_group_id);
        if (skillGroupId != 0 && unit.originalUnit.unit.HasSkillGroupId(skillGroupId))
          return false;
      }
      if (!dontCheckParamDiff && pse.HasKey(BattleskillEffectLogicArgumentEnum.param_type))
      {
        int paramType = pse.GetInt(BattleskillEffectLogicArgumentEnum.param_type);
        if (paramType != 0 && pse.GetInt(BattleskillEffectLogicArgumentEnum.target_param_type) == 0)
        {
          if (unitNbpCache == null)
            unitNbpCache = new Judgement.NonBattleParameter.FromPlayerUnitCache(unit.originalUnit.playerUnit);
          int num = -BattleFuncs.GetParamDiffValue(paramType, unitNbpCache, unitHp.HasValue ? unitHp.Value : unit.hp);
          if (num < pse.GetInt(BattleskillEffectLogicArgumentEnum.min_value) || num > pse.GetInt(BattleskillEffectLogicArgumentEnum.max_value))
            return false;
        }
      }
      if (!dontCheckParamDiff)
      {
        bool flag1 = pse.HasKey(BattleskillEffectLogicArgumentEnum.oneman_charge_player_min_range);
        bool flag2 = pse.HasKey(BattleskillEffectLogicArgumentEnum.oneman_charge_enemy_min_range);
        bool flag3 = pse.HasKey(BattleskillEffectLogicArgumentEnum.oneman_charge_complex_min_range);
        if (flag1 | flag2 | flag3)
        {
          BL.UnitPosition up = BattleFuncs.iSkillEffectListUnitToUnitPosition(unit);
          Func<int[], int> func1 = (Func<int[], int>) (range => BattleFuncs.getTargets(up.row, up.column, range, BattleFuncs.getForceIDArray(BattleFuncs.getForceID(unit.originalUnit)), BL.Unit.TargetAttribute.all, unit is BL.AIUnit, false, false, true, (List<BL.Unit>) null).Count<BL.UnitPosition>((Func<BL.UnitPosition, bool>) (x => x != up && BattleFuncs.unitPositionToISkillEffectListUnit(x).hp > 0)));
          Func<int[], int> func2 = (Func<int[], int>) (range => BattleFuncs.getTargets(up.row, up.column, range, BattleFuncs.getTargetForce(unit.originalUnit, unit.IsCharm), BL.Unit.TargetAttribute.all, unit is BL.AIUnit, false, false, true, (List<BL.Unit>) null).Count<BL.UnitPosition>((Func<BL.UnitPosition, bool>) (x => BattleFuncs.unitPositionToISkillEffectListUnit(x).hp > 0)));
          if (flag1)
          {
            int num = func1(new int[2]
            {
              pse.GetInt(BattleskillEffectLogicArgumentEnum.oneman_charge_player_min_range),
              pse.GetInt(BattleskillEffectLogicArgumentEnum.oneman_charge_player_max_range)
            });
            if (num < pse.GetInt(BattleskillEffectLogicArgumentEnum.oneman_charge_player_min_unit_count) || num > pse.GetInt(BattleskillEffectLogicArgumentEnum.oneman_charge_player_max_unit_count))
              return false;
          }
          if (flag2)
          {
            int num = func2(new int[2]
            {
              pse.GetInt(BattleskillEffectLogicArgumentEnum.oneman_charge_enemy_min_range),
              pse.GetInt(BattleskillEffectLogicArgumentEnum.oneman_charge_enemy_max_range)
            });
            if (num < pse.GetInt(BattleskillEffectLogicArgumentEnum.oneman_charge_enemy_min_unit_count) || num > pse.GetInt(BattleskillEffectLogicArgumentEnum.oneman_charge_enemy_max_unit_count))
              return false;
          }
          if (flag3)
          {
            int[] numArray = new int[2]
            {
              pse.GetInt(BattleskillEffectLogicArgumentEnum.oneman_charge_complex_min_range),
              pse.GetInt(BattleskillEffectLogicArgumentEnum.oneman_charge_complex_max_range)
            };
            int num = func1(numArray) + func2(numArray);
            if (num < pse.GetInt(BattleskillEffectLogicArgumentEnum.oneman_charge_complex_min_unit_count) || num > pse.GetInt(BattleskillEffectLogicArgumentEnum.oneman_charge_complex_max_unit_count))
              return false;
          }
        }
      }
      return true;
    }

    public static bool checkInvokeSkillEffectTarget(
      BattleFuncs.PackedSkillEffect pse,
      BL.ISkillEffectListUnit target,
      Judgement.NonBattleParameter.FromPlayerUnitCache targetNbpCache = null,
      int? targetHp = null)
    {
      if (pse.HasKey(BattleskillEffectLogicArgumentEnum.target_equip_gear_kind_id))
      {
        int kindId = pse.GetInt(BattleskillEffectLogicArgumentEnum.target_equip_gear_kind_id);
        if (kindId != 0 && (target == null || !BattleFuncs.isGearEquipped(target.originalUnit.playerUnit, kindId)))
          return false;
      }
      if (pse.HasKey(BattleskillEffectLogicArgumentEnum.target_gear_kind_id))
      {
        int num = pse.GetInt(BattleskillEffectLogicArgumentEnum.target_gear_kind_id);
        if (num != 0 && (target == null || num != target.originalUnit.unit.kind.ID))
          return false;
      }
      if (pse.HasKey(BattleskillEffectLogicArgumentEnum.target_exclude_gear_kind_id))
      {
        int num = pse.GetInt(BattleskillEffectLogicArgumentEnum.target_exclude_gear_kind_id);
        if (num != 0 && (target == null || num == target.originalUnit.unit.kind.ID))
          return false;
      }
      if (pse.HasKey(BattleskillEffectLogicArgumentEnum.target_element))
      {
        int num = pse.GetInt(BattleskillEffectLogicArgumentEnum.target_element);
        if (num != 0 && (target == null || (CommonElement) num != target.originalUnit.playerUnit.GetElement()))
          return false;
      }
      if (pse.HasKey(BattleskillEffectLogicArgumentEnum.target_exclude_element))
      {
        int num = pse.GetInt(BattleskillEffectLogicArgumentEnum.target_exclude_element);
        if (num != 0 && (target == null || (CommonElement) num == target.originalUnit.playerUnit.GetElement()))
          return false;
      }
      if (pse.HasKey(BattleskillEffectLogicArgumentEnum.target_job_id))
      {
        int num = pse.GetInt(BattleskillEffectLogicArgumentEnum.target_job_id);
        if (num != 0 && (target == null || num != target.originalUnit.job.ID))
          return false;
      }
      if (pse.HasKey(BattleskillEffectLogicArgumentEnum.target_exclude_job_id))
      {
        int num = pse.GetInt(BattleskillEffectLogicArgumentEnum.target_exclude_job_id);
        if (num != 0 && (target == null || num == target.originalUnit.job.ID))
          return false;
      }
      if (pse.HasKey(BattleskillEffectLogicArgumentEnum.target_family_id))
      {
        int num = pse.GetInt(BattleskillEffectLogicArgumentEnum.target_family_id);
        if (num != 0 && (target == null || !target.originalUnit.playerUnit.HasFamily((UnitFamily) num)))
          return false;
      }
      if (pse.HasKey(BattleskillEffectLogicArgumentEnum.target_exclude_family_id))
      {
        int num = pse.GetInt(BattleskillEffectLogicArgumentEnum.target_exclude_family_id);
        if (num != 0 && (target == null || target.originalUnit.playerUnit.HasFamily((UnitFamily) num)))
          return false;
      }
      if (pse.HasKey(BattleskillEffectLogicArgumentEnum.target_group_large_id))
      {
        int num = pse.GetInt(BattleskillEffectLogicArgumentEnum.target_group_large_id);
        if (num != 0 && (target == null || target.originalUnit.unitGroup == null || num != target.originalUnit.unitGroup.group_large_category_id.ID))
          return false;
      }
      if (pse.HasKey(BattleskillEffectLogicArgumentEnum.target_group_small_id))
      {
        int num = pse.GetInt(BattleskillEffectLogicArgumentEnum.target_group_small_id);
        if (num != 0 && (target == null || target.originalUnit.unitGroup == null || num != target.originalUnit.unitGroup.group_small_category_id.ID))
          return false;
      }
      if (pse.HasKey(BattleskillEffectLogicArgumentEnum.target_group_clothing_id))
      {
        int num = pse.GetInt(BattleskillEffectLogicArgumentEnum.target_group_clothing_id);
        if (num != 0 && (target == null || target.originalUnit.unitGroup == null || num != target.originalUnit.unitGroup.group_clothing_category_id.ID && num != target.originalUnit.unitGroup.group_clothing_category_id_2.ID))
          return false;
      }
      if (pse.HasKey(BattleskillEffectLogicArgumentEnum.target_group_generation_id))
      {
        int num = pse.GetInt(BattleskillEffectLogicArgumentEnum.target_group_generation_id);
        if (num != 0 && (target == null || target.originalUnit.unitGroup == null || num != target.originalUnit.unitGroup.group_generation_category_id.ID))
          return false;
      }
      if (pse.HasKey(BattleskillEffectLogicArgumentEnum.target_skill_group_id))
      {
        int skillGroupId = pse.GetInt(BattleskillEffectLogicArgumentEnum.target_skill_group_id);
        if (skillGroupId != 0 && (target == null || !target.originalUnit.unit.HasSkillGroupId(skillGroupId)))
          return false;
      }
      if (pse.HasKey(BattleskillEffectLogicArgumentEnum.target_exclude_skill_group_id))
      {
        int skillGroupId = pse.GetInt(BattleskillEffectLogicArgumentEnum.target_exclude_skill_group_id);
        if (skillGroupId != 0 && (target == null || target.originalUnit.unit.HasSkillGroupId(skillGroupId)))
          return false;
      }
      if (pse.HasKey(BattleskillEffectLogicArgumentEnum.target_param_type))
      {
        int paramType = pse.GetInt(BattleskillEffectLogicArgumentEnum.target_param_type);
        if (paramType != 0 && pse.GetInt(BattleskillEffectLogicArgumentEnum.param_type) == 0)
        {
          if (target == null)
            return false;
          if (targetNbpCache == null)
            targetNbpCache = new Judgement.NonBattleParameter.FromPlayerUnitCache(target.originalUnit.playerUnit);
          int paramDiffValue = BattleFuncs.GetParamDiffValue(paramType, targetNbpCache, targetHp.HasValue ? targetHp.Value : target.hp);
          if (paramDiffValue < pse.GetInt(BattleskillEffectLogicArgumentEnum.min_value) || paramDiffValue > pse.GetInt(BattleskillEffectLogicArgumentEnum.max_value))
            return false;
        }
      }
      return true;
    }

    public static bool checkInvokeSkillEffectBoth(
      BattleFuncs.PackedSkillEffect pse,
      BL.ISkillEffectListUnit unit,
      BL.ISkillEffectListUnit target,
      Judgement.NonBattleParameter.FromPlayerUnitCache unitNbpCache = null,
      Judgement.NonBattleParameter.FromPlayerUnitCache targetNbpCache = null,
      int? unitHp = null,
      int? targetHp = null)
    {
      if (pse.HasKey(BattleskillEffectLogicArgumentEnum.param_type))
      {
        int paramType1 = pse.GetInt(BattleskillEffectLogicArgumentEnum.param_type);
        int paramType2 = pse.GetInt(BattleskillEffectLogicArgumentEnum.target_param_type);
        if (paramType1 != 0 && paramType2 != 0)
        {
          if (target == null)
            return false;
          if (unitNbpCache == null)
            unitNbpCache = new Judgement.NonBattleParameter.FromPlayerUnitCache(unit.originalUnit.playerUnit);
          if (targetNbpCache == null)
            targetNbpCache = new Judgement.NonBattleParameter.FromPlayerUnitCache(target.originalUnit.playerUnit);
          int paramDiffValue = BattleFuncs.GetParamDiffValue(paramType1, unitNbpCache, unitHp.HasValue ? unitHp.Value : unit.hp);
          int num = BattleFuncs.GetParamDiffValue(paramType2, targetNbpCache, targetHp.HasValue ? targetHp.Value : target.hp) - paramDiffValue;
          if (num < pse.GetInt(BattleskillEffectLogicArgumentEnum.min_value) || num > pse.GetInt(BattleskillEffectLogicArgumentEnum.max_value))
            return false;
        }
      }
      return true;
    }

    public static bool checkInvokeSkillEffect(
      BattleFuncs.PackedSkillEffect pse,
      BL.ISkillEffectListUnit unit,
      BL.ISkillEffectListUnit target = null,
      int? colosseumTurn = null,
      Judgement.NonBattleParameter.FromPlayerUnitCache unitNbpCache = null,
      Judgement.NonBattleParameter.FromPlayerUnitCache targetNbpCache = null,
      int? unitHp = null,
      int? targetHp = null)
    {
      return BattleFuncs.checkInvokeSkillEffectCommon(pse, colosseumTurn) && BattleFuncs.checkInvokeSkillEffectSelf(pse, unit, unitNbpCache, unitHp, false) && BattleFuncs.checkInvokeSkillEffectTarget(pse, target, targetNbpCache, targetHp) && BattleFuncs.checkInvokeSkillEffectBoth(pse, unit, target, unitNbpCache, targetNbpCache, unitHp, targetHp);
    }

    private static int getShieldHp(BL.SkillEffect x, BL.ISkillEffectListUnit unit)
    {
      BattleskillEffect effect = x.effect;
      return effect.GetInt(BattleskillEffectLogicArgumentEnum.value) + x.baseSkillLevel * effect.GetInt(BattleskillEffectLogicArgumentEnum.value_skill_ratio) + Mathf.CeilToInt((float) ((Decimal) unit.originalUnit.parameter.Hp * (Decimal) (effect.GetFloat(BattleskillEffectLogicArgumentEnum.percentage) + (float) x.baseSkillLevel * effect.GetFloat(BattleskillEffectLogicArgumentEnum.percentage_skill_ratio))));
    }

    private static int getShieldRemain(BL.SkillEffect x, BL.ISkillEffectListUnit unit)
    {
      int num = BattleFuncs.getShieldHp(x, unit);
      if (x.work != null)
        num -= (int) x.work[0];
      if (num < 0)
        num = 0;
      return num;
    }

    public static int GetParamDiffValue(
      int paramType,
      Judgement.NonBattleParameter.FromPlayerUnitCache nbpCache,
      int hp)
    {
      int num = 0;
      switch (paramType)
      {
        case 1:
          num = hp;
          break;
        case 2:
          num = nbpCache.parameter.Hp;
          break;
        case 3:
          num = nbpCache.parameter.Strength;
          break;
        case 4:
          num = nbpCache.parameter.Intelligence;
          break;
        case 5:
          num = nbpCache.parameter.Vitality;
          break;
        case 6:
          num = nbpCache.parameter.Mind;
          break;
        case 7:
          num = nbpCache.parameter.Agility;
          break;
        case 8:
          num = nbpCache.parameter.Dexterity;
          break;
        case 9:
          num = nbpCache.parameter.Luck;
          break;
        case 10:
          num = nbpCache.parameter.Hit;
          break;
        case 11:
          num = nbpCache.parameter.Evasion;
          break;
        case 12:
          num = nbpCache.parameter.Critical;
          break;
        case 13:
          num = nbpCache.parameter.Move;
          break;
        case 14:
          num = nbpCache.parameter.PhysicalAttack;
          break;
        case 15:
          num = nbpCache.parameter.MagicAttack;
          break;
        case 16:
          num = nbpCache.parameter.PhysicalDefense;
          break;
        case 17:
          num = nbpCache.parameter.MagicDefense;
          break;
        case 18:
          num = nbpCache.parameter.Combat;
          break;
      }
      return num;
    }

    public static int getRunAwayValue(BL.ISkillEffectListUnit unit)
    {
      return unit.enabledSkillEffect(BattleskillEffectLogicEnum.run_away).Sum<BL.SkillEffect>((Func<BL.SkillEffect, int>) (x => x.effect.GetInt(BattleskillEffectLogicArgumentEnum.add_value)));
    }

    public static bool cantRemoveSkillEffect(BL.SkillEffect e, int investType, int ailmentGroupId)
    {
      BattleskillEffect effect = e.effect;
      if (e.baseSkill.skill_type == BattleskillSkillType.ailment)
      {
        BattleFuncs.PackedSkillEffect packedSkillEffect = BattleFuncs.PackedSkillEffect.Create(e);
        if ((packedSkillEffect.HasKey(BattleskillEffectLogicArgumentEnum.ailment_group_id) ? packedSkillEffect.GetInt(BattleskillEffectLogicArgumentEnum.ailment_group_id) : 0) != ailmentGroupId)
          return true;
      }
      return e.baseSkill.skill_type == BattleskillSkillType.leader || e.baseSkill.skill_type == BattleskillSkillType.passive && e.baseSkill.range_effect_passive_skill || (effect.EffectLogic.Enum == BattleskillEffectLogicEnum.fix_hp || effect.EffectLogic.Enum == BattleskillEffectLogicEnum.ratio_hp || (effect.EffectLogic.Enum == BattleskillEffectLogicEnum.fix_hp2 || effect.EffectLogic.Enum == BattleskillEffectLogicEnum.ratio_hp2)) || (effect.EffectLogic.Enum == BattleskillEffectLogicEnum.fix_hp4 || effect.EffectLogic.Enum == BattleskillEffectLogicEnum.ratio_hp4 || (effect.EffectLogic.Enum == BattleskillEffectLogicEnum.specific_unit_fix_hp || effect.EffectLogic.Enum == BattleskillEffectLogicEnum.specific_unit_ratio_hp) || (effect.EffectLogic.Enum == BattleskillEffectLogicEnum.specific_group_fix_hp || effect.EffectLogic.Enum == BattleskillEffectLogicEnum.specific_group_ratio_hp || (effect.EffectLogic.Enum == BattleskillEffectLogicEnum.specific_group2_fix_hp || effect.EffectLogic.Enum == BattleskillEffectLogicEnum.specific_group2_ratio_hp))) || (effect.EffectLogic.Enum == BattleskillEffectLogicEnum.specific_skill_group_fix_hp || effect.EffectLogic.Enum == BattleskillEffectLogicEnum.specific_skill_group_ratio_hp || (effect.EffectLogic.Enum == BattleskillEffectLogicEnum.attack_class_fix_hp || effect.EffectLogic.Enum == BattleskillEffectLogicEnum.attack_class_ratio_hp) || (effect.EffectLogic.Enum == BattleskillEffectLogicEnum.attack_element_fix_hp || effect.EffectLogic.Enum == BattleskillEffectLogicEnum.attack_element_ratio_hp || (effect.EffectLogic.Enum == BattleskillEffectLogicEnum.suppress_duel_skill || effect.EffectLogic.Enum == BattleskillEffectLogicEnum.steal_effect)) || (effect.EffectLogic.Enum == BattleskillEffectLogicEnum.transformation || effect.EffectLogic.Enum == BattleskillEffectLogicEnum.enemy_multi_damage_value_fluctuate)) || investType != 0 && (investType == 1 && e.isBaseSkill || investType == 2 && !e.isBaseSkill) || e.isDontDisplay;
    }

    public static int calcAttackDamage(
      BL.ISkillEffectListUnit attack,
      BL.ISkillEffectListUnit defense,
      float percentageAttack = 1f,
      float percentageDecrease = 1f,
      float percentageDamage = 1f)
    {
      BL.UnitPosition unitPosition1 = BattleFuncs.iSkillEffectListUnitToUnitPosition(attack);
      BL.UnitPosition unitPosition2 = BattleFuncs.iSkillEffectListUnitToUnitPosition(defense);
      BL.MagicBullet beAttackMagicBullet;
      bool flag;
      if (attack.originalUnit.unit.magic_warrior_flag)
      {
        beAttackMagicBullet = (BL.MagicBullet) null;
        flag = false;
      }
      else
      {
        beAttackMagicBullet = ((IEnumerable<BL.MagicBullet>) attack.originalUnit.magicBullets).Where<BL.MagicBullet>((Func<BL.MagicBullet, bool>) (x => x.isAttack)).OrderBy<BL.MagicBullet, int>((Func<BL.MagicBullet, int>) (x => x.cost)).FirstOrDefault<BL.MagicBullet>();
        flag = beAttackMagicBullet != null;
      }
      Judgement.BeforeDuelParameter duelSkill = Judgement.BeforeDuelParameter.CreateDuelSkill(attack, beAttackMagicBullet, BattleFuncs.getPanel(unitPosition1.row, unitPosition1.column), defense, BattleFuncs.getPanel(unitPosition2.row, unitPosition2.column), 0, defense.hp);
      duelSkill.DamageRate *= percentageDamage;
      if (flag)
      {
        duelSkill.attackerUnitParameter.MagicAttack = (int) Mathf.Ceil((float) duelSkill.attackerUnitParameter.MagicAttack * percentageAttack);
        duelSkill.defenderUnitParameter.MagicDefense = (int) Mathf.Ceil((float) duelSkill.defenderUnitParameter.MagicDefense * percentageDecrease);
        return duelSkill.DisplayMagicAttack;
      }
      duelSkill.attackerUnitParameter.PhysicalAttack = (int) Mathf.Ceil((float) duelSkill.attackerUnitParameter.PhysicalAttack * percentageAttack);
      duelSkill.defenderUnitParameter.PhysicalDefense = (int) Mathf.Ceil((float) duelSkill.defenderUnitParameter.PhysicalDefense * percentageDecrease);
      return duelSkill.DisplayPhysicalAttack;
    }

    public static void applyDamage(
      BL.ISkillEffectListUnit defense,
      int damage,
      BL.ISkillEffectListUnit attack = null,
      int minHp = 0)
    {
      if (damage <= 0)
        return;
      bool flag = defense is BL.AIUnit;
      int hp = defense.hp;
      if (defense.hp > minHp)
      {
        defense.hp -= damage;
        if (defense.hp < minHp)
          defense.hp = minHp;
      }
      if (hp > defense.hp)
        defense.skillEffects.RemoveEffect(1000418, BattleFuncs.env, defense);
      if (defense.hp <= 0)
      {
        if (attack != null)
        {
          if (!flag)
          {
            ++attack.originalUnit.killCount;
            defense.originalUnit.killedBy = attack.originalUnit;
          }
          attack.skillEffects.AddKillCount(1);
        }
        if (!flag)
        {
          if (BattleFuncs.env.getForceID(defense.originalUnit) == BL.ForceID.player)
            BattleFuncs.env.updateIntimateByDefense(defense.originalUnit);
          NGBattleManager instance = Singleton<NGBattleManager>.GetInstance();
          if (instance.useGameEngine)
            instance.gameEngine.applyDeadUnit(defense.originalUnit, (BL.Unit) null);
        }
      }
      if (flag || attack == null || (hp <= defense.hp || attack.originalUnit.isFacility) || defense.originalUnit.isFacility)
        return;
      attack.originalUnit.attackDamage += hp - defense.hp;
    }

    public static bool canHeal(BL.ISkillEffectListUnit unit, BattleskillSkillType skillType = (BattleskillSkillType) 0)
    {
      foreach (BL.SkillEffect skillEffect in unit.skillEffects.Where(BattleskillEffectLogicEnum.heal_impossible))
      {
        int num = skillEffect.effect.GetInt(BattleskillEffectLogicArgumentEnum.skill_type);
        if (num == 0 || (BattleskillSkillType) num == skillType)
          return false;
      }
      return true;
    }

    public static int[] getAttackRange(BL.ISkillEffectListUnit unit)
    {
      int[] numArray = new int[2]{ 1000000, 0 };
      foreach (BL.MagicBullet magicBullet in unit.originalUnit.magicBullets)
      {
        if (magicBullet.isAttack && unit.hp > magicBullet.cost)
        {
          BL.Unit.MagicRange magicRange = BattleFuncs.getMagicRange(unit, magicBullet);
          if (numArray[0] > magicRange.Min)
            numArray[0] = magicRange.Min;
          if (numArray[1] < magicRange.Max)
            numArray[1] = magicRange.Max;
        }
      }
      BL.Unit.GearRange gearRange = BattleFuncs.getGearRange(unit);
      if (numArray[0] > gearRange.Min)
        numArray[0] = gearRange.Min;
      if (numArray[1] < gearRange.Max)
        numArray[1] = gearRange.Max;
      return numArray;
    }

    public static int[] getHealRange(BL.ISkillEffectListUnit unit)
    {
      int[] numArray = new int[2]{ 1000000, 0 };
      foreach (BL.MagicBullet magicBullet in unit.originalUnit.magicBullets)
      {
        if (magicBullet.isHeal && unit.hp > magicBullet.cost)
        {
          BL.Unit.MagicRange magicRange = BattleFuncs.getMagicRange(unit, magicBullet);
          if (numArray[0] > magicRange.Min)
            numArray[0] = magicRange.Min;
          if (numArray[1] < magicRange.Max)
            numArray[1] = magicRange.Max;
        }
      }
      return numArray[0] == 1000000 ? new int[0] : numArray;
    }

    public static BL.Unit.GearRange getGearRange(BL.ISkillEffectListUnit unit)
    {
      IEnumerable<BL.SkillEffect> skillEffects1 = unit.enabledSkillEffect(BattleskillEffectLogicEnum.fix_range).Where<BL.SkillEffect>((Func<BL.SkillEffect, bool>) (x =>
      {
        int num1 = x.effect.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id);
        if (num1 != 0 && num1 != unit.originalUnit.weapon.gear.kind.ID || x.effect.HasKey(BattleskillEffectLogicArgumentEnum.magic_type) && x.effect.GetInt(BattleskillEffectLogicArgumentEnum.magic_type) != 0)
          return false;
        if (x.effect.HasKey(BattleskillEffectLogicArgumentEnum.min_hp_percentage))
        {
          Decimal num2 = (Decimal) unit.hp / (Decimal) unit.originalUnit.parameter.Hp;
          float num3 = x.effect.GetFloat(BattleskillEffectLogicArgumentEnum.min_hp_percentage);
          float num4 = x.effect.GetFloat(BattleskillEffectLogicArgumentEnum.max_hp_percentage);
          if ((double) num3 != 0.0 && num2 < (Decimal) num3 || (double) num4 != 0.0 && num2 >= (Decimal) num4)
            return false;
        }
        return true;
      }));
      IEnumerable<BL.SkillEffect> skillEffects2 = BattleFuncs.gearSkillEffectFilter(unit.originalUnit, skillEffects1);
      int minRange = unit.originalUnit.weapon.gear.min_range;
      int maxRange = unit.originalUnit.weapon.gear.max_range;
      foreach (BL.SkillEffect skillEffect in skillEffects2)
      {
        minRange += skillEffect.effect.GetInt(BattleskillEffectLogicArgumentEnum.min_add);
        maxRange += skillEffect.effect.GetInt(BattleskillEffectLogicArgumentEnum.max_add);
      }
      return new BL.Unit.GearRange(minRange, maxRange);
    }

    public static BL.Unit.MagicRange getMagicRange(
      BL.ISkillEffectListUnit unit,
      BL.MagicBullet mb)
    {
      IEnumerable<BL.SkillEffect> skillEffects1 = unit.enabledSkillEffect(BattleskillEffectLogicEnum.fix_range).Where<BL.SkillEffect>((Func<BL.SkillEffect, bool>) (x =>
      {
        int num1 = x.effect.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id);
        if (num1 != 0 && num1 != unit.originalUnit.weapon.gear.kind.ID)
          return false;
        if (x.effect.HasKey(BattleskillEffectLogicArgumentEnum.min_hp_percentage))
        {
          Decimal num2 = (Decimal) unit.hp / (Decimal) unit.originalUnit.parameter.Hp;
          float num3 = x.effect.GetFloat(BattleskillEffectLogicArgumentEnum.min_hp_percentage);
          float num4 = x.effect.GetFloat(BattleskillEffectLogicArgumentEnum.max_hp_percentage);
          if ((double) num3 != 0.0 && num2 < (Decimal) num3 || (double) num4 != 0.0 && num2 >= (Decimal) num4)
            return false;
        }
        int num5 = x.effect.HasKey(BattleskillEffectLogicArgumentEnum.magic_type) ? x.effect.GetInt(BattleskillEffectLogicArgumentEnum.magic_type) : 0;
        if (num5 == 0 || num5 == 1 && mb.isAttack)
          return true;
        return num5 == 2 && mb.isHeal;
      }));
      IEnumerable<BL.SkillEffect> skillEffects2 = BattleFuncs.gearSkillEffectFilter(unit.originalUnit, skillEffects1);
      int minRange = mb.minRange;
      int maxRange = mb.maxRange;
      foreach (BL.SkillEffect skillEffect in skillEffects2)
      {
        minRange += skillEffect.effect.GetInt(BattleskillEffectLogicArgumentEnum.min_add);
        maxRange += skillEffect.effect.GetInt(BattleskillEffectLogicArgumentEnum.max_add);
      }
      return new BL.Unit.MagicRange(minRange, maxRange);
    }

    public static float calcSpecResistDamageRate(
      BL.ISkillEffectListUnit attack,
      BL.ISkillEffectListUnit defense,
      int argumentCheckValue,
      BattleskillEffectLogicEnum specLogic,
      BattleskillEffectLogicEnum resistLogic,
      BattleskillEffectLogicArgumentEnum specArgument,
      BattleskillEffectLogicArgumentEnum resistArgument)
    {
      List<BattleFuncs.SkillParam> skillParams1 = new List<BattleFuncs.SkillParam>();
      List<BattleFuncs.SkillParam> skillParams2 = new List<BattleFuncs.SkillParam>();
      List<BattleFuncs.SkillParam> skillParams3 = new List<BattleFuncs.SkillParam>();
      List<BattleFuncs.SkillParam> skillParams4 = new List<BattleFuncs.SkillParam>();
      System.Action<BL.ISkillEffectListUnit, BL.ISkillEffectListUnit, BattleskillEffectLogicEnum, BattleskillEffectLogicArgumentEnum, List<BattleFuncs.SkillParam>, List<BattleFuncs.SkillParam>> action = (System.Action<BL.ISkillEffectListUnit, BL.ISkillEffectListUnit, BattleskillEffectLogicEnum, BattleskillEffectLogicArgumentEnum, List<BattleFuncs.SkillParam>, List<BattleFuncs.SkillParam>>) ((effectUnit, targetUnit, logic, argument, buffParams, debuffParams) =>
      {
        foreach (BL.SkillEffect effect in effectUnit.skillEffects.Where(logic, (Func<BL.SkillEffect, bool>) (x =>
        {
          BattleskillEffect effect = x.effect;
          return !BattleFuncs.isSealedSkillEffect(effectUnit, x) && (effect.GetInt(argument) == 0 || effect.GetInt(argument) == argumentCheckValue) && ((effect.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) == 0 || effect.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) == effectUnit.originalUnit.unit.kind.ID) && (effect.GetInt(BattleskillEffectLogicArgumentEnum.target_gear_kind_id) == 0 || effect.GetInt(BattleskillEffectLogicArgumentEnum.target_gear_kind_id) == targetUnit.originalUnit.unit.kind.ID)) && ((effect.GetInt(BattleskillEffectLogicArgumentEnum.element) == 0 || (CommonElement) effect.GetInt(BattleskillEffectLogicArgumentEnum.element) == effectUnit.originalUnit.playerUnit.GetElement()) && (effect.GetInt(BattleskillEffectLogicArgumentEnum.target_element) == 0 || (CommonElement) effect.GetInt(BattleskillEffectLogicArgumentEnum.target_element) == targetUnit.originalUnit.playerUnit.GetElement()) && ((effect.GetInt(BattleskillEffectLogicArgumentEnum.family_id) == 0 || effectUnit.originalUnit.playerUnit.HasFamily((UnitFamily) effect.GetInt(BattleskillEffectLogicArgumentEnum.family_id))) && (effect.GetInt(BattleskillEffectLogicArgumentEnum.target_family_id) == 0 || targetUnit.originalUnit.playerUnit.HasFamily((UnitFamily) effect.GetInt(BattleskillEffectLogicArgumentEnum.target_family_id))))) && !BattleFuncs.isEffectEnemyRangeAndInvalid(x, effectUnit, targetUnit) && !BattleFuncs.isSkillsAndEffectsInvalid(effectUnit, targetUnit, (BL.SkillEffect) null);
        })))
        {
          float mulParam = effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.buff_debuff_value) + (float) effect.baseSkillLevel * effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.skill_ratio);
          BattleFuncs.SkillParam mul = BattleFuncs.SkillParam.CreateMul(effectUnit.originalUnit, effect, mulParam, (object) null, 0);
          if ((double) mulParam > 0.0)
            buffParams.Add(mul);
          else if ((double) mulParam < 0.0)
            debuffParams.Add(mul);
        }
      });
      action(attack, defense, specLogic, specArgument, skillParams1, skillParams2);
      action(defense, attack, resistLogic, resistArgument, skillParams3, skillParams4);
      Decimal num1 = skillParams1.Count > 0 ? (Decimal) BattleFuncs.gearSkillParamFilter(skillParams1).Max<BattleFuncs.SkillParam>((Func<BattleFuncs.SkillParam, float>) (x => x.mulParam.Value)) : new Decimal(0, 0, 0, false, (byte) 1);
      Decimal num2 = skillParams2.Count > 0 ? (Decimal) BattleFuncs.gearSkillParamFilter(skillParams2).Min<BattleFuncs.SkillParam>((Func<BattleFuncs.SkillParam, float>) (x => x.mulParam.Value)) : new Decimal(0, 0, 0, false, (byte) 1);
      Decimal num3 = new Decimal(10, 0, 0, false, (byte) 1) + ((skillParams3.Count > 0 ? (Decimal) BattleFuncs.gearSkillParamFilter(skillParams3).Max<BattleFuncs.SkillParam>((Func<BattleFuncs.SkillParam, float>) (x => x.mulParam.Value)) : new Decimal(0, 0, 0, false, (byte) 1)) + (skillParams4.Count > 0 ? (Decimal) BattleFuncs.gearSkillParamFilter(skillParams4).Min<BattleFuncs.SkillParam>((Func<BattleFuncs.SkillParam, float>) (x => x.mulParam.Value)) : new Decimal(0, 0, 0, false, (byte) 1)) - num1 - num2);
      if (num3 < new Decimal(1, 0, 0, false, (byte) 2))
        num3 = new Decimal(1, 0, 0, false, (byte) 2);
      Decimal d = new Decimal(10, 0, 0, false, (byte) 1) / num3;
      if (d < Decimal.Zero)
        d = new Decimal();
      return (float) Math.Round(d, 4);
    }

    public static float calcAttackClassificationRate(
      AttackStatus attackStatus,
      BL.ISkillEffectListUnit attack,
      BL.ISkillEffectListUnit defense)
    {
      GearAttackClassification? nullable = new GearAttackClassification?();
      if (attackStatus.isMagic)
        nullable = new GearAttackClassification?(GearAttackClassification.magic);
      else if (attackStatus.weapon != null)
      {
        nullable = new GearAttackClassification?(attackStatus.weapon.attackMethod.attackClass);
      }
      else
      {
        GearGear equippedGearOrInitial = attack.originalUnit.playerUnit.equippedGearOrInitial;
        nullable = new GearAttackClassification?(equippedGearOrInitial.hasAttackClass ? equippedGearOrInitial.gearClassification.attack_classification : attack.originalUnit.playerUnit.initial_gear.gearClassification.attack_classification);
      }
      return nullable.HasValue ? BattleFuncs.calcSpecResistDamageRate(attack, defense, (int) nullable.Value, BattleskillEffectLogicEnum.attack_class_spec_ratio, BattleskillEffectLogicEnum.attack_class_resist_ratio, BattleskillEffectLogicArgumentEnum.attack_classification_id, BattleskillEffectLogicArgumentEnum.target_attack_classification_id) : 1f;
    }

    public static Tuple<int, int> getNextCompleteActionCount(
      BL.ISkillEffectListUnit unit,
      BL.UnitPosition up = null,
      BL.ISkillEffectListUnit attack = null,
      BL.ISkillEffectListUnit defense = null,
      int defenseHp = 0,
      bool isSample = false)
    {
      if (up == null)
        up = BattleFuncs.iSkillEffectListUnitToUnitPosition(unit);
      int num1 = up.completedCount;
      int num2 = up.actionCount;
      if (num1 > 1)
      {
        int num3 = 1;
        int num4 = 1;
        BL.PhaseState phaseState = BattleFuncs.getPhaseState();
        foreach (BL.SkillEffect againEffect in unit.skillEffects.GetAgainEffects())
        {
          if ((!againEffect.effect.HasKey(BattleskillEffectLogicArgumentEnum.condition) || againEffect.effect.GetInt(BattleskillEffectLogicArgumentEnum.condition) != 1 || attack != null && defense != null && defenseHp < 1) && BattleFuncs.checkInvokeSkillEffect(BattleFuncs.PackedSkillEffect.Create(againEffect), unit, defense, new int?(), (Judgement.NonBattleParameter.FromPlayerUnitCache) null, (Judgement.NonBattleParameter.FromPlayerUnitCache) null, new int?(), new int?()))
          {
            if (!againEffect.againInvoked)
            {
              if (againEffect.useRemain.HasValue)
              {
                int? useRemain = againEffect.useRemain;
                int num5 = 0;
                if (useRemain.GetValueOrDefault() <= num5 & useRemain.HasValue)
                  continue;
              }
              if (againEffect.effect.HasKey(BattleskillEffectLogicArgumentEnum.turn_invoke_count) && againEffect.effect.GetInt(BattleskillEffectLogicArgumentEnum.turn_invoke_count) != 0 && againEffect.turnCount >= againEffect.effect.GetInt(BattleskillEffectLogicArgumentEnum.turn_invoke_count))
                continue;
            }
            if (!isSample && phaseState != null && (phaseState.absoluteTurnCount >= 1 && !againEffect.againInvoked))
            {
              againEffect.againInvoked = true;
              ++againEffect.turnCount;
              if (againEffect.useRemain.HasValue)
              {
                int? useRemain1 = againEffect.useRemain;
                int num5 = 1;
                if (useRemain1.GetValueOrDefault() >= num5 & useRemain1.HasValue)
                {
                  BL.SkillEffect skillEffect = againEffect;
                  int? useRemain2 = skillEffect.useRemain;
                  skillEffect.useRemain = useRemain2.HasValue ? new int?(useRemain2.GetValueOrDefault() - 1) : new int?();
                }
              }
            }
            int num6 = againEffect.effect.GetInt(BattleskillEffectLogicArgumentEnum.complete_count);
            if (num6 > num3)
              num3 = num6;
            int num7 = againEffect.effect.GetInt(BattleskillEffectLogicArgumentEnum.action_count);
            if (num7 > num4)
              num4 = num7;
          }
        }
        if (num1 > num3)
          num1 = num3;
        if (num2 > num4)
          num2 = num4;
        if (!isSample && num2 > 1 && (up.dontUseSkillAgain && phaseState != null) && phaseState.absoluteTurnCount >= 1)
        {
          int[] numArray = new int[2]
          {
            300003113,
            300003114
          };
          foreach (int index in numArray)
          {
            BattleskillSkill skill = MasterData.BattleskillSkill[index];
            foreach (BattleskillEffect effect in skill.Effects)
              unit.skillEffects.Add(BL.SkillEffect.FromMasterData(effect, skill, 1, false, 0, (BL.Unit) null, 0, true, false, 0), new bool?(), (BL.ISkillEffectListUnit) null);
          }
          if (!(unit is BL.AIUnit))
            unit.originalUnit.commit();
        }
      }
      if (num2 > 0)
        --num2;
      return Tuple.Create<int, int>(num1 - 1, num2);
    }

    public static IEnumerable<BattleskillSkill> getAttackMethodExtraSkill(
      BL.Weapon weapon,
      bool isMagic)
    {
      if (!isMagic && weapon != null)
      {
        foreach (BattleskillEffect battleskillEffect in ((IEnumerable<BattleskillEffect>) weapon.attackMethod.skill.Effects).Where<BattleskillEffect>((Func<BattleskillEffect, bool>) (x => x.EffectLogic.Enum == BattleskillEffectLogicEnum.attack_method_extra_skill)))
        {
          int key = battleskillEffect.GetInt(BattleskillEffectLogicArgumentEnum.skill_id);
          if (MasterData.BattleskillSkill.ContainsKey(key))
            yield return MasterData.BattleskillSkill[key];
        }
      }
    }

    public static IEnumerable<BattleskillSkill> getAttackMethodExtraSkill(
      AttackStatus attackStatus)
    {
      return BattleFuncs.getAttackMethodExtraSkill(attackStatus.weapon, attackStatus.isMagic);
    }

    public static IEnumerable<BL.SkillEffect> getAttackMethodExtraSkillEffects(
      BL.Weapon weapon,
      bool isMagic,
      BattleskillEffectLogicEnum logic)
    {
      foreach (BattleskillSkill battleskillSkill in BattleFuncs.getAttackMethodExtraSkill(weapon, isMagic))
      {
        BattleskillSkill skill = battleskillSkill;
        foreach (BattleskillEffect effect in ((IEnumerable<BattleskillEffect>) skill.Effects).Where<BattleskillEffect>((Func<BattleskillEffect, bool>) (x => x.EffectLogic.Enum == logic)))
          yield return BL.SkillEffect.FromMasterData(effect, skill, 1, false, 0, (BL.Unit) null, 0, false, true, 0);
        skill = (BattleskillSkill) null;
      }
    }

    public static IEnumerable<BL.SkillEffect> getAttackMethodExtraSkillEffects(
      AttackStatus attackStatus,
      BattleskillEffectLogicEnum logic)
    {
      return BattleFuncs.getAttackMethodExtraSkillEffects(attackStatus.weapon, attackStatus.isMagic, logic);
    }

    public static bool checkInvokeGamemode(int invokeGamemode, bool isColosseum)
    {
      if (invokeGamemode == 0)
        return true;
      if (isColosseum)
        return (uint) (invokeGamemode & 2) > 0U;
      if ((invokeGamemode & 1) == 0)
        return false;
      if ((invokeGamemode & 124) == 0)
        return true;
      NGBattleManager instance = Singleton<NGBattleManager>.GetInstance();
      if (instance.isPvp || instance.isPvnpc)
        return (uint) (invokeGamemode & 8) > 0U;
      if (instance.isTower)
        return (uint) (invokeGamemode & 64) > 0U;
      if (instance.isRaid)
        return (uint) (invokeGamemode & 32) > 0U;
      return instance.isGvg ? (uint) (invokeGamemode & 16) > 0U : (uint) (invokeGamemode & 4) > 0U;
    }

    public static bool checkRushInvoke(
      BL.ISkillEffectListUnit myself,
      BL.ISkillEffectListUnit enemy,
      AttackStatus myselfStatus,
      AttackStatus enemyStatus,
      int myselfHp,
      int enemyHp,
      XorShift random,
      int? colosseumTurn = null)
    {
      return myselfStatus != null && myself.skillEffects.Where(BattleskillEffectLogicEnum.rush, (Func<BL.SkillEffect, bool>) (x =>
      {
        BattleFuncs.PackedSkillEffect pse = BattleFuncs.PackedSkillEffect.Create(x);
        if (BattleFuncs.isSealedSkillEffect(myself, x) || !BattleFuncs.checkInvokeSkillEffect(pse, myself, enemy, colosseumTurn, (Judgement.NonBattleParameter.FromPlayerUnitCache) null, (Judgement.NonBattleParameter.FromPlayerUnitCache) null, new int?(myselfHp), new int?(enemyHp)) || (BattleFuncs.isEffectEnemyRangeAndInvalid(x, myself, enemy) || BattleFuncs.isSkillsAndEffectsInvalid(myself, enemy, (BL.SkillEffect) null)))
          return false;
        float percentage_invocation = pse.GetFloat(BattleskillEffectLogicArgumentEnum.percentage_invocation);
        return (double) percentage_invocation >= 200.0 || BattleFuncs.isInvoke(myself, enemy, myselfStatus.duelParameter.attackerUnitParameter, myselfStatus.duelParameter.defenderUnitParameter, myselfStatus, enemyStatus, x.baseSkillLevel, percentage_invocation, random, false, myselfHp, enemyHp, colosseumTurn, new float?(), (BattleskillEffect) null, pse.GetFloat(BattleskillEffectLogicArgumentEnum.base_invocation), pse.GetFloat(BattleskillEffectLogicArgumentEnum.invocation_skill_ratio), pse.GetFloat(BattleskillEffectLogicArgumentEnum.invocation_luck_ratio), (List<BattleFuncs.InvalidSpecificSkillLogic>) null);
      })).Any<BL.SkillEffect>();
    }

    public static bool calcStealEffectParam(
      BL.ISkillEffectListUnit unit,
      BattleskillEffect effect,
      out int param)
    {
      Judgement.Params @params = (Judgement.Params) effect.GetInt(BattleskillEffectLogicArgumentEnum.param_id);
      if (!unit.parameter.GetParamsValue(@params, out param) || @params == Judgement.Params.Hp)
        return false;
      int num1 = Mathf.CeilToInt((float) ((Decimal) param * (Decimal) effect.GetFloat(BattleskillEffectLogicArgumentEnum.percentage) + (Decimal) effect.GetInt(BattleskillEffectLogicArgumentEnum.value)));
      int num2 = effect.GetInt(BattleskillEffectLogicArgumentEnum.limit);
      if (num1 > param)
        num1 = param;
      if (num2 != 0 && num1 > num2)
        num1 = num2;
      param = num1;
      return param > 0;
    }

    public static bool executeSteal(
      BL.ISkillEffectListUnit myself,
      BL.ISkillEffectListUnit target,
      BattleskillEffect effect,
      BL.UnitPosition myselfUp,
      BL.UnitPosition targetUp,
      bool isAI)
    {
      int num1;
      if (!BattleFuncs.calcStealEffectParam(target, effect, out num1))
        return false;
      int key = effect.GetInt(BattleskillEffectLogicArgumentEnum.skill_id);
      if (!MasterData.BattleskillSkill.ContainsKey(key))
        return false;
      BattleskillSkill skill = MasterData.BattleskillSkill[key];
      if (skill.Effects.Length == 0)
        return false;
      BattleskillEffect effect1 = skill.Effects[0];
      if (effect1.effect_logic.Enum != BattleskillEffectLogicEnum.steal_effect)
        return false;
      int num2 = effect.GetInt(BattleskillEffectLogicArgumentEnum.param_id);
      int num3 = effect.GetInt(BattleskillEffectLogicArgumentEnum.turn);
      BL.SkillEffect effect2 = BL.SkillEffect.FromMasterData(effect1, skill, 1, false, 0, myself.originalUnit, 0, false, false, 0);
      myself.skillEffects.Add(effect2, new bool?(), (BL.ISkillEffectListUnit) null);
      effect2.unit = myself.originalUnit;
      if (num3 != 0)
        effect2.turnRemain = new int?(num3);
      effect2.work = new float[2]
      {
        (float) num2,
        (float) num1
      };
      BL.SkillEffect effect3 = BL.SkillEffect.FromMasterData(effect1, skill, 1, false, 0, myself.originalUnit, 0, false, false, 0);
      target.skillEffects.Add(effect3, new bool?(), (BL.ISkillEffectListUnit) null);
      effect3.unit = myself.originalUnit;
      if (num3 != 0)
        effect3.turnRemain = new int?(num3);
      effect3.work = new float[2]
      {
        (float) num2,
        (float) -num1
      };
      if (num2 == 8)
      {
        myselfUp.clearMovePanelCache();
        targetUp.clearMovePanelCache();
      }
      if (!isAI)
      {
        myself.originalUnit.commit();
        target.originalUnit.commit();
      }
      return true;
    }

    public static void removeStealEffects(BL.ISkillEffectListUnit deadUnit)
    {
      bool isAI = deadUnit is BL.AIUnit;
      foreach (BL.ISkillEffectListUnit allUnit in BattleFuncs.getAllUnits(isAI, false, false))
      {
        if (allUnit.skillEffects.Where(BattleskillEffectLogicEnum.steal_effect).Any<BL.SkillEffect>((Func<BL.SkillEffect, bool>) (x => x.timeOfDeathDisable && x.unit == deadUnit.originalUnit)))
        {
          allUnit.skillEffects.RemoveEffect(1001676, 0, 0, BattleFuncs.env, allUnit, (Func<BL.SkillEffect, bool>) (x => !x.timeOfDeathDisable || x.unit != deadUnit.originalUnit));
          if (!isAI)
            allUnit.originalUnit.commit();
        }
      }
    }

    public static int getTransformationGroupId(BL.ISkillEffectListUnit unit)
    {
      BL.SkillEffect skillEffect = unit.skillEffects.Where(BattleskillEffectLogicEnum.transformation).FirstOrDefault<BL.SkillEffect>();
      return skillEffect == null ? 0 : skillEffect.effect.GetInt(BattleskillEffectLogicArgumentEnum.transformation_group_id);
    }

    public static SkillMetamorphosis getMetamorphosis(BL.ISkillEffectListUnit unit)
    {
      BL.SkillEffect skillEffect = unit.skillEffects.Where(BattleskillEffectLogicEnum.transformation).FirstOrDefault<BL.SkillEffect>();
      return skillEffect == null ? (SkillMetamorphosis) null : MasterData.FindSkillMetamorphosis(unit.originalUnit.unitId, skillEffect.baseSkillId, skillEffect.effect.GetInt(BattleskillEffectLogicArgumentEnum.transformation_group_id));
    }

    public static bool checkEnableUnitSkill(BL.ISkillEffectListUnit unit, BattleskillSkill skill)
    {
      if (!skill.transformationGroupId.HasValue)
        return true;
      int transformationGroupId1 = BattleFuncs.getTransformationGroupId(unit);
      int? transformationGroupId2 = skill.transformationGroupId;
      int num = transformationGroupId1;
      return transformationGroupId2.GetValueOrDefault() == num & transformationGroupId2.HasValue;
    }

    public static bool checkSkillLogicInvest(
      BL.ISkillEffectListUnit unit,
      BL.ISkillEffectListUnit target,
      int logicId,
      int ailmentGroupId,
      int skillId,
      int skillType,
      int investType,
      int conditionTarget)
    {
      BL.ISkillEffectListUnit checkUnit = conditionTarget == 0 ? unit : target;
      return (logicId == 0 ? (IEnumerable<BL.SkillEffect>) checkUnit.skillEffects.All() : checkUnit.skillEffects.Where((BattleskillEffectLogicEnum) logicId)).Any<BL.SkillEffect>((Func<BL.SkillEffect, bool>) (x =>
      {
        if (x.useRemain.HasValue && x.useRemain.Value <= 0 || (logicId == 1001338 && x.work != null && BattleFuncs.getShieldRemain(x, checkUnit) <= 0 || (skillId != 0 && skillId != x.baseSkill.ID || skillType != 0 && (BattleskillSkillType) skillType != x.baseSkill.skill_type) || investType != 0 && (investType == 1 && x.isBaseSkill || investType == 2 && !x.isBaseSkill)))
          return false;
        if (x.baseSkill.skill_type == BattleskillSkillType.ailment)
        {
          BattleFuncs.PackedSkillEffect packedSkillEffect = BattleFuncs.PackedSkillEffect.Create(x);
          if ((packedSkillEffect.HasKey(BattleskillEffectLogicArgumentEnum.ailment_group_id) ? packedSkillEffect.GetInt(BattleskillEffectLogicArgumentEnum.ailment_group_id) : 0) != ailmentGroupId)
            return false;
        }
        return true;
      }));
    }

    public static List<BL.Unit> getProvokeUnits(BL.ISkillEffectListUnit unit)
    {
      if (unit.IsCharm)
        return (List<BL.Unit>) null;
      IEnumerable<BL.SkillEffect> source1 = unit.skillEffects.Where(BattleskillEffectLogicEnum.provoke);
      if (!source1.Any<BL.SkillEffect>())
        return (List<BL.Unit>) null;
      List<BL.Unit> source2 = new List<BL.Unit>();
      bool flag = unit is BL.AIUnit;
      foreach (BL.SkillEffect skillEffect in source1)
      {
        BL.Unit investUnit = skillEffect.investUnit;
        if (!(investUnit == (BL.Unit) null))
        {
          if (!flag)
          {
            if (!investUnit.isDead)
              source2.Add(investUnit);
          }
          else
          {
            BL.AIUnit aiUnit = BattleFuncs.env.getAIUnit(investUnit);
            if (aiUnit != null && !aiUnit.isDead)
              source2.Add(investUnit);
          }
        }
      }
      return !source2.Any<BL.Unit>() ? (List<BL.Unit>) null : source2;
    }

    public static IEnumerable<BL.SkillEffect> getImmediateRebirthEffects(
      BL.ISkillEffectListUnit unit)
    {
      return (IEnumerable<BL.SkillEffect>) unit.skillEffects.Where(BattleskillEffectLogicEnum.immediate_rebirth_fix).Concat<BL.SkillEffect>(unit.skillEffects.Where(BattleskillEffectLogicEnum.immediate_rebirth_ratio)).Where<BL.SkillEffect>((Func<BL.SkillEffect, bool>) (x => (!x.useRemain.HasValue || x.useRemain.Value >= 1) && !BattleFuncs.isSealedSkillEffect(unit, x))).OrderBy<BL.SkillEffect, int>((Func<BL.SkillEffect, int>) (x => x.effectId)).OrderByDescending<BL.SkillEffect, int>((Func<BL.SkillEffect, int>) (x => x.baseSkill.weight)).ThenBy<BL.SkillEffect, int>((Func<BL.SkillEffect, int>) (x => x.effectId));
    }

    public static void useImmediateRebirthEffect(
      BL.ISkillEffectListUnit unit,
      BL.SkillEffect immediateRebirthEffect)
    {
      BattleskillEffect effect = immediateRebirthEffect.effect;
      if (effect.EffectLogic.Enum == BattleskillEffectLogicEnum.immediate_rebirth_fix)
      {
        unit.hp = effect.GetInt(BattleskillEffectLogicArgumentEnum.value) + immediateRebirthEffect.baseSkillLevel * effect.GetInt(BattleskillEffectLogicArgumentEnum.skill_ratio);
      }
      else
      {
        Decimal num = (Decimal) effect.GetFloat(BattleskillEffectLogicArgumentEnum.percentage) + (Decimal) immediateRebirthEffect.baseSkillLevel * (Decimal) effect.GetFloat(BattleskillEffectLogicArgumentEnum.skill_ratio);
        unit.hp = (int) Math.Ceiling((Decimal) unit.originalUnit.parameter.Hp * num);
      }
      int? useRemain = immediateRebirthEffect.useRemain;
      if (!useRemain.HasValue)
        return;
      BL.SkillEffect skillEffect = immediateRebirthEffect;
      useRemain = skillEffect.useRemain;
      int? nullable = useRemain;
      skillEffect.useRemain = nullable.HasValue ? new int?(nullable.GetValueOrDefault() - 1) : new int?();
    }

    public static void createAsterNodeCache(BL blenv)
    {
      List<BL.Panel> panelList = new List<BL.Panel>();
      for (int row = 0; row < blenv.getFieldHeight(); ++row)
      {
        for (int column = 0; column < blenv.getFieldWidth(); ++column)
        {
          if (blenv.getFieldPanel(row, column) != null)
            panelList.Add(blenv.getFieldPanel(row, column));
          else
            Debug.LogError((object) (" === ouch! フィールドデータがおかしい(" + (object) row + ", " + (object) column + ")"));
        }
      }
      for (int index = 0; index < 2; ++index)
      {
        Dictionary<UnitMoveType, BattleFuncs.AsterNode[]> dictionary = new Dictionary<UnitMoveType, BattleFuncs.AsterNode[]>();
        foreach (BL.UnitPosition unitPosition in blenv.unitPositions.value)
        {
          if (!unitPosition.unit.isFacility)
          {
            if (unitPosition.asterNodeCache == null)
              unitPosition.asterNodeCache = new BattleFuncs.AsterNode[2][];
            if (dictionary.ContainsKey(unitPosition.unit.job.move_type))
            {
              unitPosition.asterNodeCache[index] = dictionary[unitPosition.unit.job.move_type];
            }
            else
            {
              unitPosition.asterNodeCache[index] = BattleFuncs.createNodes((IEnumerable<BL.Panel>) panelList, unitPosition.unit, (BL.Panel) null, (BL.Panel) null, out int _, out int _, index == 1);
              dictionary[unitPosition.unit.job.move_type] = unitPosition.asterNodeCache[index];
            }
          }
        }
      }
    }

    public static HashSet<BL.ISkillEffectListUnit> applyDuelSkillEffects(
      DuelResult duelResult,
      BL.ISkillEffectListUnit atk,
      BL.ISkillEffectListUnit def,
      BL env,
      System.Action<BL.ISkillEffectListUnit, int> addHpFunction = null,
      System.Action<BL.ISkillEffectListUnit, int> subHpFunction = null,
      System.Action<BL.ISkillEffectListUnit, int> penetrateHpFunction = null,
      System.Action<BL.ISkillEffectListUnit, int> rangeAttackHpFunction = null,
      System.Action<BL.ISkillEffectListUnit, int> damageShareHpFunction = null,
      System.Action<BattleskillSkill, BL.Unit, Dictionary<BL.Unit, Tuple<int, int>>> snakeFunction = null,
      Dictionary<Tuple<BL.Unit, BattleskillSkill>, List<BL.Unit>> lifeAbsorbSkillTarget = null,
      Dictionary<Tuple<BL.Unit, BattleskillSkill>, List<BL.Unit>> curseReflectionSkillTarget = null,
      Dictionary<Tuple<BL.Unit, BattleskillSkill>, Tuple<List<BL.Unit>, float>> penetrateSkillTarget = null,
      Dictionary<Tuple<BL.Unit, BattleskillSkill>, List<BL.Unit>> rangeAttackSkillTarget = null,
      Dictionary<BL.Unit, HashSet<BattleskillSkill>> damageShareSkillTarget = null,
      Dictionary<Tuple<BL.Unit, BattleskillSkill>, List<BL.Unit>> stealSkillTarget = null)
    {
      if (duelResult.disableDuelSkillEffects)
        return new HashSet<BL.ISkillEffectListUnit>();
      BattleFuncs.apllyDuelRemoveSleepEffect(duelResult, atk, def);
      BattleFuncs.consumeSkillEffects(duelResult.turns, atk, def);
      bool isAI = atk is BL.AIUnit;
      BattleFuncs.ApplyChangeSkillEffects changeSkillEffects = new BattleFuncs.ApplyChangeSkillEffects(isAI);
      BL.ISkillEffectListUnit skillEffectListUnit1 = isAI ? (BL.ISkillEffectListUnit) env.getAIUnit(duelResult.moveUnit) : (BL.ISkillEffectListUnit) duelResult.moveUnit;
      BL.SkillEffect[] array = skillEffectListUnit1.skillEffects.All().ToArray();
      foreach (BL.DuelTurn turn in duelResult.turns)
      {
        for (int index = 0; index < turn.investUnit.Length; ++index)
        {
          BL.ISkillEffectListUnit originalUnit1 = turn.investUnit[index];
          BL.Unit originalUnit2 = turn.investFrom[index].originalUnit;
          BL.Skill skill = new BL.Skill()
          {
            id = turn.investSkillIds[index]
          };
          BL.ISkillEffectListUnit skillEffectListUnit2 = turn.isAtackker ? def : atk;
          BL.UnitPosition tup = originalUnit1 is BL.AIUnit ? originalUnit1 as BL.UnitPosition : env.getUnitPosition(originalUnit1.originalUnit);
          if (!isAI)
          {
            originalUnit1 = (BL.ISkillEffectListUnit) originalUnit1.originalUnit;
            tup = env.getUnitPosition(originalUnit1.originalUnit);
            originalUnit1.originalUnit.commit();
          }
          if (!originalUnit1.originalUnit.isDead)
          {
            changeSkillEffects.add(tup, originalUnit1);
            if (skill.skill.target_type == BattleskillTargetType.complex_single || skill.skill.target_type == BattleskillTargetType.complex_range)
            {
              bool flag = env.getForceID(originalUnit1.originalUnit) == env.getForceID(skillEffectListUnit2.originalUnit);
              foreach (BattleskillEffect effect in skill.skill.Effects)
              {
                if (effect.EffectLogic.Enum != BattleskillEffectLogicEnum.snake_venom)
                {
                  if (!effect.is_targer_enemy)
                  {
                    if (!flag)
                      originalUnit1.skillEffects.Add(BL.SkillEffect.FromMasterData(effect, skill.skill, 1, false, 0, originalUnit2, turn.investFromSkillIds[index], false, false, env.phaseState.absoluteTurnCount), new bool?(), originalUnit1);
                  }
                  else if (flag)
                    originalUnit1.skillEffects.Add(BL.SkillEffect.FromMasterData(effect, skill.skill, 1, false, 0, originalUnit2, turn.investFromSkillIds[index], false, false, env.phaseState.absoluteTurnCount), new bool?(), originalUnit1);
                }
              }
            }
            else
            {
              foreach (BattleskillEffect effect in skill.skill.Effects)
              {
                if (effect.EffectLogic.Enum != BattleskillEffectLogicEnum.snake_venom)
                  originalUnit1.skillEffects.Add(BL.SkillEffect.FromMasterData(effect, skill.skill, 1, false, 0, originalUnit2, turn.investFromSkillIds[index], false, false, env.phaseState.absoluteTurnCount), new bool?(), originalUnit1);
              }
            }
          }
        }
      }
      changeSkillEffects.execute();
      foreach (BL.SkillEffect skillEffect in skillEffectListUnit1.skillEffects.All().Except<BL.SkillEffect>((IEnumerable<BL.SkillEffect>) array))
        skillEffect.moveDistance = new int?();
      HashSet<BL.ISkillEffectListUnit> deads = new HashSet<BL.ISkillEffectListUnit>();
      if (!duelResult.isHeal)
      {
        BattleFuncs.applyDuelDamageSkillEffects(duelResult, atk, def, env, deads, addHpFunction, subHpFunction, penetrateHpFunction, rangeAttackHpFunction, damageShareHpFunction, snakeFunction, lifeAbsorbSkillTarget, curseReflectionSkillTarget, penetrateSkillTarget, rangeAttackSkillTarget, damageShareSkillTarget, stealSkillTarget);
        HashSet<BL.ISkillEffectListUnit> isInvokedSnakeVenom = new HashSet<BL.ISkillEffectListUnit>();
        Dictionary<BL.ISkillEffectListUnit, BattleskillEffect> snakeVenomUnitEffect = new Dictionary<BL.ISkillEffectListUnit, BattleskillEffect>();
        BL.Skill snakeVenomSkill = (BL.Skill) null;
        Func<BL.Skill, bool> func;
        BattleFuncs.mapDuelSkillEffects(duelResult, atk, def, env, (System.Action<Tuple<BL.DuelTurn, BL.ISkillEffectListUnit, BL.ISkillEffectListUnit, BL.ISkillEffectListUnit, BL.Skill>>) null, (System.Action<Tuple<BL.DuelTurn, BL.ISkillEffectListUnit, BL.ISkillEffectListUnit, BL.ISkillEffectListUnit, BL.Skill, bool, bool>>) (data =>
        {
          foreach (BattleskillEffect effect in data.Item5.skill.Effects)
          {
            if (effect.EffectLogic.Enum == BattleskillEffectLogicEnum.snake_venom && (!data.Item6 || effect.is_targer_enemy == data.Item7))
            {
              snakeVenomUnitEffect[data.Item4] = effect;
              snakeVenomSkill = data.Item5;
            }
          }
        }), (System.Action<Tuple<BL.DuelTurn, BL.ISkillEffectListUnit, BL.ISkillEffectListUnit, IEnumerable<BL.UnitPosition>, BL.Skill, bool, bool>>) (data =>
        {
          foreach (BL.UnitPosition unitPosition in data.Item4)
          {
            BL.ISkillEffectListUnit index = unitPosition is BL.ISkillEffectListUnit ? unitPosition as BL.ISkillEffectListUnit : (BL.ISkillEffectListUnit) unitPosition.unit;
            foreach (BattleskillEffect effect in data.Item5.skill.Effects)
            {
              if (effect.EffectLogic.Enum == BattleskillEffectLogicEnum.snake_venom && (!data.Item6 || effect.is_targer_enemy == data.Item7))
              {
                if (index != data.Item3)
                  snakeVenomUnitEffect[index] = effect;
                snakeVenomSkill = data.Item5;
              }
            }
          }
        }), (System.Action<BL.DuelTurn, BL.ISkillEffectListUnit>) ((turn, myself) =>
        {
          if (!isInvokedSnakeVenom.Contains(myself) && snakeVenomSkill != null && snakeVenomUnitEffect.Count >= 1)
          {
            Dictionary<BL.Unit, Tuple<int, int>> dictionary = snakeFunction != null ? new Dictionary<BL.Unit, Tuple<int, int>>() : (Dictionary<BL.Unit, Tuple<int, int>>) null;
            isInvokedSnakeVenom.Add(myself);
            int num = ((IEnumerable<BL.DuelTurn>) duelResult.turns).Sum<BL.DuelTurn>((Func<BL.DuelTurn, int>) (x => x.isAtackker != turn.isAtackker || !((IEnumerable<BL.Skill>) x.invokeDuelSkills).Any<BL.Skill>(func ?? (func = (Func<BL.Skill, bool>) (skill => skill == snakeVenomSkill))) ? 0 : x.realDamage));
            foreach (BL.ISkillEffectListUnit key in snakeVenomUnitEffect.Keys)
            {
              int hp = key.hp;
              BattleskillEffect effect = snakeVenomUnitEffect[key];
              if (hp >= 1)
              {
                int minHp = effect.GetInt(BattleskillEffectLogicArgumentEnum.min_hp);
                int damage1 = effect.GetInt(BattleskillEffectLogicArgumentEnum.venom_value) + Mathf.CeilToInt((float) key.originalUnit.parameter.Hp * effect.GetFloat(BattleskillEffectLogicArgumentEnum.venom_hp_percentage)) + Mathf.CeilToInt((float) num * effect.GetFloat(BattleskillEffectLogicArgumentEnum.venom_damage_percentage));
                int damage2 = BattleFuncs.applyFieldDamageFluctuate(atk, def, myself, key, effect, damage1);
                BattleFuncs.applyHpDamage(atk, def, myself, key, damage2, minHp, deads, (System.Action<BL.ISkillEffectListUnit, int>) null);
                if (dictionary != null)
                  dictionary[key as BL.Unit] = new Tuple<int, int>(hp, key.hp);
              }
            }
            if (dictionary != null)
              snakeFunction(snakeVenomSkill.skill, myself as BL.Unit, dictionary);
          }
          snakeVenomUnitEffect.Clear();
          snakeVenomSkill = (BL.Skill) null;
        }), true);
      }
      return deads;
    }

    private static void apllyDuelRemoveSleepEffect(
      DuelResult duelResult,
      BL.ISkillEffectListUnit atk,
      BL.ISkillEffectListUnit def)
    {
      if (atk != null && ((IEnumerable<BL.DuelTurn>) duelResult.turns).Any<BL.DuelTurn>((Func<BL.DuelTurn, bool>) (x => !x.isAtackker && x.dispDamage >= 1)))
        atk.skillEffects.RemoveEffect(1000418, BattleFuncs.env, atk);
      if (def == null || !((IEnumerable<BL.DuelTurn>) duelResult.turns).Any<BL.DuelTurn>((Func<BL.DuelTurn, bool>) (x => x.isAtackker && x.dispDamage >= 1)))
        return;
      def.skillEffects.RemoveEffect(1000418, BattleFuncs.env, atk);
    }

    private static void mapDuelSkillEffects(
      DuelResult duelResult,
      BL.ISkillEffectListUnit atk,
      BL.ISkillEffectListUnit def,
      BL env,
      System.Action<Tuple<BL.DuelTurn, BL.ISkillEffectListUnit, BL.ISkillEffectListUnit, BL.ISkillEffectListUnit, BL.Skill>> mapAlimentFunction,
      System.Action<Tuple<BL.DuelTurn, BL.ISkillEffectListUnit, BL.ISkillEffectListUnit, BL.ISkillEffectListUnit, BL.Skill, bool, bool>> mapGiveOneFunction,
      System.Action<Tuple<BL.DuelTurn, BL.ISkillEffectListUnit, BL.ISkillEffectListUnit, IEnumerable<BL.UnitPosition>, BL.Skill, bool, bool>> mapGiveMultiFunction,
      System.Action<BL.DuelTurn, BL.ISkillEffectListUnit> turnFunction,
      bool isCheckSingleRange)
    {
      foreach (BL.DuelTurn turn in duelResult.turns)
      {
        BL.ISkillEffectListUnit myself;
        BL.ISkillEffectListUnit enemy;
        if (turn.isAtackker)
        {
          myself = atk;
          enemy = def;
        }
        else
        {
          myself = def;
          enemy = atk;
        }
        if (mapAlimentFunction != null && turn.invokeAilmentSkills != null)
        {
          BL.ISkillEffectListUnit skillEffectListUnit = turn.isAtackker ? def : atk;
          foreach (BL.Skill invokeAilmentSkill in turn.invokeAilmentSkills)
            mapAlimentFunction(new Tuple<BL.DuelTurn, BL.ISkillEffectListUnit, BL.ISkillEffectListUnit, BL.ISkillEffectListUnit, BL.Skill>(turn, myself, enemy, skillEffectListUnit, invokeAilmentSkill));
        }
        if (mapGiveOneFunction != null && mapGiveMultiFunction != null && turn.invokeGiveSkills != null)
        {
          BL.ForceID[] forceIds1 = new BL.ForceID[1]
          {
            env.getForceID(myself.originalUnit)
          };
          BL.ForceID[] forceIds2 = new BL.ForceID[1]
          {
            env.getForceID(enemy.originalUnit)
          };
          int[] range = new int[2];
          foreach (BL.Skill invokeGiveSkill in turn.invokeGiveSkills)
          {
            range[0] = invokeGiveSkill.skill.min_range;
            range[1] = invokeGiveSkill.skill.max_range;
            BL.ISkillEffectListUnit unit = myself;
            if (((IEnumerable<BattleskillEffect>) invokeGiveSkill.skill.Effects).Where<BattleskillEffect>((Func<BattleskillEffect, bool>) (x => x.EffectLogic.Enum == BattleskillEffectLogicEnum.snake_venom)).Any<BattleskillEffect>((Func<BattleskillEffect, bool>) (x => (uint) x.GetInt(BattleskillEffectLogicArgumentEnum.is_range_from_enemy) > 0U)))
              unit = enemy;
            switch (invokeGiveSkill.skill.target_type)
            {
              case BattleskillTargetType.myself:
              case BattleskillTargetType.player_single:
                if (!isCheckSingleRange || range[0] == 0 && range[1] == 0 || BattleFuncs.getRangeTargets(unit, range, forceIds1, true).Any<BL.UnitPosition>((Func<BL.UnitPosition, bool>) (up => (up is BL.ISkillEffectListUnit ? up as BL.ISkillEffectListUnit : (BL.ISkillEffectListUnit) up.unit) == myself)))
                {
                  mapGiveOneFunction(new Tuple<BL.DuelTurn, BL.ISkillEffectListUnit, BL.ISkillEffectListUnit, BL.ISkillEffectListUnit, BL.Skill, bool, bool>(turn, myself, enemy, myself, invokeGiveSkill, false, false));
                  break;
                }
                break;
              case BattleskillTargetType.player_range:
                List<BL.UnitPosition> list1 = BattleFuncs.getRangeTargets(unit, range, forceIds1, true).ToList<BL.UnitPosition>();
                mapGiveMultiFunction(new Tuple<BL.DuelTurn, BL.ISkillEffectListUnit, BL.ISkillEffectListUnit, IEnumerable<BL.UnitPosition>, BL.Skill, bool, bool>(turn, myself, enemy, (IEnumerable<BL.UnitPosition>) list1, invokeGiveSkill, false, false));
                break;
              case BattleskillTargetType.enemy_single:
                if (!isCheckSingleRange || range[0] == 0 && range[1] == 0 || BattleFuncs.getRangeTargets(unit, range, forceIds2, true).Any<BL.UnitPosition>((Func<BL.UnitPosition, bool>) (up => (up is BL.ISkillEffectListUnit ? up as BL.ISkillEffectListUnit : (BL.ISkillEffectListUnit) up.unit) == enemy)))
                {
                  mapGiveOneFunction(new Tuple<BL.DuelTurn, BL.ISkillEffectListUnit, BL.ISkillEffectListUnit, BL.ISkillEffectListUnit, BL.Skill, bool, bool>(turn, myself, enemy, enemy, invokeGiveSkill, false, false));
                  break;
                }
                break;
              case BattleskillTargetType.enemy_range:
                List<BL.UnitPosition> list2 = BattleFuncs.getRangeTargets(unit, range, forceIds2, true).ToList<BL.UnitPosition>();
                mapGiveMultiFunction(new Tuple<BL.DuelTurn, BL.ISkillEffectListUnit, BL.ISkillEffectListUnit, IEnumerable<BL.UnitPosition>, BL.Skill, bool, bool>(turn, myself, enemy, (IEnumerable<BL.UnitPosition>) list2, invokeGiveSkill, false, true));
                break;
              case BattleskillTargetType.complex_single:
                bool flag1 = true;
                bool flag2 = true;
                if (isCheckSingleRange && (range[0] != 0 || range[1] != 0))
                {
                  flag1 = BattleFuncs.getRangeTargets(unit, range, forceIds2, true).Any<BL.UnitPosition>((Func<BL.UnitPosition, bool>) (up => (up is BL.ISkillEffectListUnit ? up as BL.ISkillEffectListUnit : (BL.ISkillEffectListUnit) up.unit) == enemy));
                  flag2 = BattleFuncs.getRangeTargets(unit, range, forceIds1, true).Any<BL.UnitPosition>((Func<BL.UnitPosition, bool>) (up => (up is BL.ISkillEffectListUnit ? up as BL.ISkillEffectListUnit : (BL.ISkillEffectListUnit) up.unit) == myself));
                }
                if (flag1)
                  mapGiveOneFunction(new Tuple<BL.DuelTurn, BL.ISkillEffectListUnit, BL.ISkillEffectListUnit, BL.ISkillEffectListUnit, BL.Skill, bool, bool>(turn, myself, enemy, enemy, invokeGiveSkill, true, true));
                if (flag2)
                {
                  mapGiveOneFunction(new Tuple<BL.DuelTurn, BL.ISkillEffectListUnit, BL.ISkillEffectListUnit, BL.ISkillEffectListUnit, BL.Skill, bool, bool>(turn, myself, enemy, myself, invokeGiveSkill, true, false));
                  break;
                }
                break;
              case BattleskillTargetType.complex_range:
                List<BL.UnitPosition> list3 = BattleFuncs.getRangeTargets(unit, range, forceIds1, true).ToList<BL.UnitPosition>();
                List<BL.UnitPosition> list4 = BattleFuncs.getRangeTargets(unit, range, forceIds2, true).ToList<BL.UnitPosition>();
                mapGiveMultiFunction(new Tuple<BL.DuelTurn, BL.ISkillEffectListUnit, BL.ISkillEffectListUnit, IEnumerable<BL.UnitPosition>, BL.Skill, bool, bool>(turn, myself, enemy, (IEnumerable<BL.UnitPosition>) list4, invokeGiveSkill, true, true));
                mapGiveMultiFunction(new Tuple<BL.DuelTurn, BL.ISkillEffectListUnit, BL.ISkillEffectListUnit, IEnumerable<BL.UnitPosition>, BL.Skill, bool, bool>(turn, myself, enemy, (IEnumerable<BL.UnitPosition>) list3, invokeGiveSkill, true, false));
                break;
            }
          }
        }
        if (turnFunction != null)
          turnFunction(turn, myself);
      }
    }

    private static void applyDuelDamageSkillEffects(
      DuelResult duelResult,
      BL.ISkillEffectListUnit atk,
      BL.ISkillEffectListUnit def,
      BL env,
      HashSet<BL.ISkillEffectListUnit> deads,
      System.Action<BL.ISkillEffectListUnit, int> addHpFunction,
      System.Action<BL.ISkillEffectListUnit, int> subHpFunction,
      System.Action<BL.ISkillEffectListUnit, int> penetrateHpFunction,
      System.Action<BL.ISkillEffectListUnit, int> rangeAttackHpFunction,
      System.Action<BL.ISkillEffectListUnit, int> damageShareHpFunction,
      System.Action<BattleskillSkill, BL.Unit, Dictionary<BL.Unit, Tuple<int, int>>> snakeFunction,
      Dictionary<Tuple<BL.Unit, BattleskillSkill>, List<BL.Unit>> lifeAbsorbSkillTarget,
      Dictionary<Tuple<BL.Unit, BattleskillSkill>, List<BL.Unit>> curseReflectionSkillTarget,
      Dictionary<Tuple<BL.Unit, BattleskillSkill>, Tuple<List<BL.Unit>, float>> penetrateSkillTarget,
      Dictionary<Tuple<BL.Unit, BattleskillSkill>, List<BL.Unit>> rangeAttackSkillTarget,
      Dictionary<BL.Unit, HashSet<BattleskillSkill>> damageShareSkillTarget,
      Dictionary<Tuple<BL.Unit, BattleskillSkill>, List<BL.Unit>> stealSkillTarget)
    {
      bool isAI = atk is BL.AIUnit;
      bool isInvokedAmbush = duelResult.moveUnit != atk.originalUnit;
      BL.ISkillEffectListUnit myself = !isInvokedAmbush ? atk : def;
      BL.ISkillEffectListUnit enemy = !isInvokedAmbush ? def : atk;
      int is_attack = 1;
      BL.ForceID[] forceIds1 = new BL.ForceID[1]
      {
        env.getForceID(myself.originalUnit)
      };
      BL.ForceID[] forceIds2 = new BL.ForceID[1]
      {
        env.getForceID(enemy.originalUnit)
      };
      int[] iWork = new int[1];
      Dictionary<BL.ISkillEffectListUnit, int> lifeAbsorbTargetHeal = new Dictionary<BL.ISkillEffectListUnit, int>();
      BL.ISkillEffectListUnit[] curseReflectionInvokeUnit = new BL.ISkillEffectListUnit[2];
      Dictionary<BL.ISkillEffectListUnit, int>[] curseReflectionTargetDamage = new Dictionary<BL.ISkillEffectListUnit, int>[2];
      List<BattleFuncs.SnakeVenomDamageData> snakeVenomDamage = new List<BattleFuncs.SnakeVenomDamageData>();
      BL.ISkillEffectListUnit[] skillEffectListUnitArray = new BL.ISkillEffectListUnit[2];
      Dictionary<BL.ISkillEffectListUnit, int>[] dictionaryArray = new Dictionary<BL.ISkillEffectListUnit, int>[2];
      int minHp1 = int.MaxValue;
      BL.ISkillEffectListUnit[] rangeAttackInvokeUnit = new BL.ISkillEffectListUnit[2];
      Dictionary<BL.ISkillEffectListUnit, int>[] rangeAttackTargetDamage = new Dictionary<BL.ISkillEffectListUnit, int>[2];
      int rangeAttackMinHp = int.MaxValue;
      BL.UnitPosition unitPosition1 = BattleFuncs.iSkillEffectListUnitToUnitPosition(myself);
      BL.UnitPosition unitPosition2 = BattleFuncs.iSkillEffectListUnitToUnitPosition(enemy);
      BL.UnitPosition[] unitPositionArray1 = new BL.UnitPosition[2]
      {
        unitPosition1,
        unitPosition2
      };
      int[] numArray1 = new int[2];
      int[] numArray2 = new int[2];
      for (int index = 0; index < 2; ++index)
      {
        List<BL.SkillEffect> skillEffectList = new List<BL.SkillEffect>();
        foreach (BL.SkillEffect effect in myself.skillEffects.Where(BattleskillEffectLogicEnum.passive_keep_away))
        {
          if (effect.useRemain.HasValue)
          {
            int? useRemain1 = effect.useRemain;
            int num = 0;
            if (!(useRemain1.GetValueOrDefault() <= num & useRemain1.HasValue))
            {
              BL.SkillEffect skillEffect = effect;
              int? useRemain2 = skillEffect.useRemain;
              skillEffect.useRemain = useRemain2.HasValue ? new int?(useRemain2.GetValueOrDefault() - 1) : new int?();
            }
            else
              continue;
          }
          if (!BattleFuncs.isSkillsAndEffectsInvalid(myself, enemy, (BL.SkillEffect) null) && !BattleFuncs.isSealedSkillEffect(myself, effect))
            skillEffectList.Add(effect);
        }
        if (skillEffectList.Count > 0 && !unitPosition2.unit.isFacility)
        {
          int penetrateCount = 0;
          foreach (BL.SkillEffect skillEffect in BattleFuncs.gearSkillEffectFilter(myself.originalUnit, (IEnumerable<BL.SkillEffect>) skillEffectList))
            penetrateCount += skillEffect.effect.GetInt(BattleskillEffectLogicArgumentEnum.range);
          if (penetrateCount >= 1)
          {
            Tuple<int, int, int> tuple = BattleFuncs.getPenetratePosition(unitPosition1.row, unitPosition1.column, unitPosition2.row, unitPosition2.column, penetrateCount, false).OrderByDescending<Tuple<int, int, int>, int>((Func<Tuple<int, int, int>, int>) (x => x.Item3)).FirstOrDefault<Tuple<int, int, int>>();
            if (tuple != null)
            {
              numArray1[index ^ 1] += tuple.Item1 - unitPosition2.row;
              numArray2[index ^ 1] += tuple.Item2 - unitPosition2.column;
            }
          }
        }
        List<BL.SkillEffect> source1 = new List<BL.SkillEffect>();
        List<BL.SkillEffect> source2 = new List<BL.SkillEffect>();
        foreach (BL.SkillEffect skillEffect1 in myself.skillEffects.Where(BattleskillEffectLogicEnum.slash_back))
        {
          if (skillEffect1.useRemain.HasValue)
          {
            int? useRemain1 = skillEffect1.useRemain;
            int num = 0;
            if (!(useRemain1.GetValueOrDefault() <= num & useRemain1.HasValue))
            {
              BL.SkillEffect skillEffect2 = skillEffect1;
              int? useRemain2 = skillEffect2.useRemain;
              skillEffect2.useRemain = useRemain2.HasValue ? new int?(useRemain2.GetValueOrDefault() - 1) : new int?();
            }
            else
              continue;
          }
          if (!BattleFuncs.isSkillsAndEffectsInvalid(myself, enemy, (BL.SkillEffect) null) && !BattleFuncs.isSealedSkillEffect(myself, skillEffect1) && BattleFuncs.checkInvokeSkillEffect(BattleFuncs.PackedSkillEffect.Create(skillEffect1), myself, enemy, new int?(), (Judgement.NonBattleParameter.FromPlayerUnitCache) null, (Judgement.NonBattleParameter.FromPlayerUnitCache) null, new int?(), new int?()))
          {
            int num = skillEffect1.effect.GetInt(BattleskillEffectLogicArgumentEnum.range);
            if (num > 0)
              source1.Add(skillEffect1);
            else if (num < 0)
              source2.Add(skillEffect1);
          }
        }
        if (source1.Any<BL.SkillEffect>())
        {
          int penetrateCount = 0;
          foreach (BL.SkillEffect skillEffect in BattleFuncs.gearSkillEffectFilter(myself.originalUnit, (IEnumerable<BL.SkillEffect>) source1))
            penetrateCount += skillEffect.effect.GetInt(BattleskillEffectLogicArgumentEnum.range);
          if (penetrateCount >= 1)
          {
            Tuple<int, int, int> tuple = BattleFuncs.getPenetratePosition(unitPosition2.row, unitPosition2.column, unitPosition1.row, unitPosition1.column, penetrateCount, false).OrderByDescending<Tuple<int, int, int>, int>((Func<Tuple<int, int, int>, int>) (x => x.Item3)).FirstOrDefault<Tuple<int, int, int>>();
            if (tuple != null)
            {
              numArray1[index] += tuple.Item1 - unitPosition1.row;
              numArray2[index] += tuple.Item2 - unitPosition1.column;
            }
          }
        }
        if (source2.Any<BL.SkillEffect>())
        {
          int penetrateCount = 0;
          foreach (BL.SkillEffect skillEffect in BattleFuncs.gearSkillEffectFilter(myself.originalUnit, (IEnumerable<BL.SkillEffect>) source2))
            penetrateCount -= skillEffect.effect.GetInt(BattleskillEffectLogicArgumentEnum.range);
          if (penetrateCount >= 1)
          {
            Tuple<int, int, int> tuple = BattleFuncs.getPenetratePosition(unitPosition1.row, unitPosition1.column, unitPosition2.row, unitPosition2.column, penetrateCount, false).OrderByDescending<Tuple<int, int, int>, int>((Func<Tuple<int, int, int>, int>) (x => x.Item3)).FirstOrDefault<Tuple<int, int, int>>();
            if (tuple != null)
            {
              numArray1[index] += tuple.Item1 - unitPosition1.row;
              numArray2[index] += tuple.Item2 - unitPosition1.column;
            }
          }
        }
        BL.ISkillEffectListUnit skillEffectListUnit = myself;
        myself = enemy;
        enemy = skillEffectListUnit;
        BL.UnitPosition unitPosition3 = unitPosition1;
        unitPosition1 = unitPosition2;
        unitPosition2 = unitPosition3;
      }
      for (int index = 0; index < 2; ++index)
      {
        if (numArray1[index] != 0 || numArray2[index] != 0)
        {
          List<Tuple<int, int, int>> penetratePosition = BattleFuncs.getPenetratePosition(unitPositionArray1[index].row, unitPositionArray1[index].column, unitPositionArray1[index].row + numArray1[index], unitPositionArray1[index].column + numArray2[index], 0, true);
          BL.Unit[] unitArray = new BL.Unit[0];
          foreach (Tuple<int, int, int> tuple in (IEnumerable<Tuple<int, int, int>>) penetratePosition.OrderByDescending<Tuple<int, int, int>, int>((Func<Tuple<int, int, int>, int>) (x => x.Item3)))
          {
            if (BattleFuncs.isResetPositionOK(unitPositionArray1[index].unit, tuple.Item1, tuple.Item2, unitPositionArray1[index].unit.parameter.Move, (IEnumerable<BL.Unit>) unitArray, isAI))
            {
              RecoveryUtility.resetPosition(unitPositionArray1[index], tuple.Item1, tuple.Item2, env, true, index == 0);
              break;
            }
          }
        }
      }
      BL.UnitPosition myselfUp = BattleFuncs.iSkillEffectListUnitToUnitPosition(myself);
      BL.UnitPosition targetUp = BattleFuncs.iSkillEffectListUnitToUnitPosition(enemy);
      for (int index = 0; index < 2; ++index)
      {
        List<BL.SkillEffect> source = new List<BL.SkillEffect>();
        foreach (BL.SkillEffect skillEffect1 in myself.skillEffects.Where(BattleskillEffectLogicEnum.passive_steal))
        {
          if (skillEffect1.useRemain.HasValue)
          {
            int? useRemain1 = skillEffect1.useRemain;
            int num = 0;
            if (!(useRemain1.GetValueOrDefault() <= num & useRemain1.HasValue))
            {
              BL.SkillEffect skillEffect2 = skillEffect1;
              int? useRemain2 = skillEffect2.useRemain;
              skillEffect2.useRemain = useRemain2.HasValue ? new int?(useRemain2.GetValueOrDefault() - 1) : new int?();
            }
            else
              continue;
          }
          if (!BattleFuncs.isSkillsAndEffectsInvalid(myself, enemy, (BL.SkillEffect) null) && !BattleFuncs.isSealedSkillEffect(myself, skillEffect1) && BattleFuncs.checkInvokeSkillEffect(BattleFuncs.PackedSkillEffect.Create(skillEffect1), myself, enemy, new int?(), (Judgement.NonBattleParameter.FromPlayerUnitCache) null, (Judgement.NonBattleParameter.FromPlayerUnitCache) null, new int?(), new int?()))
            source.Add(skillEffect1);
        }
        if (source.Any<BL.SkillEffect>() && !targetUp.unit.isFacility)
        {
          foreach (BL.SkillEffect skillEffect in BattleFuncs.gearSkillEffectFilter(myself.originalUnit, (IEnumerable<BL.SkillEffect>) source))
          {
            bool flag = BattleFuncs.executeSteal(myself, enemy, skillEffect.effect, myselfUp, targetUp, isAI);
            if (stealSkillTarget != null & flag)
            {
              Tuple<BL.Unit, BattleskillSkill> key = Tuple.Create<BL.Unit, BattleskillSkill>(myself as BL.Unit, skillEffect.baseSkill);
              if (!stealSkillTarget.ContainsKey(key))
                stealSkillTarget[key] = new List<BL.Unit>();
              if (!stealSkillTarget[key].Contains(enemy as BL.Unit))
                stealSkillTarget[key].Add(enemy as BL.Unit);
            }
          }
        }
        BL.ISkillEffectListUnit skillEffectListUnit = myself;
        myself = enemy;
        enemy = skillEffectListUnit;
        BL.UnitPosition unitPosition3 = myselfUp;
        myselfUp = targetUp;
        targetUp = unitPosition3;
      }
      foreach (BL.DuelTurn turn in duelResult.turns)
      {
        int count = turn.damageShareUnit.Count;
        for (int index = 0; index < count; ++index)
        {
          BL.ISkillEffectListUnit originalUnit = turn.damageShareUnit[index];
          int damage = turn.damageShareDamage[index];
          BL.UseSkillEffect effect = turn.damageShareSkillEffect[index];
          if (!isAI)
            originalUnit = (BL.ISkillEffectListUnit) originalUnit.originalUnit;
          BL.SkillEffect[] array = originalUnit.skillEffects.All().Where<BL.SkillEffect>((Func<BL.SkillEffect, bool>) (x =>
          {
            if (x.effectId == effect.effectEffectId && x.baseSkillLevel == effect.effectBaseSkillLevel)
            {
              if (x.turnRemain.HasValue || effect.effectTurnRemain != -1)
              {
                int? turnRemain = x.turnRemain;
                int effectTurnRemain = effect.effectTurnRemain;
                if (!(turnRemain.GetValueOrDefault() == effectTurnRemain & turnRemain.HasValue))
                  goto label_8;
              }
              if (x.unit == (BL.Unit) null && effect.effectUnitIndex == -1)
                return true;
              return x.unit != (BL.Unit) null && effect.effectUnitIndex != -1 && x.unit.index == effect.effectUnitIndex && x.unit.isPlayerForce == effect.effectUnitIsPlayerControl;
            }
label_8:
            return false;
          })).ToArray<BL.SkillEffect>();
          BattleFuncs.applyHpDamage(atk, def, turn.isAtackker ? atk : def, originalUnit, damage, 0, deads, damageShareHpFunction);
          BL.SkillEffect skillEffect1 = ((IEnumerable<BL.SkillEffect>) array).FirstOrDefault<BL.SkillEffect>((Func<BL.SkillEffect, bool>) (x =>
          {
            int? useRemain = x.useRemain;
            int num = 1;
            return useRemain.GetValueOrDefault() >= num & useRemain.HasValue;
          }));
          if (skillEffect1 != null)
          {
            BL.SkillEffect skillEffect2 = skillEffect1;
            int? useRemain = skillEffect2.useRemain;
            skillEffect2.useRemain = useRemain.HasValue ? new int?(useRemain.GetValueOrDefault() - 1) : new int?();
          }
          BL.SkillEffect skillEffect3 = ((IEnumerable<BL.SkillEffect>) array).FirstOrDefault<BL.SkillEffect>();
          if (skillEffect3 != null && damageShareSkillTarget != null)
          {
            BL.Unit key = originalUnit as BL.Unit;
            if (!damageShareSkillTarget.ContainsKey(key))
              damageShareSkillTarget[key] = new HashSet<BattleskillSkill>();
            damageShareSkillTarget[key].Add(skillEffect3.baseSkill);
          }
        }
      }
      AttackStatus attackStatus1 = !isInvokedAmbush ? duelResult.attackAttackStatus : duelResult.defenseAttackStatus;
      AttackStatus attackStatus2 = !isInvokedAmbush ? duelResult.defenseAttackStatus : duelResult.attackAttackStatus;
      for (is_attack = 1; is_attack <= 2; is_attack++)
      {
        List<BL.SkillEffect> skillEffectList = new List<BL.SkillEffect>();
        foreach (BL.SkillEffect effect in myself.skillEffects.Where(BattleskillEffectLogicEnum.penetrate))
        {
          if (effect.useRemain.HasValue)
          {
            int? useRemain1 = effect.useRemain;
            int num = 0;
            if (!(useRemain1.GetValueOrDefault() <= num & useRemain1.HasValue))
            {
              BL.SkillEffect skillEffect = effect;
              int? useRemain2 = skillEffect.useRemain;
              skillEffect.useRemain = useRemain2.HasValue ? new int?(useRemain2.GetValueOrDefault() - 1) : new int?();
            }
            else
              continue;
          }
          if (!BattleFuncs.isSkillsAndEffectsInvalid(myself, enemy, (BL.SkillEffect) null) && !BattleFuncs.isSealedSkillEffect(myself, effect))
          {
            int num = effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.attack_type);
            if ((num != 1 || attackStatus1 != null && !attackStatus1.isMagic) && (num != 2 || attackStatus1 != null && attackStatus1.isMagic))
              skillEffectList.Add(effect);
          }
        }
        foreach (BL.SkillEffect skillEffect in BattleFuncs.gearSkillEffectFilter(myself.originalUnit, (IEnumerable<BL.SkillEffect>) skillEffectList))
        {
          BL.SkillEffect effect = skillEffect;
          if (skillEffectListUnitArray[is_attack - 1] == null)
          {
            skillEffectListUnitArray[is_attack - 1] = myself;
            dictionaryArray[is_attack - 1] = new Dictionary<BL.ISkillEffectListUnit, int>();
          }
          int num1 = effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.min_hp);
          if (num1 < minHp1)
            minHp1 = num1;
          BL.UnitPosition unitPosition3 = BattleFuncs.iSkillEffectListUnitToUnitPosition(enemy);
          int num2 = effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.min_range);
          int penetrateCount = effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.max_range);
          List<BL.UnitPosition> unitPositionList = new List<BL.UnitPosition>();
          unitPositionList.Add(BattleFuncs.iSkillEffectListUnitToUnitPosition(myself));
          if (effect.effect.HasKey(BattleskillEffectLogicArgumentEnum.enable_combi) && effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.enable_combi) == 1)
          {
            bool flag = is_attack == 1;
            if (isInvokedAmbush)
              flag = !flag;
            for (int index = duelResult.turns.Length - 1; index >= 0; --index)
            {
              BL.DuelTurn turn = duelResult.turns[index];
              if (turn.isAtackker == flag && turn.invokeGiveSkills != null && ((IEnumerable<BL.Skill>) turn.invokeGiveSkills).Any<BL.Skill>((Func<BL.Skill, bool>) (skill => ((IEnumerable<BattleskillEffect>) skill.skill.Effects).Any<BattleskillEffect>((Func<BattleskillEffect, bool>) (e => e.skill.ID == effect.baseSkillId)))))
              {
                if (turn.attackerCombiUnit != null)
                {
                  foreach (BL.ISkillEffectListUnit skillEffectListUnit in turn.attackerCombiUnit)
                  {
                    if (skillEffectListUnit != null)
                    {
                      BL.ISkillEffectListUnit unit = skillEffectListUnit;
                      if (!isAI)
                        unit = (BL.ISkillEffectListUnit) unit.originalUnit;
                      unitPositionList.Add(BattleFuncs.iSkillEffectListUnitToUnitPosition(unit));
                    }
                  }
                  break;
                }
                break;
              }
            }
          }
          foreach (BL.UnitPosition up1 in unitPositionList)
          {
            List<Tuple<int, int, int>> penetratePosition = BattleFuncs.getPenetratePosition(up1.row, up1.column, unitPosition3.row, unitPosition3.column, penetrateCount, false);
            BL.ISkillEffectListUnit iskillEffectListUnit1 = BattleFuncs.unitPositionToISkillEffectListUnit(up1);
            foreach (Tuple<int, int, int> tuple in penetratePosition)
            {
              if (tuple.Item3 >= num2)
              {
                BL.UnitPosition[] unitPositionArray2 = isAI ? BattleFuncs.getFieldUnitsAI(tuple.Item1, tuple.Item2) : env.getFieldUnits(tuple.Item1, tuple.Item2, false, false);
                if (unitPositionArray2 != null)
                {
                  foreach (BL.UnitPosition up2 in unitPositionArray2)
                  {
                    if (((IEnumerable<BL.ForceID>) forceIds2).Contains<BL.ForceID>(env.getForceID(up2.unit)) && !up2.unit.isFacility)
                    {
                      BL.ISkillEffectListUnit iskillEffectListUnit2 = BattleFuncs.unitPositionToISkillEffectListUnit(up2);
                      int damage = BattleFuncs.calcAttackDamage(iskillEffectListUnit1, iskillEffectListUnit2, effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.percentage_attack), effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.percentage_decrease), effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.percentage_damage));
                      if (damage != 0)
                      {
                        int num3 = BattleFuncs.applyFieldDamageFluctuate(atk, def, myself, iskillEffectListUnit2, true, damage);
                        int index = is_attack - 1;
                        if (!dictionaryArray[index].ContainsKey(iskillEffectListUnit2))
                          dictionaryArray[index][iskillEffectListUnit2] = 0;
                        dictionaryArray[index][iskillEffectListUnit2] += num3;
                        if (penetrateSkillTarget != null)
                        {
                          Tuple<BL.Unit, BattleskillSkill> key = new Tuple<BL.Unit, BattleskillSkill>(iskillEffectListUnit1 as BL.Unit, effect.baseSkill);
                          if (!penetrateSkillTarget.ContainsKey(key))
                          {
                            float num4 = Mathf.Atan2((float) (unitPosition3.column - up1.column), (float) (unitPosition3.row - up1.row)) * 57.29578f;
                            penetrateSkillTarget[key] = new Tuple<List<BL.Unit>, float>(new List<BL.Unit>(), num4);
                          }
                          if (!penetrateSkillTarget[key].Item1.Contains(iskillEffectListUnit2 as BL.Unit))
                            penetrateSkillTarget[key].Item1.Add(iskillEffectListUnit2 as BL.Unit);
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
        BL.ISkillEffectListUnit skillEffectListUnit1 = myself;
        myself = enemy;
        enemy = skillEffectListUnit1;
        BL.ForceID[] forceIdArray = forceIds1;
        forceIds1 = forceIds2;
        forceIds2 = forceIdArray;
        AttackStatus attackStatus3 = attackStatus1;
        attackStatus1 = attackStatus2;
        attackStatus2 = attackStatus3;
      }
      Dictionary<BattleskillEffectLogicEnum, Tuple<Func<BL.SkillEffect, bool>, Func<BL.SkillEffect, bool>, System.Action<BL.SkillEffect, BL.ISkillEffectListUnit>, System.Action<BL.SkillEffect>>> dictionary1 = new Dictionary<BattleskillEffectLogicEnum, Tuple<Func<BL.SkillEffect, bool>, Func<BL.SkillEffect, bool>, System.Action<BL.SkillEffect, BL.ISkillEffectListUnit>, System.Action<BL.SkillEffect>>>()
      {
        {
          BattleskillEffectLogicEnum.life_absorb,
          new Tuple<Func<BL.SkillEffect, bool>, Func<BL.SkillEffect, bool>, System.Action<BL.SkillEffect, BL.ISkillEffectListUnit>, System.Action<BL.SkillEffect>>((Func<BL.SkillEffect, bool>) (effect =>
          {
            if (effect.effect.HasKey(BattleskillEffectLogicArgumentEnum.element))
            {
              int num = effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.element);
              if (num != 0 && (CommonElement) num != myself.originalUnit.playerUnit.GetElement())
                return false;
            }
            if (effect.effect.HasKey(BattleskillEffectLogicArgumentEnum.target_element))
            {
              int num = effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.target_element);
              if (num != 0 && (CommonElement) num != enemy.originalUnit.playerUnit.GetElement())
                return false;
            }
            if (effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.condition) == 1)
              return enemy.hp <= 0;
            return effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.condition) != 2 && enemy.hp <= 0 || myself.hp <= 0;
          }), (Func<BL.SkillEffect, bool>) null, (System.Action<BL.SkillEffect, BL.ISkillEffectListUnit>) ((effect, unit) =>
          {
            if (!lifeAbsorbTargetHeal.ContainsKey(unit))
              lifeAbsorbTargetHeal[unit] = 0;
            if (unit.CanHeal(effect.baseSkill.skill_type))
            {
              lifeAbsorbTargetHeal[unit] += effect.baseSkillLevel;
              lifeAbsorbTargetHeal[unit] += effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.value);
              lifeAbsorbTargetHeal[unit] += Mathf.CeilToInt((float) unit.originalUnit.parameter.Hp * effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.percentage));
            }
            if (lifeAbsorbSkillTarget == null)
              return;
            Tuple<BL.Unit, BattleskillSkill> key = new Tuple<BL.Unit, BattleskillSkill>(myself as BL.Unit, effect.baseSkill);
            if (!lifeAbsorbSkillTarget.ContainsKey(key))
              lifeAbsorbSkillTarget[key] = new List<BL.Unit>();
            if (lifeAbsorbSkillTarget[key].Contains(unit as BL.Unit))
              return;
            lifeAbsorbSkillTarget[key].Add(unit as BL.Unit);
          }), (System.Action<BL.SkillEffect>) null)
        },
        {
          BattleskillEffectLogicEnum.range_attack,
          new Tuple<Func<BL.SkillEffect, bool>, Func<BL.SkillEffect, bool>, System.Action<BL.SkillEffect, BL.ISkillEffectListUnit>, System.Action<BL.SkillEffect>>((Func<BL.SkillEffect, bool>) (effect =>
          {
            if (effect.useRemain.HasValue)
            {
              int? useRemain = effect.useRemain;
              int num = 0;
              if (useRemain.GetValueOrDefault() <= num & useRemain.HasValue)
                return false;
            }
            return true;
          }), (Func<BL.SkillEffect, bool>) (effect =>
          {
            if (rangeAttackInvokeUnit[is_attack - 1] == null)
            {
              rangeAttackInvokeUnit[is_attack - 1] = myself;
              rangeAttackTargetDamage[is_attack - 1] = new Dictionary<BL.ISkillEffectListUnit, int>();
            }
            int num = effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.min_hp);
            if (num < rangeAttackMinHp)
              rangeAttackMinHp = num;
            return true;
          }), (System.Action<BL.SkillEffect, BL.ISkillEffectListUnit>) ((effect, unit) =>
          {
            int damage = BattleFuncs.calcAttackDamage(myself, unit, effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.percentage_attack), effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.percentage_decrease), effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.percentage_damage));
            if (damage == 0)
              return;
            int num = BattleFuncs.applyFieldDamageFluctuate(atk, def, myself, unit, effect.effect, damage);
            int index = is_attack - 1;
            if (!rangeAttackTargetDamage[index].ContainsKey(unit))
              rangeAttackTargetDamage[index][unit] = 0;
            rangeAttackTargetDamage[index][unit] += num;
            if (rangeAttackSkillTarget == null)
              return;
            Tuple<BL.Unit, BattleskillSkill> key = new Tuple<BL.Unit, BattleskillSkill>(myself as BL.Unit, effect.baseSkill);
            if (!rangeAttackSkillTarget.ContainsKey(key))
              rangeAttackSkillTarget[key] = new List<BL.Unit>();
            if (rangeAttackSkillTarget[key].Contains(unit as BL.Unit))
              return;
            rangeAttackSkillTarget[key].Add(unit as BL.Unit);
          }), (System.Action<BL.SkillEffect>) (effect =>
          {
            if (!effect.useRemain.HasValue)
              return;
            BL.SkillEffect skillEffect = effect;
            int? useRemain = skillEffect.useRemain;
            skillEffect.useRemain = useRemain.HasValue ? new int?(useRemain.GetValueOrDefault() - 1) : new int?();
          }))
        },
        {
          BattleskillEffectLogicEnum.curse_reflection,
          new Tuple<Func<BL.SkillEffect, bool>, Func<BL.SkillEffect, bool>, System.Action<BL.SkillEffect, BL.ISkillEffectListUnit>, System.Action<BL.SkillEffect>>((Func<BL.SkillEffect, bool>) (effect =>
          {
            int num1 = effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.target_gear_kind_id);
            if (num1 != 0 && num1 != enemy.originalUnit.unit.kind.ID)
              return false;
            if (effect.effect.HasKey(BattleskillEffectLogicArgumentEnum.target_element))
            {
              int num2 = effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.target_element);
              if (num2 != 0 && (CommonElement) num2 != enemy.originalUnit.playerUnit.GetElement())
                return false;
            }
            int num3 = effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.condition);
            bool flag = true;
            switch (num3)
            {
              case 1:
                flag = myself.hp >= 1;
                break;
              case 2:
                flag = myself.hp <= 0;
                break;
            }
            return flag;
          }), (Func<BL.SkillEffect, bool>) (effect =>
          {
            if (curseReflectionInvokeUnit[is_attack - 1] == null)
            {
              curseReflectionInvokeUnit[is_attack - 1] = myself;
              curseReflectionTargetDamage[is_attack - 1] = new Dictionary<BL.ISkillEffectListUnit, int>();
              bool isa = is_attack == 1;
              if (isInvokedAmbush)
                isa = !isa;
              iWork[0] = ((IEnumerable<BL.DuelTurn>) duelResult.turns).Sum<BL.DuelTurn>((Func<BL.DuelTurn, int>) (x => x.isAtackker == isa ? 0 : x.realDamage));
            }
            return true;
          }), (System.Action<BL.SkillEffect, BL.ISkillEffectListUnit>) ((effect, unit) =>
          {
            int damage = effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.value) + Mathf.CeilToInt((float) iWork[0] * effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.percentage));
            if (damage == 0)
              return;
            int num = BattleFuncs.applyFieldDamageFluctuate(atk, def, myself, unit, effect.effect, damage);
            if (num < 0 && !unit.CanHeal(effect.baseSkill.skill_type))
              num = 0;
            int index = is_attack - 1;
            if (!curseReflectionTargetDamage[index].ContainsKey(unit))
              curseReflectionTargetDamage[index][unit] = 0;
            curseReflectionTargetDamage[index][unit] += num;
            if (curseReflectionSkillTarget == null)
              return;
            Tuple<BL.Unit, BattleskillSkill> key = new Tuple<BL.Unit, BattleskillSkill>(myself as BL.Unit, effect.baseSkill);
            if (!curseReflectionSkillTarget.ContainsKey(key))
              curseReflectionSkillTarget[key] = new List<BL.Unit>();
            if (curseReflectionSkillTarget[key].Contains(unit as BL.Unit))
              return;
            curseReflectionSkillTarget[key].Add(unit as BL.Unit);
          }), (System.Action<BL.SkillEffect>) null)
        },
        {
          BattleskillEffectLogicEnum.snake_venom_damage,
          new Tuple<Func<BL.SkillEffect, bool>, Func<BL.SkillEffect, bool>, System.Action<BL.SkillEffect, BL.ISkillEffectListUnit>, System.Action<BL.SkillEffect>>((Func<BL.SkillEffect, bool>) (effect =>
          {
            if (effect.effect.HasKey(BattleskillEffectLogicArgumentEnum.target_element))
            {
              int num = effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.target_element);
              if (num != 0 && (CommonElement) num != enemy.originalUnit.playerUnit.GetElement())
                return false;
            }
            return true;
          }), (Func<BL.SkillEffect, bool>) (effect =>
          {
            bool flag = is_attack == 1;
            if (isInvokedAmbush)
              flag = !flag;
            List<BattleFuncs.SnakeVenomDamageData> snakeVenomDamageDataList = (List<BattleFuncs.SnakeVenomDamageData>) null;
            for (int index = duelResult.turns.Length - 1; index >= 0; --index)
            {
              BL.DuelTurn turn = duelResult.turns[index];
              if (turn.isAtackker == flag && turn.invokeGiveSkills != null && ((IEnumerable<BL.Skill>) turn.invokeGiveSkills).Any<BL.Skill>((Func<BL.Skill, bool>) (skill => ((IEnumerable<BattleskillEffect>) skill.skill.Effects).Any<BattleskillEffect>((Func<BattleskillEffect, bool>) (e => e.skill.ID == effect.baseSkillId)))))
              {
                if (snakeVenomDamage.Any<BattleFuncs.SnakeVenomDamageData>((Func<BattleFuncs.SnakeVenomDamageData, bool>) (x => x.effect.effectId == effect.effectId)))
                  return false;
                if (snakeVenomDamageDataList == null)
                  snakeVenomDamageDataList = new List<BattleFuncs.SnakeVenomDamageData>();
                snakeVenomDamageDataList.Add(new BattleFuncs.SnakeVenomDamageData()
                {
                  invokeUnit = myself,
                  effect = effect,
                  invokeOrder = index,
                  turnDamage = turn.realDamage
                });
              }
            }
            if (snakeVenomDamageDataList != null)
              snakeVenomDamage.AddRange((IEnumerable<BattleFuncs.SnakeVenomDamageData>) snakeVenomDamageDataList);
            else
              snakeVenomDamage.Add(new BattleFuncs.SnakeVenomDamageData()
              {
                invokeUnit = myself,
                effect = effect,
                invokeOrder = 10000 + is_attack,
                turnDamage = 0
              });
            return true;
          }), (System.Action<BL.SkillEffect, BL.ISkillEffectListUnit>) ((effect, unit) =>
          {
            if (unit == enemy && effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.effect_target) != 3 && (effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.min_range) != 0 || effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.max_range) != 0))
              return;
            float num1 = effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.venom_hp_percentage);
            float num2 = effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.venom_damage_percentage);
            int num3 = (double) num1 >= 0.0 ? Mathf.CeilToInt((float) unit.originalUnit.parameter.Hp * num1) : Mathf.FloorToInt((float) unit.originalUnit.parameter.Hp * num1);
            foreach (BattleFuncs.SnakeVenomDamageData snakeVenomDamageData in snakeVenomDamage.Where<BattleFuncs.SnakeVenomDamageData>((Func<BattleFuncs.SnakeVenomDamageData, bool>) (x => x.effect == effect)))
            {
              int num4 = (double) num2 >= 0.0 ? Mathf.CeilToInt((float) snakeVenomDamageData.turnDamage * num2) : Mathf.FloorToInt((float) snakeVenomDamageData.turnDamage * num2);
              int damage = effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.venom_value) + num3 + num4;
              int num5 = BattleFuncs.applyFieldDamageFluctuate(atk, def, myself, unit, effect.effect, damage);
              if (num5 < 0 && !unit.CanHeal(effect.baseSkill.skill_type))
                num5 = 0;
              if (num5 < 0 && unit == myself && myself.hp <= 0)
              {
                BattleFuncs.PackedSkillEffect packedSkillEffect = BattleFuncs.PackedSkillEffect.Create(effect);
                if (!packedSkillEffect.HasKey(BattleskillEffectLogicArgumentEnum.condition) || packedSkillEffect.GetInt(BattleskillEffectLogicArgumentEnum.condition) != 1)
                  continue;
              }
              if (snakeVenomDamageData.unitDamage == null)
                snakeVenomDamageData.unitDamage = new Dictionary<BL.ISkillEffectListUnit, int>();
              snakeVenomDamageData.unitDamage[unit] = num5;
            }
          }), (System.Action<BL.SkillEffect>) null)
        }
      };
      foreach (BattleskillEffectLogicEnum key in dictionary1.Keys)
      {
        for (is_attack = 1; is_attack <= 2; is_attack++)
        {
          List<BL.SkillEffect> skillEffectList = new List<BL.SkillEffect>();
          foreach (BL.SkillEffect effect in myself.skillEffects.Where(key))
          {
            if ((effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.is_attack_nc) == 0 || effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.is_attack_nc) == is_attack) && (!BattleFuncs.isSkillsAndEffectsInvalid(myself, enemy, (BL.SkillEffect) null) && !BattleFuncs.isSealedSkillEffect(myself, effect)) && dictionary1[key].Item1(effect))
              skillEffectList.Add(effect);
            if (dictionary1[key].Item4 != null)
              dictionary1[key].Item4(effect);
          }
          foreach (BL.SkillEffect headerEffect in BattleFuncs.gearSkillEffectFilter(myself.originalUnit, (IEnumerable<BL.SkillEffect>) skillEffectList))
          {
            if (dictionary1[key].Item2 == null || dictionary1[key].Item2(headerEffect))
            {
              int[] range = new int[2]
              {
                headerEffect.effect.GetInt(BattleskillEffectLogicArgumentEnum.min_range),
                headerEffect.effect.GetInt(BattleskillEffectLogicArgumentEnum.max_range)
              };
              BL.Unit unit = headerEffect.effect.GetInt(BattleskillEffectLogicArgumentEnum.is_range_from_enemy) == 0 ? myself.originalUnit : enemy.originalUnit;
              int num1 = headerEffect.effect.GetInt(BattleskillEffectLogicArgumentEnum.effect_target);
              List<BL.UnitPosition> source;
              switch (num1)
              {
                case 0:
                case 2:
                  source = BattleFuncs.getTargets(unit, range, forceIds1, BL.Unit.TargetAttribute.all, isAI, false, false, true);
                  if (num1 == 2)
                  {
                    source = source.Where<BL.UnitPosition>((Func<BL.UnitPosition, bool>) (x => BattleFuncs.unitPositionToISkillEffectListUnit(x) == myself)).ToList<BL.UnitPosition>();
                    break;
                  }
                  break;
                default:
                  source = BattleFuncs.getTargets(unit, range, forceIds2, BL.Unit.TargetAttribute.all, isAI, false, false, true);
                  if (num1 == 3)
                  {
                    source = source.Where<BL.UnitPosition>((Func<BL.UnitPosition, bool>) (x => BattleFuncs.unitPositionToISkillEffectListUnit(x) == enemy)).ToList<BL.UnitPosition>();
                    break;
                  }
                  break;
              }
              BattleFuncs.PackedSkillEffect packedSkillEffect = BattleFuncs.PackedSkillEffect.Create(headerEffect);
              int num2 = !packedSkillEffect.HasKey(BattleskillEffectLogicArgumentEnum.range_form) ? 0 : packedSkillEffect.GetInt(BattleskillEffectLogicArgumentEnum.range_form);
              foreach (BL.UnitPosition unitPosition3 in source)
              {
                switch (num2)
                {
                  case 1:
                    Tuple<int, int> unitCell1 = BattleFuncs.getUnitCell(unit, isAI, false);
                    if (unitPosition3.row == unitCell1.Item1 || unitPosition3.column == unitCell1.Item2)
                      break;
                    continue;
                  case 2:
                    Tuple<int, int> unitCell2 = BattleFuncs.getUnitCell(unit, isAI, false);
                    if (Mathf.Abs(unitPosition3.row - unitCell2.Item1) != Mathf.Abs(unitPosition3.column - unitCell2.Item2))
                      continue;
                    break;
                }
                BL.ISkillEffectListUnit skillEffectListUnit = unitPosition3 is BL.ISkillEffectListUnit ? unitPosition3 as BL.ISkillEffectListUnit : (BL.ISkillEffectListUnit) unitPosition3.unit;
                if (skillEffectListUnit != null)
                  dictionary1[key].Item3(headerEffect, skillEffectListUnit);
              }
            }
          }
          BL.ISkillEffectListUnit skillEffectListUnit1 = myself;
          myself = enemy;
          enemy = skillEffectListUnit1;
          BL.ForceID[] forceIdArray = forceIds1;
          forceIds1 = forceIds2;
          forceIds2 = forceIdArray;
        }
      }
      if (lifeAbsorbTargetHeal.Count >= 1)
      {
        foreach (BL.ISkillEffectListUnit key1 in lifeAbsorbTargetHeal.Keys)
        {
          int hp = key1.hp;
          if (hp >= 1)
          {
            key1.hp += lifeAbsorbTargetHeal[key1];
            if (addHpFunction != null)
              addHpFunction(key1, hp);
          }
          else if (lifeAbsorbSkillTarget != null)
          {
            foreach (Tuple<BL.Unit, BattleskillSkill> key2 in lifeAbsorbSkillTarget.Keys)
              lifeAbsorbSkillTarget[key2].Remove(key1 as BL.Unit);
          }
        }
      }
      if (skillEffectListUnitArray[0] != null || skillEffectListUnitArray[1] != null)
      {
        if (penetrateSkillTarget != null)
        {
          foreach (Tuple<BL.Unit, BattleskillSkill> key in penetrateSkillTarget.Keys)
          {
            List<BL.Unit> unitList = new List<BL.Unit>();
            foreach (BL.Unit unit in penetrateSkillTarget[key].Item1)
            {
              if (unit.hp <= 0)
                unitList.Add(unit);
            }
            foreach (BL.Unit unit in unitList)
              penetrateSkillTarget[key].Item1.Remove(unit);
          }
        }
        for (int index = 0; index < 2; ++index)
        {
          if (dictionaryArray[index] != null && dictionaryArray[index].Count >= 1)
          {
            BL.ISkillEffectListUnit invokeUnit = skillEffectListUnitArray[index];
            foreach (BL.ISkillEffectListUnit key in dictionaryArray[index].Keys)
            {
              if (key.hp >= 1)
                BattleFuncs.applyHpDamage(atk, def, invokeUnit, key, dictionaryArray[index][key], minHp1, deads, penetrateHpFunction);
            }
          }
        }
      }
      if (rangeAttackInvokeUnit[0] != null || rangeAttackInvokeUnit[1] != null)
      {
        if (rangeAttackSkillTarget != null)
        {
          foreach (Tuple<BL.Unit, BattleskillSkill> key in rangeAttackSkillTarget.Keys)
          {
            List<BL.Unit> unitList = new List<BL.Unit>();
            foreach (BL.Unit unit in rangeAttackSkillTarget[key])
            {
              if (unit.hp <= 0)
                unitList.Add(unit);
            }
            foreach (BL.Unit unit in unitList)
              rangeAttackSkillTarget[key].Remove(unit);
          }
        }
        for (int index = 0; index < 2; ++index)
        {
          if (rangeAttackTargetDamage[index] != null && rangeAttackTargetDamage[index].Count >= 1)
          {
            BL.ISkillEffectListUnit invokeUnit = rangeAttackInvokeUnit[index];
            foreach (BL.ISkillEffectListUnit key in rangeAttackTargetDamage[index].Keys)
            {
              if (key.hp >= 1)
                BattleFuncs.applyHpDamage(atk, def, invokeUnit, key, rangeAttackTargetDamage[index][key], rangeAttackMinHp, deads, rangeAttackHpFunction);
            }
          }
        }
      }
      if (curseReflectionInvokeUnit[0] != null || curseReflectionInvokeUnit[1] != null)
      {
        if (curseReflectionSkillTarget != null)
        {
          foreach (Tuple<BL.Unit, BattleskillSkill> key in curseReflectionSkillTarget.Keys)
          {
            List<BL.Unit> unitList = new List<BL.Unit>();
            foreach (BL.Unit unit in curseReflectionSkillTarget[key])
            {
              if (unit.hp <= 0)
                unitList.Add(unit);
            }
            foreach (BL.Unit unit in unitList)
              curseReflectionSkillTarget[key].Remove(unit);
          }
        }
        for (int index = 0; index < 2; ++index)
        {
          if (curseReflectionTargetDamage[index] != null && curseReflectionTargetDamage[index].Count >= 1)
          {
            BL.ISkillEffectListUnit invokeUnit = curseReflectionInvokeUnit[index];
            foreach (BL.ISkillEffectListUnit key in curseReflectionTargetDamage[index].Keys)
            {
              if (key.hp >= 1)
                BattleFuncs.applyHpDamage(atk, def, invokeUnit, key, curseReflectionTargetDamage[index][key], 0, deads, subHpFunction);
            }
          }
        }
      }
      foreach (BattleFuncs.SnakeVenomDamageData snakeVenomDamageData in (IEnumerable<BattleFuncs.SnakeVenomDamageData>) snakeVenomDamage.OrderBy<BattleFuncs.SnakeVenomDamageData, int>((Func<BattleFuncs.SnakeVenomDamageData, int>) (x => x.invokeOrder)).ThenByDescending<BattleFuncs.SnakeVenomDamageData, int>((Func<BattleFuncs.SnakeVenomDamageData, int>) (x => x.effect.baseSkill.weight)).ThenBy<BattleFuncs.SnakeVenomDamageData, int>((Func<BattleFuncs.SnakeVenomDamageData, int>) (x => x.effect.baseSkillId)))
      {
        snakeVenomDamageData.invokeUnit.skillEffects.RemoveEffect(1000413, env, snakeVenomDamageData.invokeUnit);
        if (snakeVenomDamageData.unitDamage != null)
        {
          Dictionary<BL.Unit, Tuple<int, int>> dictionary2 = snakeFunction != null ? new Dictionary<BL.Unit, Tuple<int, int>>() : (Dictionary<BL.Unit, Tuple<int, int>>) null;
          foreach (BL.ISkillEffectListUnit key in snakeVenomDamageData.unitDamage.Keys)
          {
            int minHp2 = snakeVenomDamageData.effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.min_hp);
            if (key.hp >= minHp2)
            {
              int hp = key.hp;
              int damage = snakeVenomDamageData.unitDamage[key];
              if (damage >= 0)
                BattleFuncs.applyHpDamage(atk, def, snakeVenomDamageData.invokeUnit, key, damage, minHp2, deads, (System.Action<BL.ISkillEffectListUnit, int>) null);
              else
                key.hp -= damage;
              if (dictionary2 != null && (damage < 0 || hp >= 1))
                dictionary2[key as BL.Unit] = new Tuple<int, int>(hp, key.hp);
            }
          }
          if (dictionary2 != null)
            snakeFunction(snakeVenomDamageData.effect.baseSkill, snakeVenomDamageData.invokeUnit as BL.Unit, dictionary2);
        }
      }
    }

    private static void applyHpDamage(
      BL.ISkillEffectListUnit atk,
      BL.ISkillEffectListUnit def,
      BL.ISkillEffectListUnit invokeUnit,
      BL.ISkillEffectListUnit target,
      int damage,
      int minHp,
      HashSet<BL.ISkillEffectListUnit> deads,
      System.Action<BL.ISkillEffectListUnit, int> hpFunction)
    {
      BL.Unit unit1 = invokeUnit as BL.Unit;
      int hp = target.hp;
      target.hp -= damage;
      if (target.hp < minHp)
        target.hp = minHp;
      if (damage >= 1 && target != atk && target != def)
        target.skillEffects.RemoveEffect(1000418, BattleFuncs.env, target);
      if (hpFunction != null)
        hpFunction(target, hp);
      if (target.hp <= 0 && target != atk && target != def)
      {
        if (unit1 != (BL.Unit) null)
        {
          BL.Unit unit2 = target as BL.Unit;
          ++unit1.killCount;
          unit2.killedBy = unit1;
          if (BattleFuncs.env.getForceID(unit2) == BL.ForceID.player)
            BattleFuncs.env.updateIntimateByDefense(unit2);
        }
        invokeUnit.skillEffects.AddKillCount(1);
        deads.Add(target);
      }
      if (!(unit1 != (BL.Unit) null) || hp <= target.hp || (unit1.originalUnit.isFacility || target.originalUnit.isFacility))
        return;
      unit1.attackDamage += hp - target.hp;
    }

    public static bool applyServantsJoy(BL.ISkillEffectListUnit unit, int healHpTotal)
    {
      List<BattleFuncs.SkillParam> skillParams = new List<BattleFuncs.SkillParam>();
      bool flag = false;
      foreach (BL.SkillEffect effect in unit.enabledSkillEffect(BattleskillEffectLogicEnum.ratio_servants_joy))
      {
        float num1 = effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.percentage) + effect.effect.GetFloat(BattleskillEffectLogicArgumentEnum.skill_ratio) * (float) effect.baseSkillLevel;
        int num2 = (int) ((Decimal) healHpTotal * (Decimal) num1);
        if ((double) num1 > 0.0)
        {
          if (num2 <= 0)
            num2 = 1;
        }
        else if ((double) num1 < 0.0 && num2 >= 0)
          num2 = -1;
        if (unit.CanHeal(effect.baseSkill.skill_type))
          skillParams.Add(BattleFuncs.SkillParam.CreateAdd(unit.originalUnit, effect, (float) num2, (object) null, 0));
        flag = true;
      }
      foreach (BL.SkillEffect effect in unit.enabledSkillEffect(BattleskillEffectLogicEnum.fix_servants_joy))
      {
        int num = effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.value) + effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.skill_ratio) * effect.baseSkillLevel;
        if (unit.CanHeal(effect.baseSkill.skill_type))
          skillParams.Add(BattleFuncs.SkillParam.CreateAdd(unit.originalUnit, effect, (float) num, (object) null, 0));
        flag = true;
      }
      if (flag)
        unit.hp += BattleFuncs.calcSkillParamAdd(skillParams);
      return flag;
    }

    public static bool hasEnabledMoveDistanceEffects(BL.ISkillEffectListUnit unit)
    {
      foreach (BattleskillEffectLogicEnum logic in BattleFuncs.moveDistanceSkillEffectsEnum)
      {
        if (unit.HasEnabledSkillEffect(logic))
          return true;
      }
      return false;
    }

    public static bool hasEnabledRangeEffects(BL.ISkillEffectListUnit unit)
    {
      foreach (BattleskillEffectLogicEnum logic in BattleFuncs.rangeSkillEffectsEnum)
      {
        if (unit.HasEnabledSkillEffect(logic))
          return true;
      }
      return false;
    }

    public static bool hasEnabledOnemanChargeEffects(BL.ISkillEffectListUnit unit)
    {
      foreach (BattleskillEffectLogicEnum logic in BattleFuncs.onemanChargeEffectsEnum)
      {
        if (unit.HasEnabledSkillEffect(logic))
          return true;
      }
      return false;
    }

    public static bool isCharismaEffect(BattleskillEffectLogicEnum e)
    {
      return ((IEnumerable<BattleskillEffectLogicEnum>) BattleFuncs.charismaEffectsEnum).Contains<BattleskillEffectLogicEnum>(e);
    }

    public static IEnumerable<BL.SkillEffect> getEnabledCharismaEffects(
      BL.ISkillEffectListUnit unit)
    {
      BattleskillEffectLogicEnum[] battleskillEffectLogicEnumArray = BattleFuncs.charismaEffectsEnum;
      for (int index = 0; index < battleskillEffectLogicEnumArray.Length; ++index)
      {
        foreach (BL.SkillEffect enabledEffect in BattleFuncs.getEnabledEffects(unit.skillEffects, battleskillEffectLogicEnumArray[index], unit))
          yield return enabledEffect;
      }
      battleskillEffectLogicEnumArray = (BattleskillEffectLogicEnum[]) null;
    }

    public static bool hasEnabledDeadCountEffects(
      BL.ISkillEffectListUnit unit,
      BL.ISkillEffectListUnit deadUnit)
    {
      BL.ForceID forceId = BattleFuncs.env.getForceID(deadUnit.originalUnit);
      if (BattleFuncs.env.getForceID(unit.originalUnit) == forceId)
      {
        foreach (BattleskillEffectLogicEnum e in BattleFuncs.deadCountSkillEffectsEnumPlayerForce)
        {
          if (BattleFuncs.getEnabledEffects(unit.skillEffects, e, unit).Any<BL.SkillEffect>((Func<BL.SkillEffect, bool>) (x => !x.effect.HasKey(BattleskillEffectLogicArgumentEnum.unit_id) || x.effect.GetInt(BattleskillEffectLogicArgumentEnum.unit_id) == 0 || x.effect.GetInt(BattleskillEffectLogicArgumentEnum.unit_id) == deadUnit.originalUnit.unit.ID)))
            return true;
        }
      }
      else if (((IEnumerable<BL.ForceID>) BattleFuncs.env.getTargetForce(unit.originalUnit, false)).Contains<BL.ForceID>(forceId))
      {
        foreach (BattleskillEffectLogicEnum e in BattleFuncs.deadCountSkillEffectsEnumEnemyForce)
        {
          if (BattleFuncs.getEnabledEffects(unit.skillEffects, e, unit).Any<BL.SkillEffect>((Func<BL.SkillEffect, bool>) (x => !x.effect.HasKey(BattleskillEffectLogicArgumentEnum.unit_id) || x.effect.GetInt(BattleskillEffectLogicArgumentEnum.unit_id) == 0 || x.effect.GetInt(BattleskillEffectLogicArgumentEnum.unit_id) == deadUnit.originalUnit.unit.ID)))
            return true;
        }
      }
      return false;
    }

    public static IEnumerable<BL.SkillEffect> getEnabledEffects(
      BL.SkillEffectList self,
      BattleskillEffectLogicEnum e,
      BL.ISkillEffectListUnit effectUnit)
    {
      return self.Where(e, (Func<BL.SkillEffect, bool>) (effect =>
      {
        if (effect.effect.HasKey(BattleskillEffectLogicArgumentEnum.element) && effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.element) != 0 && (CommonElement) effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.element) != effectUnit.originalUnit.playerUnit.GetElement())
          return false;
        return !effect.effect.HasKey(BattleskillEffectLogicArgumentEnum.gear_kind_id) || effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) == 0 || effect.effect.GetInt(BattleskillEffectLogicArgumentEnum.gear_kind_id) == effectUnit.originalUnit.unit.kind.ID;
      }));
    }

    public static IEnumerable<BL.SkillEffect> getEnabledOnemanChargeEffects(
      BL.ISkillEffectListUnit unit)
    {
      BattleskillEffectLogicEnum[] battleskillEffectLogicEnumArray = BattleFuncs.onemanChargeEffectsEnum;
      for (int index = 0; index < battleskillEffectLogicEnumArray.Length; ++index)
      {
        foreach (BL.SkillEffect skillEffect in unit.skillEffects.Where(battleskillEffectLogicEnumArray[index]))
          yield return skillEffect;
      }
      battleskillEffectLogicEnumArray = (BattleskillEffectLogicEnum[]) null;
    }

    public static IEnumerable<BL.Panel> getSkillZocPanels(
      BL.ISkillEffectListUnit unit,
      int row,
      int column,
      bool useGearFilter)
    {
      IEnumerable<BL.SkillEffect> skillEffects = unit.enabledSkillEffect(BattleskillEffectLogicEnum.zoc);
      if (useGearFilter)
        skillEffects = BattleFuncs.gearSkillEffectFilter(unit.originalUnit, skillEffects);
      return BattleFuncs.getSkillEffectZocPanels(skillEffects, row, column);
    }

    public static IEnumerable<BL.Panel> getSkillEffectZocPanels(
      IEnumerable<BL.SkillEffect> zocSkillEffects,
      int row,
      int column)
    {
      if (zocSkillEffects.Any<BL.SkillEffect>())
      {
        foreach (BL.SkillEffect zocSkillEffect in zocSkillEffects)
        {
          int num1 = zocSkillEffect.effect.GetInt(BattleskillEffectLogicArgumentEnum.min_range);
          int num2 = zocSkillEffect.effect.GetInt(BattleskillEffectLogicArgumentEnum.max_range);
          bool includingSlanting = zocSkillEffect.effect.GetInt(BattleskillEffectLogicArgumentEnum.excluding_slanting) == 0;
          int[] range = new int[2]{ num1, num2 };
          foreach (BL.Panel rangePanel in BattleFuncs.getRangePanels(row, column, range))
          {
            if (includingSlanting || !includingSlanting && (rangePanel.row == row || rangePanel.column == column))
              yield return rangePanel;
          }
        }
      }
    }

    public struct AttackDamageData
    {
      public bool isHit;
      public bool isCritical;
      public int damage;
      public int dispDamage;
      public int realDamage;
      public int drainDamage;
      public int dispDrainDamage;
      public int defenderDispDrainDamage;
      public int defenderDrainDamage;
      public BattleDuelSkill duelSkillProc;
      public TurnHp hp;
      public int counterDamage;
      public List<string> invokeSkillExtraInfo;
      public List<BL.ISkillEffectListUnit> damageShareUnit;
      public List<int> damageShareDamage;
      public List<BL.UseSkillEffect> damageShareSkillEffect;
      public List<BL.UseSkillEffect> attackerUseSkillEffects;
      public List<BL.UseSkillEffect> defenderUseSkillEffects;
    }

    public class AsterEdge
    {
      public int to;
      public int cost;

      public AsterEdge(int to, int cost)
      {
        this.to = to;
        this.cost = cost;
      }
    }

    public class AsterNode
    {
      public BL.Panel panel;
      public BattleFuncs.AsterEdge[] edges;
      public int cost;
      public int no;
      public int from;

      public AsterNode(int no, BL.Panel panel, BattleFuncs.AsterEdge[] edges)
      {
        this.panel = panel;
        this.no = no;
        this.edges = edges;
        this.cost = 1000000000;
        this.from = -1;
      }
    }

    public class BeforeDuelDuelSupport
    {
      public IntimateDuelSupport duelSupport;
      public int hit;
      public int evasion;
      public int critical;
      public int criticalEvasion;
      public int hitIncr;
      public int evasionIncr;
      public int criticalIncr;
      public int criticalEvasionIncr;
    }

    public class InvalidSpecificSkillLogic
    {
      public int skillId { get; private set; }

      public int logicId { get; private set; }

      public object param { get; private set; }

      public static BattleFuncs.InvalidSpecificSkillLogic Create(
        int skillId,
        int logicId,
        object param = null)
      {
        return new BattleFuncs.InvalidSpecificSkillLogic()
        {
          skillId = skillId,
          logicId = logicId,
          param = param
        };
      }
    }

    public class PackedSkillEffect
    {
      private List<BattleskillEffect> effects;
      private bool ignoreHeader;
      private List<BattleskillEffect> ignoreHeaderEffects;

      public BL.SkillEffect skillEffect { get; private set; }

      public static BattleFuncs.PackedSkillEffect Create(BL.SkillEffect headerEffect)
      {
        BattleFuncs.PackedSkillEffect packedSkillEffect = new BattleFuncs.PackedSkillEffect();
        packedSkillEffect.skillEffect = headerEffect;
        packedSkillEffect.Init(headerEffect.baseSkill, headerEffect.effect);
        return packedSkillEffect;
      }

      public static BattleFuncs.PackedSkillEffect Create(BattleskillEffect headerEffect)
      {
        BattleFuncs.PackedSkillEffect packedSkillEffect = new BattleFuncs.PackedSkillEffect();
        packedSkillEffect.skillEffect = (BL.SkillEffect) null;
        packedSkillEffect.Init(headerEffect.skill, headerEffect);
        return packedSkillEffect;
      }

      private void Init(BattleskillSkill skill, BattleskillEffect headerEffect)
      {
        int id = headerEffect.ID;
        bool flag = false;
        this.effects = new List<BattleskillEffect>();
        this.effects.Add(headerEffect);
        foreach (BattleskillEffect effect in skill.Effects)
        {
          if (!flag)
          {
            if (effect.ID == id)
              flag = true;
          }
          else
          {
            if (!effect.EffectLogic.HasTag(BattleskillEffectTag.ext_arg))
              break;
            this.effects.Add(effect);
          }
        }
      }

      public bool HasKey(BattleskillEffectLogicArgumentEnum key)
      {
        return this.GetEffects().Find((Predicate<BattleskillEffect>) (x => x.HasKey(key))) != null;
      }

      public int GetInt(BattleskillEffectLogicArgumentEnum key)
      {
        return this.GetEffects().Find((Predicate<BattleskillEffect>) (x => x.HasKey(key))).GetInt(key);
      }

      public float GetFloat(BattleskillEffectLogicArgumentEnum key)
      {
        return this.GetEffects().Find((Predicate<BattleskillEffect>) (x => x.HasKey(key))).GetFloat(key);
      }

      public BattleskillEffectLogicEnum LogicEnum()
      {
        return this.effects[0].EffectLogic.Enum;
      }

      public BattleskillEffect GetHasKeyEffect(
        BattleskillEffectLogicArgumentEnum key)
      {
        return this.GetEffects().Find((Predicate<BattleskillEffect>) (x => x.HasKey(key)));
      }

      public void SetIgnoreHeader(bool v)
      {
        this.ignoreHeader = v;
        if (!this.ignoreHeader || this.ignoreHeaderEffects != null)
          return;
        this.ignoreHeaderEffects = this.effects.Where<BattleskillEffect>((Func<BattleskillEffect, bool>) (x => x.EffectLogic.HasTag(BattleskillEffectTag.ext_arg))).ToList<BattleskillEffect>();
      }

      public List<BattleskillEffect> GetEffects()
      {
        return !this.ignoreHeader ? this.effects : this.ignoreHeaderEffects;
      }

      public bool CheckLandTag(BattleLandform landform)
      {
        if (!this.HasKey(BattleskillEffectLogicArgumentEnum.land_tag1))
          return true;
        if (landform == null)
          return false;
        for (BattleskillEffectLogicArgumentEnum key = BattleskillEffectLogicArgumentEnum.land_tag1; key <= BattleskillEffectLogicArgumentEnum.land_tag10; ++key)
        {
          int tag = this.GetInt(key);
          if (tag == 0)
            return false;
          if (landform.HasTag(tag))
            return true;
        }
        return false;
      }
    }

    public class SkillParam
    {
      public BL.Unit hasUnit;
      public BL.SkillEffect effect;
      public float? addParam;
      public float? mulParam;
      public object param;
      public int category;
      private BL.Unit effectUnit_;

      public BL.Unit effectUnit
      {
        get
        {
          if (this.effectUnit_ == (BL.Unit) null)
            this.effectUnit_ = BattleFuncs.getEffectUnit(this.effect, this.hasUnit);
          return this.effectUnit_;
        }
        set
        {
          this.effectUnit_ = value;
        }
      }

      public static BattleFuncs.SkillParam Create(BL.Unit hasUnit, BL.SkillEffect effect)
      {
        return new BattleFuncs.SkillParam()
        {
          hasUnit = hasUnit,
          effect = effect
        };
      }

      public static BattleFuncs.SkillParam CreateAdd(
        BL.Unit hasUnit,
        BL.SkillEffect effect,
        float addParam,
        object param = null,
        int category = 0)
      {
        return new BattleFuncs.SkillParam()
        {
          hasUnit = hasUnit,
          effect = effect,
          addParam = new float?(addParam),
          param = param,
          category = category
        };
      }

      public static BattleFuncs.SkillParam CreateMul(
        BL.Unit hasUnit,
        BL.SkillEffect effect,
        float mulParam,
        object param = null,
        int category = 0)
      {
        return new BattleFuncs.SkillParam()
        {
          hasUnit = hasUnit,
          effect = effect,
          mulParam = new float?(mulParam),
          param = param,
          category = category
        };
      }

      public static BattleFuncs.SkillParam CreateParam(
        BL.Unit hasUnit,
        BL.SkillEffect effect,
        object param,
        int category = 0)
      {
        return new BattleFuncs.SkillParam()
        {
          hasUnit = hasUnit,
          effect = effect,
          param = param,
          category = category
        };
      }
    }

    public class BuffDebuffSwapState
    {
      private HashSet<int> flagBuff;
      private HashSet<int> flagDebuff;

      public static BattleFuncs.BuffDebuffSwapState Create(
        BL.ISkillEffectListUnit unit,
        BL.ISkillEffectListUnit target = null)
      {
        IEnumerable<BL.SkillEffect> skillEffects = unit.skillEffects.Where(BattleskillEffectLogicEnum.buff_debuff_swap, (Func<BL.SkillEffect, bool>) (x =>
        {
          if (BattleFuncs.isSealedSkillEffect(unit, x) || target != null && BattleFuncs.isEffectEnemyRangeAndInvalid(x, unit, target))
            return false;
          return target == null || !BattleFuncs.isSkillsAndEffectsInvalid(unit, target, (BL.SkillEffect) null);
        }));
        bool flag1 = false;
        HashSet<int> flag2 = (HashSet<int>) null;
        HashSet<int> flag3 = (HashSet<int>) null;
        foreach (BL.SkillEffect skillEffect in skillEffects)
        {
          if (!flag1)
          {
            flag1 = true;
            flag2 = new HashSet<int>();
            flag3 = new HashSet<int>();
          }
          int num = skillEffect.effect.GetInt(BattleskillEffectLogicArgumentEnum.buff_type);
          int skillType = skillEffect.effect.GetInt(BattleskillEffectLogicArgumentEnum.skill_type);
          int investType = skillEffect.effect.GetInt(BattleskillEffectLogicArgumentEnum.invest_type);
          if (num == 0 || num == 1)
            BattleFuncs.BuffDebuffSwapState.setFlag(flag2, skillType, investType);
          if (num == 0 || num == 2)
            BattleFuncs.BuffDebuffSwapState.setFlag(flag3, skillType, investType);
        }
        if (!flag1)
          return (BattleFuncs.BuffDebuffSwapState) null;
        return new BattleFuncs.BuffDebuffSwapState()
        {
          flagBuff = flag2,
          flagDebuff = flag3
        };
      }

      private static void setFlag(HashSet<int> flag, int skillType, int investType)
      {
        switch (skillType)
        {
          case 0:
            flag.Add(1);
            flag.Add(2);
            flag.Add(3);
            flag.Add(0);
            flag.Add(6);
            flag.Add(7);
            break;
          case 3:
            if (investType == 0 || investType == 1)
              flag.Add(3);
            if (investType != 0 && investType != 2)
              break;
            flag.Add(0);
            break;
          default:
            flag.Add(skillType);
            break;
        }
      }

      private int getFlag(BL.SkillEffect effect)
      {
        if (effect.baseSkill.skill_type != BattleskillSkillType.passive)
          return (int) effect.baseSkill.skill_type;
        return effect.isBaseSkill ? 0 : 3;
      }

      public bool isBuffSwap(BL.SkillEffect effect)
      {
        return this.flagBuff.Contains(this.getFlag(effect)) && !BattleFuncs.isBonusSkillId(effect.baseSkillId);
      }

      public bool isDebuffSwap(BL.SkillEffect effect)
      {
        return this.flagDebuff.Contains(this.getFlag(effect)) && !BattleFuncs.isBonusSkillId(effect.baseSkillId);
      }
    }

    public class InvestSkill
    {
      public BattleskillSkill skill;
      public int? turnRemain;
      public bool fromEnemy;
      public bool fromFriend;
      public bool isEnemyIcon;
    }

    public class ApplyChangeSkillEffects
    {
      private Dictionary<BL.ISkillEffectListUnit, BattleFuncs.ApplyChangeSkillEffectsOne> dict;
      private bool isAI;
      public bool clearMovePanelCacheAll;

      public ApplyChangeSkillEffects(bool isAI)
      {
        this.dict = new Dictionary<BL.ISkillEffectListUnit, BattleFuncs.ApplyChangeSkillEffectsOne>();
        this.isAI = isAI;
        this.clearMovePanelCacheAll = false;
      }

      public void add(BL.UnitPosition tup, BL.ISkillEffectListUnit target)
      {
        if (this.dict.ContainsKey(target))
          return;
        BattleFuncs.ApplyChangeSkillEffectsOne changeSkillEffectsOne = new BattleFuncs.ApplyChangeSkillEffectsOne(tup, target, this.isAI);
        this.dict[target] = changeSkillEffectsOne;
        changeSkillEffectsOne.doBefore();
      }

      public void add(BL.UnitPosition tup)
      {
        this.add(tup, BattleFuncs.unitPositionToISkillEffectListUnit(tup));
      }

      public void add(BL.ISkillEffectListUnit target)
      {
        this.add(BattleFuncs.iSkillEffectListUnitToUnitPosition(target), target);
      }

      public void execute()
      {
        foreach (BattleFuncs.ApplyChangeSkillEffectsOne changeSkillEffectsOne in this.dict.Values)
        {
          changeSkillEffectsOne.doAfter(false);
          this.clearMovePanelCacheAll |= changeSkillEffectsOne.clearMovePanelCacheAll;
        }
        if (!this.clearMovePanelCacheAll)
          return;
        BattleFuncs.getAllUnits(this.isAI, true, false).ForEach<BL.ISkillEffectListUnit>((System.Action<BL.ISkillEffectListUnit>) (x => BattleFuncs.iSkillEffectListUnitToUnitPosition(x).clearMovePanelCache()));
      }
    }

    public class ApplyChangeSkillEffectsOne
    {
      private BL.UnitPosition tup;
      private BL.ISkillEffectListUnit target;
      private bool isAI;
      public bool clearMovePanelCacheAll;
      private int backupMove;
      private bool enabledIgnoreMoveCost;
      private bool enabledSlipThru;
      private bool isDontMove;
      private List<BL.SkillEffect> charismaEffect;
      private List<BL.SkillEffect> noSealedCharismaEffect;
      private BL.SkillEffect[] enabledZocEffect;
      private int[] attackRange;
      private int[] healRange;
      private Dictionary<BL.ISkillEffectListUnit, BL.SkillEffect[]> noSealedRangeEffect;

      public ApplyChangeSkillEffectsOne(
        BL.UnitPosition tup,
        BL.ISkillEffectListUnit target,
        bool isAI)
      {
        this.tup = tup;
        this.target = target;
        this.isAI = isAI;
      }

      public ApplyChangeSkillEffectsOne(BL.UnitPosition tup)
        : this(tup, BattleFuncs.unitPositionToISkillEffectListUnit(tup), tup is BL.AIUnit)
      {
      }

      public ApplyChangeSkillEffectsOne(BL.ISkillEffectListUnit target)
        : this(BattleFuncs.iSkillEffectListUnitToUnitPosition(target), target, target is BL.AIUnit)
      {
      }

      public void doBefore()
      {
        this.clearMovePanelCacheAll = false;
        this.noSealedRangeEffect = new Dictionary<BL.ISkillEffectListUnit, BL.SkillEffect[]>();
        foreach (BL.ISkillEffectListUnit allUnit in BattleFuncs.getAllUnits(this.isAI, true, false))
        {
          BL.ISkillEffectListUnit unit = allUnit;
          List<BL.SkillEffect> rangeFromEffects = unit.skillEffects.GetRangeFromEffects(this.target.originalUnit);
          this.noSealedRangeEffect[unit] = rangeFromEffects == null ? new BL.SkillEffect[0] : rangeFromEffects.Where<BL.SkillEffect>((Func<BL.SkillEffect, bool>) (x => !BattleFuncs.isSealedSkillEffect(unit, x))).ToArray<BL.SkillEffect>();
        }
        this.backupMove = this.tup.moveCost;
        this.enabledIgnoreMoveCost = this.target.HasEnabledSkillEffect(BattleskillEffectLogicEnum.ignore_move_cost);
        this.enabledSlipThru = this.target.HasEnabledSkillEffect(BattleskillEffectLogicEnum.slip_thru);
        this.isDontMove = this.target.IsDontMove;
        this.charismaEffect = BattleFuncs.getEnabledCharismaEffects(this.target).ToList<BL.SkillEffect>();
        this.noSealedCharismaEffect = this.charismaEffect.Where<BL.SkillEffect>((Func<BL.SkillEffect, bool>) (x => !BattleFuncs.isSealedSkillEffect(this.target, x))).ToList<BL.SkillEffect>();
        this.enabledZocEffect = this.target.enabledSkillEffect(BattleskillEffectLogicEnum.zoc).ToArray<BL.SkillEffect>();
        if (this.charismaEffect.Count > 0)
          this.tup.removePanelSkillEffects(true);
        this.attackRange = this.target.attackRange;
        this.healRange = this.target.healRange;
      }

      public void doAfter(bool isClearMovePanelCacheAll = true)
      {
        foreach (KeyValuePair<BL.ISkillEffectListUnit, BL.SkillEffect[]> keyValuePair in this.noSealedRangeEffect)
        {
          BL.ISkillEffectListUnit unit = keyValuePair.Key;
          BL.SkillEffect[] skillEffectArray1 = keyValuePair.Value;
          List<BL.SkillEffect> rangeFromEffects = unit.skillEffects.GetRangeFromEffects(this.target.originalUnit);
          BL.SkillEffect[] skillEffectArray2 = rangeFromEffects == null ? new BL.SkillEffect[0] : rangeFromEffects.Where<BL.SkillEffect>((Func<BL.SkillEffect, bool>) (x => !BattleFuncs.isSealedSkillEffect(unit, x))).ToArray<BL.SkillEffect>();
          IEnumerable<BL.SkillEffect> source = ((IEnumerable<BL.SkillEffect>) skillEffectArray1).Union<BL.SkillEffect>((IEnumerable<BL.SkillEffect>) skillEffectArray2).Except<BL.SkillEffect>(((IEnumerable<BL.SkillEffect>) skillEffectArray1).Intersect<BL.SkillEffect>((IEnumerable<BL.SkillEffect>) skillEffectArray2));
          if (source.Any<BL.SkillEffect>())
          {
            unit.skillEffects.commit();
            if (!this.isAI)
              unit.originalUnit.commit();
            if (source.Any<BL.SkillEffect>((Func<BL.SkillEffect, bool>) (x => x.effect.EffectLogic.Enum == BattleskillEffectLogicEnum.ignore_move_cost || x.effect.EffectLogic.Enum == BattleskillEffectLogicEnum.slip_thru || x.effect.EffectLogic.opt_test4 == 8)))
              BattleFuncs.iSkillEffectListUnitToUnitPosition(unit).clearMovePanelCache();
            if (source.Any<BL.SkillEffect>((Func<BL.SkillEffect, bool>) (x => x.effect.EffectLogic.Enum == BattleskillEffectLogicEnum.fix_range)))
            {
              BL.UnitPosition unitPosition = BattleFuncs.iSkillEffectListUnitToUnitPosition(unit);
              unitPosition.clearMoveActionRangePanelCache();
              unitPosition.clearMoveHealRangePanelCache();
            }
          }
        }
        if (this.tup.moveCost != this.backupMove || this.isDontMove != this.target.IsDontMove || (this.enabledIgnoreMoveCost != this.target.HasEnabledSkillEffect(BattleskillEffectLogicEnum.ignore_move_cost) || this.enabledSlipThru != this.target.HasEnabledSkillEffect(BattleskillEffectLogicEnum.slip_thru)))
          this.tup.clearMovePanelCache();
        int[] attackRange = this.target.attackRange;
        int[] healRange = this.target.healRange;
        if (this.attackRange[0] != attackRange[0] || this.attackRange[1] != attackRange[1])
          this.tup.clearMoveActionRangePanelCache();
        if (this.healRange.Length != healRange.Length || this.healRange.Length == 2 && (this.healRange[0] != healRange[0] || this.healRange[1] != healRange[1]))
          this.tup.clearMoveHealRangePanelCache();
        List<BL.SkillEffect> list1 = BattleFuncs.getEnabledCharismaEffects(this.target).ToList<BL.SkillEffect>();
        IEnumerable<BL.SkillEffect> source1 = this.charismaEffect.Union<BL.SkillEffect>((IEnumerable<BL.SkillEffect>) list1).Except<BL.SkillEffect>(this.charismaEffect.Intersect<BL.SkillEffect>((IEnumerable<BL.SkillEffect>) list1));
        if (list1.Count > 0)
        {
          if (source1.Any<BL.SkillEffect>())
            this.tup.commitPanelSkillEffects(this.charismaEffect.Except<BL.SkillEffect>((IEnumerable<BL.SkillEffect>) list1));
          this.tup.addPanelSkillEffects(!source1.Any<BL.SkillEffect>());
        }
        else
          this.tup.commitPanelSkillEffects((IEnumerable<BL.SkillEffect>) this.charismaEffect);
        List<BL.SkillEffect> list2 = list1.Where<BL.SkillEffect>((Func<BL.SkillEffect, bool>) (x => !BattleFuncs.isSealedSkillEffect(this.target, x))).ToList<BL.SkillEffect>();
        this.tup.commitPanelSkillEffects(this.noSealedCharismaEffect.Union<BL.SkillEffect>((IEnumerable<BL.SkillEffect>) list2).Except<BL.SkillEffect>(this.noSealedCharismaEffect.Intersect<BL.SkillEffect>((IEnumerable<BL.SkillEffect>) list2)));
        BL.SkillEffect[] array1 = this.target.enabledSkillEffect(BattleskillEffectLogicEnum.zoc).ToArray<BL.SkillEffect>();
        if (((IEnumerable<BL.SkillEffect>) this.enabledZocEffect).Union<BL.SkillEffect>((IEnumerable<BL.SkillEffect>) array1).Except<BL.SkillEffect>(((IEnumerable<BL.SkillEffect>) this.enabledZocEffect).Intersect<BL.SkillEffect>((IEnumerable<BL.SkillEffect>) array1)).Any<BL.SkillEffect>())
        {
          BL.SkillEffect[] array2 = ((IEnumerable<BL.SkillEffect>) this.enabledZocEffect).Except<BL.SkillEffect>((IEnumerable<BL.SkillEffect>) array1).ToArray<BL.SkillEffect>();
          if (array2.Length != 0)
          {
            foreach (BL.Panel skillEffectZocPanel in BattleFuncs.getSkillEffectZocPanels((IEnumerable<BL.SkillEffect>) array2, this.tup.row, this.tup.column))
              skillEffectZocPanel.removeZocUnit(this.target.originalUnit, this.isAI);
          }
          if (array1.Length != 0)
          {
            foreach (BL.Panel skillEffectZocPanel in BattleFuncs.getSkillEffectZocPanels((IEnumerable<BL.SkillEffect>) array1, this.tup.row, this.tup.column))
              skillEffectZocPanel.addZocUnit(this.target.originalUnit, this.isAI);
          }
          this.clearMovePanelCacheAll = true;
        }
        if (!isClearMovePanelCacheAll || !this.clearMovePanelCacheAll)
          return;
        BattleFuncs.getAllUnits(this.isAI, true, false).ForEach<BL.ISkillEffectListUnit>((System.Action<BL.ISkillEffectListUnit>) (x => BattleFuncs.iSkillEffectListUnitToUnitPosition(x).clearMovePanelCache()));
      }
    }

    private class SnakeVenomDamageData
    {
      public BL.ISkillEffectListUnit invokeUnit;
      public BL.SkillEffect effect;
      public int invokeOrder;
      public int turnDamage;
      public Dictionary<BL.ISkillEffectListUnit, int> unitDamage;
    }
  }
}
