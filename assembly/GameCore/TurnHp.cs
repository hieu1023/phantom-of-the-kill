﻿// Decompiled with JetBrains decompiler
// Type: GameCore.TurnHp
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

namespace GameCore
{
  public class TurnHp
  {
    public int attackerHp;
    public int defenderHp;
    public bool attackerIsDontAction;
    public bool defenderIsDontAction;
    public bool attackerIsDontEvasion;
    public bool defenderIsDontEvasion;
    public bool attackerIsDontUseSkill;
    public bool defenderIsDontUseSkill;
    public bool attackerCantOneMore;
    public bool defenderCantOneMore;
    public Dictionary<BL.ISkillEffectListUnit, TurnOtherHp> otherHp;

    public bool isDieAttackerOrDefender()
    {
      return this.attackerHp <= 0 || this.defenderHp <= 0;
    }
  }
}
