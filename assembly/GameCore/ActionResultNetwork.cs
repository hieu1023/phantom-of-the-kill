﻿// Decompiled with JetBrains decompiler
// Type: GameCore.ActionResultNetwork
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace GameCore
{
  [Serializable]
  public class ActionResultNetwork
  {
    public bool isMove;
    public int row;
    public int column;
    public bool terminate;
  }
}
