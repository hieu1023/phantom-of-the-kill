﻿// Decompiled with JetBrains decompiler
// Type: GameCore.MoveCompleteResult
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace GameCore
{
  [Serializable]
  public class MoveCompleteResult : ActionResult
  {
    public MoveCompleteResult(int r, int c)
    {
      this.row = r;
      this.column = c;
      this.isMove = true;
      this.terminate = true;
    }

    public override ActionResultNetwork ToNetworkLocal(BL env)
    {
      return new ActionResultNetwork();
    }
  }
}
