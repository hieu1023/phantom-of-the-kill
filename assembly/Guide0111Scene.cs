﻿// Decompiled with JetBrains decompiler
// Type: Guide0111Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;
using UnityEngine;

public class Guide0111Scene : NGSceneBase
{
  public override IEnumerator onInitSceneAsync()
  {
    Guide0111Scene guide0111Scene = this;
    Singleton<CommonRoot>.GetInstance().ShowLoadingLayer(0, false);
    Future<GameObject> fBG = Res.Prefabs.BackGround.picturebook.Load<GameObject>();
    IEnumerator e = fBG.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    guide0111Scene.backgroundPrefab = fBG.Result;
  }

  public IEnumerator onStartSceneAsync()
  {
    if (Singleton<CommonRoot>.GetInstance().isLoading)
    {
      yield return (object) null;
      Singleton<CommonRoot>.GetInstance().HideLoadingLayer();
    }
  }

  public override void onSceneInitialized()
  {
    base.onSceneInitialized();
    Singleton<CommonRoot>.GetInstance().loadingMode = 0;
  }

  public class UpdateInfo
  {
    public long? version;
    public long? verSub;
  }
}
