﻿// Decompiled with JetBrains decompiler
// Type: Shop00742CommonTicket
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Shop00742CommonTicket : MonoBehaviour
{
  [SerializeField]
  private UILabel TxtFlavor;
  [SerializeField]
  private UI2DSprite SlcTarget;
  [SerializeField]
  private UI2DSprite background;

  public IEnumerator Init(MasterDataTable.CommonRewardType type, int entity_id)
  {
    switch (type)
    {
      case MasterDataTable.CommonRewardType.gacha_ticket:
        yield return (object) this.doGachaTicket(entity_id);
        break;
      case MasterDataTable.CommonRewardType.unit_ticket:
        yield return (object) this.doUnitTicket(entity_id);
        break;
      case MasterDataTable.CommonRewardType.stamp:
        yield return (object) this.doStamp(entity_id);
        break;
      case MasterDataTable.CommonRewardType.reincarnation_type_ticket:
        yield return (object) this.doTicket(entity_id);
        break;
    }
  }

  private IEnumerator doGachaTicket(int entity_id)
  {
    this.TxtFlavor.SetText(((IEnumerable<GachaTicket>) MasterData.GachaTicketList).First<GachaTicket>((Func<GachaTicket, bool>) (x => x.ID == entity_id)).name);
    Future<UnityEngine.Sprite> r = Singleton<ResourceManager>.GetInstance().Load<UnityEngine.Sprite>(string.Format("GachaTicket/{0}/ticket", (object) entity_id), 1f);
    IEnumerator e = r.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.SlcTarget.sprite2D = r.Result;
  }

  private IEnumerator doUnitTicket(int entity_id)
  {
    this.TxtFlavor.SetText(((IEnumerable<SelectTicket>) MasterData.SelectTicketList).First<SelectTicket>((Func<SelectTicket, bool>) (x => x.ID == entity_id)).name);
    Future<UnityEngine.Sprite> r = Singleton<ResourceManager>.GetInstance().Load<UnityEngine.Sprite>(string.Format("UnitTicket/{0}/ticket", (object) entity_id), 1f);
    IEnumerator e = r.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.SlcTarget.sprite2D = r.Result;
  }

  private IEnumerator doTicket(int entity_id)
  {
    UnitTypeTicket unitTypeTicket = ((IEnumerable<UnitTypeTicket>) MasterData.UnitTypeTicketList).First<UnitTypeTicket>((Func<UnitTypeTicket, bool>) (x => x.ID == entity_id));
    this.TxtFlavor.SetText(unitTypeTicket.name);
    Future<UnityEngine.Sprite> r = Singleton<ResourceManager>.GetInstance().Load<UnityEngine.Sprite>(string.Format("ReincarnationTypeTicket/{0}/ticket", (object) unitTypeTicket.icon_id), 1f);
    IEnumerator e = r.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.SlcTarget.sprite2D = r.Result;
  }

  private IEnumerator doStamp(int entity_id)
  {
    this.background.enabled = false;
    this.SlcTarget.enabled = false;
    this.TxtFlavor.SetText(((IEnumerable<GuildStamp>) MasterData.GuildStampList).First<GuildStamp>((Func<GuildStamp, bool>) (x => x.groupID_GuildStampGroup == entity_id)).groupID.name);
    Future<GameObject> r = Singleton<ResourceManager>.GetInstance().Load<GameObject>(string.Format("GUI/chat_stamp_group{0}/chat_stamp_group{1}_prefab", (object) entity_id, (object) entity_id), 1f);
    IEnumerator e = r.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    UIAtlas component = r.Result.GetComponent<UIAtlas>();
    UISprite uiSprite = this.SlcTarget.gameObject.AddComponent<UISprite>();
    uiSprite.atlas = component;
    uiSprite.spriteName = component.spriteList[0].name;
    uiSprite.depth = this.SlcTarget.depth;
  }
}
