﻿// Decompiled with JetBrains decompiler
// Type: NGTweenGaugeScale
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class NGTweenGaugeScale : MonoBehaviour
{
  private float gaugeValue = -1f;
  [SerializeField]
  protected GameObject gauge;
  private TweenScale _gaugeTweener;

  private TweenScale gaugeTweener
  {
    get
    {
      if ((Object) this._gaugeTweener == (Object) null)
      {
        TweenScale[] componentsInChildren = this.gauge.GetComponentsInChildren<TweenScale>();
        if (componentsInChildren.Length != 0)
          this._gaugeTweener = componentsInChildren[0];
      }
      return this._gaugeTweener;
    }
  }

  public bool setValue(int n, int max, bool doTween = true, float delay = -1f, float duration = -1f)
  {
    if ((Object) this.gaugeTweener != (Object) null)
    {
      if ((double) delay < 0.0)
        delay = this.gaugeTweener.delay;
      if ((double) duration < 0.0)
        duration = this.gaugeTweener.duration;
    }
    if (Mathf.Approximately((float) max, 0.0f))
      return false;
    float b = (float) n / (float) max;
    if (Mathf.Approximately(this.gaugeValue, b))
      return false;
    float x = b;
    if (doTween && (Object) this.gaugeTweener != (Object) null)
    {
      this.gaugeTweener.from = this.gauge.transform.localScale;
      this.gaugeTweener.to = new Vector3(x, this.gauge.transform.localScale.y, this.gauge.transform.localScale.z);
      this.gaugeTweener.duration = duration;
      this.gaugeTweener.delay = delay;
      NGTween.playTween((UITweener) this.gaugeTweener, false);
    }
    else
    {
      if ((Object) this.gaugeTweener != (Object) null)
        this.gaugeTweener.enabled = false;
      this.gauge.transform.localScale = new Vector3(x, this.gauge.transform.localScale.y, this.gauge.transform.localScale.z);
    }
    this.gaugeValue = b;
    return true;
  }
}
