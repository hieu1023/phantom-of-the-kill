﻿// Decompiled with JetBrains decompiler
// Type: BattleManagerBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;
using UnityEngine;

public abstract class BattleManagerBase : MonoBehaviour
{
  public abstract IEnumerator initialize(BattleInfo battleInfo, BE env_ = null);

  public abstract IEnumerator cleanup();
}
