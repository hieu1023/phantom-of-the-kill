﻿// Decompiled with JetBrains decompiler
// Type: SM_QuestHMExtension
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;
using System;
using System.Collections.Generic;
using UniLinq;

public static class SM_QuestHMExtension
{
  public static PlayerHarmonyQuestM[] FilterDisabled(
    this PlayerHarmonyQuestM[] self,
    Dictionary<int, bool> playableQuestIDsMap)
  {
    return ((IEnumerable<PlayerHarmonyQuestM>) self).Select<PlayerHarmonyQuestM, PlayerHarmonyQuestM>((Func<PlayerHarmonyQuestM, PlayerHarmonyQuestM>) (quest =>
    {
      quest.player_quests = ((IEnumerable<PlayerHarmonyQuestS>) quest.player_quests).Where<PlayerHarmonyQuestS>((Func<PlayerHarmonyQuestS, bool>) (questDetail => !questDetail.quest_harmony_s.start_at.HasValue || questDetail.quest_harmony_s.start_at.Value.CompareTo(ServerTime.NowAppTime()) <= 0)).ToArray<PlayerHarmonyQuestS>();
      if (!playableQuestIDsMap.ContainsKey(quest._quest_m_id))
        quest.is_playable = false;
      return quest;
    })).Where<PlayerHarmonyQuestM>((Func<PlayerHarmonyQuestM, bool>) (quest => (uint) quest.player_quests.Length > 0U)).ToArray<PlayerHarmonyQuestM>();
  }
}
