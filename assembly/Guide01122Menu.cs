﻿// Decompiled with JetBrains decompiler
// Type: Guide01122Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Guide01122Menu : BackButtonMenuBase
{
  private bool isGroupOpen = true;
  private float groupSpriteHeight = 46.5f;
  private int groupBaseHeightInit = 36;
  [SerializeField]
  private NGHorizontalScrollParts informationScrollView;
  [SerializeField]
  private SpriteSelectDirectButton[] groupSprites;
  [SerializeField]
  private GameObject slc_GroupBase;
  private TweenHeight tween_GroupBaseOpen;
  private TweenHeight tween_GroupBaseClose;
  [SerializeField]
  private GameObject dir_GroupSprite;
  private TweenPosition tween_GroupSpriteDirOpen;
  private TweenPosition tween_GroupSpriteDirClose;
  [SerializeField]
  private UIButton IbtnGroupTabIdle;
  [SerializeField]
  private GameObject dir_GroupPressed;
  private bool isGroupTween;
  private int dispGroupCount;
  private const int TWEEN_GROUPID_START = 100;
  private const int TWEEN_GROUPID_END = 101;
  [SerializeField]
  private GameObject slcSelectEvolution;
  [SerializeField]
  private UIButton ibtnEvolution;
  [SerializeField]
  private UI2DSprite rarityBtnSprite;
  private GameObject floatingRarityDialogObject;
  [SerializeField]
  private GameObject slcAwakening;
  [SerializeField]
  private UISprite slcCountry;
  public UILabel txt_CharacterName;
  public UILabel txt_JobName;
  public UILabel txt_number;
  public UILabel txt_DefeatNum;
  public UI2DSprite DynCharacter;
  public UI2DSprite rarityStarsIcon;
  public GearKindIcon GearKindIcon;
  public GuideUnitDetailScrollViewSkill detailSkill;
  public GuideUnitDetailScrollViewJob detailJob;
  private UnitUnit unit_;
  private List<UnitUnit> commonUnitList;
  private List<PlayerUnitHistory> historyList;
  private int job_id;
  private MasterDataTable.UnitJob jobData;
  private PlayerUnitSkills koyuDuel;
  private PlayerUnitSkills koyuMulti;
  private int m_windowHeight;
  private int m_windowWidth;
  private RenderTextureRecoveryUtil util;

  public virtual void IbtnZoom()
  {
    if (this.IsPushAndSet())
      return;
    Unit0043Scene.changeScene(true, this.unit_, this.job_id, true, this.koyuDuel, this.koyuMulti, true);
  }

  public virtual void IbtnBack()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<NGSoundManager>.GetInstance().stopVoice(-1);
    this.backScene();
  }

  public override void onBackButton()
  {
    this.IbtnBack();
  }

  private void OnDestroy()
  {
    Singleton<NGSoundManager>.GetInstance().stopVoice(-1);
  }

  public virtual void updateMenu(UnitUnit unit, int job_id)
  {
    this.StartCoroutine(this.onStartSceneAsync(unit, false, job_id));
  }

  public IEnumerator onStartSceneAsync(UnitUnit unit, bool voiceFlag, int select_job_id = 0)
  {
    Guide01122Menu guide01122Menu = this;
    guide01122Menu.unit_ = unit;
    guide01122Menu.koyuDuel = guide01122Menu.koyuMulti = (PlayerUnitSkills) null;
    guide01122Menu.commonUnitList = ((IEnumerable<UnitUnit>) MasterData.UnitUnitList).Where<UnitUnit>((Func<UnitUnit, bool>) (x => x.character.category == UnitCategory.player)).Where<UnitUnit>((Func<UnitUnit, bool>) (x => x.history_group_number == unit.history_group_number)).ToList<UnitUnit>();
    guide01122Menu.historyList = ((IEnumerable<PlayerUnitHistory>) SMManager.Get<PlayerUnitHistory[]>()).ToList<PlayerUnitHistory>();
    int? nullable = guide01122Menu.historyList.FirstIndexOrNull<PlayerUnitHistory>((Func<PlayerUnitHistory, bool>) (x => x.unit_id == unit.ID));
    if (nullable.HasValue)
    {
      PlayerUnitHistory history = guide01122Menu.historyList[nullable.Value];
      guide01122Menu.job_id = 0;
      guide01122Menu.jobData = (MasterDataTable.UnitJob) null;
      if (select_job_id != 0)
      {
        if (select_job_id != unit.job_UnitJob)
        {
          guide01122Menu.job_id = select_job_id;
          guide01122Menu.jobData = MasterData.UnitJob[guide01122Menu.job_id];
        }
      }
      else
      {
        int[] array = unit.getClassChangeJobIdList().ToArray();
        if (((IEnumerable<int>) array).Any<int>())
        {
          foreach (int num in ((IEnumerable<int>) array).Reverse<int>())
          {
            if (((IEnumerable<int?>) history.job_ids).Contains<int?>(new int?(num)))
            {
              guide01122Menu.job_id = num;
              guide01122Menu.jobData = MasterData.UnitJob[guide01122Menu.job_id];
              break;
            }
          }
        }
      }
      if (guide01122Menu.job_id == 0)
      {
        guide01122Menu.job_id = unit.job_UnitJob;
        guide01122Menu.jobData = MasterData.UnitJob[guide01122Menu.job_id];
      }
      guide01122Menu.SetUnitName(unit);
      guide01122Menu.SetJobName(unit);
      PlayerUnit playerUnit = PlayerUnit.create_by_unitunit(unit, 0);
      playerUnit.job_id = guide01122Menu.jobData.ID;
      guide01122Menu.SetUnitRarity(playerUnit);
      guide01122Menu.SetUnitGearType(unit, guide01122Menu.job_id);
      guide01122Menu.SetNumber(unit);
      guide01122Menu.SetDefeat(unit, guide01122Menu.commonUnitList, guide01122Menu.historyList);
      IEnumerator e = guide01122Menu.SetUnitImg(unit);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      e = guide01122Menu.detailSkill.init(unit, history);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      guide01122Menu.koyuDuel = guide01122Menu.detailSkill.koyuDuel;
      guide01122Menu.koyuMulti = guide01122Menu.detailSkill.koyuMulti;
      e = guide01122Menu.detailJob.init(unit, guide01122Menu.jobData);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      guide01122Menu.util = guide01122Menu.GetComponent<RenderTextureRecoveryUtil>();
      if ((UnityEngine.Object) guide01122Menu.util != (UnityEngine.Object) null)
        guide01122Menu.util.SaveRenderTexture();
      if ((bool) (UnityEngine.Object) guide01122Menu.slc_GroupBase)
      {
        foreach (TweenHeight componentsInChild in guide01122Menu.slc_GroupBase.GetComponentsInChildren<TweenHeight>())
        {
          if (componentsInChild.tweenGroup == 100)
            guide01122Menu.tween_GroupBaseOpen = componentsInChild;
          if (componentsInChild.tweenGroup == 101)
            guide01122Menu.tween_GroupBaseClose = componentsInChild;
        }
        foreach (TweenPosition componentsInChild in guide01122Menu.dir_GroupSprite.GetComponentsInChildren<TweenPosition>())
        {
          if (componentsInChild.tweenGroup == 100)
            guide01122Menu.tween_GroupSpriteDirOpen = componentsInChild;
          if (componentsInChild.tweenGroup == 101)
            guide01122Menu.tween_GroupSpriteDirClose = componentsInChild;
        }
        guide01122Menu.DisplayGroupLogo(unit);
      }
      RarityIcon.SetRarity(playerUnit, guide01122Menu.rarityBtnSprite, false, true, true);
      if (guide01122Menu.commonUnitList.OrderBy<UnitUnit, int>((Func<UnitUnit, int>) (x => x.ID)).ToArray<UnitUnit>().Length <= 1)
      {
        guide01122Menu.slcSelectEvolution.SetActive(false);
        guide01122Menu.ibtnEvolution.enabled = false;
      }
      if (voiceFlag && (bool) (UnityEngine.Object) Singleton<NGSoundManager>.GetInstanceOrNull())
      {
        Singleton<NGSoundManager>.GetInstance().stopVoice(-1);
        Singleton<NGSoundManager>.GetInstance().playVoiceByID(unit.unitVoicePattern, 42, -1, 0.0f);
      }
    }
  }

  public void onEndScene()
  {
  }

  public IEnumerator onEndSceneAsync()
  {
    NGSoundManager instance = Singleton<NGSoundManager>.GetInstance();
    if ((UnityEngine.Object) instance != (UnityEngine.Object) null)
    {
      instance.stopVoice(-1);
      yield break;
    }
  }

  private void DisplayGroupLogo(UnitUnit playerUnit)
  {
    if (this.groupSprites == null)
      return;
    int index = 0;
    ((IEnumerable<SpriteSelectDirectButton>) this.groupSprites).ForEach<SpriteSelectDirectButton>((System.Action<SpriteSelectDirectButton>) (x => x.gameObject.SetActive(false)));
    UnitGroup groupInfo = ((IEnumerable<UnitGroup>) MasterData.UnitGroupList).FirstOrDefault<UnitGroup>((Func<UnitGroup, bool>) (x => x.unit_id == playerUnit.ID));
    if (groupInfo != null)
    {
      UnitGroupLargeCategory groupLargeCategory = ((IEnumerable<UnitGroupLargeCategory>) MasterData.UnitGroupLargeCategoryList).FirstOrDefault<UnitGroupLargeCategory>((Func<UnitGroupLargeCategory, bool>) (x => x.ID == groupInfo.group_large_category_id_UnitGroupLargeCategory));
      UnitGroupSmallCategory groupSmallCategory = ((IEnumerable<UnitGroupSmallCategory>) MasterData.UnitGroupSmallCategoryList).FirstOrDefault<UnitGroupSmallCategory>((Func<UnitGroupSmallCategory, bool>) (x => x.ID == groupInfo.group_small_category_id_UnitGroupSmallCategory));
      UnitGroupClothingCategory clothingCategory1 = ((IEnumerable<UnitGroupClothingCategory>) MasterData.UnitGroupClothingCategoryList).FirstOrDefault<UnitGroupClothingCategory>((Func<UnitGroupClothingCategory, bool>) (x => x.ID == groupInfo.group_clothing_category_id_UnitGroupClothingCategory));
      UnitGroupClothingCategory clothingCategory2 = ((IEnumerable<UnitGroupClothingCategory>) MasterData.UnitGroupClothingCategoryList).FirstOrDefault<UnitGroupClothingCategory>((Func<UnitGroupClothingCategory, bool>) (x => x.ID == groupInfo.group_clothing_category_id_2_UnitGroupClothingCategory));
      UnitGroupGenerationCategory generationCategory = ((IEnumerable<UnitGroupGenerationCategory>) MasterData.UnitGroupGenerationCategoryList).FirstOrDefault<UnitGroupGenerationCategory>((Func<UnitGroupGenerationCategory, bool>) (x => x.ID == groupInfo.group_generation_category_id_UnitGroupGenerationCategory));
      if (groupLargeCategory == null && groupSmallCategory == null && (clothingCategory1 == null && clothingCategory2 == null) && generationCategory == null)
        return;
      if (groupLargeCategory != null && groupLargeCategory.ID != 1)
      {
        this.groupSprites[index].SetSpriteName<string>(groupLargeCategory.GetSpriteName(), true);
        this.groupSprites[index].gameObject.SetActive(true);
        ++index;
      }
      if (groupSmallCategory != null && groupSmallCategory.ID != 1)
      {
        this.groupSprites[index].SetSpriteName<string>(groupSmallCategory.GetSpriteName(), true);
        this.groupSprites[index].gameObject.SetActive(true);
        ++index;
      }
      if (clothingCategory1 != null && clothingCategory1.ID != 1)
      {
        this.groupSprites[index].SetSpriteName<string>(clothingCategory1.GetSpriteName(), true);
        this.groupSprites[index].gameObject.SetActive(true);
        ++index;
      }
      if (clothingCategory2 != null && clothingCategory2.ID != 1)
      {
        this.groupSprites[index].SetSpriteName<string>(clothingCategory2.GetSpriteName(), true);
        this.groupSprites[index].gameObject.SetActive(true);
        ++index;
      }
      if (generationCategory != null && generationCategory.ID != 1)
      {
        this.groupSprites[index].SetSpriteName<string>(generationCategory.GetSpriteName(), true);
        this.groupSprites[index].gameObject.SetActive(true);
        ++index;
      }
    }
    if (!((UnityEngine.Object) this.slc_GroupBase != (UnityEngine.Object) null))
      return;
    this.dispGroupCount = index;
    this.tween_GroupBaseOpen.to = (int) this.getGroupHeightTarget();
    this.tween_GroupBaseClose.from = (int) this.getGroupHeightTarget();
    this.tween_GroupSpriteDirOpen.to.y = this.getGroupPositionTarget();
    this.tween_GroupSpriteDirOpen.from.y = this.getGroupPositionInit();
    this.tween_GroupSpriteDirClose.to.y = this.getGroupPositionInit();
    this.tween_GroupSpriteDirClose.from.y = this.getGroupPositionTarget();
    this.setGroupPos();
    if (this.dispGroupCount != 0)
      return;
    this.IbtnGroupTabIdle.gameObject.SetActive(false);
    this.dir_GroupPressed.SetActive(false);
  }

  public void IbtnGroupOpen()
  {
    if (this.isGroupTween || this.isGroupOpen)
      return;
    this.IbtnGroupTabIdle.gameObject.SetActive(false);
    this.dir_GroupPressed.SetActive(true);
    NGTween.playTweens(this.GetComponentsInChildren<UITweener>(true), 100, false);
    this.isGroupOpen = true;
    this.isGroupTween = true;
  }

  public void onFinishedGroupOpen()
  {
    this.isGroupTween = false;
  }

  public void IbtnGroupClose()
  {
    if (this.isGroupTween || !this.isGroupOpen)
      return;
    NGTween.playTweens(this.GetComponentsInChildren<UITweener>(true), 101, false);
    this.isGroupOpen = false;
    this.isGroupTween = true;
  }

  public void onFinishedGroupClose()
  {
    if (!this.isGroupTween)
      return;
    if (this.dispGroupCount > 0)
      this.IbtnGroupTabIdle.gameObject.SetActive(true);
    else
      this.IbtnGroupTabIdle.gameObject.SetActive(false);
    this.dir_GroupPressed.SetActive(false);
    this.isGroupTween = false;
  }

  private void setGroupPos()
  {
    if (this.isGroupOpen)
    {
      this.slc_GroupBase.GetComponent<UISprite>().height = (int) this.getGroupHeightTarget();
      this.dir_GroupSprite.transform.localPosition = new Vector3(this.dir_GroupSprite.transform.localPosition.x, this.getGroupPositionTarget(), this.dir_GroupSprite.transform.localPosition.z);
      this.IbtnGroupTabIdle.gameObject.SetActive(false);
      this.dir_GroupPressed.SetActive(true);
    }
    else
    {
      this.slc_GroupBase.GetComponent<UISprite>().height = this.groupBaseHeightInit;
      this.dir_GroupSprite.transform.localPosition = new Vector3(this.dir_GroupSprite.transform.localPosition.x, this.getGroupPositionInit(), this.dir_GroupSprite.transform.localPosition.z);
      this.IbtnGroupTabIdle.gameObject.SetActive(true);
      this.dir_GroupPressed.SetActive(false);
    }
    this.isGroupTween = false;
  }

  private float getGroupHeightTarget()
  {
    return (float) ((double) this.groupBaseHeightInit + (double) this.groupSpriteHeight * (double) this.dispGroupCount + 8.0);
  }

  private float getGroupPositionTarget()
  {
    return 0.0f;
  }

  private float getGroupPositionInit()
  {
    return (float) (-(double) this.groupSpriteHeight * (double) this.dispGroupCount - 8.0);
  }

  public void onSelectRarityBtn()
  {
    if (this.IsPushAndSet())
      return;
    this.StartCoroutine(this.openRarityDetailDialog());
  }

  public IEnumerator openRarityDetailDialog()
  {
    Guide01122Menu menu = this;
    Future<GameObject> loader = new ResourceObject("Prefabs/guide01122/SelectRarity").Load<GameObject>();
    IEnumerator e = loader.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    GameObject prefab = loader.Result.Clone((Transform) null);
    prefab.GetComponent<GuideRaritySelectDialog>().Init(menu, menu.unit_, menu.commonUnitList.ToArray(), menu.historyList.ToArray(), menu.job_id);
    Singleton<PopupManager>.GetInstance().open(prefab, false, false, true, true, false, false, "SE_1006");
    menu.IsPushOff();
  }

  public void SetUnitName(UnitUnit unit)
  {
    if (string.IsNullOrEmpty(unit.formal_name))
      this.txt_CharacterName.SetText(unit.name);
    else
      this.txt_CharacterName.SetText(unit.formal_name);
  }

  public void SetJobName(UnitUnit unit)
  {
    this.txt_JobName.SetTextLocalize(this.jobData.name);
  }

  public void SetNumber(UnitUnit unit)
  {
    this.txt_number.SetTextLocalize("NO." + (unit.history_group_number % 1000).ToString().PadLeft(3, '0'));
  }

  public void SetDefeat(
    UnitUnit unit,
    List<UnitUnit> commonUnitList,
    List<PlayerUnitHistory> historyList)
  {
    int defeat = 0;
    historyList.ForEach((System.Action<PlayerUnitHistory>) (obj =>
    {
      if (obj.unit_id != unit.ID)
        return;
      defeat += obj.defeat;
    }));
    if (defeat > 99999)
      defeat = 99999;
    this.txt_DefeatNum.SetTextLocalize(defeat);
  }

  public void SetUnitRarity(PlayerUnit playerUnit)
  {
    RarityIcon.SetRarity(playerUnit, this.rarityStarsIcon, true, true, false);
    this.slcAwakening.SetActive(false);
    if (!playerUnit.unit.awake_unit_flag)
      return;
    this.slcAwakening.SetActive(true);
  }

  public void SetUnitGearType(UnitUnit unit, int job_id)
  {
    if (!(bool) (UnityEngine.Object) this.GearKindIcon)
      return;
    this.GearKindIcon.Init(unit.GetInitialGear(job_id).kind, unit.GetElement());
  }

  public IEnumerator SetUnitImg(UnitUnit unit)
  {
    Future<UnityEngine.Sprite> loader = unit.job_UnitJob == this.job_id ? unit.LoadFullSprite(1f) : unit.LoadJobFullSprite(this.job_id);
    IEnumerator e = loader.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.DynCharacter.sprite2D = loader.Result;
  }

  protected override void Update()
  {
    if (this.m_windowHeight == 0 || this.m_windowWidth == 0)
    {
      this.m_windowHeight = Screen.height;
      this.m_windowWidth = Screen.width;
    }
    else if (this.m_windowHeight != Screen.height || this.m_windowWidth != Screen.width)
    {
      this.StartCoroutine(this.onStartSceneAsync(this.unit_, false, 0));
      this.m_windowHeight = Screen.height;
      this.m_windowWidth = Screen.width;
    }
    base.Update();
    if (!((UnityEngine.Object) this.util != (UnityEngine.Object) null))
      return;
    this.util.FixRenderTexture();
  }
}
