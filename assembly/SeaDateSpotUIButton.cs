﻿// Decompiled with JetBrains decompiler
// Type: SeaDateSpotUIButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class SeaDateSpotUIButton : UIButton
{
  protected override void SetState(UIButtonColor.State state, bool immediate)
  {
    base.SetState(state, immediate);
    if (state != UIButtonColor.State.Pressed)
      this.playTweenScale(false);
    else
      this.playTweenScale(true);
  }

  protected void playTweenScale(bool isPressed)
  {
    TweenScale componentInChildren = this.GetComponentInChildren<TweenScale>();
    if (!((Object) componentInChildren != (Object) null))
      return;
    if (isPressed)
      componentInChildren.PlayForward();
    else
      componentInChildren.ResetToBeginning();
  }
}
