﻿// Decompiled with JetBrains decompiler
// Type: BattleUI05HardModeOpenPopup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

public class BattleUI05HardModeOpenPopup : NGMenuBase
{
  public UILabel TxtTitle;
  public UILabel TxtDescription;
  private BattleUI05HardModeOpenMenu menu;

  public void IbtnPopupOk()
  {
    this.menu.ToNext = true;
  }

  public void Init(BattleUI05HardModeOpenMenu menu, string title, string message)
  {
    this.menu = menu;
    this.TxtTitle.SetText(title);
    this.TxtDescription.SetText(message);
  }
}
