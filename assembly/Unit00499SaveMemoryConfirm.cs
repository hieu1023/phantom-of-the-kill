﻿// Decompiled with JetBrains decompiler
// Type: Unit00499SaveMemoryConfirm
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System.Collections;
using UnityEngine;

public class Unit00499SaveMemoryConfirm : Unit00499MemoryBase
{
  public void Initialize(PlayerUnit unit, int index, Unit00499SaveMemorySlotSelect menu)
  {
    this.unit = unit;
    this.index = index;
    this.menu = menu;
    this.before.SetStatusText(unit, false);
    this.StartCoroutine(this.LoadUnit());
    this.txt_Description.SetTextLocalize(Consts.Format(Consts.GetInstance().SAVE_MEMORY_SLOT_DESCRIPTION, (IDictionary) new Hashtable()
    {
      {
        (object) nameof (index),
        (object) (index + 1)
      }
    }));
  }

  public IEnumerator LoadUnit()
  {
    Unit00499SaveMemoryConfirm saveMemoryConfirm = this;
    Future<GameObject> prefabF = Res.Prefabs.UnitIcon.normal.Load<GameObject>();
    IEnumerator e = prefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    UnitIcon component = prefabF.Result.Clone(saveMemoryConfirm.before.linkUnit.transform).GetComponent<UnitIcon>();
    PlayerUnit[] playerUnits = new PlayerUnit[1]
    {
      saveMemoryConfirm.unit
    };
    component.RarityCenter();
    component.buttonBoxCollider.enabled = true;
    component.Button.enabled = true;
    component.SetPressEvent((System.Action) (() => Singleton<PopupManager>.GetInstance().closeAll(false)));
    e = component.SetPlayerUnit(saveMemoryConfirm.unit, playerUnits, (PlayerUnit) null, true, false);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public void IbtnDecision()
  {
    if (this.IsPushAndSet())
      return;
    this.StartCoroutine(this.Save());
    this.menu.isClose = true;
  }
}
