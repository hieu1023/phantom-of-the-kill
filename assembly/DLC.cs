﻿// Decompiled with JetBrains decompiler
// Type: DLC
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using DeviceKit;
using GameCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using UniLinq;
using UnityEngine;

public class DLC
{
  private Dictionary<string, DLC.Data> dataDic = new Dictionary<string, DLC.Data>();
  private DLC.Consumer[] consumers = new DLC.Consumer[0];
  private const int CONSUMER_COUNT = 5;
  private string error;

  public static string ResourceDirectory
  {
    get
    {
      return StorageUtil.persistentDataPath + "/cache/";
    }
  }

  public static string ResourceTempDirectory
  {
    get
    {
      return DLC.ResourceDirectory + "temp/";
    }
  }

  public static string UrlBase
  {
    get
    {
      return ServerSelector.DlcUrl;
    }
  }

  public DLC(ResourceInfo resourceInfo, string[] paths, bool fileCheckDisable = false)
  {
    using (new ScopeTimer("DLC.ctor"))
    {
      foreach (string path in new HashSet<string>((IEnumerable<string>) paths).Shuffle<string>().ToArray<string>())
      {
        ResourceInfo.Resource resource = resourceInfo.SearchResourceInfo(path);
        if (!string.IsNullOrEmpty(resource._key))
        {
          switch (resource._value._path_type)
          {
            case ResourceInfo.PathType.AssetBundle:
              string fileName1 = resource._value._file_name;
              this.dataDic[path] = new DLC.Data()
              {
                Path = path,
                Name = fileName1,
                Exists = !fileCheckDisable && CachedFile.Exists(DLC.ResourceDirectory + fileName1),
                AssetBundle = true,
                FileSize = (int) resource._value._file_size,
                Uncompress = false
              };
              continue;
            case ResourceInfo.PathType.StreamingAssets:
              string fileName2 = resource._value._file_name;
              this.dataDic[path] = new DLC.Data()
              {
                Path = path,
                Name = fileName2,
                Exists = !fileCheckDisable && CachedFile.Exists(DLC.ResourceDirectory + fileName2),
                AssetBundle = false,
                FileSize = (int) resource._value._file_size,
                Uncompress = false
              };
              continue;
            default:
              continue;
          }
        }
      }
    }
  }

  public DLC(List<ResourceInfo.Resource> resources)
  {
    using (new ScopeTimer("DLC.ctor"))
    {
      foreach (ResourceInfo.Resource resource in new HashSet<ResourceInfo.Resource>((IEnumerable<ResourceInfo.Resource>) resources).Shuffle<ResourceInfo.Resource>().ToArray<ResourceInfo.Resource>())
      {
        if (!string.IsNullOrEmpty(resource._key))
        {
          switch (resource._value._path_type)
          {
            case ResourceInfo.PathType.AssetBundle:
              string fileName1 = resource._value._file_name;
              this.dataDic[resource._key] = new DLC.Data()
              {
                Path = resource._key,
                Name = fileName1,
                Exists = false,
                AssetBundle = true,
                FileSize = (int) resource._value._file_size,
                Uncompress = false
              };
              continue;
            case ResourceInfo.PathType.StreamingAssets:
              string fileName2 = resource._value._file_name;
              this.dataDic[resource._key] = new DLC.Data()
              {
                Path = resource._key,
                Name = fileName2,
                Exists = false,
                AssetBundle = false,
                FileSize = (int) resource._value._file_size,
                Uncompress = false
              };
              continue;
            default:
              continue;
          }
        }
      }
    }
  }

  public long GetDownloadedSize(bool includesExistFile = false)
  {
    return (includesExistFile ? this.GetDownloadSize(true) - this.GetDownloadSize(false) : 0L) + ((IEnumerable<DLC.Consumer>) this.consumers).Sum<DLC.Consumer>((Func<DLC.Consumer, long>) (x => (long) x.Downloaded));
  }

  public bool DownloadRequired
  {
    get
    {
      return (ulong) this.GetDownloadSize(false) > 0UL;
    }
  }

  public long GetDownloadSize(bool includesExistFile = false)
  {
    return this.dataDic.Where<KeyValuePair<string, DLC.Data>>((Func<KeyValuePair<string, DLC.Data>, bool>) (x => includesExistFile || !x.Value.Exists)).Sum<KeyValuePair<string, DLC.Data>>((Func<KeyValuePair<string, DLC.Data>, long>) (x => (long) x.Value.FileSize));
  }

  public long GetStoredSize(bool includesExistFile = false)
  {
    return (includesExistFile ? this.GetStoreSize(true) - this.GetStoreSize(false) : 0L) + ((IEnumerable<DLC.Consumer>) this.consumers).Sum<DLC.Consumer>((Func<DLC.Consumer, long>) (x => (long) x.Stored));
  }

  public long GetStoreSize(bool includesExistFile = false)
  {
    return this.dataDic.Where<KeyValuePair<string, DLC.Data>>((Func<KeyValuePair<string, DLC.Data>, bool>) (x => includesExistFile || !x.Value.Exists)).Sum<KeyValuePair<string, DLC.Data>>((Func<KeyValuePair<string, DLC.Data>, long>) (x => (long) x.Value.FileSize));
  }

  public string Error
  {
    get
    {
      return this.error;
    }
  }

  public IEnumerator Start(MonoBehaviour mono)
  {
    this.error = (string) null;
    long storeSize = this.GetStoreSize(false);
    long int64 = Convert.ToInt64(Sys.GetAvailableStorageBytes());
    if (storeSize > int64)
    {
      this.error = Consts.GetInstance().dlcNotEnoughDiskSpace(storeSize);
    }
    else
    {
      if (this.consumers.Length == 0)
      {
        Dictionary<string, DLC.Data> dic = new Dictionary<string, DLC.Data>();
        foreach (KeyValuePair<string, DLC.Data> keyValuePair in this.dataDic)
        {
          if (!keyValuePair.Value.Exists && !keyValuePair.Value.Completed)
            dic.Add(keyValuePair.Key, keyValuePair.Value);
        }
        this.consumers = Enumerable.Range(0, 5).Select<int, DLC.Consumer>((Func<int, DLC.Consumer>) (n => new DLC.Consumer(dic))).ToArray<DLC.Consumer>();
        foreach (DLC.Consumer consumer in this.consumers)
          mono.StartCoroutine(consumer.Run());
      }
      else
      {
        foreach (DLC.Consumer consumer in this.consumers)
        {
          if (!consumer.Completed && !string.IsNullOrEmpty(consumer.Error))
            consumer.Restart();
        }
      }
      while (!((IEnumerable<DLC.Consumer>) this.consumers).All<DLC.Consumer>((Func<DLC.Consumer, bool>) (x => x.Completed)))
      {
        yield return (object) null;
        if (((IEnumerable<DLC.Consumer>) this.consumers).Any<DLC.Consumer>((Func<DLC.Consumer, bool>) (x => x.Error != null)))
        {
          foreach (DLC.Consumer consumer in this.consumers)
            consumer.Interrupt();
          this.error = Consts.GetInstance().dlc_fail_download_text;
          yield break;
        }
      }
      foreach (DLC.Consumer consumer in this.consumers)
        consumer.Dispose();
    }
  }

  private class WWWFile : IDisposable
  {
    private string url;
    private string path;
    private bool succeeded;
    private string error;
    private WWW www;
    private int beforeBytesDownloaded;

    public bool Succeeded
    {
      get
      {
        return this.succeeded;
      }
    }

    public string Error
    {
      get
      {
        return this.error;
      }
    }

    public WWWFile(string url, string path)
    {
      this.path = path;
      this.www = new WWW(url);
      this.beforeBytesDownloaded = 0;
    }

    public IEnumerator Wait(System.Action<int> progressCallback)
    {
      Stopwatch watch = new Stopwatch();
      while (!this.www.isDone)
      {
        int bytesDownloaded = this.www.bytesDownloaded;
        if (this.beforeBytesDownloaded == bytesDownloaded)
        {
          if (watch.ElapsedMilliseconds > 30000L)
          {
            this.error = string.Format("download wait time out. url: {0}, bytesDownloaded: {1}", (object) this.www.url, (object) bytesDownloaded);
            this.www.Dispose();
            yield break;
          }
        }
        else
        {
          watch.Restart();
          progressCallback(bytesDownloaded - this.beforeBytesDownloaded);
          this.beforeBytesDownloaded = bytesDownloaded;
        }
        yield return (object) new WaitForSeconds(0.1f);
      }
      progressCallback(this.www.bytesDownloaded - this.beforeBytesDownloaded);
      yield return (object) null;
      if (this.www.error != null)
      {
        this.error = this.www.error;
      }
      else
      {
        try
        {
          File.WriteAllBytes(this.path, this.www.bytes);
        }
        catch (Exception ex1)
        {
          try
          {
            File.Delete(this.path);
          }
          catch (Exception ex2)
          {
            this.error = string.Format("failed to delete file: {0}, exception: {1}", (object) this.path, (object) ex2);
          }
          this.error = string.Format("failed to write file: {0}, exception: {1}", (object) this.path, (object) ex1);
          yield break;
        }
        this.succeeded = this.error == null;
      }
    }

    public void Dispose()
    {
      if (this.www == null)
        return;
      this.www.Dispose();
    }
  }

  private class Data
  {
    public string Path;
    public string Name;
    public bool Exists;
    public bool AssetBundle;
    public int FileSize;
    public bool Uncompress;
    public bool Completed;
    public bool Downloading;

    public string Url
    {
      get
      {
        return this.AssetBundle ? DLC.UrlBase + "ab/" + this.Name : DLC.UrlBase + "sa/" + this.Name;
      }
    }

    public string StorePath
    {
      get
      {
        return DLC.ResourceDirectory + this.Name;
      }
    }

    public string StoreTempPath
    {
      get
      {
        return DLC.ResourceTempDirectory + this.Name;
      }
    }
  }

  private class Consumer : IDisposable
  {
    private bool interrupted;
    private Dictionary<string, DLC.Data> dataDic;
    private DLC.Data nowDownloadData;
    private int maxDownloadSize;
    private const int DEFAULT_MAXDOWNLOADSIZE = 5242880;
    private string error;
    private int downloaded;
    private int stored;

    public Consumer(Dictionary<string, DLC.Data> dataDic)
    {
      this.dataDic = dataDic;
      this.maxDownloadSize = this.dataDic.Any<KeyValuePair<string, DLC.Data>>() ? this.dataDic.Select<KeyValuePair<string, DLC.Data>, int>((Func<KeyValuePair<string, DLC.Data>, int>) (x => x.Value.FileSize)).Max() : 0;
      if (this.maxDownloadSize < 5242880)
        this.maxDownloadSize = 5242880;
      this.interrupted = false;
      this.nowDownloadData = (DLC.Data) null;
    }

    public void Dispose()
    {
    }

    private void RemoveCompletedData()
    {
      List<string> stringList = new List<string>();
      foreach (KeyValuePair<string, DLC.Data> keyValuePair in this.dataDic)
      {
        if (keyValuePair.Value.Completed)
          stringList.Add(keyValuePair.Key);
      }
      foreach (string key in stringList)
        this.dataDic.Remove(key);
    }

    private DLC.Data GetNext()
    {
      int num = 0;
      foreach (KeyValuePair<string, DLC.Data> keyValuePair in this.dataDic)
      {
        if (!keyValuePair.Value.Completed && keyValuePair.Value.Downloading)
          num += keyValuePair.Value.FileSize;
      }
      if (num >= this.maxDownloadSize)
        return (DLC.Data) null;
      foreach (KeyValuePair<string, DLC.Data> keyValuePair in this.dataDic)
      {
        if (!keyValuePair.Value.Completed && !keyValuePair.Value.Downloading)
          return keyValuePair.Value;
      }
      return (DLC.Data) null;
    }

    public string Error
    {
      get
      {
        return this.error;
      }
    }

    public int Downloaded
    {
      get
      {
        return this.downloaded;
      }
    }

    public int Stored
    {
      get
      {
        return this.stored;
      }
    }

    public bool Completed
    {
      get
      {
        return this.dataDic.Count == 0;
      }
    }

    public void Restart()
    {
      this.error = (string) null;
      this.interrupted = false;
      this.nowDownloadData = (DLC.Data) null;
    }

    public void Interrupt()
    {
      this.interrupted = true;
    }

    private IEnumerator Run_()
    {
      DLC.Consumer consumer = this;
      consumer.RemoveCompletedData();
      if (consumer.dataDic.Count % 100 == 0)
      {
        GC.Collect();
        GC.WaitForPendingFinalizers();
      }
      if (consumer.dataDic.Count != 0)
      {
        DLC.Data data = consumer.GetNext();
        consumer.nowDownloadData = data;
        if (data == null)
        {
          yield return (object) new WaitForSeconds(1f);
        }
        else
        {
          data.Downloading = true;
          consumer.error = (string) null;
          int retry = 0;
          DLC.WWWFile www;
          while (true)
          {
            www = new DLC.WWWFile(data.Url, data.StorePath);
            try
            {
              IEnumerator e = www.Wait(new System.Action<int>(consumer.addDownloadProgress));
              while (e.MoveNext())
                yield return e.Current;
              e = (IEnumerator) null;
              if (www.Error != null)
              {
                if (retry < 5)
                {
                  ++retry;
                  yield return (object) new WaitForSeconds(1f);
                }
                else
                {
                  consumer.error = www.Error;
                  data.Downloading = false;
                  Debug.LogError((object) consumer.error);
                  yield break;
                }
              }
              else
                break;
            }
            finally
            {
              www?.Dispose();
            }
          }
          www = (DLC.WWWFile) null;
          data.Downloading = false;
          data.Completed = true;
          consumer.stored += data.FileSize;
        }
      }
    }

    public IEnumerator Run()
    {
      while (!this.Completed)
      {
        IEnumerator e = this.Run_();
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        while (this.error != null && !this.Completed || this.interrupted && this.nowDownloadData != null && this.nowDownloadData.Completed)
          yield return (object) new WaitForSeconds(1f);
      }
    }

    private void addDownloadProgress(int count)
    {
      this.downloaded += count;
    }
  }
}
