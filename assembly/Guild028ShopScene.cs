﻿// Decompiled with JetBrains decompiler
// Type: Guild028ShopScene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System;
using System.Collections;
using UnityEngine;

public class Guild028ShopScene : NGSceneBase
{
  [SerializeField]
  private Guild028ShopMenu menu;

  public static void ChangeScene(bool isStack)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("guild028_shop", isStack, (object[]) Array.Empty<object>());
  }

  public IEnumerator onStartSceneAsync()
  {
    Future<WebAPI.Response.GuildtownGuildMedalShop> shopT = WebAPI.GuildtownGuildMedalShop((System.Action<WebAPI.Response.UserError>) (e =>
    {
      WebAPI.DefaultUserErrorCallback(e);
      Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
    }));
    IEnumerator e1 = shopT.Wait();
    while (e1.MoveNext())
      yield return e1.Current;
    e1 = (IEnumerator) null;
    if (shopT.Result != null)
    {
      e1 = this.menu.Init(Res.Prefabs.shop007_4_1.vscroll7_4_1.Load<GameObject>());
      while (e1.MoveNext())
        yield return e1.Current;
      e1 = (IEnumerator) null;
    }
  }

  public void onStartScene()
  {
    Singleton<CommonRoot>.GetInstance().isLoading = false;
  }
}
