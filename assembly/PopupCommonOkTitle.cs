﻿// Decompiled with JetBrains decompiler
// Type: PopupCommonOkTitle
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class PopupCommonOkTitle : MonoBehaviour
{
  private static string prefab_path = "Prefabs/popup_common_ok_title";
  [SerializeField]
  private UILabel title;
  [SerializeField]
  private UILabel message;
  private System.Action yes;
  private System.Action no;

  public void OnYes()
  {
    this.yes();
    Singleton<PopupManager>.GetInstance().dismiss(false);
  }

  public void OnNo()
  {
    this.no();
    Singleton<PopupManager>.GetInstance().dismiss(false);
  }

  public static void Show(string title, string message, System.Action yes = null, System.Action no = null)
  {
    PopupCommonOkTitle component = Singleton<PopupManager>.GetInstance().open(Resources.Load<GameObject>(PopupCommonOkTitle.prefab_path), false, false, false, true, false, false, "SE_1006").GetComponent<PopupCommonOkTitle>();
    component.title.SetText(title);
    component.message.SetText(message);
    component.yes = yes ?? new System.Action(PopupCommonOkTitle.defaultCallback);
    component.no = no ?? new System.Action(PopupCommonOkTitle.defaultCallback);
  }

  public static void defaultCallback()
  {
    Debug.Log((object) "popup close");
  }
}
