﻿// Decompiled with JetBrains decompiler
// Type: Guide0111Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

public class Guide0111Menu : BackButtonMenuBase
{
  private Guide0111Scene.UpdateInfo updaterUnit = new Guide0111Scene.UpdateInfo();
  private Guide0111Scene.UpdateInfo updaterEnemy = new Guide0111Scene.UpdateInfo();
  private Guide0111Scene.UpdateInfo updaterBugu = new Guide0111Scene.UpdateInfo();

  public virtual void IbtnBack()
  {
    if (this.IsPushAndSet())
      return;
    this.backScene();
  }

  public virtual void IbtnBugubook_AnimList3()
  {
    if (this.IsPushAndSet())
      return;
    Guide0114Scene.changeScene(true, this.updaterBugu);
  }

  public virtual void IbtnEnemybook_AnimList2()
  {
    if (this.IsPushAndSet())
      return;
    Guide0113Scene.changeScene(true, this.updaterEnemy);
  }

  public virtual void IbtnUnitbook_AnimList1()
  {
    if (this.IsPushAndSet())
      return;
    Guide0112Scene.changeScene(true, this.updaterUnit);
  }

  public override void onBackButton()
  {
    this.IbtnBack();
  }
}
