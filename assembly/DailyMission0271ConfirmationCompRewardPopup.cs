﻿// Decompiled with JetBrains decompiler
// Type: DailyMission0271ConfirmationCompRewardPopup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using System.Collections;
using UnityEngine;

public class DailyMission0271ConfirmationCompRewardPopup : BackButtonMonoBehaiviour
{
  [SerializeField]
  private CreateIconObject RewardThumb;
  [SerializeField]
  private UILabel rewardName;

  public IEnumerator Init(BingoRewardGroup completeReward)
  {
    this.rewardName.SetTextLocalize(CommonRewardType.GetRewardName(completeReward.reward_type_id, completeReward.reward_id, completeReward.reward_quantity, false));
    IEnumerator e = this.RewardThumb.CreateThumbnail(completeReward.reward_type_id, completeReward.reward_id, completeReward.reward_quantity, true, true, new CommonQuestType?(), false);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public void IbtnOK()
  {
    Singleton<PopupManager>.GetInstance().onDismiss();
  }

  public override void onBackButton()
  {
    this.IbtnOK();
  }
}
