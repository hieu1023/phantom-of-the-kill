﻿// Decompiled with JetBrains decompiler
// Type: AnimationEventAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System;
using System.Collections.Generic;
using UnityEngine;

public class AnimationEventAction : MonoBehaviour
{
  public string func_name_ = "Action";
  public List<CallFuncData> func_data_list = new List<CallFuncData>();
  public GameObject func_obj_;

  private void Action()
  {
    if (this.ErrorCheck())
      return;
    this.func_obj_.SendMessage(this.func_name_);
  }

  private void ListAction(int number)
  {
    int? nullable = this.func_data_list.FirstIndexOrNull<CallFuncData>((Func<CallFuncData, bool>) (x => x.ID == number));
    if (!nullable.HasValue)
      return;
    CallFuncData funcData = this.func_data_list[nullable.Value];
    if (this.ErrorCheck(funcData))
      return;
    funcData.FuncObj.SendMessage(funcData.FuncName, (object) funcData.WaitTime);
  }

  private void ListActionString(string func_name)
  {
    int? nullable = this.func_data_list.FirstIndexOrNull<CallFuncData>((Func<CallFuncData, bool>) (x => x.FuncName == func_name));
    if (!nullable.HasValue)
      return;
    CallFuncData funcData = this.func_data_list[nullable.Value];
    if (this.ErrorCheck(funcData))
      return;
    funcData.FuncObj.SendMessage(funcData.FuncName, (object) funcData.WaitTime);
  }

  private void Action2(string func_name)
  {
    if (this.ErrorCheck())
      return;
    this.func_obj_.SendMessage(func_name);
  }

  private bool ErrorCheck()
  {
    return this.func_name_ == "" || (UnityEngine.Object) this.func_obj_ == (UnityEngine.Object) null;
  }

  private bool ErrorCheck(CallFuncData cfd)
  {
    return cfd.FuncName == "" || (UnityEngine.Object) cfd.FuncObj == (UnityEngine.Object) null;
  }

  public void PartuclePlay()
  {
    this.GetComponent<ParticleSystem>().Play();
  }
}
