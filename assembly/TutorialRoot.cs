﻿// Decompiled with JetBrains decompiler
// Type: TutorialRoot
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UniLinq;
using UnityEngine;

public class TutorialRoot : Singleton<TutorialRoot>
{
  [NonSerialized]
  public const int BeginnerBannerID = 887;
  [NonSerialized]
  public const int TutorialTicketID = 675;
  [SerializeField]
  private Transform prefabRoot;
  [SerializeField]
  private TutorialProgress progress;
  [SerializeField]
  private TutorialAdvice advice;
  [SerializeField]
  private UIPanel mainPanel;
  [SerializeField]
  private UICamera uiCamera;
  [SerializeField]
  private UIRoot uiRoot;
  [SerializeField]
  private GameObject tutorialPagesGameObject;
  private bool isInitilizing;
  private bool isNowFinish;
  private const string tutorial_finish_scene = "gacha";
  private int[] tutorial_gacha_unit_ids;
  private int[] tutorial_gacha_unit_types;
  private int[] tutorial_gacha_deck_ids;
  private WebAPI.Response.TutorialTutorialRagnarokResume resume;

  public bool IsInitilizing
  {
    get
    {
      return this.isInitilizing;
    }
  }

  public int[] Tutorial_gacha_unit_ids
  {
    set
    {
      this.tutorial_gacha_unit_ids = value;
    }
    get
    {
      return this.tutorial_gacha_unit_ids;
    }
  }

  public int[] Tutorial_gacha_unit_types
  {
    set
    {
      this.tutorial_gacha_unit_types = value;
    }
    get
    {
      return this.tutorial_gacha_unit_types;
    }
  }

  public int[] Tutorial_gacha_deck_ids
  {
    set
    {
      this.tutorial_gacha_deck_ids = value;
    }
    get
    {
      return this.tutorial_gacha_deck_ids;
    }
  }

  public List<GachaTutorialDeckEntity> TutorialGachaDeckEntities { get; set; }

  public WebAPI.Response.TutorialTutorialRagnarokResume Resume
  {
    get
    {
      return this.resume;
    }
  }

  private GameObject wrap
  {
    get
    {
      return this.mainPanel.gameObject;
    }
  }

  public bool IsAdviced
  {
    get
    {
      return this.advice.IsShow;
    }
  }

  protected override void Initialize()
  {
    this.isNowFinish = false;
    this.isInitilizing = true;
    UnityEngine.Object.Destroy((UnityEngine.Object) this.uiCamera.gameObject);
    UIRoot component = Singleton<CommonRoot>.GetInstance().GetComponent<UIRoot>();
    this.uiRoot.manualHeight = component.manualHeight;
    this.uiRoot.minimumHeight = component.minimumHeight;
    this.StartCoroutine(this.initAsync());
  }

  private IEnumerator initAsync()
  {
    TutorialRoot tutorialRoot = this;
    tutorialRoot.mainPanel.alpha = 0.0f;
    yield return (object) Singleton<ResourceManager>.GetInstance().LoadResource(tutorialRoot.gameObject);
    tutorialRoot.mainPanel.alpha = 1f;
    tutorialRoot.advice.Init();
    EventTracker.BeaconTutorial("Start", 0);
    TutorialPageBase[] array = ((IEnumerable<TutorialPageBase>) tutorialRoot.tutorialPagesGameObject.GetComponentsInChildren<TutorialPageBase>(true)).OrderBy<TutorialPageBase, string>((Func<TutorialPageBase, string>) (x => x.name)).ToArray<TutorialPageBase>();
    tutorialRoot.progress = new TutorialProgress(array, tutorialRoot.advice, tutorialRoot.wrap);
    // ISSUE: reference to a compiler-generated method
    tutorialRoot.progress.OnNextPageCallback = new System.Action(tutorialRoot.\u003CinitAsync\u003Eb__38_1);
    tutorialRoot.progress.OnFinishCallback = new System.Action<bool>(tutorialRoot.finish);
    tutorialRoot.wrap.SetActive(false);
    tutorialRoot.isInitilizing = false;
  }

  public void DebugSetResume(
    WebAPI.Response.TutorialTutorialRagnarokResume resume)
  {
    this.resume = resume;
  }

  public void StartTutorial(
    WebAPI.Response.TutorialTutorialRagnarokResume resume)
  {
    Persist.battleEnvironment.Data = (BE) null;
    Persist.battleEnvironment.Delete();
    this.resume = resume;
    if (!Persist.tutorial.Exists)
      Persist.tutorial.Flush();
    this.progress.CurrentPageIndex = Persist.tutorial.Data.CurrentPage;
    if (this.progress.IsFinish())
    {
      Debug.LogError((object) "call Renbder() but tutorial is finished. so restart tutorial");
      this.progress.CurrentPageIndex = 0;
      Persist.tutorial.Data.SetPageIndex(0);
    }
    this.StartCoroutine(this.startTutorial());
  }

  private IEnumerator startTutorial()
  {
    this.wrap.SetActive(false);
    NGSceneManager sceneManager = Singleton<NGSceneManager>.GetInstance();
    sceneManager.clearStack();
    sceneManager.changeScene("empty", true, (object[]) Array.Empty<object>());
    Singleton<CommonRoot>.GetInstance().isLoading = false;
    while (!sceneManager.isSceneInitialized)
      yield return (object) null;
    while (this.isInitilizing)
      yield return (object) null;
    IEnumerator e = this.progress.Render();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.wrap.SetActive(true);
    Debug.Log((object) "tutorial is initialized");
  }

  private void finish(bool isFinishedManually)
  {
    if (this.isNowFinish)
      return;
    this.isNowFinish = true;
    Persist.battleEnvironment.Data = (BE) null;
    Persist.battleEnvironment.Delete();
    Singleton<CommonRoot>.GetInstance().isTouchBlock = false;
    Singleton<CommonRoot>.GetInstance().ShowLoadingLayer(4, false);
    this.wrap.SetActive(false);
    this.StartCoroutine(this.signUpLoop(isFinishedManually, (System.Action) (() =>
    {
      this.endTutorial(true, true);
      Debug.Log((object) "tutorial end, go to next page");
      Singleton<NGGameDataManager>.GetInstance().refreshHomeHome = true;
      Singleton<NGGameDataManager>.GetInstance().StartSceneAsyncProxy((System.Action<NGGameDataManager.StartSceneProxyResult>) (_ =>
      {
        Singleton<NGGameDataManager>.GetInstance().lastReferenceUnitID = -1;
        Singleton<NGGameDataManager>.GetInstance().lastReferenceUnitIndex = -1;
        Singleton<NGSceneManager>.GetInstance().destroyLoadedScenes();
        Singleton<NGSceneManager>.GetInstance().clearStack();
        Singleton<NGGameDataManager>.GetInstance().IsSea = false;
        MypageScene.ChangeScene(false, false, false);
      }));
      this.isNowFinish = false;
    })));
    EventTracker.SendEvent(EventTracker.EventType.FINISH_TUTORIAL);
  }

  public void TutorialGachaAdvice()
  {
    if (this.DecryptGachaData())
    {
      if (Persist.newTutorial.Data.tutoialGacha)
        Singleton<TutorialRoot>.GetInstance().CurrentAdvise();
      else
        this.StartCoroutine(PopupCommon.Show(Consts.GetInstance().GACHA_NOT_END_TITLE, Consts.GetInstance().GACHA_NOT_END_DESCRIPTION, (System.Action) (() =>
        {
          List<PlayerUnit> player_units = new List<PlayerUnit>();
          for (int index = 0; index < this.tutorial_gacha_unit_ids.Length; ++index)
            player_units.Add(PlayerUnit.FromUnit(MasterData.UnitUnit[this.tutorial_gacha_unit_ids[index]], this.tutorial_gacha_unit_types[index], this.tutorial_gacha_unit_ids[index]));
          GachaResultData.GetInstance().SetTutorialData(player_units, this.TutorialGachaDeckEntities);
          Gacha00613Scene.ChangeScene(false, true);
        })));
    }
    else
    {
      Singleton<NGSceneManager>.GetInstance().changeScene("gacha006_3", false, (object[]) Array.Empty<object>());
      Singleton<NGSceneManager>.GetInstance().waitSceneAction((System.Action) (() =>
      {
        Singleton<NGSceneManager>.GetInstance().clearStack();
        Singleton<TutorialRoot>.GetInstance().ForceShowAdviceInNextButton("newchapter_gacha1_tutorial", new Dictionary<string, Func<Transform, UIButton>>()
        {
          {
            "chapter_gacha_ticket",
            (Func<Transform, UIButton>) (root =>
            {
              Transform childInFind = root.GetChildInFind("Middle");
              UIButton componentInChildren = childInFind.GetComponentInChildren<UIButton>();
              GameObject gameObject = GameObject.Find("Gacha0063Gacha0063Scene UI Root").GetComponent<Gacha0063Scene>().GetTutorialGachaTop().Clone(childInFind.GetChildInFind("dir_gacha_ticket_position"));
              gameObject.SetActive(false);
              gameObject.SetActive(true);
              return componentInChildren;
            })
          }
        }, (System.Action) (() => this.StartCoroutine(Singleton<TutorialRoot>.GetInstance().TutorialGacha(true))));
      }));
    }
  }

  public IEnumerator TutorialGacha(bool isStack)
  {
    Singleton<CommonRoot>.GetInstance().ShowLoadingLayer(0, true);
    yield return (object) new WaitForSeconds(0.1f);
    SMManager.Get<GachaModule[]>();
    IEnumerator e = GachaPlay.GetInstance().ChargeGachaTutorial();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Singleton<NGSceneManager>.GetInstance().changeScene("gacha006_effect", isStack, (object[]) Array.Empty<object>());
    Singleton<CommonRoot>.GetInstance().SetFooterEnable(true);
  }

  public void EndTutorial()
  {
    Debug.LogWarning((object) "call EndTutorial() without tutorial");
    this.progress.CurrentPageIndex = Persist.tutorial.Data.LastPageIndex;
    Persist.EndTutorial();
  }

  private void endTutorial(bool logFiveRocks = true, bool logfacebook = true)
  {
    this.progress.CurrentPageIndex = Persist.tutorial.Data.LastPageIndex;
    Persist.tutorial.Data.SetTutorialFinish();
    Persist.tutorial.Flush();
    Persist.newTutorial.Data.SetTutorialFinish();
    Persist.newTutorial.Flush();
    Persist.tutorialGacha.Data.clearGachaResult();
    Persist.tutorialGacha.Flush();
    if (logFiveRocks)
      EventTracker.BeaconTutorial("Finish", Persist.tutorial.Data.DuringSeconds());
    if (!logfacebook)
      return;
    FaceBookWrapper.TutorialComplete();
  }

  private IEnumerator signUpLoop(bool isFinishedManually, System.Action callback)
  {
    Persist.Tutorial conf = Persist.tutorial.Data;
    int gachaUnitId = conf.GachaUnitId;
    int[] numArray = this.GachaEffectUnitIds();
    if (!((IEnumerable<int>) numArray).Contains<int>(gachaUnitId))
      gachaUnitId = numArray[0];
    string playerName = string.IsNullOrEmpty(conf.PlayerName) ? Consts.GetInstance().DEFAULT_PLAYER_NAME : conf.PlayerName;
    IEnumerator e = this.CheckTutorialGachaData(isFinishedManually);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Future<WebAPI.Response.PlayerSignup> future = WebAPI.PlayerSignup(conf.MiniGameScore, playerName, this.GetTutorialGachaID(), this.tutorial_gacha_deck_ids, this.tutorial_gacha_unit_ids, this.tutorial_gacha_unit_types, gachaUnitId, (System.Action<WebAPI.Response.UserError>) null);
    e = future.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if (future.HasResult && future.Result != null)
    {
      Persist.tutorial.Data.signupCalled = true;
      callback();
    }
    else
    {
      Consts instance = Consts.GetInstance();
      PopupCommonOkTitle.Show(instance.tutorial_fail_signup_title, instance.tutorial_fail_signup_text, (System.Action) (() => this.StartCoroutine(this.signUpLoop(isFinishedManually, callback))), (System.Action) (() =>
      {
        Persist.tutorial.Data.PlayerName = "";
        Persist.tutorial.Data.CurrentPage = 0;
        Persist.tutorial.Flush();
        StartScript.Restart();
      }));
    }
  }

  public bool IsTutorialFinish()
  {
    return Persist.tutorial.Data.IsFinishTutorial();
  }

  public void OnTenseiFinish()
  {
    if (this.IsTutorialFinish())
      return;
    TutorialTenseiEffectPage tenseiEffectPage = this.progress.TenseiEffectPage();
    if ((UnityEngine.Object) tenseiEffectPage == (UnityEngine.Object) null)
      Debug.LogError((object) ("call OnBattleStateChange but not CurrentPageIndex=" + (object) this.progress.CurrentPageIndex));
    else
      tenseiEffectPage.OnTenseiFinish();
  }

  public void OnChangeSceneFinish(string sceneName)
  {
    string tipsMessage = this.getTipsMessage(sceneName, 0);
    if (string.IsNullOrEmpty(tipsMessage))
      return;
    this.showAdvice(tipsMessage, sceneName, (Dictionary<string, Func<Transform, UIButton>>) null, (System.Action) null);
  }

  public void OnBattleStateChange(BL env)
  {
    if (this.IsTutorialFinish())
    {
      this.FirstAnnihilated(env);
    }
    else
    {
      if (env.phaseState.state == BL.Phase.finalize)
      {
        Singleton<NGBattleManager>.GetInstance().isBattleEnable = false;
        Singleton<NGBattleManager>.GetInstance().popupOpen((GameObject) null, false, (EventDelegate) null, false, false, false, true, false);
      }
      this.StartCoroutine(this.onBattleStateChange(env.phaseState.state, env.phaseState.turnCount));
    }
  }

  public bool FirstAnnihilated(BL env)
  {
    if (env.phaseState.state == BL.Phase.finalize && !env.battleInfo.pvp && (!env.battleInfo.gvg && !env.battleInfo.isEarthMode) && (env.battleInfo.quest_type != CommonQuestType.GuildRaid && env.battleInfo.isFirstAllDead && env.allDeadUnitsp(BL.ForceID.player)))
    {
      Singleton<NGBattleManager>.GetInstance().isBattleEnable = false;
      if (!this.ShowAdvice("firstgameover", 0, (System.Action) (() => Singleton<NGBattleManager>.GetInstance().isBattleEnable = true)))
      {
        Singleton<NGBattleManager>.GetInstance().isBattleEnable = true;
        Debug.LogError((object) "ERROR FirstAnnihilated ShowAdvice");
      }
    }
    return false;
  }

  public void ReleaseResources()
  {
    this.progress.ReleaseResources();
  }

  public void CurrentAdvise()
  {
    TutorialPageBase tutorialPageBase = this.progress.currentOrNull();
    if (!((UnityEngine.Object) tutorialPageBase != (UnityEngine.Object) null))
      return;
    tutorialPageBase.Advise();
  }

  public IEnumerator onBattleStateChange(BL.Phase state, int turn)
  {
    while (!Singleton<NGSceneManager>.GetInstance().isSceneInitialized)
      yield return (object) null;
    TutorialBattlePage battle = this.progress.BattlePage();
    if ((UnityEngine.Object) battle == (UnityEngine.Object) null)
    {
      Debug.LogError((object) ("call OnBattleStateChange but not CurrentPageIndex=" + (object) this.progress.CurrentPageIndex));
    }
    else
    {
      switch (state)
      {
        case BL.Phase.player_start:
          yield return (object) new WaitForSeconds(1.5f);
          battle.OnPlayerTurnStart(turn);
          break;
        case BL.Phase.finalize:
          battle.OnBattleFinish();
          Time.timeScale = 1f;
          while (!Singleton<NGSceneManager>.GetInstance().isSceneInitialized)
            yield return (object) null;
          Singleton<NGBattleManager>.GetInstance().popupCloseAll(false);
          break;
      }
    }
  }

  public int[] GachaEffectUnitIds()
  {
    return new int[6]
    {
      100113,
      100223,
      200413,
      300113,
      400133,
      500213
    };
  }

  private string getTipsMessage(string tipsName, int id = 0)
  {
    string str;
    return Consts.GetInstance().tutorial.TryGetValue(tipsName, out str) && !Persist.tutorial.Data.Hints.ContainsKey(tipsName) ? str : string.Empty;
  }

  private string getTipsMessageForce(string tipsName, int id = 0)
  {
    string str;
    return Consts.GetInstance().tutorial.TryGetValue(tipsName, out str) ? str : string.Empty;
  }

  public bool isReadHint(string sceneName, int id)
  {
    return string.IsNullOrEmpty(this.getTipsMessage(sceneName, id));
  }

  private void doneReadHint(string sceneName)
  {
    if (!Consts.GetInstance().tutorial.ContainsKey(sceneName))
      return;
    Persist.tutorial.Data.Hints[sceneName] = true;
    Persist.tutorial.Flush();
  }

  public bool ShowAdvice(string sceneName = null, int id = 0, System.Action finishCallback = null)
  {
    string tipsMessage = this.getTipsMessage(sceneName, id);
    if (string.IsNullOrEmpty(tipsMessage))
      return false;
    this.showAdvice(tipsMessage, sceneName, (Dictionary<string, Func<Transform, UIButton>>) null, finishCallback);
    return true;
  }

  public bool ForceShowAdvice(string sceneName = null, System.Action finishCallback = null)
  {
    return this.ForceShowAdviceInNextButton(sceneName, (Dictionary<string, Func<Transform, UIButton>>) null, finishCallback);
  }

  public bool ForceShowAdviceInNextButton(
    string sceneName = null,
    Dictionary<string, Func<Transform, UIButton>> next_button_info = null,
    System.Action finishCallback = null)
  {
    string tipsMessageForce = this.getTipsMessageForce(sceneName, 0);
    if (string.IsNullOrEmpty(tipsMessageForce))
      return false;
    this.showAdvice(tipsMessageForce, sceneName, next_button_info, finishCallback);
    return true;
  }

  private void showAdvice(
    string message,
    string sceneName = null,
    Dictionary<string, Func<Transform, UIButton>> next_button_info = null,
    System.Action finishCallback = null)
  {
    this.wrap.SetActive(true);
    this.advice.SetMessage(message, next_button_info);
    this.advice.FinishCallback = (System.Action) (() =>
    {
      if (sceneName != null)
      {
        this.doneReadHint(sceneName);
        Consts instance = Consts.GetInstance();
        if (sceneName.Equals("gacha"))
          PopupCommonYesNo.Show(instance.tutorial_finish_bulk_download_title, instance.tutorial_finish_bulk_download_text, (System.Action) (() => this.StartCoroutine(this.bulkDownLoadCheck())), (System.Action) (() => {}));
      }
      this.wrap.SetActive(false);
      if (finishCallback == null)
        return;
      finishCallback();
    });
  }

  private void doneEventQuestExplanation(int sceneID)
  {
    Persist.explanation.Data.Explanation[sceneID] = true;
    Persist.explanation.Flush();
  }

  public void showEventQuestExplanation(string message, int sceneID = -1)
  {
    this.wrap.SetActive(true);
    this.advice.SetMessage(message, (Dictionary<string, Func<Transform, UIButton>>) null);
    this.advice.FinishCallback = (System.Action) (() =>
    {
      if (sceneID != -1)
        this.doneEventQuestExplanation(sceneID);
      this.wrap.SetActive(false);
    });
  }

  private IEnumerator CheckTutorialGachaData(bool isFinishedManually)
  {
    TutorialRoot tutorialRoot1 = this;
    if (tutorialRoot1.tutorial_gacha_deck_ids == null || tutorialRoot1.tutorial_gacha_unit_ids == null || tutorialRoot1.tutorial_gacha_unit_types == null)
    {
      IEnumerator e = tutorialRoot1.SetTutorialGachaData(isFinishedManually);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    TutorialRoot tutorialRoot = tutorialRoot1;
    for (int i = 0; i < tutorialRoot1.tutorial_gacha_unit_types.Length; i++)
    {
      UnitUnit unit;
      if (!MasterData.UnitUnit.TryGetValue(tutorialRoot1.tutorial_gacha_unit_ids[i], out unit))
        tutorialRoot1.tutorial_gacha_unit_types[i] = 1;
      UnitTypeParameter unitTypeParameter = ((IEnumerable<UnitTypeParameter>) MasterData.UnitTypeParameterList).Where<UnitTypeParameter>((Func<UnitTypeParameter, bool>) (x => x.rarity_UnitRarity == unit.rarity_UnitRarity && x.unit_type_UnitType == closure_0.tutorial_gacha_unit_types[i])).FirstOrDefault<UnitTypeParameter>();
      tutorialRoot1.tutorial_gacha_unit_types[i] = unitTypeParameter == null ? 1 : unitTypeParameter.ID;
    }
  }

  public IEnumerator SetTutorialGachaData(bool isFinishedManually = false)
  {
    DateTime nowTime = ServerTime.NowAppTimeAddDelta();
    GachaTutorialPeriod gachaPeriod = ((IEnumerable<GachaTutorialPeriod>) MasterData.GachaTutorialPeriodList).Where<GachaTutorialPeriod>((Func<GachaTutorialPeriod, bool>) (x =>
    {
      if (x.start_at.HasValue)
      {
        DateTime dateTime1 = nowTime;
        DateTime? nullable = x.start_at;
        if ((nullable.HasValue ? (dateTime1 >= nullable.GetValueOrDefault() ? 1 : 0) : 0) != 0 && x.end_at.HasValue)
        {
          DateTime dateTime2 = nowTime;
          nullable = x.end_at;
          return nullable.HasValue && dateTime2 < nullable.GetValueOrDefault();
        }
      }
      return false;
    })).FirstOrDefault<GachaTutorialPeriod>();
    if (gachaPeriod != null)
    {
      GachaTutorial gacha = ((IEnumerable<GachaTutorial>) MasterData.GachaTutorialList).Where<GachaTutorial>((Func<GachaTutorial, bool>) (x =>
      {
        int? gachaTutorialPeriod = x._period_id_GachaTutorialPeriod;
        int id = gachaPeriod.ID;
        return gachaTutorialPeriod.GetValueOrDefault() == id & gachaTutorialPeriod.HasValue;
      })).FirstOrDefault<GachaTutorial>();
      if (gacha != null)
      {
        List<PlayerUnit> player_units = new List<PlayerUnit>();
        List<int> intList1 = new List<int>();
        List<int> intList2 = new List<int>();
        List<int> intList3 = new List<int>();
        List<GachaTutorialDeckEntity> tutorialDeckEntityList = new List<GachaTutorialDeckEntity>();
        int rollCount = gacha._roll_count;
        int default_deck_id = gacha._deck_id;
        GachaTutorialDeckEntity[] array1 = ((IEnumerable<GachaTutorialDeckEntity>) MasterData.GachaTutorialDeckEntityList).Where<GachaTutorialDeckEntity>((Func<GachaTutorialDeckEntity, bool>) (x => x.reward_type_id == MasterDataTable.CommonRewardType.deck && x.deck_id_GachaTutorialDeck == default_deck_id)).ToArray<GachaTutorialDeckEntity>();
        int num1 = ((IEnumerable<GachaTutorialDeckEntity>) array1).Sum<GachaTutorialDeckEntity>((Func<GachaTutorialDeckEntity, int>) (x => x._appearance));
        GachaTutorialFixedEntity tutorialFixedEntity = ((IEnumerable<GachaTutorialFixedEntity>) MasterData.GachaTutorialFixedEntityList).Where<GachaTutorialFixedEntity>((Func<GachaTutorialFixedEntity, bool>) (x => x._gacha_id == gacha.ID)).FirstOrDefault<GachaTutorialFixedEntity>();
        if (tutorialFixedEntity != null)
        {
          int fixCount = tutorialFixedEntity._fix_count;
          int fixed_deck_id = tutorialFixedEntity._deck_id;
          GachaTutorialDeckEntity[] array2 = ((IEnumerable<GachaTutorialDeckEntity>) MasterData.GachaTutorialDeckEntityList).Where<GachaTutorialDeckEntity>((Func<GachaTutorialDeckEntity, bool>) (x => x.reward_type_id == MasterDataTable.CommonRewardType.deck && x.deck_id_GachaTutorialDeck == fixed_deck_id)).ToArray<GachaTutorialDeckEntity>();
          int num2 = ((IEnumerable<GachaTutorialDeckEntity>) array2).Sum<GachaTutorialDeckEntity>((Func<GachaTutorialDeckEntity, int>) (x => x._appearance));
          GachaTutorialDeckEntity[] array3 = ((IEnumerable<GachaTutorialDeckEntity>) MasterData.GachaTutorialDeckEntityList).Where<GachaTutorialDeckEntity>((Func<GachaTutorialDeckEntity, bool>) (x => x.reward_type_id == MasterDataTable.CommonRewardType.unit)).ToArray<GachaTutorialDeckEntity>();
          UnitTypeDeck[] array4 = ((IEnumerable<UnitTypeDeck>) MasterData.UnitTypeDeckList).Where<UnitTypeDeck>((Func<UnitTypeDeck, bool>) (x =>
          {
            if (x.start_at.HasValue)
            {
              if (x.start_at.HasValue)
              {
                DateTime dateTime = nowTime;
                DateTime? startAt = x.start_at;
                if ((startAt.HasValue ? (dateTime >= startAt.GetValueOrDefault() ? 1 : 0) : 0) != 0)
                  goto label_3;
              }
              return false;
            }
label_3:
            if (!x.end_at.HasValue)
              return true;
            if (!x.end_at.HasValue)
              return false;
            DateTime dateTime1 = nowTime;
            DateTime? endAt = x.end_at;
            return endAt.HasValue && dateTime1 < endAt.GetValueOrDefault();
          })).ToArray<UnitTypeDeck>();
          for (int index = 0; index < fixCount; ++index)
          {
            intList3.Add(fixed_deck_id);
            int num3 = UnityEngine.Random.Range(1, num2 + 1);
            int num4 = 0;
            foreach (GachaTutorialDeckEntity tutorialDeckEntity1 in array2)
            {
              GachaTutorialDeckEntity deck = tutorialDeckEntity1;
              if (deck._appearance + num4 >= num3)
              {
                GachaTutorialDeckEntity[] array5 = ((IEnumerable<GachaTutorialDeckEntity>) array3).Where<GachaTutorialDeckEntity>((Func<GachaTutorialDeckEntity, bool>) (x => x.deck_id_GachaTutorialDeck == deck.reward_id)).ToArray<GachaTutorialDeckEntity>();
                int num5 = UnityEngine.Random.Range(1, ((IEnumerable<GachaTutorialDeckEntity>) array5).Sum<GachaTutorialDeckEntity>((Func<GachaTutorialDeckEntity, int>) (x => x._appearance)) + 1);
                int num6 = 0;
                foreach (GachaTutorialDeckEntity tutorialDeckEntity2 in array5)
                {
                  if (tutorialDeckEntity2._appearance + num6 >= num5)
                  {
                    intList1.Add(tutorialDeckEntity2.reward_id);
                    intList2.Add(this.GetUnitType(tutorialDeckEntity2.reward_id, array4));
                    tutorialDeckEntityList.Add(tutorialDeckEntity2);
                    break;
                  }
                  num6 += tutorialDeckEntity2._appearance;
                }
                break;
              }
              num4 += deck._appearance;
            }
          }
          for (int index = 0; index < rollCount - fixCount; ++index)
          {
            intList3.Add(default_deck_id);
            int num3 = UnityEngine.Random.Range(1, num1 + 1);
            int num4 = 0;
            foreach (GachaTutorialDeckEntity tutorialDeckEntity1 in array1)
            {
              GachaTutorialDeckEntity deck = tutorialDeckEntity1;
              if (deck._appearance + num4 >= num3)
              {
                GachaTutorialDeckEntity[] array5 = ((IEnumerable<GachaTutorialDeckEntity>) array3).Where<GachaTutorialDeckEntity>((Func<GachaTutorialDeckEntity, bool>) (x => x.deck_id_GachaTutorialDeck == deck.reward_id)).ToArray<GachaTutorialDeckEntity>();
                int num5 = UnityEngine.Random.Range(1, ((IEnumerable<GachaTutorialDeckEntity>) array5).Sum<GachaTutorialDeckEntity>((Func<GachaTutorialDeckEntity, int>) (x => x._appearance)) + 1);
                int num6 = 0;
                foreach (GachaTutorialDeckEntity tutorialDeckEntity2 in array5)
                {
                  if (tutorialDeckEntity2._appearance + num6 >= num5)
                  {
                    intList1.Add(tutorialDeckEntity2.reward_id);
                    intList2.Add(this.GetUnitType(tutorialDeckEntity2.reward_id, array4));
                    tutorialDeckEntityList.Add(tutorialDeckEntity2);
                    break;
                  }
                  num6 += tutorialDeckEntity2._appearance;
                }
                break;
              }
              num4 += deck._appearance;
            }
          }
          for (int ID = 0; ID < intList1.Count; ++ID)
            player_units.Add(PlayerUnit.FromUnit(MasterData.UnitUnit[intList1[ID]], intList2[ID], ID));
          this.Tutorial_gacha_unit_ids = intList1.ToArray();
          this.Tutorial_gacha_unit_types = intList2.ToArray();
          this.Tutorial_gacha_deck_ids = intList3.ToArray();
          this.TutorialGachaDeckEntities = tutorialDeckEntityList;
          this.EncryptAndSaveGachaData();
          IEnumerator e = OnDemandDownload.WaitLoadUnitResource((IEnumerable<PlayerUnit>) player_units, !isFinishedManually, (IEnumerable<string>) null, false);
          while (e.MoveNext())
            yield return e.Current;
          e = (IEnumerator) null;
          GachaResultData.GetInstance().SetTutorialData(player_units, this.TutorialGachaDeckEntities);
        }
      }
    }
  }

  private void EncryptAndSaveGachaData()
  {
    if (!Persist.tutorialGacha.Exists)
    {
      Persist.tutorialGacha.Data.setDefault();
      Persist.tutorialGacha.Flush();
    }
    string empty1 = string.Empty;
    for (int index = 0; index < this.tutorial_gacha_unit_ids.Length; ++index)
    {
      empty1 += (string) (object) this.tutorial_gacha_unit_ids[index];
      if (index < this.tutorial_gacha_unit_ids.Length - 1)
        empty1 += ",";
    }
    Persist.tutorialGacha.Data.strUnitIDs = Convert.ToBase64String(Crypt.Encrypt(Encoding.UTF8.GetBytes(empty1)));
    string empty2 = string.Empty;
    for (int index = 0; index < this.Tutorial_gacha_unit_types.Length; ++index)
    {
      empty2 += (string) (object) this.Tutorial_gacha_unit_types[index];
      if (index < this.Tutorial_gacha_unit_types.Length - 1)
        empty2 += ",";
    }
    Persist.tutorialGacha.Data.strUnitTypes = Convert.ToBase64String(Crypt.Encrypt(Encoding.UTF8.GetBytes(empty2)));
    string empty3 = string.Empty;
    for (int index = 0; index < this.Tutorial_gacha_deck_ids.Length; ++index)
    {
      empty3 += (string) (object) this.Tutorial_gacha_deck_ids[index];
      if (index < this.Tutorial_gacha_deck_ids.Length - 1)
        empty3 += ",";
    }
    Persist.tutorialGacha.Data.strGachaDeckIDs = Convert.ToBase64String(Crypt.Encrypt(Encoding.UTF8.GetBytes(empty3)));
    Persist.tutorialGacha.Flush();
  }

  private bool DecryptGachaData()
  {
    if (!string.IsNullOrEmpty(Persist.tutorialGacha.Data.strUnitIDs) && !string.IsNullOrEmpty(Persist.tutorialGacha.Data.strUnitTypes) && !string.IsNullOrEmpty(Persist.tutorialGacha.Data.strGachaDeckIDs))
    {
      string[] strArray = Encoding.UTF8.GetString(Crypt.Decrypt(Convert.FromBase64String(Persist.tutorialGacha.Data.strUnitIDs))).Split(',');
      List<int> intList = new List<int>();
      for (int index = 0; index < strArray.Length; ++index)
      {
        int result = 0;
        int.TryParse(strArray[index], out result);
        intList.Add(result);
        this.tutorial_gacha_unit_ids = intList.ToArray();
      }
      intList.Clear();
      string str1 = Encoding.UTF8.GetString(Crypt.Decrypt(Convert.FromBase64String(Persist.tutorialGacha.Data.strUnitTypes)));
      char[] chArray1 = new char[1]{ ',' };
      foreach (string s in str1.Split(chArray1))
      {
        int result = 0;
        int.TryParse(s, out result);
        intList.Add(result);
        this.tutorial_gacha_unit_types = intList.ToArray();
      }
      intList.Clear();
      string str2 = Encoding.UTF8.GetString(Crypt.Decrypt(Convert.FromBase64String(Persist.tutorialGacha.Data.strGachaDeckIDs)));
      char[] chArray2 = new char[1]{ ',' };
      foreach (string s in str2.Split(chArray2))
      {
        int result = 0;
        int.TryParse(s, out result);
        intList.Add(result);
        this.tutorial_gacha_deck_ids = intList.ToArray();
      }
      return true;
    }
    Persist.tutorialGacha.Data.clearGachaResult();
    Persist.tutorialGacha.Flush();
    return false;
  }

  private int GetUnitType(int unitID, UnitTypeDeck[] unitTypeList)
  {
    UnitTypeDeck[] array = ((IEnumerable<UnitTypeDeck>) unitTypeList).Where<UnitTypeDeck>((Func<UnitTypeDeck, bool>) (x => x.group_id_UnitUnit.HasValue && x.group_id_UnitUnit.Value == unitID)).ToArray<UnitTypeDeck>();
    int num1 = UnityEngine.Random.Range(1, ((IEnumerable<UnitTypeDeck>) array).Sum<UnitTypeDeck>((Func<UnitTypeDeck, int>) (x => x.appearance)) + 1);
    int num2 = 0;
    for (int index = 0; index < array.Length; ++index)
    {
      if (num2 + array[index].appearance >= num1)
        return array[index].category_id;
      num2 += array[index].appearance;
    }
    return 1;
  }

  private int GetTutorialGachaID()
  {
    DateTime nowTime = ServerTime.NowAppTimeAddDelta();
    GachaTutorialPeriod gachaPeriod = ((IEnumerable<GachaTutorialPeriod>) MasterData.GachaTutorialPeriodList).Where<GachaTutorialPeriod>((Func<GachaTutorialPeriod, bool>) (x =>
    {
      if (x.start_at.HasValue)
      {
        DateTime dateTime1 = nowTime;
        DateTime? nullable = x.start_at;
        if ((nullable.HasValue ? (dateTime1 >= nullable.GetValueOrDefault() ? 1 : 0) : 0) != 0 && x.end_at.HasValue)
        {
          DateTime dateTime2 = nowTime;
          nullable = x.end_at;
          return nullable.HasValue && dateTime2 < nullable.GetValueOrDefault();
        }
      }
      return false;
    })).FirstOrDefault<GachaTutorialPeriod>();
    if (gachaPeriod == null)
      return -1;
    GachaTutorial gachaTutorial = ((IEnumerable<GachaTutorial>) MasterData.GachaTutorialList).Where<GachaTutorial>((Func<GachaTutorial, bool>) (x =>
    {
      int? gachaTutorialPeriod = x._period_id_GachaTutorialPeriod;
      int id = gachaPeriod.ID;
      return gachaTutorialPeriod.GetValueOrDefault() == id & gachaTutorialPeriod.HasValue;
    })).FirstOrDefault<GachaTutorial>();
    return gachaTutorial == null ? -1 : gachaTutorial.ID;
  }

  public void DebugTutorialStart()
  {
    Persist.tutorial.Data.SetPageIndex(0);
    Persist.tutorial.Delete();
    Singleton<NGGameDataManager>.GetInstance().refreshHomeHome = true;
    Singleton<NGGameDataManager>.GetInstance().StartSceneAsyncProxy((System.Action<NGGameDataManager.StartSceneProxyResult>) (_ => this.StartTutorial((WebAPI.Response.TutorialTutorialRagnarokResume) null)));
  }

  public void DebugTutorialFinish()
  {
    this.wrap.SetActive(false);
    this.finish(true);
  }

  public void DebugTutorialAdvice(string message)
  {
    this.showAdvice(message, (string) null, (Dictionary<string, Func<Transform, UIButton>>) null, (System.Action) null);
  }

  private IEnumerator bulkDownLoadCheck()
  {
    TutorialRoot tutorialRoot = this;
    CommonRoot common = Singleton<CommonRoot>.GetInstance();
    common.isTouchBlock = true;
    yield return (object) new WaitForSeconds(0.5f);
    common.isTouchBlock = false;
    common.isLoading = true;
    yield return (object) new WaitForSeconds(0.5f);
    long requiredSize = OnDemandDownload.SizeOfLoadAllUnits();
    common.isLoading = false;
    Consts instance = Consts.GetInstance();
    if (requiredSize > 0L)
    {
      // ISSUE: reference to a compiler-generated method
      PopupCommonYesNo.Show(instance.bulk_download_title, instance.bulkDownloadText(requiredSize), new System.Action(tutorialRoot.\u003CbulkDownLoadCheck\u003Eb__76_0), (System.Action) (() => {}));
    }
    else
      tutorialRoot.StartCoroutine(PopupCommon.Show(instance.bulk_download_title, instance.bulk_downloaded_text, (System.Action) (() => {})));
  }

  private IEnumerator doBulkDownload()
  {
    CommonRoot common = Singleton<CommonRoot>.GetInstance();
    common.isTouchBlock = true;
    yield return (object) new WaitForSeconds(0.5f);
    common.isTouchBlock = false;
    common.isLoading = true;
    yield return (object) new WaitForSeconds(0.5f);
    Debug.Log((object) "start bulk download");
    IEnumerator e = OnDemandDownload.WaitLoadAllUnits(false);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    common.isLoading = false;
    MypageScene.ChangeScene(false, false, false);
  }

  public void resumeTutorialForVer710()
  {
    Persist.tutorial.Data.SetPageIndex(this.progress.GetTutoarialGachaPage());
    Persist.tutorial.Flush();
    Persist.newTutorial.Data.beginnersQuest = false;
    Persist.newTutorial.Flush();
  }
}
