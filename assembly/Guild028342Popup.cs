﻿// Decompiled with JetBrains decompiler
// Type: Guild028342Popup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System;
using System.Collections;
using UnityEngine;

public class Guild028342Popup : BackButtonMenuBase
{
  [SerializeField]
  private UILabel popupTitle;
  [SerializeField]
  private UILabel popupDesc;
  private Guild0283Menu menu;

  public void Initialize(Guild0283Menu menu)
  {
    if ((UnityEngine.Object) this.GetComponent<UIWidget>() != (UnityEngine.Object) null)
      this.GetComponent<UIWidget>().alpha = 0.0f;
    this.menu = menu;
    this.popupTitle.SetTextLocalize(Consts.Format(Consts.GetInstance().POPUP_GUILD_MENU_RESIGN_CONFIRM_TITLE, (IDictionary) null));
    this.popupDesc.SetTextLocalize(Consts.Format(Consts.GetInstance().POPUP_GUILD_MENU_RESIGN_DESC3, (IDictionary) null));
  }

  public override void onBackButton()
  {
    Singleton<PopupManager>.GetInstance().closeAll(false);
  }

  private IEnumerator LeaveFromGuild()
  {
    Guild028342Popup guild028342Popup = this;
    Singleton<PopupManager>.GetInstance().closeAllWithoutAnim(false);
    while (Singleton<PopupManager>.GetInstance().isOpenNoFinish)
      yield return (object) null;
    Singleton<CommonRoot>.GetInstance().loadingMode = 1;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    // ISSUE: reference to a compiler-generated method
    Future<WebAPI.Response.GuildMembershipsLeave> ft = WebAPI.GuildMembershipsLeave(false, new System.Action<WebAPI.Response.UserError>(guild028342Popup.\u003CLeaveFromGuild\u003Eb__5_0));
    IEnumerator e1 = ft.Wait();
    while (e1.MoveNext())
      yield return e1.Current;
    e1 = (IEnumerator) null;
    if (ft.Result != null)
    {
      Future<WebAPI.Response.GuildTop> guildTop = WebAPI.GuildTop(-1, false, (System.Action<WebAPI.Response.UserError>) (e =>
      {
        Singleton<CommonRoot>.GetInstance().isLoading = false;
        Singleton<CommonRoot>.GetInstance().loadingMode = 0;
        WebAPI.DefaultUserErrorCallback(e);
        Singleton<CommonRoot>.GetInstance().isLoading = true;
        Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
      }));
      e1 = guildTop.Wait();
      while (e1.MoveNext())
        yield return e1.Current;
      e1 = (IEnumerator) null;
      if (guildTop.Result != null)
      {
        Singleton<CommonRoot>.GetInstance().isLoading = false;
        Singleton<CommonRoot>.GetInstance().loadingMode = 0;
        Singleton<NGSceneManager>.GetInstance().destroyLoadedScenes();
        Singleton<NGSceneManager>.GetInstance().clearStack();
        Guild02811Scene.ChangeScene();
      }
    }
  }

  public void onYesButton()
  {
    this.StartCoroutine(this.LeaveFromGuild());
  }
}
