﻿// Decompiled with JetBrains decompiler
// Type: BattlePrepareCharacterInfoPlayer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class BattlePrepareCharacterInfoPlayer : BattlePrepareCharacterInfoBase
{
  [SerializeField]
  private GameObject selectCommand;
  [SerializeField]
  private NGHorizontalScrollParts indicator;
  [SerializeField]
  private GameObject mDotContainer;
  private GameObject commandPrefab;

  public override IEnumerator Init(
    BL.UnitPosition up,
    AttackStatus[] attackStatus,
    bool isFacility)
  {
    BattlePrepareCharacterInfoPlayer characterInfoPlayer = this;
    characterInfoPlayer.selectCommand.SetActive(false);
    Future<GameObject> commandPrefabF = !Singleton<NGBattleManager>.GetInstance().isSea ? Res.Prefabs.battleUI_04.command.Load<GameObject>() : Res.Prefabs.battleUI_04_sea.command_sea.Load<GameObject>();
    IEnumerator e = commandPrefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    characterInfoPlayer.commandPrefab = commandPrefabF.Result;
    if (up == null)
    {
      Debug.LogWarning((object) "unit is null");
    }
    else
    {
      // ISSUE: reference to a compiler-generated method
      yield return (object) characterInfoPlayer.\u003C\u003En__0(up, attackStatus, isFacility);
      characterInfoPlayer.indicator.destroyParts(true);
      characterInfoPlayer.indicator.resetScrollView();
      characterInfoPlayer.setIndicatorVisible();
      characterInfoPlayer.selectCommand.SetActive(true);
      AttackStatus[] attackStatusArray = characterInfoPlayer.attacks;
      for (int index = 0; index < attackStatusArray.Length; ++index)
      {
        AttackStatus attack = attackStatusArray[index];
        yield return (object) characterInfoPlayer.indicator.instantiateParts(characterInfoPlayer.commandPrefab, true).GetComponent<BattleUI04CommandPrefab>().Init(attack, up.unit);
      }
      attackStatusArray = (AttackStatus[]) null;
      if (((IEnumerable<AttackStatus>) characterInfoPlayer.attacks).Where<AttackStatus>((Func<AttackStatus, bool>) (x => !x.isHeal)).Count<AttackStatus>() == 1)
      {
        if ((UnityEngine.Object) null != (UnityEngine.Object) characterInfoPlayer.mDotContainer)
          characterInfoPlayer.mDotContainer.SetActive(false);
      }
      else if ((UnityEngine.Object) null != (UnityEngine.Object) characterInfoPlayer.mDotContainer)
      {
        characterInfoPlayer.mDotContainer.SetActive(true);
        Vector3 localPosition = characterInfoPlayer.mDotContainer.transform.localPosition;
        localPosition.y = -32f;
        characterInfoPlayer.mDotContainer.transform.localPosition = localPosition;
      }
      characterInfoPlayer.indicator.resetScrollView();
    }
  }

  private void setIndicatorVisible()
  {
    this.indicator.gameObject.GetComponent<TweenAlpha>().PlayForward();
  }

  public void onItemChanged()
  {
    if (this.attacks.Length == 0)
    {
      Debug.LogWarning((object) "magicBullets is empty but call onItemChanged");
    }
    else
    {
      int selected = this.indicator.selected;
      if (selected < 0 || selected >= this.attacks.Length)
      {
        Debug.LogError((object) ("bug, illegal indicator index=" + (object) selected));
      }
      else
      {
        this.setCurrentAttack(this.attacks[selected]);
        int cost = this.attacks[selected].magicBullet.cost;
        if (cost > 0)
          this.TxtConsume.SetTextLocalize("-" + (object) cost);
        else
          this.TxtConsume.SetText("");
      }
    }
  }

  protected override ResourceObject maskResource()
  {
    return Res.GUI._009_3_sozai.mask_Chara_L;
  }
}
