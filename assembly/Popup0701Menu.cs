﻿// Decompiled with JetBrains decompiler
// Type: Popup0701Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

public class Popup0701Menu : BackButtonMonoBehaiviour
{
  private System.Action<bool> mCloseCallback;
  private GameObject popup2;
  private bool pushEnable;

  public void Init(System.Action<bool> closeCallBack, GameObject popup0702)
  {
    this.popup2 = popup0702;
    this.mCloseCallback = closeCallBack;
    this.pushEnable = false;
  }

  public IEnumerator pushOnWait()
  {
    yield return (object) new WaitForSeconds(0.2f);
    this.pushEnable = true;
  }

  private void OnEnable()
  {
    this.StartCoroutine(this.pushOnWait());
  }

  public void onYes()
  {
    if (!this.pushEnable)
      return;
    this.pushEnable = false;
    this.Save();
  }

  private void Save()
  {
    Singleton<PopupManager>.GetInstance().dismiss(false);
    this.mCloseCallback(false);
  }

  public void onNo()
  {
    if (!this.pushEnable)
      return;
    this.pushEnable = false;
    Singleton<PopupManager>.GetInstance().open(this.popup2, false, false, false, true, false, false, "SE_1006").GetComponent<Popup0702Menu>().Init(this.mCloseCallback);
  }

  public override void onBackButton()
  {
    this.onNo();
  }
}
