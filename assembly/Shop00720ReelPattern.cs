﻿// Decompiled with JetBrains decompiler
// Type: Shop00720ReelPattern
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Shop00720ReelPattern : MonoBehaviour
{
  [SerializeField]
  private UI2DSprite[] Icons;
  [SerializeField]
  private UILabel Description;

  public void Init(List<UnityEngine.Sprite> sprites, string txt)
  {
    foreach (var data in ((IEnumerable<UI2DSprite>) this.Icons).Select((s, i) => new
    {
      s = s,
      i = i
    }))
      this.SetIcon(data.s, sprites[data.i]);
    this.SetDiscription(txt);
  }

  private void SetIcon(UI2DSprite target, UnityEngine.Sprite sprite)
  {
    target.sprite2D = sprite;
  }

  private void SetDiscription(string txt)
  {
    this.Description.SetTextLocalize(txt);
  }
}
