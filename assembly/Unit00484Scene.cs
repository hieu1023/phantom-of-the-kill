﻿// Decompiled with JetBrains decompiler
// Type: Unit00484Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Unit00484Scene : NGSceneBase
{
  public Unit00484Menu menu;

  public static void changeScene(
    bool stack,
    PlayerUnit basePlayerUnit,
    PlayerUnit[] materialPlayerUnits,
    Unit00468Scene.Mode mode,
    System.Action exceptionBackScene)
  {
    if (mode == Unit00468Scene.Mode.Unit0048)
      Singleton<NGGameDataManager>.GetInstance().clearPreviewInheritance(0);
    Singleton<NGSceneManager>.GetInstance().changeScene("unit004_8_4", (stack ? 1 : 0) != 0, (object) basePlayerUnit, (object) materialPlayerUnits, (object) mode, (object) exceptionBackScene);
  }

  public static void changeScene(
    bool stack,
    PlayerUnit basePlayerUnit,
    PlayerUnit[] materialPlayerUnits,
    Unit00468Scene.Mode mode)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("unit004_8_4", (stack ? 1 : 0) != 0, (object) basePlayerUnit, (object) materialPlayerUnits, (object) mode);
  }

  public IEnumerator onStartSceneAsync(
    PlayerUnit basePlayerUnit,
    PlayerUnit[] materialPlayerUnits,
    Unit00468Scene.Mode mode,
    System.Action exceptionBackScene)
  {
    this.menu.exceptionBackScene = exceptionBackScene;
    this.menu.ibtn_Change.SetActive(false);
    yield return (object) this.onStartSceneAsync(basePlayerUnit, materialPlayerUnits, mode);
    CommonRoot instance = Singleton<CommonRoot>.GetInstance();
    instance.isLoading = false;
    instance.isWebRunning = false;
  }

  public IEnumerator onStartSceneAsync(
    PlayerUnit basePlayerUnit,
    PlayerUnit[] materialPlayerUnits,
    Unit00468Scene.Mode _)
  {
    Unit00484Scene unit00484Scene = this;
    IEnumerator e;
    if ((UnityEngine.Object) unit00484Scene.menu.combineResultMenu == (UnityEngine.Object) null)
    {
      Future<GameObject> combineResultMenuF = new ResourceObject("Animations/Unit_Integration/dir_Unit_Integration").Load<GameObject>();
      e = combineResultMenuF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      GameObject gameObject = combineResultMenuF.Result.Clone(unit00484Scene.transform);
      unit00484Scene.menu.combineResultMenu = gameObject.GetComponent<Unit004813Menu>();
      combineResultMenuF = (Future<GameObject>) null;
    }
    unit00484Scene.menu.mainPanel00484.SetActive(true);
    unit00484Scene.menu.combineResultMenu.gameObject.SetActive(false);
    unit00484Scene.menu.combineResultMenu.IsPush = false;
    Singleton<CommonRoot>.GetInstance().setBackground(unit00484Scene.backgroundPrefab);
    PlayerUnit basePlayerUnit1 = ((IEnumerable<PlayerUnit>) SMManager.Get<PlayerUnit[]>()).FirstOrDefault<PlayerUnit>((Func<PlayerUnit, bool>) (x => x.id == basePlayerUnit.id));
    if (basePlayerUnit1 == (PlayerUnit) null)
      basePlayerUnit1 = basePlayerUnit;
    e = unit00484Scene.menu.Init(basePlayerUnit1, materialPlayerUnits);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    unit00484Scene.AutoOpenUnityToutaPopupCoroutine();
    CommonRoot instance = Singleton<CommonRoot>.GetInstance();
    instance.isLoading = false;
    instance.isWebRunning = false;
  }

  public void onStartScene(
    PlayerUnit basePlayerUnit,
    PlayerUnit[] materialPlayerUnits,
    Unit00468Scene.Mode mode,
    System.Action exceptionBackScene)
  {
    this.onStartScene(basePlayerUnit, materialPlayerUnits, mode);
  }

  public void onStartScene(
    PlayerUnit basePlayerUnit,
    PlayerUnit[] materialPlayerUnits,
    Unit00468Scene.Mode mode)
  {
    switch (mode)
    {
      case Unit00468Scene.Mode.Unit0048:
        Singleton<TutorialRoot>.GetInstance().ShowAdvice("unit004_8_3", 0, (System.Action) null);
        break;
      case Unit00468Scene.Mode.Unit00481:
        Singleton<TutorialRoot>.GetInstance().ShowAdvice("unit004_8_4_reinforce", 0, (System.Action) null);
        break;
    }
    this.menu.ResetScrollViewPosition();
  }

  private IEnumerator delayStartScene(float delay, System.Action exec)
  {
    yield return (object) new WaitForSeconds(delay);
    exec();
  }

  private void AutoOpenUnityToutaPopupCoroutine()
  {
    NGGameDataManager instance = Singleton<NGGameDataManager>.GetInstance();
    if (instance.fromUnityPopup != NGGameDataManager.FromUnityPopup.Unit00484Scene)
      return;
    instance.OnceOpenUnityPopup();
    instance.fromUnityPopup = NGGameDataManager.FromUnityPopup.None;
  }

  public override void onEndScene()
  {
    base.onEndScene();
    Singleton<CommonRoot>.GetInstance().SetFooterEnable(true);
    Singleton<CommonRoot>.GetInstance().setDisableFooterColor(false);
  }
}
