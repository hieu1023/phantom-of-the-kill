﻿// Decompiled with JetBrains decompiler
// Type: Jump
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Jump : MonoBehaviour
{
  public float jump = 60f;
  public float time = 0.1f;
  public Jump.JumpEffect state;
  private float afterPos;

  public void StartJump()
  {
  }

  private void Start()
  {
    this.state = Jump.JumpEffect.None;
  }

  private void Update()
  {
    if (this.state != Jump.JumpEffect.Start)
      return;
    float y = this.transform.localPosition.y;
    float x = this.transform.localPosition.x;
    if ((double) y == (double) this.afterPos || (double) y <= (double) this.afterPos)
      TweenPosition.Begin(this.gameObject, this.time, new Vector3(x, this.jump, 0.0f));
    if ((double) y < (double) this.jump)
      return;
    TweenPosition.Begin(this.gameObject, this.time, new Vector3(x, this.afterPos, 0.0f));
    this.state = Jump.JumpEffect.None;
  }

  public enum JumpEffect
  {
    None,
    Start,
  }
}
