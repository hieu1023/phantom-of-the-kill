﻿// Decompiled with JetBrains decompiler
// Type: Versus02610WeekRankPopup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

public class Versus02610WeekRankPopup : BackButtonMenuBase
{
  [SerializeField]
  private UILabel txt;
  private Versus02610Menu menu;
  private Popup02689Menu rankingDoneMenu;

  public IEnumerator Init(
    Versus02610Menu menu,
    int current_rank,
    Popup02689Menu rankingDoneMenu = null)
  {
    this.menu = menu;
    this.rankingDoneMenu = rankingDoneMenu;
    this.txt.SetText(MasterData.PvpRankingKind[current_rank].name);
    yield return (object) null;
  }

  public override void onBackButton()
  {
    this.IbtnTouch();
  }

  public void IbtnTouch()
  {
    if (this.IsPushAndSet())
      return;
    this.close();
  }

  public void close()
  {
    this.menu.dispHowto();
    if ((Object) this.rankingDoneMenu != (Object) null)
      this.rankingDoneMenu.close();
    Singleton<PopupManager>.GetInstance().dismiss(false);
  }
}
