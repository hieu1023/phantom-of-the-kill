﻿// Decompiled with JetBrains decompiler
// Type: EnumExtensions
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnitDetails;

public static class EnumExtensions
{
  public static bool IsOn(this Control self, Control flags)
  {
    return (self & flags) == flags;
  }

  public static bool IsOff(this Control self, Control flags)
  {
    return (self & flags) == Control.Zero;
  }

  public static Control Set(this Control self, Control flags)
  {
    return self | flags;
  }

  public static Control Clear(this Control self, Control flags)
  {
    return self & ~flags;
  }
}
