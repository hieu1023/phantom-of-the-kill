﻿// Decompiled with JetBrains decompiler
// Type: Unit004682Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;

public class Unit004682Menu : Unit00468Menu
{
  private int changeNumber;
  private HashSet<int> includeOverkillersIds;
  private const int FIRST_DECK = 0;
  private const int LAST_MEMBER = 1;

  protected override void Sort(SortInfo info)
  {
    this.BaseSort(info);
  }

  protected override int GetUsedCost()
  {
    int cost = 0;
    this.selectedUnitIcons.ForEach((System.Action<UnitIconInfo>) (x => cost += x.playerUnit.cost));
    UnitIconInfo unitIconInfo = this.selectedUnitIcons.FirstOrDefault<UnitIconInfo>((Func<UnitIconInfo, bool>) (x => x.select == this.changeNumber));
    if (unitIconInfo != null)
      cost -= unitIconInfo.playerUnit.cost;
    return cost;
  }

  protected override void updateTxtCostValue(int cost = 0)
  {
    this.totalCost = cost;
  }

  public override void UpdateInfomation()
  {
  }

  public override void InitializeAllUnitInfosExtend(PlayerDeck playerDeck)
  {
    this.selectedUnitIcons.Clear();
    PlayerUnit[] playerUnits = playerDeck.player_units;
    foreach (UnitIconInfo allUnitInfo in this.allUnitInfos)
    {
      UnitIconInfo info = allUnitInfo;
      info.select = -1;
      info.for_battle = false;
      if (!info.removeButton)
      {
        int? nullable = ((IEnumerable<PlayerUnit>) playerUnits).FirstIndexOrNull<PlayerUnit>((Func<PlayerUnit, bool>) (a => a != (PlayerUnit) null && a.id == info.playerUnit.id));
        if (nullable.HasValue)
        {
          info.select = nullable.Value;
          info.for_battle = true;
          this.selectedUnitIcons.Add(info);
        }
      }
    }
    this.updateExcludeOverkillers(playerUnits);
    this.includeOverkillersIds = new HashSet<int>();
    if (playerUnits.Length > this.changeNumber && playerUnits[this.changeNumber] != (PlayerUnit) null)
    {
      PlayerUnit playerUnit = playerUnits[this.changeNumber];
      if (playerUnit.isAnyCacheOverkillersUnits)
      {
        for (int index = 0; index < playerUnit.cache_overkillers_units.Length; ++index)
        {
          if (playerUnit.cache_overkillers_units[index] != (PlayerUnit) null)
            this.includeOverkillersIds.Add(playerUnit.cache_overkillers_units[index].id);
        }
      }
      else
      {
        int overkillersBaseId;
        if ((overkillersBaseId = playerUnit.overkillers_base_id) > 0)
          this.includeOverkillersIds.Add(overkillersBaseId);
      }
      if (this.includeOverkillersIds.Count > 0)
        this.includeOverkillersIds.Add(playerUnit.id);
    }
    this.ReflectionSelectUnit();
    this.CreateSelectUnitList(true);
    this.updateTxtCostValue(this.GetUsedCost());
  }

  private bool GetRemoveBtnFlg(PlayerDeck playerDeck, int number)
  {
    bool flag = false;
    if (playerDeck.player_unit_ids[number].HasValue)
    {
      flag = true;
      int num = 0;
      foreach (PlayerUnit playerUnit in playerDeck.player_units)
      {
        if (playerUnit != (PlayerUnit) null)
          ++num;
      }
      if (playerDeck.deck_number == 0 && num <= 1)
        flag = false;
    }
    return flag;
  }

  public IEnumerator Init(
    Player player,
    PlayerDeck playerDeck,
    PlayerUnit[] playerUnits,
    Promise<int?[]> player_unit_ids,
    int max_cost,
    int number,
    bool isEquip)
  {
    Unit004682Menu unit004682Menu = this;
    IEnumerator e = unit004682Menu.Initialize();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    e = ServerTime.WaitSync();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    unit004682Menu.serverTime = ServerTime.NowAppTime();
    unit004682Menu.playerUnitIds = player_unit_ids;
    unit004682Menu.changeNumber = number;
    unit004682Menu.deck_type_id = playerDeck.deck_type_id;
    unit004682Menu.deck_number = playerDeck.deck_number;
    unit004682Menu.maxCost = max_cost;
    unit004682Menu.totalCost = 0;
    playerUnits = ((IEnumerable<PlayerUnit>) playerUnits).Where<PlayerUnit>((Func<PlayerUnit, bool>) (x => x.unit.IsNormalUnit)).ToArray<PlayerUnit>();
    unit004682Menu.InitializeInfo((IEnumerable<PlayerUnit>) playerUnits, (IEnumerable<PlayerMaterialUnit>) null, Persist.unit00468SortAndFilter, isEquip, unit004682Menu.GetRemoveBtnFlg(playerDeck, number), false, true, true, (System.Action) (() => this.InitializeAllUnitInfosExtend(playerDeck)), 0);
    e = unit004682Menu.CreateUnitIcon();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    unit004682Menu.TxtNumber.SetTextLocalize(string.Format("{0}/{1}", (object) unit004682Menu.allUnitInfos.Count<UnitIconInfo>((Func<UnitIconInfo, bool>) (x => !x.removeButton)), (object) player.max_units));
    unit004682Menu.InitializeEnd();
  }

  private void DeckRemove(UnitIconBase ui)
  {
    ModelUnits.Instance.DestroyModelUnits();
    int? nullable = this.allUnitInfos.FirstIndexOrNull<UnitIconInfo>((Func<UnitIconInfo, bool>) (v => v.playerUnit != (PlayerUnit) null && v.select == this.changeNumber));
    if (!nullable.HasValue)
      return;
    UnitIconInfo allUnitInfo = this.allUnitInfos[nullable.Value];
    if (allUnitInfo != null && !allUnitInfo.removeButton)
      allUnitInfo.select = -1;
    this.CreateSelectUnitList(true);
    this.StartCoroutine(this.DeckEditAsync());
  }

  private void DeckEdit(UnitIconBase ui)
  {
    if (ui.Gray)
      return;
    ModelUnits.Instance.DestroyModelUnits();
    int? nullable = this.allUnitInfos.FirstIndexOrNull<UnitIconInfo>((Func<UnitIconInfo, bool>) (v => v.playerUnit != (PlayerUnit) null && v.select == this.changeNumber));
    if (nullable.HasValue)
    {
      UnitIconInfo allUnitInfo = this.allUnitInfos[nullable.Value];
      if (allUnitInfo != null && !allUnitInfo.removeButton)
        allUnitInfo.select = -1;
    }
    UnitIconInfo unitInfoAll = this.GetUnitInfoAll(ui.PlayerUnit);
    if (unitInfoAll != null && !unitInfoAll.removeButton)
      unitInfoAll.select = this.changeNumber;
    this.CreateSelectUnitList(true);
    this.StartCoroutine(this.DeckEditAsync());
  }

  protected override IEnumerator DeckEditAsync()
  {
    Unit004682Menu unit004682Menu = this;
    int[] array = unit004682Menu.selectedUnitIcons.Select<UnitIconInfo, int>((Func<UnitIconInfo, int>) (x => x.playerUnit.id)).ToArray<int>();
    IEnumerator e = unit004682Menu.DeckEditApi(unit004682Menu.deck_type_id, unit004682Menu.deck_number, array);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    unit004682Menu.backScene();
  }

  protected override void CreateUnitIconAction(int info_index, int unit_index)
  {
    UnitIconBase allUnitIcon = this.allUnitIcons[unit_index];
    UnitIconInfo displayUnitInfo = this.displayUnitInfos[info_index];
    if (displayUnitInfo.removeButton)
    {
      allUnitIcon.onClick = (System.Action<UnitIconBase>) (ui => this.DeckRemove(ui));
      allUnitIcon.Gray = false;
      allUnitIcon.Button.enabled = true;
    }
    else
    {
      allUnitIcon.onClick = (System.Action<UnitIconBase>) (ui => this.DeckEdit(ui));
      bool flag1 = this.includeOverkillersIds.Contains(allUnitIcon.PlayerUnit.id);
      bool flag2 = this.excludeOverkillersIds.Contains(allUnitIcon.PlayerUnit.id);
      if (allUnitIcon.PlayerUnit.cost > this.maxCost - this.totalCost || displayUnitInfo.select != -1 && displayUnitInfo.select != this.changeNumber || !flag1 & flag2)
      {
        displayUnitInfo.gray = true;
        allUnitIcon.Gray = true;
        allUnitIcon.Button.enabled = true;
        allUnitIcon.Overkillers = flag2;
      }
      else
      {
        displayUnitInfo.gray = false;
        allUnitIcon.Gray = false;
        allUnitIcon.Button.enabled = true;
        allUnitIcon.Overkillers = flag1;
      }
      allUnitIcon.SetupDeckStatusBlink();
      allUnitIcon.SelectNumberBase.SetActive(false);
      if (!Singleton<NGGameDataManager>.GetInstance().IsSea)
        return;
      if (displayUnitInfo != null && displayUnitInfo.unit != null)
        ((UnitIcon) allUnitIcon).SetSeaPiece(displayUnitInfo.unit.GetPiece);
      else
        ((UnitIcon) allUnitIcon).SetSeaPiece(false);
    }
  }
}
