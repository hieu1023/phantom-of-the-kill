﻿// Decompiled with JetBrains decompiler
// Type: Unit0042Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnitDetails;
using UnityEngine;

public class Unit0042Menu : BackButtonMenuBase
{
  private float groupSpriteHeight = 46.5f;
  private int groupBaseHeightInit = 36;
  private readonly int DISPLAY_OBJECT_MAX = 4;
  private Dictionary<int, bool> firstSetting = new Dictionary<int, bool>();
  private Dictionary<int, bool> changeSetting = new Dictionary<int, bool>();
  private bool lightON = true;
  private bool isArrowBtn = true;
  private bool isInitializing = true;
  [SerializeField]
  protected UILabel txt_CharacterName;
  [SerializeField]
  protected UILabel txt_JobName;
  [SerializeField]
  protected UI2DSprite weaponTypeIcon;
  [SerializeField]
  protected UI2DSprite rarityStarsIcon;
  [SerializeField]
  protected GameObject slcAwakening;
  [SerializeField]
  private GameObject LeftArrow;
  [SerializeField]
  private GameObject RightArrow;
  [SerializeField]
  private SpriteSelectDirectButton[] groupSprites;
  [SerializeField]
  private GameObject slc_GroupBase;
  private TweenHeight tween_GroupBaseOpen;
  private TweenHeight tween_GroupBaseClose;
  [SerializeField]
  private GameObject dir_GroupSprite;
  private TweenPosition tween_GroupSpriteDirOpen;
  private TweenPosition tween_GroupSpriteDirClose;
  [SerializeField]
  private UIButton IbtnGroupTabIdle;
  [SerializeField]
  private GameObject dir_GroupPressed;
  private bool isGroupOpen;
  private bool isGroupTween;
  private int dispGroupCount;
  private const int TWEEN_GROUPID_START = 100;
  private const int TWEEN_GROUPID_END = 101;
  protected int objectCnt;
  private List<GameObject> objectList;
  private Dictionary<GameObject, DetailMenuPrefab> detailMenuPrefabDict;
  private GameObject gearKindIcon;
  public NGxScroll scrollView;
  [SerializeField]
  private UICenterOnChild centerOnChild;
  private WheelScrollLog wheelLog;
  private PlayerItem equippedGear;
  private PlayerItem equippedGear2;
  private PlayerUnit[] unitList;
  public GameObject detailPrefab;
  public GameObject gearKindIconPrefab;
  public GameObject gearIconPrefab;
  public GameObject skillDetailDialogPrefab;
  public GameObject skillDetailDialog_changePrefab;
  public GameObject specialPointDetailDialogPrefab;
  public GameObject terraiAbilityDialogPrefab;
  public GameObject profIconPrefab;
  public GameObject skillTypeIconPrefab;
  public GameObject skillfullnessIconPrefab;
  public GameObject commonElementIconPrefab;
  public GameObject spAtkTypeIconPrefab;
  public GameObject modelPrefab;
  public GameObject skillListPrefab;
  protected GameObject statusDetailPrefab;
  public bool isFavorited;
  [SerializeField]
  private GameObject dirFavorite;
  [SerializeField]
  private GameObject btnFavoritedOff;
  [SerializeField]
  private GameObject btnFavoritedOn;
  [Header("オーバーキラーズベース")]
  [SerializeField]
  [Tooltip("特攻アイコン数に連動して繋ぎ変える")]
  private GameObject[] positionsOverkillersBase;
  private OverkillersUnitBaseTag tagOverkillersBase;
  private string playerId_;
  private int currentOverkillersBase;
  private GameObject trainingPrefab;
  private GameObject groupDetailDialogPrefab;
  private int currentIndex;
  private int infoIndex;
  private Control? controlFlags_;
  private bool isUpdateLastReference;
  private bool isDisabledScroll;
  private bool isEnabledBottomScroll;
  private bool isLockBottomScroll;
  private float? oldScrollViewLocalX;
  private bool isScrollViewDragStart;
  private int scrollStartCurrent;
  private bool isVRdrag;

  public bool isEarthMode { get; private set; }

  public bool IsFriend { get; private set; }

  public bool IsStorage { get; private set; }

  public bool IsLimitMode { get; private set; }

  public bool IsGvgMode { get; private set; }

  public bool IsMaterial { get; private set; }

  public bool IsMemory { get; private set; }

  public PlayerUnit[] UnitList
  {
    get
    {
      return this.unitList;
    }
  }

  public GameObject unityDetailPrefab { get; set; }

  public GameObject overkillersSlotReleasePrefab { get; set; }

  public GameObject unitIconPrefab { get; set; }

  public GameObject stageItemPrefab { get; set; }

  public GameObject overkillersBaseTagPrefab { get; set; }

  public GameObject skillLockIconPrefab { get; set; }

  public GameObject StatusDetailPrefab
  {
    get
    {
      return this.statusDetailPrefab;
    }
  }

  private string playerId
  {
    get
    {
      return this.playerId_ ?? (this.playerId_ = Player.Current.id);
    }
  }

  public GameObject TrainingPrefab
  {
    get
    {
      return this.trainingPrefab;
    }
  }

  public GameObject GroupDetailDialogPrefab
  {
    get
    {
      return this.groupDetailDialogPrefab;
    }
  }

  private int characterID { get; set; }

  private int voicePattern { get; set; }

  public int CurrentIndex
  {
    set
    {
      this.currentIndex = value;
    }
    get
    {
      return this.currentIndex;
    }
  }

  public int InfoIndex
  {
    set
    {
      this.infoIndex = value;
    }
    get
    {
      return this.infoIndex;
    }
  }

  public PlayerUnit CurrentUnit
  {
    get
    {
      return this.unitList == null || this.unitList.Length <= this.currentIndex ? (PlayerUnit) null : this.unitList[this.currentIndex];
    }
  }

  public GameObject CurrentScrollObject
  {
    get
    {
      return this.detailMenuPrefabDict == null || this.CurrentIndex < 0 ? (GameObject) null : this.detailMenuPrefabDict.FirstOrDefault<KeyValuePair<GameObject, DetailMenuPrefab>>((Func<KeyValuePair<GameObject, DetailMenuPrefab>, bool>) (x => x.Value.Index == this.CurrentIndex)).Key;
    }
  }

  public DetailMenuScrollViewParam.TabMode viewParamTabMode { get; private set; }

  public bool LightON
  {
    get
    {
      return this.lightON;
    }
    set
    {
      this.lightON = value;
    }
  }

  public virtual Control controlFlags
  {
    get
    {
      return !this.controlFlags_.HasValue ? (this.controlFlags_ = new Control?(this.GetComponent<Unit0042Scene>().bootParam.controlFlags)).Value : this.controlFlags_.Value;
    }
  }

  public void UpdateInfoIndicator(int idx)
  {
    this.InfoIndex = idx;
    if (this.detailMenuPrefabDict == null)
      return;
    foreach (KeyValuePair<GameObject, DetailMenuPrefab> keyValuePair in this.detailMenuPrefabDict)
    {
      GameObject key = keyValuePair.Key;
      if (!key.activeSelf)
        key.SetActive(true);
      keyValuePair.Value.SetInformationPanelIndex(this.InfoIndex);
    }
  }

  public void UpdateInfoIndicator(DetailMenuScrollViewParam.TabMode mode)
  {
    this.viewParamTabMode = mode;
    if (this.detailMenuPrefabDict == null)
      return;
    foreach (DetailMenuPrefab detailMenuPrefab in this.detailMenuPrefabDict.Values)
      detailMenuPrefab.SetInformationPanelTab(mode);
  }

  public virtual void IbtnBack()
  {
    if (this.IsPushAndSet())
      return;
    if (Singleton<NGSceneManager>.GetInstance().LastHeaderType.HasValue)
    {
      Singleton<CommonRoot>.GetInstance().headerType = Singleton<NGSceneManager>.GetInstance().LastHeaderType.Value;
      Singleton<NGSceneManager>.GetInstance().LastHeaderType = new CommonRoot.HeaderType?();
    }
    if (Singleton<NGGameDataManager>.GetInstance().IsSea)
    {
      Singleton<CommonRoot>.GetInstance().startScene = "sea030_home";
      Singleton<CommonRoot>.GetInstance().startSceneArgs = new object[1]
      {
        (object) false
      };
    }
    else
    {
      Singleton<CommonRoot>.GetInstance().startScene = "unit004_6_8";
      Singleton<CommonRoot>.GetInstance().startSceneArgs = new object[1]
      {
        (object) Unit00468Scene.Mode.Unit00411
      };
    }
    this.backScene();
  }

  public override void onBackButton()
  {
    this.IbtnBack();
  }

  public virtual void IbtnIntimacy()
  {
  }

  public virtual void IbtnWpEquip()
  {
  }

  public virtual void IbtnZoom()
  {
    if (this.IsPushAndSet())
      return;
    Unit0043Scene.changeScene(true, this.unitList[this.CurrentIndex], false);
  }

  public void IbtnLeftArrow()
  {
    if (!this.isArrowBtn)
      return;
    this.isArrowBtn = false;
    int num = this.CurrentIndex - 1;
    if (num >= 0 && this.CenterOnChild(num, false))
      return;
    this.StartCoroutine(this.IsArrowBtnOn());
  }

  public void IbtnRightArrow()
  {
    if (!this.isArrowBtn)
      return;
    this.isArrowBtn = false;
    int num = this.CurrentIndex + 1;
    if (num <= this.unitList.Length - 1 && this.CenterOnChild(num, false))
      return;
    this.StartCoroutine(this.IsArrowBtnOn());
  }

  private void OnCocFinished()
  {
    this.isArrowBtn = true;
    UIScrollView componentInChildren1 = this.centerOnChild.centeredObject.GetComponentInChildren<UIScrollView>();
    if ((UnityEngine.Object) componentInChildren1 != (UnityEngine.Object) null)
      componentInChildren1.enabled = true;
    UICenterOnChild componentInChildren2 = this.centerOnChild.centeredObject.GetComponentInChildren<UICenterOnChild>();
    if (!((UnityEngine.Object) componentInChildren2 != (UnityEngine.Object) null) || !((UnityEngine.Object) componentInChildren2.centeredObject != (UnityEngine.Object) null))
      return;
    componentInChildren2.centeredObject.GetComponent<DetailMenuScrollViewBase>()?.MarkAsChanged();
  }

  protected IEnumerator IsArrowBtnOn()
  {
    yield return (object) new WaitForSeconds(0.2f);
    this.isArrowBtn = true;
    UIScrollView componentInChildren = this.centerOnChild.centeredObject.GetComponentInChildren<UIScrollView>();
    if ((UnityEngine.Object) componentInChildren != (UnityEngine.Object) null)
      componentInChildren.enabled = true;
  }

  private IEnumerator UpdateActive(GameObject go)
  {
    go.SetActive(false);
    yield return (object) null;
    go.SetActive(true);
  }

  private void SetTitleBarInfo(bool isSe)
  {
    this.LeftArrow.SetActive(true);
    this.RightArrow.SetActive(true);
    if (this.CurrentIndex == 0)
      this.LeftArrow.SetActive(false);
    if (this.CurrentIndex >= this.unitList.Length - 1)
      this.RightArrow.SetActive(false);
    PlayerUnit unit1 = this.unitList[this.CurrentIndex];
    if (this.isUpdateLastReference)
      Singleton<NGGameDataManager>.GetInstance().lastReferenceUnitID = unit1.id;
    UnitUnit unit2 = unit1.unit;
    if (string.IsNullOrEmpty(unit2.formal_name))
      this.txt_CharacterName.SetText(unit2.name);
    else
      this.txt_CharacterName.SetText(unit2.formal_name);
    if ((UnityEngine.Object) this.txt_JobName != (UnityEngine.Object) null)
      this.txt_JobName.SetText(unit1.getJobData().name);
    this.gearKindIcon.SetActive(false);
    this.rarityStarsIcon.gameObject.SetActive(false);
    if ((UnityEngine.Object) this.slcAwakening != (UnityEngine.Object) null)
      this.slcAwakening.SetActive(false);
    if (unit2.IsNormalUnit)
    {
      this.gearKindIcon.SetActive(true);
      this.rarityStarsIcon.gameObject.SetActive(true);
      if ((UnityEngine.Object) this.slcAwakening != (UnityEngine.Object) null)
        this.slcAwakening.SetActive(unit2.awake_unit_flag);
      this.gearKindIcon.GetComponent<GearKindIcon>().Init(unit2.kind, unit1.GetElement());
      RarityIcon.SetRarity(unit1, this.rarityStarsIcon, true, false, false);
    }
    if ((UnityEngine.Object) this.dirFavorite != (UnityEngine.Object) null)
    {
      if (this.IsFriend || this.IsLimitMode)
      {
        this.dirFavorite.SetActive(false);
      }
      else
      {
        this.dirFavorite.SetActive(true);
        this.SetFavorite(this.changeSetting[unit1.id]);
      }
    }
    this.DisplayGroupLogo(unit2);
    this.setOverkillersBase(unit1);
    Unit0042Scene component = this.GetComponent<Unit0042Scene>();
    if ((UnityEngine.Object) component != (UnityEngine.Object) null)
      component.bootParam.playerUnit = unit1;
    NGSoundManager instance = Singleton<NGSoundManager>.GetInstance();
    if ((UnityEngine.Object) instance == (UnityEngine.Object) null)
      return;
    if (isSe)
      instance.playSE(Singleton<NGGameDataManager>.GetInstance().IsSea ? "SE_1043" : "SE_1005", false, 0.0f, -1);
    this.ChangeCharacterVoice(unit2, instance);
  }

  private void setOverkillersBase(PlayerUnit currentUnit)
  {
    if (this.controlFlags.IsOff(Control.OverkillersBase))
      return;
    int baseUnitId = currentUnit.is_storage || !(currentUnit.player_id == this.playerId) ? -1 : currentUnit.overkillers_base_id;
    PlayerUnit playerUnit = baseUnitId > 0 ? Array.Find<PlayerUnit>(SMManager.Get<PlayerUnit[]>(), (Predicate<PlayerUnit>) (x => x.id == baseUnitId)) : (PlayerUnit) null;
    if (playerUnit != (PlayerUnit) null)
    {
      if ((UnityEngine.Object) this.tagOverkillersBase == (UnityEngine.Object) null)
      {
        this.tagOverkillersBase = this.overkillersBaseTagPrefab.Clone(this.positionsOverkillersBase[0].transform).GetComponent<OverkillersUnitBaseTag>();
        this.tagOverkillersBase.initialize(this.unitIconPrefab, this.scrollView.scrollView, new System.Action(this.onClickedOverkillersBaseIcon), new System.Action(this.onLongPressedOverkillersBaseIcon));
      }
      this.StopCoroutine("doSetOverkillersBase");
      DateTime dateTime = ServerTime.NowAppTime();
      this.changePositionOverkillersBase(!string.IsNullOrEmpty(currentUnit.SpecialEffectType((IEnumerable<QuestScoreBonusTimetable>) this.getActiveQuestScoreBonus(dateTime), (IEnumerable<UnitBonus>) UnitBonus.getActiveUnitBonus(dateTime, new int?(), new int?()))) ? 1 : 0);
      this.StartCoroutine("doSetOverkillersBase", (object) playerUnit);
    }
    else
      this.setActiveOverkilersBase(false);
  }

  private IEnumerator doSetOverkillersBase(PlayerUnit unit)
  {
    IEnumerator e = this.tagOverkillersBase.doReset(unit);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  private void changePositionOverkillersBase(int index)
  {
    if (this.currentOverkillersBase == index || this.positionsOverkillersBase == null || this.positionsOverkillersBase.Length <= index)
      return;
    this.setActiveOverkilersBase(false);
    this.tagOverkillersBase.gameObject.SetParentSafeLocalTransform(this.positionsOverkillersBase[index]);
    this.currentOverkillersBase = index;
  }

  private void setActiveOverkilersBase(bool flag)
  {
    if (!((UnityEngine.Object) this.tagOverkillersBase != (UnityEngine.Object) null))
      return;
    this.tagOverkillersBase.gameObject.SetActive(flag);
  }

  private void onClickedOverkillersBaseIcon()
  {
    this.onLongPressedOverkillersBaseIcon();
  }

  private void onLongPressedOverkillersBaseIcon()
  {
    if (this.controlFlags.IsOff(Control.OverkillersMove))
      return;
    PlayerUnit currentUnit = this.CurrentUnit;
    if (!(currentUnit != (PlayerUnit) null))
      return;
    this.moveUnitPage(currentUnit.overkillers_base_id, this.tagOverkillersBase.objButton);
  }

  public void moveUnitPage(int unitId, GameObject objButton)
  {
    if (unitId <= 0)
      return;
    PlayerUnit target = Array.Find<PlayerUnit>(this.unitList, (Predicate<PlayerUnit>) (x => x.id == unitId));
    UIDragScrollView uiDragScrollView = (UnityEngine.Object) objButton != (UnityEngine.Object) null ? objButton.GetComponent<UIDragScrollView>() : (UIDragScrollView) null;
    bool flag = (UnityEngine.Object) uiDragScrollView != (UnityEngine.Object) null;
    UIEventTrigger uiTrigger;
    if (flag)
    {
      uiDragScrollView.SendMessage("OnPress", (object) false);
      uiDragScrollView.enabled = false;
      uiTrigger = objButton.GetOrAddComponent<UIEventTrigger>();
    }
    else
      uiTrigger = (UIEventTrigger) null;
    System.Action actMoveFinished = flag ? (System.Action) (() =>
    {
      if (!((UnityEngine.Object) objButton != (UnityEngine.Object) null))
        return;
      objButton.GetComponent<UIDragScrollView>().enabled = true;
    }) : (System.Action) (() => {});
    if ((UnityEngine.Object) uiTrigger != (UnityEngine.Object) null)
      EventDelegate.Set(uiTrigger.onRelease, (EventDelegate.Callback) (() =>
      {
        actMoveFinished();
        UnityEngine.Object.Destroy((UnityEngine.Object) uiTrigger);
      }));
    this.StartCoroutine(this.doMoveUnitPage(target, actMoveFinished));
  }

  private IEnumerator doMoveUnitPage(PlayerUnit target, System.Action actMoveFinished)
  {
    if (target == (PlayerUnit) null)
    {
      Consts instance = Consts.GetInstance();
      yield return (object) PopupCommon.Show(instance.POPUP_004_TITLE_ERROR_MOVE_UNITPAGE, instance.POPUP_004_MESSAGE_ERROR_MOVE_UNITPAGE, actMoveFinished);
    }
    else
    {
      this.isLockBottomScroll = true;
      Singleton<CommonRoot>.GetInstance().loadingMode = 0;
      Singleton<CommonRoot>.GetInstance().isLoading = true;
      int targetIndex = Array.FindIndex<PlayerUnit>(this.unitList, (Predicate<PlayerUnit>) (x => x.id == target.id));
      if (Mathf.Abs(targetIndex - this.currentIndex) > 1 || !this.CenterOnChild(targetIndex, false))
      {
        this.UpdateObjectList(targetIndex);
        yield return (object) this.CreatePage(targetIndex, true);
        this.CenterOnChild(targetIndex, true);
        while (targetIndex != this.currentIndex)
          yield return (object) null;
        for (int n = targetIndex - 1; n <= targetIndex + 1 && n < this.unitList.Length; ++n)
        {
          if (n >= 0 && n != targetIndex)
          {
            this.UpdateObjectList(n);
            yield return (object) this.CreatePage(n, true);
          }
        }
        foreach (UIScrollView componentsInChild in this.detailMenuPrefabDict.First<KeyValuePair<GameObject, DetailMenuPrefab>>((Func<KeyValuePair<GameObject, DetailMenuPrefab>, bool>) (x => x.Value.Index == targetIndex)).Key.GetComponentsInChildren<UIScrollView>())
        {
          if (componentsInChild.gameObject.activeInHierarchy)
          {
            componentsInChild.gameObject.SetActive(false);
            componentsInChild.gameObject.SetActive(true);
          }
        }
        yield return (object) null;
      }
      else
      {
        SpringPanel component;
        while (targetIndex != this.currentIndex || (UnityEngine.Object) (component = this.scrollView.scrollView.GetComponent<SpringPanel>()) == (UnityEngine.Object) null || component.enabled)
          yield return (object) null;
      }
      Singleton<CommonRoot>.GetInstance().isLoading = false;
      this.isLockBottomScroll = false;
    }
  }

  private void ChangeCharacterVoice(UnitUnit unit, NGSoundManager sm)
  {
    if (unit.IsMaterialUnit)
      return;
    int id = unit.character.ID;
    UnitVoicePattern unitVoicePattern = unit.unitVoicePattern;
    if (unitVoicePattern == null || this.characterID == id && this.voicePattern == unitVoicePattern.voice_pattern)
      return;
    this.characterID = id;
    this.voicePattern = unitVoicePattern.voice_pattern;
    sm.stopVoice(-1);
    sm.playVoiceByID(unitVoicePattern, 42, -1, 0.0f);
  }

  private void UpdateObjectList()
  {
    foreach (KeyValuePair<GameObject, DetailMenuPrefab> keyValuePair in this.detailMenuPrefabDict)
    {
      int index = keyValuePair.Value.Index;
      if ((index < this.CurrentIndex - 1 || index > this.CurrentIndex + 1) && !this.objectList.Contains(keyValuePair.Key))
        this.objectList.Add(keyValuePair.Key);
    }
  }

  private void UpdateObjectList(int includeIndex)
  {
    foreach (KeyValuePair<GameObject, DetailMenuPrefab> keyValuePair in this.detailMenuPrefabDict)
    {
      int index = keyValuePair.Value.Index;
      if (index == includeIndex)
      {
        if (this.objectList.Contains(keyValuePair.Key))
          this.objectList.Remove(keyValuePair.Key);
        this.objectList.Insert(0, keyValuePair.Key);
      }
      else if ((index < this.CurrentIndex - 1 || index > this.CurrentIndex + 1) && !this.objectList.Contains(keyValuePair.Key))
        this.objectList.Add(keyValuePair.Key);
    }
  }

  protected override void Update()
  {
    if (this.isInitializing)
    {
      this.isEnabledBottomScroll = this.scrollView.scrollView.isDragging;
    }
    else
    {
      base.Update();
      if (this.isDisabledScroll)
        return;
      int num1 = this.CurrentIndex;
      float x = this.scrollView.scrollView.transform.localPosition.x;
      if (this.oldScrollViewLocalX.HasValue)
      {
        double num2 = (double) x;
        float? scrollViewLocalX = this.oldScrollViewLocalX;
        double valueOrDefault = (double) scrollViewLocalX.GetValueOrDefault();
        if (num2 == valueOrDefault & scrollViewLocalX.HasValue)
        {
          this.isVRdrag = false;
          goto label_8;
        }
      }
      int num3 = -(int) (((double) x - (double) this.scrollView.grid.cellWidth / 2.0) / (double) this.scrollView.grid.cellWidth);
      num1 = num3 < 0 ? 0 : (num3 < this.unitList.Length ? num3 : this.unitList.Length - 1);
      this.oldScrollViewLocalX = new float?(x);
label_8:
      if (this.detailMenuPrefabDict != null && this.controlFlags.IsOff(Control.OverkillersUnit))
      {
        if ((UnityEngine.Object) this.wheelLog != (UnityEngine.Object) null)
        {
          this.isVRdrag |= (double) this.wheelLog.amount != 0.0;
          this.wheelLog.resetAmount();
        }
        this.isVRdrag |= this.scrollView.scrollView.isDragging;
        bool enable = !this.isVRdrag && !this.isLockBottomScroll;
        if (this.isEnabledBottomScroll != enable)
        {
          foreach (DetailMenuPrefab detailMenuPrefab in this.detailMenuPrefabDict.Values)
            detailMenuPrefab.SetInformationPaneEnable(enable, detailMenuPrefab.Index < 0 || detailMenuPrefab.isInitalizing ? new int?() : new int?(this.InfoIndex));
          this.isEnabledBottomScroll = enable;
        }
      }
      if (this.CurrentIndex != num1)
      {
        int num2 = this.CurrentIndex < num1 ? 1 : 0;
        this.CurrentIndex = num1;
        this.SetTitleBarInfo(true);
        if (num2 != 0)
        {
          if (this.CurrentIndex < this.unitList.Length - 1)
          {
            this.UpdateObjectList();
            this.StartCoroutine(this.CreatePage(this.CurrentIndex + 1, true));
          }
        }
        else if (this.CurrentIndex > 0)
        {
          this.UpdateObjectList();
          this.StartCoroutine(this.CreatePage(this.CurrentIndex - 1, true));
        }
      }
      if (this.scrollView.scrollView.isDragging)
      {
        if (this.isScrollViewDragStart)
          return;
        this.isScrollViewDragStart = true;
        this.scrollStartCurrent = this.CurrentIndex;
      }
      else
      {
        if (this.isScrollViewDragStart && this.scrollStartCurrent == this.CurrentIndex)
        {
          int currentIndex = this.CurrentIndex;
          double num2 = -(double) this.scrollView.grid.cellWidth * (double) this.CurrentIndex;
          float num4 = this.scrollView.grid.cellWidth * 0.25f;
          float num5 = (float) num2 - num4;
          float num6 = (float) num2 + num4;
          if ((double) this.scrollView.scrollView.transform.localPosition.x <= (double) num5)
            ++currentIndex;
          else if ((double) this.scrollView.scrollView.transform.localPosition.x >= (double) num6)
            --currentIndex;
          this.CenterOnChild(currentIndex <= this.unitList.Length ? currentIndex : this.unitList.Length - 1, false);
        }
        this.isScrollViewDragStart = false;
      }
    }
  }

  private void CreateFirstFavoriteSetting()
  {
    this.firstSetting.Clear();
    this.changeSetting.Clear();
    foreach (PlayerUnit playerUnit in ((IEnumerable<PlayerUnit>) this.unitList).Where<PlayerUnit>((Func<PlayerUnit, bool>) (x => x.unit.IsNormalUnit)))
    {
      this.firstSetting.Add(playerUnit.id, playerUnit.favorite);
      this.changeSetting.Add(playerUnit.id, playerUnit.favorite);
    }
  }

  public bool GetSetting(int id)
  {
    return this.changeSetting[id];
  }

  public void UpdateSetting(int id, bool flg)
  {
    this.changeSetting[id] = flg;
  }

  protected virtual IEnumerator LoadPrefabs()
  {
    Future<GameObject> loader = (Future<GameObject>) null;
    bool is_sea = Singleton<NGGameDataManager>.GetInstance().IsSea;
    IEnumerator e;
    if (is_sea)
    {
      loader = new ResourceObject("Prefabs/unit004_2_sea/detail_sea").Load<GameObject>();
      e = loader.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    else
    {
      loader = new ResourceObject("Prefabs/unit004_2/detail").Load<GameObject>();
      e = loader.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    this.detailPrefab = loader.Result;
    loader = !is_sea ? Res.Prefabs.ItemIcon.prefab.Load<GameObject>() : new ResourceObject("Prefabs/Sea/ItemIcon/prefab_sea").Load<GameObject>();
    e = loader.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.gearIconPrefab = loader.Result;
    loader = Res.Icons.GearKindIcon.Load<GameObject>();
    e = loader.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.gearKindIconPrefab = loader.Result;
    loader = PopupSkillDetails.createPrefabLoader(is_sea);
    yield return (object) loader.Wait();
    this.skillDetailDialogPrefab = loader.Result;
    loader = is_sea ? new ResourceObject("Prefabs/unit004_2_sea/SpecialPoint_DetailDialog_sea").Load<GameObject>() : new ResourceObject("Prefabs/unit004_2/SpecialPoint_DetailDialog").Load<GameObject>();
    e = loader.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.specialPointDetailDialogPrefab = loader.Result;
    loader = is_sea ? new ResourceObject("Prefabs/unit004_2_sea/TerraiAbilityDialog_sea").Load<GameObject>() : new ResourceObject("Prefabs/unit004_2/TerraiAbilityDialog").Load<GameObject>();
    e = loader.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.terraiAbilityDialogPrefab = loader.Result;
    loader = Res.Icons.GearProfiencyIcon.Load<GameObject>();
    e = loader.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.profIconPrefab = loader.Result;
    loader = Res.Prefabs.BattleSkillIcon._battleSkillIcon.Load<GameObject>();
    e = loader.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.skillTypeIconPrefab = loader.Result;
    loader = new ResourceObject("Prefabs/SkillFamily/SkillFamilyIcon").Load<GameObject>();
    e = loader.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.skillfullnessIconPrefab = loader.Result;
    loader = Res.Icons.CommonElementIcon.Load<GameObject>();
    e = loader.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.commonElementIconPrefab = loader.Result;
    loader = Res.Icons.SPAtkTypeIcon.Load<GameObject>();
    e = loader.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.spAtkTypeIconPrefab = loader.Result;
    loader = new ResourceObject("Prefabs/unit004_2/GroupDetailDialog").Load<GameObject>();
    e = loader.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.groupDetailDialogPrefab = loader.Result;
    loader = is_sea ? Res.Prefabs.unit.dir_unit_status_detail_sea.Load<GameObject>() : Res.Prefabs.unit.dir_unit_status_detail.Load<GameObject>();
    e = loader.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.statusDetailPrefab = loader.Result;
    loader = is_sea ? Res.Prefabs.unit004_2_sea.dir_unit_training_sea.Load<GameObject>() : Res.Prefabs.unit004_2.dir_unit_training.Load<GameObject>();
    e = loader.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.trainingPrefab = loader.Result;
    loader = !is_sea ? new ResourceObject("Prefabs/battle017_11_1_1/popup_SkillList").Load<GameObject>() : new ResourceObject("Prefabs/battle017_11_1_1/popup_SkillList_sea").Load<GameObject>();
    e = loader.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.skillListPrefab = loader.Result;
    Future<GameObject>[] loaders = PopupUnityValueDetail.createLoaders(is_sea);
    yield return (object) loaders[0].Wait();
    this.unityDetailPrefab = loaders[0].Result;
    yield return (object) loaders[1].Wait();
    this.stageItemPrefab = loaders[1].Result;
    loaders = (Future<GameObject>[]) null;
    loader = PopupOverkillersSlotRelease.createLoader(is_sea);
    yield return (object) loader.Wait();
    this.overkillersSlotReleasePrefab = loader.Result;
    loader = is_sea ? Res.Prefabs.Sea.UnitIcon.normal_sea.Load<GameObject>() : Res.Prefabs.UnitIcon.normal.Load<GameObject>();
    yield return (object) loader.Wait();
    this.unitIconPrefab = loader.Result;
    loader = OverkillersUnitBaseTag.createLoander(is_sea);
    yield return (object) loader.Wait();
    this.overkillersBaseTagPrefab = loader.Result;
    loader = new ResourceObject("Prefabs/BattleSkillIcon/dir_SkillLock").Load<GameObject>();
    yield return (object) loader.Wait();
    this.skillLockIconPrefab = loader.Result;
  }

  public IEnumerator Init(
    PlayerUnit playerUnit,
    PlayerUnit[] playerUnits,
    bool isFriend,
    bool limitMode,
    bool gvgMode,
    bool isEarthMode,
    bool isMaterial = false,
    PlayerItem equippedGear = null,
    PlayerItem equippedGear2 = null,
    bool isMemory = false)
  {
    Unit0042Menu unit0042Menu = this;
    unit0042Menu.isInitializing = true;
    unit0042Menu.unitList = playerUnits;
    unit0042Menu.IsFriend = isFriend;
    unit0042Menu.IsLimitMode = limitMode | isMemory;
    unit0042Menu.IsGvgMode = gvgMode;
    unit0042Menu.equippedGear = equippedGear;
    unit0042Menu.equippedGear2 = equippedGear2;
    unit0042Menu.IsStorage = playerUnit.is_storage;
    unit0042Menu.IsMaterial = isMaterial | isMemory | isFriend;
    unit0042Menu.IsMemory = isMemory;
    unit0042Menu.isEarthMode = isEarthMode;
    unit0042Menu.setEnabledArrowAnimation(true);
    if ((bool) (UnityEngine.Object) unit0042Menu.slc_GroupBase)
    {
      foreach (TweenHeight componentsInChild in unit0042Menu.slc_GroupBase.GetComponentsInChildren<TweenHeight>())
      {
        if (componentsInChild.tweenGroup == 100)
          unit0042Menu.tween_GroupBaseOpen = componentsInChild;
        if (componentsInChild.tweenGroup == 101)
          unit0042Menu.tween_GroupBaseClose = componentsInChild;
      }
      foreach (TweenPosition componentsInChild in unit0042Menu.dir_GroupSprite.GetComponentsInChildren<TweenPosition>())
      {
        if (componentsInChild.tweenGroup == 100)
          unit0042Menu.tween_GroupSpriteDirOpen = componentsInChild;
        if (componentsInChild.tweenGroup == 101)
          unit0042Menu.tween_GroupSpriteDirClose = componentsInChild;
      }
    }
    if (unit0042Menu.IsMemory && PlayerTransmigrateMemoryPlayerUnitIds.Current != null)
      PlayerTransmigrateMemoryPlayerUnitIds.Current.AddMemoryData(playerUnit);
    IEnumerator e = unit0042Menu.LoadPrefabs();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    unit0042Menu.gearKindIcon = unit0042Menu.gearKindIconPrefab.Clone(unit0042Menu.weaponTypeIcon.transform);
    unit0042Menu.gearKindIcon.GetComponent<UI2DSprite>().depth = unit0042Menu.weaponTypeIcon.depth + 1;
    unit0042Menu.CreateFirstFavoriteSetting();
    unit0042Menu.objectCnt = ((IEnumerable<PlayerUnit>) unit0042Menu.unitList).Count<PlayerUnit>();
    if (unit0042Menu.objectCnt > unit0042Menu.DISPLAY_OBJECT_MAX)
      unit0042Menu.objectCnt = unit0042Menu.DISPLAY_OBJECT_MAX;
    unit0042Menu.objectList = new List<GameObject>(unit0042Menu.objectCnt);
    if (unit0042Menu.detailMenuPrefabDict != null)
    {
      foreach (GameObject key in unit0042Menu.detailMenuPrefabDict.Keys)
      {
        if (!((UnityEngine.Object) key == (UnityEngine.Object) null))
        {
          key.SetActive(false);
          UnityEngine.Object.Destroy((UnityEngine.Object) key);
        }
      }
      unit0042Menu.detailMenuPrefabDict.Clear();
    }
    unit0042Menu.detailMenuPrefabDict = new Dictionary<GameObject, DetailMenuPrefab>(unit0042Menu.objectCnt);
    for (int index = 0; index < unit0042Menu.objectCnt; ++index)
    {
      GameObject key = unit0042Menu.detailPrefab.Clone((Transform) null);
      unit0042Menu.objectList.Add(key);
      unit0042Menu.scrollView.Add(key, false);
      DetailMenuPrefab component = key.GetComponent<DetailMenuPrefab>();
      DetailMenu normal = component.normal;
      unit0042Menu.detailMenuPrefabDict.Add(key, component);
      normal.ModelPos = new Vector3((float) (1000 * index), 0.0f, 0.0f);
      normal.isEarthMode = isEarthMode;
      normal.isMemory = unit0042Menu.IsMemory;
    }
    int? nullable = ((IEnumerable<PlayerUnit>) unit0042Menu.unitList).FirstIndexOrNull<PlayerUnit>((Func<PlayerUnit, bool>) (x => x.id == playerUnit.id));
    unit0042Menu.CurrentIndex = nullable.HasValue ? nullable.Value : 0;
    unit0042Menu.isUpdateLastReference = unit0042Menu.controlFlags.IsOff(Control.OverkillersUnit);
    if (unit0042Menu.isUpdateLastReference)
      Singleton<NGGameDataManager>.GetInstance().lastReferenceUnitIndex = unit0042Menu.CurrentIndex;
    int start = unit0042Menu.CurrentIndex - 1 < 0 ? 0 : unit0042Menu.CurrentIndex - 1;
    int end = unit0042Menu.CurrentIndex + 1 >= ((IEnumerable<PlayerUnit>) unit0042Menu.unitList).Count<PlayerUnit>() ? ((IEnumerable<PlayerUnit>) unit0042Menu.unitList).Count<PlayerUnit>() - 1 : unit0042Menu.CurrentIndex + 1;
    unit0042Menu.lightON = true;
    e = unit0042Menu.CreatePage(unit0042Menu.CurrentIndex, false);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if (unit0042Menu.isUpdateLastReference)
      unit0042Menu.CurrentIndex = Singleton<NGGameDataManager>.GetInstance().lastReferenceUnitIndex;
    unit0042Menu.scrollView.scrollView.transform.localPosition = new Vector3(-unit0042Menu.scrollView.grid.cellWidth * (float) unit0042Menu.CurrentIndex, 0.0f, 0.0f);
    unit0042Menu.lightON = false;
    for (int unitIdx = start; unitIdx <= end; ++unitIdx)
    {
      if (unitIdx != unit0042Menu.CurrentIndex)
        unit0042Menu.StartCoroutine(unit0042Menu.CreatePage(unitIdx, true));
    }
    unit0042Menu.SetTitleBarInfo(false);
    for (int index = 0; index < unit0042Menu.objectList.Count; ++index)
      unit0042Menu.objectList[index].transform.localPosition = end < unit0042Menu.unitList.Length - 1 ? new Vector3(unit0042Menu.scrollView.grid.cellWidth * (float) (end + index + 1), 0.0f, 0.0f) : new Vector3(unit0042Menu.scrollView.grid.cellWidth * (float) (start - (index + 1)), 0.0f, 0.0f);
    if (!unit0042Menu.scrollView.scrollView.enabled && unit0042Menu.unitList.Length > 1)
      unit0042Menu.scrollView.scrollView.enabled = true;
    unit0042Menu.isDisabledScroll = !unit0042Menu.scrollView.scrollView.enabled;
    if (!unit0042Menu.isDisabledScroll)
      unit0042Menu.wheelLog = unit0042Menu.scrollView.scrollView.GetComponent<WheelScrollLog>();
    unit0042Menu.isInitializing = false;
    if (!playerUnit.unit.IsMaterialUnit && !Persist.tutorial.Data.Hints.ContainsKey("unit004_2"))
      Singleton<TutorialRoot>.GetInstance().ShowAdvice("unit004_2_unit", 0, (System.Action) null);
  }

  public virtual IEnumerator CreatePage(int unitIdx, bool isUpdate = true)
  {
    Unit0042Menu menu = this;
    KeyValuePair<GameObject, DetailMenuPrefab> keyValuePair = menu.detailMenuPrefabDict.FirstOrDefault<KeyValuePair<GameObject, DetailMenuPrefab>>((Func<KeyValuePair<GameObject, DetailMenuPrefab>, bool>) (x => x.Value.Index == unitIdx));
    if ((UnityEngine.Object) keyValuePair.Key != (UnityEngine.Object) null)
      menu.objectList.Remove(keyValuePair.Key);
    else if (menu.objectList.Any<GameObject>())
    {
      GameObject go = menu.objectList.First<GameObject>();
      DetailMenuPrefab script = menu.detailMenuPrefabDict[go];
      menu.objectList.RemoveAt(0);
      Vector3 gridPos = menu.scrollView.grid.transform.localPosition;
      go.transform.localPosition = new Vector3(menu.scrollView.grid.cellWidth * (float) unitIdx, 0.0f, 0.0f);
      yield return (object) null;
      DateTime dateTime = ServerTime.NowAppTime();
      SMManager.Get<QuestScoreBonusTimetable[]>();
      QuestScoreBonusTimetable[] activeQuestScoreBonus = menu.getActiveQuestScoreBonus(dateTime);
      UnitBonus[] activeUnitBonus = UnitBonus.getActiveUnitBonus(dateTime, new int?(), new int?());
      IEnumerator e = script.Init(menu, unitIdx, menu.unitList[unitIdx], menu.InfoIndex, menu.IsLimitMode, activeQuestScoreBonus, activeUnitBonus, isUpdate, menu.IsMaterial);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      if (menu.IsFriend)
      {
        if (menu.equippedGear == (PlayerItem) null)
        {
          e = script.normal.setDefaultWeapon(0, menu.unitList[unitIdx].initial_gear);
          while (e.MoveNext())
            yield return e.Current;
          e = (IEnumerator) null;
        }
      }
      else if (menu.IsGvgMode && menu.unitList[unitIdx].primary_equipped_gear == (PlayerItem) null)
      {
        e = script.normal.setDefaultWeapon(0, menu.unitList[unitIdx].initial_gear);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
      }
      go.transform.localPosition = new Vector3(menu.scrollView.grid.cellWidth * (float) unitIdx, 0.0f, 0.0f);
      menu.scrollView.grid.transform.localPosition = gridPos;
    }
  }

  private QuestScoreBonusTimetable[] getActiveQuestScoreBonus(
    DateTime nowTime)
  {
    QuestScoreBonusTimetable[] scoreBonusTimetableArray = SMManager.Get<QuestScoreBonusTimetable[]>();
    return scoreBonusTimetableArray == null ? new QuestScoreBonusTimetable[0] : ((IEnumerable<QuestScoreBonusTimetable>) scoreBonusTimetableArray).Where<QuestScoreBonusTimetable>((Func<QuestScoreBonusTimetable, bool>) (x => x.start_at < nowTime && x.end_at > nowTime)).ToArray<QuestScoreBonusTimetable>();
  }

  public bool CheckLength(PlayerUnit[] playerUnits)
  {
    return this.unitList == null || this.unitList.Length == playerUnits.Length;
  }

  public IEnumerator UpdateAllPage(
    PlayerUnit[] playerUnits,
    bool limitMode,
    bool isMaterial = false,
    bool isMemory = false)
  {
    this.IsLimitMode = limitMode | isMemory;
    this.IsMaterial = isMaterial | isMemory || this.IsFriend;
    this.IsMemory = isMemory;
    this.unitList = playerUnits;
    this.setEnabledArrowAnimation(true);
    int num = this.CurrentIndex - 1 < 0 ? 0 : this.CurrentIndex - 1;
    int end = this.CurrentIndex + 1 >= ((IEnumerable<PlayerUnit>) this.unitList).Count<PlayerUnit>() ? ((IEnumerable<PlayerUnit>) this.unitList).Count<PlayerUnit>() - 1 : this.CurrentIndex + 1;
    for (int i = num; i <= end; ++i)
    {
      IEnumerator e = this.UpdatePage(i);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
  }

  public IEnumerator UpdatePage(int unitIdx)
  {
    Unit0042Menu menu = this;
    menu.CreateFirstFavoriteSetting();
    foreach (Transform transform in menu.scrollView.grid.transform.GetChildren().ToList<Transform>())
    {
      DetailMenuPrefab script = (DetailMenuPrefab) null;
      DetailMenuPrefab prefab = menu.detailMenuPrefabDict[transform.gameObject];
      if (prefab.Index == unitIdx)
        script = prefab;
      else if (prefab.Index >= 0 && prefab.PlayerUnit.unit.IsNormalUnit && (prefab.normal.EquippedGearSlot1 != menu.unitList[prefab.Index].equippedGear || prefab.normal.ExtraSkill() != menu.unitList[prefab.Index].equippedExtraSkill))
        script = prefab;
      if ((UnityEngine.Object) script != (UnityEngine.Object) null)
      {
        DateTime serverTime = ServerTime.NowAppTime();
        QuestScoreBonusTimetable[] scoreBonusTimetableArray = SMManager.Get<QuestScoreBonusTimetable[]>();
        QuestScoreBonusTimetable[] tables = scoreBonusTimetableArray != null ? ((IEnumerable<QuestScoreBonusTimetable>) scoreBonusTimetableArray).Where<QuestScoreBonusTimetable>((Func<QuestScoreBonusTimetable, bool>) (x => x.start_at < serverTime && x.end_at > serverTime)).ToArray<QuestScoreBonusTimetable>() : new QuestScoreBonusTimetable[0];
        UnitBonus[] activeUnitBonus = UnitBonus.getActiveUnitBonus(serverTime, new int?(), new int?());
        IEnumerator e;
        if (prefab.normal.EquippedGearSlot1 != menu.unitList[prefab.Index].equippedGear || prefab.normal.ExtraSkill() != menu.unitList[prefab.Index].equippedExtraSkill)
        {
          e = script.Init(menu, script.Index, menu.unitList[script.Index], menu.InfoIndex, menu.IsLimitMode, tables, activeUnitBonus, true, menu.IsMaterial);
          while (e.MoveNext())
            yield return e.Current;
          e = (IEnumerator) null;
          script.transform.localPosition = new Vector3(menu.scrollView.grid.cellWidth * (float) prefab.Index, 0.0f, 0.0f);
        }
        else
        {
          e = script.Init(menu, unitIdx, menu.unitList[unitIdx], menu.InfoIndex, menu.IsLimitMode, tables, activeUnitBonus, true, menu.IsMaterial);
          while (e.MoveNext())
            yield return e.Current;
          e = (IEnumerator) null;
          script.transform.localPosition = new Vector3(menu.scrollView.grid.cellWidth * (float) unitIdx, 0.0f, 0.0f);
        }
      }
      script = (DetailMenuPrefab) null;
      prefab = (DetailMenuPrefab) null;
    }
  }

  private IEnumerator UnitFavoriteAscync(
    int[] player_unit_ids,
    int[] unlock_player_unit_ids)
  {
    Singleton<CommonRoot>.GetInstance().loadingMode = 1;
    IEnumerator e1 = WebAPI.UnitFavorite(player_unit_ids, unlock_player_unit_ids, (System.Action<WebAPI.Response.UserError>) (e => WebAPI.DefaultUserErrorCallback(e))).Wait();
    while (e1.MoveNext())
      yield return e1.Current;
    e1 = (IEnumerator) null;
    Singleton<CommonRoot>.GetInstance().loadingMode = 0;
  }

  private IEnumerator UnitFavoritetReservesAscync(
    int[] player_unit_ids,
    int[] unlock_player_unit_ids)
  {
    Singleton<CommonRoot>.GetInstance().loadingMode = 1;
    IEnumerator e1 = WebAPI.UnitReservesFavorite(player_unit_ids, unlock_player_unit_ids, (System.Action<WebAPI.Response.UserError>) (e =>
    {
      WebAPI.DefaultUserErrorCallback(e);
      Singleton<CommonRoot>.GetInstance().loadingMode = 0;
    })).Wait();
    while (e1.MoveNext())
      yield return e1.Current;
    e1 = (IEnumerator) null;
    Singleton<CommonRoot>.GetInstance().loadingMode = 0;
  }

  public IEnumerator onEndSceneAsync()
  {
    List<int> intList1 = new List<int>();
    List<int> intList2 = new List<int>();
    foreach (KeyValuePair<int, bool> keyValuePair in this.firstSetting)
    {
      if (this.changeSetting[keyValuePair.Key] != keyValuePair.Value)
      {
        if (this.changeSetting[keyValuePair.Key])
          intList1.Add(keyValuePair.Key);
        else
          intList2.Add(keyValuePair.Key);
      }
    }
    IEnumerator e;
    if (intList1.Count > 0 || intList2.Count > 0)
    {
      if (this.IsStorage)
      {
        e = this.UnitFavoritetReservesAscync(intList1.ToArray(), intList2.ToArray());
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
      }
      else
      {
        e = this.UnitFavoriteAscync(intList1.ToArray(), intList2.ToArray());
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
      }
    }
    NGSoundManager instance = Singleton<NGSoundManager>.GetInstance();
    if ((UnityEngine.Object) instance != (UnityEngine.Object) null)
      instance.stopVoice(-1);
  }

  private void DisplayGroupLogo(UnitUnit playerUnit)
  {
    if (this.isEarthMode || this.groupSprites == null || this.groupSprites.Length == 0)
      return;
    int index = 0;
    foreach (Component groupSprite in this.groupSprites)
      groupSprite.gameObject.SetActive(false);
    UnitGroup groupInfo = Array.Find<UnitGroup>(MasterData.UnitGroupList, (Predicate<UnitGroup>) (x => x.unit_id == playerUnit.ID));
    if (groupInfo != null)
    {
      UnitGroupLargeCategory groupLargeCategory = ((IEnumerable<UnitGroupLargeCategory>) MasterData.UnitGroupLargeCategoryList).FirstOrDefault<UnitGroupLargeCategory>((Func<UnitGroupLargeCategory, bool>) (x => x.ID == groupInfo.group_large_category_id_UnitGroupLargeCategory));
      UnitGroupSmallCategory groupSmallCategory = ((IEnumerable<UnitGroupSmallCategory>) MasterData.UnitGroupSmallCategoryList).FirstOrDefault<UnitGroupSmallCategory>((Func<UnitGroupSmallCategory, bool>) (x => x.ID == groupInfo.group_small_category_id_UnitGroupSmallCategory));
      UnitGroupClothingCategory clothingCategory1 = ((IEnumerable<UnitGroupClothingCategory>) MasterData.UnitGroupClothingCategoryList).FirstOrDefault<UnitGroupClothingCategory>((Func<UnitGroupClothingCategory, bool>) (x => x.ID == groupInfo.group_clothing_category_id_UnitGroupClothingCategory));
      UnitGroupClothingCategory clothingCategory2 = ((IEnumerable<UnitGroupClothingCategory>) MasterData.UnitGroupClothingCategoryList).FirstOrDefault<UnitGroupClothingCategory>((Func<UnitGroupClothingCategory, bool>) (x => x.ID == groupInfo.group_clothing_category_id_2_UnitGroupClothingCategory));
      UnitGroupGenerationCategory generationCategory = ((IEnumerable<UnitGroupGenerationCategory>) MasterData.UnitGroupGenerationCategoryList).FirstOrDefault<UnitGroupGenerationCategory>((Func<UnitGroupGenerationCategory, bool>) (x => x.ID == groupInfo.group_generation_category_id_UnitGroupGenerationCategory));
      if (groupLargeCategory == null && groupSmallCategory == null && (clothingCategory1 == null && clothingCategory2 == null) && generationCategory == null)
        return;
      if (groupLargeCategory != null && groupLargeCategory.ID != 1)
      {
        this.groupSprites[index].SetSpriteName<string>(groupLargeCategory.GetSpriteName(), true);
        this.groupSprites[index].gameObject.SetActive(true);
        ++index;
      }
      if (groupSmallCategory != null && groupSmallCategory.ID != 1)
      {
        this.groupSprites[index].SetSpriteName<string>(groupSmallCategory.GetSpriteName(), true);
        this.groupSprites[index].gameObject.SetActive(true);
        ++index;
      }
      if (clothingCategory1 != null && clothingCategory1.ID != 1)
      {
        this.groupSprites[index].SetSpriteName<string>(clothingCategory1.GetSpriteName(), true);
        this.groupSprites[index].gameObject.SetActive(true);
        ++index;
      }
      if (clothingCategory2 != null && clothingCategory2.ID != 1)
      {
        this.groupSprites[index].SetSpriteName<string>(clothingCategory2.GetSpriteName(), true);
        this.groupSprites[index].gameObject.SetActive(true);
        ++index;
      }
      if (generationCategory != null && generationCategory.ID != 1)
      {
        this.groupSprites[index].SetSpriteName<string>(generationCategory.GetSpriteName(), true);
        this.groupSprites[index].gameObject.SetActive(true);
        ++index;
      }
    }
    if (!((UnityEngine.Object) this.slc_GroupBase != (UnityEngine.Object) null))
      return;
    this.dispGroupCount = index;
    this.tween_GroupBaseOpen.to = (int) this.getGroupHeightTarget();
    this.tween_GroupBaseClose.from = (int) this.getGroupHeightTarget();
    this.tween_GroupSpriteDirOpen.to.y = this.getGroupPositionTarget();
    this.tween_GroupSpriteDirOpen.from.y = this.getGroupPositionInit();
    this.tween_GroupSpriteDirClose.to.y = this.getGroupPositionInit();
    this.tween_GroupSpriteDirClose.from.y = this.getGroupPositionTarget();
    this.setGroupPos();
    if (this.dispGroupCount != 0)
      return;
    this.IbtnGroupTabIdle.gameObject.SetActive(false);
    this.dir_GroupPressed.SetActive(false);
  }

  public void IbtnFavoriteToggle()
  {
    this.SetFavorite(!this.isFavorited);
  }

  public void SetFavorite(bool active)
  {
    this.isFavorited = active;
    this.btnFavoritedOff.SetActive(!active);
    this.btnFavoritedOn.SetActive(active);
    this.UpdateSetting(this.unitList[this.CurrentIndex].id, this.isFavorited);
  }

  public void IbtnGroupOpen()
  {
    if (this.isGroupTween || this.isGroupOpen)
      return;
    this.IbtnGroupTabIdle.gameObject.SetActive(false);
    this.dir_GroupPressed.SetActive(true);
    NGTween.playTweens(this.GetComponentsInChildren<UITweener>(true), 100, false);
    this.isGroupOpen = true;
    this.isGroupTween = true;
  }

  public void onFinishedGroupOpen()
  {
    this.isGroupTween = false;
  }

  public void IbtnGroupClose()
  {
    if (this.isGroupTween || !this.isGroupOpen)
      return;
    NGTween.playTweens(this.GetComponentsInChildren<UITweener>(true), 101, false);
    this.isGroupOpen = false;
    this.isGroupTween = true;
  }

  public void onFinishedGroupClose()
  {
    if (!this.isGroupTween)
      return;
    if (this.dispGroupCount > 0)
      this.IbtnGroupTabIdle.gameObject.SetActive(true);
    else
      this.IbtnGroupTabIdle.gameObject.SetActive(false);
    this.dir_GroupPressed.SetActive(false);
    this.isGroupTween = false;
  }

  private void setGroupPos()
  {
    if (this.isGroupOpen)
    {
      this.slc_GroupBase.GetComponent<UISprite>().height = (int) this.getGroupHeightTarget();
      this.dir_GroupSprite.transform.localPosition = new Vector3(this.dir_GroupSprite.transform.localPosition.x, this.getGroupPositionTarget(), this.dir_GroupSprite.transform.localPosition.z);
      this.IbtnGroupTabIdle.gameObject.SetActive(false);
      this.dir_GroupPressed.SetActive(true);
    }
    else
    {
      this.slc_GroupBase.GetComponent<UISprite>().height = this.groupBaseHeightInit;
      this.dir_GroupSprite.transform.localPosition = new Vector3(this.dir_GroupSprite.transform.localPosition.x, this.getGroupPositionInit(), this.dir_GroupSprite.transform.localPosition.z);
      this.IbtnGroupTabIdle.gameObject.SetActive(true);
      this.dir_GroupPressed.SetActive(false);
    }
    this.isGroupTween = false;
  }

  private float getGroupHeightTarget()
  {
    return (float) ((double) this.groupBaseHeightInit + (double) this.groupSpriteHeight * (double) this.dispGroupCount + 8.0);
  }

  private float getGroupPositionTarget()
  {
    return 0.0f;
  }

  private float getGroupPositionInit()
  {
    return (float) (-(double) this.groupSpriteHeight * (double) this.dispGroupCount - 8.0);
  }

  private bool CenterOnChild(int num, bool disabledAnime = false)
  {
    if (num < 0)
      return false;
    foreach (KeyValuePair<GameObject, DetailMenuPrefab> keyValuePair in this.detailMenuPrefabDict)
    {
      if (keyValuePair.Value.Index == num)
      {
        if (disabledAnime)
          this.resetScrollViewPosition(keyValuePair.Key.transform.localPosition);
        this.centerOnChild.onFinished = new SpringPanel.OnFinished(this.OnCocFinished);
        this.centerOnChild.CenterOn(keyValuePair.Key.transform);
        UIScrollView componentInChildren = this.centerOnChild.centeredObject.GetComponentInChildren<UIScrollView>();
        if ((UnityEngine.Object) componentInChildren != (UnityEngine.Object) null)
          componentInChildren.enabled = false;
        return true;
      }
    }
    return false;
  }

  private void resetScrollViewPosition(Vector3 target)
  {
    this.scrollView.scrollView.ResetHorizontalCenter(target.x);
  }

  public void onEndScene()
  {
    this.setEnabledArrowAnimation(false);
    this.scrollView.scrollView.Press(false);
  }

  public void onBackScene()
  {
    this.setEnabledArrowAnimation(true);
  }

  private void setEnabledArrowAnimation(bool bEnabled)
  {
    TweenAlpha component1 = this.LeftArrow.GetComponent<TweenAlpha>();
    if ((UnityEngine.Object) component1 != (UnityEngine.Object) null)
    {
      component1.enabled = bEnabled;
      if (!bEnabled)
        component1.value = 0.0f;
    }
    TweenAlpha component2 = this.RightArrow.GetComponent<TweenAlpha>();
    if (!((UnityEngine.Object) component2 != (UnityEngine.Object) null))
      return;
    component2.enabled = bEnabled;
    if (bEnabled)
      return;
    component2.value = 0.0f;
  }
}
