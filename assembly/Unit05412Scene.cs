﻿// Decompiled with JetBrains decompiler
// Type: Unit05412Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using Earth;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;

public class Unit05412Scene : NGSceneBase
{
  private bool isInit = true;
  public Unit05412Menu menu;

  public static void ChangeScene(bool stack)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("unit054_12", stack, (object[]) Array.Empty<object>());
  }

  public IEnumerator onStartSceneAsync()
  {
    PlayerUnit[] array = ((IEnumerable<PlayerUnit>) Singleton<EarthDataManager>.GetInstance().GetPlayerUnits()).Where<PlayerUnit>((Func<PlayerUnit, bool>) (x => !x.unit.IsMaterialUnit)).ToArray<PlayerUnit>();
    Player player = SMManager.Get<Player>();
    IEnumerator e;
    if (!this.isInit)
    {
      e = this.menu.UpdateInfoAndScroll(array, (PlayerMaterialUnit[]) null);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    else
    {
      e = this.menu.Init(player, array, true, false);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.isInit = false;
    }
  }
}
