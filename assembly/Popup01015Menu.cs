﻿// Decompiled with JetBrains decompiler
// Type: Popup01015Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;
using UnityEngine;

public class Popup01015Menu : BackButtonMenuBase
{
  public UILabel TextDescription;
  private Setting01013Menu menu01013;
  private string newName;

  public IEnumerator Init(Setting01013Menu menu, string name)
  {
    this.menu01013 = menu;
    this.newName = name;
    this.TextDescription.SetText(Consts.Format(Consts.GetInstance().POPUP_01015_DESCRIPTION, (IDictionary) new Hashtable()
    {
      {
        (object) nameof (name),
        (object) name
      }
    }));
    yield break;
  }

  private IEnumerator Popup01017()
  {
    Future<GameObject> popupPrefabF = Res.Prefabs.popup.popup_010_1_7__anim_popup01.Load<GameObject>();
    IEnumerator e = popupPrefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Singleton<PopupManager>.GetInstance().open(popupPrefabF.Result, false, false, false, true, false, false, "SE_1006");
  }

  private IEnumerator Popup01018()
  {
    Future<GameObject> popupPrefabF = Res.Prefabs.popup.popup_010_1_8__anim_popup01.Load<GameObject>();
    IEnumerator e = popupPrefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Singleton<PopupManager>.GetInstance().open(popupPrefabF.Result, false, false, false, true, false, false, "SE_1006");
  }

  private IEnumerator BtnYes()
  {
    Popup01015Menu popup01015Menu = this;
    Singleton<PopupManager>.GetInstance().onDismiss();
    Singleton<CommonRoot>.GetInstance().loadingMode = 1;
    // ISSUE: reference to a compiler-generated method
    Future<WebAPI.Response.PlayerNameEdit> result = WebAPI.PlayerNameEdit(popup01015Menu.newName, new System.Action<WebAPI.Response.UserError>(popup01015Menu.\u003CBtnYes\u003Eb__6_0));
    IEnumerator e = result.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if (result.Result != null)
    {
      Future<GameObject> popupPrefabF = Res.Prefabs.popup.popup_010_1_6__anim_popup01.Load<GameObject>();
      e = popupPrefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      Popup01016Menu component = Singleton<PopupManager>.GetInstance().open(popupPrefabF.Result, false, false, false, true, false, false, "SE_1006").GetComponent<Popup01016Menu>();
      popup01015Menu.StartCoroutine(component.Init(popup01015Menu.menu01013, result.Result.player.name));
      Singleton<CommonRoot>.GetInstance().loadingMode = 0;
    }
  }

  public void IbtnYes()
  {
    Singleton<PopupManager>.GetInstance().onDismiss();
    this.StartCoroutine(this.BtnYes());
  }

  public void IbtnNo()
  {
    Singleton<PopupManager>.GetInstance().onDismiss();
  }

  public override void onBackButton()
  {
    this.IbtnNo();
  }
}
