﻿// Decompiled with JetBrains decompiler
// Type: Unit0541Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Unit0541Menu : BackButtonMenuBase
{
  [SerializeField]
  private UIButton evolutionButton;

  public IEnumerator InitSceneAsync()
  {
    if (((IEnumerable<UnitEvolutionPattern>) MasterData.UnitEvolutionPatternList).Count<UnitEvolutionPattern>() <= 0)
    {
      this.evolutionButton.isEnabled = false;
    }
    else
    {
      this.evolutionButton.isEnabled = true;
      yield break;
    }
  }

  public IEnumerator StartSceneAsync()
  {
    yield break;
  }

  public override void onBackButton()
  {
    Singleton<NGSceneManager>.GetInstance().clearStack();
    Singleton<NGSceneManager>.GetInstance().destroyCurrentScene();
    Singleton<NGSceneManager>.GetInstance().changeScene("mypage051", false, (object[]) Array.Empty<object>());
  }

  public void onOrderClick()
  {
    Unit05468Scene.ChangeScene(true);
  }

  public void onEvolutionClick()
  {
    Unit0549Scene.ChangeScene(true);
  }

  public void onEquipClick()
  {
    Unit05412Scene.ChangeScene(true);
  }

  public void onListClick()
  {
    Unit05411Scene.ChangeScene(true);
  }
}
