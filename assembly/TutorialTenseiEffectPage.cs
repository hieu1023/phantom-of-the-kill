﻿// Decompiled with JetBrains decompiler
// Type: TutorialTenseiEffectPage
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;
using System;
using System.Collections;
using System.Collections.Generic;

public class TutorialTenseiEffectPage : TutorialPageBase
{
  private const int REWARD_TYPE_UNIT = 1;

  public override IEnumerator Show()
  {
    WebAPI.Response.TutorialTutorialRagnarokResume resume = Singleton<TutorialRoot>.GetInstance().Resume;
    PlayerUnit transmigratePlayerUnit = resume.after_transmigrate_player_unit;
    unit00497Scene.ChangeScene(false, new PrincesEvolutionParam()
    {
      materiaqlUnits = new List<PlayerUnit>()
      {
        PlayerUnit.CreateByPlayerMaterialUnit(resume.player_material_units[0], 1)
      },
      is_new = false,
      baseUnit = resume.after_levelup1_player_unit,
      resultUnit = transmigratePlayerUnit,
      mode = Unit00499Scene.Mode.Transmigration
    });
    Singleton<NGSceneManager>.GetInstance().destroyScene("unit004_9_9");
    yield break;
  }

  public void OnTenseiFinish()
  {
    this.StartCoroutine(this.finish());
  }

  private IEnumerator finish()
  {
    TutorialTenseiEffectPage tenseiEffectPage = this;
    NGSceneManager sceneManager = Singleton<NGSceneManager>.GetInstance();
    sceneManager.destroyLoadedScenes();
    sceneManager.changeScene("empty", false, (object[]) Array.Empty<object>());
    while (!sceneManager.isSceneInitialized)
      yield return (object) null;
    Singleton<PopupManager>.GetInstance().closeAll(false);
    tenseiEffectPage.NextPage();
  }
}
