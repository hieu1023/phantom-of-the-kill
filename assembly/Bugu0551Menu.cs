﻿// Decompiled with JetBrains decompiler
// Type: Bugu0551Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;

public class Bugu0551Menu : BackButtonMenuBase
{
  public IEnumerator InitSceneAsync()
  {
    yield break;
  }

  public IEnumerator StartSceneAsync()
  {
    yield break;
  }

  public override void onBackButton()
  {
    Singleton<NGSceneManager>.GetInstance().clearStack();
    Singleton<NGSceneManager>.GetInstance().destroyCurrentScene();
    Singleton<NGSceneManager>.GetInstance().changeScene("mypage051", false, (object[]) Array.Empty<object>());
  }

  public void onListClick()
  {
    Bugu0552Scene.ChangeScene(true);
  }

  public void onSaleClick()
  {
    Bugu055SellScene.ChangeScene(true);
  }
}
