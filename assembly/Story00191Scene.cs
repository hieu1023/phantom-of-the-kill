﻿// Decompiled with JetBrains decompiler
// Type: Story00191Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

public class Story00191Scene : NGSceneBase
{
  private bool flg_init = true;
  [SerializeField]
  private NGxScroll ScrollContainer;
  [SerializeField]
  private Story00191Menu menu;
  [SerializeField]
  private GameObject IbtnCopyRight;
  [SerializeField]
  private GameObject IbtnColabo;
  [SerializeField]
  private GameObject IbtnMigrate;
  [SerializeField]
  private GameObject IbtnHintsAndTips;
  [SerializeField]
  private GameObject LobiButton;
  [SerializeField]
  private GameObject AchievementsButton;
  [SerializeField]
  private UISprite AchievementsBtnAndroidSprite;

  public override IEnumerator onInitSceneAsync()
  {
    this.LobiButton.SetActive(false);
    this.AchievementsButton.SetActive(false);
    this.IbtnHintsAndTips.SetActive(false);
    this.menu.showBtnCredit(false);
    this.menu.InitAsync();
    this.ScrollContainer.ResolvePosition();
    IEnumerator e = this.menu.onInitSceneAsync();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public void onStartScene()
  {
    if (this.flg_init)
    {
      this.ScrollContainer.ResolvePosition();
      this.flg_init = false;
    }
    this.menu.Init();
    Singleton<CommonRoot>.GetInstance().isLoading = false;
  }
}
