﻿// Decompiled with JetBrains decompiler
// Type: MypageMenuBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class MypageMenuBase : BackButtonMenuBase
{
  protected static List<int> playTouchVoiceList = new List<int>();
  private static readonly int INFO_READ_JUDGE_NUM = 3;
  public static readonly int START_TWEENGROUP = 22;
  public static readonly int END_TWEENGROUP = 33;
  public static readonly int START_TWEENGROUP_TOP = 50;
  public static readonly int END_TWEENGROUP_TOP = 51;
  public static readonly int START_TWEEN_GROUP_JOGDIAL = 122;
  public static readonly int END_TWEEN_GROUP_JOGDIAL = 133;
  public static readonly int HEAVEN_OUT_BG_TWEENGROUP = 151;
  public static readonly int HEAVEN_IN_BG_TWEENGROUP = 152;
  public static readonly int EARTH_OUT_BG_TWEENGROUP = 153;
  public static readonly int EARTH_IN_BG_TWEENGROUP = 150;
  protected string tweenName = string.Empty;
  public static int LeaderID;
  [NonSerialized]
  public NGSoundManager sm;
  [NonSerialized]
  public GameObject CharaAnimObj;
  [NonSerialized]
  public UI2DSprite[] CharaSprites;
  [NonSerialized]
  public UILabel[] CharaName;
  [NonSerialized]
  public UILabel _chara_anim_english_name;
  [NonSerialized]
  public Animator CharaAnimator;
  [NonSerialized]
  public bool CharaAnimation;
  [NonSerialized]
  public int ButtonCount;
  [NonSerialized]
  public int ButtonTweenFinishedCount;
  [SerializeField]
  private BannersProc BannerParent;
  [SerializeField]
  private GameObject FriendBadge;
  [SerializeField]
  private GameObject InfoNew;
  [SerializeField]
  private GameObject PresentNew;
  [SerializeField]
  private UILabel _lbl_present_num;
  [SerializeField]
  private GameObject PanelMissionBtn;
  [SerializeField]
  private GameObject PanelMissionClear;
  [SerializeField]
  private MypagePanelMissionInfo NextPanelMission;
  [SerializeField]
  private GameObject DirPanelMissionBtn;
  [SerializeField]
  private GameObject DirPanelMissionEffect;
  [SerializeField]
  private UIButton ExploreBtn;
  [SerializeField]
  private GameObject ExploreProgressMaxBadge;
  [SerializeField]
  public GameObject slc_Circle_Base;
  [SerializeField]
  public GameObject dir_Select;
  [SerializeField]
  public GameObject dir_Roulette;
  [SerializeField]
  public Transform mainMenuEffectPosition;
  [SerializeField]
  public UITexture mainMenuEffectTexture;
  [SerializeField]
  public Camera mainMenuEffectCamera;
  [SerializeField]
  public Transform mainMenuEffectParticlePosition;
  [SerializeField]
  protected MypageSlidePanelDragged Story_Container;
  [SerializeField]
  private MypageSlidePanelDragged Character_Container;
  [SerializeField]
  protected MypageSlidePanelDragged Event_Container;
  [SerializeField]
  private MypageSlidePanelDragged Colosseum_Container;
  [SerializeField]
  private MypageSlidePanelDragged Multi_Container;
  [SerializeField]
  private GameObject OpenEffectObject;
  [SerializeField]
  private const float MoviePlayTime = 1.5f;
  [SerializeField]
  private UI2DSprite slc_story_start;
  [SerializeField]
  private GameObject bannerParent;
  [SerializeField]
  public MypageEventButtonController EventButtonController;
  protected int nextPlayIndex;
  private ulong? characterId;
  private int LoginBonusCloseCounter;
  private int LevelUpBonusCloseCounter;
  private int LevelUpBonusCount;
  private GameObject LoginPopup;
  public Transform CharaAnimTrans;
  public GameObject NotTouch;
  public CircularMotionPositionSet CirculButton;
  public UIPanel MainPanel;
  public MypageMissionClearInfo MissionClearInfo;
  private const int SHIFT_JOB_ID = 32;

  public int SetLoginBonusCloseCounter
  {
    set
    {
      this.LoginBonusCloseCounter = value;
    }
  }

  public float CharaMovieTime
  {
    get
    {
      return 1.5f;
    }
  }

  public bool isAnimePlaying
  {
    get
    {
      return (UnityEngine.Object) this.CharaAnimator != (UnityEngine.Object) null && this.CharaAnimation && (double) this.CharaAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime < 1.0;
    }
  }

  public bool LoginPopupActive()
  {
    return !((UnityEngine.Object) this.LoginPopup == (UnityEngine.Object) null);
  }

  public virtual IEnumerator InitSceneAsync()
  {
    IEnumerator e = this.InitOne();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.sm = Singleton<NGSoundManager>.GetInstance();
    e = this.SetMainMenuEffect();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public virtual IEnumerator StartSceneAsync(
    bool isCloudAnim,
    bool isReservedEventScript)
  {
    MypageMenuBase mypageMenuBase = this;
    Singleton<NGGameDataManager>.GetInstance().IsColosseum = false;
    mypageMenuBase.CharaAnimation = false;
    mypageMenuBase.ButtonCount = 0;
    mypageMenuBase.ButtonTweenFinishedCount = 0;
    mypageMenuBase.TweenInit();
    IEnumerator e;
    if ((UnityEngine.Object) mypageMenuBase.BannerParent != (UnityEngine.Object) null)
    {
      e = mypageMenuBase.BannerParent.BannerCreate(isCloudAnim, (System.Action) null, true);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    e = mypageMenuBase.SetStoryDecorateImage();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    mypageMenuBase.JogDialSetting();
    e = mypageMenuBase.CharcterAnimationSetting(isCloudAnim, MypageMenuBase.HEAVEN_IN_BG_TWEENGROUP);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    mypageMenuBase.PresentIconSetting();
    mypageMenuBase.InfoIconSetting();
    mypageMenuBase.FriendBadgeSetting();
    e = mypageMenuBase.PanelMissionIconSetting();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if (!isReservedEventScript)
    {
      e = mypageMenuBase.MissionClearInfoSetting();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    Singleton<CommonRoot>.GetInstance().UpdateFooterGachaButton();
    mypageMenuBase.StartCoroutine(Singleton<CommonRoot>.GetInstance().UpdateFooterLimitedShopButton());
    Singleton<CommonRoot>.GetInstance().UpdateHeaderBikkuriIcon();
    Singleton<CommonRoot>.GetInstance().UpdateFooterBikkuriIcon();
    Singleton<CommonRoot>.GetInstance().UpdateFooterNewbiePacksIcon();
    mypageMenuBase.EventButtonController.UpdateButtonState();
    Animator component1 = Singleton<CommonRoot>.GetInstance().GetComponent<Animator>();
    if ((bool) (UnityEngine.Object) component1)
    {
      AnimatorStateInfo animatorStateInfo = component1.GetCurrentAnimatorStateInfo(0);
      component1.Play(animatorStateInfo.fullPathHash, 0, 0.0f);
    }
    Animator component2 = mypageMenuBase.GetComponent<Animator>();
    if ((bool) (UnityEngine.Object) component2)
    {
      AnimatorStateInfo animatorStateInfo = component2.GetCurrentAnimatorStateInfo(0);
      component2.Play(animatorStateInfo.fullPathHash, 0, 0.0f);
    }
  }

  public IEnumerator SetMainMenuEffect()
  {
    this.mainMenuEffectCamera.gameObject.SetActive(true);
    Future<GameObject> dirEffectF = Res.Animations.mypage.dir_mainmenu_effect.Load<GameObject>();
    IEnumerator e = dirEffectF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    dirEffectF.Result.Clone(this.mainMenuEffectPosition);
    this.mainMenuEffectCamera.targetTexture = new RenderTexture(512, 256, -5);
    this.mainMenuEffectCamera.targetTexture.antiAliasing = 1;
    this.mainMenuEffectCamera.targetTexture.wrapMode = TextureWrapMode.Clamp;
    this.mainMenuEffectCamera.targetTexture.filterMode = FilterMode.Bilinear;
    this.mainMenuEffectCamera.targetTexture.enableRandomWrite = false;
    this.mainMenuEffectCamera.farClipPlane = 1000f;
    this.mainMenuEffectTexture.mainTexture = (Texture) this.mainMenuEffectCamera.targetTexture;
    Future<GameObject> mainMenuEffectGlowF = Res.Animations.mypage.Particle_mainmenu_glow.Load<GameObject>();
    e = mainMenuEffectGlowF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    mainMenuEffectGlowF.Result.Clone(this.mainMenuEffectParticlePosition);
  }

  public override void onBackButton()
  {
    if (this.IsPushAndSet())
      return;
    this.StartCoroutine(this.backPopoup());
  }

  private IEnumerator backPopoup()
  {
    Future<GameObject> prefabF = Singleton<ResourceManager>.GetInstance().Load<GameObject>("Prefabs/popup/popup_gameend__anim_popup01", 1f);
    IEnumerator e = prefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Singleton<PopupManager>.GetInstance().open(prefabF.Result, false, false, false, true, false, false, "SE_1006").GetComponent<PopupTitleBack>().Init();
  }

  protected void TweenInit()
  {
    this.NotTouch.SetActive(true);
    ((IEnumerable<UITweener>) this.GetComponentsInChildren<UITweener>(true)).ForEach<UITweener>((System.Action<UITweener>) (x =>
    {
      x.onFinished.Clear();
      x.enabled = false;
    }));
  }

  protected virtual IEnumerator InitOne()
  {
    MypageMenuBase mypageMenuBase = this;
    if (!((UnityEngine.Object) mypageMenuBase.CharaAnimObj != (UnityEngine.Object) null))
    {
      IEnumerator e = mypageMenuBase.CreateCharcterAnimation();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      e = mypageMenuBase.CreateJogDial();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      e = mypageMenuBase.CreateLoginPopup();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      e = mypageMenuBase.CreateMissionClearInfo();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      mypageMenuBase.BackBtnEnable = false;
    }
  }

  protected virtual void JogDialSetting()
  {
    this.CirculButton.Init(this.Story_Container);
  }

  public PlayerUnit getDeckLeaderPlayerUnit()
  {
    PlayerDeck[] playerDeckArray = SMManager.Get<PlayerDeck[]>();
    PlayerUnit playerUnit = ((IEnumerable<PlayerUnit>) playerDeckArray[Persist.deckOrganized.Data.number].player_units).FirstOrDefault<PlayerUnit>();
    if (playerUnit == (PlayerUnit) null)
      playerUnit = ((IEnumerable<PlayerUnit>) playerDeckArray[0].player_units).First<PlayerUnit>();
    return playerUnit;
  }

  public PlayerUnit getDisplayPlayerUnit()
  {
    int mypage_unit_id = MypageUnitUtil.getUnitId();
    if (mypage_unit_id == 0)
      return this.getDeckLeaderPlayerUnit();
    PlayerUnit playerUnit = ((IEnumerable<PlayerUnit>) SMManager.Get<PlayerUnit[]>()).FirstOrDefault<PlayerUnit>((Func<PlayerUnit, bool>) (x => x.id == mypage_unit_id));
    if (!(playerUnit == (PlayerUnit) null))
      return playerUnit;
    MypageUnitUtil.setDefaultUnitNotFound();
    return this.getDeckLeaderPlayerUnit();
  }

  protected virtual IEnumerator CharcterAnimationSetting(bool isCloudAnim, int tweenID)
  {
    if ((UnityEngine.Object) this.CharaAnimObj != (UnityEngine.Object) null)
      this.CharaAnimObj.SetActive(false);
    PlayerUnit displayPlayerUnit = this.getDisplayPlayerUnit();
    UnitUnit unitUnit = displayPlayerUnit.unit;
    ulong charId = this.makeCharacterId(unitUnit.ID, displayPlayerUnit.job_id);
    if (this.characterId.HasValue)
    {
      long num = (long) charId;
      ulong? characterId = this.characterId;
      long valueOrDefault = (long) characterId.GetValueOrDefault();
      if (num == valueOrDefault & characterId.HasValue)
        goto label_14;
    }
    Future<UnityEngine.Sprite> LeaderF = unitUnit.LoadSpriteLarge(displayPlayerUnit.job_id, 1f);
    IEnumerator e = LeaderF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    for (int index = 0; index < this.CharaSprites.Length; ++index)
      this.CharaSprites[index].sprite2D = LeaderF.Result;
    for (int index = 0; index < this.CharaName.Length; ++index)
      this.CharaName[index].SetTextLocalize(unitUnit.name);
    this.characterId = new ulong?(charId);
    LeaderF = (Future<UnityEngine.Sprite>) null;
label_14:
    if (unitUnit.ID != MypageMenuBase.LeaderID)
    {
      MypageMenuBase.playTouchVoiceList.Clear();
      this.nextPlayIndex = 0;
      if (unitUnit.unitVoicePattern != null && this.sm.LoadVoiceData(unitUnit.unitVoicePattern.file_name))
      {
        while (!this.sm.LoadedCueSheet(unitUnit.unitVoicePattern.file_name))
          yield return (object) null;
        HashSet<int> self = new HashSet<int>();
        foreach (int enableId in UnitHomeVoicePattern.GetEnableIDList())
        {
          if (this.sm.ExistsCueID(unitUnit.unitVoicePattern.file_name, enableId))
            self.Add(enableId);
        }
        MypageMenuBase.playTouchVoiceList = self.Shuffle<int>().ToList<int>();
      }
    }
    float addDelayTime = this.SetCharacterAnimControllerWithGetAddDelayTime(unitUnit.ID != MypageMenuBase.LeaderID && !isCloudAnim, unitUnit.ID);
    if (isCloudAnim)
    {
      Singleton<CommonRoot>.GetInstance().StartBGTween(tweenID);
      this.AddStartTweenDelay(addDelayTime, MypageMenuBase.START_TWEENGROUP_TOP);
      this.AddStartTweenDelay(addDelayTime, MypageMenuBase.START_TWEEN_GROUP_JOGDIAL);
    }
    else
      this.AddStartTweenDelay(addDelayTime, MypageMenuBase.START_TWEENGROUP);
  }

  private ulong makeCharacterId(int unitId, int jobId)
  {
    return ((ulong) jobId << 32) + (ulong) unitId;
  }

  private void PresentIconSetting()
  {
    if ((UnityEngine.Object) this.PresentNew == (UnityEngine.Object) null)
      return;
    PlayerPresent[] playerPresentArray = SMManager.Get<PlayerPresent[]>();
    int num1 = 0;
    if (playerPresentArray != null)
    {
      foreach (PlayerPresent playerPresent in playerPresentArray)
      {
        if (!playerPresent.received_at.HasValue)
          ++num1;
      }
    }
    if (num1 > 0)
    {
      int num2 = num1;
      if (num2 > StaticConsts.PRESENT_COUNT_DISPLAY_MAX)
        num2 = StaticConsts.PRESENT_COUNT_DISPLAY_MAX;
      this.PresentNew.SetActive(true);
      if (!(bool) (UnityEngine.Object) this._lbl_present_num)
        return;
      this._lbl_present_num.SetTextLocalize(string.Format("[b]{0}[-]", (object) num2));
    }
    else
      this.PresentNew.SetActive(false);
  }

  private void InfoIconSetting()
  {
    if ((UnityEngine.Object) this.InfoNew == (UnityEngine.Object) null)
      return;
    OfficialInformationArticle[] informationArticleArray = SMManager.Get<OfficialInformationArticle[]>();
    if (informationArticleArray == null)
    {
      this.InfoNew.SetActive(false);
    }
    else
    {
      bool flag;
      try
      {
        flag = ((IEnumerable<OfficialInformationArticle>) informationArticleArray).Any<OfficialInformationArticle>((Func<OfficialInformationArticle, bool>) (w => !Persist.infoUnRead.Data.GetUnRead(w)));
      }
      catch
      {
        Persist.infoUnRead.Delete();
        flag = informationArticleArray != null && (uint) informationArticleArray.Length > 0U;
      }
      this.InfoNew.SetActive(flag);
    }
  }

  private IEnumerator PanelMissionIconSetting()
  {
    if (!((UnityEngine.Object) this.PanelMissionBtn == (UnityEngine.Object) null) && !((UnityEngine.Object) this.PanelMissionClear == (UnityEngine.Object) null) && !((UnityEngine.Object) this.NextPanelMission == (UnityEngine.Object) null))
    {
      Player player = SMManager.Get<Player>();
      if (player.next_panel_mission_id > 0 && MasterData.BingoMission.ContainsKey(player.next_panel_mission_id))
      {
        this.NextPanelMission.gameObject.SetActive(true);
        IEnumerator e = this.NextPanelMission.InitPanelMissionInfo(player.next_panel_mission_id);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
      }
      else
        this.NextPanelMission.gameObject.SetActive(false);
      this.PanelMissionClear.SetActive(player.is_open_bingo);
      this.DirPanelMissionBtn.SetActive(!player.is_bingo_end);
      this.DirPanelMissionEffect.SetActive(!player.is_bingo_end);
      this.PanelMissionBtn.SetActive(!player.is_bingo_end);
    }
  }

  private void FriendBadgeSetting()
  {
    if ((UnityEngine.Object) this.FriendBadge == (UnityEngine.Object) null)
      return;
    this.FriendBadge.SetActive(Singleton<NGGameDataManager>.GetInstance().ReceivedFriendRequestCount > 0);
  }

  public void ExploreButtonSetting()
  {
    this.ExploreBtn.isEnabled = true;
    this.ExploreProgressMaxBadge.SetActive(false);
    if (!Singleton<TutorialRoot>.GetInstance().IsTutorialFinish())
      return;
    ExploreDataManager instance = Singleton<ExploreDataManager>.GetInstance();
    if (instance.IsNotRegisteredDeck())
    {
      if (!(instance.LastSyncTime > ServerTime.NowAppTimeAddDelta()))
        return;
      this.ExploreBtn.isEnabled = false;
    }
    else if (instance.IsRankingAcceptanceFinish)
    {
      this.ExploreBtn.isEnabled = false;
    }
    else
    {
      if (!instance.NeedShowBadge)
        return;
      this.ExploreProgressMaxBadge.SetActive(true);
    }
  }

  public void ResetTweenDelay()
  {
    ((IEnumerable<UITweener>) this.GetComponentsInChildren<UITweener>(true)).ForEach<UITweener>((System.Action<UITweener>) (x =>
    {
      if (x.tweenGroup != MypageMenuBase.START_TWEENGROUP || (double) x.delay < (double) this.CharaMovieTime)
        return;
      x.delay -= this.CharaMovieTime;
    }));
  }

  protected float SetCharacterAnimControllerWithGetAddDelayTime(bool LeaderChange, int id)
  {
    float num;
    if (LeaderChange)
    {
      this.tweenName = "Play";
      num = 0.0f;
    }
    else
    {
      this.tweenName = "Fade";
      num = 0.0f;
    }
    this.CharaAnimation = true;
    MypageMenuBase.LeaderID = id;
    return num;
  }

  protected void AddStartTweenDelay(float addTime, int groupID)
  {
    List<MypageSlidePanelDragged> buttons = !((UnityEngine.Object) this.CirculButton != (UnityEngine.Object) null) ? new List<MypageSlidePanelDragged>() : this.CirculButton.GetTargets;
    ((IEnumerable<UITweener>) this.GetComponentsInChildren<UITweener>(true)).ForEach<UITweener>((System.Action<UITweener>) (x =>
    {
      if (x.tweenGroup != groupID)
        return;
      x.delay += addTime;
      EventDelegate.Add(x.onFinished, (EventDelegate.Callback) (() => x.delay -= addTime));
      foreach (Component component in buttons)
      {
        if ((UnityEngine.Object) component.transform == (UnityEngine.Object) x.gameObject.transform)
        {
          EventDelegate.Add(x.onFinished, (EventDelegate.Callback) (() => this.ButtonTweenFinished()));
          break;
        }
      }
    }));
    this.ButtonCount = buttons.Count;
  }

  public void LoginPopupStart(List<PlayerLoginBonus> loginBonus, int loginBonusCount)
  {
    this.MainPanel.alpha = 0.0f;
    Startup000144Menu component = Singleton<PopupManager>.GetInstance().open(this.LoginPopup, false, false, false, true, false, false, "SE_1006").GetComponent<Startup000144Menu>();
    component.InitScene(loginBonus[this.LoginBonusCloseCounter]);
    Singleton<CommonRoot>.GetInstance().isLoading = false;
    component.OnOkButton = (System.Action) (() =>
    {
      Singleton<PopupManager>.GetInstance().onDismiss();
      ++this.LoginBonusCloseCounter;
      if (loginBonusCount <= this.LoginBonusCloseCounter)
      {
        List<LevelRewardSchemaMixin> playerLevelRewards = Singleton<NGGameDataManager>.GetInstance().playerLevelRewards;
        if (playerLevelRewards != null && playerLevelRewards.Count > 0)
        {
          this.LevelUpPopupInit(playerLevelRewards);
          this.LevelUpPopupStart(playerLevelRewards);
        }
        else
        {
          this.MainPanel.alpha = 1f;
          this.CharaAnimProc();
          this.PlayTween(MypageMenuBase.START_TWEENGROUP);
        }
      }
      else
        this.LoginPopupStart(loginBonus, loginBonusCount);
    });
  }

  public IEnumerator DoLoginPopup(List<PlayerLoginBonus> loginBonus)
  {
    this.MainPanel.alpha = 0.0f;
    Singleton<CommonRoot>.GetInstance().isLoading = false;
    foreach (PlayerLoginBonus loginBonu in loginBonus)
    {
      bool bWait = true;
      Startup000144Menu component = Singleton<PopupManager>.GetInstance().open(this.LoginPopup, false, false, false, true, false, false, "SE_1006").GetComponent<Startup000144Menu>();
      component.InitScene(loginBonu);
      component.OnOkButton = (System.Action) (() =>
      {
        Singleton<PopupManager>.GetInstance().onDismiss();
        bWait = false;
      });
      while (bWait)
        yield return (object) null;
    }
    this.MainPanel.alpha = 1f;
  }

  public void LevelUpPopupInit(List<LevelRewardSchemaMixin> levelupBonus)
  {
    Singleton<CommonRoot>.GetInstance().isLoading = false;
    this.LevelUpBonusCount = levelupBonus.Count;
    this.LevelUpBonusCloseCounter = 0;
    this.MainPanel.alpha = 0.0f;
  }

  public void LevelUpPopupStart(List<LevelRewardSchemaMixin> levelupBonus)
  {
    ModalWindow.Show(levelupBonus[this.LevelUpBonusCloseCounter].reward_title, levelupBonus[this.LevelUpBonusCloseCounter].reward_message, (System.Action) (() =>
    {
      ++this.LevelUpBonusCloseCounter;
      if (this.LevelUpBonusCount <= this.LevelUpBonusCloseCounter)
      {
        Singleton<NGGameDataManager>.GetInstance().playerLevelRewards = (List<LevelRewardSchemaMixin>) null;
        this.MainPanel.alpha = 1f;
        this.CharaAnimProc();
        this.PlayTween(MypageMenuBase.START_TWEENGROUP);
        this.TutorialAdvice();
      }
      else
        this.LevelUpPopupStart(levelupBonus);
    }));
  }

  public IEnumerator DoLevelUpPopup()
  {
    Singleton<CommonRoot>.GetInstance().isLoading = false;
    this.MainPanel.alpha = 0.0f;
    foreach (LevelRewardSchemaMixin playerLevelReward in Singleton<NGGameDataManager>.GetInstance().playerLevelRewards)
    {
      bool bWait = true;
      ModalWindow.Show(playerLevelReward.reward_title, playerLevelReward.reward_message, (System.Action) (() => bWait = false));
      while (bWait)
        yield return (object) null;
    }
    this.MainPanel.alpha = 1f;
    Singleton<NGGameDataManager>.GetInstance().playerLevelRewards = (List<LevelRewardSchemaMixin>) null;
  }

  public IEnumerator DoExploreRankingPopup()
  {
    Singleton<CommonRoot>.GetInstance().isTouchBlock = true;
    Singleton<CommonRoot>.GetInstance().isLoading = false;
    Future<WebAPI.Response.ExploreRankingResult> futureF = WebAPI.ExploreRankingResult((System.Action<WebAPI.Response.UserError>) (e =>
    {
      WebAPI.DefaultUserErrorCallback(e);
      Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
    }));
    IEnumerator e1 = futureF.Wait();
    while (e1.MoveNext())
      yield return e1.Current;
    e1 = (IEnumerator) null;
    if (futureF.Result != null)
    {
      WebAPI.Response.ExploreRankingResult resultData = futureF.Result;
      Singleton<CommonRoot>.GetInstance().isTouchBlock = false;
      if (resultData.current_period_id.HasValue)
      {
        Persist.exploreRankingInfo.Data.lastPeriodId = resultData.current_period_id.Value;
        Persist.exploreRankingInfo.Data.isResultView = false;
      }
      else
        Persist.exploreRankingInfo.Data.isResultView = true;
      Persist.exploreRankingInfo.Flush();
      if (resultData.aggregate_period_id.HasValue)
      {
        int? aggregatePeriodId = resultData.aggregate_period_id;
        int num = 0;
        if (!(aggregatePeriodId.GetValueOrDefault() == num & aggregatePeriodId.HasValue) && resultData.rank != 0)
        {
          int period_id = resultData.aggregate_period_id.Value;
          int rank = resultData.rank;
          int defeatCount = resultData.defeat_count;
          int floor = MasterData.ExploreFloor[resultData.floor_id].floor;
          ExploreRankingRewardPopupSequence rankingRewardPopupSeq = new ExploreRankingRewardPopupSequence();
          yield return (object) rankingRewardPopupSeq.Init(period_id, rank, defeatCount, floor);
          if (resultData.ranking_rewards != null)
          {
            foreach (WebAPI.Response.ExploreRankingResultRanking_rewards rankingReward in resultData.ranking_rewards)
            {
              if (rankingReward.rewards != null)
              {
                foreach (WebAPI.Response.ExploreRankingResultRanking_rewardsRewards reward in rankingReward.rewards)
                  rankingRewardPopupSeq.addRewardData(rankingReward.condition_id, (MasterDataTable.CommonRewardType) reward.reward_type_id, reward.reward_id, reward.reward_quantity);
              }
            }
          }
          yield return (object) rankingRewardPopupSeq.Run();
        }
      }
    }
  }

  public virtual void SetJogDialActive(bool active)
  {
    this.slc_Circle_Base.SetActive(active);
    this.dir_Roulette.SetActive(active);
    this.dir_Select.SetActive(active);
  }

  public void PlayTween(int groupID)
  {
    UITweener[] uiTweenerArray = this.GetComponentsInChildren<UITweener>();
    if (groupID == 50)
      uiTweenerArray = ((IEnumerable<UITweener>) uiTweenerArray).Where<UITweener>((Func<UITweener, bool>) (x => !x.name.Equals("icon") && !x.name.Equals("GearKindIcon(Clone)") && (!x.name.Equals("weapon_type") && !x.name.Equals("rarity_star")) && !x.name.Equals("hime_type"))).ToArray<UITweener>();
    ((IEnumerable<UITweener>) uiTweenerArray).ForEach<UITweener>((System.Action<UITweener>) (x =>
    {
      if (x.tweenGroup != groupID)
        return;
      x.gameObject.SetActive(true);
      x.ResetToBeginning();
      x.PlayForward();
    }));
  }

  public void ButtonTweenFinished()
  {
    ++this.ButtonTweenFinishedCount;
    if (this.ButtonCount != this.ButtonTweenFinishedCount)
      return;
    this.AllCircleTweenFinished();
  }

  private IEnumerator OpenColosseum()
  {
    MypageMenuBase mypageMenuBase = this;
    Future<GameObject> effect = Res.Prefabs.colosseum.colosseum_open_effectTween.Load<GameObject>();
    IEnumerator e = effect.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    // ISSUE: reference to a compiler-generated method
    effect.Result.Clone(mypageMenuBase.OpenEffectObject.transform).GetComponent<MypageSlidePanelOpen>().Init(new System.Action<MypageSlidePanelOpen>(mypageMenuBase.\u003COpenColosseum\u003Eb__100_0));
    mypageMenuBase.Colosseum_Container.ChangeState(true);
    Persist.colosseumOpen.Data.isOpen = true;
    Persist.colosseumOpen.Flush();
  }

  private void OpenColosseumEnd(MypageSlidePanelOpen obj)
  {
    this.Colosseum_Container.EnableEffect();
    UnityEngine.Object.DestroyObject((UnityEngine.Object) obj.gameObject);
    this.AllCircleTweenFinished();
  }

  public virtual IEnumerator CloudAnimationStart()
  {
    MypageMenuBase mypageMenuBase = this;
    Singleton<CommonRoot>.GetInstance().StartBGTween(MypageMenuBase.HEAVEN_OUT_BG_TWEENGROUP);
    mypageMenuBase.PlayTween(MypageMenuBase.END_TWEENGROUP_TOP);
    // ISSUE: reference to a compiler-generated method
    IEnumerator e = Singleton<CommonRoot>.GetInstance().StartCloudAnim(MypageCloudAnim.HeavenOut, MypageCloudAnim.EarthIn, new System.Action(mypageMenuBase.\u003CCloudAnimationStart\u003Eb__102_0));
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  protected virtual void AllCircleTweenFinished()
  {
    this.NotTouch.SetActive(false);
    this.CharaStartVoice();
    if ((UnityEngine.Object) this.MissionClearInfo != (UnityEngine.Object) null)
      this.MissionClearInfo.startDisplay();
    if ((bool) (UnityEngine.Object) this.CirculButton)
      this.CirculButton.condition = CircularMotionPositionSet.CirculCondition.START;
    if (!((UnityEngine.Object) this.BannerParent != (UnityEngine.Object) null))
      return;
    this.BannerParent.StartLoadBannerAll();
  }

  private void CharaStartVoice()
  {
    CharaVoiceCueEnum.CueID[] cueIdArray = new CharaVoiceCueEnum.CueID[6]
    {
      CharaVoiceCueEnum.CueID.MYPAGE_TIMESIGNAL_0_3,
      CharaVoiceCueEnum.CueID.MYPAGE_TIMESIGNAL_4_7,
      CharaVoiceCueEnum.CueID.MYPAGE_TIMESIGNAL_8_11,
      CharaVoiceCueEnum.CueID.MYPAGE_TIMESIGNAL_12_15,
      CharaVoiceCueEnum.CueID.MYPAGE_TIMESIGNAL_16_19,
      CharaVoiceCueEnum.CueID.MYPAGE_TIMESIGNAL_20_23
    };
    if (!MasterData.UnitUnit.ContainsKey(MypageMenuBase.LeaderID))
      return;
    this.sm.playVoiceByID(MasterData.UnitUnit[MypageMenuBase.LeaderID].unitVoicePattern, (int) cueIdArray[DateTime.Now.Hour / 4], -1, 0.0f);
  }

  protected void CharaTouchVoice()
  {
    if ((UnityEngine.Object) this.sm == (UnityEngine.Object) null)
      this.sm = Singleton<NGSoundManager>.GetInstance();
    this.sm.stopVoice(-1);
    if (!MasterData.UnitUnit.ContainsKey(MypageMenuBase.LeaderID))
      return;
    UnitVoicePattern unitVoicePattern = MasterData.UnitUnit[MypageMenuBase.LeaderID].unitVoicePattern;
    if (UnityEngine.Random.Range(0, 1000) < 500 && unitVoicePattern != null && this.sm.ExistsCueID(unitVoicePattern.file_name, 90))
    {
      this.sm.playVoiceByID(unitVoicePattern, 90, -1, 0.0f);
    }
    else
    {
      if (MypageMenuBase.playTouchVoiceList.Count <= 0 || unitVoicePattern == null)
        return;
      this.sm.playVoiceByID(unitVoicePattern, MypageMenuBase.playTouchVoiceList[this.nextPlayIndex], -1, 0.0f);
      this.nextPlayIndex = (this.nextPlayIndex + 1) % MypageMenuBase.playTouchVoiceList.Count;
      if (this.nextPlayIndex != 0)
        return;
      MypageMenuBase.playTouchVoiceList = MypageMenuBase.playTouchVoiceList.Shuffle<int>().ToList<int>();
    }
  }

  public virtual void CharaAnimProc()
  {
    if (!this.CharaAnimation)
      return;
    this.CharaAnimObj.SetActive(true);
    this.CharaAnimator.SetBool(this.tweenName, true);
    Animator component1 = Singleton<CommonRoot>.GetInstance().getBackgroundComponent<QuestBG>().GetComponent<Animator>();
    if ((bool) (UnityEngine.Object) component1)
      component1.SetTrigger(this.tweenName);
    Animator component2 = Singleton<CommonRoot>.GetInstance().GetComponent<Animator>();
    if ((bool) (UnityEngine.Object) component2)
      component2.SetTrigger(this.tweenName);
    Animator component3 = this.GetComponent<Animator>();
    if (!(bool) (UnityEngine.Object) component3)
      return;
    component3.SetTrigger(this.tweenName);
  }

  public void CharaAnimInit()
  {
    if ((UnityEngine.Object) this.CharaAnimObj == (UnityEngine.Object) null)
      return;
    this.CharaAnimObj.SetActive(false);
  }

  protected virtual IEnumerator CreateCharcterAnimation()
  {
    MypageMenuBase mypageMenuBase = this;
    Future<GameObject> f = new ResourceObject("Prefabs/mypage/" + (!Singleton<NGGameDataManager>.GetInstance().IsEarth ? "CharacterAnimator" : "CharacterAnimator_ground")).Load<GameObject>();
    IEnumerator e = f.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    mypageMenuBase.CharaAnimObj = f.Result.Clone(mypageMenuBase.CharaAnimTrans);
    CharaAnimationConst component = mypageMenuBase.CharaAnimObj.GetComponent<CharaAnimationConst>();
    mypageMenuBase.CharaAnimator = component.anim;
    mypageMenuBase.CharaSprites = component.charaSprite;
    mypageMenuBase.CharaName = component.charaName;
    // ISSUE: reference to a compiler-generated method
    ((IEnumerable<UI2DSprite>) mypageMenuBase.CharaSprites).ForEach<UI2DSprite>(new System.Action<UI2DSprite>(mypageMenuBase.\u003CCreateCharcterAnimation\u003Eb__108_0));
    mypageMenuBase._chara_anim_english_name = component._english_name;
  }

  protected virtual IEnumerator CreateJogDial()
  {
    Future<GameObject> f = Res.Prefabs.mypage.CircleAnimation_Story.Load<GameObject>();
    IEnumerator e = f.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if ((UnityEngine.Object) this.Story_Container != (UnityEngine.Object) null)
      this.Story_Container.SetEffect(f.Result, "story", true);
    if ((UnityEngine.Object) this.Character_Container != (UnityEngine.Object) null)
      this.Character_Container.SetEffect(f.Result, "character", true);
    if ((UnityEngine.Object) this.Event_Container != (UnityEngine.Object) null)
      this.Event_Container.SetEffect(f.Result, "event", true);
    Player player = SMManager.Get<Player>();
    if (player.IsPvp() && (UnityEngine.Object) this.Multi_Container != (UnityEngine.Object) null)
    {
      this.Multi_Container.SetEffect(f.Result, "multi", true);
      this.Multi_Container.ChangeState(true);
    }
    if (player.IsColosseum() && (UnityEngine.Object) this.Colosseum_Container != (UnityEngine.Object) null)
    {
      this.Colosseum_Container.SetEffect(f.Result, "colosseum", true);
      this.Colosseum_Container.ChangeState(true);
    }
  }

  private IEnumerator CreateLoginPopup()
  {
    if (!((UnityEngine.Object) this.LoginPopup != (UnityEngine.Object) null))
    {
      Future<GameObject> f = Res.Prefabs.popup.popup_000_14_4__anim_popup01.Load<GameObject>();
      IEnumerator e = f.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.LoginPopup = f.Result;
    }
  }

  public void StopBannerScroll()
  {
    if (!((UnityEngine.Object) this.BannerParent != (UnityEngine.Object) null))
      return;
    this.BannerParent.StopEffect();
    this.BannerParent.StopScroll();
  }

  private IEnumerator SetStoryDecorateImage()
  {
    IEnumerator e = ServerTime.WaitSync();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    DateTime now = ServerTime.NowAppTime();
    QuestCommonJogDecoration commonJogDecoration = ((IEnumerable<QuestCommonJogDecoration>) MasterData.QuestCommonJogDecorationList).FirstOrDefault<QuestCommonJogDecoration>((Func<QuestCommonJogDecoration, bool>) (x => now >= x.start_at.Value && now <= x.end_at.Value));
    if (commonJogDecoration != null)
    {
      Future<UnityEngine.Sprite> f = Singleton<ResourceManager>.GetInstance().Load<UnityEngine.Sprite>(string.Format("Prefabs/mypage/storyquest_banner/{0}", (object) commonJogDecoration.banner_id), 1f);
      e = f.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.slc_story_start.sprite2D = f.Result;
      f = (Future<UnityEngine.Sprite>) null;
    }
    else
      this.slc_story_start.sprite2D = (UnityEngine.Sprite) null;
  }

  public void SetMypageBannerHilight(bool light)
  {
    if ((UnityEngine.Object) this.bannerParent == (UnityEngine.Object) null)
      return;
    for (int index = 0; index < this.bannerParent.transform.childCount; ++index)
    {
      BannerSetting component = this.bannerParent.transform.GetChild(index).GetComponent<BannerSetting>();
      if (!((UnityEngine.Object) component == (UnityEngine.Object) null))
        component.EffectRenderer.gameObject.SetActive(light);
    }
  }

  private IEnumerator CreateMissionClearInfo()
  {
    if (!((UnityEngine.Object) this.MissionClearInfo == (UnityEngine.Object) null))
    {
      IEnumerator e = this.MissionClearInfo.initOne();
      while (e.MoveNext())
        yield return e.Current;
    }
  }

  private IEnumerator MissionClearInfoSetting()
  {
    if (!((UnityEngine.Object) this.MissionClearInfo == (UnityEngine.Object) null))
    {
      this.MissionClearInfo.init(SMManager.Get<Player>());
      yield break;
    }
  }

  public void StopMissionClearInfo()
  {
    if (!((UnityEngine.Object) this.MissionClearInfo != (UnityEngine.Object) null))
      return;
    this.MissionClearInfo.clearAll();
  }

  public void HidePanelMission()
  {
    this.NextPanelMission.gameObject.SetActive(false);
    this.PanelMissionBtn.gameObject.SetActive(false);
    this.DirPanelMissionBtn.SetActive(false);
    this.DirPanelMissionEffect.SetActive(false);
  }

  public void TutorialAdvice()
  {
    this.StartCoroutine(this.Advice());
  }

  public IEnumerator Advice()
  {
    MypageMenuBase mypageMenuBase = this;
    UITweener[] tweens = mypageMenuBase.GetComponentsInChildren<UITweener>();
    while (((IEnumerable<UITweener>) tweens).Any<UITweener>((Func<UITweener, bool>) (x => x.gameObject.activeInHierarchy && x.tweenGroup == MypageMenuBase.START_TWEENGROUP && x.enabled)))
    {
      mypageMenuBase.NotTouch.SetActive(true);
      yield return (object) null;
    }
    mypageMenuBase.NotTouch.SetActive(false);
    mypageMenuBase.BackBtnEnable = true;
    if (!Persist.tutorial.Data.IsFinishTutorial())
      Singleton<TutorialRoot>.GetInstance().CurrentAdvise();
    else if (Persist.newTutorial.Data.beginnersQuest)
    {
      ServerTime.NowAppTime();
      // ISSUE: reference to a compiler-generated method
      Singleton<TutorialRoot>.GetInstance().ForceShowAdviceInNextButton("newchapter_home2_tutorial", new Dictionary<string, Func<Transform, UIButton>>()
      {
        {
          "chapter_home2",
          new Func<Transform, UIButton>(mypageMenuBase.\u003CAdvice\u003Eb__119_1)
        }
      }, (System.Action) null);
    }
  }
}
