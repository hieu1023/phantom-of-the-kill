﻿// Decompiled with JetBrains decompiler
// Type: Unit0046ConfirmAutoOrganization
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;
using UnityEngine;

public class Unit0046ConfirmAutoOrganization : BackButtonMenuBase
{
  [SerializeField]
  private UILabel txtCommon_;
  [SerializeField]
  private GameObject topLimited_;
  [SerializeField]
  private UILabel txtLimited_;
  [SerializeField]
  private GameObject topNormal_;
  [SerializeField]
  private UILabel txtNormal_;
  private EventDelegate.Callback eventYes_;

  public static IEnumerator doPopup(
    bool modeLimited,
    string description,
    EventDelegate.Callback eventYes,
    EventDelegate.Callback errorExit)
  {
    Future<GameObject> ld = Res.Prefabs.popup.popup_002_quest_automatic_team_edit__anim_popup01.Load<GameObject>();
    IEnumerator e = ld.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if ((Object) ld.Result == (Object) null)
    {
      if (errorExit != null)
        errorExit();
    }
    else
    {
      Singleton<PopupManager>.GetInstance().open(ld.Result, false, false, false, true, false, false, "SE_1006").GetComponent<Unit0046ConfirmAutoOrganization>().initialize(modeLimited, description, eventYes);
      while (Singleton<PopupManager>.GetInstance().isOpen)
        yield return (object) null;
    }
  }

  private void initialize(bool modeLimited, string description, EventDelegate.Callback eventYes)
  {
    UILabel label;
    if (modeLimited)
    {
      this.txtCommon_.SetTextLocalize(Consts.GetInstance().UNIT_0046_CONFIRM_LIMITATION_AUTODECK);
      this.topLimited_.SetActive(true);
      label = this.txtLimited_;
      if ((Object) this.topNormal_ != (Object) null)
        this.topNormal_.SetActive(false);
    }
    else
    {
      this.txtCommon_.SetTextLocalize(Consts.GetInstance().UNIT_0046_CONFIRM_NORMAL_AUTODECK);
      this.topLimited_.SetActive(false);
      if ((Object) this.topNormal_ != (Object) null)
        this.topNormal_.SetActive(true);
      description = Consts.GetInstance().UNIT_0046_CONFIRM_NORMAL_RULE_AUTODECK;
      label = this.txtNormal_;
    }
    if ((Object) label != (Object) null)
      label.SetTextLocalize(!string.IsNullOrEmpty(description) ? description : "");
    this.eventYes_ = eventYes;
  }

  public override void onBackButton()
  {
    this.onClickedNO();
  }

  public void onClickedNO()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<PopupManager>.GetInstance().dismiss(false);
  }

  public void onClickedYES()
  {
    if (this.IsPushAndSet())
      return;
    if (this.eventYes_ != null)
      this.eventYes_();
    Singleton<PopupManager>.GetInstance().dismiss(false);
  }
}
