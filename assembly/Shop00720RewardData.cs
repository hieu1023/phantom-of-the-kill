﻿// Decompiled with JetBrains decompiler
// Type: Shop00720RewardData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

public class Shop00720RewardData
{
  private int rewardId;
  private MasterDataTable.CommonRewardType rewardType;
  private string description;
  private int quantity;

  public MasterDataTable.CommonRewardType RewardType
  {
    get
    {
      return this.rewardType;
    }
    set
    {
      this.rewardType = value;
    }
  }

  public int RewardId
  {
    get
    {
      return this.rewardId;
    }
    set
    {
      this.rewardId = value;
    }
  }

  public int Quantity
  {
    get
    {
      return this.quantity;
    }
    set
    {
      this.quantity = value;
    }
  }

  public string Description
  {
    get
    {
      return this.description;
    }
    set
    {
      this.description = value;
    }
  }
}
