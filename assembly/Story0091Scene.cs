﻿// Decompiled with JetBrains decompiler
// Type: Story0091Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;
using System.Collections;
using UnityEngine;

public class Story0091Scene : NGSceneBase
{
  public Story0091Menu menu;
  [SerializeField]
  private NGxScroll ScrollContainer;

  public IEnumerator onStartSceneAsync(int XL)
  {
    PlayerStoryQuestS[] self = SMManager.Get<PlayerStoryQuestS[]>();
    this.ScrollContainer.Clear();
    IEnumerator e = this.menu.InitPartButton(self.DisplayScrollL(XL), XL);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.ScrollContainer.ResolvePosition();
    if (Singleton<CommonRoot>.GetInstance().isLoading)
    {
      yield return (object) null;
      Singleton<CommonRoot>.GetInstance().isLoading = false;
    }
  }
}
