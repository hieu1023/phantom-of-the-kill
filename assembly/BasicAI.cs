﻿// Decompiled with JetBrains decompiler
// Type: BasicAI
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using AI.Logic;
using GameCore;
using System.Collections;
using UnityEngine;

public class BasicAI : BattleMonoBehaviour
{
  private LispAILogic ai = new LispAILogic();

  public AILogicBase aiLogic
  {
    get
    {
      return (AILogicBase) this.ai;
    }
  }

  protected override IEnumerator Start_Battle()
  {
    BasicAI basicAi = this;
    basicAi.ai.env = basicAi.env.core;
    basicAi.ai.cleanup();
    IEnumerator e = basicAi.loadScript();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    basicAi.StartCoroutine(basicAi.ai.doExecute());
  }

  private IEnumerator loadScript()
  {
    BasicAI basicAi = this;
    Future<TextAsset> scriptF = Singleton<ResourceManager>.GetInstance().Load<TextAsset>("AI/ai_script_bytes", 1f);
    IEnumerator e = scriptF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Future<TextAsset> numberF = Singleton<ResourceManager>.GetInstance().Load<TextAsset>("AI/ai_script_number_bytes", 1f);
    e = numberF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    object sexp = basicAi.ai.readLisp(scriptF.Result.bytes, numberF.Result.bytes);
    e = basicAi.ai.createEngine(sexp);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    foreach (BL.UnitPosition unitPosition in basicAi.env.core.unitPositions.value)
    {
      e = unitPosition.unit.InitAIExtention(basicAi.ai, (byte[]) null);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
  }
}
