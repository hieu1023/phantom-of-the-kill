﻿// Decompiled with JetBrains decompiler
// Type: Raid032GuildRankingRewardScene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using UnityEngine;

public class Raid032GuildRankingRewardScene : NGSceneBase
{
  private static readonly string DEFAULT_NAME = "raid032_RankingReward_conf";
  private static int periodID = 0;
  private Raid032GuildRankingRewardMenu rankingRewardMenu;

  public static void ChangeScene(int periodId, bool bstack = true)
  {
    Raid032GuildRankingRewardScene.periodID = periodId;
    Singleton<NGSceneManager>.GetInstance().changeScene(Raid032GuildRankingRewardScene.DEFAULT_NAME, bstack, (object[]) Array.Empty<object>());
  }

  public IEnumerator onStartSceneAsync()
  {
    Raid032GuildRankingRewardScene rankingRewardScene = this;
    if ((UnityEngine.Object) rankingRewardScene.rankingRewardMenu == (UnityEngine.Object) null)
      rankingRewardScene.rankingRewardMenu = rankingRewardScene.gameObject.GetComponent<Raid032GuildRankingRewardMenu>();
    IEnumerator e = rankingRewardScene.rankingRewardMenu.Initalize(Raid032GuildRankingRewardScene.periodID);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public void onStartScene()
  {
    Singleton<CommonRoot>.GetInstance().isLoading = false;
  }

  public override IEnumerator onEndSceneAsync()
  {
    Raid032GuildRankingRewardScene rankingRewardScene = this;
    float startTime = Time.time;
    while (!rankingRewardScene.isTweenFinished && (double) Time.time - (double) startTime < (double) rankingRewardScene.tweenTimeoutTime)
      yield return (object) null;
    Singleton<CommonRoot>.GetInstance().loadingMode = 0;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    rankingRewardScene.isTweenFinished = true;
    yield return (object) null;
    // ISSUE: reference to a compiler-generated method
    yield return (object) rankingRewardScene.\u003C\u003En__0();
  }
}
