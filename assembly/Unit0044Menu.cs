﻿// Decompiled with JetBrains decompiler
// Type: Unit0044Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Unit0044Menu : Bugu005SelectItemListMenuBase
{
  [SerializeField]
  protected UILabel TxtNumberpossession;
  private GameObject StatusChangePopupPrefab;
  private GameObject EquipAlertPopupPrefab;
  private bool isOpened;
  private int changeGearIndex;
  private PlayerUnit basePlayerUnit;
  private bool isEarthMode;

  public int ChangeGearIndex
  {
    set
    {
      this.changeGearIndex = value;
    }
    get
    {
      return this.changeGearIndex;
    }
  }

  public PlayerUnit BasePlayerUnit
  {
    set
    {
      this.basePlayerUnit = value;
    }
    get
    {
      return this.basePlayerUnit;
    }
  }

  public bool IsEarthMode
  {
    set
    {
      this.isEarthMode = value;
    }
    get
    {
      return this.isEarthMode;
    }
  }

  public override Persist<Persist.ItemSortAndFilterInfo> GetPersist()
  {
    return Persist.unit0044SortAndFilter;
  }

  protected override List<PlayerItem> GetItemList()
  {
    if (this.BasePlayerUnit.unit.awake_unit_flag)
    {
      if (this.ChangeGearIndex == 1)
        return this.filterGearRestriction(((IEnumerable<PlayerItem>) SMManager.Get<PlayerItem[]>()).Where<PlayerItem>((Func<PlayerItem, bool>) (x => !x.broken && x.isWeapon() && x.gear.kind_GearKind == this.basePlayerUnit.unit.kind_GearKind && x.gear.enableEquipmentUnit(this.basePlayerUnit))).ToList<PlayerItem>());
      if (this.ChangeGearIndex == 2)
        return this.filterGearRestriction(((IEnumerable<PlayerItem>) SMManager.Get<PlayerItem[]>()).Where<PlayerItem>((Func<PlayerItem, bool>) (x => !x.broken && x.isWeapon() && (x.gear.kind.Enum == GearKindEnum.shield || x.gear.kind.Enum == GearKindEnum.accessories) && x.gear.enableEquipmentUnit(this.basePlayerUnit))).ToList<PlayerItem>());
    }
    return this.filterGearRestriction(((IEnumerable<PlayerItem>) SMManager.Get<PlayerItem[]>()).Where<PlayerItem>((Func<PlayerItem, bool>) (x => !x.broken && x.isWeapon() && (x.gear.kind.Enum == GearKindEnum.shield || x.gear.kind.Enum == GearKindEnum.accessories || x.gear.kind_GearKind == this.basePlayerUnit.unit.kind_GearKind) && x.gear.enableEquipmentUnit(this.basePlayerUnit))).ToList<PlayerItem>());
  }

  private List<PlayerItem> filterGearRestriction(List<PlayerItem> playerItems)
  {
    MasterDataTable.UnitJob jobData = this.basePlayerUnit.getJobData();
    int classification_GearClassificationPattern = 0;
    if (jobData.classification_GearClassificationPattern.HasValue)
    {
      int? classificationPattern = jobData.classification_GearClassificationPattern;
      int num = 0;
      if (!(classificationPattern.GetValueOrDefault() == num & classificationPattern.HasValue))
        classification_GearClassificationPattern = jobData.classification_GearClassificationPattern.Value;
    }
    return classification_GearClassificationPattern == 0 ? playerItems : playerItems.Where<PlayerItem>((Func<PlayerItem, bool>) (x =>
    {
      int? classificationPattern = x.gear.classification_GearClassificationPattern;
      int num = classification_GearClassificationPattern;
      return classificationPattern.GetValueOrDefault() == num & classificationPattern.HasValue || x.gear.kind.isNonWeapon;
    })).ToList<PlayerItem>();
  }

  protected override IEnumerator InitExtension()
  {
    Unit0044Menu unit0044Menu = this;
    if (unit0044Menu.ChangeGearIndex == 1 && unit0044Menu.basePlayerUnit.equippedGear != (PlayerItem) null || unit0044Menu.changeGearIndex == 2 && unit0044Menu.basePlayerUnit.equippedGear2 != (PlayerItem) null)
    {
      InventoryItem inventoryItem = new InventoryItem();
      unit0044Menu.InventoryItems.Insert(0, inventoryItem);
    }
    Future<GameObject> popupPrefabF;
    IEnumerator e;
    if ((UnityEngine.Object) unit0044Menu.StatusChangePopupPrefab == (UnityEngine.Object) null)
    {
      popupPrefabF = Res.Prefabs.popup.popup_004_4_1__anim_popup01.Load<GameObject>();
      e = popupPrefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      unit0044Menu.StatusChangePopupPrefab = popupPrefabF.Result;
      popupPrefabF = (Future<GameObject>) null;
    }
    if ((UnityEngine.Object) unit0044Menu.EquipAlertPopupPrefab == (UnityEngine.Object) null)
    {
      popupPrefabF = Res.Prefabs.popup.popup_004_12_4__anim_popup01.Load<GameObject>();
      e = popupPrefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      unit0044Menu.EquipAlertPopupPrefab = popupPrefabF.Result;
      popupPrefabF = (Future<GameObject>) null;
    }
  }

  protected override void ChangeDetailScene(GameCore.ItemInfo item)
  {
    Unit00443Scene.changeSceneLimited(true, item);
  }

  protected override void BottomInfoUpdate()
  {
    int num = 0;
    foreach (PlayerItem playerItem in SMManager.Get<PlayerItem[]>())
    {
      if (!playerItem.isSupply() && !playerItem.isReisou())
        ++num;
    }
    this.TxtNumberpossession.SetTextLocalize(string.Format("{0}/{1}", (object) num, (object) SMManager.Get<Player>().max_items));
  }

  protected override void SelectItemProc(GameCore.ItemInfo item)
  {
    if (this.InventoryItems.FindByItem(item).removeButton)
    {
      this.StartCoroutine(this.RemoveGearAsync());
    }
    else
    {
      PlayerItem afterGear = ((IEnumerable<PlayerItem>) SMManager.Get<PlayerItem[]>()).FirstOrDefault<PlayerItem>((Func<PlayerItem, bool>) (x => x.id == item.itemID));
      this.ChangeGear(this.basePlayerUnit, this.basePlayerUnit.unit.awake_unit_flag ? (this.changeGearIndex == 1 ? this.basePlayerUnit.equippedGear : this.basePlayerUnit.equippedGear2) : this.basePlayerUnit.equippedGear, afterGear);
      this.UpdateSelectItemIndexWithInfo();
    }
  }

  protected override void CreateItemIconAdvencedSetting(int inventoryItemIdx, int allItemIdx)
  {
    ItemIcon itemIcon = this.AllItemIcon[allItemIdx];
    InventoryItem displayItem = this.DisplayItems[inventoryItemIdx];
    itemIcon.Gray = false;
    itemIcon.QuantitySupply = false;
    if (displayItem.removeButton)
    {
      itemIcon.Favorite = false;
      itemIcon.ForBattle = false;
      itemIcon.onClick = (System.Action<ItemIcon>) (playeritem => this.StartCoroutine(this.RemoveGearAsync()));
      itemIcon.DisableLongPressEvent();
    }
    else
    {
      itemIcon.ForBattle = displayItem.Item.ForBattle;
      itemIcon.Favorite = displayItem.Item.favorite;
      itemIcon.onClick = (System.Action<ItemIcon>) (playeritem => this.SelectItemProc(playeritem.ItemInfo));
      if (this.IsGrayIcon(displayItem))
      {
        itemIcon.Gray = true;
        displayItem.Gray = true;
        itemIcon.onClick = (System.Action<ItemIcon>) (_ => {});
      }
      itemIcon.EnableLongPressEvent(new System.Action<GameCore.ItemInfo>(((Bugu005ItemListMenuBase) this).ChangeDetailScene));
    }
  }

  protected override bool DisableTouchIcon(InventoryItem item)
  {
    if (item.Item == null)
      return true;
    PlayerItem equippedGear = this.basePlayerUnit.equippedGear;
    PlayerItem equippedGear2 = this.basePlayerUnit.equippedGear2;
    if (!item.Item.gear.isEquipment(this.basePlayerUnit) || equippedGear != (PlayerItem) null && equippedGear.id == item.Item.itemID || equippedGear2 != (PlayerItem) null && equippedGear2.id == item.Item.itemID)
      return true;
    return !item.Item.gear.isEquipment(this.basePlayerUnit) && base.DisableTouchIcon(item);
  }

  private IEnumerator StatusPopup(
    PlayerUnit baseUnit,
    PlayerItem beforeGear,
    PlayerItem afterGear)
  {
    Future<GameObject> iconPrefabF = Res.Prefabs.ItemIcon.prefab.Load<GameObject>();
    IEnumerator e = iconPrefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    GameObject iconPrefab = iconPrefabF.Result;
    GameObject beforeGearIcon = UnityEngine.Object.Instantiate<GameObject>(iconPrefab);
    ItemIcon beforeGearIconScript = beforeGearIcon.GetComponent<ItemIcon>();
    if (beforeGear != (PlayerItem) null)
    {
      e = beforeGearIconScript.InitByPlayerItem(beforeGear);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    else
    {
      e = beforeGearIconScript.InitByGear((GearGear) null, CommonElement.none, false, false, false);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      beforeGearIconScript.SetEmpty(true);
    }
    beforeGearIcon.SetActive(false);
    GameObject afterGearIcon = UnityEngine.Object.Instantiate<GameObject>(iconPrefab);
    ItemIcon afterGearIconScript = afterGearIcon.GetComponent<ItemIcon>();
    if (afterGear != (PlayerItem) null)
    {
      e = afterGearIconScript.InitByPlayerItem(afterGear);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    else
    {
      e = afterGearIconScript.InitByGear((GearGear) null, CommonElement.none, false, false, false);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      afterGearIconScript.SetEmpty(true);
    }
    afterGearIcon.SetActive(false);
    Future<GameObject> skillTypeIconLoader = Res.Prefabs.BattleSkillIcon._battleSkillIcon.Load<GameObject>();
    e = skillTypeIconLoader.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    GameObject skillTypeIconPrefab = skillTypeIconLoader.Result;
    List<GameObject> beforeSkillTypeIcons = new List<GameObject>();
    GearGearSkill[] gearGearSkillArray;
    int index;
    GameObject beforeSkillTypeIcon;
    if (beforeGear != (PlayerItem) null)
    {
      gearGearSkillArray = beforeGear.skills;
      for (index = 0; index < gearGearSkillArray.Length; ++index)
      {
        GearGearSkill gearGearSkill = gearGearSkillArray[index];
        beforeSkillTypeIcon = UnityEngine.Object.Instantiate<GameObject>(skillTypeIconPrefab);
        e = beforeSkillTypeIcon.GetComponent<BattleSkillIcon>().Init(gearGearSkill.skill);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        beforeSkillTypeIcon.SetActive(false);
        beforeSkillTypeIcons.Add(beforeSkillTypeIcon);
        beforeSkillTypeIcon = (GameObject) null;
      }
      gearGearSkillArray = (GearGearSkill[]) null;
    }
    List<GameObject> afterSkillTypeIcons = new List<GameObject>();
    if (afterGear != (PlayerItem) null)
    {
      gearGearSkillArray = afterGear.skills;
      for (index = 0; index < gearGearSkillArray.Length; ++index)
      {
        GearGearSkill gearGearSkill = gearGearSkillArray[index];
        beforeSkillTypeIcon = UnityEngine.Object.Instantiate<GameObject>(skillTypeIconPrefab);
        e = beforeSkillTypeIcon.GetComponent<BattleSkillIcon>().Init(gearGearSkill.skill);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        beforeSkillTypeIcon.SetActive(false);
        afterSkillTypeIcons.Add(beforeSkillTypeIcon);
        beforeSkillTypeIcon = (GameObject) null;
      }
      gearGearSkillArray = (GearGearSkill[]) null;
    }
    GameObject statusPopup = this.StatusChangePopupPrefab.Clone((Transform) null);
    Unit00441Menu component = statusPopup.GetComponent<Unit00441Menu>();
    statusPopup.SetActive(false);
    beforeGearIcon.SetActive(false);
    afterGearIcon.SetActive(false);
    e = component.SetGear((PlayerUnit) null, baseUnit, beforeGear, afterGear, beforeGearIcon, afterGearIcon, beforeSkillTypeIcons.ToArray(), afterSkillTypeIcons.ToArray(), this.changeGearIndex, this.isEarthMode);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Singleton<PopupManager>.GetInstance().open(statusPopup, false, false, true, true, false, false, "SE_1006");
    statusPopup.SetActive(true);
    beforeGearIcon.SetActive(true);
    afterGearIcon.SetActive(true);
    this.isOpened = false;
  }

  private void ChangeGear(PlayerUnit baseUnit, PlayerItem beforeGear, PlayerItem afterGear)
  {
    if (afterGear != (PlayerItem) null)
    {
      if (afterGear.ForBattle)
      {
        Singleton<PopupManager>.GetInstance().open(this.EquipAlertPopupPrefab, false, false, false, true, false, false, "SE_1006").GetComponent<Unit004431Popup>().Init(((IEnumerable<PlayerUnit>) SMManager.Get<PlayerUnit[]>()).Where<PlayerUnit>((Func<PlayerUnit, bool>) (x => !x.unit.IsMaterialUnit)).ToArray<PlayerUnit>(), baseUnit, afterGear, this.changeGearIndex, this.isEarthMode);
      }
      else
      {
        if (this.isOpened)
          return;
        this.isOpened = true;
        this.StartCoroutine(this.StatusPopup(baseUnit, beforeGear, afterGear));
      }
    }
    else
      this.StartCoroutine(this.StatusPopup(baseUnit, beforeGear, afterGear));
  }

  private IEnumerator RemoveGearAsync()
  {
    Unit0044Menu unit0044Menu = this;
    Singleton<CommonRoot>.GetInstance().loadingMode = 1;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    IEnumerator e = RequestDispatcher.EquipGear(unit0044Menu.changeGearIndex, new int?(0), unit0044Menu.basePlayerUnit.id, (System.Action<WebAPI.Response.UserError>) (error =>
    {
      Singleton<CommonRoot>.GetInstance().isLoading = false;
      Singleton<CommonRoot>.GetInstance().loadingMode = 0;
      if (error == null)
        return;
      WebAPI.DefaultUserErrorCallback(error);
    }), unit0044Menu.isEarthMode);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    switch (GuildUtil.gvgPopupState)
    {
      case GuildUtil.GvGPopupState.None:
        Singleton<CommonRoot>.GetInstance().isLoading = false;
        Singleton<CommonRoot>.GetInstance().loadingMode = 0;
        unit0044Menu.backScene();
        yield break;
      case GuildUtil.GvGPopupState.AtkTeam:
        // ISSUE: reference to a compiler-generated method
        e = GuildUtil.UpdateGuildDeckAttack(PlayerAffiliation.Current.guild_id, Player.Current.id, new System.Action(unit0044Menu.\u003CRemoveGearAsync\u003Eb__27_1));
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        break;
      case GuildUtil.GvGPopupState.DefTeam:
        // ISSUE: reference to a compiler-generated method
        e = GuildUtil.UpdateGuildDeckDefanse(PlayerAffiliation.Current.guild_id, Player.Current.id, new System.Action(unit0044Menu.\u003CRemoveGearAsync\u003Eb__27_2));
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        break;
      default:
        unit0044Menu.backScene();
        break;
    }
    Singleton<CommonRoot>.GetInstance().isLoading = false;
    Singleton<CommonRoot>.GetInstance().loadingMode = 0;
  }
}
