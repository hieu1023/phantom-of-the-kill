﻿// Decompiled with JetBrains decompiler
// Type: Gacha0063Ticket
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Gacha0063Ticket : Gacha0063hindicator
{
  private float Timer = 30f;
  [SerializeField]
  private UI2DSprite TopImg;
  [SerializeField]
  private UI2DSprite TitleImg;
  [SerializeField]
  private UILabel GachaTickets;
  [SerializeField]
  private UILabel GachaTerm;
  [SerializeField]
  private UIButton m_detailButton;
  private GachaModuleGacha Gacha;
  private TicketBanner BannerInfo;
  private const float INTERVAL = 30f;

  public override void InitGachaModuleGacha(
    Gacha0063Menu menu,
    GachaModuleGacha gacha,
    GachaModule gachaModule)
  {
    this.Menu = menu;
    this.Gacha = gacha;
    this.BannerInfo = ((IEnumerable<TicketBanner>) SMManager.Get<TicketBanner[]>()).FirstOrDefault<TicketBanner>((Func<TicketBanner, bool>) (i => i.gacha_id == gacha.id));
    this.singleGachaButton.Init("", gacha, this.Menu, 4, gachaModule.number);
  }

  public override IEnumerator Set(GameObject detailPopup)
  {
    Gacha0063Ticket gacha0063Ticket = this;
    IEnumerator e = gacha0063Ticket.InitDetailButton(gacha0063Ticket.Gacha, detailPopup);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if (gacha0063Ticket.BannerInfo.title_url != "")
    {
      e = Singleton<NGGameDataManager>.GetInstance().GetWebImage(gacha0063Ticket.BannerInfo.title_url, gacha0063Ticket.TitleImg);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      gacha0063Ticket.TitleImg.gameObject.SetActive(true);
    }
    else
      Debug.LogError((object) "タイトル画像URLなし");
    if (gacha0063Ticket.BannerInfo.front_url != "")
    {
      e = Singleton<NGGameDataManager>.GetInstance().GetWebImage(gacha0063Ticket.BannerInfo.front_url, gacha0063Ticket.TopImg);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      gacha0063Ticket.TopImg.gameObject.SetActive(true);
    }
    else
      Debug.LogError((object) "メイン画像URLなし");
    if (gacha0063Ticket.Gacha.payment_id.HasValue)
    {
      // ISSUE: reference to a compiler-generated method
      int quantity = ((IEnumerable<PlayerGachaTicket>) SMManager.Get<Player>().gacha_tickets).FirstOrDefault<PlayerGachaTicket>(new Func<PlayerGachaTicket, bool>(gacha0063Ticket.\u003CSet\u003Eb__10_0)).quantity;
      gacha0063Ticket.GachaTickets.SetTextLocalize(Consts.Format(Consts.GetInstance().GACHA_0063TICKET_NUM, (IDictionary) new Hashtable()
      {
        {
          (object) "num",
          (object) quantity
        }
      }));
      gacha0063Ticket.singleGachaButton.SetGachaButton(gacha0063Ticket.Gacha.payment_amount, quantity);
    }
    if (gacha0063Ticket.Gacha.end_at.HasValue)
    {
      e = gacha0063Ticket.GetTerm(new DateTime?(gacha0063Ticket.Gacha.end_at.Value));
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
  }

  public void Update()
  {
    this.Timer -= Time.deltaTime;
    if ((double) this.Timer > 0.0)
      return;
    this.StartCoroutine(this.GetTerm(this.Gacha.end_at));
    this.Timer = 30f;
  }

  private IEnumerator GetTerm(DateTime? endAt)
  {
    IEnumerator e = ServerTime.WaitSync();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    DateTime dateTime = ServerTime.NowAppTime();
    if (endAt.HasValue)
      this.GachaTerm.SetTextLocalize(Gacha0063Ticket.GetTerm(endAt.Value - dateTime));
  }

  private static string GetTerm(TimeSpan diff)
  {
    double num = Math.Floor(diff.TotalMinutes);
    string str;
    if (num > 1440.0)
      str = Consts.Format(Consts.GetInstance().GACHA_0063TICKET_LIMIT_DAY, (IDictionary) new Hashtable()
      {
        {
          (object) "day",
          (object) Math.Floor(diff.TotalDays).ToString()
        }
      });
    else if (num > 60.0)
      str = Consts.Format(Consts.GetInstance().GACHA_0063TICKET_LIMIT_HOUR, (IDictionary) new Hashtable()
      {
        {
          (object) "hour",
          (object) Math.Floor(diff.TotalHours).ToString()
        }
      });
    else
      str = Consts.Format(Consts.GetInstance().GACHA_0063TICKET_LIMIT_MIN, (IDictionary) new Hashtable()
      {
        {
          (object) "min",
          (object) Math.Floor(diff.TotalMinutes).ToString()
        }
      });
    return str;
  }

  private IEnumerator InitDetailButton(GachaModuleGacha gacha, GameObject detailPopup)
  {
    // ISSUE: reference to a compiler-generated field
    int num = this.\u003C\u003E1__state;
    Gacha0063Ticket gacha0063Ticket = this;
    if (num != 0)
      return false;
    // ISSUE: reference to a compiler-generated field
    this.\u003C\u003E1__state = -1;
    bool flag = !string.IsNullOrEmpty(gacha.details.title);
    if ((UnityEngine.Object) gacha0063Ticket.m_detailButton == (UnityEngine.Object) null)
    {
      Debug.LogError((object) ("Detail button is null: " + gacha0063Ticket.gameObject.name));
      return false;
    }
    gacha0063Ticket.m_detailButton.gameObject.SetActive(flag);
    if (!flag)
      return false;
    gacha0063Ticket.SetDetailButton(gacha, detailPopup);
    return false;
  }

  private void SetDetailButton(GachaModuleGacha gacha, GameObject detailPopup)
  {
    GachaDescription details = gacha.details;
    EventDelegate.Add(this.m_detailButton.onClick, (EventDelegate.Callback) (() =>
    {
      if (this.Menu.IsPushAndSet())
        return;
      Singleton<CommonRoot>.GetInstance().loadingMode = 1;
      Singleton<CommonRoot>.GetInstance().isLoading = true;
      this.StartCoroutine(this.OpenDetailPopup(gacha, detailPopup));
    }));
  }

  private IEnumerator OpenDetailPopup(GachaModuleGacha gacha, GameObject detailPopup)
  {
    Gacha0063Ticket gacha0063Ticket = this;
    GachaDescription details = gacha.details;
    GameObject gameObject = Singleton<PopupManager>.GetInstance().openAlert(detailPopup, false, false, (EventDelegate) null, false, true, false, true);
    if ((UnityEngine.Object) gameObject == (UnityEngine.Object) null)
    {
      Debug.LogError((object) ("Can't get popup object from PopupManager: " + gacha0063Ticket.gameObject.name));
      Singleton<CommonRoot>.GetInstance().isLoading = false;
      Singleton<CommonRoot>.GetInstance().loadingMode = 0;
    }
    else
    {
      Popup00631Menu component = gameObject.GetComponent<Popup00631Menu>();
      if ((UnityEngine.Object) component == (UnityEngine.Object) null)
      {
        Debug.LogError((object) ("Can't get menu object from popup object: " + gacha0063Ticket.gameObject.name));
        Singleton<CommonRoot>.GetInstance().isLoading = false;
        Singleton<CommonRoot>.GetInstance().loadingMode = 0;
      }
      else
      {
        IEnumerator e = component.InitGachaDetail(details.title, details.bodies);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        Singleton<CommonRoot>.GetInstance().isLoading = false;
        Singleton<CommonRoot>.GetInstance().loadingMode = 0;
      }
    }
  }
}
