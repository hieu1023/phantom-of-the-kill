﻿// Decompiled with JetBrains decompiler
// Type: Bugu00527Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;
using System.Collections.Generic;

public class Bugu00527Scene : NGSceneBase
{
  public Bugu00527Menu menu;

  public override IEnumerator onInitSceneAsync()
  {
    yield break;
  }

  public static void ChangeScene(
    bool stack,
    List<InventoryItem> select,
    ItemInfo target,
    bool isSpecial)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("bugu005_drilling_material", (stack ? 1 : 0) != 0, (object) select, (object) target, (object) isSpecial);
  }

  public IEnumerator onStartSceneAsync(
    List<InventoryItem> select,
    ItemInfo target,
    bool isSpecial)
  {
    if (Singleton<NGGameDataManager>.GetInstance().IsColosseum)
      Singleton<CommonRoot>.GetInstance().SetFooterEnable(false);
    this.menu.SetFirstSelectItem(isSpecial ? Bugu00527Menu.DrillingType.Special : Bugu00527Menu.DrillingType.Normal, select, target);
    IEnumerator e = this.menu.Init();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public virtual void onStartScene(List<InventoryItem> select, ItemInfo target, bool isSpecial)
  {
    Singleton<PopupManager>.GetInstance().closeAll(false);
    Singleton<CommonRoot>.GetInstance().isActiveHeader = true;
    Singleton<CommonRoot>.GetInstance().isActiveFooter = true;
  }

  public override void onEndScene()
  {
    Persist.sortOrder.Flush();
    this.menu.onEndScene();
    ItemIcon.ClearCache();
    this.GetComponentInChildren<NGxScroll2>().scrollView.Press(false);
  }
}
