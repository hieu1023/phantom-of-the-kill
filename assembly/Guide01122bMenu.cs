﻿// Decompiled with JetBrains decompiler
// Type: Guide01122bMenu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;

public class Guide01122bMenu : Unit00493Menu
{
  public void SetNumber(UnitUnit unit)
  {
    this.TxtOwnednumber.SetTextLocalize("NO." + (unit.history_group_number % 1000).ToString().PadLeft(3, '0'));
  }

  public override void IbtnBack()
  {
    this.backScene();
  }
}
