﻿// Decompiled with JetBrains decompiler
// Type: Popup026872Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System.Collections;
using UnityEngine;

public class Popup026872Menu : Popup02610Base
{
  [SerializeField]
  private UILabel txtTitle;
  [SerializeField]
  private UILabel txtDescriptionYellow;
  [SerializeField]
  private UILabel txtDescriptionNormal;
  [SerializeField]
  private UIButton btnRareMadal;
  [SerializeField]
  private UIButton btnBattleMadal;

  public override void Init(WebAPI.Response.PvpBoot pvpInfo, Versus02610Menu menu)
  {
    base.Init(pvpInfo, menu);
    Consts instance = Consts.GetInstance();
    this.txtTitle.SetText(instance.VERSUS_0026872POPUP_TITLE);
    this.txtDescriptionYellow.SetText(instance.VERSUS_0026872POPUP_DESCRIPTION1);
    this.txtDescriptionNormal.SetText(instance.VERSUS_0026872POPUP_DESCRIPTION2);
    this.btnRareMadal.GetComponent<BoxCollider>().enabled = pvpInfo.medal_shop_is_available;
    this.btnBattleMadal.GetComponent<BoxCollider>().enabled = pvpInfo.battle_medal_shop_is_available && Player.Current.GetReleaseColosseum();
  }

  public override void IbtnNo()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<PopupManager>.GetInstance().onDismiss();
    Singleton<NGGameDataManager>.GetInstance().StartCoroutine(this.Return2688Popup());
  }

  public override void onBackButton()
  {
    this.IbtnNo();
  }

  public void IbtnKiseki()
  {
    if (this.IsPushAndSet())
      return;
    this.StartCoroutine(this.ExchangeTicketPopup());
  }

  private IEnumerator ExchangeTicketPopup()
  {
    Popup026872Menu popup026872Menu = this;
    Future<GameObject> pF = Res.Prefabs.popup.popup_026_8_7_3__anim_popup01.Load<GameObject>();
    IEnumerator e = pF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Singleton<PopupManager>.GetInstance().onDismiss();
    Singleton<PopupManager>.GetInstance().open(pF.Result, false, false, false, true, false, false, "SE_1006").GetComponent<Popup026873Menu>().Init(popup026872Menu.pvpInfo, popup026872Menu.menu);
  }

  public void IbtnRareMedal()
  {
    if (this.IsPushAndSet())
      return;
    Shop00741Scene.changeScene(true);
    Singleton<PopupManager>.GetInstance().onDismiss();
  }

  public void IbtnBattleMedal()
  {
    if (this.IsPushAndSet())
      return;
    Shop00743Scene.changeScene(true);
    Singleton<PopupManager>.GetInstance().onDismiss();
  }
}
