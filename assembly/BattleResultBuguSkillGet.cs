﻿// Decompiled with JetBrains decompiler
// Type: BattleResultBuguSkillGet
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using System.Collections;
using UnityEngine;

public class BattleResultBuguSkillGet : MonoBehaviour
{
  private bool mIsAdd;
  private System.Action mOnClick;
  [SerializeField]
  private GameObject dyn_Bugu_Thum;
  [SerializeField]
  private GameObject dyn_SkillDetail;
  [SerializeField]
  private Animator anime;
  [SerializeField]
  private UILabel skillNameLabel;
  [SerializeField]
  private UILabel skillDescriptionLabel;
  [SerializeField]
  private Transform skillIconRoot;
  [SerializeField]
  private Transform skillProperty1Root;
  [SerializeField]
  private Transform skillProperty2Root;
  [SerializeField]
  private UILabel txt_skillLv;

  public IEnumerator Init(bool isAdd, GameCore.ItemInfo gear, GearGearSkill skill)
  {
    this.mIsAdd = isAdd;
    Future<GameObject> itemIconF = Res.Prefabs.ItemIcon.prefab.Load<GameObject>();
    IEnumerator e = itemIconF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    e = itemIconF.Result.CloneAndGetComponent<ItemIcon>(this.dyn_Bugu_Thum).InitByItemInfo(gear);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Future<GameObject> battleSkillIconF = Res.Prefabs.BattleSkillIcon._battleSkillIcon.Load<GameObject>();
    e = battleSkillIconF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    e = battleSkillIconF.Result.CloneAndGetComponent<BattleSkillIcon>(this.skillIconRoot.gameObject).Init(skill.skill);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Future<GameObject> skillGenreIconF = Res.Icons.SkillGenreIcon.Load<GameObject>();
    e = skillGenreIconF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    skillGenreIconF.Result.CloneAndGetComponent<SkillGenreIcon>(this.skillProperty1Root.gameObject).Init(skill.skill.genre1);
    skillGenreIconF.Result.CloneAndGetComponent<SkillGenreIcon>(this.skillProperty2Root.gameObject).Init(skill.skill.genre2);
    this.skillNameLabel.SetTextLocalize(skill.skill.name);
    this.skillDescriptionLabel.SetTextLocalize(skill.skill.description);
    this.txt_skillLv.SetText(skill.skill_level.ToString() + "/" + (object) skill.skill.upper_level);
  }

  public void doStart()
  {
    string stateName = "Bugu_NewSkill_Update";
    if (this.mIsAdd)
      stateName = "Bugu_NewSkill";
    this.anime.Play(stateName);
  }

  public void onClick()
  {
    this.mOnClick();
    Singleton<PopupManager>.GetInstance().dismiss(false);
  }

  public void SetCallback(System.Action f)
  {
    this.mOnClick = f;
  }
}
