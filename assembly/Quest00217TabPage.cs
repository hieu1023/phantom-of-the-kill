﻿// Decompiled with JetBrains decompiler
// Type: Quest00217TabPage
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;
using System;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Quest00217TabPage : MonoBehaviour
{
  private List<Quest00217TabPage.Cell> cells_ = new List<Quest00217TabPage.Cell>();
  public List<Quest00217TabPage.RequestParam> RequestList = new List<Quest00217TabPage.RequestParam>();
  private int id_;
  [SerializeField]
  private SpreadColorButton mTabButton;
  [SerializeField]
  private GameObject mScrollContainer;
  [SerializeField]
  private UIScrollView mScrollView;
  [SerializeField]
  private UIGrid mGrid;
  private Quest00217Menu mQuestMenu;

  public int ID
  {
    get
    {
      return this.id_;
    }
  }

  public void Init(Quest00217Menu menu, int id)
  {
    this.mQuestMenu = menu;
    this.id_ = id;
    this.mGrid.onReposition = new UIGrid.OnReposition(this.onGridReposition);
    this.RequestList.Clear();
  }

  public void AddRequest(Quest00217Scroll.Parameter param, GameObject prefab)
  {
    this.RequestList.Add(new Quest00217TabPage.RequestParam()
    {
      ScrollEventParam = param,
      Prefab = prefab
    });
  }

  public void AddRequest(EventInfo param, GameObject prefab)
  {
    this.RequestList.Add(new Quest00217TabPage.RequestParam()
    {
      ScrollHuntingParam = param,
      Prefab = prefab
    });
  }

  public void AddRequest(TowerPeriod param, GameObject prefab)
  {
    this.RequestList.Add(new Quest00217TabPage.RequestParam()
    {
      ScrollTowerParam = param,
      Prefab = prefab
    });
  }

  public void AddRequest(GameObject prefab)
  {
    this.RequestList.Add(new Quest00217TabPage.RequestParam()
    {
      Prefab = prefab
    });
  }

  public void SetGridReposition()
  {
    this.mScrollView.ResetPosition();
  }

  private void onGridReposition()
  {
    this.mScrollView.ResetPosition();
  }

  public void SetButtonActive(bool active)
  {
    if (active)
      this.setTabButtonColor(Color.white);
    else
      this.setTabButtonColor(Color.gray);
  }

  public void SetPageActive(bool active)
  {
    this.SetButtonActive(active);
    this.mScrollContainer.gameObject.SetActive(active);
  }

  public void AddItem(GameObject obj)
  {
    this.setItem(obj);
  }

  public void UpdateTime(DateTime serverTime)
  {
    foreach (Quest00217Scroll quest00217Scroll in this.getItems())
      quest00217Scroll.SetTime(serverTime, quest00217Scroll.RankingEventTerm);
  }

  public void OnPushTab()
  {
    this.mQuestMenu.OnPushPageTab(this.ID);
  }

  private List<Quest00217Scroll> getItems()
  {
    return this.getItems<Quest00217Scroll>();
  }

  private void setTabButtonColor(Color color)
  {
    this.mTabButton.defaultColor = this.mTabButton.pressed = this.mTabButton.hover = color;
    this.mTabButton.SetTweenColor(false, 0.0f, color);
    foreach (UIWidget componentsInChild in this.mTabButton.GetComponentsInChildren<UISprite>())
      componentsInChild.color = color;
  }

  public List<Quest00217TabPage.Cell> units
  {
    get
    {
      return this.cells_;
    }
  }

  public List<GameObject> items
  {
    get
    {
      return this.cells_.Select<Quest00217TabPage.Cell, GameObject>((Func<Quest00217TabPage.Cell, GameObject>) (u => u.item)).ToList<GameObject>();
    }
  }

  public List<T> getItems<T>()
  {
    return this.items.Select<GameObject, T>((Func<GameObject, T>) (i => i.GetComponent<T>())).Where<T>((Func<T, bool>) (c => (object) c != null)).ToList<T>();
  }

  public int setItem(GameObject item)
  {
    if ((UnityEngine.Object) item == (UnityEngine.Object) null)
      return -1;
    Transform transform = item.transform;
    transform.parent = this.mGrid.gameObject.transform;
    transform.localPosition = Vector3.zero;
    transform.localRotation = Quaternion.identity;
    transform.localScale = Vector3.one;
    int count = this.cells_.Count;
    this.cells_.Add(new Quest00217TabPage.Cell(item));
    return count;
  }

  public bool deleteItem(int index)
  {
    if (index < 0 || this.cells_.Count <= index)
      return false;
    UnityEngine.Object.Destroy((UnityEngine.Object) this.cells_[index].item);
    this.cells_.RemoveAt(index);
    this.mGrid.Reposition();
    return true;
  }

  public bool deleteItem(GameObject item)
  {
    for (int index = 0; index < this.cells_.Count; ++index)
    {
      Quest00217TabPage.Cell cell = this.cells_[index];
      if ((UnityEngine.Object) cell.item == (UnityEngine.Object) item)
      {
        UnityEngine.Object.Destroy((UnityEngine.Object) cell.item);
        this.cells_.RemoveAt(index);
        this.mGrid.Reposition();
        return true;
      }
    }
    return false;
  }

  public void deleteItemAll()
  {
    if (this.cells_.Count == 0)
      return;
    foreach (UnityEngine.Object @object in this.items)
      UnityEngine.Object.Destroy(@object);
    this.cells_.Clear();
    this.mGrid.Reposition();
  }

  public Vector2 calcSimpleWidgetSize()
  {
    return new Vector2(this.mGrid.cellWidth, Mathf.Abs(this.mGrid.gameObject.transform.localPosition.y) + this.mGrid.cellHeight * (float) this.cells_.Count);
  }

  public Vector2 calcWidgetSize()
  {
    return (Vector2) NGUIMath.CalculateRelativeWidgetBounds(this.gameObject.transform).size;
  }

  public class Cell
  {
    private GameObject item_;
    private bool initialized_;
    private Bounds bounds_;
    private bool inside_;

    public GameObject item
    {
      get
      {
        return this.item_;
      }
    }

    public bool initialized
    {
      get
      {
        return this.initialized_;
      }
    }

    public Bounds bounds
    {
      get
      {
        if (!this.initialized_)
        {
          this.initialized_ = true;
          this.bounds_ = NGUIMath.CalculateRelativeWidgetBounds(this.item_.transform);
        }
        return this.bounds_;
      }
    }

    public bool inside
    {
      get
      {
        return this.inside_;
      }
    }

    public void setInside(bool flagIn)
    {
      this.inside_ = flagIn;
    }

    public Cell(GameObject item)
    {
      this.item_ = item;
      this.initialized_ = false;
      this.inside_ = item.activeSelf;
    }
  }

  public class RequestParam
  {
    public bool IsCreated;
    public Quest00217Scroll.Parameter ScrollEventParam;
    public EventInfo ScrollHuntingParam;
    public TowerPeriod ScrollTowerParam;
    public GameObject Prefab;
  }
}
