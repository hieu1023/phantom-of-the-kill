﻿// Decompiled with JetBrains decompiler
// Type: Guide0112BuguDetailB
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using UnityEngine;

public class Guide0112BuguDetailB : Bugu00561Menu
{
  private Guide01142bMenu menu;
  [SerializeField]
  protected UILabel TxtNumber;
  [SerializeField]
  protected GameObject dirNumber;
  [HideInInspector]
  public int index;
  private bool isDispNumber;
  private GearGear actualGear;

  public void Init(Guide01142bMenu m, GearGear gear, bool dispN)
  {
    this.menu = m;
    this.actualGear = gear;
    this.isDispNumber = dispN;
    this.SetNumber(gear, this.isDispNumber);
  }

  public void SetNumber(GearGear gear, bool isDispNumber)
  {
    this.dirNumber.SetActive(isDispNumber);
    this.TxtNumber.SetTextLocalize("NO." + (gear.history_group_number % 1000).ToString().PadLeft(3, '0'));
  }

  public void SetContainerPosition()
  {
    Transform childInFind1 = this.transform.GetChildInFind("Middle");
    childInFind1.transform.localPosition = new Vector3(0.0f, (float) (((double) this.menu.scroll.scrollView.panel.GetViewSize().y - (double) childInFind1.GetComponent<UIWidget>().height) / 2.0), 0.0f);
    Transform childInFind2 = this.transform.GetChildInFind("Bottom");
    childInFind2.transform.localPosition = new Vector3(0.0f, (float) -(((double) this.menu.scroll.scrollView.panel.GetViewSize().y - (double) childInFind2.GetComponent<UIWidget>().height) / 2.0), 0.0f);
  }

  public void SetGearInformation()
  {
    this.SetNumber(this.actualGear, this.isDispNumber);
    if ((Object) this.dirNumber != (Object) null)
      this.dirNumber.SetActive(this.isDispNumber);
    this.menu.SetTitleText(this.actualGear.name);
  }
}
