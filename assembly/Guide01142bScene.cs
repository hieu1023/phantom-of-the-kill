﻿// Decompiled with JetBrains decompiler
// Type: Guide01142bScene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using System.Collections;

public class Guide01142bScene : NGSceneBase
{
  public Guide01142bMenu menu;

  public IEnumerator onStartSceneAsync(GearGear gear, bool isDispNumber, int index)
  {
    IEnumerator e = this.menu.onStartSceneAsync(gear, isDispNumber, index);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public IEnumerator onStartSceneAsync(GearGear[] gears, bool isDispNumber, int index = 0)
  {
    IEnumerator e = this.menu.onStartSceneAsync(gears, isDispNumber, index);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }
}
