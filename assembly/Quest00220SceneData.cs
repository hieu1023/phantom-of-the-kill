﻿// Decompiled with JetBrains decompiler
// Type: Quest00220SceneData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

public class Quest00220SceneData
{
  public int L;
  public int M;
  public int S;
  public bool Forcus;
  public bool Guerrilla;

  public Quest00220SceneData(int l, int m, int s, bool forcus, bool guerrilla)
  {
    this.L = l;
    this.M = m;
    this.S = s;
    this.Forcus = forcus;
    this.Guerrilla = guerrilla;
  }
}
