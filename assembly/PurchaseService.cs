﻿// Decompiled with JetBrains decompiler
// Type: PurchaseService
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class PurchaseService : MonoBehaviour
{
  public static bool Initialized { get; private set; }

  public static PurchaseService Instance { get; private set; }

  private static string GameObjectName { get; set; }

  public static void CreateInstance()
  {
    if (!((UnityEngine.Object) PurchaseService.Instance == (UnityEngine.Object) null))
      return;
    GameObject gameObject = new GameObject("GSCC.PurchaseService");
    gameObject.hideFlags = HideFlags.HideAndDontSave;
    UnityEngine.Object.DontDestroyOnLoad((UnityEngine.Object) gameObject);
    gameObject.AddComponent<PurchaseService>();
  }

  private void Awake()
  {
    PurchaseService.Instance = this;
    PurchaseService.GameObjectName = this.gameObject.name;
  }

  public void Init(string[] productIds, PurchaseKit.Logger logger, IntPtr nativeLogger)
  {
    PurchaseBridge.unity_purchasekit_attach(PurchaseService.GameObjectName, PurchaseBridge.SetLogger(logger), nativeLogger);
    if (productIds != null)
      PurchaseBridge.purchasekit_initWithProducts(productIds, productIds.Length);
    else
      PurchaseBridge.purchasekit_init();
  }

  private void __PurchaseKit__onInitResult__(string message)
  {
    IntPtr int64 = (IntPtr) Convert.ToInt64(message, 16);
    PurchaseBridge.UnmanagedResult structure = PurchaseBridge.MarshalSupport.ToStructure<PurchaseBridge.UnmanagedResult>(int64);
    PurchaseBridge.unity_purchasekit_purge_cs_init_message(ref int64);
    PurchaseKit.Listener.OnInitResult((int) structure.resultCode);
  }

  private void __PurchaseKit__onProductResult__(string message)
  {
    IntPtr int64 = (IntPtr) Convert.ToInt64(message, 16);
    PurchaseBridge.UnmanagedResult structure = PurchaseBridge.MarshalSupport.ToStructure<PurchaseBridge.UnmanagedResult>(int64);
    PurchaseKit.ProductResponse response = structure.response > 0UL ? new PurchaseKit.ProductResponse((IntPtr) (long) structure.response) : (PurchaseKit.ProductResponse) null;
    PurchaseBridge.unity_purchasekit_purge_cs_product_message(ref int64);
    PurchaseKit.Listener.OnProductResult((int) structure.resultCode, response);
  }

  private void __PurchaseKit__onPurchaseResult__(string message)
  {
    IntPtr int64 = (IntPtr) Convert.ToInt64(message, 16);
    PurchaseBridge.UnmanagedResult structure = PurchaseBridge.MarshalSupport.ToStructure<PurchaseBridge.UnmanagedResult>(int64);
    PurchaseKit.PurchaseResponse response = structure.response > 0UL ? new PurchaseKit.PurchaseResponse((IntPtr) (long) structure.response) : (PurchaseKit.PurchaseResponse) null;
    PurchaseBridge.unity_purchasekit_purge_cs_purchase_message(ref int64);
    PurchaseKit.Listener.OnPurchaseResult((int) structure.resultCode, response);
  }
}
