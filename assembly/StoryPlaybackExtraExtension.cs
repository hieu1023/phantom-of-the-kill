﻿// Decompiled with JetBrains decompiler
// Type: StoryPlaybackExtraExtension
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using System;
using System.Collections.Generic;
using UniLinq;

public static class StoryPlaybackExtraExtension
{
  public static StoryPlaybackExtra[] DisplayList(
    this StoryPlaybackExtra[] self,
    DateTime serverTime)
  {
    return ((IEnumerable<StoryPlaybackExtra>) self).Where<StoryPlaybackExtra>((Func<StoryPlaybackExtra, bool>) (x =>
    {
      if (!x.display_expire_at.HasValue)
        return true;
      if (!x.display_expire_at.HasValue)
        return false;
      DateTime? displayExpireAt = x.display_expire_at;
      DateTime dateTime = serverTime;
      return displayExpireAt.HasValue && displayExpireAt.GetValueOrDefault() > dateTime;
    })).ToArray<StoryPlaybackExtra>();
  }
}
