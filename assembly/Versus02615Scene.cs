﻿// Decompiled with JetBrains decompiler
// Type: Versus02615Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;
using System.Collections;
using UnityEngine;

public class Versus02615Scene : NGSceneBase
{
  [SerializeField]
  private Versus02615Menu menu;

  public static void ChangeScene(
    bool stack,
    RankingGroup[] ranking_data,
    WebAPI.Response.PvpBootCampaign_rewards[] campaign_rewards)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("versus026_15", (stack ? 1 : 0) != 0, (object) ranking_data, (object) campaign_rewards);
  }

  public IEnumerator onStartSceneAsync(
    RankingGroup[] ranking_data,
    WebAPI.Response.PvpBootCampaign_rewards[] campaign_rewards)
  {
    yield return (object) this.menu.Init(ranking_data, campaign_rewards);
  }
}
