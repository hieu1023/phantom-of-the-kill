﻿// Decompiled with JetBrains decompiler
// Type: Bugu005ReisouListMenu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Bugu005ReisouListMenu : Bugu005ItemListMenuBase
{
  private bool needClearCache = true;
  [SerializeField]
  protected UILabel TxtNumberPattern1;
  [SerializeField]
  protected UIButton BtnSort;
  [SerializeField]
  protected GameObject DirBottomBtn;
  [SerializeField]
  protected GameObject dirNoItem;
  protected new GameObject reisouPopupPrefab;
  private List<int> equipedReisouIdList;

  public new virtual IEnumerator Init()
  {
    this.BtnSort.gameObject.SetActive(false);
    this.DirBottomBtn.SetActive(false);
    yield return (object) base.Init();
  }

  public override Persist<Persist.ItemSortAndFilterInfo> GetPersist()
  {
    return Persist.bugu0052SortAndFilter;
  }

  protected override List<PlayerItem> GetItemList()
  {
    List<PlayerItem> playerItemList = new List<PlayerItem>();
    this.equipedReisouIdList = new List<int>();
    foreach (PlayerItem playerItem in SMManager.Get<PlayerItem[]>())
    {
      if (playerItem.isWeapon())
      {
        if (playerItem.equipped_reisou_player_gear_id != 0)
          this.equipedReisouIdList.Add(playerItem.equipped_reisou_player_gear_id);
      }
      else if (playerItem.isReisou())
        playerItemList.Add(playerItem);
    }
    if ((UnityEngine.Object) this.dirNoItem != (UnityEngine.Object) null)
      this.dirNoItem.SetActive(playerItemList.Count <= 0);
    return playerItemList;
  }

  protected override long GetRevisionItemList()
  {
    return SMManager.Revision<PlayerItem[]>();
  }

  protected override void BottomInfoUpdate()
  {
    InventoryItem[] array = this.InventoryItems.Where<InventoryItem>((Func<InventoryItem, bool>) (x => x.Item != null && !x.removeButton && x.Item.isReisou)).ToArray<InventoryItem>();
    SMManager.Get<Player>();
    int num = 500;
    this.TxtNumberPattern1.SetTextLocalize(Consts.Format(Consts.GetInstance().GEAR_0052_POSSESSION, (IDictionary) new Hashtable()
    {
      {
        (object) "now",
        (object) ((IEnumerable<InventoryItem>) array).Count<InventoryItem>()
      },
      {
        (object) "max",
        (object) num
      }
    }));
  }

  protected virtual void OnEnable()
  {
    if (!this.scroll.scrollView.isDragging)
      return;
    this.scroll.scrollView.Press(false);
  }

  protected override void CreateItemIconAdvencedSetting(int inventoryItemIdx, int allItemIdx)
  {
    ItemIcon itemIcon = this.AllItemIcon[allItemIdx];
    InventoryItem displayItem = this.DisplayItems[inventoryItemIdx];
    itemIcon.onClick = (System.Action<ItemIcon>) (playeritem => this.OpenReisouDetailPopup(playeritem.ItemInfo));
    if (displayItem.Item.isSupply || displayItem.Item.isExchangable || (displayItem.Item.isCompse || displayItem.Item.isWeaponMaterial))
    {
      itemIcon.QuantitySupply = true;
      itemIcon.EnableQuantity(displayItem.Item.quantity);
    }
    else
      itemIcon.QuantitySupply = false;
    displayItem.Item.ForBattle = this.equipedReisouIdList.FirstOrDefault<int>((Func<int, bool>) (x => x == itemIcon.ItemInfo.itemID)) > 0;
    itemIcon.ForBattle = displayItem.Item.ForBattle;
    itemIcon.Favorite = displayItem.Item.favorite;
    itemIcon.Gray = false;
    itemIcon.SelectedQuantity(0);
    itemIcon.Deselect();
    itemIcon.EnableLongPressEvent(new System.Action<GameCore.ItemInfo>(this.OpenReisouDetailPopup));
  }

  protected new void OpenReisouDetailPopup(GameCore.ItemInfo item)
  {
    if (this.IsPushAndSet())
      return;
    this.StartCoroutine(this.OpenReisouDetailPopupAsync(item));
  }

  protected new IEnumerator OpenReisouDetailPopupAsync(GameCore.ItemInfo item)
  {
    Bugu005ReisouListMenu bugu005ReisouListMenu = this;
    if (item != null)
    {
      IEnumerator e;
      if ((UnityEngine.Object) bugu005ReisouListMenu.reisouPopupPrefab == (UnityEngine.Object) null)
      {
        Future<GameObject> popupPrefabF = new ResourceObject("Prefabs/UnitGUIs/PopupReisouSkillDetails").Load<GameObject>();
        e = popupPrefabF.Wait();
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        bugu005ReisouListMenu.reisouPopupPrefab = popupPrefabF.Result;
        popupPrefabF = (Future<GameObject>) null;
      }
      GameObject popup = bugu005ReisouListMenu.reisouPopupPrefab.Clone((Transform) null);
      PopupReisouDetails script = popup.GetComponent<PopupReisouDetails>();
      popup.SetActive(false);
      e = script.Init(item, (PlayerItem) null, true);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      popup.SetActive(true);
      Singleton<PopupManager>.GetInstance().open(popup, false, false, true, true, false, false, "SE_1006");
      yield return (object) null;
      script.scrollResetPosition();
      bugu005ReisouListMenu.IsPushOff();
    }
  }

  public override void Sort(
    ItemSortAndFilter.SORT_TYPES type,
    SortAndFilter.SORT_TYPE_ORDER_BUY order,
    bool isEquipFirst)
  {
    this.CurrentSortType = type;
    if ((UnityEngine.Object) this.SortSprite != (UnityEngine.Object) null)
      this.SortSprite = ItemSortAndFilter.SortSpriteLabel(type, this.SortSprite);
    this.DisplayItems = this.InventoryItems.SortByReisou().ToList<InventoryItem>();
    this.scroll.Reset();
    this.AllItemIcon.ForEach((System.Action<ItemIcon>) (x =>
    {
      x.transform.parent = this.transform;
      x.gameObject.SetActive(false);
    }));
    for (int index = 0; index < Mathf.Min(this.iconMaxValue, this.DisplayItems.Count); ++index)
    {
      this.scroll.Add(this.AllItemIcon[index].gameObject, this.iconWidth, this.iconHeight, false);
      this.AllItemIcon[index].gameObject.SetActive(true);
    }
    this.InventoryItems.ForEach((System.Action<InventoryItem>) (v => v.icon = (ItemIcon) null));
    this.StartCoroutine(this.CreateItemIconRange(Mathf.Min(this.iconMaxValue, this.DisplayItems.Count)));
    this.scroll.CreateScrollPoint(this.iconHeight, this.DisplayItems.Count);
    this.scroll.ResolvePosition();
  }

  public void onBackScene()
  {
    if ((UnityEngine.Object) this.SortPopupPrefab != (UnityEngine.Object) null)
      this.SortPopupPrefab.GetComponent<ItemSortAndFilter>().Initialize((Bugu005ItemListMenuBase) this, false);
    this.Sort(this.SortCategory, this.OrderBuySort, this.isEquipFirst);
    this.needClearCache = true;
  }

  public override void onEndScene()
  {
    base.onEndScene();
    Persist.sortOrder.Flush();
    if (!this.needClearCache)
      return;
    ItemIcon.ClearCache();
  }

  public void IbtnSell()
  {
    if (this.IsPushAndSet())
      return;
    this.needClearCache = false;
    Bugu00525Scene.ChangeScene(true, Bugu00525Scene.Mode.Reisou);
  }
}
