﻿// Decompiled with JetBrains decompiler
// Type: BattleUnitController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleUnitController : BattleMonoBehaviour
{
  private BL.UnitPosition moveUnit;
  private BL.Panel startPanel;
  private BL.Panel lastTouchPanel;
  private BattleCameraController cameraController;
  private BattleTimeManager btm;
  private BattleInputObserver _inputObserver;

  public bool isMoving
  {
    get
    {
      return this.moveUnit != null;
    }
  }

  private BattleInputObserver inputObserver
  {
    get
    {
      if ((Object) this._inputObserver == (Object) null)
        this._inputObserver = this.battleManager.getController<BattleInputObserver>();
      return this._inputObserver;
    }
  }

  protected override IEnumerator Start_Battle()
  {
    // ISSUE: reference to a compiler-generated field
    int num = this.\u003C\u003E1__state;
    BattleUnitController battleUnitController = this;
    if (num != 0)
      return false;
    // ISSUE: reference to a compiler-generated field
    this.\u003C\u003E1__state = -1;
    battleUnitController.cameraController = battleUnitController.GetComponent<BattleCameraController>();
    battleUnitController.btm = battleUnitController.battleManager.getManager<BattleTimeManager>();
    return false;
  }

  private bool isDispositionMode
  {
    get
    {
      return this.inputObserver.isDispositionMode;
    }
  }

  protected override void Update_Battle()
  {
    if (!this.battleManager.isBattleEnable || this.isDispositionMode || !this.isMoving)
      return;
    this.cameraController.setLookAtTarget(this.env.unitResource[this.moveUnit.unit].gameObject.transform.position, false);
  }

  public bool onPressMoveUnit(BL.UnitPosition up)
  {
    if (this.env.core.isAutoBattle.value || !up.unit.isPlayerControl || (up.isCompleted || !this.env.core.currentPhaseUnitp(up)))
      return false;
    if (this.moveUnit == up)
      return true;
    this.btm.setCurrentUnit(up.unit, 0.1f, false);
    this.moveUnit = up;
    this.lastTouchPanel = this.env.core.getFieldPanel(up, false);
    this.startPanel = this.env.core.getFieldPanel(up.originalRow, up.originalColumn);
    return true;
  }

  private void cleanup()
  {
    this.moveUnit = (BL.UnitPosition) null;
  }

  public void onReleaseMoveUnit()
  {
    this.cleanup();
  }

  public void onCancel()
  {
    if (this.moveUnit != null)
      this.moveUnit.cancelMove(this.env);
    this.cleanup();
  }

  public bool onDrag(BL.Panel panel)
  {
    if (this.moveUnit != null && panel != null && panel != this.lastTouchPanel)
    {
      HashSet<BL.Panel> movePanels = this.inputObserver.getMovePanels(this.moveUnit);
      this.lastTouchPanel = panel;
      HashSet<BL.Panel> completePanels = this.inputObserver.isDispositionMode ? BattleFuncs.moveCompletePanels_(movePanels, this.moveUnit.unit, false, false) : this.moveUnit.completePanels;
      if (completePanels.Contains(panel))
      {
        if (this.env.core.getRouteNonCache(this.moveUnit, this.startPanel, panel, movePanels, completePanels) != null)
          this.moveUnit.startMoveRoute(this.env.core.getRouteNonCache(this.moveUnit, this.env.core.getFieldPanel(this.moveUnit.row, this.moveUnit.column), panel, movePanels, completePanels), this.battleManager.defaultUnitSpeed, this.env);
        return true;
      }
    }
    return false;
  }

  public bool isMoveUnit(GameObject o)
  {
    BL.Unit unit = this.env.core.unitCurrent.unit;
    return this.battleManager.isBattleEnable && unit != (BL.Unit) null && ((Object) this.env.unitResource[unit].gameObject == (Object) o && !this.env.core.isCompleted(unit)) && this.env.core.currentPhaseUnitp(unit) && !unit.IsDontMove;
  }
}
