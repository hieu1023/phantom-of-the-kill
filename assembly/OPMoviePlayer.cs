﻿// Decompiled with JetBrains decompiler
// Type: OPMoviePlayer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using CriMana;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OPMoviePlayer
{
  private const string movieAsset = "op_fix_800x450.mp4";
  public const string toNextScene = "startup000_6";

  public IEnumerator Attach()
  {
    string str = Application.streamingAssetsPath + "/op_fix_800x450.mp4";
    string filePath = new Uri(str).AbsoluteUri;
    Debug.Log((object) ("play : " + filePath));
    StatusBarHelper.SetVisibilityForIPhoneX(false);
    WindowsMovieController wmc = (UnityEngine.Object.Instantiate(Resources.Load("Prefabs/WindowsMovie")) as GameObject).GetComponent<WindowsMovieController>();
    wmc.ShowMovie(str);
    yield return (object) new WaitForEndOfFrame();
    while (wmc.movieScreen.player.status != Player.Status.Stop)
      yield return (object) new WaitForEndOfFrame();
    StatusBarHelper.SetVisibilityForIPhoneX(true);
    Debug.Log((object) ("play end : " + filePath));
    SceneManager.LoadScene("startup000_6");
  }
}
