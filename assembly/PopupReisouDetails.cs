﻿// Decompiled with JetBrains decompiler
// Type: PopupReisouDetails
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System.Collections;
using System.Collections.Generic;
using UnitStatusInformation;
using UnityEngine;

public class PopupReisouDetails : BackButtonPopupBase
{
  [SerializeField]
  protected GearKindIcon iconGearKind;
  [SerializeField]
  protected UILabel txtReisouName;
  [SerializeField]
  protected UI2DSprite slcRarityStar;
  [SerializeField]
  protected UILabel txtReisouRank;
  [SerializeField]
  protected UISprite slcReisouGauge;
  [SerializeField]
  protected GameObject slcReisouGaugeBase;
  [SerializeField]
  protected UIScrollView scrollview;
  [SerializeField]
  protected UIGrid grid;
  protected GameObject prefabReisouSkillDetail01;
  protected GameObject prefabReisouSkillDetail02;
  private const int paramSkillDispCntMax = 12;

  public IEnumerator Init(ItemInfo item, PlayerItem playerItem = null, bool isDispRank = true)
  {
    Future<GameObject> prefabF = new ResourceObject("Prefabs/UnitGUIs/ReisouSkillDetail_01").Load<GameObject>();
    IEnumerator e = prefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.prefabReisouSkillDetail01 = prefabF.Result;
    prefabF = (Future<GameObject>) null;
    prefabF = new ResourceObject("Prefabs/UnitGUIs/ReisouSkillDetail_02").Load<GameObject>();
    e = prefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.prefabReisouSkillDetail02 = prefabF.Result;
    prefabF = (Future<GameObject>) null;
    this.iconGearKind.Init(item.gear.kind, CommonElement.none);
    this.txtReisouName.SetTextLocalize(item.name);
    RarityIcon.SetRarity(item.gear, this.slcRarityStar);
    if (!isDispRank)
    {
      this.txtReisouRank.gameObject.SetActive(false);
      this.slcReisouGauge.gameObject.SetActive(false);
      this.slcReisouGaugeBase.gameObject.SetActive(false);
    }
    else
    {
      this.txtReisouRank.SetTextLocalize(Consts.GetInstance().UNIT_00443_REISOU_RANK.F((object) item.gearLevel, (object) item.gearLevelLimit));
      float num = (float) this.slcReisouGauge.width * ((float) item.gearExp / (float) (item.gearExpNext + item.gearExp));
      if ((double) num == 0.0 || item.gearExpNext + item.gearExp == 0)
      {
        this.slcReisouGauge.gameObject.SetActive(false);
      }
      else
      {
        this.slcReisouGauge.gameObject.SetActive(true);
        this.slcReisouGauge.width = (int) num;
      }
    }
    yield return (object) this.setParamSkillDetail(item, playerItem);
    yield return (object) this.setSkillDetail(item);
  }

  protected IEnumerator setParamSkillDetail(ItemInfo item, PlayerItem playerItem = null)
  {
    PlayerItem playerItem1;
    if (item.playerItem != (PlayerItem) null)
      playerItem1 = item.playerItem;
    else if (!(playerItem != (PlayerItem) null))
      yield break;
    else
      playerItem1 = playerItem;
    Judgement.GearParameter gearParam = Judgement.GearParameter.FromPlayerGear(playerItem1);
    Queue<string> paramTextQueue = new Queue<string>();
    this.SetParamSkillStrings(paramTextQueue, playerItem1, gearParam);
    if (paramTextQueue.Count > 0)
    {
      int page = paramTextQueue.Count / 12 + 1;
      for (int i = 0; i < page; ++i)
      {
        List<string> paramTextList = new List<string>();
        for (int index = 0; index < 12 && paramTextQueue.Count > 0; ++index)
          paramTextList.Add(paramTextQueue.Dequeue());
        yield return (object) this.prefabReisouSkillDetail02.Clone(this.grid.transform).GetComponent<ReisouSkillDetail_02>().Init(this.scrollview, paramTextList);
      }
    }
  }

  protected void SetParamSkillStrings(
    Queue<string> paramTextQueue,
    PlayerItem playerItem,
    Judgement.GearParameter gearParam)
  {
    if (gearParam.Hp > 0)
    {
      string str = Consts.GetInstance().UNIT_0044_REISOU_HP.F((object) gearParam.Hp);
      paramTextQueue.Enqueue(str);
    }
    if (gearParam.Strength > 0)
    {
      string str = Consts.GetInstance().UNIT_0044_REISOU_POWER.F((object) gearParam.Strength);
      paramTextQueue.Enqueue(str);
    }
    if (gearParam.Intelligence > 0)
    {
      string str = Consts.GetInstance().UNIT_0044_REISOU_MAGIC_POWER.F((object) gearParam.Intelligence);
      paramTextQueue.Enqueue(str);
    }
    int vitalityIncremental = playerItem.vitality_incremental;
    if (vitalityIncremental > 0)
    {
      string str = Consts.GetInstance().UNIT_0044_REISOU_VITALITY.F((object) vitalityIncremental);
      paramTextQueue.Enqueue(str);
    }
    if (gearParam.Mind > 0)
    {
      string str = Consts.GetInstance().UNIT_0044_REISOU_MIND.F((object) gearParam.Mind);
      paramTextQueue.Enqueue(str);
    }
    if (gearParam.Agility > 0)
    {
      string str = Consts.GetInstance().UNIT_0044_REISOU_AGILITY.F((object) gearParam.Agility);
      paramTextQueue.Enqueue(str);
    }
    if (gearParam.Dexterity > 0)
    {
      string str = Consts.GetInstance().UNIT_0044_REISOU_DEXTERITY.F((object) gearParam.Dexterity);
      paramTextQueue.Enqueue(str);
    }
    if (gearParam.Luck > 0)
    {
      string str = Consts.GetInstance().UNIT_0044_REISOU_LUCK.F((object) gearParam.Luck);
      paramTextQueue.Enqueue(str);
    }
    if (gearParam.PhysicalPower > 0)
    {
      string str = Consts.GetInstance().UNIT_0044_REISOU_PHY_POW.F((object) gearParam.PhysicalPower);
      paramTextQueue.Enqueue(str);
    }
    if (gearParam.MagicalPower > 0)
    {
      string str = Consts.GetInstance().UNIT_0044_REISOU_MAG_POW.F((object) gearParam.MagicalPower);
      paramTextQueue.Enqueue(str);
    }
    if (gearParam.PhysicalDefense > 0)
    {
      string str = Consts.GetInstance().UNIT_0044_REISOU_PHY_DEF.F((object) gearParam.PhysicalDefense);
      paramTextQueue.Enqueue(str);
    }
    if (gearParam.MagicDefense > 0)
    {
      string str = Consts.GetInstance().UNIT_0044_REISOU_MAG_DEF.F((object) gearParam.MagicDefense);
      paramTextQueue.Enqueue(str);
    }
    if (gearParam.Hit > 0)
    {
      string str = Consts.GetInstance().UNIT_0044_REISOU_HIT.F((object) gearParam.Hit);
      paramTextQueue.Enqueue(str);
    }
    if (gearParam.Critical > 0)
    {
      string str = Consts.GetInstance().UNIT_0044_REISOU_CRITICAL.F((object) gearParam.Critical);
      paramTextQueue.Enqueue(str);
    }
    int evasion = playerItem.evasion;
    if (evasion <= 0)
      return;
    string str1 = Consts.GetInstance().UNIT_0044_REISOU_EVASION.F((object) evasion);
    paramTextQueue.Enqueue(str1);
  }

  protected IEnumerator setSkillDetail(ItemInfo item)
  {
    List<PopupSkillDetails.Param> objList = new List<PopupSkillDetails.Param>();
    List<GearGearSkill> skillList = new List<GearGearSkill>();
    List<GearGearSkill> releaseSkillList = new List<GearGearSkill>();
    foreach (List<GearGearSkill> rememberSkill in item.gear.rememberSkills)
    {
      foreach (GearGearSkill gearGearSkill in rememberSkill)
      {
        skillList.Add(gearGearSkill);
        PopupSkillDetails.Param obj = new PopupSkillDetails.Param(gearGearSkill.skill, UnitParameter.SkillGroup.Reisou, new int?(gearGearSkill.skill_level));
        objList.Add(obj);
        if (gearGearSkill.release_rank <= item.gearLevel)
          releaseSkillList.Add(gearGearSkill);
      }
    }
    PopupSkillDetails.Param[] skillParamsArray = objList.ToArray();
    for (int i = 0; i < skillList.Count; ++i)
    {
      GearGearSkill skill = skillList[i];
      bool is_release = false;
      foreach (GearGearSkill gearGearSkill in releaseSkillList)
      {
        if (gearGearSkill.ID == skill.ID)
        {
          is_release = true;
          break;
        }
      }
      yield return (object) this.prefabReisouSkillDetail01.Clone(this.grid.transform).GetComponent<ReisouSkillDetail_01>().Init(this.scrollview, skill, skillParamsArray, is_release);
    }
  }

  public void scrollResetPosition()
  {
    this.grid.Reposition();
    this.scrollview.ResetPosition();
  }

  public override void onBackButton()
  {
    this.onClickedClose();
  }

  public void onClickedClose()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<PopupManager>.GetInstance().dismiss(false);
  }
}
