﻿// Decompiled with JetBrains decompiler
// Type: Popup004813Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;
using UnityEngine;

public class Popup004813Menu : BackButtonMenuBase
{
  [SerializeField]
  protected UILabel txtDescription1;
  private System.Action callback;

  public void SetText(int medal)
  {
    this.txtDescription1.SetText(Consts.Format(Consts.GetInstance().POPUP_004813_DESCRIPT_TEXT, (IDictionary) new Hashtable()
    {
      {
        (object) "Count",
        (object) medal.ToLocalizeNumberText()
      }
    }));
  }

  public void SetIbtnOKCallback(System.Action callback)
  {
    this.callback = callback;
  }

  public virtual void IbtnPopupOk()
  {
    if (this.IsPushAndSet())
      return;
    if (this.callback != null)
      this.callback();
    Singleton<PopupManager>.GetInstance().onDismiss();
  }

  public override void onBackButton()
  {
    this.IbtnPopupOk();
  }
}
