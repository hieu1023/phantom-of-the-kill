﻿// Decompiled with JetBrains decompiler
// Type: Gsc.Purchase.API.Request.ProductList
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using Gsc.Auth;
using System.Collections.Generic;

namespace Gsc.Purchase.API.Request
{
  public class ProductList : Gsc.Network.Request<ProductList, Gsc.Purchase.API.Response.ProductList>
  {
    private const string ___path = "{0}/{1}/products";

    public override string GetPath()
    {
      return string.Format("{0}/{1}/products", (object) SDK.Configuration.Env.PurchaseApiPrefix, (object) Device.Platform);
    }

    public override string GetMethod()
    {
      return "GET";
    }

    protected override Dictionary<string, object> GetParameters()
    {
      return (Dictionary<string, object>) null;
    }
  }
}
