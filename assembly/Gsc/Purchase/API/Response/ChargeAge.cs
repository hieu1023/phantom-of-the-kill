﻿// Decompiled with JetBrains decompiler
// Type: Gsc.Purchase.API.Response.ChargeAge
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using Gsc.Network.Support.MiniJsonHelper;
using System;
using System.Collections.Generic;

namespace Gsc.Purchase.API.Response
{
  public class ChargeAge : Gsc.Network.Response<ChargeAge>
  {
    public int Age { get; private set; }

    public ChargeAge(byte[] payload)
    {
      Dictionary<string, object> result = Gsc.Network.Response<ChargeAge>.GetResult(payload);
      this.Age = Deserializer.Instance.Add<int>(new Func<object, int>(Deserializer.ToIntegerType.int32)).Deserialize<int>(result["age"]);
    }
  }
}
