﻿// Decompiled with JetBrains decompiler
// Type: Gsc.Purchase.API.Response.Fulfillment
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using Gsc.Network.Support.MiniJsonHelper;
using System;
using System.Collections.Generic;

namespace Gsc.Purchase.API.Response
{
  public class Fulfillment : Gsc.Network.Response<Fulfillment>
  {
    public FulfillmentResult Result { get; private set; }

    public Fulfillment(byte[] payload)
    {
      Dictionary<string, object> result = Gsc.Network.Response<Fulfillment>.GetResult(payload);
      this.Result = Deserializer.Instance.Add<FulfillmentResult>(new Func<object, FulfillmentResult>(Deserializer.ToObject<FulfillmentResult>)).Deserialize<FulfillmentResult>((object) result);
    }
  }
}
