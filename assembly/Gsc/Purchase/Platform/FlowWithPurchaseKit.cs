﻿// Decompiled with JetBrains decompiler
// Type: Gsc.Purchase.Platform.FlowWithPurchaseKit
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using Gsc.Core;
using Gsc.Network;
using Gsc.Purchase.API.Response;
using System;
using UnityEngine;

namespace Gsc.Purchase.Platform
{
  public abstract class FlowWithPurchaseKit : IPurchaseFlowImpl, IPurchaseListener
  {
    protected readonly PurchaseHandler handler;

    public FlowWithPurchaseKit(PurchaseHandler handler)
    {
      this.handler = handler;
    }

    public virtual void Init(string[] productIds)
    {
      GameObject instance1 = NativeRootObject.Instance;
      PurchaseService instance2 = PurchaseService.Instance;
      PurchaseKit.Init(productIds, (IPurchaseListener) this, instance1, instance2, new PurchaseKit.Logger(PurchaseHandler.Log), IntPtr.Zero);
    }

    public virtual void Resume()
    {
      PurchaseKit.Resume();
    }

    public virtual bool Purchase(ProductInfo product)
    {
      return PurchaseKit.Purchase(product.ProductId);
    }

    public virtual bool Confirmed()
    {
      return false;
    }

    public virtual void UpdateProducts(string[] productIds)
    {
      PurchaseKit.UpdateProducts(productIds);
    }

    public virtual void Consume(string transactionId)
    {
      PurchaseKit.Consume(transactionId);
    }

    private static ResultCode GetResultCode(int resultCode)
    {
      switch (resultCode)
      {
        case 0:
          return ResultCode.Succeeded;
        case 2:
          return ResultCode.Unavailabled;
        case 16:
          return ResultCode.Canceled;
        case 17:
          return ResultCode.AlreadyOwned;
        case 32:
          return ResultCode.Deferred;
        default:
          return ResultCode.Failed;
      }
    }

    public virtual void OnInitResult(int resultCode)
    {
      this.handler.OnInitResult(FlowWithPurchaseKit.GetResultCode(resultCode));
    }

    public virtual void OnProductResult(int resultCode, PurchaseKit.ProductResponse response)
    {
      if (resultCode == 0 && response != null)
      {
        ProductInfo[] productInfos = new ProductInfo[response.Values.Length];
        for (int index = 0; index < response.Values.Length; ++index)
        {
          PurchaseKit.ProductData productData = response.Values[index];
          productInfos[index] = new ProductInfo(productData.ID, productData.LocalizedTitle, productData.LocalizedDescription, productData.LocalizedPrice, productData.Currency, (float) productData.Price);
        }
        this.handler.OnProductResult(ResultCode.Succeeded, productInfos);
      }
      else
        this.handler.OnProductResult(FlowWithPurchaseKit.GetResultCode(resultCode), (ProductInfo[]) null);
    }

    public virtual void OnPurchaseResult(int resultCode, PurchaseKit.PurchaseResponse response)
    {
      if (resultCode == 0 && response != null)
        this.CreateFulfillmentTask(response);
      else
        this.handler.OnPurchaseResult(FlowWithPurchaseKit.GetResultCode(resultCode), (FulfillmentResult) null);
    }

    protected void OnFulfillmentResponse(Fulfillment response, IErrorResponse error)
    {
      if (error != null)
        this.handler.OnPurchaseResult(ResultCode.AlreadyOwned, (FulfillmentResult) null);
      else
        this.handler.OnPurchaseResult(ResultCode.Succeeded, response.Result);
    }

    protected abstract IWebTask CreateFulfillmentTask(PurchaseKit.PurchaseResponse response);
  }
}
