﻿// Decompiled with JetBrains decompiler
// Type: Gsc.Auth.GAuth.GAuth.API.Response.AccessToken
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using Gsc.Network.Support.MiniJsonHelper;
using System;
using System.Collections.Generic;

namespace Gsc.Auth.GAuth.GAuth.API.Response
{
  public class AccessToken : Gsc.Network.Response<AccessToken>
  {
    public string Token { get; private set; }

    public int ExpiresIn { get; private set; }

    public AccessToken(byte[] payload)
    {
      Dictionary<string, object> result = Gsc.Network.Response<AccessToken>.GetResult(payload);
      this.Token = Deserializer.Instance.Add<string>(new Func<object, string>(Deserializer.To<string>)).Deserialize<string>(result["access_token"]);
      this.ExpiresIn = Deserializer.Instance.Add<int>(new Func<object, int>(Deserializer.ToIntegerType.int32)).Deserialize<int>(result["expires_in"]);
    }
  }
}
