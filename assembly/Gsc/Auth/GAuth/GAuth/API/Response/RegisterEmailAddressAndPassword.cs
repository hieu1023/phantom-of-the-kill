﻿// Decompiled with JetBrains decompiler
// Type: Gsc.Auth.GAuth.GAuth.API.Response.RegisterEmailAddressAndPassword
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using Gsc.Network.Support.MiniJsonHelper;
using System;
using System.Collections.Generic;

namespace Gsc.Auth.GAuth.GAuth.API.Response
{
  public class RegisterEmailAddressAndPassword : Gsc.Network.Response<RegisterEmailAddressAndPassword>
  {
    public bool IsSuccess { get; private set; }

    public RegisterEmailAddressAndPassword(byte[] payload)
    {
      Dictionary<string, object> result = Gsc.Network.Response<RegisterEmailAddressAndPassword>.GetResult(payload);
      this.IsSuccess = Deserializer.Instance.Add<bool>(new Func<object, bool>(Deserializer.To<bool>)).Deserialize<bool>(result["is_success"]);
    }
  }
}
