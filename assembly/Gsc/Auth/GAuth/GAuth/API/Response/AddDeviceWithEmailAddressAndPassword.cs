﻿// Decompiled with JetBrains decompiler
// Type: Gsc.Auth.GAuth.GAuth.API.Response.AddDeviceWithEmailAddressAndPassword
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using Gsc.Network.Support.MiniJsonHelper;
using System;
using System.Collections.Generic;

namespace Gsc.Auth.GAuth.GAuth.API.Response
{
  public class AddDeviceWithEmailAddressAndPassword : Gsc.Network.Response<AddDeviceWithEmailAddressAndPassword>
  {
    public string DeviceId { get; private set; }

    public string SecretKey { get; private set; }

    public AddDeviceWithEmailAddressAndPassword(byte[] payload)
    {
      Dictionary<string, object> result = Gsc.Network.Response<AddDeviceWithEmailAddressAndPassword>.GetResult(payload);
      this.DeviceId = Deserializer.Instance.Add<string>(new Func<object, string>(Deserializer.To<string>)).Deserialize<string>(result["device_id"]);
      this.SecretKey = Deserializer.Instance.Add<string>(new Func<object, string>(Deserializer.To<string>)).Deserialize<string>(result["secret_key"]);
    }
  }
}
