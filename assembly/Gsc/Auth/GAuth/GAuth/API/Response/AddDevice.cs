﻿// Decompiled with JetBrains decompiler
// Type: Gsc.Auth.GAuth.GAuth.API.Response.AddDevice
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using Gsc.Network.Support.MiniJsonHelper;
using System;
using System.Collections.Generic;

namespace Gsc.Auth.GAuth.GAuth.API.Response
{
  public class AddDevice : Gsc.Network.Response<AddDevice>
  {
    public bool IsSucceeded { get; private set; }

    public AddDevice(byte[] payload)
    {
      Dictionary<string, object> result = Gsc.Network.Response<AddDevice>.GetResult(payload);
      this.IsSucceeded = Deserializer.Instance.Add<bool>(new Func<object, bool>(Deserializer.To<bool>)).Deserialize<bool>(result["is_succeeded"]);
    }
  }
}
