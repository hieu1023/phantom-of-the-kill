﻿// Decompiled with JetBrains decompiler
// Type: Gsc.Auth.GAuth.GAuth.API.Response.ErrorResponse
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using Gsc.DOM;
using Gsc.DOM.Json;
using Gsc.Network;

namespace Gsc.Auth.GAuth.GAuth.API.Response
{
  public class ErrorResponse : Gsc.Network.Response<ErrorResponse>, IErrorResponse, IResponse
  {
    public string ErrorCode { get; private set; }

    public Document data { get; private set; }

    IDocument IErrorResponse.data
    {
      get
      {
        return (IDocument) this.data;
      }
    }

    public ErrorResponse(byte[] payload)
    {
      this.data = Document.Parse(payload);
      Value root = this.data.Root;
      string valueByPointer = root.GetValueByPointer("/code", (string) null);
      if (valueByPointer == null)
      {
        root = this.data.Root;
        valueByPointer = root.GetValueByPointer("/error_code", (string) null);
      }
      this.ErrorCode = valueByPointer;
    }
  }
}
