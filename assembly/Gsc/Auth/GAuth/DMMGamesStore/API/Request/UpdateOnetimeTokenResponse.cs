﻿// Decompiled with JetBrains decompiler
// Type: Gsc.Auth.GAuth.DMMGamesStore.API.Request.UpdateOnetimeTokenResponse
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using Gsc.Network.Support.MiniJsonHelper;
using System;
using System.Collections.Generic;

namespace Gsc.Auth.GAuth.DMMGamesStore.API.Request
{
  public class UpdateOnetimeTokenResponse : Gsc.Network.Response<UpdateOnetimeTokenResponse>
  {
    public string OnetimeToken { get; private set; }

    public UpdateOnetimeTokenResponse(byte[] payload)
    {
      Dictionary<string, object> result = Gsc.Network.Response<UpdateOnetimeTokenResponse>.GetResult(payload);
      this.OnetimeToken = Deserializer.Instance.Add<string>(new Func<object, string>(Deserializer.To<string>)).Deserialize<string>(result["dmm_onetime_token"]);
    }
  }
}
