﻿// Decompiled with JetBrains decompiler
// Type: Gsc.DOM.Json.Mutable.Value
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using Gsc.Network;
using System.Runtime.InteropServices;

namespace Gsc.DOM.Json.Mutable
{
  [StructLayout(LayoutKind.Sequential, Size = 1)]
  public struct Value
  {
    public void SetNull()
    {
    }

    public void SetObject()
    {
    }

    public void SetArray()
    {
    }

    public void Set(IRequestObject value)
    {
    }

    public void Set(bool value)
    {
    }

    public void Set(string value)
    {
    }

    public void Set(int value)
    {
    }

    public void Set(uint value)
    {
    }

    public void Set(long value)
    {
    }

    public void Set(ulong value)
    {
    }

    public void Set(float value)
    {
    }

    public void Set(double value)
    {
    }
  }
}
