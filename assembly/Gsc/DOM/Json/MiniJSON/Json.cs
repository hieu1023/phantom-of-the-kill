﻿// Decompiled with JetBrains decompiler
// Type: Gsc.DOM.Json.MiniJSON.Json
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

namespace Gsc.DOM.Json.MiniJSON
{
  public static class Json
  {
    public static object Deserialize(string text)
    {
      using (Document document = Document.Parse(text))
        return Gsc.DOM.MiniJSON.Json.Deserialize((IValue) document.Root);
    }

    public static object Deserialize(byte[] bytes)
    {
      using (Document document = Document.Parse(bytes))
        return Gsc.DOM.MiniJSON.Json.Deserialize((IValue) document.Root);
    }

    public static object Deserialize(Value value)
    {
      return Gsc.DOM.MiniJSON.Json.Deserialize((IValue) value);
    }
  }
}
