﻿// Decompiled with JetBrains decompiler
// Type: Gsc.DOM.Json.Object
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace Gsc.DOM.Json
{
  public struct Object : IObject, IEnumerable<IMember>, IEnumerable, IEnumerable<Member>
  {
    private readonly rapidjson.Object value;

    public int MemberCount
    {
      get
      {
        return this.value.MemberCount;
      }
    }

    public Object(rapidjson.Object value)
    {
      this.value = value;
    }

    public bool HasMember(string name)
    {
      return this.value.HasMember(name);
    }

    public bool TryGetValue(string name, out Value value)
    {
      rapidjson.Value obj;
      int num = this.value.TryGetValue(name, out obj) ? 1 : 0;
      value = new Value(obj);
      return num != 0;
    }

    bool IObject.TryGetValue(string name, out IValue value)
    {
      rapidjson.Value obj;
      int num = this.value.TryGetValue(name, out obj) ? 1 : 0;
      value = (IValue) new Value(obj);
      return num != 0;
    }

    public IEnumerator<Member> GetEnumerator()
    {
      foreach (KeyValuePair<string, rapidjson.Value> keyValuePair in this.value)
        yield return new Member(keyValuePair.Key, new Value(keyValuePair.Value));
    }

    IEnumerator<IMember> IEnumerable<IMember>.GetEnumerator()
    {
      foreach (KeyValuePair<string, rapidjson.Value> keyValuePair in this.value)
        yield return (IMember) new Member(keyValuePair.Key, new Value(keyValuePair.Value));
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
      return (IEnumerator) this.GetEnumerator();
    }

    public Value this[string name]
    {
      get
      {
        return new Value(this.value[name]);
      }
    }

    IValue IObject.this[string name]
    {
      get
      {
        return (IValue) this[name];
      }
    }
  }
}
