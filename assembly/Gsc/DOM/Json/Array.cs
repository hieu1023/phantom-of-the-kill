﻿// Decompiled with JetBrains decompiler
// Type: Gsc.DOM.Json.Array
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace Gsc.DOM.Json
{
  public struct Array : IArray, IEnumerable<IValue>, IEnumerable, IEnumerable<Value>
  {
    private readonly rapidjson.Array value;

    public int Length
    {
      get
      {
        return this.value.Length;
      }
    }

    public Array(rapidjson.Array value)
    {
      this.value = value;
    }

    public IEnumerator<Value> GetEnumerator()
    {
      foreach (rapidjson.Value obj in this.value)
        yield return new Value(obj);
    }

    IEnumerator<IValue> IEnumerable<IValue>.GetEnumerator()
    {
      foreach (rapidjson.Value obj in this.value)
        yield return (IValue) new Value(obj);
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
      return (IEnumerator) this.GetEnumerator();
    }

    public Value this[int index]
    {
      get
      {
        return new Value(this.value[index]);
      }
    }

    IValue IArray.this[int index]
    {
      get
      {
        return (IValue) this[index];
      }
    }
  }
}
