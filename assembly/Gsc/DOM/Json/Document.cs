﻿// Decompiled with JetBrains decompiler
// Type: Gsc.DOM.Json.Document
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace Gsc.DOM.Json
{
  public class Document : IDocument, IDisposable
  {
    private readonly rapidjson.Document document;
    private readonly Value root;

    public static Document Parse(byte[] bytes)
    {
      return new Document(rapidjson.Document.Parse(bytes));
    }

    public static Document Parse(string text)
    {
      return new Document(rapidjson.Document.Parse(text));
    }

    public static Document ParseFromFile(string filepath)
    {
      return new Document(rapidjson.Document.ParseFromFile(filepath));
    }

    public Value Root
    {
      get
      {
        return this.root;
      }
    }

    IValue IDocument.Root
    {
      get
      {
        return (IValue) this.root;
      }
    }

    public Document(Document document, ref Value root)
    {
      this.document = document.document;
      this.root = root;
    }

    private Document(rapidjson.Document document)
    {
      this.document = document;
      this.root = new Value(document.Root);
    }

    ~Document()
    {
      this.Dispose();
    }

    public void Dispose()
    {
      this.document.Dispose();
    }
  }
}
