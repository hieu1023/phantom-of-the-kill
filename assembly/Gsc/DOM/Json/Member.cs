﻿// Decompiled with JetBrains decompiler
// Type: Gsc.DOM.Json.Member
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

namespace Gsc.DOM.Json
{
  public struct Member : IMember
  {
    private readonly string name;
    private readonly Value value;

    public string Name
    {
      get
      {
        return this.name;
      }
    }

    public Value Value
    {
      get
      {
        return this.value;
      }
    }

    IValue IMember.Value
    {
      get
      {
        return (IValue) this.value;
      }
    }

    public Member(string name, Value value)
    {
      this.name = name;
      this.value = value;
    }
  }
}
