﻿// Decompiled with JetBrains decompiler
// Type: Gsc.Network.EnvLoader`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using DeviceKit;
using Gsc.DOM.Json;
using System.Linq;

namespace Gsc.Network
{
  public class EnvLoader<T> : Request<EnvLoader<T>, EnvLoader<T>.Response> where T : struct, Configuration.IEnvironment
  {
    private string url;
    private static string cachedVersion;

    public EnvLoader(string url, string version)
    {
      this.url = url;
      EnvLoader<T>.cachedVersion = version;
    }

    public override string GetMethod()
    {
      return "GET";
    }

    public override string GetPath()
    {
      return (string) null;
    }

    public override string GetUrl()
    {
      return this.url;
    }

    public class Response : Gsc.Network.Response<EnvLoader<T>.Response>
    {
      public string EnvName { get; private set; }

      public Configuration.IEnvironment Env { get; private set; }

      public Response(WebInternalResponse response)
      {
        string str = EnvLoader<T>.cachedVersion ?? App.GetBundleVersion();
        EnvLoader<T>.cachedVersion = (string) null;
        using (Document document = Document.Parse(response.Payload))
        {
          string index = document.Root.GetValueByPointer("/ver_route/" + str, (string) null) ?? document.Root.GetValueByPointer("/ver_route/default", (string) null);
          Value obj1;
          if (!document.Root.GetObject().TryGetValue("environments", out obj1))
            return;
          Value obj2;
          if (index != null)
          {
            obj2 = obj1[index];
          }
          else
          {
            Member member = obj1.GetObject().First<Member>();
            index = member.Name;
            obj2 = member.Value;
          }
          T obj3 = new T();
          foreach (Member member in obj2.GetObject())
            obj3.SetValue(member.Name, member.Value.ToString());
          this.EnvName = index;
          this.Env = (Configuration.IEnvironment) obj3;
        }
      }
    }
  }
}
