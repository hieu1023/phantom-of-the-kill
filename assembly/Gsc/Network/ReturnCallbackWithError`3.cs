﻿// Decompiled with JetBrains decompiler
// Type: Gsc.Network.ReturnCallbackWithError`3
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

namespace Gsc.Network
{
  public delegate TResult ReturnCallbackWithError<TRequest, TResponse, TResult>(
    TRequest request,
    TResponse response,
    IErrorResponse error);
}
