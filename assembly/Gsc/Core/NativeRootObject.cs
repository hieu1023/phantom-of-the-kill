﻿// Decompiled with JetBrains decompiler
// Type: Gsc.Core.NativeRootObject
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Gsc.Core
{
  public class NativeRootObject : MonoBehaviour
  {
    public static GameObject Instance { get; private set; }

    public static void CreateInstance()
    {
      if (!((Object) NativeRootObject.Instance == (Object) null))
        return;
      GameObject gameObject = new GameObject("GSCC.NativeRootObject");
      gameObject.hideFlags = HideFlags.HideAndDontSave;
      Object.DontDestroyOnLoad((Object) gameObject);
      gameObject.AddComponent<NativeRootObject>();
    }

    private void Awake()
    {
      NativeRootObject.Instance = this.gameObject;
    }
  }
}
