﻿// Decompiled with JetBrains decompiler
// Type: SM_EnemyTopInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;
using System;
using System.Collections.Generic;
using UniLinq;

public static class SM_EnemyTopInfo
{
  public static IEnumerable<IGrouping<int, EnemyTopInfo>> GroupBy(
    this EnemyTopInfo[] self)
  {
    return ((IEnumerable<EnemyTopInfo>) self).GroupBy<EnemyTopInfo, int>((Func<EnemyTopInfo, int>) (x => MasterData.UnitUnit[x.unit_id].same_character_id));
  }
}
