﻿// Decompiled with JetBrains decompiler
// Type: Popup00591Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Popup00591Menu : BackButtonMonoBehaiviour
{
  private static readonly string COLOR_TAG_GREEN = "[00ff00]{0}[-]";
  private static readonly string COLOR_TAG_RED = "[ff0000]{0}[-]";
  [SerializeField]
  private UILabel m_Warning;
  [SerializeField]
  private UILabel m_Description;
  [SerializeField]
  private UILabel m_DescriptionReisou;
  [SerializeField]
  private UIGrid m_Grid;
  private System.Action m_YesCallback;

  public IEnumerator Init(
    GameCore.ItemInfo before,
    GameCore.ItemInfo after,
    GameCore.ItemInfo beforeReisou,
    GameCore.ItemInfo afterReisou,
    List<InventoryItem> materials,
    System.Action yesCallback,
    GameObject iconPrefab = null)
  {
    Consts consts = Consts.GetInstance();
    IEnumerator e;
    if ((UnityEngine.Object) iconPrefab == (UnityEngine.Object) null)
    {
      Future<GameObject> ItemIconF = Res.Prefabs.ItemIcon.prefab.Load<GameObject>();
      e = ItemIconF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      iconPrefab = ItemIconF.Result;
      ItemIconF = (Future<GameObject>) null;
    }
    this.m_YesCallback = yesCallback;
    foreach (InventoryItem material in materials)
    {
      e = iconPrefab.CloneAndGetComponent<ItemIcon>(this.m_Grid.transform).InitByItemInfo(material.Item);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    this.m_Grid.repositionNow = true;
    if (materials.Any<InventoryItem>((Func<InventoryItem, bool>) (x => x.Item.gear.rarity.index >= 3)))
      this.m_Warning.SetTextLocalize(consts.Popup00591DescriptionRareText);
    else
      this.m_Warning.SetTextLocalize(consts.Popup00591DescriptionNormalText);
    string str1 = after.gearLevel.ToString();
    if (before.gearLevel < after.gearLevel)
      str1 = Popup00591Menu.COLOR_TAG_GREEN.F((object) str1);
    else if (before.gearLevel > after.gearLevel)
      str1 = Popup00591Menu.COLOR_TAG_RED.F((object) str1);
    string str2 = after.gearLevelLimit.ToString();
    if (before.gearLevelLimit < after.gearLevelLimit)
      str2 = Popup00591Menu.COLOR_TAG_GREEN.F((object) str2);
    else if (before.gearLevelLimit > after.gearLevelLimit)
      str2 = Popup00591Menu.COLOR_TAG_RED.F((object) str2);
    this.m_Description.SetTextLocalize(consts.Popup00591DescriptionText.F((object) before.gearLevel, (object) before.gearLevelLimit, (object) str1, (object) str2));
    if (afterReisou != null)
    {
      string str3 = afterReisou.gearLevel.ToString();
      if (beforeReisou.gearLevel < afterReisou.gearLevel)
        str3 = Popup00591Menu.COLOR_TAG_GREEN.F((object) str3);
      else if (beforeReisou.gearLevel > afterReisou.gearLevel)
        str3 = Popup00591Menu.COLOR_TAG_RED.F((object) str3);
      string str4 = afterReisou.gearLevelLimit.ToString();
      if (beforeReisou.gearLevelLimit < afterReisou.gearLevelLimit)
        str4 = Popup00591Menu.COLOR_TAG_GREEN.F((object) str4);
      else if (beforeReisou.gearLevelLimit > afterReisou.gearLevelLimit)
        str4 = Popup00591Menu.COLOR_TAG_RED.F((object) str4);
      this.m_DescriptionReisou.SetTextLocalize(consts.Popup00591ReisouDescriptionText.F((object) beforeReisou.gearLevel, (object) beforeReisou.gearLevelLimit, (object) str3, (object) str4));
    }
    else
      this.m_DescriptionReisou.gameObject.SetActive(false);
  }

  public void IbtnYes()
  {
    if (this.m_YesCallback == null)
      return;
    this.m_YesCallback();
  }

  public void IbtnNo()
  {
    Singleton<PopupManager>.GetInstance().onDismiss();
  }

  public override void onBackButton()
  {
    this.IbtnNo();
  }
}
