﻿// Decompiled with JetBrains decompiler
// Type: WWWUtil
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class WWWUtil
{
  public static IEnumerator RequestAndCache(
    string url,
    System.Action<Dictionary<string, object>> callback)
  {
    Texture2D texture = new Texture2D(0, 0);
    WWW www = new WWW(url);
    while (!www.isDone)
      yield return (object) null;
    if (string.IsNullOrEmpty(www.error))
    {
      texture = new Texture2D(www.texture.width, www.texture.height, www.texture.format, false);
      www.LoadImageIntoTexture(texture);
    }
    callback(new Dictionary<string, object>()
    {
      {
        "www",
        (object) www
      },
      {
        "texture",
        (object) texture
      }
    });
  }
}
