﻿// Decompiled with JetBrains decompiler
// Type: Story0090Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections;

public class Story0090Scene : NGSceneBase
{
  public Story0090Menu menu;

  public override IEnumerator onInitSceneAsync()
  {
    return base.onInitSceneAsync();
  }

  public IEnumerator onStartSceneAsync()
  {
    this.menu.Init();
    if (Singleton<CommonRoot>.GetInstance().isLoading)
    {
      yield return (object) null;
      Singleton<CommonRoot>.GetInstance().isLoading = false;
    }
  }
}
