﻿// Decompiled with JetBrains decompiler
// Type: AddStatus
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System;
using System.Collections.Generic;
using UnityEngine;

public class AddStatus : MonoBehaviour
{
  [SerializeField]
  private GameObject[] BG;
  [SerializeField]
  private UILabel TxtCategory;
  [SerializeField]
  private UILabel TxtOperator;
  [SerializeField]
  private UILabel[] TxtStatus;

  public void Init(string category, int value)
  {
    AddStatus.Type type = value > 0 ? AddStatus.Type.ADD : AddStatus.Type.SUB;
    ((IEnumerable<GameObject>) this.BG).ToggleOnce((int) type);
    this.TxtCategory.SetText(category);
    foreach (Component txtStatu in this.TxtStatus)
      txtStatu.gameObject.SetActive(false);
    if (type == AddStatus.Type.ADD)
    {
      this.TxtOperator.SetText(Consts.GetInstance().PLUS);
      this.TxtStatus[(int) type].SetTextLocalize(Math.Abs(value));
      this.TxtStatus[(int) type].gameObject.SetActive(true);
    }
    else
    {
      this.TxtOperator.SetText(Consts.GetInstance().MINUS);
      this.TxtStatus[(int) type].SetTextLocalize(Math.Abs(value));
      this.TxtStatus[(int) type].gameObject.SetActive(true);
    }
  }

  private enum Type
  {
    SUB,
    ADD,
  }
}
