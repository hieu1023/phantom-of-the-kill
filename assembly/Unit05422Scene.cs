﻿// Decompiled with JetBrains decompiler
// Type: Unit05422Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;
using System.Collections;

public class Unit05422Scene : NGSceneBase
{
  public Unit05422Menu menu;

  public static void changeScene(bool stack, PlayerUnit playerUnit)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("unit054_2_2", (stack ? 1 : 0) != 0, (object) playerUnit);
  }

  public IEnumerator onStartSceneAsync(PlayerUnit playerUnit)
  {
    IEnumerator e = this.menu.Init(playerUnit);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }
}
