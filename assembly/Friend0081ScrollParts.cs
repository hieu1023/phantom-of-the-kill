﻿// Decompiled with JetBrains decompiler
// Type: Friend0081ScrollParts
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Friend0081ScrollParts : FriendScrollBar
{
  [SerializeField]
  private UIButton button;

  public void Set(int index, Friend0081Menu menu)
  {
    EventDelegate.Set(this.button.onClick, new EventDelegate.Callback(((FriendScrollBar) this).onDetails));
    this.index = index;
    this.menu = menu;
  }
}
