﻿// Decompiled with JetBrains decompiler
// Type: Explore033TopMenu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;
using UnityEngine;

public class Explore033TopMenu : BackButtonMenuBase
{
  [SerializeField]
  private GameObject mTransitionAnchor;
  [SerializeField]
  private GameObject mTransitionFullAnchor;
  [SerializeField]
  private ExploreFooter mExploreFooter;
  [SerializeField]
  private UILabel mTxtNumDungeon;
  [SerializeField]
  private UILabel mTxtTitleDungeon;
  [SerializeField]
  private UILabel mTxtTicketNum;
  [SerializeField]
  private UIButton mFloorSelectButton;
  [SerializeField]
  private Explore033MiniMap mMiniMap;
  private GameObject mDeckEditPopupPrefab;
  private GameObject mRankingPopupPrefab;
  private GameObject mRewardBoxPopupPrefab;
  private GameObject mFloorSelectPopupPrefab;

  public IEnumerator InitAsync()
  {
    yield return (object) this.loadDeckEditPopup();
    yield return (object) this.loadRewardBoxPopup();
    yield return (object) this.loadFloorSelectPopup();
    yield return (object) this.setupTransitionParts();
  }

  public IEnumerator onStartSceneAsync()
  {
    Explore033TopMenu explore033TopMenu = this;
    ExploreDataManager dataMgr = Singleton<ExploreDataManager>.GetInstance();
    yield return (object) explore033TopMenu.mExploreFooter.UpdateDeckUnitIconsAsync();
    explore033TopMenu.mTxtNumDungeon.SetTextLocalize(dataMgr.NowFloor);
    explore033TopMenu.mTxtTitleDungeon.SetTextLocalize(dataMgr.FloorData.name);
    int challengePoint = Singleton<NGGameDataManager>.GetInstance().challenge_point;
    explore033TopMenu.mTxtTicketNum.SetTextLocalize(challengePoint);
    if (challengePoint == 0)
      explore033TopMenu.mTxtTicketNum.color = Color.red;
    else
      explore033TopMenu.mTxtTicketNum.color = Color.white;
    if (MasterData.ExploreFloor[dataMgr.FrontFloorId].floor == 1)
      explore033TopMenu.mFloorSelectButton.isEnabled = false;
    explore033TopMenu.mMiniMap.UpdateFloorData();
    yield return (object) explore033TopMenu.mExploreFooter.Initialize();
    Singleton<ExploreSceneManager>.GetInstance().TopMenu = explore033TopMenu;
    Singleton<ExploreSceneManager>.GetInstance().Footer = explore033TopMenu.mExploreFooter;
  }

  public IEnumerator onBackSceneAsync()
  {
    yield return (object) this.mExploreFooter.UpdateDeckUnitIconsAsync();
  }

  public override void onBackButton()
  {
    if (this.IsPushAndSet())
      return;
    this.backScene();
  }

  protected override void backScene()
  {
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    NGSceneManager instance = Singleton<NGSceneManager>.GetInstance();
    instance.destroyCurrentScene();
    instance.clearStack();
    MypageScene.ChangeScene(false, false, false);
  }

  public void onHelpButton()
  {
    if (this.IsPushAndSet())
      return;
    Help0152Scene.ChangeScene(true, MasterData.HelpCategory[30]);
  }

  public void onRewardButton()
  {
    if (this.IsPushAndSet())
      return;
    Explore033RankingRewardScene.changeScene();
  }

  public void onDeckEditButton()
  {
    if (this.IsPushAndSet())
      return;
    this.StartCoroutine(this.openDeckEditPopup());
  }

  public void onRankingButton()
  {
    if (this.IsPushAndSet())
      return;
    this.StartCoroutine(this.openRankingPopup());
  }

  public void onRewardBoxButton()
  {
    if (this.IsPushAndSet())
      return;
    this.StartCoroutine(this.openRewardBoxPopup());
  }

  public void onFloorSelectButton()
  {
    if (this.IsPushAndSet())
      return;
    this.StartCoroutine(this.openFloorSelectPopup());
  }

  private IEnumerator setupTransitionParts()
  {
    Future<GameObject> loader = Singleton<ResourceManager>.GetInstance().Load<GameObject>("Prefabs/explore033_Top/dir_transition", 1f);
    IEnumerator e = loader.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    GameObject result = loader.Result;
    ExploreSceneManager instance = Singleton<ExploreSceneManager>.GetInstance();
    instance.ScreenEffect.SetTransitionObject(result.Clone(this.mTransitionAnchor.transform));
    instance.ScreenEffect.SetTransitionFullObject(result.Clone(this.mTransitionFullAnchor.transform));
  }

  private IEnumerator loadDeckEditPopup()
  {
    Future<GameObject> loader = Singleton<ResourceManager>.GetInstance().Load<GameObject>("Prefabs/explore033_Top/popup_ExploreDeckSelect_anim_popup", 1f);
    IEnumerator e = loader.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.mDeckEditPopupPrefab = loader.Result;
  }

  private IEnumerator openDeckEditPopup()
  {
    GameObject popup = Singleton<PopupManager>.GetInstance().open(this.mDeckEditPopupPrefab, false, false, false, true, true, true, "SE_1006");
    IEnumerator e = popup.GetComponent<ExploreDeckEditPopup>().InitializeAsync();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Singleton<PopupManager>.GetInstance().startOpenAnime(popup, false);
  }

  private IEnumerator loadRankingPopup()
  {
    Future<GameObject> loader = Singleton<ResourceManager>.GetInstance().Load<GameObject>("Prefabs/explore033_Ranking/popup_ExploreRanking_anim_popup", 1f);
    IEnumerator e = loader.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.mRankingPopupPrefab = loader.Result;
  }

  private IEnumerator openRankingPopup()
  {
    Singleton<PopupManager>.GetInstance().open((GameObject) null, false, false, false, true, true, false, "SE_1006");
    GameObject clone = this.mRankingPopupPrefab.Clone((Transform) null);
    clone.SetActive(false);
    ExploreRankingPopup exploreRankingAnim = clone.GetComponent<ExploreRankingPopup>();
    yield return (object) exploreRankingAnim.Initialize();
    Singleton<PopupManager>.GetInstance().dismiss(false);
    clone.SetActive(true);
    Singleton<PopupManager>.GetInstance().open(clone, false, false, true, true, false, false, "SE_1006");
    exploreRankingAnim.setRankingInfo();
  }

  private IEnumerator loadRewardBoxPopup()
  {
    Future<GameObject> loader = Singleton<ResourceManager>.GetInstance().Load<GameObject>("Prefabs/explore033_Top/popup_ExploreRewardBox_anim_popup", 1f);
    IEnumerator e = loader.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.mRewardBoxPopupPrefab = loader.Result;
  }

  private IEnumerator openRewardBoxPopup()
  {
    ExploreRewardBoxPopup exploreRewardBoxAnim = Singleton<PopupManager>.GetInstance().open(this.mRewardBoxPopupPrefab, false, false, false, true, false, false, "SE_1006").GetComponent<ExploreRewardBoxPopup>();
    IEnumerator e = exploreRewardBoxAnim.Initialize();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    yield return (object) exploreRewardBoxAnim.updateScrollPosition();
  }

  private IEnumerator loadFloorSelectPopup()
  {
    Future<GameObject> loader = Singleton<ResourceManager>.GetInstance().Load<GameObject>("Prefabs/explore033_Top/popup_ExploreFloorSelect", 1f);
    IEnumerator e = loader.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.mFloorSelectPopupPrefab = loader.Result;
  }

  private IEnumerator openFloorSelectPopup()
  {
    IEnumerator e = Singleton<PopupManager>.GetInstance().open(this.mFloorSelectPopupPrefab, false, false, false, true, false, false, "SE_1006").GetComponent<ExploreFloorSelectPopup>().Initialize();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public IEnumerator ReopenPopup()
  {
    if (Singleton<ExploreDataManager>.GetInstance().IsPopupStateDeckEdit)
      yield return (object) this.openDeckEditPopup();
    else if (Singleton<ExploreDataManager>.GetInstance().IsPopupStateChallenge)
    {
      yield return (object) this.mExploreFooter.openChallengePopup();
    }
    else
    {
      Debug.Log((object) "invalid popupState");
      Singleton<ExploreDataManager>.GetInstance().InitReopenPopupState();
    }
    Singleton<CommonRoot>.GetInstance().isLoading = false;
    Singleton<ExploreSceneManager>.GetInstance().Pause(false);
  }
}
