﻿// Decompiled with JetBrains decompiler
// Type: PopupUnityValueDetail
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PopupUnityValueDetail : BackButtonPopupBase
{
  [SerializeField]
  private GameObject noAvailableQuest;
  [SerializeField]
  [Tooltip("合算値")]
  private UILabel txtTotal_;
  [SerializeField]
  [Tooltip("淘汰値")]
  private UILabel txtValue_;
  [SerializeField]
  [Tooltip("強化淘汰値")]
  private UILabel txtBuildup_;
  [SerializeField]
  private UIScrollView stageListScrollView;
  [SerializeField]
  private UIGrid stageListGrid;

  public static Future<GameObject>[] createLoaders(bool isSea)
  {
    Future<GameObject>[] futureArray = new Future<GameObject>[2];
    string str = isSea ? "_sea" : "";
    futureArray[0] = new ResourceObject("Prefabs/unit/Popup_UnitTouta_Detail" + str).Load<GameObject>();
    futureArray[1] = new ResourceObject("Prefabs/unit/dir_AvailableQuest_Menu" + str).Load<GameObject>();
    return futureArray;
  }

  public static void show(
    GameObject prefab,
    GameObject stageItemPrefab,
    float unityValue,
    float buildupUnity,
    UnitUnit baseUnit,
    System.Action beforeChangeSceneAction)
  {
    Singleton<PopupManager>.GetInstance().open(prefab, false, false, false, true, false, false, "SE_1006").GetComponent<PopupUnityValueDetail>().initalize(stageItemPrefab, unityValue, buildupUnity, baseUnit, beforeChangeSceneAction);
  }

  private void initalize(
    GameObject stageItemPrefab,
    float unityValue,
    float buildupUnity,
    UnitUnit baseUnit,
    System.Action beforeChangeSceneAction)
  {
    this.StartCoroutine(this.InitializeCoroutine(stageItemPrefab, unityValue, buildupUnity, baseUnit, beforeChangeSceneAction));
  }

  private IEnumerator InitializeCoroutine(
    GameObject stageItemPrefab,
    float unityValue,
    float buildupUnity,
    UnitUnit baseUnit,
    System.Action beforeChangeSceneAction)
  {
    PopupUnityValueDetail unityValueDetail = this;
    unityValueDetail.setTopObject(unityValueDetail.gameObject);
    float num1 = Mathf.Min(unityValue + buildupUnity, (float) PlayerUnit.GetUnityValueMax());
    unityValueDetail.txtTotal_.SetTextLocalize((double) num1 < 99.0 ? num1.ToString("f1") : num1.ToString());
    unityValueDetail.txtValue_.SetTextLocalize(unityValue.ToString());
    unityValueDetail.txtBuildup_.SetTextLocalize((double) buildupUnity < 99.0 ? buildupUnity.ToString("f1") : buildupUnity.ToString());
    List<UnitUnit> list1 = ((IEnumerable<UnitUnit>) MasterData.UnitUnitList).Where<UnitUnit>((Func<UnitUnit, bool>) (x => x.is_unity_value_up && x.FindValueUpPattern(baseUnit, (Func<UnitFamily[]>) (() => baseUnit.Families)) != null)).ToList<UnitUnit>();
    unityValueDetail.noAvailableQuest.SetActive(false);
    if (list1.Count > 0)
    {
      HashSet<int> source1 = new HashSet<int>();
      HashSet<int> source2 = new HashSet<int>();
      bool isFromUnityPopupStageList = Singleton<NGGameDataManager>.GetInstance().IsFromUnityPopupStageList;
      for (int index1 = 0; index1 < list1.Count; ++index1)
      {
        int currenMaterialUnitID = list1[index1].ID;
        List<string> list2 = ((IEnumerable<UnityValueUpItemQuest>) MasterData.UnityValueUpItemQuestList).Where<UnityValueUpItemQuest>((Func<UnityValueUpItemQuest, bool>) (x => x.material_unit_id.ID == currenMaterialUnitID && x.quest_type == CommonQuestType.Story)).Select<UnityValueUpItemQuest, string>((Func<UnityValueUpItemQuest, string>) (x => x.quest_sids)).ToList<string>();
        for (int index2 = 0; index2 < list2.Count; ++index2)
        {
          string str1 = list2[index2];
          char[] chArray = new char[1]{ ':' };
          foreach (string str2 in str1.Split(chArray))
            source1.Add(int.Parse(str2.Replace(".0", "")));
        }
        List<string> list3 = ((IEnumerable<UnityValueUpItemQuest>) MasterData.UnityValueUpItemQuestList).Where<UnityValueUpItemQuest>((Func<UnityValueUpItemQuest, bool>) (x => x.material_unit_id.ID == currenMaterialUnitID && x.quest_type == CommonQuestType.Extra)).Select<UnityValueUpItemQuest, string>((Func<UnityValueUpItemQuest, string>) (x => x.quest_sids)).ToList<string>();
        for (int index2 = 0; index2 < list3.Count; ++index2)
        {
          string str1 = list3[index2];
          char[] chArray = new char[1]{ ':' };
          foreach (string str2 in str1.Split(chArray))
            source2.Add(int.Parse(str2.Replace(".0", "")));
        }
      }
      List<int> storySIDsList = source1.ToList<int>();
      List<int> eventSIDsList = source2.ToList<int>();
      if (eventSIDsList.Count > 0 && !WebAPI.IsResponsedAtRecent("QuestProgressLimited", 60.0))
      {
        Future<WebAPI.Response.QuestProgressLimited> extra = WebAPI.QuestProgressLimited((System.Action<WebAPI.Response.UserError>) (e =>
        {
          WebAPI.DefaultUserErrorCallback(e);
          Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
        }));
        IEnumerator e1 = extra.Wait();
        while (e1.MoveNext())
          yield return e1.Current;
        e1 = (IEnumerator) null;
        if (extra.Result == null)
        {
          yield break;
        }
        else
        {
          WebAPI.SetLatestResponsedAt("QuestProgressLimited");
          extra = (Future<WebAPI.Response.QuestProgressLimited>) null;
        }
      }
      int? remainBattleCount;
      if (eventSIDsList.Count > 0)
      {
        PlayerExtraQuestS[] playerExtraQuestSArray = SMManager.Get<PlayerExtraQuestS[]>();
        List<PlayerExtraQuestS> playerExtraQuestSList = new List<PlayerExtraQuestS>();
        List<int> intList = new List<int>();
        for (int index = 0; index < eventSIDsList.Count; ++index)
        {
          int currentID = eventSIDsList[index];
          PlayerExtraQuestS playerExtraQuestS = ((IEnumerable<PlayerExtraQuestS>) playerExtraQuestSArray).FirstOrDefault<PlayerExtraQuestS>((Func<PlayerExtraQuestS, bool>) (x => x.quest_extra_s.ID == currentID));
          if (playerExtraQuestS != null)
          {
            remainBattleCount = playerExtraQuestS.remain_battle_count;
            int num2 = 0;
            if (!(remainBattleCount.GetValueOrDefault() <= num2 & remainBattleCount.HasValue))
            {
              playerExtraQuestSList.Add(playerExtraQuestS);
              continue;
            }
          }
          intList.Add(currentID);
        }
        List<QuestConverterData> questConverterDataList = QuestStageMenuBase.Convert(playerExtraQuestSList.ToArray(), new bool?());
        for (int index = 0; index < questConverterDataList.Count; ++index)
          stageItemPrefab.Clone(unityValueDetail.stageListGrid.transform).GetComponent<PopupUnityValueDetailStageItem>().Initialize(questConverterDataList[index], beforeChangeSceneAction, isFromUnityPopupStageList);
        for (int index = 0; index < intList.Count; ++index)
        {
          int currentID = intList[index];
          QuestExtraS questExtraS = ((IEnumerable<QuestExtraS>) MasterData.QuestExtraSList).FirstOrDefault<QuestExtraS>((Func<QuestExtraS, bool>) (x => x.ID == currentID));
          stageItemPrefab.Clone(unityValueDetail.stageListGrid.transform).GetComponent<PopupUnityValueDetailStageItem>().Initialize(questExtraS.quest_m.name, questExtraS.name, questExtraS.lost_ap, CommonQuestType.Extra);
        }
      }
      if (storySIDsList.Count > 0)
      {
        PlayerStoryQuestS[] playerStoryQuestSArray = SMManager.Get<PlayerStoryQuestS[]>();
        List<PlayerStoryQuestS> playerStoryQuestSList = new List<PlayerStoryQuestS>();
        List<int> intList = new List<int>();
        for (int index = 0; index < storySIDsList.Count; ++index)
        {
          int currentID = storySIDsList[index];
          PlayerStoryQuestS playerStoryQuestS = ((IEnumerable<PlayerStoryQuestS>) playerStoryQuestSArray).FirstOrDefault<PlayerStoryQuestS>((Func<PlayerStoryQuestS, bool>) (x => x.quest_story_s.ID == currentID));
          if (playerStoryQuestS != null)
          {
            remainBattleCount = playerStoryQuestS.remain_battle_count;
            int num2 = 0;
            if (!(remainBattleCount.GetValueOrDefault() <= num2 & remainBattleCount.HasValue))
            {
              playerStoryQuestSList.Add(playerStoryQuestS);
              continue;
            }
          }
          intList.Add(currentID);
        }
        List<QuestConverterData> questConverterDataList = QuestStageMenuBase.Convert(playerStoryQuestSList.ToArray());
        for (int index = 0; index < questConverterDataList.Count; ++index)
          stageItemPrefab.Clone(unityValueDetail.stageListGrid.transform).GetComponent<PopupUnityValueDetailStageItem>().Initialize(questConverterDataList[index], beforeChangeSceneAction, isFromUnityPopupStageList);
        for (int index = 0; index < intList.Count; ++index)
        {
          int currentID = intList[index];
          QuestStoryS questStoryS = ((IEnumerable<QuestStoryS>) MasterData.QuestStorySList).FirstOrDefault<QuestStoryS>((Func<QuestStoryS, bool>) (x => x.ID == currentID));
          PopupUnityValueDetailStageItem component = stageItemPrefab.Clone(unityValueDetail.stageListGrid.transform).GetComponent<PopupUnityValueDetailStageItem>();
          QuestStoryL questStoryL = (QuestStoryL) null;
          string str = string.Empty;
          if (MasterData.QuestStoryL.TryGetValue(questStoryS.quest_l_QuestStoryL, out questStoryL))
            str = questStoryL.name;
          string title1 = str;
          string name = questStoryS.name;
          int lostAp = questStoryS.lost_ap;
          component.Initialize(title1, name, lostAp, CommonQuestType.Story);
        }
      }
      if (storySIDsList.Count > 0 || eventSIDsList.Count > 0)
      {
        unityValueDetail.noAvailableQuest.SetActive(false);
        unityValueDetail.stageListScrollView.transform.parent.GetComponent<UIWidget>().alpha = 1f / 1000f;
        unityValueDetail.stageListGrid.Reposition();
        yield return (object) new WaitForSeconds(0.05f);
        unityValueDetail.stageListScrollView.ResetPosition();
        unityValueDetail.stageListScrollView.transform.parent.GetComponent<UIWidget>().alpha = 1f;
      }
      else
        unityValueDetail.noAvailableQuest.SetActive(true);
      storySIDsList = (List<int>) null;
      eventSIDsList = (List<int>) null;
    }
    else
      unityValueDetail.noAvailableQuest.SetActive(true);
  }

  public override void onBackButton()
  {
    this.onClickedClose();
  }

  public void onClickedClose()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<PopupManager>.GetInstance().dismiss(false);
  }
}
