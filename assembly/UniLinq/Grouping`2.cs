﻿// Decompiled with JetBrains decompiler
// Type: UniLinq.Grouping`2
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;

namespace UniLinq
{
  internal class Grouping<K, T> : IGrouping<K, T>, IEnumerable<T>, IEnumerable
  {
    private K key;
    private IEnumerable<T> group;

    public Grouping(K key, IEnumerable<T> group)
    {
      this.group = group;
      this.key = key;
    }

    public K Key
    {
      get
      {
        return this.key;
      }
      set
      {
        this.key = value;
      }
    }

    public IEnumerator<T> GetEnumerator()
    {
      return this.group.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
      return (IEnumerator) this.group.GetEnumerator();
    }
  }
}
