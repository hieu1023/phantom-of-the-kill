﻿// Decompiled with JetBrains decompiler
// Type: CommonBackground
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using UniLinq;
using UnityEngine;

public class CommonBackground : NGMenuBase
{
  public UIWidget bgContainer;
  private GameObject current;

  public GameObject Current
  {
    get
    {
      return this.current;
    }
  }

  private IEnumerator LateRemoveAll(GameObject[] objs)
  {
    yield return (object) null;
    foreach (UnityEngine.Object @object in objs)
      UnityEngine.Object.DestroyImmediate(@object);
  }

  public void releaseBackground()
  {
    this.StartCoroutine(this.LateRemoveAll(this.bgContainer.transform.GetChildren().Select<Transform, GameObject>((Func<Transform, GameObject>) (x => x.gameObject)).ToArray<GameObject>()));
    this.current = (GameObject) null;
  }

  public void setBackground(GameObject prefab)
  {
    this.StartCoroutine(this.LateRemoveAll(this.bgContainer.transform.GetChildren().Select<Transform, GameObject>((Func<Transform, GameObject>) (x => x.gameObject)).ToArray<GameObject>()));
    this.current = NGUITools.AddChild(this.bgContainer.gameObject, prefab);
    foreach (UI2DSprite componentsInChild in this.current.GetComponentsInChildren<UI2DSprite>())
    {
      if (this.bgContainer.height >= componentsInChild.height)
      {
        componentsInChild.keepAspectRatio = UIWidget.AspectRatioSource.BasedOnHeight;
        componentsInChild.topAnchor.target = this.bgContainer.transform;
        componentsInChild.topAnchor.absolute = 0;
        componentsInChild.bottomAnchor.target = this.bgContainer.transform;
        componentsInChild.bottomAnchor.absolute = 0;
      }
    }
  }

  public bool ComparisonBackground(GameObject prefab)
  {
    return this.hasBackground() || this.bgContainer.transform.childCount <= 0 || !(this.current.GetComponent<QuestBG>().namePrefab == prefab.GetComponent<QuestBG>().namePrefab);
  }

  public bool hasBackground()
  {
    return (UnityEngine.Object) this.current == (UnityEngine.Object) null || (UnityEngine.Object) this.current.GetComponent<QuestBG>() == (UnityEngine.Object) null;
  }
}
