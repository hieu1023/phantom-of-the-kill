﻿// Decompiled with JetBrains decompiler
// Type: Popup05110Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;
using UnityEngine;

public class Popup05110Menu : BackButtonMenuBase
{
  public override void onBackButton()
  {
    this.IbtnClose();
  }

  public void IbtnClose()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<PopupManager>.GetInstance().onDismiss();
  }

  public void IbtnReset()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<PopupManager>.GetInstance().onDismiss();
    this.StartCoroutine(this.ShowDetaResetPopup());
  }

  private IEnumerator ShowDetaResetPopup()
  {
    Future<GameObject> prefabF = Res.Prefabs.popup.popup_051_11__anim_popup01.Load<GameObject>();
    IEnumerator e = prefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Singleton<PopupManager>.GetInstance().open(prefabF.Result.Clone((Transform) null), false, false, true, true, false, false, "SE_1006");
  }
}
