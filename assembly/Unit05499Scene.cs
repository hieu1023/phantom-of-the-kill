﻿// Decompiled with JetBrains decompiler
// Type: Unit05499Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using Earth;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Unit05499Scene : NGSceneBase
{
  [SerializeField]
  private Unit05499Menu menu;

  public static void ChangeScene(bool stack, PlayerUnit selectUnit)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("unit054_9_9", (stack ? 1 : 0) != 0, (object) selectUnit);
  }

  public IEnumerator onStartSceneAsync(PlayerUnit selectUnit)
  {
    PlayerUnit[] evolutionPattern = Singleton<EarthDataManager>.GetInstance().GetEvolutionPattern(selectUnit.id, ((IEnumerable<UnitEvolutionPattern>) selectUnit.unit.EvolutionPattern).Select<UnitEvolutionPattern, int>((Func<UnitEvolutionPattern, int>) (x => x.ID)).ToArray<int>());
    IEnumerator e = this.menu.Initialize(selectUnit, evolutionPattern);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }
}
