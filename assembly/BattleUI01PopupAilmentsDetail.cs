﻿// Decompiled with JetBrains decompiler
// Type: BattleUI01PopupAilmentsDetail
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleUI01PopupAilmentsDetail : BattleBackButtonMenuBase
{
  public UIGrid grid;
  public UIScrollView scrollview;
  private GameObject dirAilmentsDetailPrefab;

  public IEnumerator Init(IEnumerable<BattleFuncs.InvestSkill> skills)
  {
    Future<GameObject> dirAilmentsDetailPrefabF = Res.Prefabs.battle.dir_Ailments_Detail.Load<GameObject>();
    IEnumerator e = dirAilmentsDetailPrefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.dirAilmentsDetailPrefab = dirAilmentsDetailPrefabF.Result;
    this.scrollview.ResetPosition();
    foreach (BattleFuncs.InvestSkill skill in skills)
    {
      e = this.dirAilmentsDetailPrefab.Clone(this.grid.transform).GetComponent<BattleUI01DirAilmentsDetail>().Init(skill);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
  }

  public override void onBackButton()
  {
    this.IbtnNo();
  }

  public void IbtnNo()
  {
    this.battleManager.popupDismiss(false, false);
  }
}
