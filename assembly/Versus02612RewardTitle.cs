﻿// Decompiled with JetBrains decompiler
// Type: Versus02612RewardTitle
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;
using UnityEngine;

public class Versus02612RewardTitle : MonoBehaviour
{
  [SerializeField]
  private UI2DSprite title;

  public IEnumerator Init(int id, bool isGuild = false)
  {
    Future<UnityEngine.Sprite> spriteF = (Future<UnityEngine.Sprite>) null;
    spriteF = !isGuild ? EmblemUtility.LoadEmblemSprite(id) : EmblemUtility.LoadGuildEmblemSprite(id);
    IEnumerator e = spriteF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.title.sprite2D = spriteF.Result;
  }
}
