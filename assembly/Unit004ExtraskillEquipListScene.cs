﻿// Decompiled with JetBrains decompiler
// Type: Unit004ExtraskillEquipListScene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;
using System.Collections;
using UnityEngine;

public class Unit004ExtraskillEquipListScene : NGSceneBase
{
  [SerializeField]
  private Unit004ExtraskillEquipListMenu menu;

  public static void changeScene(bool stack, PlayerUnit unit)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("unit004_extraskill_equip_list", (stack ? 1 : 0) != 0, (object) unit);
  }

  public virtual IEnumerator onStartSceneAsync(PlayerUnit unit)
  {
    this.menu.TargetUnit = unit;
    IEnumerator e = this.menu.Init();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public virtual void onStartScene(PlayerUnit unit)
  {
  }
}
