﻿// Decompiled with JetBrains decompiler
// Type: BattleUI05PunitiveExpeditionRewardPopup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

public class BattleUI05PunitiveExpeditionRewardPopup : MonoBehaviour
{
  [SerializeField]
  private BattleUI05PunitiveExpeditionRewardPopupAnim anim;
  [SerializeField]
  private GameObject popupObj;

  public IEnumerator Init()
  {
    yield break;
  }

  public void SetTapCallBack(System.Action callback)
  {
    this.anim.Init(callback);
  }
}
