﻿// Decompiled with JetBrains decompiler
// Type: Quest00210aScene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections;

public class Quest00210aScene : NGSceneBase
{
  public Quest00210aMenu menu;

  public static void ChangeScene(bool stack, Quest00210Menu.Param param, bool fromTower = false)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("quest002_10a", (stack ? 1 : 0) != 0, (object) param, (object) fromTower);
  }

  public override IEnumerator onInitSceneAsync()
  {
    yield break;
  }

  public IEnumerator onStartSceneAsync(Quest00210Menu.Param param, bool fromTower = false)
  {
    Quest00210aScene quest00210aScene = this;
    if (Singleton<NGGameDataManager>.GetInstance().IsEarth)
      quest00210aScene.bgmName = "bgm104";
    else if (Singleton<CommonRoot>.GetInstance().headerType == CommonRoot.HeaderType.Tower)
    {
      quest00210aScene.bgmFile = TowerUtil.BgmFile;
      quest00210aScene.bgmName = TowerUtil.BgmName;
    }
    IEnumerator e = quest00210aScene.menu.InitSupplyList(param, fromTower);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public virtual void onStartScene(Quest00210Menu.Param param, bool fromTower = false)
  {
    Singleton<PopupManager>.GetInstance().closeAll(false);
    Singleton<CommonRoot>.GetInstance().isActiveHeader = true;
    Singleton<CommonRoot>.GetInstance().isActiveFooter = true;
    Singleton<CommonRoot>.GetInstance().isLoading = false;
  }

  public override void onEndScene()
  {
    this.menu.onEndScene();
  }
}
