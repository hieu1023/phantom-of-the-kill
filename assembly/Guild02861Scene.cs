﻿// Decompiled with JetBrains decompiler
// Type: Guild02861Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System;
using System.Collections;
using UnityEngine;

public class Guild02861Scene : NGSceneBase
{
  [SerializeField]
  private Guild02861Menu menu;

  public static void ChangeScene(bool bStack)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("guild028_6_1", bStack, (object[]) Array.Empty<object>());
  }

  public IEnumerator onStartSceneAsync()
  {
    Guild02861Scene guild02861Scene = this;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    // ISSUE: reference to a compiler-generated method
    Future<WebAPI.Response.GuildGiftSendList> send = WebAPI.GuildGiftSendList(false, new System.Action<WebAPI.Response.UserError>(guild02861Scene.\u003ConStartSceneAsync\u003Eb__2_0));
    IEnumerator e = send.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if (send.Result != null)
    {
      e = guild02861Scene.menu.Init(send.Result.player_send);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      Singleton<CommonRoot>.GetInstance().isLoading = false;
    }
  }

  public void onStartScene()
  {
  }
}
