﻿// Decompiled with JetBrains decompiler
// Type: UniqueIcons
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class UniqueIcons : IconPrefabBase
{
  public string spriteName = "Item_Icon_Kiseki";
  private string kiseki = "Item_Icon_Kiseki";
  private string zeny = "Item_Icon_Zeny";
  private string medal = "Item_Icon_Medal";
  private string point = "Item_Icon_Point";
  private string key = "Item_Icon_Key";
  private string ticket = "Item_Icon_GachaTicket";
  private string season_ticket = "thum";
  public string itemName = "";
  private int width = 24;
  private int height = 28;
  private float scale = 0.8f;
  [SerializeField]
  private GameObject item;
  [SerializeField]
  private GameObject backGround;
  [SerializeField]
  private GameObject labelBase;
  [SerializeField]
  private bool isLoadedNumbers;
  [SerializeField]
  private UnityEngine.Sprite[] numbers;
  [SerializeField]
  private UnityEngine.Sprite equals;
  [SerializeField]
  private GameObject[] numbersObj;
  [SerializeField]
  private GameObject[] numbers10Obj;
  public UnityEngine.Sprite[] backSprite;
  [SerializeField]
  private UILabel label;
  private LongPressButton button_;
  private System.Action<UniqueIcons> onClick_;
  private System.Action<UniqueIcons> onLongPress_;

  public static void ClearCache()
  {
  }

  public bool BackGroundActivated
  {
    get
    {
      return this.backGround.activeSelf;
    }
    set
    {
      this.backGround.SetActive(value);
    }
  }

  public bool LabelActivated
  {
    get
    {
      return this.labelBase.activeSelf;
    }
    set
    {
      this.labelBase.SetActive(value);
    }
  }

  private IEnumerator InitNumber()
  {
    if (!this.isLoadedNumbers)
    {
      for (int index = 0; index < this.numbers.Length; ++index)
      {
        if ((UnityEngine.Object) this.numbers[index] == (UnityEngine.Object) null)
          this.numbers[index] = Resources.Load<UnityEngine.Sprite>("Icons/slc_Number" + index.ToString());
      }
      this.isLoadedNumbers = true;
    }
    if ((UnityEngine.Object) this.equals == (UnityEngine.Object) null)
    {
      Future<UnityEngine.Sprite> equalsF = Singleton<ResourceManager>.GetInstance().Load<UnityEngine.Sprite>("Icons/slc_equals", 1f);
      IEnumerator e = equalsF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.equals = equalsF.Result;
      equalsF = (Future<UnityEngine.Sprite>) null;
    }
  }

  public IEnumerator SetRamdom()
  {
    IEnumerator e;
    switch (UnityEngine.Random.Range(0, 4))
    {
      case 0:
        e = this.SetKiseki(5);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        break;
      case 1:
        e = this.SetZeny(1500);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        break;
      case 2:
        e = this.SetMedal(30);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        break;
      case 3:
        e = this.SetPoint(500);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        break;
    }
  }

  public IEnumerator Set(int num = 0)
  {
    IEnumerator e;
    switch (num)
    {
      case 0:
        e = this.SetKiseki(5);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        break;
      case 1:
        e = this.SetZeny(1500);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        break;
      case 2:
        e = this.SetMedal(30);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        break;
      case 3:
        e = this.SetPoint(500);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        break;
    }
  }

  public IEnumerator SetKiseki(int count = 0)
  {
    IEnumerator e = this.InitNumber();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Future<UnityEngine.Sprite> spriteF = Res.Icons.Item_Icon_Kiseki.Load<UnityEngine.Sprite>();
    e = spriteF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    UnityEngine.Sprite result = spriteF.Result;
    this.SetNumber(count, true);
    this.backGround.GetComponent<UI2DSprite>().sprite2D = this.backSprite[Consts.GetInstance().NORMAL_BUGU];
    this.SetTexture(result, Consts.GetInstance().UNIQUE_ICON_KISEKI, this.kiseki, false);
  }

  public IEnumerator SetMedal(int count = 0)
  {
    IEnumerator e = this.InitNumber();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Future<UnityEngine.Sprite> spriteF = Res.Icons.Item_Icon_Medal.Load<UnityEngine.Sprite>();
    e = spriteF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    UnityEngine.Sprite result = spriteF.Result;
    this.SetNumber(count, true);
    this.backGround.GetComponent<UI2DSprite>().sprite2D = this.backSprite[Consts.GetInstance().NORMAL_BUGU];
    this.SetTexture(result, Consts.GetInstance().UNIQUE_ICON_MEDAL, this.medal, false);
  }

  public IEnumerator SetZeny(int count = 0)
  {
    IEnumerator e = this.InitNumber();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Future<UnityEngine.Sprite> spriteF = Res.Icons.Item_Icon_Zeny.Load<UnityEngine.Sprite>();
    e = spriteF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    UnityEngine.Sprite result = spriteF.Result;
    this.SetNumber(count, false);
    this.backGround.GetComponent<UI2DSprite>().sprite2D = this.backSprite[Consts.GetInstance().NORMAL_BUGU];
    this.SetTexture(result, Consts.GetInstance().UNIQUE_ICON_ZENY, this.zeny, false);
  }

  public IEnumerator SetKey(int id, int count = 0)
  {
    IEnumerator e = this.InitNumber();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Future<UnityEngine.Sprite> spriteF = MasterData.QuestkeyQuestkey[id].LoadSpriteThumbnail();
    e = spriteF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    UnityEngine.Sprite result = spriteF.Result;
    this.SetNumber(count, true);
    this.backGround.GetComponent<UI2DSprite>().sprite2D = this.backSprite[Consts.GetInstance().QUEST_KEY_ITEM];
    this.SetTexture(result, Consts.GetInstance().UNIQUE_ICON_KEY, this.key, false);
  }

  public IEnumerator SetPoint(int count = 0)
  {
    IEnumerator e = this.InitNumber();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Future<UnityEngine.Sprite> spriteF = Res.Icons.Item_Icon_Point.Load<UnityEngine.Sprite>();
    e = spriteF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    UnityEngine.Sprite result = spriteF.Result;
    this.SetNumber(count, false);
    this.backGround.GetComponent<UI2DSprite>().sprite2D = this.backSprite[Consts.GetInstance().NORMAL_BUGU];
    this.SetTexture(result, Consts.GetInstance().UNIQUE_ICON_POINT, this.point, false);
  }

  public IEnumerator SetBattleMedal(int count = 0)
  {
    IEnumerator e = this.InitNumber();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Future<UnityEngine.Sprite> spriteF = Res.Icons.Item_Icon_BattleMedal.Load<UnityEngine.Sprite>();
    e = spriteF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    UnityEngine.Sprite result = spriteF.Result;
    this.SetNumber(count, true);
    this.backGround.GetComponent<UI2DSprite>().sprite2D = this.backSprite[Consts.GetInstance().NORMAL_BUGU];
    this.SetTexture(result, Consts.GetInstance().UNIQUE_ICON_BATTLE_MEDAL, this.point, false);
  }

  public IEnumerator SetAwakeSkill(int id = 0)
  {
    IEnumerator e = this.InitNumber();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Future<UnityEngine.Sprite> spriteF = MasterData.BattleskillSkill[id].LoadBattleSkillIcon((BattleFuncs.InvestSkill) null);
    e = spriteF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    UnityEngine.Sprite result = spriteF.Result;
    this.backGround.GetComponent<UI2DSprite>().enabled = false;
    this.SetTexture(result, MasterData.BattleskillSkill[id].name, this.point, true);
    this.item.transform.localScale = new Vector3(1.6f, 1.6f, 1f);
    this.item.transform.localPosition = new Vector3(-2f, 8f, 0.0f);
  }

  public IEnumerator SetTowerMedal(int count = 0)
  {
    IEnumerator e = this.InitNumber();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Future<UnityEngine.Sprite> spriteF = Res.Icons.Item_Icon_TowerMedal.Load<UnityEngine.Sprite>();
    e = spriteF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    UnityEngine.Sprite result = spriteF.Result;
    this.SetNumber(count, true);
    this.backGround.GetComponent<UI2DSprite>().sprite2D = this.backSprite[Consts.GetInstance().NORMAL_BUGU];
    this.SetTexture(result, Consts.GetInstance().UNIQUE_ICON_TOWER_MEDAL, this.point, false);
  }

  public IEnumerator SetRaidMedal(int count = 0)
  {
    IEnumerator e = this.InitNumber();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Future<UnityEngine.Sprite> spriteF = new ResourceObject("Icons/Item_Icon_RaidJuel").Load<UnityEngine.Sprite>();
    e = spriteF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    UnityEngine.Sprite result = spriteF.Result;
    this.SetNumber(count, true);
    this.backGround.GetComponent<UI2DSprite>().sprite2D = this.backSprite[Consts.GetInstance().NORMAL_BUGU];
    this.SetTexture(result, Consts.GetInstance().UNIQUE_ICON_RAID_MEDAL, this.point, false);
  }

  public IEnumerator SetGachaTicket(int count = 0, int id = 0)
  {
    IEnumerator e;
    if (id <= 0)
    {
      e = this.SetCommonGachaTicket(count);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    else
    {
      e = this.InitNumber();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      GachaTicket data = MasterData.GachaTicket[id];
      Future<UnityEngine.Sprite> spriteF = data.LoadSpriteF();
      e = spriteF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      UnityEngine.Sprite result = spriteF.Result;
      this.SetNumber(count, true);
      this.backGround.GetComponent<UI2DSprite>().sprite2D = this.backSprite[Consts.GetInstance().NORMAL_BUGU];
      this.SetTexture(result, data.short_name, this.ticket, false);
    }
  }

  public IEnumerator SetSeasonTicket(int count = 0, int id = 1)
  {
    IEnumerator e = this.InitNumber();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    SeasonTicketSeasonTicket data = MasterData.SeasonTicketSeasonTicket[id];
    Future<UnityEngine.Sprite> spriteF = data.LoadThumneilF();
    e = spriteF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    UnityEngine.Sprite result = spriteF.Result;
    this.SetNumber(count, true);
    this.backGround.GetComponent<UI2DSprite>().sprite2D = this.backSprite[Consts.GetInstance().NORMAL_BUGU];
    this.SetTexture(result, data.name, this.season_ticket, false);
  }

  public IEnumerator SetPlayerExp(int count)
  {
    IEnumerator e = this.InitNumber();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Future<UnityEngine.Sprite> spriteF = Res.Icons.Item_Icon_Common.Load<UnityEngine.Sprite>();
    e = spriteF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    UnityEngine.Sprite result = spriteF.Result;
    this.SetNumber(count, false);
    this.backGround.GetComponent<UI2DSprite>().sprite2D = this.backSprite[Consts.GetInstance().NORMAL_BUGU];
    this.SetTexture(result, Consts.GetInstance().UNIQUE_ICON_PLAYEREXP, this.point, false);
  }

  public IEnumerator SetUnitExp(int count)
  {
    IEnumerator e = this.InitNumber();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Future<UnityEngine.Sprite> spriteF = Res.Icons.Item_Icon_Common.Load<UnityEngine.Sprite>();
    e = spriteF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    UnityEngine.Sprite result = spriteF.Result;
    this.SetNumber(count, false);
    this.backGround.GetComponent<UI2DSprite>().sprite2D = this.backSprite[Consts.GetInstance().NORMAL_BUGU];
    this.SetTexture(result, Consts.GetInstance().UNIQUE_ICON_UNITEXP, this.point, false);
  }

  public IEnumerator SetApRecover(int count)
  {
    IEnumerator e = this.InitNumber();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Future<UnityEngine.Sprite> spriteF = Res.Icons.Item_Icon_Common.Load<UnityEngine.Sprite>();
    e = spriteF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    UnityEngine.Sprite result = spriteF.Result;
    this.backGround.GetComponent<UI2DSprite>().sprite2D = this.backSprite[Consts.GetInstance().NORMAL_BUGU];
    this.SetTexture(result, Consts.GetInstance().UNIQUE_ICON_APRECOVER, this.point, false);
  }

  public IEnumerator SetDpRecover(int count)
  {
    IEnumerator e = this.InitNumber();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Future<UnityEngine.Sprite> spriteF = Res.Icons.Item_Icon_Common.Load<UnityEngine.Sprite>();
    e = spriteF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    UnityEngine.Sprite result = spriteF.Result;
    this.backGround.GetComponent<UI2DSprite>().sprite2D = this.backSprite[Consts.GetInstance().NORMAL_BUGU];
    this.SetTexture(result, Consts.GetInstance().UNIQUE_ICON_DPRECOVER, this.point, false);
  }

  public IEnumerator SetMaxUnit(int count)
  {
    IEnumerator e = this.InitNumber();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Future<UnityEngine.Sprite> spriteF = Res.Icons.Item_Icon_Common.Load<UnityEngine.Sprite>();
    e = spriteF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    UnityEngine.Sprite result = spriteF.Result;
    this.SetNumber(count, false);
    this.backGround.GetComponent<UI2DSprite>().sprite2D = this.backSprite[Consts.GetInstance().NORMAL_BUGU];
    this.SetTexture(result, Consts.GetInstance().UNIQUE_ICON_MAXUNIT, this.point, false);
  }

  public IEnumerator SetMaxItem(int count)
  {
    IEnumerator e = this.InitNumber();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Future<UnityEngine.Sprite> spriteF = Res.Icons.Item_Icon_Common.Load<UnityEngine.Sprite>();
    e = spriteF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    UnityEngine.Sprite result = spriteF.Result;
    this.SetNumber(count, false);
    this.backGround.GetComponent<UI2DSprite>().sprite2D = this.backSprite[Consts.GetInstance().NORMAL_BUGU];
    this.SetTexture(result, Consts.GetInstance().UNIQUE_ICON_MAXITEM, this.point, false);
  }

  public IEnumerator SetCpRecover(int count)
  {
    IEnumerator e = this.InitNumber();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Future<UnityEngine.Sprite> spriteF = Res.Icons.Item_Icon_Common.Load<UnityEngine.Sprite>();
    e = spriteF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    UnityEngine.Sprite result = spriteF.Result;
    this.SetNumber(count, false);
    this.backGround.GetComponent<UI2DSprite>().sprite2D = this.backSprite[Consts.GetInstance().NORMAL_BUGU];
    this.SetTexture(result, Consts.GetInstance().UNIQUE_ICON_CPRECOVER, this.point, false);
  }

  public IEnumerator SetMpRecover(int count)
  {
    IEnumerator e = this.InitNumber();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Future<UnityEngine.Sprite> spriteF = Res.Icons.Item_Icon_Common.Load<UnityEngine.Sprite>();
    e = spriteF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    UnityEngine.Sprite result = spriteF.Result;
    this.SetNumber(count, false);
    this.backGround.GetComponent<UI2DSprite>().sprite2D = this.backSprite[Consts.GetInstance().NORMAL_BUGU];
    this.SetTexture(result, Consts.GetInstance().UNIQUE_ICON_MPRECOVER, this.point, false);
  }

  public IEnumerator SetStamp(int count)
  {
    IEnumerator e = this.InitNumber();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Future<UnityEngine.Sprite> spriteF = Res.Icons.Item_Icon_Common.Load<UnityEngine.Sprite>();
    e = spriteF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    UnityEngine.Sprite result = spriteF.Result;
    this.SetNumber(count, true);
    this.backGround.GetComponent<UI2DSprite>().sprite2D = this.backSprite[Consts.GetInstance().NORMAL_BUGU];
    this.SetTexture(result, Consts.GetInstance().UNIQUE_ICON_STAMP, this.point, false);
  }

  public IEnumerator SetEmblem()
  {
    IEnumerator e = this.InitNumber();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Future<UnityEngine.Sprite> spriteF = Res.Icons.Item_Icon_Common.Load<UnityEngine.Sprite>();
    e = spriteF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    UnityEngine.Sprite result = spriteF.Result;
    this.SetNumber(0, true);
    this.backGround.GetComponent<UI2DSprite>().sprite2D = this.backSprite[Consts.GetInstance().NORMAL_BUGU];
    this.SetTexture(result, Consts.GetInstance().UNIQUE_ICON_EMBLEM, this.point, false);
  }

  public IEnumerator SetKillersTicket(int id, int count = 0)
  {
    IEnumerator e = this.InitNumber();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    SelectTicket data = MasterData.SelectTicket[id];
    Future<UnityEngine.Sprite> spriteF = Singleton<ResourceManager>.GetInstance().Load<UnityEngine.Sprite>(string.Format("UnitTicket/{0}/ticket", (object) data.ID), 1f);
    e = spriteF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    UnityEngine.Sprite result = spriteF.Result;
    this.SetNumber(count, true);
    this.backGround.GetComponent<UI2DSprite>().sprite2D = this.backSprite[Consts.GetInstance().NORMAL_BUGU];
    this.SetTexture(result, data.short_name, this.ticket, false);
  }

  public IEnumerator SetMaterialTicket(int id, int count = 0)
  {
    IEnumerator e = this.InitNumber();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    SelectTicket data = MasterData.SelectTicket[id];
    Future<UnityEngine.Sprite> spriteF = Singleton<ResourceManager>.GetInstance().Load<UnityEngine.Sprite>(string.Format("MaterialTicket/{0}/ticket", (object) data.picID), 1f);
    e = spriteF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    UnityEngine.Sprite result = spriteF.Result;
    this.SetNumber(count, true);
    this.backGround.GetComponent<UI2DSprite>().sprite2D = this.backSprite[Consts.GetInstance().NORMAL_BUGU];
    this.SetTexture(result, data.short_name, this.ticket, false);
  }

  public IEnumerator SetMaterialPack(int ticketID, int count = 0)
  {
    IEnumerator e = this.InitNumber();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    SelectTicket data = MasterData.SelectTicket[ticketID];
    Future<UnityEngine.Sprite> spriteF = Singleton<ResourceManager>.GetInstance().Load<UnityEngine.Sprite>(string.Format("MaterialPack/{0}/pack", (object) data.packID), 1f);
    e = spriteF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    UnityEngine.Sprite result = spriteF.Result;
    this.SetNumber(count, true);
    this.backGround.GetComponent<UI2DSprite>().sprite2D = this.backSprite[Consts.GetInstance().NORMAL_BUGU];
    this.SetTexture(result, data.short_name, this.ticket, false);
  }

  public IEnumerator SetGuildMap(int id)
  {
    IEnumerator e = this.InitNumber();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Future<UnityEngine.Sprite> spriteF = Singleton<ResourceManager>.GetInstance().Load<UnityEngine.Sprite>(string.Format("AssetBundle/Resources/GuildMap/{0}/2D/c_thum", (object) id), 1f);
    e = spriteF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    UnityEngine.Sprite result = spriteF.Result;
    this.SetNumber(0, true);
    this.backGround.GetComponent<UI2DSprite>().sprite2D = this.backSprite[Consts.GetInstance().NORMAL_BUGU];
    this.SetTexture(result, Consts.GetInstance().UNIQUE_ICON_GUILD_TOWN, this.point, false);
    this.labelBase.SetActive(false);
  }

  public IEnumerator SetGuildFacility(int id)
  {
    IEnumerator e = this.InitNumber();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    UnitUnit unitUnit;
    if (MasterData.UnitUnit.TryGetValue(id, out unitUnit))
    {
      Future<UnityEngine.Sprite> spriteF = unitUnit.LoadSpriteThumbnail();
      e = spriteF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      UnityEngine.Sprite result = spriteF.Result;
      this.SetNumber(0, true);
      this.backGround.GetComponent<UI2DSprite>().sprite2D = this.backSprite[Consts.GetInstance().NORMAL_BUGU];
      this.SetTexture(result, Consts.GetInstance().UNIQUE_ICON_GUILD_TOWN, this.point, false);
      this.labelBase.SetActive(false);
    }
  }

  public IEnumerator SetGuildMedal(int count = 0)
  {
    IEnumerator e = this.InitNumber();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Future<UnityEngine.Sprite> spriteF = new ResourceObject("Icons/Item_Icon_GuildMedal").Load<UnityEngine.Sprite>();
    e = spriteF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    UnityEngine.Sprite result = spriteF.Result;
    this.SetNumber(count, true);
    this.backGround.GetComponent<UI2DSprite>().sprite2D = this.backSprite[Consts.GetInstance().NORMAL_BUGU];
    this.SetTexture(result, Consts.GetInstance().UNIQUE_ICON_GUILD_MEDAL, this.point, false);
  }

  public IEnumerator SetReincarnationTypeTicket(int id, int count = 0)
  {
    yield return (object) this.InitNumber();
    UnitTypeTicket data = MasterData.UnitTypeTicket[id];
    Future<UnityEngine.Sprite> spriteF = Singleton<ResourceManager>.GetInstance().Load<UnityEngine.Sprite>(string.Format("ReincarnationTypeTicket/{0}/ticket", (object) data.icon_id), 1f);
    IEnumerator e = spriteF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    UnityEngine.Sprite result = spriteF.Result;
    this.SetNumber(count, true);
    this.backGround.GetComponent<UI2DSprite>().sprite2D = this.backSprite[Consts.GetInstance().NORMAL_BUGU];
    this.SetTexture(result, data.short_name, this.ticket, false);
  }

  public IEnumerator SetItemIconCommonImage()
  {
    IEnumerator e = this.InitNumber();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Future<UnityEngine.Sprite> spriteF = Res.Icons.Item_Icon_Common.Load<UnityEngine.Sprite>();
    e = spriteF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    UnityEngine.Sprite result = spriteF.Result;
    this.backGround.GetComponent<UI2DSprite>().sprite2D = this.backSprite[Consts.GetInstance().NORMAL_BUGU];
    this.item.GetComponent<UI2DSprite>().sprite2D = result;
  }

  private IEnumerator SetCommonGachaTicket(int count = 0)
  {
    IEnumerator e = this.InitNumber();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Future<UnityEngine.Sprite> spriteF = Res.Icons.Item_Icon_GachaTicket.Load<UnityEngine.Sprite>();
    e = spriteF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    UnityEngine.Sprite result = spriteF.Result;
    this.SetNumber(count, true);
    this.backGround.GetComponent<UI2DSprite>().sprite2D = this.backSprite[Consts.GetInstance().NORMAL_BUGU];
    this.SetTexture(result, Consts.GetInstance().UNIQUE_ICON_TICKET, this.ticket, false);
  }

  private void SetTexture(UnityEngine.Sprite sprite, string _itemName, string _spriteName, bool isSnap = false)
  {
    this.spriteName = _spriteName;
    this.itemName = _itemName;
    this.label.SetText(this.itemName);
    UI2DSprite component = this.item.GetComponent<UI2DSprite>();
    component.sprite2D = sprite;
    if (!isSnap)
      return;
    component.SetDimensions(sprite.texture.width, sprite.texture.height);
  }

  private void SetNumber(int count, bool equal = true)
  {
    int length = count.ToString().Length;
    if (equal)
      ++length;
    int num1 = count;
    GameObject[] useNumbers;
    if (this.numbersObj.Length < num1.ToString().Length | this.numbersObj.Length < num1.ToString().Length + 1 & equal)
    {
      if (this.numbers10Obj.Length < num1.ToString().Length | this.numbers10Obj.Length < num1.ToString().Length + 1 & equal)
      {
        Debug.LogWarning((object) "桁数オーバーの為、個数を表示できませんでした。");
        return;
      }
      useNumbers = this.numbers10Obj;
    }
    else
      useNumbers = this.numbersObj;
    int index1 = 0;
    if (num1 != 0)
    {
      int index2 = 0;
      while (num1 > 0)
      {
        int num2 = num1 % 10;
        useNumbers[index2].SetActive(true);
        UI2DSprite component = useNumbers[index2].GetComponent<UI2DSprite>();
        component.sprite2D = this.numbers[num2];
        component.SetDirty();
        this.ChoiceNumberSize(component, num2);
        num1 /= 10;
        ++index2;
      }
      if (equal)
      {
        useNumbers[index2].SetActive(true);
        UI2DSprite component = useNumbers[index2].GetComponent<UI2DSprite>();
        component.sprite2D = this.equals;
        component.height = this.equals.texture.height;
        component.width = this.equals.texture.width;
        ++index2;
      }
      index1 = index2;
    }
    for (; index1 < useNumbers.Length; ++index1)
      useNumbers[index1].SetActive(false);
    bool flag = false;
    switch (length)
    {
      case 4:
      case 5:
        flag = true;
        break;
      case 6:
        this.scale *= 0.85f;
        flag = true;
        break;
      case 7:
        this.scale *= 0.7f;
        flag = true;
        break;
    }
    if (!flag)
      return;
    this.SetNumberScale(useNumbers);
  }

  public void SetNumberPositionY(float y)
  {
    foreach (GameObject gameObject in this.numbersObj)
    {
      Transform transform = gameObject.transform;
      transform.localPosition = new Vector3(transform.localPosition.x, y, transform.localPosition.z);
    }
  }

  public void SetNumberScale(GameObject[] useNumbers)
  {
    ((IEnumerable<GameObject>) useNumbers).Select<GameObject, UI2DSprite>((Func<GameObject, UI2DSprite>) (x => x.GetComponent<UI2DSprite>())).ForEach<UI2DSprite>((System.Action<UI2DSprite>) (sprite =>
    {
      sprite.width = (int) ((double) sprite.width * (double) this.scale);
      sprite.height = (int) ((double) sprite.height * (double) this.scale);
    }));
    float num = (float) this.width * this.scale;
    for (int index = 1; index < useNumbers.Length; ++index)
    {
      Vector3 localPosition = useNumbers[index].transform.localPosition;
      localPosition.x = useNumbers[index - 1].transform.localPosition.x - num;
      useNumbers[index].transform.localPosition = localPosition;
    }
  }

  public void ChoiceNumberSize(UI2DSprite target, int num)
  {
    int num1 = this.width;
    int height = this.height;
    switch (num)
    {
      case 0:
        num1 = 24;
        break;
      case 1:
        num1 = 16;
        break;
      case 2:
        num1 = 22;
        break;
      case 3:
        num1 = 22;
        break;
      case 4:
        num1 = 24;
        break;
      case 5:
        num1 = 22;
        break;
      case 6:
        num1 = 22;
        break;
      case 7:
        num1 = 22;
        break;
      case 8:
        num1 = 24;
        break;
      case 9:
        num1 = 22;
        break;
    }
    target.width = num1;
    target.height = height;
  }

  public override bool Gray
  {
    get
    {
      return this.gray;
    }
    set
    {
      if (this.gray == value)
        return;
      this.gray = value;
      Color color = this.gray ? Color.gray : Color.white;
      this.GetComponent<UIWidget>().color = color;
      if (!((UnityEngine.Object) this.button_ != (UnityEngine.Object) null))
        return;
      this.button_.hover = this.button_.pressed = this.button_.defaultColor = color;
    }
  }

  public void CreateButton()
  {
    UIWidget component = this.GetComponent<UIWidget>();
    GameObject gameObject = new GameObject();
    gameObject.name = "ibtn_button";
    GameObject self = gameObject;
    self.SetParent(this.gameObject);
    self.AddComponent<UIWidget>().depth = component.depth;
    self.AddComponent<BoxCollider>().size = new Vector3((float) component.width, (float) component.height, 0.0f);
    this.button_ = self.AddComponent<LongPressButton>();
    this.button_.hover = this.button_.pressed = this.button_.defaultColor;
    self.AddComponent<UIDragScrollView>();
  }

  public System.Action<UniqueIcons> onClick
  {
    get
    {
      return this.onClick_;
    }
    set
    {
      this.onClick_ = value;
      if (this.onClick_ == null)
        return;
      EventDelegate.Set(this.button_.onClick, (EventDelegate.Callback) (() => this.onClick_(this)));
    }
  }

  public System.Action<UniqueIcons> onLongPress
  {
    get
    {
      return this.onLongPress_;
    }
    set
    {
      this.onLongPress_ = value;
      if (this.onLongPress_ == null)
        return;
      EventDelegate.Set(this.button_.onLongPress, (EventDelegate.Callback) (() => this.onLongPress_(this)));
    }
  }

  public void DisableLongPressEvent()
  {
    this.button_.onClick.Clear();
    this.button_.onLongPress.Clear();
  }
}
