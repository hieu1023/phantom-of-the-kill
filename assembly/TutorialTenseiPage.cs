﻿// Decompiled with JetBrains decompiler
// Type: TutorialTenseiPage
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialTenseiPage : TutorialPageBase
{
  public override IEnumerator Show()
  {
    Player data = SMManager.Get<Player>();
    data.money = 15000L;
    SMManager.Change<Player>(data);
    Unit00499Scene.changeScene(true, Singleton<TutorialRoot>.GetInstance().Resume.after_levelup1_player_unit, Unit00499Scene.Mode.Transmigration);
    yield break;
  }

  public override void Advise()
  {
    Singleton<TutorialRoot>.GetInstance().ForceShowAdviceInNextButton("newchapter_reincarnation3_tutorial", new Dictionary<string, Func<Transform, UIButton>>()
    {
      {
        "chapter_unit_Reincarnation",
        (Func<Transform, UIButton>) (root => root.GetChildInFind("Middle").GetComponentInChildren<UIButton>())
      }
    }, (System.Action) (() => this.NextPage()));
  }
}
