﻿// Decompiled with JetBrains decompiler
// Type: ArticleInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;
using System;
using System.Collections;

public class ArticleInfo
{
  public Shop0074Scroll scroll;

  public PlayerShopArticle article { get; set; }

  public Shop shop { get; set; }

  public ShopArticleListMenu menu { get; set; }

  public Func<IEnumerator> onPurchased { get; set; }

  public System.Action<long> onPurchasedHolding { get; set; }

  public ArticleInfo TempCopy()
  {
    ArticleInfo articleInfo = (ArticleInfo) this.MemberwiseClone();
    articleInfo.scroll = (Shop0074Scroll) null;
    return articleInfo;
  }
}
