﻿// Decompiled with JetBrains decompiler
// Type: MobUnits
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using UnityEngine;

public class MobUnits : MonoBehaviour
{
  public const int MOBCHARA_MAX_ID = 999;

  public static Future<UnityEngine.Sprite> LoadSpriteLarge(int id)
  {
    return Singleton<ResourceManager>.GetInstance().Load<UnityEngine.Sprite>(string.Format("AssetBundle/Resources/MobUnits/{0}/unit_large", (object) id), 1f);
  }

  private static string GetSpriteFacePath(int id, string name)
  {
    return string.Format("AssetBundle/Resources/MobUnits/{0}/Face/{1}", (object) id, (object) name);
  }

  public static bool HasSpriteFace(int id, string name)
  {
    return Singleton<ResourceManager>.GetInstance().Contains(MobUnits.GetSpriteFacePath(id, name));
  }

  public static Future<UnityEngine.Sprite> LoadSpriteFace(int id, string name)
  {
    return Singleton<ResourceManager>.GetInstance().Load<UnityEngine.Sprite>(MobUnits.GetSpriteFacePath(id, name), 1f);
  }

  private static string GetSpriteEyePath(int id, string name)
  {
    return string.Format("AssetBundle/Resources/MobUnits/{0}/Eye/{1}", (object) id, (object) name);
  }

  public static bool HasSpriteEye(int id, string name)
  {
    return Singleton<ResourceManager>.GetInstance().Contains(MobUnits.GetSpriteEyePath(id, name));
  }

  public static Future<UnityEngine.Sprite> LoadSpriteEye(int id, string name)
  {
    return Singleton<ResourceManager>.GetInstance().Load<UnityEngine.Sprite>(MobUnits.GetSpriteEyePath(id, name), 1f);
  }

  public static Future<UnityEngine.Sprite> LoadSpriteBasic(int id)
  {
    return Singleton<ResourceManager>.GetInstance().Load<UnityEngine.Sprite>(string.Format("AssetBundle/Resources/MobUnits/{0}/unit_large", (object) id), 1f);
  }

  public static Future<GameObject> LoadStory(int id)
  {
    return Singleton<ResourceManager>.GetInstance().Load<GameObject>(string.Format("MobUnits/{0}/StoryUnitPrefab", (object) id), 1f);
  }
}
