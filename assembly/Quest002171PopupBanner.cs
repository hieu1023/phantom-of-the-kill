﻿// Decompiled with JetBrains decompiler
// Type: Quest002171PopupBanner
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System;
using System.Collections;
using UnityEngine;

public class Quest002171PopupBanner : MonoBehaviour
{
  [SerializeField]
  private FloatButton BtnFormation;
  [SerializeField]
  private UI2DSprite IdleSprite;
  [SerializeField]
  private UI2DSprite PressSprite;
  [SerializeField]
  private UIButton button;
  private Quest002171Scroll scrollcomp;

  public void SetBtnFormationEnable(bool active)
  {
    this.BtnFormation.GetComponent<BoxCollider>().enabled = active;
  }

  public IEnumerator InitScroll(
    bool isScroll,
    bool isAtlas,
    PlayerQuestGate gate,
    Quest002171Scroll scrollcomp)
  {
    this.scrollcomp = scrollcomp;
    IEnumerator e = this.SetSprite(gate.quest_gate_id, this.IdleSprite, this.PressSprite);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.SetScrollButtonCondition(gate);
  }

  private IEnumerator SetSprite(int id, UI2DSprite idle, UI2DSprite press)
  {
    string path = string.Format("Prefabs/Banners/KeyQuest/popup_banner/{0}_idle", (object) id);
    string presspath = string.Format("Prefabs/Banners/KeyQuest/popup_banner/{0}_pressed", (object) id);
    IEnumerator e = this.CreateSprite(path, idle);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    e = this.CreateSprite(presspath, press);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  private IEnumerator SetSprite(int id, UISprite sprite, PlayerQuestGate gate)
  {
    PlayerQuestGate[] tmp = new PlayerQuestGate[1]
    {
      gate
    };
    EventDelegate.Set(this.button.onClick, (EventDelegate.Callback) (() =>
    {
      Singleton<PopupManager>.GetInstance().onDismiss();
      this.scrollcomp.StartQuestReleasePopup(tmp);
    }));
    yield break;
  }

  private IEnumerator CreateSprite(string path, UI2DSprite obj)
  {
    if (!Singleton<ResourceManager>.GetInstance().Contains(path))
      path = string.Format("Prefabs/Banners/ExtraQuest/M/1/Specialquest_idle", (object[]) Array.Empty<object>());
    Future<Texture2D> future = Singleton<ResourceManager>.GetInstance().Load<Texture2D>(path, 1f);
    IEnumerator e = future.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Texture2D result = future.Result;
    if (!((UnityEngine.Object) result == (UnityEngine.Object) null))
    {
      UnityEngine.Sprite sprite = UnityEngine.Sprite.Create(result, new Rect(0.0f, 0.0f, (float) result.width, (float) result.height), new Vector2(0.5f, 0.5f), 1f, 100U, SpriteMeshType.FullRect);
      sprite.name = result.name;
      obj.width = result.width;
      obj.height = result.height;
      obj.sprite2D = sprite;
    }
  }

  private void SetScrollButtonCondition(PlayerQuestGate gate)
  {
    PlayerQuestGate[] tmp = new PlayerQuestGate[1]
    {
      gate
    };
    EventDelegate.Set(this.BtnFormation.onClick, (EventDelegate.Callback) (() =>
    {
      Singleton<PopupManager>.GetInstance().onDismiss();
      this.scrollcomp.StartQuestReleasePopup(tmp);
    }));
    EventDelegate.Set(this.BtnFormation.onOver, (EventDelegate.Callback) (() => this.onOver(this.gameObject)));
    EventDelegate.Set(this.BtnFormation.onOut, (EventDelegate.Callback) (() => this.onOut(this.gameObject)));
  }

  private void onOver(GameObject obj)
  {
    obj.GetComponent<Quest002171PopupBanner>().IdleSprite.gameObject.SetActive(false);
    obj.GetComponent<Quest002171PopupBanner>().PressSprite.gameObject.SetActive(true);
  }

  private void onOut(GameObject obj)
  {
    obj.GetComponent<Quest002171PopupBanner>().IdleSprite.gameObject.SetActive(true);
    obj.GetComponent<Quest002171PopupBanner>().PressSprite.gameObject.SetActive(false);
  }
}
