﻿// Decompiled with JetBrains decompiler
// Type: RouletteAwardResultPopupController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using System.Collections;
using UnityEngine;

public class RouletteAwardResultPopupController : MonoBehaviour
{
  [SerializeField]
  private GameObject awardIconContainer;
  [SerializeField]
  private UILabel awardDescription;
  private GameObject campaignPopupPrefab;
  private bool shouldShowCampaign;
  private string campaignURL;
  private bool isDisplayingCampaignPopup;
  private NGSoundManager soundManager;

  public IEnumerator Init(
    RouletteR001FreeDeckEntity resultDeckEntity,
    bool shouldShowCampaign,
    GameObject campaignPopupPrefab,
    string campaignURL)
  {
    this.soundManager = Singleton<NGSoundManager>.GetInstance();
    this.isDisplayingCampaignPopup = false;
    this.shouldShowCampaign = shouldShowCampaign;
    this.campaignPopupPrefab = campaignPopupPrefab;
    this.campaignURL = campaignURL;
    CreateIconObject target = this.awardIconContainer.GetOrAddComponent<CreateIconObject>();
    IEnumerator e = target.CreateThumbnail(resultDeckEntity.reward_type_id, resultDeckEntity.reward_id, 0, true, true, new CommonQuestType?(), false);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    target.GetIcon().GetComponent<UniqueIcons>();
    this.awardDescription.SetTextLocalize(resultDeckEntity.reward_message);
  }

  public void OnTouchToNext()
  {
    if (this.shouldShowCampaign)
    {
      if (this.isDisplayingCampaignPopup)
        return;
      this.isDisplayingCampaignPopup = true;
      this.soundManager.playSE("SE_1002", false, 0.0f, -1);
      this.ShowCampaignPopup();
    }
    else
    {
      this.soundManager.playSE("SE_1002", false, 0.0f, -1);
      Singleton<PopupManager>.GetInstance().dismiss(false);
      Singleton<NGSceneManager>.GetInstance().backScene();
    }
  }

  private void ShowCampaignPopup()
  {
    Singleton<PopupManager>.GetInstance().open(this.campaignPopupPrefab, false, false, false, true, false, false, "SE_1006").GetComponent<RouletteCampaignPopupController>().Init(this.campaignURL);
  }
}
