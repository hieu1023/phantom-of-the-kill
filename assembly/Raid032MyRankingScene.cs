﻿// Decompiled with JetBrains decompiler
// Type: Raid032MyRankingScene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using System.Collections;
using UnityEngine;

public class Raid032MyRankingScene : NGSceneBase
{
  [SerializeField]
  private Raid032MyRankingMenu menu;

  public static void changeScene(GuildRaid raid, bool isStack = true)
  {
    Singleton<NGSceneManager>.GetInstance().clearStack("raid032_MyRanking");
    Singleton<NGSceneManager>.GetInstance().changeScene("raid032_MyRanking", (isStack ? 1 : 0) != 0, (object) raid);
  }

  public IEnumerator onStartSceneAsync(GuildRaid raid)
  {
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    yield return (object) new WaitForEndOfFrame();
    IEnumerator e = this.menu.coInitalize(raid);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public void onStartScene(GuildRaid raid)
  {
    Singleton<CommonRoot>.GetInstance().isLoading = false;
  }

  public override IEnumerator onEndSceneAsync()
  {
    Raid032MyRankingScene raid032MyRankingScene = this;
    float startTime = Time.time;
    while (!raid032MyRankingScene.isTweenFinished && (double) Time.time - (double) startTime < (double) raid032MyRankingScene.tweenTimeoutTime)
      yield return (object) null;
    Singleton<CommonRoot>.GetInstance().loadingMode = 0;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    raid032MyRankingScene.isTweenFinished = true;
    yield return (object) null;
    // ISSUE: reference to a compiler-generated method
    yield return (object) raid032MyRankingScene.\u003C\u003En__0();
  }
}
