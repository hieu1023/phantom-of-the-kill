﻿// Decompiled with JetBrains decompiler
// Type: Unit0041012Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;
using UnityEngine;

public class Unit0041012Menu : BackButtonMenuBase
{
  [SerializeField]
  protected UILabel txtDescription1;

  public void SetText(long zeny, int medal)
  {
    string empty = string.Empty;
    if (zeny > 0L)
    {
      string localizeNumberText = zeny.ToLocalizeNumberText();
      empty += Consts.Format(Consts.GetInstance().popup_004_10_12_zeny_text, (IDictionary) new Hashtable()
      {
        {
          (object) "money",
          (object) localizeNumberText
        }
      });
    }
    if (medal > 0)
    {
      string localizeNumberText = medal.ToLocalizeNumberText();
      empty += Consts.Format(Consts.GetInstance().popup_004_10_12_medal_text, (IDictionary) new Hashtable()
      {
        {
          (object) nameof (medal),
          (object) localizeNumberText
        }
      });
    }
    if (string.IsNullOrEmpty(empty))
      empty = Consts.GetInstance().popup_004_10_12_empty_text;
    this.txtDescription1.SetText(empty);
  }

  public virtual void IbtnPopupOk()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<PopupManager>.GetInstance().closeAll(false);
  }

  public override void onBackButton()
  {
    this.IbtnPopupOk();
  }
}
