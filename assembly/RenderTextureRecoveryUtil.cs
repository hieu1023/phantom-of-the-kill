﻿// Decompiled with JetBrains decompiler
// Type: RenderTextureRecoveryUtil
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class RenderTextureRecoveryUtil : MonoBehaviour
{
  private Dictionary<int, RenderTexture> m_cameraInfo;

  public void SaveRenderTexture()
  {
    if (this.m_cameraInfo == null)
      this.m_cameraInfo = new Dictionary<int, RenderTexture>();
    else
      this.m_cameraInfo.Clear();
    foreach (Camera camera in Object.FindObjectsOfType<Camera>())
      this.m_cameraInfo.Add(camera.gameObject.GetInstanceID(), camera.targetTexture);
  }

  public void FixRenderTexture()
  {
    if (this.m_cameraInfo == null || this.m_cameraInfo.Count < 1)
    {
      this.SaveRenderTexture();
    }
    else
    {
      foreach (Camera camera in Object.FindObjectsOfType<Camera>())
      {
        if (((Object) camera.targetTexture == (Object) null || !camera.targetTexture.IsCreated()) && this.m_cameraInfo.ContainsKey(camera.gameObject.GetInstanceID()))
        {
          RenderTexture renderTexture = (RenderTexture) null;
          if (this.m_cameraInfo.TryGetValue(camera.gameObject.GetInstanceID(), out renderTexture) && (Object) renderTexture != (Object) null)
          {
            renderTexture.Create();
            camera.targetTexture = renderTexture;
          }
        }
      }
    }
  }
}
