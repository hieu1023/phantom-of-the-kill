﻿// Decompiled with JetBrains decompiler
// Type: MissionPointRewardDetailItemController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using System.Collections;
using UnityEngine;

public class MissionPointRewardDetailItemController : MonoBehaviour
{
  [SerializeField]
  private UILabel title;
  [SerializeField]
  private GameObject icon;

  public IEnumerator Init(PointReward pointRewardData)
  {
    this.title.SetTextLocalize(pointRewardData.reward_title);
    CreateIconObject target = this.icon.GetOrAddComponent<CreateIconObject>();
    IEnumerator e = target.CreateThumbnail(pointRewardData.reward_type, pointRewardData.reward_id, pointRewardData.reward_quantity, true, true, new CommonQuestType?(), false);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if ((Object) target.GetIcon() != (Object) null)
    {
      foreach (UIButton componentsInChild in target.GetIcon().GetComponentsInChildren<UIButton>())
      {
        componentsInChild.enabled = false;
        BoxCollider component = componentsInChild.GetComponent<BoxCollider>();
        if ((Object) component != (Object) null)
          component.enabled = false;
      }
    }
  }
}
