﻿// Decompiled with JetBrains decompiler
// Type: TutorialBattleResultPage
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialBattleResultPage : TutorialPageBase
{
  private Dictionary<int, string> turnDict = new Dictionary<int, string>();
  [SerializeField]
  private TutorialBattleResultPage.MESSAGE_TYPE messageType;
  [SerializeField]
  private int stageId;
  [SerializeField]
  private int deckId;
  [SerializeField]
  private int questSId;

  public override IEnumerator Show()
  {
    // ISSUE: reference to a compiler-generated field
    int num = this.\u003C\u003E1__state;
    TutorialBattleResultPage battleResultPage = this;
    if (num != 0)
      return false;
    // ISSUE: reference to a compiler-generated field
    this.\u003C\u003E1__state = -1;
    // ISSUE: reference to a compiler-generated method
    battleResultPage.StartCoroutine(battleResultPage.\u003C\u003En__0());
    PlayerUnit unit;
    switch (battleResultPage.messageType)
    {
      case TutorialBattleResultPage.MESSAGE_TYPE.BATTLE1:
        unit = Singleton<TutorialRoot>.GetInstance().Resume.after_levelup1_player_unit;
        break;
      case TutorialBattleResultPage.MESSAGE_TYPE.BATTLE2:
        unit = Singleton<TutorialRoot>.GetInstance().Resume.after_levelup2_player_unit;
        break;
      default:
        unit = Singleton<TutorialRoot>.GetInstance().Resume.after_levelup2_player_unit;
        break;
    }
    BattleUI05Scene.TutorialChangeScene(battleResultPage.stageId, battleResultPage.deckId, battleResultPage.questSId, unit);
    return false;
  }

  public override void Advise()
  {
    switch (this.messageType)
    {
      case TutorialBattleResultPage.MESSAGE_TYPE.BATTLE1:
        Singleton<TutorialRoot>.GetInstance().ForceShowAdviceInNextButton("newchapter_battleresult1_tutorial", new Dictionary<string, Func<Transform, UIButton>>()
        {
          {
            "versus_multi7",
            (Func<Transform, UIButton>) (root => root.GetChildInFind("Middle").GetComponentInChildren<UIButton>())
          }
        }, (System.Action) (() =>
        {
          Singleton<NGSceneManager>.GetInstance().destroyLoadedScenes();
          Singleton<NGSceneManager>.GetInstance().clearStack();
          Singleton<NGSceneManager>.GetInstance().changeScene("empty", false, (object[]) Array.Empty<object>());
          this.NextPage();
        }));
        break;
      case TutorialBattleResultPage.MESSAGE_TYPE.BATTLE2:
        Singleton<TutorialRoot>.GetInstance().ForceShowAdviceInNextButton("newchapter_battleresult2_tutorial", new Dictionary<string, Func<Transform, UIButton>>()
        {
          {
            "versus_multi7",
            (Func<Transform, UIButton>) (root => root.GetChildInFind("Middle").GetComponentInChildren<UIButton>())
          }
        }, (System.Action) (() =>
        {
          Singleton<NGSceneManager>.GetInstance().destroyLoadedScenes();
          Singleton<NGSceneManager>.GetInstance().clearStack();
          Singleton<NGSceneManager>.GetInstance().changeScene("empty", false, (object[]) Array.Empty<object>());
          this.NextPage();
        }));
        break;
    }
  }

  private enum MESSAGE_TYPE
  {
    BATTLE1,
    BATTLE2,
  }
}
