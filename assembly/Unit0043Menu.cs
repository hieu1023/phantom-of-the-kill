﻿// Decompiled with JetBrains decompiler
// Type: Unit0043Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using ModelViewer;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Unit0043Menu : BackButtonMenuBase
{
  private static readonly string DefineNameButtonClose = "ibtn_Close";
  private static readonly string KeyQuestProgress = "QuestProgressCharacter";
  private static readonly string DuelSceneName = "battle018_1";
  private int voiceAction = -1;
  private PlayerUnit targetPlayerUnit;
  private UnitUnit targetUnit;
  private MasterDataTable.UnitJob targetJob;
  private UnitCharacter targetChar;
  private UnitUnit lastCharacterQuestUnit;
  [SerializeField]
  protected UILabel TxtTitle;
  [SerializeField]
  protected UILabel TxtCvName;
  [SerializeField]
  protected UILabel TxtIllustratorname;
  [SerializeField]
  protected GameObject DynCharacter;
  [SerializeField]
  protected UI2DSprite dynIllust;
  [SerializeField]
  protected GameObject camera3dModel;
  [SerializeField]
  protected UITexture modelTex3d;
  [SerializeField]
  protected GameObject illustController;
  [SerializeField]
  private IllustrationController controller;
  [SerializeField]
  private UIPanel mainPanel;
  [SerializeField]
  private GameObject dir_3DModel;
  private GameObject modelPrefab;
  private UI3DModel ui3DModel;
  private Camera cam3d;
  private bool initFinished;
  private float oldTextureWidth;
  [SerializeField]
  protected Transform weaponTypeIconParent;
  [SerializeField]
  protected GameObject weaponTypeIconPrefab;
  private GameObject goWeaponType;
  [SerializeField]
  private UI2DSprite rarityStarsTypeIconParent;
  [SerializeField]
  private GameObject slcAwakening;
  [SerializeField]
  private UISprite slcCountry;
  [SerializeField]
  private UI2DSprite slcInclusion;
  [SerializeField]
  private GameObject containerR;
  [SerializeField]
  private GameObject containerL;
  [Header("Unit Pannel")]
  [SerializeField]
  private GameObject unitContainer;
  [SerializeField]
  private UILabel txtHeight;
  [SerializeField]
  private UILabel txtWeight;
  [SerializeField]
  private UILabel txtBust;
  [SerializeField]
  private UILabel txtWaist;
  [SerializeField]
  private UILabel txtHip;
  [SerializeField]
  private UILabel txtBirthday;
  [SerializeField]
  private UILabel txtConstellation;
  [SerializeField]
  private UILabel txtBloodgroup;
  [SerializeField]
  private UILabel txtBirthplace;
  [SerializeField]
  private UILabel txtFavorite;
  [SerializeField]
  private UILabel txtInterest;
  [SerializeField]
  private UILabel txtIntroduction;
  [Header("Enemy Pannel")]
  [SerializeField]
  private GameObject enemyContainer;
  [SerializeField]
  private UILabel txtEnemyIntroduction;
  [SerializeField]
  private UILabel txtEnemySubjugateNum;
  [SerializeField]
  private UILabel txtEnemyJobName;
  [Header("イベント画像")]
  [SerializeField]
  private GameObject dirEventImage;
  [SerializeField]
  private UI2DSprite slcEventImage;
  [SerializeField]
  private UIButton btnShowEventImage;
  [Header("デュエル再生用のパラメータ")]
  [SerializeField]
  private DuelDemoSettings duelDemoSettings;
  [SerializeField]
  private GameObject btnPlayDuelSkill;
  [SerializeField]
  private GameObject btnPlayMultiDuelSkill;
  [Header("UnitCategory毎のUIセット")]
  [SerializeField]
  private Unit0043Menu.UnitCategoryLayout[] uiLayouts;
  [Header("アニメーションステート名テーブル")]
  [SerializeField]
  private Unit0043Menu.PairActionKey[] pairActionKeys;
  private int current;
  private Unit0043Menu.UnitCategoryLayout currentLayout;
  private bool isLoading;
  private bool isFooterEnable;
  private int? playingAction;
  private Future<RuntimeAnimatorController> winAnimator;
  private string storeNameBtnShowActions;
  private PlayerUnitSkills duelSkill;
  private PlayerUnitSkills multiDuelSkill;
  private bool isEnabledEventImage;
  private const int INDEX_EPISODE_2 = 1;

  public bool isChangedSceneDuelDemo { get; set; }

  public virtual void IbtnBack()
  {
    if (this.isLoading || this.IsPushAndSet())
      return;
    Singleton<CommonRoot>.GetInstance().SetFooterEnable(this.isFooterEnable);
    this.backScene();
    this.finalizedDuelSettings();
  }

  public override void onBackButton()
  {
    if (this.dirEventImage.activeSelf)
      this.setActiveEventImage(false);
    else
      this.IbtnBack();
  }

  public IEnumerator Init(Unit0043Scene.BootParam bp)
  {
    this.initFinished = false;
    this.isLoading = true;
    this.targetPlayerUnit = bp.target;
    this.targetUnit = this.targetPlayerUnit.unit;
    this.targetJob = this.targetPlayerUnit.getJobData();
    this.targetChar = this.targetUnit.character;
    this.isEnabledEventImage = this.targetChar.category == UnitCategory.player && (this.targetPlayerUnit.player_id == Player.Current.id || bp.isLibrary);
    this.TxtTitle.SetTextLocalize(this.targetUnit.name);
    if (!this.isEnabledEventImage && this.targetChar.category == UnitCategory.player)
    {
      this.current = this.uiLayouts.Length - 1;
    }
    else
    {
      this.current = 0;
      for (int index = 0; index < this.uiLayouts.Length; ++index)
      {
        if (this.uiLayouts[index].category == this.targetChar.category)
        {
          this.current = index;
          break;
        }
      }
    }
    ((IEnumerable<Unit0043Menu.UnitCategoryLayout>) this.uiLayouts).Select<Unit0043Menu.UnitCategoryLayout, GameObject>((Func<Unit0043Menu.UnitCategoryLayout, GameObject>) (x => x.dirTop)).ToggleOnce(this.current);
    this.currentLayout = this.uiLayouts[this.current];
    string str1;
    switch (this.targetUnit.kind.Enum)
    {
      case GearKindEnum.bow:
      case GearKindEnum.gun:
      case GearKindEnum.staff:
        str1 = "far";
        break;
      default:
        str1 = "near";
        break;
    }
    for (int index1 = 0; index1 < this.currentLayout.actionParameters.Length; ++index1)
    {
      Unit0043Menu.ActionParameter actionParameter = this.currentLayout.actionParameters[index1];
      string str2 = actionParameter.perGearKind ? actionParameter.label + "_" + str1 : actionParameter.label;
      actionParameter.states = (string[]) null;
      for (int index2 = 0; index2 < this.pairActionKeys.Length; ++index2)
      {
        if (this.pairActionKeys[index2].label == str2)
        {
          actionParameter.states = this.pairActionKeys[index2].states;
          break;
        }
      }
    }
    this.isFooterEnable = Singleton<CommonRoot>.GetInstance().GetFooterEnable();
    Singleton<CommonRoot>.GetInstance().SetFooterEnable(true);
    IEnumerator e = this.LoadPrefabs();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    yield return (object) this.setup3DModel(true);
    yield return (object) this.setEventImage();
    if (this.targetChar.category == UnitCategory.player)
    {
      this.duelSkill = bp.koyuDuelSkill;
      this.multiDuelSkill = bp.koyuMultiSkill;
      this.btnPlayDuelSkill.SetActive(this.duelSkill != null);
      this.btnPlayMultiDuelSkill.SetActive(this.multiDuelSkill != null);
      if (this.btnPlayDuelSkill.activeSelf || this.btnPlayMultiDuelSkill.activeSelf)
        yield return (object) this.duelDemoSettings.preSetupLightmaps();
    }
    if (this.targetUnit.cv_name.CompareTo("") == 0)
      this.containerL.SetActive(false);
    else
      this.TxtCvName.SetTextLocalize(this.targetUnit.cv_name);
    if (this.targetUnit.illustrator_name.CompareTo("") == 0)
      this.containerR.SetActive(false);
    else
      this.TxtIllustratorname.SetTextLocalize(this.targetUnit.illustrator_name);
    this.goWeaponType = this.createIcon(this.weaponTypeIconPrefab, this.weaponTypeIconParent);
    this.goWeaponType.GetComponent<GearKindIcon>().Init(this.targetUnit.kind, this.targetPlayerUnit.GetElement());
    RarityIcon.SetRarity(this.targetUnit, this.rarityStarsTypeIconParent, true, bp.isSame, false);
    e = this.SetIllustration();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.SetUnitInformations();
    if ((UnityEngine.Object) this.slcCountry != (UnityEngine.Object) null)
    {
      this.slcCountry.gameObject.SetActive(false);
      if (this.targetUnit.country_attribute.HasValue)
      {
        this.slcCountry.gameObject.SetActive(true);
        this.targetUnit.SetCuntrySpriteName(ref this.slcCountry);
      }
    }
    if ((UnityEngine.Object) this.slcInclusion != (UnityEngine.Object) null)
    {
      this.slcInclusion.gameObject.SetActive(false);
      if (this.targetUnit.inclusion_ip.HasValue)
      {
        this.slcInclusion.gameObject.SetActive(true);
        e = this.targetUnit.SetInclusionIP(this.slcInclusion);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
      }
    }
    this.controller.Init(this.mainPanel.width, (System.Action) null);
    if ((UnityEngine.Object) this.slcAwakening != (UnityEngine.Object) null)
      this.slcAwakening.SetActive(false);
    if (this.targetUnit.awake_unit_flag)
      this.slcAwakening.SetActive(true);
    this.mainPanel.alpha = 1f;
    this.cam3d = this.camera3dModel.GetComponent<Camera>();
    this.SetRenderTexture();
    yield return (object) new WaitForEndOfFrame();
    this.isLoading = false;
    this.initFinished = true;
  }

  public IEnumerator InitOnBackScene()
  {
    if ((UnityEngine.Object) this.ui3DModel != (UnityEngine.Object) null)
    {
      this.ui3DModel.gameObject.SetActive(false);
      UnityEngine.Object.Destroy((UnityEngine.Object) this.ui3DModel.gameObject);
    }
    yield return (object) this.setup3DModel(false);
    this.mainPanel.alpha = 1f;
  }

  private IEnumerator setup3DModel(bool bResetCamera)
  {
    this.ui3DModel = this.modelPrefab.Clone(this.dir_3DModel.transform).GetComponent<UI3DModel>();
    this.ui3DModel.lightOn = true;
    yield return (object) this.ui3DModel.Unit3D(this.targetPlayerUnit);
    if ((UnityEngine.Object) this.ui3DModel.model_creater_.UnitAnimator != (UnityEngine.Object) null)
    {
      if (this.currentLayout.actionParameters != null && this.currentLayout.actionButtons != null && this.currentLayout.actionParameters.Length == this.currentLayout.actionButtons.Length)
      {
        for (int actionNo = 0; actionNo < this.currentLayout.actionParameters.Length; ++actionNo)
          this.setActionButton(actionNo);
      }
      if (this.targetChar.category == UnitCategory.player)
      {
        this.winAnimator = this.targetPlayerUnit.LoadWinAnimator(0);
        yield return (object) this.winAnimator.Wait();
      }
      else
        this.winAnimator = (Future<RuntimeAnimatorController>) null;
    }
    ViewerCameraController component1 = this.camera3dModel.GetComponent<ViewerCameraController>();
    if ((UnityEngine.Object) component1 != (UnityEngine.Object) null)
    {
      component1.Initialize(this.ui3DModel.ModelTarget.transform, this.targetUnit.vehicle_model_name != null, bResetCamera);
      ModelController component2 = this.camera3dModel.GetComponent<ModelController>();
      if ((UnityEngine.Object) component2 != (UnityEngine.Object) null)
        component2.enabled = false;
    }
    else
    {
      ModelController component2 = this.camera3dModel.GetComponent<ModelController>();
      if ((UnityEngine.Object) component2 != (UnityEngine.Object) null)
      {
        component2.target = this.ui3DModel.ModelTarget.transform;
        component2.unitWithAnimals = this.targetUnit.vehicle_model_name != null;
      }
    }
  }

  private PlayerUnitSkills createPlayerUnitSkill(int? skillId)
  {
    if (!skillId.HasValue)
      return (PlayerUnitSkills) null;
    return new PlayerUnitSkills()
    {
      skill_id = skillId.Value
    };
  }

  private void setActionButton(int actionNo)
  {
    UIButton actionButton = this.currentLayout.actionButtons[actionNo];
    if (this.currentLayout.actionParameters[actionNo].states != null)
      EventDelegate.Set(actionButton.onClick, (EventDelegate.Callback) (() => this.playAction(actionNo)));
    else
      actionButton.onClick.Clear();
  }

  protected override void Update()
  {
    if (!this.initFinished)
      return;
    base.Update();
    if ((double) this.oldTextureWidth == (double) this.modelTex3d.width)
      return;
    this.cam3d.targetTexture.Release();
    this.SetRenderTexture();
  }

  private void SetRenderTexture()
  {
    this.cam3d.targetTexture = new RenderTexture(this.modelTex3d.width, this.modelTex3d.height, 16);
    this.oldTextureWidth = (float) this.modelTex3d.width;
    this.cam3d.targetTexture.antiAliasing = 1;
    this.cam3d.targetTexture.wrapMode = TextureWrapMode.Clamp;
    this.cam3d.targetTexture.filterMode = FilterMode.Bilinear;
    this.cam3d.targetTexture.enableRandomWrite = false;
    this.cam3d.farClipPlane = 1000f;
    this.modelTex3d.mainTexture = (Texture) this.cam3d.targetTexture;
  }

  private IEnumerator LoadPrefabs()
  {
    Future<GameObject> loader = Res.Prefabs.gacha006_8.slc_3DModel.Load<GameObject>();
    IEnumerator e = loader.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.modelPrefab = loader.Result;
  }

  private void SetUnitInformations()
  {
    UnitCharacter targetChar = this.targetChar;
    int result;
    string text;
    if (targetChar.birthday.Length == 4 && int.TryParse(targetChar.birthday, out result))
      text = Consts.Format(Consts.GetInstance().unit004_2_birthday, (IDictionary) new Hashtable()
      {
        {
          (object) "month",
          (object) (result / 100).ToString()
        },
        {
          (object) "date",
          (object) (result % 100).ToString()
        }
      });
    else
      text = targetChar.birthday;
    if (targetChar.category == UnitCategory.player)
    {
      this.txtBust.SetTextLocalize(targetChar.bust);
      this.txtHip.SetTextLocalize(targetChar.hip);
      this.txtWaist.SetTextLocalize(targetChar.waist);
      this.txtHeight.SetTextLocalize(targetChar.height);
      this.txtWeight.SetTextLocalize(targetChar.weight);
      this.txtBirthday.SetTextLocalize(text);
      this.txtBirthplace.SetTextLocalize(targetChar.origin);
      this.txtBloodgroup.SetTextLocalize(targetChar.blood_type);
      this.txtInterest.SetTextLocalize(targetChar.hobby);
      this.txtFavorite.SetTextLocalize(targetChar.favorite);
      this.txtConstellation.SetTextLocalize(targetChar.zodiac_sign);
      this.txtIntroduction.SetTextLocalize(this.targetUnit.description);
    }
    else
    {
      this.txtEnemyIntroduction.SetTextLocalize(this.targetUnit.description);
      this.txtEnemyJobName.SetTextLocalize(this.targetJob.name);
      int defeat = 0;
      if (SMManager.Get<PlayerEnemyHistory[]>() != null)
        ((IEnumerable<PlayerEnemyHistory>) ((IEnumerable<PlayerEnemyHistory>) SMManager.Get<PlayerEnemyHistory[]>()).ToArray<PlayerEnemyHistory>()).ForEach<PlayerEnemyHistory>((System.Action<PlayerEnemyHistory>) (obj =>
        {
          if (obj.unit_id != this.targetUnit.ID)
            return;
          defeat += obj.defeat;
        }));
      if (defeat > 99999)
        defeat = 99999;
      this.txtEnemySubjugateNum.SetTextLocalize(defeat);
    }
  }

  private GameObject createIcon(GameObject prefab, Transform trans)
  {
    GameObject gameObject = prefab.Clone(trans);
    UI2DSprite componentInChildren1 = gameObject.GetComponentInChildren<UI2DSprite>();
    UI2DSprite componentInChildren2 = trans.GetComponentInChildren<UI2DSprite>();
    componentInChildren1.SetDimensions(componentInChildren2.width, componentInChildren2.height);
    componentInChildren1.depth += 1000;
    componentInChildren1.transform.localPosition = Vector3.zero;
    return gameObject;
  }

  public IEnumerator SetCharacterLargeImage()
  {
    Future<GameObject> goFuture = this.targetUnit.LoadMypage();
    IEnumerator e = goFuture.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    GameObject go = goFuture.Result.Clone(this.DynCharacter.transform);
    go.transform.localPosition = new Vector3(this.targetUnit.illust_pattern.illust_x, this.targetUnit.illust_pattern.illust_y, 0.0f);
    go.transform.localScale = new Vector3(this.targetUnit.illust_pattern.illust_scale, this.targetUnit.illust_pattern.illust_scale, this.targetUnit.illust_pattern.illust_scale);
    e = this.targetUnit.SetLargeSpriteSnap(go, 0);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    e = this.targetUnit.SetLargeSpriteWithMask(go, Res.GUI._004_3_sozai.mask_chara.Load<Texture2D>(), 5, 0, -80);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public IEnumerator SetIllustration()
  {
    Future<UnityEngine.Sprite> illustF = this.targetUnit.LoadHiResSprite(this.targetJob.ID);
    if (illustF == null)
    {
      this.dynIllust.enabled = false;
    }
    else
    {
      IEnumerator e = illustF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.dynIllust.sprite2D = illustF.Result;
    }
  }

  private IEnumerator setEventImage()
  {
    if (!this.isEnabledEventImage)
    {
      this.btnShowEventImage.gameObject.SetActive(false);
    }
    else
    {
      this.dirEventImage.GetComponentInChildren<UIButton>(true).gameObject.name = Unit0043Menu.DefineNameButtonClose;
      if (!WebAPI.IsResponsedAtContainsKey(Unit0043Menu.KeyQuestProgress))
      {
        yield return (object) ServerTime.WaitSync();
        Future<WebAPI.Response.QuestProgressCharacter> wapi = WebAPI.QuestProgressCharacter((System.Action<WebAPI.Response.UserError>) (error =>
        {
          WebAPI.DefaultUserErrorCallback(error);
          Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
        }));
        yield return (object) wapi.Wait();
        if (wapi.Result == null)
        {
          yield break;
        }
        else
        {
          WebAPI.SetLatestResponsedAt(Unit0043Menu.KeyQuestProgress);
          WebAPI.SetLatestResponsedAt("QuestProgressHarmony");
          wapi = (Future<WebAPI.Response.QuestProgressCharacter>) null;
        }
      }
      if (!this.isClearedCharacterQuestEpisode2)
      {
        this.btnShowEventImage.isEnabled = false;
      }
      else
      {
        int[] numArray1;
        if (this.lastCharacterQuestUnit.ID == this.lastCharacterQuestUnit.resource_reference_unit_id_UnitUnit)
          numArray1 = new int[1]
          {
            this.lastCharacterQuestUnit.ID
          };
        else
          numArray1 = new int[2]
          {
            this.lastCharacterQuestUnit.ID,
            this.lastCharacterQuestUnit.resource_reference_unit_id_UnitUnit
          };
        int[] numArray2 = numArray1;
        string path1 = (string) null;
        for (int index = 0; index < numArray2.Length; ++index)
        {
          string path2 = string.Format("AssetBundle/Resources/EventImages/c{0}", (object) numArray2[index]);
          if (Singleton<ResourceManager>.GetInstance().Contains(path2 + "_1"))
          {
            path1 = path2 + "_1";
            break;
          }
          if (Singleton<ResourceManager>.GetInstance().Contains(path2))
          {
            path1 = path2;
            break;
          }
        }
        if (path1 == null)
        {
          this.btnShowEventImage.isEnabled = false;
        }
        else
        {
          Future<UnityEngine.Sprite> ldImage = Singleton<ResourceManager>.GetInstance().Load<UnityEngine.Sprite>(path1, 1f);
          yield return (object) ldImage.Wait();
          if ((UnityEngine.Object) ldImage.Result != (UnityEngine.Object) null)
          {
            this.slcEventImage.sprite2D = ldImage.Result;
            this.btnShowEventImage.isEnabled = true;
          }
          else
            this.btnShowEventImage.isEnabled = false;
        }
      }
    }
  }

  private bool isClearedCharacterQuestEpisode2
  {
    get
    {
      QuestCharacterS[] array1 = ((IEnumerable<QuestCharacterS>) MasterData.QuestCharacterSList).Where<QuestCharacterS>((Func<QuestCharacterS, bool>) (x => QuestCharacterS.CheckIsReleased(x.start_at) && x.unit.same_character_id == this.targetUnit.same_character_id)).OrderBy<QuestCharacterS, int>((Func<QuestCharacterS, int>) (y => y.priority)).ToArray<QuestCharacterS>();
      this.lastCharacterQuestUnit = array1.Length != 0 ? array1[0].unit : (UnitUnit) null;
      if (array1.Length <= 1)
        return false;
      QuestCharacterS quest = array1[1];
      PlayerCharacterQuestS[] array2 = SMManager.Get<PlayerCharacterQuestS[]>();
      if (array2 == null)
        return false;
      PlayerCharacterQuestS playerCharacterQuestS = Array.Find<PlayerCharacterQuestS>(array2, (Predicate<PlayerCharacterQuestS>) (x => x._quest_character_s == quest.ID));
      return playerCharacterQuestS != null && playerCharacterQuestS.is_clear;
    }
  }

  public void CloseContainers()
  {
    if (this.IsPush)
      return;
    this.unitContainer.SetActive(false);
    this.enemyContainer.SetActive(false);
    this.currentLayout.btnCloseContainer.SetActive(false);
  }

  public void OpenContainer()
  {
    if (this.isLoading || this.IsPush)
      return;
    this.currentLayout.btnCloseContainer.SetActive(true);
    if (this.targetChar.category == UnitCategory.player)
      this.unitContainer.SetActive(true);
    else
      this.enemyContainer.SetActive(true);
  }

  public void Show3dModel()
  {
    if (this.isLoading || this.IsPush)
      return;
    this.currentLayout.btn3dModel.SetActive(false);
    this.currentLayout.btn2dIllust.SetActive(true);
    this.currentLayout.btnActions.SetActive(true);
    this.camera3dModel.SetActive(true);
    this.modelTex3d.gameObject.SetActive(true);
    this.illustController.SetActive(false);
  }

  public void Show2dIllust()
  {
    if (this.isLoading || this.IsPush)
      return;
    this.illustController.GetComponent<IllustrationController>().InitSize();
    this.currentLayout.btn3dModel.SetActive(true);
    this.currentLayout.btn2dIllust.SetActive(false);
    this.currentLayout.btnActions.SetActive(false);
    this.camera3dModel.SetActive(false);
    this.modelTex3d.gameObject.SetActive(false);
    this.illustController.SetActive(true);
  }

  public void onEndScene()
  {
    Resources.UnloadUnusedAssets();
    NGSoundManager instanceOrNull = Singleton<NGSoundManager>.GetInstanceOrNull();
    if (!((UnityEngine.Object) instanceOrNull != (UnityEngine.Object) null))
      return;
    instanceOrNull.stopVoice(-1);
  }

  private void playAction(int actionNo)
  {
    if (this.IsPush)
      return;
    if (this.playingAction.HasValue)
    {
      int num = actionNo;
      int? playingAction = this.playingAction;
      int valueOrDefault = playingAction.GetValueOrDefault();
      if (num == valueOrDefault & playingAction.HasValue)
        return;
    }
    this.StopCoroutine("doPlayAction");
    Singleton<NGSoundManager>.GetInstance().StopVoice(-1, 0.5f);
    this.voiceAction = -1;
    if (this.playingAction.HasValue && this.currentLayout.actionParameters[this.playingAction.Value].winAnimation && (UnityEngine.Object) this.winAnimator.Result != (UnityEngine.Object) null)
      this.ui3DModel.model_creater_.ResetAnimator();
    this.disableShieldModel();
    this.StartCoroutine("doPlayAction", (object) actionNo);
  }

  private IEnumerator doPlayAction(int actionNo)
  {
    Unit0043Menu unit0043Menu = this;
    unit0043Menu.playingAction = new int?(actionNo);
    Unit0043Menu.ActionParameter actionParameter = unit0043Menu.currentLayout.actionParameters[actionNo];
    IEnumerator e;
    if (!actionParameter.winAnimation || (UnityEngine.Object) unit0043Menu.winAnimator.Result == (UnityEngine.Object) null)
    {
      e = unit0043Menu.ui3DModel.TryPlayAction(actionParameter.states, new System.Action<bool>(unit0043Menu.startEndAction), actionParameter.waitNext);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    else
    {
      unit0043Menu.startEndAction(true);
      e = unit0043Menu.ui3DModel.PlayAction(unit0043Menu.winAnimator.Result, unit0043Menu.targetPlayerUnit.getWinAnimatorControllerName(0), actionParameter.states[0], actionParameter.waitNext, new Func<IEnumerator>(unit0043Menu.playVoiceWait));
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      unit0043Menu.startEndAction(false);
    }
    unit0043Menu.playingAction = new int?();
  }

  private void startEndAction(bool started)
  {
    if (started)
    {
      if (!this.playingAction.HasValue)
        return;
      Unit0043Menu.ActionParameter actionParameter = this.currentLayout.actionParameters[this.playingAction.Value];
      if ((double) actionParameter.displayShield > 0.0)
        this.StartCoroutine("doDisplayShield", (object) actionParameter.displayShield);
      NGSoundManager instanceOrNull = Singleton<NGSoundManager>.GetInstanceOrNull();
      CharaVoiceCueEnum.CueID? voice;
      if ((UnityEngine.Object) instanceOrNull == (UnityEngine.Object) null || !(voice = actionParameter.voice).HasValue)
        return;
      int channel = actionParameter.winAnimation ? 2 : -1;
      this.voiceAction = instanceOrNull.playVoiceByID(this.targetUnit.unitVoicePattern, (int) voice.Value, channel, 0.0f);
    }
    else
      this.disableShieldModel();
  }

  private void disableShieldModel()
  {
    if ((UnityEngine.Object) this.ui3DModel.model_creater_.EquipShieldModel == (UnityEngine.Object) null || !this.ui3DModel.model_creater_.EquipShieldModel.activeSelf)
      return;
    this.ui3DModel.model_creater_.EquipShieldModel.SetActive(false);
    this.StopCoroutine("doDisplayShield");
  }

  private IEnumerator doDisplayShield(float wait)
  {
    if (!((UnityEngine.Object) this.ui3DModel.model_creater_.EquipShieldModel == (UnityEngine.Object) null))
    {
      this.ui3DModel.model_creater_.EquipShieldModel.SetActive(true);
      yield return (object) new WaitForSeconds(wait);
      this.ui3DModel.model_creater_.EquipShieldModel.SetActive(false);
    }
  }

  private IEnumerator playVoiceWait()
  {
    if (this.voiceAction != -1)
    {
      while (Singleton<NGSoundManager>.GetInstance().IsVoicePlaying(this.voiceAction))
        yield return (object) null;
      this.voiceAction = -1;
    }
  }

  public void OnClickedActions()
  {
    if (this.IsPush)
      return;
    this.currentLayout.dirActions.SetActive(!this.currentLayout.dirActions.activeSelf);
    this.StartCoroutine("doChangeNameBtnShowActions");
  }

  private IEnumerator doChangeNameBtnShowActions()
  {
    if (string.IsNullOrEmpty(this.currentLayout.storeNameBtnShowActions))
      this.currentLayout.storeNameBtnShowActions = this.currentLayout.btnActions.name;
    yield return (object) null;
    this.currentLayout.btnActions.name = this.currentLayout.dirActions.activeSelf ? Unit0043Menu.DefineNameButtonClose : this.currentLayout.storeNameBtnShowActions;
  }

  public void ShowEventImage()
  {
    if (this.isLoading || this.IsPush)
      return;
    this.setActiveEventImage(true);
  }

  public void HideEventImage()
  {
    this.setActiveEventImage(false);
  }

  private void setActiveEventImage(bool flag)
  {
    NGTweenParts component = this.dirEventImage.GetComponent<NGTweenParts>();
    if ((UnityEngine.Object) component != (UnityEngine.Object) null)
      component.isActive = flag;
    else
      this.dirEventImage.SetActive(flag);
  }

  public void onClickedPlayDuelSkill()
  {
    if (this.duelSkill == null || this.IsPushAndSet())
      return;
    this.resetAction();
    this.StartCoroutine(this.doPlayDuelSkill(false));
  }

  public void onClickedPlayMultiDuelSkill()
  {
    if (this.multiDuelSkill == null || this.IsPushAndSet())
      return;
    this.resetAction();
    this.StartCoroutine(this.doPlayDuelSkill(true));
  }

  private IEnumerator doPlayDuelSkill(bool multi)
  {
    Unit0043Menu unit0043Menu = this;
    PlayerUnitSkills koyuSkill = multi ? unit0043Menu.multiDuelSkill : unit0043Menu.duelSkill;
    DuelEnvironment duelEnv = unit0043Menu.duelDemoSettings.makeEnvironment();
    Future<DuelResult> future = unit0043Menu.duelDemoSettings.makeResult(unit0043Menu.targetPlayerUnit, koyuSkill);
    yield return (object) future.Wait();
    if (future.Result == null)
    {
      unit0043Menu.IsPush = false;
    }
    else
    {
      unit0043Menu.isChangedSceneDuelDemo = true;
      Singleton<NGSceneManager>.GetInstance().changeScene(Unit0043Menu.DuelSceneName, true, (object) future.Result, (object) duelEnv);
      unit0043Menu.duelDemoSettings.playBGM();
    }
  }

  private void resetAction()
  {
    if (!this.playingAction.HasValue)
      return;
    this.StopCoroutine("doPlayAction");
    Singleton<NGSoundManager>.GetInstance().StopVoice(-1, 0.5f);
    this.voiceAction = -1;
    if (this.currentLayout.actionParameters[this.playingAction.Value].winAnimation && (UnityEngine.Object) this.winAnimator.Result != (UnityEngine.Object) null)
      this.ui3DModel.model_creater_.ResetAnimator();
    else
      this.ui3DModel.ResetWaitAction();
    this.disableShieldModel();
    this.playingAction = new int?();
  }

  private void finalizedDuelSettings()
  {
    if (!this.duelDemoSettings.isSetupedLightmaps)
      return;
    Singleton<NGDuelDataManager>.GetInstance().Init();
    this.duelDemoSettings.clearLightmaps();
    Singleton<NGSceneManager>.GetInstance().destroyScene(Unit0043Menu.DuelSceneName);
  }

  [Serializable]
  private class ActionParameter
  {
    public float waitNext = 1f;
    public string voiceName = string.Empty;
    public string label;
    public bool perGearKind;
    public bool winAnimation;
    public float displayShield;

    public string[] states { get; set; }

    public CharaVoiceCueEnum.CueID? voice
    {
      get
      {
        if (string.IsNullOrEmpty(this.voiceName))
          return new CharaVoiceCueEnum.CueID?();
        CharaVoiceCueEnum.CueID result;
        return !Enum.TryParse<CharaVoiceCueEnum.CueID>(this.voiceName, out result) ? new CharaVoiceCueEnum.CueID?() : new CharaVoiceCueEnum.CueID?(result);
      }
    }
  }

  [Serializable]
  private class UnitCategoryLayout
  {
    public UnitCategory category;
    public GameObject dirTop;
    public GameObject btnCloseContainer;
    public GameObject btn3dModel;
    public GameObject btn2dIllust;
    public GameObject btnActions;
    public GameObject dirActions;
    [Tooltip("アニメーション再生パラメータ")]
    public Unit0043Menu.ActionParameter[] actionParameters;
    [Tooltip("actionParameters[]に対応するボタン")]
    public UIButton[] actionButtons;

    public string storeNameBtnShowActions { get; set; }
  }

  [Serializable]
  private class PairActionKey
  {
    public string label;
    public string[] states;
  }
}
