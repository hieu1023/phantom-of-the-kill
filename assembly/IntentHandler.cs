﻿// Decompiled with JetBrains decompiler
// Type: IntentHandler
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

public class IntentHandler
{
  private static IntentHandler instance = new IntentHandler();

  public static IntentHandler GetInstance()
  {
    return IntentHandler.instance;
  }

  public void ClearIntentHandlers()
  {
  }

  public void AddNoopIntentHandler()
  {
  }

  public void AddUrlIntentHandler()
  {
  }

  public void AddCustomIntentHandler(string gameObjectName, string methodName)
  {
  }
}
