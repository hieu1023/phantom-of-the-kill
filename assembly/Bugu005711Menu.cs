﻿// Decompiled with JetBrains decompiler
// Type: Bugu005711Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;
using UnityEngine;

public class Bugu005711Menu : BackButtonMenuBase
{
  [SerializeField]
  private UISlider slider;
  [SerializeField]
  private UILabel TxtItemName;
  [SerializeField]
  private UILabel TxtItemDescription;
  [SerializeField]
  private UILabel TxtPossessionNum;
  [SerializeField]
  private UILabel TxtSellValue;
  [SerializeField]
  private UILabel TxtSellNum;
  [SerializeField]
  private UILabel TxtTotalSellValue;
  [SerializeField]
  private UILabel TxtSelectMax;
  [SerializeField]
  private UI2DSprite LinkItem;
  private Bugu00525Menu menu;
  private long itemZenny;
  private int itemCount;
  private int totalItem;
  private long totalZenny;
  private InventoryItem item;

  public void IbtnPopupOk()
  {
    if (this.IsPushAndSet())
      return;
    this.menu.SetSellCount(this.item, this.itemCount);
    Singleton<PopupManager>.GetInstance().onDismiss();
  }

  public void IbtnNo()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<PopupManager>.GetInstance().onDismiss();
  }

  public override void onBackButton()
  {
    this.IbtnNo();
  }

  public IEnumerator InitSceneAsync(InventoryItem item, Bugu00525Menu menu)
  {
    Future<UnityEngine.Sprite> spriteF = item.LoadSpriteThumbnail();
    IEnumerator e = spriteF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.LinkItem.sprite2D = spriteF.Result;
    this.item = item;
    this.menu = menu;
    this.Set(item);
  }

  public void Set(InventoryItem item)
  {
    this.totalItem = item.Item.quantity < this.menu.SelectMax ? item.Item.quantity : this.menu.SelectMax;
    this.itemZenny = item.GetSingleSellPrice();
    this.TxtItemName.SetText(item.GetName());
    this.TxtItemDescription.SetText(item.GetDescription());
    this.TxtPossessionNum.SetTextLocalize(item.Item.quantity);
    this.TxtSellNum.SetTextLocalize(this.totalItem);
    this.TxtSelectMax.SetTextLocalize(this.totalItem);
    this.TxtSellValue.SetTextLocalize(this.itemZenny);
    this.TxtTotalSellValue.SetTextLocalize((long) this.totalItem * this.itemZenny);
    this.slider.value = 1f;
  }

  protected override void Update()
  {
    this.itemCount = (int) ((double) this.slider.value * (double) this.totalItem);
    if (this.totalItem <= 1 && (double) this.slider.value < 1.0)
    {
      if ((double) this.slider.value >= 0.00999999977648258)
        this.itemCount = 1;
    }
    else if (this.itemCount > this.totalItem)
    {
      this.itemCount = this.totalItem;
      this.slider.value = (float) this.itemCount / (float) this.totalItem;
    }
    this.TxtSellNum.SetTextLocalize(this.itemCount);
    this.totalZenny = this.itemZenny * (long) this.itemCount;
    this.TxtTotalSellValue.SetTextLocalize(this.totalZenny);
  }
}
