﻿// Decompiled with JetBrains decompiler
// Type: Modified`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

public class Modified<T> where T : class
{
  private long revision;

  public Modified(long revision)
  {
    this.revision = revision;
  }

  public bool Loaded
  {
    get
    {
      return SMManager.Contains<T>();
    }
  }

  public bool Changed
  {
    get
    {
      return this.revision != SMManager.Revision<T>();
    }
  }

  public void Commit()
  {
    this.revision = SMManager.Revision<T>();
  }

  public void NotifyChanged()
  {
    if (!this.Loaded)
      return;
    SMManager.Change<T>(SMManager.Get<T>());
  }

  public bool IsChangedOnce()
  {
    int num = this.Changed ? 1 : 0;
    this.revision = SMManager.Revision<T>();
    return num != 0;
  }

  public T Value
  {
    get
    {
      return SMManager.Get<T>();
    }
  }
}
