﻿// Decompiled with JetBrains decompiler
// Type: Tower029WeaponEditScene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using UnityEngine;

public class Tower029WeaponEditScene : NGSceneBase
{
  [SerializeField]
  private Tower029WeaponEditMenu menu;

  public static void ChangeScene()
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("tower029_weapon_edit", true, (object[]) Array.Empty<object>());
  }

  public IEnumerator onStartSceneAsync()
  {
    Tower029WeaponEditScene tower029WeaponEditScene = this;
    IEnumerator e = tower029WeaponEditScene.menu.InitializeAsync();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    tower029WeaponEditScene.bgmFile = TowerUtil.BgmFile;
    tower029WeaponEditScene.bgmName = TowerUtil.BgmName;
  }

  public void onStartScene()
  {
    Singleton<CommonRoot>.GetInstance().isLoading = false;
  }
}
