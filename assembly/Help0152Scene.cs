﻿// Decompiled with JetBrains decompiler
// Type: Help0152Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using System.Collections;
using System.Collections.Generic;

public class Help0152Scene : NGSceneBase
{
  private static readonly string DEFAULT_SCENE = "help015_2";
  public Help0152Menu menu;

  public static void ChangeScene(bool stack, List<HelpHelp> helps)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene(Help0152Scene.DEFAULT_SCENE, (stack ? 1 : 0) != 0, (object) helps);
  }

  public static void ChangeScene(bool stack, HelpCategory helpCategory)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene(Help0152Scene.DEFAULT_SCENE, (stack ? 1 : 0) != 0, (object) helpCategory);
  }

  public IEnumerator onStartSceneAsync(HelpCategory helpCategory)
  {
    yield return (object) this.onStartSceneAsync(Help0151Button.getHelpList(helpCategory));
  }

  public IEnumerator onStartSceneAsync(List<HelpHelp> helps)
  {
    Help0152Scene help0152Scene = this;
    if (Singleton<NGGameDataManager>.GetInstance().IsEarth)
      help0152Scene.bgmName = "bgm104";
    IEnumerator e = help0152Scene.menu.InitSubCatecory(helps);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if (Singleton<CommonRoot>.GetInstance().isLoading)
    {
      yield return (object) null;
      Singleton<CommonRoot>.GetInstance().isLoading = false;
    }
  }
}
