﻿// Decompiled with JetBrains decompiler
// Type: Shop00713Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

public class Shop00713Menu : BackButtonMenuBase
{
  private System.Action btnAct;

  public void SetBtnAct(System.Action questChangeScene)
  {
    this.btnAct = questChangeScene;
  }

  public virtual void IbtnPopupOK()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<PopupManager>.GetInstance().closeAll(false);
    if (this.btnAct == null)
      return;
    this.btnAct();
  }

  public override void onBackButton()
  {
    this.IbtnPopupOK();
  }
}
