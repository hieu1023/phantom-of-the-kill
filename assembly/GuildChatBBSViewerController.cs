﻿// Decompiled with JetBrains decompiler
// Type: GuildChatBBSViewerController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class GuildChatBBSViewerController : MonoBehaviour
{
  [SerializeField]
  private UILabel textMessage;
  [SerializeField]
  private UIButton OKButton;
  [SerializeField]
  private UIButton EditButton;

  private void Start()
  {
  }

  private void Update()
  {
  }

  public void InitializeBBSViewerDialog()
  {
    GuildMembership guildMembership = ((IEnumerable<GuildMembership>) PlayerAffiliation.Current.guild.memberships).First<GuildMembership>((Func<GuildMembership, bool>) (x => x.player.player_id == Player.Current.id));
    if (guildMembership != null && guildMembership.role == GuildRole.master)
      this.EditButton.gameObject.SetActive(true);
    else
      this.EditButton.gameObject.SetActive(false);
    this.textMessage.SetTextLocalize(PlayerAffiliation.Current.guild.private_message);
  }

  public void OnBBSViewerOKButtonClicked()
  {
    Singleton<PopupManager>.GetInstance().dismiss(false);
  }

  public void OnBBSViewerEditButtonClicked()
  {
    Singleton<PopupManager>.GetInstance().dismiss(false);
    this.StartCoroutine(this.OpenBBSEditorDialog());
  }

  private IEnumerator OpenBBSEditorDialog()
  {
    GuildChatDetailedListController detailedListController = Singleton<CommonRoot>.GetInstance().guildChatManager.detailedListController;
    while (detailedListController.bbsEditorDialogPrefab == null)
      yield return (object) null;
    GameObject prefab = Singleton<CommonRoot>.GetInstance().guildChatManager.detailedListController.bbsEditorDialogPrefab.Result.Clone((Transform) null);
    prefab.GetComponent<GuildChatBBSEditorController>().InitializeBBSEditorDialog();
    Singleton<PopupManager>.GetInstance().open(prefab, false, false, true, true, false, false, "SE_1006");
  }
}
