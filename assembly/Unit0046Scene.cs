﻿// Decompiled with JetBrains decompiler
// Type: Unit0046Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit0046Scene : NGSceneBase
{
  public Unit0046Menu menu;

  public static void changeScene(
    bool stack,
    QuestLimitationBase[] limitations = null,
    string limitedDescription = null,
    bool isFromMypage = false)
  {
    if (limitations != null && limitations.Length != 0)
    {
      if (!Singleton<NGGameDataManager>.GetInstance().IsSea || Singleton<NGGameDataManager>.GetInstance().QuestType.HasValue)
      {
        CommonQuestType? questType = Singleton<NGGameDataManager>.GetInstance().QuestType;
        CommonQuestType commonQuestType = CommonQuestType.Sea;
        if (!(questType.GetValueOrDefault() == commonQuestType & questType.HasValue))
        {
          Singleton<NGSceneManager>.GetInstance().changeScene("unit004_6_0822", (stack ? 1 : 0) != 0, (object) new Unit0046Scene.LimitationData(limitations, limitedDescription));
          return;
        }
      }
      Singleton<NGSceneManager>.GetInstance().changeScene("unit004_6_0822_sea", (stack ? 1 : 0) != 0, (object) new Unit0046Scene.LimitationData(limitations, limitedDescription), (object) isFromMypage);
    }
    else
    {
      if (!Singleton<NGGameDataManager>.GetInstance().IsSea || Singleton<NGGameDataManager>.GetInstance().QuestType.HasValue)
      {
        CommonQuestType? questType = Singleton<NGGameDataManager>.GetInstance().QuestType;
        CommonQuestType commonQuestType = CommonQuestType.Sea;
        if (!(questType.GetValueOrDefault() == commonQuestType & questType.HasValue))
        {
          Singleton<NGSceneManager>.GetInstance().changeScene("unit004_6_0822", stack, (object[]) Array.Empty<object>());
          return;
        }
      }
      Singleton<NGSceneManager>.GetInstance().changeScene("unit004_6_0822_sea", (stack ? 1 : 0) != 0, (object) false, (object) isFromMypage);
    }
  }

  public static void changeScene(bool stack, ColosseumUtility.Info colosseumInfo)
  {
    if (Singleton<NGGameDataManager>.GetInstance().IsSea)
      Singleton<NGSceneManager>.GetInstance().changeScene("unit004_6_0822_sea", (stack ? 1 : 0) != 0, (object) colosseumInfo);
    else
      Singleton<NGSceneManager>.GetInstance().changeScene("unit004_6_0822", (stack ? 1 : 0) != 0, (object) colosseumInfo);
  }

  public static void changeSceneVersus(bool stack)
  {
    if (Singleton<NGGameDataManager>.GetInstance().IsSea)
      Singleton<NGSceneManager>.GetInstance().changeScene("unit004_6_0822_sea", (stack ? 1 : 0) != 0, (object) true);
    else
      Singleton<NGSceneManager>.GetInstance().changeScene("unit004_6_0822", (stack ? 1 : 0) != 0, (object) true);
  }

  public IEnumerator onStartSceneAsync()
  {
    Unit0046Scene unit0046Scene = this;
    Singleton<CommonRoot>.GetInstance().isTouchBlock = true;
    CommonQuestType? questType;
    if (!Singleton<NGGameDataManager>.GetInstance().IsSea || Singleton<NGGameDataManager>.GetInstance().QuestType.HasValue)
    {
      questType = Singleton<NGGameDataManager>.GetInstance().QuestType;
      CommonQuestType commonQuestType = CommonQuestType.Sea;
      if (!(questType.GetValueOrDefault() == commonQuestType & questType.HasValue))
        goto label_9;
    }
    IEnumerator e = unit0046Scene.SetSeaBackground();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    e = unit0046Scene.SetSeaBgm();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
label_9:
    unit0046Scene.menu.TopObject.GetComponent<UIWidget>().alpha = 0.0f;
    unit0046Scene.menu.MiddleObject.GetComponent<UIWidget>().alpha = 0.0f;
    if (Singleton<NGGameDataManager>.GetInstance().IsColosseum)
      Singleton<CommonRoot>.GetInstance().SetFooterEnable(false);
    e = ServerTime.WaitSync();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    unit0046Scene.menu.SevertTime = ServerTime.NowAppTime();
    if (Singleton<NGGameDataManager>.GetInstance().IsSea)
    {
      questType = Singleton<NGGameDataManager>.GetInstance().QuestType;
      if (!questType.HasValue)
        goto label_17;
    }
    questType = Singleton<NGGameDataManager>.GetInstance().QuestType;
    CommonQuestType commonQuestType1 = CommonQuestType.Sea;
    PlayerDeck[] _playerDecks;
    if (!(questType.GetValueOrDefault() == commonQuestType1 & questType.HasValue))
    {
      _playerDecks = SMManager.Get<PlayerDeck[]>();
      goto label_19;
    }
label_17:
    _playerDecks = PlayerSeaDeck.convertDeckData(SMManager.Get<PlayerSeaDeck[]>());
label_19:
    e = unit0046Scene.menu.InitPlayerDecks(_playerDecks);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    PlayerItem[] items = SMManager.Get<PlayerItem[]>().AllBattleSupplies();
    e = unit0046Scene.menu.InitPlayerItems(items);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    unit0046Scene.menu.TopObject.GetComponent<UIWidget>().alpha = 1f;
    unit0046Scene.menu.MiddleObject.GetComponent<UIWidget>().alpha = 1f;
    unit0046Scene.menu.old_indicator_select = -1;
    unit0046Scene.StartCoroutine(unit0046Scene.menu.Init3DModelPrefab());
    Singleton<CommonRoot>.GetInstance().isTouchBlock = false;
  }

  public IEnumerator onStartSceneAsync(ColosseumUtility.Info colosseumInfo)
  {
    Unit0046Scene unit0046Scene = this;
    Singleton<CommonRoot>.GetInstance().isTouchBlock = true;
    IEnumerator e;
    if (Singleton<NGGameDataManager>.GetInstance().IsSea)
    {
      e = unit0046Scene.SetSeaBackground();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      e = unit0046Scene.SetSeaBgm();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    unit0046Scene.menu.TopObject.GetComponent<UIWidget>().alpha = 0.0f;
    unit0046Scene.menu.MiddleObject.GetComponent<UIWidget>().alpha = 0.0f;
    if (Singleton<NGGameDataManager>.GetInstance().IsColosseum)
      Singleton<CommonRoot>.GetInstance().SetFooterEnable(false);
    unit0046Scene.menu.info = colosseumInfo;
    PlayerDeck[] _playerDecks = SMManager.Get<PlayerDeck[]>();
    e = unit0046Scene.menu.InitPlayerDecks(_playerDecks);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    PlayerItem[] items = SMManager.Get<PlayerItem[]>().AllBattleSupplies();
    e = unit0046Scene.menu.InitPlayerItems(items);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    unit0046Scene.menu.TopObject.GetComponent<UIWidget>().alpha = 1f;
    unit0046Scene.menu.MiddleObject.GetComponent<UIWidget>().alpha = 1f;
    unit0046Scene.menu.old_indicator_select = -1;
    unit0046Scene.StartCoroutine(unit0046Scene.menu.Init3DModelPrefab());
    Singleton<CommonRoot>.GetInstance().isTouchBlock = false;
  }

  public IEnumerator onStartSceneAsync(bool fromVersus)
  {
    this.menu.fromVersus = fromVersus;
    IEnumerator e = this.onStartSceneAsync();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public IEnumerator onStartSceneAsync(bool fromVersus, bool fromMypage)
  {
    this.menu.fromVersus = fromVersus;
    this.menu.fromMypage = fromMypage;
    IEnumerator e = this.onStartSceneAsync();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public IEnumerator onStartSceneAsync(Unit0046Scene.LimitationData limitationData)
  {
    this.menu.limitationData = limitationData;
    IEnumerator e = this.onStartSceneAsync();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public IEnumerator onStartSceneAsync(
    Unit0046Scene.LimitationData limitationData,
    bool fromMypage)
  {
    this.menu.limitationData = limitationData;
    this.menu.fromMypage = fromMypage;
    IEnumerator e = this.onStartSceneAsync();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public override IEnumerator onEndSceneAsync()
  {
    this.menu.stop = true;
    while (this.menu.coroutine)
      yield return (object) null;
  }

  public void onStartScene()
  {
    Singleton<CommonRoot>.GetInstance().isLoading = false;
  }

  public void onStartScene(ColosseumUtility.Info colosseumInfo)
  {
    this.onStartScene();
  }

  public void onStartScene(bool fromVersus)
  {
    this.onStartScene();
  }

  public void onStartScene(bool fromVersus, bool isFromMypage)
  {
    this.onStartScene();
  }

  public void onStartScene(Unit0046Scene.LimitationData limitationData)
  {
    this.onStartScene();
  }

  public void onStartScene(Unit0046Scene.LimitationData limitationData, bool isFromMypage)
  {
    this.onStartScene();
  }

  public override void onEndScene()
  {
    this.menu.onEndScene();
    this.menu.stop = true;
  }

  private IEnumerator SetSeaBackground()
  {
    Unit0046Scene unit0046Scene = this;
    Future<GameObject> bgF = new ResourceObject("Prefabs/BackGround/DefaultBackground_sea").Load<GameObject>();
    IEnumerator e = bgF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if ((UnityEngine.Object) bgF.Result != (UnityEngine.Object) null)
      unit0046Scene.backgroundPrefab = bgF.Result;
  }

  private IEnumerator SetSeaBgm()
  {
    Unit0046Scene unit0046Scene = this;
    IEnumerator e = ServerTime.WaitSync();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    SeaHomeMap seaHomeMap = ((IEnumerable<SeaHomeMap>) MasterData.SeaHomeMapList).ActiveSeaHomeMap(ServerTime.NowAppTimeAddDelta());
    if (seaHomeMap != null && !string.IsNullOrEmpty(seaHomeMap.bgm_cuesheet_name) && !string.IsNullOrEmpty(seaHomeMap.bgm_cue_name))
    {
      unit0046Scene.bgmFile = seaHomeMap.bgm_cuesheet_name;
      unit0046Scene.bgmName = seaHomeMap.bgm_cue_name;
    }
  }

  public class LimitationData
  {
    public QuestLimitationBase[] limitations_ { get; private set; }

    public string description_ { get; private set; }

    public LimitationData(QuestLimitationBase[] limitations, string description)
    {
      this.limitations_ = limitations;
      this.description_ = description;
    }
  }
}
