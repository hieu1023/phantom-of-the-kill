﻿// Decompiled with JetBrains decompiler
// Type: SpriteSelectDirect
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[RequireComponent(typeof (UISprite))]
public class SpriteSelectDirect : MonoBehaviour
{
  protected string spriteName = string.Empty;
  [SerializeField]
  protected string spritePrefix;
  [SerializeField]
  protected string spriteExt;
  [SerializeField]
  private UISprite sprite;

  public UISprite Sprite
  {
    get
    {
      return this.sprite;
    }
  }

  public virtual void SetSpriteName<T>(T n, bool resizeTarget = true)
  {
    this.spriteName = string.Format(this.spritePrefix + "{0}" + this.spriteExt, (object) n);
    this.sprite.ChangeSprite(this.spriteName, resizeTarget);
  }
}
