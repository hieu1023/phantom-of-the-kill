﻿// Decompiled with JetBrains decompiler
// Type: Guide01142bMenu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Guide01142bMenu : BackButtonMenuBase
{
  private bool isArrowBtn = true;
  public NGxScroll scroll;
  private int currentIndex;
  private int lastIndex;
  [SerializeField]
  private UICenterOnChild centerOnChild;
  [SerializeField]
  private UILabel txtTitle;
  [SerializeField]
  private GameObject rightArrow;
  [SerializeField]
  private GameObject leftArrow;
  private GameObject[] detailObject;
  private Dictionary<GameObject, Guide0112BuguDetailB> detailPrefabDict;

  public IEnumerator onStartSceneAsync(GearGear gear, bool isDispNumber, int index)
  {
    Guide01142bMenu guide01142bMenu = this;
    guide01142bMenu.leftArrow.SetActive(false);
    guide01142bMenu.rightArrow.SetActive(false);
    guide01142bMenu.scroll.scrollView.enabled = false;
    IEnumerator e = guide01142bMenu.GearsInit(new GearGear[1]
    {
      gear
    }, (isDispNumber ? 1 : 0) != 0, index);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public IEnumerator onStartSceneAsync(GearGear[] gears, bool isDispNumber, int index = 0)
  {
    IEnumerator e = this.GearsInit(gears, isDispNumber, index);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public IEnumerator GearsInit(GearGear[] gear, bool isDispNumber, int index)
  {
    Guide01142bMenu m = this;
    Future<GameObject> prefabF = Res.Prefabs.guide011_4_2.guid_bugu_detail_b.Load<GameObject>();
    IEnumerator e = prefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    GameObject result = prefabF.Result;
    m.detailObject = new GameObject[gear.Length];
    m.detailPrefabDict = new Dictionary<GameObject, Guide0112BuguDetailB>();
    for (int index1 = 0; index1 < gear.Length; ++index1)
    {
      m.detailObject[index1] = Object.Instantiate<GameObject>(result);
      m.scroll.Add(m.detailObject[index1], false);
      Guide0112BuguDetailB component = m.detailObject[index1].GetComponent<Guide0112BuguDetailB>();
      m.detailPrefabDict.Add(m.detailObject[index1], component);
    }
    m.scroll.ResolvePosition();
    for (int i = 0; i < m.detailObject.Length; ++i)
    {
      Guide0112BuguDetailB d = m.detailObject[i].GetComponent<Guide0112BuguDetailB>();
      d.Init(m, gear[i], isDispNumber);
      e = d.InitDetailedScreen(gear[i]);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      d.index = i;
      d.SetContainerPosition();
      d = (Guide0112BuguDetailB) null;
    }
    m.currentIndex = index;
    m.scroll.scrollView.transform.localPosition = new Vector3(-m.scroll.grid.cellWidth * (float) m.currentIndex, 0.0f, 0.0f);
    m.SetMenuInformation(m.currentIndex);
    Singleton<CommonRoot>.GetInstance().HideLoadingLayer();
  }

  protected override void Update()
  {
    base.Update();
    if ((double) this.scroll.scrollView.transform.localPosition.x < 0.0)
    {
      this.currentIndex = (int) Mathf.Abs((this.scroll.scrollView.transform.localPosition.x - this.scroll.grid.cellWidth / 2f) / this.scroll.grid.cellWidth);
      this.currentIndex = this.currentIndex <= this.detailObject.Length ? this.currentIndex : this.detailObject.Length - 1;
    }
    if (this.currentIndex == this.lastIndex)
      return;
    this.SetMenuInformation(this.currentIndex);
    this.lastIndex = this.currentIndex;
  }

  private void SetMenuInformation(int idx)
  {
    if (idx < 0 || idx > this.detailObject.Length - 1)
      return;
    this.detailObject[idx].GetComponent<Guide0112BuguDetailB>().SetGearInformation();
    this.rightArrow.SetActive(true);
    this.leftArrow.SetActive(true);
    if (idx == 0)
      this.leftArrow.SetActive(false);
    if (idx != this.detailObject.Length - 1)
      return;
    this.rightArrow.SetActive(false);
  }

  public void SetTitleText(string gearName)
  {
    this.txtTitle.gameObject.SetActive(true);
    this.txtTitle.SetText(gearName);
  }

  private void CenterOnChild(int num)
  {
    foreach (GameObject index in this.detailObject)
    {
      if (this.detailPrefabDict[index].index == num)
      {
        this.currentIndex = num;
        this.centerOnChild.onFinished = (SpringPanel.OnFinished) (() => this.isArrowBtn = true);
        this.centerOnChild.CenterOn(index.transform);
        break;
      }
    }
  }

  public void IbtnLeftArrow()
  {
    if (!this.isArrowBtn)
      return;
    this.isArrowBtn = false;
    int num = this.currentIndex - 1;
    if (num < 0)
      return;
    this.CenterOnChild(num);
  }

  public void IbtnRightArrow()
  {
    if (!this.isArrowBtn)
      return;
    this.isArrowBtn = false;
    int num = this.currentIndex + 1;
    if (num > this.detailObject.Length - 1)
      return;
    this.CenterOnChild(num);
  }

  public virtual void IbtnBack()
  {
    if (this.IsPushAndSet())
      return;
    this.backScene();
  }

  public override void onBackButton()
  {
    this.IbtnBack();
  }
}
