﻿// Decompiled with JetBrains decompiler
// Type: SafeAreaBandRoot
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class SafeAreaBandRoot : MonoBehaviour
{
  private static SafeAreaBandRoot instance;
  [SerializeField]
  private GameObject bandPanel;

  private void Awake()
  {
    if ((Object) SafeAreaBandRoot.instance != (Object) null)
    {
      Object.Destroy((Object) this.gameObject);
    }
    else
    {
      SafeAreaBandRoot.instance = this;
      Object.DontDestroyOnLoad((Object) this.gameObject);
      this.GetComponent<UIRoot>().manualHeight = ModalWindow.setupRootPanel(this.GetComponent<UIRoot>());
    }
  }

  public static void ShowSafeAreaBand()
  {
    if (!((Object) SafeAreaBandRoot.instance != (Object) null))
      return;
    SafeAreaBandRoot.instance.bandPanel.SetActive(true);
  }

  public static void HideSafeAreaBand()
  {
    if (!((Object) SafeAreaBandRoot.instance != (Object) null))
      return;
    SafeAreaBandRoot.instance.bandPanel.SetActive(false);
  }

  public static void DestroySafeAreaBand()
  {
    if (!((Object) SafeAreaBandRoot.instance != (Object) null))
      return;
    Object.Destroy((Object) SafeAreaBandRoot.instance.gameObject);
    SafeAreaBandRoot.instance = (SafeAreaBandRoot) null;
  }

  private void Start()
  {
  }

  private void Update()
  {
  }
}
