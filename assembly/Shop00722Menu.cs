﻿// Decompiled with JetBrains decompiler
// Type: Shop00722Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Shop00722Menu : BackButtonMenuBase
{
  [SerializeField]
  private NGxScroll scroll;
  [SerializeField]
  private GameObject dynProductBanner;
  [SerializeField]
  private UILabel txtProductDescription;
  [SerializeField]
  private UIButton ibtnPopupBuy;
  [SerializeField]
  private GameObject slcItemFull;
  private int m_shopID;
  private PlayerShopArticle m_article;
  private GameObject detailPopup;
  private Shop00721SpecialShopProduction bannerObject;

  public void UpdateInfo()
  {
    this.StartCoroutine(this.UpdateArticleInfo());
  }

  public IEnumerator UpdateArticleInfo()
  {
    Shop00722Menu shop00722Menu = this;
    bool flag = false;
    Shop[] shopArray = SMManager.Get<Shop[]>();
    if (shopArray != null)
    {
      // ISSUE: reference to a compiler-generated method
      Shop shop = ((IEnumerable<Shop>) shopArray).FirstOrDefault<Shop>(new Func<Shop, bool>(shop00722Menu.\u003CUpdateArticleInfo\u003Eb__10_0));
      if (shop != null)
      {
        // ISSUE: reference to a compiler-generated method
        PlayerShopArticle article = ((IEnumerable<PlayerShopArticle>) shop.articles).FirstOrDefault<PlayerShopArticle>(new Func<PlayerShopArticle, bool>(shop00722Menu.\u003CUpdateArticleInfo\u003Eb__10_1));
        if (article != null)
        {
          IEnumerator e = ServerTime.WaitSync();
          while (e.MoveNext())
            yield return e.Current;
          e = (IEnumerator) null;
          e = shop00722Menu.bannerObject.Init(ServerTime.NowAppTime(), shop.id, article, (Shop00721Menu) null, 0);
          while (e.MoveNext())
            yield return e.Current;
          e = (IEnumerator) null;
          shop00722Menu.SetBtnActive(article, ServerTime.NowAppTime());
          flag = true;
        }
        article = (PlayerShopArticle) null;
      }
      shop = (Shop) null;
    }
    if (!flag)
      shop00722Menu.BackScene();
  }

  private void SetBtnActive(PlayerShopArticle article, DateTime serverTime)
  {
    this.ibtnPopupBuy.isEnabled = true;
    this.slcItemFull.SetActive(false);
    if (article.recharge_at.HasValue && serverTime < article.recharge_at.Value)
    {
      this.ibtnPopupBuy.isEnabled = false;
    }
    else
    {
      if (article.limit.HasValue)
      {
        int? limit = article.limit;
        int num = 0;
        if (limit.GetValueOrDefault() < num & limit.HasValue)
        {
          this.ibtnPopupBuy.isEnabled = false;
          return;
        }
      }
      if (!article.article.limit.HasValue && !article.article.daily_limit.HasValue || (!article.article.limit.HasValue || article.limit.Value > 0) && (!article.article.daily_limit.HasValue || article.limit.Value > 0))
        return;
      this.ibtnPopupBuy.isEnabled = false;
    }
  }

  public IEnumerator Init(
    int shopID,
    PlayerShopArticle article,
    GameObject product,
    DateTime serverTime)
  {
    Shop00722Menu shop00722Menu = this;
    shop00722Menu.m_shopID = shopID;
    shop00722Menu.m_article = article;
    GameObject gameObject = product.Clone(shop00722Menu.dynProductBanner.transform);
    shop00722Menu.bannerObject = gameObject.GetComponent<Shop00721SpecialShopProduction>();
    shop00722Menu.bannerObject.Btn.enabled = false;
    yield return (object) shop00722Menu.StartCoroutine(shop00722Menu.SetText(article.article.description));
    if ((UnityEngine.Object) shop00722Menu.detailPopup == (UnityEngine.Object) null)
    {
      Future<GameObject> detailPopupF = Res.Prefabs.popup.popup_000_7_4_2__anim_popup01.Load<GameObject>();
      IEnumerator e = detailPopupF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      shop00722Menu.detailPopup = detailPopupF.Result;
      detailPopupF = (Future<GameObject>) null;
    }
    yield return (object) shop00722Menu.StartCoroutine(shop00722Menu.createList(article));
    shop00722Menu.scroll.ResolvePosition();
    shop00722Menu.SetBtnActive(article, serverTime);
  }

  private IEnumerator SetText(string description)
  {
    this.txtProductDescription.SetTextLocalize(description);
    yield break;
  }

  private IEnumerator createList(PlayerShopArticle article)
  {
    Shop00722Menu shop00722Menu = this;
    Future<GameObject> prefabF = Res.Prefabs.shop007_22.dir_product.Load<GameObject>();
    IEnumerator e = prefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    GameObject prefab = prefabF.Result;
    ShopContent[] shopContentArray = article.article.ShopContents;
    for (int index = 0; index < shopContentArray.Length; ++index)
    {
      ShopContent content = shopContentArray[index];
      GameObject obj = UnityEngine.Object.Instantiate<GameObject>(prefab);
      obj.SetActive(false);
      e = obj.GetComponent<Shop00722Product>().Init(content, new System.Action<ShopContent>(shop00722Menu.IconBtnAction));
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      shop00722Menu.scroll.Add(obj, false);
      obj = (GameObject) null;
    }
    shopContentArray = (ShopContent[]) null;
    foreach (Component child in shop00722Menu.scroll.grid.transform.GetChildren())
      child.gameObject.SetActive(true);
  }

  public void IconBtnAction(ShopContent content)
  {
    if (this.IsPushAndSet())
      return;
    this.StartCoroutine(this.ShowDetailPopup(content));
  }

  private IEnumerator ShowDetailPopup(ShopContent content)
  {
    GameObject popup = Singleton<PopupManager>.GetInstance().open(this.detailPopup, false, false, false, true, false, false, "SE_1006");
    popup.SetActive(false);
    IEnumerator e = popup.GetComponent<Shop00742Menu>().Init(content);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    popup.SetActive(true);
  }

  protected override void Update()
  {
    base.Update();
  }

  public virtual void Foreground()
  {
  }

  public void IbtnBuy()
  {
    this.StartCoroutine(this.popupAlert007222());
  }

  public void IbtnFonds()
  {
    if (this.IsPushAndSet())
      return;
    this.StartCoroutine(PopupUtility._007_19());
  }

  public void IbtnSpecific()
  {
    if (this.IsPushAndSet())
      return;
    this.StartCoroutine(PopupUtility._007_18());
  }

  private IEnumerator popupAlert007222()
  {
    Shop00722Menu menu = this;
    Future<GameObject> prefab = Res.Prefabs.popup.popup_007_22_2__anim_popup01.Load<GameObject>();
    IEnumerator e = prefab.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Singleton<PopupManager>.GetInstance().open(prefab.Result, false, false, false, true, false, false, "SE_1006").GetComponent<Shop007222Menu>().Init(menu.m_article, menu);
  }

  public virtual void IbtnBack()
  {
    if (this.IsPushAndSet())
      return;
    this.backScene();
  }

  public override void onBackButton()
  {
    this.IbtnBack();
  }

  public virtual void VScrollBar()
  {
  }

  public void BackScene()
  {
    this.backScene();
  }
}
