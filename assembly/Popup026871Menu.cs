﻿// Decompiled with JetBrains decompiler
// Type: Popup026871Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;
using UnityEngine;

public class Popup026871Menu : BackButtonMonoBehaiviour
{
  [SerializeField]
  private UILabel txtTitle;
  [SerializeField]
  private UILabel txtDescription1;
  [SerializeField]
  private UILabel txtDescription2;

  public void Init(int battleCnt)
  {
    Consts instance = Consts.GetInstance();
    this.txtTitle.SetText(instance.VERSUS_0026871POPUP_TITLE);
    this.txtDescription1.SetText(instance.VERSUS_0026871POPUP_DESCRIPTION1);
    this.txtDescription2.SetText(Consts.Format(instance.VERSUS_0026871POPUP_DESCRIPTION2, (IDictionary) new Hashtable()
    {
      {
        (object) "cnt",
        (object) battleCnt.ToLocalizeNumberText()
      }
    }));
  }

  public void IbtnOK()
  {
    Singleton<PopupManager>.GetInstance().onDismiss();
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    Singleton<NGGameDataManager>.GetInstance().StartCoroutine(this.SceneUpdate());
  }

  private IEnumerator SceneUpdate()
  {
    GameObject gameObject = GameObject.Find("Versus02610Versus02610Scene UI Root");
    if ((UnityEngine.Object) gameObject != (UnityEngine.Object) null)
    {
      gameObject.GetComponent<Versus02610Menu>().StartSceneUpdate((System.Action) null);
      yield break;
    }
  }

  public void IbtnNo()
  {
    this.IbtnOK();
  }

  public override void onBackButton()
  {
    this.IbtnNo();
  }
}
