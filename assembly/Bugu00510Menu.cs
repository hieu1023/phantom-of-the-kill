﻿// Decompiled with JetBrains decompiler
// Type: Bugu00510Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Bugu00510Menu : BackButtonMenuBase
{
  private readonly Bugu00510Menu.SortType[] sortTypes = new Bugu00510Menu.SortType[4]
  {
    Bugu00510Menu.SortType.Recommend,
    Bugu00510Menu.SortType.Category,
    Bugu00510Menu.SortType.Rarity,
    Bugu00510Menu.SortType.Name
  };
  private List<ItemIcon> allItemIcon = new List<ItemIcon>();
  private List<InventoryItem> inventoryItems = new List<InventoryItem>();
  private Dictionary<int, Tuple<List<GameCore.ItemInfo>, int>> playerGearDic = new Dictionary<int, Tuple<List<GameCore.ItemInfo>, int>>();
  private Dictionary<int, Tuple<List<GameCore.ItemInfo>, int>> playerGearDicDescending = new Dictionary<int, Tuple<List<GameCore.ItemInfo>, int>>();
  private List<int> recipeGearIDList = new List<int>();
  [SerializeField]
  private UIScrollView ScrollView;
  private SortAndFilter.SORT_TYPE_ORDER_BUY currOrderBuy;
  [SerializeField]
  private NGxScroll2 scroll;
  [SerializeField]
  private GameObject[] SortLabel;
  [SerializeField]
  private GameObject recipeRoot;
  private List<Bugu00510Menu.RecipeData> recipeDataList;
  private int sortIndex;
  private float scroolStartY;
  private bool isInitialize;
  private GameObject recipePopupPrefab;
  private GameObject kakuninnPopupPrefab;
  private GameObject itemIconPrefab;
  [HideInInspector]
  public Bugu0053DirRecipePopup dirRecipe;
  private Bugu00510Menu.RecipeData currentRecipe;
  private bool isIntegrated;

  public IEnumerator InitAsync()
  {
    this.isInitialize = false;
    Future<GameObject> prefabF = Res.Prefabs.ItemIcon.prefab.Load<GameObject>();
    IEnumerator e = prefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.itemIconPrefab = prefabF.Result;
    Future<GameObject> popupPrefabF = Res.Prefabs.popup.popup_005_recipe_composite.Load<GameObject>();
    e = popupPrefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.kakuninnPopupPrefab = popupPrefabF.Result;
    prefabF = Res.Prefabs.bugu005_3.dir_Recipe.Load<GameObject>();
    e = prefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.recipePopupPrefab = prefabF.Result;
    this.dirRecipe = this.recipePopupPrefab.CloneAndGetComponent<Bugu0053DirRecipePopup>(this.recipeRoot);
    this.dirRecipe.gameObject.GetComponent<NGTweenParts>().isActive = false;
    this.allItemIcon.Clear();
    this.recipeGearIDList.Clear();
    this.isIntegrated = false;
    this.currentRecipe = (Bugu00510Menu.RecipeData) null;
  }

  public IEnumerator StartAsync(bool isForceSetup = false)
  {
    Bugu00510Menu bugu00510Menu = this;
    if (bugu00510Menu.currentRecipe == null || isForceSetup || bugu00510Menu.isIntegrated)
    {
      if (bugu00510Menu.currentRecipe == null)
        Singleton<CommonRoot>.GetInstance().ShowLoadingLayer(1, true);
      IEnumerator e = ServerTime.WaitSync();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      List<GameCore.ItemInfo> itemInfoList = new List<GameCore.ItemInfo>();
      ((IEnumerable<PlayerItem>) SMManager.Get<PlayerItem[]>()).ForEach<PlayerItem>((System.Action<PlayerItem>) (x => itemInfoList.Add(new GameCore.ItemInfo(x))));
      ((IEnumerable<PlayerMaterialGear>) SMManager.Get<PlayerMaterialGear[]>()).ForEach<PlayerMaterialGear>((System.Action<PlayerMaterialGear>) (x => itemInfoList.Add(new GameCore.ItemInfo(x, 0))));
      bugu00510Menu.playerGearDic.Clear();
      bugu00510Menu.playerGearDicDescending.Clear();
      foreach (IGrouping<int, GameCore.ItemInfo> source in itemInfoList.Where<GameCore.ItemInfo>((Func<GameCore.ItemInfo, bool>) (x => x.isWeapon || x.isCompse || x.isWeaponMaterial)).GroupBy<GameCore.ItemInfo, int>((Func<GameCore.ItemInfo, int>) (x => x.gear.group_id)))
      {
        List<GameCore.ItemInfo> list1 = source.OrderBy<GameCore.ItemInfo, int>((Func<GameCore.ItemInfo, int>) (x => this.CheckCanMaterial(x))).ThenBy<GameCore.ItemInfo, int>((Func<GameCore.ItemInfo, int>) (x => !x.isDisappearItem ? int.MaxValue : x.gearAccessoryRemainingAmount)).ThenBy<GameCore.ItemInfo, bool>((Func<GameCore.ItemInfo, bool>) (x => !x.isWeaponMaterial)).ThenBy<GameCore.ItemInfo, int>((Func<GameCore.ItemInfo, int>) (x => x.gearLevel)).ThenBy<GameCore.ItemInfo, int>((Func<GameCore.ItemInfo, int>) (x => x.gear.ID)).ToList<GameCore.ItemInfo>();
        int num1 = list1.Sum<GameCore.ItemInfo>((Func<GameCore.ItemInfo, int>) (x => x.quantity));
        bugu00510Menu.playerGearDic[source.Key] = new Tuple<List<GameCore.ItemInfo>, int>(list1, num1);
        List<GameCore.ItemInfo> list2 = source.OrderBy<GameCore.ItemInfo, int>((Func<GameCore.ItemInfo, int>) (x => this.CheckCanMaterial(x))).ThenBy<GameCore.ItemInfo, int>((Func<GameCore.ItemInfo, int>) (x => !x.isDisappearItem ? int.MaxValue : x.gearAccessoryRemainingAmount)).ThenBy<GameCore.ItemInfo, bool>((Func<GameCore.ItemInfo, bool>) (x => !x.isWeaponMaterial)).ThenByDescending<GameCore.ItemInfo, int>((Func<GameCore.ItemInfo, int>) (x => x.gearLevel)).ThenBy<GameCore.ItemInfo, int>((Func<GameCore.ItemInfo, int>) (x => x.gear.ID)).ThenBy<GameCore.ItemInfo, int>((Func<GameCore.ItemInfo, int>) (x => x.gearLevelUnLimit)).ToList<GameCore.ItemInfo>();
        int num2 = list2.Sum<GameCore.ItemInfo>((Func<GameCore.ItemInfo, int>) (x => x.quantity));
        bugu00510Menu.playerGearDicDescending[source.Key] = new Tuple<List<GameCore.ItemInfo>, int>(list2, num2);
      }
      bugu00510Menu.SetNewInventory();
      List<Tuple<int, GameCore.ItemInfo, int>> playerItemData = bugu00510Menu.playerGearDic.SelectMany<KeyValuePair<int, Tuple<List<GameCore.ItemInfo>, int>>, Tuple<int, GameCore.ItemInfo, int>>((Func<KeyValuePair<int, Tuple<List<GameCore.ItemInfo>, int>>, IEnumerable<Tuple<int, GameCore.ItemInfo, int>>>) (x => x.Value.Item1.Where<GameCore.ItemInfo>((Func<GameCore.ItemInfo, bool>) (y => !y.broken && !y.favorite && !y.ForBattle)).Select<GameCore.ItemInfo, Tuple<int, GameCore.ItemInfo, int>>((Func<GameCore.ItemInfo, Tuple<int, GameCore.ItemInfo, int>>) (y => new Tuple<int, GameCore.ItemInfo, int>(x.Key, y, x.Value.Item2))))).OrderBy<Tuple<int, GameCore.ItemInfo, int>, int>((Func<Tuple<int, GameCore.ItemInfo, int>, int>) (x => x.Item1)).ThenBy<Tuple<int, GameCore.ItemInfo, int>, int>((Func<Tuple<int, GameCore.ItemInfo, int>, int>) (x => x.Item2.gearLevel)).ToList<Tuple<int, GameCore.ItemInfo, int>>();
      bugu00510Menu.recipeDataList = new List<Bugu00510Menu.RecipeData>();
      ((IEnumerable<GearCombineRecipe>) MasterData.GearCombineRecipeList).Where<GearCombineRecipe>((Func<GearCombineRecipe, bool>) (x =>
      {
        if (x.start_at.HasValue)
        {
          DateTime? startAt = x.start_at;
          DateTime dateTime = ServerTime.NowAppTime();
          if ((startAt.HasValue ? (startAt.GetValueOrDefault() <= dateTime ? 1 : 0) : 0) == 0)
            goto label_5;
        }
        if (x.end_at.HasValue)
        {
          DateTime? endAt = x.end_at;
          DateTime dateTime = ServerTime.NowAppTime();
          if ((endAt.HasValue ? (endAt.GetValueOrDefault() >= dateTime ? 1 : 0) : 0) == 0)
            goto label_5;
        }
        return MasterData.GearGear.ContainsKey(x.combined_gear_id);
label_5:
        return false;
      })).GroupBy<GearCombineRecipe, int>((Func<GearCombineRecipe, int>) (x => MasterData.GearGear[x.combined_gear_id].group_id)).ForEach<IGrouping<int, GearCombineRecipe>>((System.Action<IGrouping<int, GearCombineRecipe>>) (x => this.recipeDataList.Add(new Bugu00510Menu.RecipeData((IEnumerable<GearCombineRecipe>) x, x.Any<GearCombineRecipe>((Func<GearCombineRecipe, bool>) (y => this.CheckEnableRecipe(playerItemData, y)))))));
      float timer = Time.realtimeSinceStartup;
      List<string> recipePaths = new List<string>();
      foreach (Bugu00510Menu.RecipeData recipeData in bugu00510Menu.recipeDataList)
      {
        Bugu00510Menu.RecipeData recipe = recipeData;
        if (!bugu00510Menu.recipeGearIDList.Any<int>((Func<int, bool>) (x => x == recipe.combinedGear.ID)))
        {
          recipePaths.AddRange((IEnumerable<string>) recipe.GetResouceNames());
          bugu00510Menu.recipeGearIDList.Add(recipe.combinedGear.ID);
          if ((double) Time.realtimeSinceStartup - (double) timer > 0.200000002980232)
          {
            timer = Time.realtimeSinceStartup;
            yield return (object) null;
          }
        }
      }
      yield return (object) null;
      e = OnDemandDownload.waitLoadSomethingResource((IEnumerable<string>) recipePaths, false, false);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      for (int count = bugu00510Menu.allItemIcon.Count; count < Mathf.Min(ItemIcon.MaxValue, bugu00510Menu.recipeDataList.Count); ++count)
      {
        ItemIcon component = bugu00510Menu.itemIconPrefab.Clone((Transform) null).GetComponent<ItemIcon>();
        bugu00510Menu.allItemIcon.Add(component);
        component.gameObject.SetActive(false);
      }
      bugu00510Menu.Sort();
      e = bugu00510Menu.CreateAllIcon();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      bugu00510Menu.SetScrollItem();
      bugu00510Menu.scroolStartY = bugu00510Menu.scroll.scrollView.transform.localPosition.y;
      if (bugu00510Menu.currentRecipe != null)
      {
        int index = bugu00510Menu.recipeDataList.FindIndex((Predicate<Bugu00510Menu.RecipeData>) (x => x.combinedGear.ID == this.currentRecipe.combinedGear.ID));
        bugu00510Menu.scroll.ResolvePosition(index, bugu00510Menu.recipeDataList.Count);
      }
      bugu00510Menu.StartCoroutine(bugu00510Menu.LoadObject());
      Singleton<CommonRoot>.GetInstance().HideLoadingLayer();
      bugu00510Menu.isIntegrated = false;
      ItemIcon.IsPoolCache = true;
      bugu00510Menu.isInitialize = true;
    }
  }

  public IEnumerator StartAsync(
    GearGear sGear,
    List<InventoryItem> selectedItems,
    List<GearCombineRecipe> sGearRecipes)
  {
    IEnumerator e = this.StartAsync(true);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    List<GearGear> gearGearList = new List<GearGear>();
    foreach (InventoryItem selectedItem in selectedItems)
      gearGearList.Add(selectedItem.Item.gear);
    this.PushOK(sGear, selectedItems, sGearRecipes, this.SetZenie((IEnumerable<GearGear>) gearGearList));
  }

  private bool CheckEnableRecipe(
    List<Tuple<int, GameCore.ItemInfo, int>> playerItemData,
    GearCombineRecipe recipe)
  {
    List<Tuple<int, int>> source = new List<Tuple<int, int>>();
    source.Add(new Tuple<int, int>(recipe.material1_gear_id, recipe.material1_gear_rank.HasValue ? recipe.material1_gear_rank.Value : 0));
    if (recipe.material2_gear_id.HasValue)
      source.Add(new Tuple<int, int>(recipe.material2_gear_id.Value, recipe.material2_gear_rank.HasValue ? recipe.material2_gear_rank.Value : 0));
    if (recipe.material3_gear_id.HasValue)
      source.Add(new Tuple<int, int>(recipe.material3_gear_id.Value, recipe.material3_gear_rank.HasValue ? recipe.material3_gear_rank.Value : 0));
    if (recipe.material4_gear_id.HasValue)
      source.Add(new Tuple<int, int>(recipe.material4_gear_id.Value, recipe.material4_gear_rank.HasValue ? recipe.material4_gear_rank.Value : 0));
    if (recipe.material5_gear_id.HasValue)
      source.Add(new Tuple<int, int>(recipe.material5_gear_id.Value, recipe.material5_gear_rank.HasValue ? recipe.material5_gear_rank.Value : 0));
    Dictionary<int, int> dictionary = new Dictionary<int, int>();
    List<GameCore.ItemInfo> usebleItemInfoList = new List<GameCore.ItemInfo>();
    foreach (Tuple<int, int> tuple1 in (IEnumerable<Tuple<int, int>>) source.OrderBy<Tuple<int, int>, int>((Func<Tuple<int, int>, int>) (x => x.Item1)).ThenBy<Tuple<int, int>, int>((Func<Tuple<int, int>, int>) (x => x.Item2)))
    {
      Tuple<int, int> part = tuple1;
      Tuple<int, GameCore.ItemInfo, int> tuple2 = playerItemData.Find((Predicate<Tuple<int, GameCore.ItemInfo, int>>) (x => x.Item1 == part.Item1 && x.Item2.gearLevel >= part.Item2 && !usebleItemInfoList.Any<GameCore.ItemInfo>((Func<GameCore.ItemInfo, bool>) (y => x.Item2 == y))));
      if (tuple2 == null)
        return false;
      if (MasterData.GearGear[tuple2.Item2.masterID].isMaterial() || tuple2.Item2.isWeaponMaterial)
      {
        if (!dictionary.ContainsKey(part.Item1))
          dictionary[part.Item1] = 1;
        else
          dictionary[part.Item1]++;
        if (dictionary[part.Item1] > tuple2.Item3)
          return false;
      }
      else
        usebleItemInfoList.Add(tuple2.Item2);
    }
    return SMManager.Get<Player>().CheckZeny(this.SetZenie(source.Select<Tuple<int, int>, GearGear>((Func<Tuple<int, int>, GearGear>) (x => ((IEnumerable<GearGear>) MasterData.GearGearList).FirstOrDefault<GearGear>((Func<GearGear, bool>) (gear => gear.group_id == x.Item1))))));
  }

  public int SetZenie(IEnumerable<GearGear> gears)
  {
    int total_item_level = 0;
    int total_item_rarity = 0;
    int cnt_use_gears = 0;
    gears.ForEach<GearGear>((System.Action<GearGear>) (item =>
    {
      if (item == null)
        return;
      total_item_level += item.compose_level;
      total_item_rarity += item.rarity.index;
      ++cnt_use_gears;
    }));
    if (cnt_use_gears < 1)
      cnt_use_gears = 1;
    int index = total_item_rarity / cnt_use_gears - 1;
    if (index < 0)
      index = 0;
    NGGameDataManager.Boost boostInfo = Singleton<NGGameDataManager>.GetInstance().BoostInfo;
    return (int) ((boostInfo == null ? new Decimal(10, 0, 0, false, (byte) 1) : boostInfo.DiscountGearCombine) * (Decimal) total_item_level * new Decimal(50) * (Decimal) GearRarity.ComposeRatio(index));
  }

  private int CheckCanMaterial(GameCore.ItemInfo gear)
  {
    int num = 0;
    if (gear.broken)
      ++num;
    if (gear.favorite)
      ++num;
    if (gear.ForBattle)
      ++num;
    return num;
  }

  private void Sort()
  {
    ((IEnumerable<GameObject>) this.SortLabel).ToggleOnce(this.sortIndex);
    switch (this.sortTypes[this.sortIndex])
    {
      case Bugu00510Menu.SortType.Recommend:
        this.recipeDataList = this.recipeDataList.OrderBy<Bugu00510Menu.RecipeData, int>((Func<Bugu00510Menu.RecipeData, int>) (x => x.priority)).ThenByDescending<Bugu00510Menu.RecipeData, int>((Func<Bugu00510Menu.RecipeData, int>) (x => x.combinedGear.rarity.index)).ThenBy<Bugu00510Menu.RecipeData, int>((Func<Bugu00510Menu.RecipeData, int>) (x => x.combinedGear.resource_reference_gear_id_GearGear)).ToList<Bugu00510Menu.RecipeData>();
        break;
      case Bugu00510Menu.SortType.Category:
        this.recipeDataList = this.recipeDataList.OrderBy<Bugu00510Menu.RecipeData, GearKindEnum>((Func<Bugu00510Menu.RecipeData, GearKindEnum>) (x => x.combinedGear.kind.Enum)).ThenByDescending<Bugu00510Menu.RecipeData, int>((Func<Bugu00510Menu.RecipeData, int>) (x => x.combinedGear.resource_reference_gear_id_GearGear)).ThenByDescending<Bugu00510Menu.RecipeData, int>((Func<Bugu00510Menu.RecipeData, int>) (x => x.combinedGear.rarity.index)).ToList<Bugu00510Menu.RecipeData>();
        break;
      case Bugu00510Menu.SortType.Rarity:
        this.recipeDataList = this.recipeDataList.OrderByDescending<Bugu00510Menu.RecipeData, int>((Func<Bugu00510Menu.RecipeData, int>) (x => x.combinedGear.rarity.index)).ThenBy<Bugu00510Menu.RecipeData, GearKindEnum>((Func<Bugu00510Menu.RecipeData, GearKindEnum>) (x => x.combinedGear.kind.Enum)).ThenByDescending<Bugu00510Menu.RecipeData, int>((Func<Bugu00510Menu.RecipeData, int>) (x => x.combinedGear.resource_reference_gear_id_GearGear)).ToList<Bugu00510Menu.RecipeData>();
        break;
      case Bugu00510Menu.SortType.Name:
        this.recipeDataList = this.recipeDataList.OrderBy<Bugu00510Menu.RecipeData, string>((Func<Bugu00510Menu.RecipeData, string>) (x => x.combinedGear.name)).ThenBy<Bugu00510Menu.RecipeData, GearKindEnum>((Func<Bugu00510Menu.RecipeData, GearKindEnum>) (x => x.combinedGear.kind.Enum)).ThenByDescending<Bugu00510Menu.RecipeData, int>((Func<Bugu00510Menu.RecipeData, int>) (x => x.combinedGear.resource_reference_gear_id_GearGear)).ThenByDescending<Bugu00510Menu.RecipeData, int>((Func<Bugu00510Menu.RecipeData, int>) (x => x.combinedGear.rarity.index)).ToList<Bugu00510Menu.RecipeData>();
        break;
    }
  }

  private void SetScrollItem()
  {
    this.scroll.Reset();
    for (int index = 0; index < Mathf.Min(ItemIcon.MaxValue, this.recipeDataList.Count); ++index)
    {
      ItemIcon itemIcon = this.allItemIcon[index];
      this.scroll.Add(itemIcon.gameObject, ItemIcon.Width, ItemIcon.Height, false);
      itemIcon.gameObject.SetActive(true);
    }
    this.scroll.CreateScrollPoint(ItemIcon.Height, this.recipeDataList.Count);
    this.scroll.ResolvePosition();
  }

  private IEnumerator CreateAllIcon()
  {
    Bugu00510Menu bugu00510Menu = this;
    for (int i = 0; i < bugu00510Menu.allItemIcon.Count; ++i)
    {
      IEnumerator e = bugu00510Menu.allItemIcon[i].InitByGear(bugu00510Menu.recipeDataList[i].combinedGear, bugu00510Menu.recipeDataList[i].combinedGear.GetElement(), false, false, false);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      bugu00510Menu.recipeDataList[i].icon = bugu00510Menu.allItemIcon[i];
      bugu00510Menu.allItemIcon[i].Gray = !bugu00510Menu.recipeDataList[i].isCombinEnable;
      bugu00510Menu.allItemIcon[i].gameObject.SetActive(true);
      bugu00510Menu.allItemIcon[i].onClick = new System.Action<ItemIcon>(bugu00510Menu.IconClicked);
      bugu00510Menu.allItemIcon[i].EnableLongPressEvent(true, new System.Action<ItemIcon>(bugu00510Menu.GearDetail));
    }
  }

  private void IconClicked(ItemIcon itemIcon)
  {
    if (this.IsPush)
      return;
    this.StartCoroutine(this.OpenRecipePopup(this.recipeDataList.FirstOrDefault<Bugu00510Menu.RecipeData>((Func<Bugu00510Menu.RecipeData, bool>) (x => (UnityEngine.Object) x.icon == (UnityEngine.Object) itemIcon))));
  }

  private void GearDetail(ItemIcon itemIcon)
  {
    Bugu00510Menu.RecipeData recipeData = this.recipeDataList.FirstOrDefault<Bugu00510Menu.RecipeData>((Func<Bugu00510Menu.RecipeData, bool>) (x => (UnityEngine.Object) x.icon == (UnityEngine.Object) itemIcon));
    string sceneName;
    if (recipeData.combinedGear.kind.Enum != GearKindEnum.smith)
    {
      Singleton<CommonRoot>.GetInstance().ShowLoadingLayer(1, true);
      sceneName = "guide011_4_2";
    }
    else if (recipeData.combinedGear.compose_kind.kind.Enum != GearKindEnum.smith)
    {
      Singleton<CommonRoot>.GetInstance().ShowLoadingLayer(1, true);
      sceneName = "guide011_4_2b";
    }
    else
      sceneName = "guide011_4_2c";
    this.currentRecipe = recipeData;
    Singleton<NGSceneManager>.GetInstance().changeScene(sceneName, true, (object) recipeData.combinedGear, (object) false, (object) 0);
  }

  private IEnumerator LoadObject()
  {
    if (this.recipeDataList.Count > ItemIcon.MaxValue)
    {
      for (int i = ItemIcon.MaxValue; i < this.recipeDataList.Count; ++i)
      {
        IEnumerator e = ItemIcon.LoadSprite(this.recipeDataList[i].combinedGear);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
      }
    }
  }

  private void ScrollIconUpdate(int recipeIndex, int iconCount)
  {
    if (this.recipeDataList[recipeIndex] != null && (UnityEngine.Object) this.recipeDataList[recipeIndex].icon == (UnityEngine.Object) this.allItemIcon[iconCount])
      return;
    this.recipeDataList.Where<Bugu00510Menu.RecipeData>((Func<Bugu00510Menu.RecipeData, bool>) (a => (UnityEngine.Object) a.icon == (UnityEngine.Object) this.allItemIcon[iconCount])).ForEach<Bugu00510Menu.RecipeData>((System.Action<Bugu00510Menu.RecipeData>) (b => b.icon = (ItemIcon) null));
    this.recipeDataList[recipeIndex].icon = this.allItemIcon[iconCount];
    this.StartCoroutine(this.allItemIcon[iconCount].InitByGear(this.recipeDataList[recipeIndex].combinedGear, this.recipeDataList[recipeIndex].combinedGear.GetElement(), false, false, false));
    this.allItemIcon[iconCount].Gray = !this.recipeDataList[recipeIndex].isCombinEnable;
  }

  private IEnumerator OpenRecipePopup(Bugu00510Menu.RecipeData recipe)
  {
    Bugu00510Menu bugu00510Menu = this;
    Singleton<CommonRoot>.GetInstance().ShowLoadingLayer(1, true);
    bugu00510Menu.dirRecipe.gameObject.SetActive(true);
    bugu00510Menu.dirRecipe.gameObject.GetComponent<TweenAlpha>().enabled = false;
    bugu00510Menu.dirRecipe.gameObject.GetComponent<UIWidget>().alpha = 0.0f;
    IEnumerator e = bugu00510Menu.dirRecipe.Init(new System.Action<GearGear, List<InventoryItem>, List<GearCombineRecipe>, int>(bugu00510Menu.PushOK), bugu00510Menu.itemIconPrefab, (IEnumerable<GearCombineRecipe>) recipe.recipes, bugu00510Menu.playerGearDic, bugu00510Menu.playerGearDicDescending);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    bugu00510Menu.dirRecipe.gameObject.GetComponent<NGTweenParts>().isActive = true;
    bugu00510Menu.currentRecipe = recipe;
    yield return (object) new WaitForSeconds(0.2f);
    Singleton<CommonRoot>.GetInstance().HideLoadingLayer();
  }

  public void IbtnBack()
  {
    if (this.IsPushAndSet())
      return;
    this.backScene();
  }

  public void IbtnSort()
  {
    if (this.IsPush)
      return;
    Singleton<CommonRoot>.GetInstance().ShowLoadingLayer(1, true);
    this.StartCoroutine("SortIcon");
  }

  private IEnumerator SortIcon()
  {
    this.sortIndex = (this.sortIndex + 1) % this.sortTypes.Length;
    this.allItemIcon.ForEach((System.Action<ItemIcon>) (x => x.gameObject.SetActive(false)));
    this.recipeDataList.ForEach((System.Action<Bugu00510Menu.RecipeData>) (x => x.icon = (ItemIcon) null));
    this.Sort();
    IEnumerator e = this.CreateAllIcon();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.SetScrollItem();
    Singleton<CommonRoot>.GetInstance().HideLoadingLayer();
  }

  private void SetNewInventory()
  {
    this.inventoryItems.Clear();
    this.CreateInventoryItem(this.GetItemList(), this.GetMaterialList());
  }

  private List<PlayerItem> GetItemList()
  {
    return ((IEnumerable<PlayerItem>) SMManager.Get<PlayerItem[]>()).Where<PlayerItem>((Func<PlayerItem, bool>) (x => !x.broken)).ToList<PlayerItem>();
  }

  private List<PlayerMaterialGear> GetMaterialList()
  {
    return ((IEnumerable<PlayerMaterialGear>) SMManager.Get<PlayerMaterialGear[]>()).Where<PlayerMaterialGear>((Func<PlayerMaterialGear, bool>) (x =>
    {
      if (x.isDilling() || x.isSpecialDilling())
        return false;
      return x.isCompse() || x.isWeaponMaterial();
    })).ToList<PlayerMaterialGear>();
  }

  private void CreateInventoryItem(
    List<PlayerItem> itemList,
    List<PlayerMaterialGear> materialItemList)
  {
    if (itemList != null)
    {
      foreach (PlayerItem playerItem in itemList)
        this.inventoryItems.Add(new InventoryItem(playerItem));
    }
    if (materialItemList == null)
      return;
    foreach (PlayerMaterialGear materialItem in materialItemList)
    {
      PlayerMaterialGear item = materialItem;
      this.inventoryItems.Add(new InventoryItem(item, this.inventoryItems.Count<InventoryItem>((Func<InventoryItem, bool>) (x => x.Item.itemID == item.id))));
    }
  }

  private void PushOK(
    GearGear gear,
    List<InventoryItem> items,
    List<GearCombineRecipe> gearRecipes,
    int zenyAmount)
  {
    this.StartCoroutine(this.StartComposite(gear, items.Select<InventoryItem, GameCore.ItemInfo>((Func<InventoryItem, GameCore.ItemInfo>) (x => x.Item)).ToList<GameCore.ItemInfo>(), gearRecipes, zenyAmount));
  }

  private IEnumerator StartComposite(
    GearGear gear,
    List<GameCore.ItemInfo> sendGears,
    List<GearCombineRecipe> gearRecipes,
    int zenyAmount)
  {
    Bugu00510Menu m = this;
    if (sendGears.Where<GameCore.ItemInfo>((Func<GameCore.ItemInfo, bool>) (x => x.favorite)).ToList<GameCore.ItemInfo>().Count == 0)
    {
      Singleton<CommonRoot>.GetInstance().ShowLoadingLayer(1, true);
      Future<WebAPI.Response.ItemGearCombineRecipeConfirm> futureF = WebAPI.ItemGearCombineRecipeConfirm(0, gearRecipes[0].ID, (System.Action<WebAPI.Response.UserError>) (e =>
      {
        WebAPI.DefaultUserErrorCallback(e);
        Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
      }));
      IEnumerator e1 = futureF.Wait();
      while (e1.MoveNext())
        yield return e1.Current;
      e1 = (IEnumerator) null;
      WebAPI.Response.ItemGearCombineRecipeConfirm result = futureF.Result;
      if (result == null)
      {
        Singleton<CommonRoot>.GetInstance().HideLoadingLayer();
      }
      else
      {
        int maxCreateNum = result.max_create_num;
        GameObject gameObject = Singleton<PopupManager>.GetInstance().open(m.kakuninnPopupPrefab, false, false, false, true, false, false, "SE_1006");
        gameObject.transform.localPosition = new Vector3(0.0f, -60f, 0.0f);
        e1 = gameObject.GetComponent<Bugu005RecipeCompositeMenu>().Init(m, gear, sendGears, gearRecipes, zenyAmount, maxCreateNum, m.inventoryItems);
        while (e1.MoveNext())
          yield return e1.Current;
        e1 = (IEnumerator) null;
        yield return (object) null;
        m.dirRecipe.gameObject.SetActive(false);
        yield return (object) new WaitForSeconds(0.1f);
        Singleton<CommonRoot>.GetInstance().HideLoadingLayer();
        futureF = (Future<WebAPI.Response.ItemGearCombineRecipeConfirm>) null;
      }
    }
    else
    {
      string title = Consts.Format(Consts.GetInstance().POPUP_005_GEAR_WARNING_TITLE, (IDictionary) new Hashtable()
      {
        {
          (object) "type",
          (object) Consts.GetInstance().GEAR_0052_COMPOSITE
        }
      });
      string message = Consts.Format(Consts.GetInstance().POPUP_005_FAVORITE_WARNING_MESSAGE, (IDictionary) new Hashtable()
      {
        {
          (object) "type",
          (object) Consts.GetInstance().GEAR_0052_COMPOSITE
        }
      });
      m.StartCoroutine(PopupCommon.Show(title, message, (System.Action) null));
    }
  }

  public IEnumerator CompositeAPI(List<GameCore.ItemInfo> sendGears)
  {
    Singleton<CommonRoot>.GetInstance().ShowLoadingLayer(1, true);
    Future<WebAPI.Response.ItemGearCombine> futureF = WebAPI.ItemGearCombine(sendGears.Where<GameCore.ItemInfo>((Func<GameCore.ItemInfo, bool>) (x => x.isWeapon)).Select<GameCore.ItemInfo, int>((Func<GameCore.ItemInfo, int>) (x => x.itemID)).ToArray<int>(), sendGears.Where<GameCore.ItemInfo>((Func<GameCore.ItemInfo, bool>) (x => x.isCompse || x.isWeaponMaterial)).Select<GameCore.ItemInfo, int>((Func<GameCore.ItemInfo, int>) (x => x.itemID)).ToArray<int>(), (System.Action<WebAPI.Response.UserError>) (e =>
    {
      WebAPI.DefaultUserErrorCallback(e);
      Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
    }));
    IEnumerator e1 = futureF.Wait();
    while (e1.MoveNext())
      yield return e1.Current;
    e1 = (IEnumerator) null;
    WebAPI.Response.ItemGearCombine result = futureF.Result;
    if (result == null)
    {
      Singleton<CommonRoot>.GetInstance().HideLoadingLayer();
    }
    else
    {
      GameCore.ItemInfo revTargetData = !(result.player_item != (PlayerItem) null) ? new GameCore.ItemInfo(result.player_material_gear, 0) : new GameCore.ItemInfo(result.player_item);
      Singleton<CommonRoot>.GetInstance().HideLoadingLayer();
      if (!revTargetData.gear.kind.isEquip)
        Bugu00561Scene.changeScene(true, revTargetData, revTargetData.isNew, true);
      else
        Gacha00611Scene.changeScene(true, revTargetData.isNew, 0, revTargetData);
      ItemIcon.IsPoolCache = true;
      this.isIntegrated = true;
    }
  }

  public IEnumerator CompositeMultipleAPI(int recipeID, int counter)
  {
    Singleton<CommonRoot>.GetInstance().ShowLoadingLayer(1, true);
    Future<WebAPI.Response.ItemGearCombineRecipe> futureF = WebAPI.ItemGearCombineRecipe(counter, recipeID, (System.Action<WebAPI.Response.UserError>) (e =>
    {
      WebAPI.DefaultUserErrorCallback(e);
      Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
    }));
    IEnumerator e1 = futureF.Wait();
    while (e1.MoveNext())
      yield return e1.Current;
    e1 = (IEnumerator) null;
    WebAPI.Response.ItemGearCombineRecipe result = futureF.Result;
    if (result == null)
    {
      Singleton<CommonRoot>.GetInstance().HideLoadingLayer();
    }
    else
    {
      GameCore.ItemInfo revTargetData = result.player_items.Length == 0 ? new GameCore.ItemInfo(result.player_material_gear, 0) : new GameCore.ItemInfo(((IEnumerable<PlayerItem>) result.player_items).First<PlayerItem>());
      Singleton<CommonRoot>.GetInstance().HideLoadingLayer();
      if (!revTargetData.gear.kind.isEquip)
        Bugu00561Scene.changeScene(true, revTargetData, revTargetData.isNew, true, false, counter);
      else
        Gacha00611Scene.changeScene(true, revTargetData.isNew, counter, revTargetData);
      ItemIcon.IsPoolCache = true;
      this.isIntegrated = true;
    }
  }

  protected override void Update()
  {
    base.Update();
    if (!this.isInitialize || this.recipeDataList.Count <= ItemIcon.ScreenValue)
      return;
    int num1 = ItemIcon.Height * 2;
    float num2 = this.scroll.scrollView.transform.localPosition.y - this.scroolStartY;
    float num3 = (float) (Mathf.Max(0, this.recipeDataList.Count - ItemIcon.ScreenValue - 1) / ItemIcon.ColumnValue * ItemIcon.Height);
    float num4 = (float) (ItemIcon.Height * ItemIcon.RowValue);
    if ((double) num2 < 0.0)
      num2 = 0.0f;
    if ((double) num2 > (double) num3)
      num2 = num3;
    bool flag;
    do
    {
      flag = false;
      int iconCount = 0;
      foreach (GameObject gameObject in this.scroll)
      {
        GameObject item = gameObject;
        float num5 = item.transform.localPosition.y + num2;
        int? nullable = this.recipeDataList.FirstIndexOrNull<Bugu00510Menu.RecipeData>((Func<Bugu00510Menu.RecipeData, bool>) (v => (UnityEngine.Object) v.icon != (UnityEngine.Object) null && (UnityEngine.Object) v.icon.gameObject == (UnityEngine.Object) item));
        if ((double) num5 > (double) num1)
        {
          item.transform.localPosition = new Vector3(item.transform.localPosition.x, item.transform.localPosition.y - num4, 0.0f);
          if (nullable.HasValue && nullable.Value + ItemIcon.MaxValue < (this.recipeDataList.Count + 4) / 5 * 5)
          {
            if (nullable.Value + ItemIcon.MaxValue >= this.recipeDataList.Count)
              item.SetActive(false);
            else
              this.ScrollIconUpdate(nullable.Value + ItemIcon.MaxValue, iconCount);
            flag = true;
          }
        }
        else if ((double) num5 < -((double) num4 - (double) num1))
        {
          int num6 = ItemIcon.MaxValue;
          if (!item.activeSelf)
          {
            item.SetActive(true);
            num6 = 0;
          }
          if (nullable.HasValue && nullable.Value - num6 >= 0)
          {
            item.transform.localPosition = new Vector3(item.transform.localPosition.x, item.transform.localPosition.y + num4, 0.0f);
            this.ScrollIconUpdate(nullable.Value - num6, iconCount);
            flag = true;
          }
        }
        else if (nullable.HasValue)
          this.ScrollIconUpdate(nullable.Value, iconCount);
        ++iconCount;
      }
    }
    while (flag);
  }

  private void OnDestroy()
  {
  }

  public override void onBackButton()
  {
    if (this.dirRecipe.gameObject.activeSelf)
      return;
    if (Singleton<PopupManager>.GetInstance().isOpen)
      Singleton<PopupManager>.GetInstance().onDismiss();
    else
      this.backScene();
  }

  protected virtual void OnEnable()
  {
    if (!this.ScrollView.isDragging)
      return;
    this.ScrollView.Press(false);
  }

  private enum SortType
  {
    Recommend,
    Category,
    Rarity,
    Name,
  }

  private class RecipeData
  {
    public GearGear combinedGear;
    public List<GearCombineRecipe> recipes;
    public bool isCombinEnable;
    public ItemIcon icon;

    public RecipeData(IEnumerable<GearCombineRecipe> recipeDatas, bool isEnable)
    {
      this.combinedGear = MasterData.GearGear[recipeDatas.First<GearCombineRecipe>().combined_gear_id];
      this.recipes = recipeDatas.OrderBy<GearCombineRecipe, int>((Func<GearCombineRecipe, int>) (x => x.priority)).ThenBy<GearCombineRecipe, int>((Func<GearCombineRecipe, int>) (x => x.combined_gear_id)).ToList<GearCombineRecipe>();
      this.isCombinEnable = isEnable;
      this.icon = (ItemIcon) null;
    }

    public int priority
    {
      get
      {
        return this.recipes.Count <= 0 ? int.MaxValue : this.recipes.Min<GearCombineRecipe>((Func<GearCombineRecipe, int>) (x => x.priority));
      }
    }

    public List<string> GetResouceNames()
    {
      List<string> stringList = this.combinedGear.ResourcePaths();
      foreach (GearCombineRecipe recipe in this.recipes)
        stringList.AddRange((IEnumerable<string>) recipe.ResourcePaths());
      return stringList;
    }
  }
}
