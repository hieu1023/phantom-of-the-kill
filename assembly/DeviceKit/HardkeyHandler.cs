﻿// Decompiled with JetBrains decompiler
// Type: DeviceKit.HardkeyHandler
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace DeviceKit
{
  internal class HardkeyHandler : MonoBehaviour
  {
    private static IHardkeyListener _listener;

    private static void devicekit_setHardkeyListener(string gameObjectName)
    {
    }

    public static void Init(GameObject serviceNode = null)
    {
      if ((Object) serviceNode == (Object) null)
      {
        serviceNode = new GameObject(nameof (HardkeyHandler));
        serviceNode.hideFlags |= HideFlags.HideInHierarchy;
        Object.DontDestroyOnLoad((Object) serviceNode);
      }
      if (!((Object) serviceNode.GetComponent<HardkeyHandler>() == (Object) null))
        return;
      HardkeyHandler.devicekit_setHardkeyListener(serviceNode.AddComponent<HardkeyHandler>().gameObject.name);
    }

    public static void SetListener(IHardkeyListener listener)
    {
      HardkeyHandler._listener = listener;
    }

    private void Hardkey_OnBackKey(string msg)
    {
      if (HardkeyHandler._listener == null)
        return;
      HardkeyHandler._listener.OnBackKey();
    }
  }
}
