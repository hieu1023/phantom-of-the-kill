﻿// Decompiled with JetBrains decompiler
// Type: Friend0083Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System;
using System.Collections;
using System.Collections.Generic;

public class Friend0083Menu : BackButtonMenuBase
{
  private List<string> target_friend_ids = new List<string>();
  private System.Action callback;

  public void AddTargetFriendId(string id)
  {
    this.target_friend_ids.Add(id);
  }

  private IEnumerator FriendRemoveAsync()
  {
    Friend0083Menu friend0083Menu = this;
    Singleton<PopupManager>.GetInstance().onDismiss();
    CommonRoot common = Singleton<CommonRoot>.GetInstance();
    common.isTouchBlock = true;
    common.loadingMode = 1;
    if (friend0083Menu.target_friend_ids.Count > 0)
    {
      // ISSUE: reference to a compiler-generated method
      Future<WebAPI.Response.FriendRemove> future = WebAPI.FriendRemove(friend0083Menu.target_friend_ids.ToArray(), new System.Action<WebAPI.Response.UserError>(friend0083Menu.\u003CFriendRemoveAsync\u003Eb__3_0));
      IEnumerator e = future.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      if (future.Result == null)
        yield break;
      else
        future = (Future<WebAPI.Response.FriendRemove>) null;
    }
    common.loadingMode = 0;
    common.isTouchBlock = false;
    Singleton<PopupManager>.GetInstance().onDismiss();
    if (friend0083Menu.callback != null)
      friend0083Menu.callback();
  }

  private void RemoveError(string err)
  {
    Debug.LogWarning((object) ("FriendRemove API ERROR CODE : " + err));
    if (err == "FRD012")
      Singleton<NGGameDataManager>.GetInstance().refreshHomeHome = true;
    Singleton<CommonRoot>.GetInstance().isTouchBlock = false;
    Singleton<CommonRoot>.GetInstance().loadingMode = 0;
    Singleton<NGSceneManager>.GetInstance().destroyLoadedScenes();
    Singleton<NGSceneManager>.GetInstance().clearStack();
    Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
  }

  public void IbtnYes()
  {
    if (this.IsPushAndSet())
      return;
    this.StartCoroutine(this.FriendRemoveAsync());
  }

  public void IbtnNo()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<PopupManager>.GetInstance().onDismiss();
  }

  public override void onBackButton()
  {
    this.IbtnNo();
  }

  public void SetCallback(System.Action callback)
  {
    this.callback = callback;
  }
}
