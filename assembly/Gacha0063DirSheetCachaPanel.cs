﻿// Decompiled with JetBrains decompiler
// Type: Gacha0063DirSheetCachaPanel
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gacha0063DirSheetCachaPanel : MonoBehaviour
{
  [SerializeField]
  private GameObject iBtn_Panel;
  [SerializeField]
  private GameObject dyn_Detail;
  [SerializeField]
  private GameObject dyn_RewardThum;
  [SerializeField]
  private GameObject dir_Amount;
  [SerializeField]
  private GameObject dyn_Point;
  [SerializeField]
  private GameObject SheetPanelsBlacks;
  [SerializeField]
  private GameObject amountDigitX;
  [SerializeField]
  private GameObject[] amountDigit;
  [SerializeField]
  private GameObject[] pointDigit;
  private GameObject effectObject;
  private GameObject hitEffectObject;
  private Popup0063SheetMenu parentObject;
  private MasterDataTable.CommonRewardType rewardType;
  private int rewardID;
  public bool isOpen;
  public bool IsResultEffect;
  private const float ANIM_START_TIME = 0.25f;
  private const float ANIM_DURUTION = 0.5f;
  private const float ANIM_ADD_TIME1 = 0.5f;
  private const float ANIM_ADD_TIME2 = 0.25f;

  private void SentNum(GameObject parent, GameObject[] go, int num)
  {
    int num1 = num;
    ((IEnumerable<GameObject>) go).ForEach<GameObject>((System.Action<GameObject>) (x => x.SetActive(false)));
    if (num1 == -1)
    {
      parent.SetActive(false);
    }
    else
    {
      parent.SetActive(true);
      int length = num.ToString().Length;
      if (length > go.Length)
        return;
      for (int index = 0; index < length; ++index)
      {
        UnityEngine.Sprite sprite = Resources.Load<UnityEngine.Sprite>("Icons/slc_Number" + (num1 % 10).ToString());
        go[index].GetComponent<UI2DSprite>().sprite2D = sprite;
        go[index].GetComponent<UI2DSprite>().SetDimensions((int) sprite.textureRect.width, (int) sprite.textureRect.height);
        go[index].SetActive(true);
        num1 /= 10;
      }
    }
  }

  private void SetAmountDigit(int num)
  {
    this.SentNum(this.dir_Amount, this.amountDigit, num);
    if (num < 10)
      this.amountDigitX.transform.position = new Vector3(this.amountDigit[1].transform.position.x, this.amountDigitX.transform.position.y, this.amountDigitX.transform.position.z);
    else if (num < 100)
    {
      this.amountDigitX.transform.position = new Vector3(this.amountDigit[2].transform.position.x, this.amountDigitX.transform.position.y, this.amountDigitX.transform.position.z);
    }
    else
    {
      if (num >= 1000)
        return;
      this.amountDigitX.transform.position = new Vector3(this.amountDigit[3].transform.position.x, this.amountDigitX.transform.position.y, this.amountDigitX.transform.position.z);
    }
  }

  private void SetPointDigit(int num)
  {
    this.SentNum(this.dyn_Point, this.pointDigit, num);
  }

  public IEnumerator Init(
    Popup0063SheetMenu parent,
    GachaG007PlayerPanel panel,
    bool isResultEffect = false,
    GameObject effectPrefab = null,
    GameObject hitEffectPrefab = null)
  {
    Gacha0063DirSheetCachaPanel dirSheetCachaPanel = this;
    dirSheetCachaPanel.parentObject = parent;
    dirSheetCachaPanel.dir_Amount.SetActive(false);
    dirSheetCachaPanel.dyn_Point.SetActive(false);
    dirSheetCachaPanel.isOpen = panel.is_opened;
    dirSheetCachaPanel.IsResultEffect = isResultEffect;
    dirSheetCachaPanel.dyn_Detail.SetActive(false);
    dirSheetCachaPanel.iBtn_Panel.GetComponent<UIButton>().enabled = false;
    CreateIconObject target = dirSheetCachaPanel.dyn_RewardThum.GetOrAddComponent<CreateIconObject>();
    if (panel.reward_type_id.HasValue)
    {
      dirSheetCachaPanel.rewardType = (MasterDataTable.CommonRewardType) panel.reward_type_id.Value;
      dirSheetCachaPanel.rewardID = panel.reward_id.HasValue ? panel.reward_id.Value : 0;
      IEnumerator e = target.CreateThumbnail((MasterDataTable.CommonRewardType) panel.reward_type_id.Value, !panel.reward_id.HasValue ? 0 : panel.reward_id.Value, 0, true, true, new CommonQuestType?(), false);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      if (panel.reward_type_id.Value == 1 || panel.reward_type_id.Value == 24)
      {
        target.GetIcon().GetComponent<UnitIcon>().setLevelText("1");
        target.GetIcon().GetComponent<UnitIcon>().ShowBottomInfo(UnitSortAndFilter.SORT_TYPES.Level);
        dirSheetCachaPanel.SetAmountDigit(panel.reward_quantity.Value);
        dirSheetCachaPanel.SetPointDigit(-1);
      }
      else if (panel.reward_type_id.Value == 3 || panel.reward_type_id.Value == 26 || (panel.reward_type_id.Value == 35 || panel.reward_type_id.Value == 2) || (panel.reward_type_id.Value == 5 || panel.reward_type_id.Value == 10 || (panel.reward_type_id.Value == 14 || panel.reward_type_id.Value == 17)) || (panel.reward_type_id.Value == 19 || panel.reward_type_id.Value == 20 || panel.reward_type_id.Value == 29))
      {
        dirSheetCachaPanel.SetAmountDigit(panel.reward_quantity.Value);
        dirSheetCachaPanel.SetPointDigit(-1);
      }
      else if (panel.reward_type_id.Value == 4 || panel.reward_type_id.Value == 6 || (panel.reward_type_id.Value == 7 || panel.reward_type_id.Value == 8) || (panel.reward_type_id.Value == 9 || panel.reward_type_id.Value == 12 || (panel.reward_type_id.Value == 13 || panel.reward_type_id.Value == 15)))
      {
        dirSheetCachaPanel.SetAmountDigit(-1);
        dirSheetCachaPanel.SetPointDigit(panel.reward_quantity.Value);
      }
      else
      {
        dirSheetCachaPanel.SetAmountDigit(-1);
        dirSheetCachaPanel.SetPointDigit(-1);
      }
    }
    if (dirSheetCachaPanel.IsResultEffect && !dirSheetCachaPanel.isOpen)
    {
      dirSheetCachaPanel.effectObject = effectPrefab.Clone(dirSheetCachaPanel.transform);
      dirSheetCachaPanel.hitEffectObject = hitEffectPrefab.Clone(dirSheetCachaPanel.transform);
      dirSheetCachaPanel.effectObject.SetActive(false);
      dirSheetCachaPanel.hitEffectObject.SetActive(false);
    }
    dirSheetCachaPanel.SheetPanelsBlacks.SetActive(dirSheetCachaPanel.isOpen);
  }

  public void SelEffect(float correct)
  {
    this.effectObject.GetComponent<ParticleSystem>().playbackSpeed = 1f;
    this.effectObject.SetActive(false);
    this.effectObject.SetActive(true);
  }

  public void HitEffect()
  {
    this.effectObject.SetActive(false);
    this.hitEffectObject.SetActive(true);
  }

  public void IbtnDetail()
  {
    this.StartCoroutine(this.ShowDetailPopup());
  }

  private IEnumerator ShowDetailPopup()
  {
    IEnumerator e = this.parentObject.ShowDetaiPopup(this.rewardType, this.rewardID);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }
}
