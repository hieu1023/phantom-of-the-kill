﻿// Decompiled with JetBrains decompiler
// Type: Schedule
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using UnityEngine;

public class Schedule
{
  public BL.Phase state = BL.Phase.unset;
  public bool isSetBattleEnable;
  public bool isBattleEnable;
  public float startTime;
  public System.Action endAction;
  public bool isInsertMode;

  public virtual bool body()
  {
    return true;
  }

  public virtual bool completedp()
  {
    return true;
  }

  public float time
  {
    get
    {
      return Time.time;
    }
  }

  public float deltaTime
  {
    get
    {
      return Time.time - this.startTime;
    }
  }

  public bool execBody()
  {
    BattleTimeManager manager = Singleton<NGBattleManager>.GetInstance().getManager<BattleTimeManager>();
    bool insertMode = manager.insertMode;
    manager.insertMode = this.isInsertMode;
    int num = this.body() ? 1 : 0;
    manager.insertMode = insertMode;
    return num != 0;
  }
}
