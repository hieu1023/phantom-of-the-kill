﻿// Decompiled with JetBrains decompiler
// Type: MaterialDetailMenu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class MaterialDetailMenu : DetailMenuBase
{
  [SerializeField]
  protected UILabel TxtDetaildescription;
  [SerializeField]
  protected UILabel TxtOwnednumber;
  [SerializeField]
  protected UILabel TxtDropQuestName;
  [SerializeField]
  protected UISprite IconEvolution;
  [SerializeField]
  protected UISprite IconUnification;
  [SerializeField]
  protected UISprite IconRevival;
  [SerializeField]
  private UI2DSprite mainSprite;
  [SerializeField]
  private Transform transBottom;

  public override IEnumerator Init(
    Unit0042Menu menu,
    int index,
    PlayerUnit playerUnit,
    int infoIndex,
    bool isLimit,
    bool isMaterial,
    QuestScoreBonusTimetable[] tables,
    UnitBonus[] unitBonus,
    bool isUpdate = true)
  {
    MaterialDetailMenu materialDetailMenu = this;
    materialDetailMenu.menu = menu;
    materialDetailMenu.index = index;
    if ((UnityEngine.Object) materialDetailMenu.transBottom == (UnityEngine.Object) null)
      materialDetailMenu.transBottom = materialDetailMenu.transform.GetChildInFind("Bottom");
    materialDetailMenu.transBottom.localPosition = new Vector3(0.0f, (float) -(((double) menu.scrollView.scrollView.panel.GetViewSize().y - (double) materialDetailMenu.transBottom.GetComponent<UIWidget>().height) / 2.0), 0.0f);
    materialDetailMenu.TxtDetaildescription.text = playerUnit.unit.description;
    UnitMaterialQuestInfo materialQuestInfo = ((IEnumerable<UnitMaterialQuestInfo>) MasterData.UnitMaterialQuestInfoList).SingleOrDefault<UnitMaterialQuestInfo>((Func<UnitMaterialQuestInfo, bool>) (x => x.unit_id == playerUnit.unit.ID));
    if (materialQuestInfo == null)
      materialDetailMenu.TxtDropQuestName.text = "";
    else if ((UnityEngine.Object) materialDetailMenu.TxtDropQuestName != (UnityEngine.Object) null)
      materialDetailMenu.TxtDropQuestName.text = materialQuestInfo.short_desc;
    PlayerMaterialUnit playerMaterialUnit = ((IEnumerable<PlayerMaterialUnit>) SMManager.Get<PlayerMaterialUnit[]>()).FirstOrDefault<PlayerMaterialUnit>((Func<PlayerMaterialUnit, bool>) (x => x._unit == playerUnit.unit.ID));
    materialDetailMenu.TxtOwnednumber.SetTextLocalize(playerMaterialUnit != null ? playerMaterialUnit.quantity : 0);
    Future<UnityEngine.Sprite> targetSprite = playerUnit.unit.LoadSpriteMedium(1f);
    IEnumerator e = targetSprite.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    materialDetailMenu.mainSprite.sprite2D = targetSprite.Result;
    materialDetailMenu.IconEvolution.gameObject.SetActive(false);
    materialDetailMenu.IconUnification.gameObject.SetActive(false);
    materialDetailMenu.IconRevival.gameObject.SetActive(false);
    if (playerUnit.unit.IsTougouUnit)
      materialDetailMenu.IconUnification.gameObject.SetActive(true);
    else if (playerUnit.unit.IsSinkaUnit)
      materialDetailMenu.IconEvolution.gameObject.SetActive(true);
    else if (playerUnit.unit.IsTenseiUnit)
      materialDetailMenu.IconRevival.gameObject.SetActive(true);
  }

  public void changeBackScene()
  {
    this.backScene();
  }
}
