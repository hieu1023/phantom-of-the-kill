﻿// Decompiled with JetBrains decompiler
// Type: InventoryExtraSkill
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using SM;

public class InventoryExtraSkill
{
  public PlayerAwakeSkill skill;
  public int level;
  public int uniqueID;
  public ExtraSkillIcon icon;
  public bool select;
  public int index;
  public bool Gray;
  public bool removeButton;
  public bool forBattle;
  public bool favorite;
  public PlayerUnit equipUnit;

  public InventoryExtraSkill()
  {
    this.removeButton = true;
  }

  public InventoryExtraSkill(PlayerAwakeSkill skill)
  {
    this.Init(skill);
  }

  public void Init(PlayerAwakeSkill awakeSkill)
  {
    this.skill = awakeSkill;
    this.favorite = this.skill.favorite;
    this.level = this.skill.level;
    this.uniqueID = this.skill.id;
    this.equipUnit = this.skill.EqupmentUnit;
    this.forBattle = this.equipUnit != (PlayerUnit) null;
  }

  public string GetName()
  {
    return this.skill.masterData.name;
  }

  public string GetDescription()
  {
    return this.skill.masterData.description;
  }

  public int GetMaxLevel()
  {
    return this.skill.masterData.upper_level;
  }

  public BattleskillGenre? GetGenre1()
  {
    return this.skill.masterData.genre1;
  }

  public BattleskillGenre? GetGenre2()
  {
    return this.skill.masterData.genre2;
  }
}
