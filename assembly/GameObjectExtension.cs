﻿// Decompiled with JetBrains decompiler
// Type: GameObjectExtension
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections.Generic;
using UnityEngine;

public static class GameObjectExtension
{
  public static T CloneAndGetComponent<T>(this GameObject self, GameObject parent = null) where T : Component
  {
    return (UnityEngine.Object) self == (UnityEngine.Object) null ? default (T) : self.CloneAndGetComponent<T>((bool) (UnityEngine.Object) parent ? parent.transform : (Transform) null);
  }

  public static T CloneAndGetComponent<T>(this GameObject self, Transform parent) where T : Component
  {
    return (UnityEngine.Object) self == (UnityEngine.Object) null ? default (T) : self.Clone(parent).GetComponent<T>();
  }

  public static T CloneAndAddComponent<T>(this GameObject self, GameObject parent = null) where T : Component
  {
    return (UnityEngine.Object) self == (UnityEngine.Object) null ? default (T) : self.CloneAndAddComponent<T>((bool) (UnityEngine.Object) parent ? parent.transform : (Transform) null);
  }

  public static T CloneAndAddComponent<T>(this GameObject self, Transform parent) where T : Component
  {
    return (UnityEngine.Object) self == (UnityEngine.Object) null ? default (T) : self.Clone(parent).AddComponent<T>();
  }

  public static GameObject SetParentSafeLocalTransform(
    this GameObject self,
    GameObject parent)
  {
    if ((UnityEngine.Object) self == (UnityEngine.Object) null)
      return (GameObject) null;
    Vector3 localScale = self.transform.localScale;
    Vector3 localPosition = self.transform.localPosition;
    Quaternion localRotation = self.transform.localRotation;
    self.layer = parent.layer;
    self.transform.parent = parent.transform;
    self.transform.localScale = localScale;
    self.transform.localPosition = localPosition;
    self.transform.localRotation = localRotation;
    return self;
  }

  public static GameObject SetParent(this GameObject self, GameObject parent)
  {
    if ((UnityEngine.Object) self == (UnityEngine.Object) null)
      return (GameObject) null;
    self.layer = parent.layer;
    self.transform.parent = parent.transform;
    self.transform.localScale = Vector3.one;
    self.transform.localPosition = Vector3.zero;
    self.transform.localRotation = Quaternion.identity;
    return self;
  }

  public static GameObject SetParent(
    this GameObject self,
    GameObject parent,
    float ratio)
  {
    if ((UnityEngine.Object) self == (UnityEngine.Object) null)
      return (GameObject) null;
    self.layer = parent.layer;
    self.transform.parent = parent.transform;
    self.transform.localScale = new Vector3(ratio, ratio, 1f);
    self.transform.localPosition = Vector3.zero;
    self.transform.localRotation = Quaternion.identity;
    return self;
  }

  public static GameObject Clone(this GameObject self, Transform parent = null)
  {
    if ((UnityEngine.Object) self == (UnityEngine.Object) null)
      return (GameObject) null;
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(self);
    gameObject.transform.parent = parent ?? gameObject.transform.parent;
    gameObject.transform.localScale = Vector3.one;
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.transform.localRotation = Quaternion.identity;
    return gameObject;
  }

  public static void ToggleOnce(this IEnumerable<GameObject> self, int index)
  {
    self.ForEachIndex<GameObject>((System.Action<GameObject, int>) ((go, n) => go.SetActive(n == index)));
  }

  public static void ToggleOnceEx(this IEnumerable<GameObject> self, int index)
  {
    self.ForEachIndex<GameObject>((System.Action<GameObject, int>) ((go, n) =>
    {
      if (!((UnityEngine.Object) go != (UnityEngine.Object) null))
        return;
      go.SetActive(n == index);
    }));
  }

  public static T GetOrAddComponent<T>(this GameObject self) where T : Component
  {
    if ((UnityEngine.Object) self == (UnityEngine.Object) null)
      return default (T);
    T component = self.GetComponent<T>();
    return (UnityEngine.Object) component != (UnityEngine.Object) null ? component : self.AddComponent<T>();
  }
}
