﻿// Decompiled with JetBrains decompiler
// Type: UISpriteUpdater
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

public class UISpriteUpdater : MonoBehaviour
{
  [SerializeField]
  private UISpriteUpdater.Timing timing_ = UISpriteUpdater.Timing.NextFrame;
  [SerializeField]
  private UISprite[] sprites_;

  public void UpdateSprites()
  {
    if (this.timing_ == UISpriteUpdater.Timing.Immediate)
      this.updateSprites();
    else
      this.StartCoroutine(this.coDelayUpdate());
  }

  private IEnumerator coDelayUpdate()
  {
    if (this.timing_ == UISpriteUpdater.Timing.NextFrame)
      yield return (object) null;
    else
      yield return (object) new WaitForEndOfFrame();
    this.updateSprites();
  }

  private void updateSprites()
  {
    foreach (UIWidget sprite in this.sprites_)
      sprite.MarkAsChanged();
  }

  private enum Timing
  {
    Immediate,
    NextFrame,
    EndOfFrame,
  }
}
