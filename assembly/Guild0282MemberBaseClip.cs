﻿// Decompiled with JetBrains decompiler
// Type: Guild0282MemberBaseClip
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Guild0282MemberBaseClip : MonoBehaviour
{
  private System.Action onCountDownStar_;
  private System.Action<string> onSetNumberColor_;

  public void countDownStar()
  {
    if (this.onCountDownStar_ == null)
      return;
    this.onCountDownStar_();
  }

  public void setEventCountDownStar(System.Action onCountDown = null)
  {
    this.onCountDownStar_ = onCountDown;
  }

  public void setNumberColor(string col)
  {
    if (this.onSetNumberColor_ == null)
      return;
    this.onSetNumberColor_(col);
  }

  public void setEventSetNumberColor(System.Action<string> onSetNumberColor = null)
  {
    this.onSetNumberColor_ = onSetNumberColor;
  }
}
