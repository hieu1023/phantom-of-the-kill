﻿// Decompiled with JetBrains decompiler
// Type: Unit00499SaveMemorySlotSelect
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Unit00499SaveMemorySlotSelect : BackButtonMenuBase
{
  private PlayerUnit playerUnit;
  private List<PlayerUnit> recordUnitList;
  private GameObject listItem;
  [SerializeField]
  private NGxScroll scroll;
  private GameObject SaveMemoryConfirmPrefab;
  private GameObject MemoryDeletConfirmPrefab;
  private GameObject MemoryConfirmPrefab;
  [SerializeField]
  private UILabel txt_Description;
  public System.Action endUpdate;
  public bool isClose;

  public IEnumerator DeleteUpdate()
  {
    this.scroll.Clear();
    IEnumerator e = this.Initialize(this.playerUnit, this.endUpdate);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public IEnumerator Initialize(PlayerUnit playerUnit, System.Action endUpdate)
  {
    Unit00499SaveMemorySlotSelect menu = this;
    menu.endUpdate = endUpdate;
    menu.playerUnit = playerUnit;
    menu.recordUnitList = PlayerTransmigrateMemoryPlayerUnitIds.Current != null ? PlayerTransmigrateMemoryPlayerUnitIds.Current.PlayerUnits() : new List<PlayerUnit>();
    Future<GameObject> prefabF = (Future<GameObject>) null;
    IEnumerator e;
    if ((UnityEngine.Object) menu.listItem == (UnityEngine.Object) null)
    {
      prefabF = Res.Prefabs.unit004_9_9.dir_reinforce_memory_slot_list.Load<GameObject>();
      e = prefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      menu.listItem = prefabF.Result;
    }
    for (int index = 0; index < menu.recordUnitList.Count; ++index)
    {
      PlayerUnit recordUnit = menu.recordUnitList[index];
      GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(menu.listItem);
      Unit00499MemorySlotItem component = gameObject.GetComponent<Unit00499MemorySlotItem>();
      menu.scroll.Add(gameObject, false);
      menu.StartCoroutine(component.Initialize(recordUnit, menu, index));
    }
    menu.scroll.ResolvePosition();
    if (playerUnit == (PlayerUnit) null)
      menu.txt_Description.SetTextLocalize(Consts.GetInstance().MEMORY_SLOT);
    else if (menu.recordUnitList.Any<PlayerUnit>((Func<PlayerUnit, bool>) (x => x == (PlayerUnit) null)))
      menu.txt_Description.SetTextLocalize(Consts.GetInstance().SAVE_MEMORY_SLOT);
    else
      menu.txt_Description.SetTextLocalize(Consts.GetInstance().SAVE_MEMORY_SLOT_LIMIT);
    if ((UnityEngine.Object) menu.SaveMemoryConfirmPrefab == (UnityEngine.Object) null)
    {
      prefabF = Res.Prefabs.popup.popup_004_save_memory_confirm__anim_popup01.Load<GameObject>();
      e = prefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      menu.SaveMemoryConfirmPrefab = prefabF.Result;
    }
    if ((UnityEngine.Object) menu.MemoryDeletConfirmPrefab == (UnityEngine.Object) null)
    {
      prefabF = Res.Prefabs.popup.popup_004_memory_delet_confirm__anim_popup01.Load<GameObject>();
      e = prefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      menu.MemoryDeletConfirmPrefab = prefabF.Result;
    }
    if ((UnityEngine.Object) menu.MemoryConfirmPrefab == (UnityEngine.Object) null)
    {
      prefabF = Res.Prefabs.popup.popup_004_memory_confirm__anim_popup01.Load<GameObject>();
      e = prefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      menu.MemoryConfirmPrefab = prefabF.Result;
    }
    menu.isClose = false;
  }

  public void IbtnBack()
  {
    if (this.endUpdate != null)
      this.endUpdate();
    Singleton<PopupManager>.GetInstance().dismiss(false);
  }

  public override void onBackButton()
  {
    this.IbtnBack();
  }

  public void ShowSaveMemoryConfirm(int index)
  {
    if (this.isClose || this.playerUnit == (PlayerUnit) null || PlayerTransmigrateMemoryPlayerUnitIds.Current != null && PlayerTransmigrateMemoryPlayerUnitIds.Current.PlayerUnits().Any<PlayerUnit>((Func<PlayerUnit, bool>) (x => x != (PlayerUnit) null && x.id == this.playerUnit.id)))
      return;
    Singleton<PopupManager>.GetInstance().open(this.SaveMemoryConfirmPrefab, false, false, false, true, false, false, "SE_1006").GetComponent<Unit00499SaveMemoryConfirm>().Initialize(this.playerUnit, index, this);
  }

  public void ShowMemoryDeletConfirm(PlayerUnit unit, int index)
  {
    if (this.isClose)
      return;
    Singleton<PopupManager>.GetInstance().open(this.MemoryDeletConfirmPrefab, false, false, false, true, false, false, "SE_1006").GetComponent<Unit00499MemoryDeleteConfirm>().Initialize(unit, index, this);
  }

  public void ShowMemoryConfirmPrefab(PlayerUnit unit, int index)
  {
    if (this.isClose)
      return;
    Singleton<PopupManager>.GetInstance().open(this.MemoryConfirmPrefab, false, false, false, true, false, false, "SE_1006").GetComponent<Unit00499MemoryConfirm>().Initialize(index, this);
  }
}
