﻿// Decompiled with JetBrains decompiler
// Type: Guide01142cScene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using System.Collections;

public class Guide01142cScene : NGSceneBase
{
  public Guide01142cMenu menu;

  public IEnumerator onStartSceneAsync(GearGear gear, bool isDispNumber, int index)
  {
    this.menu.SetNumber(gear, isDispNumber);
    IEnumerator e = this.menu.InitDetailedScreen(gear);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public IEnumerator onStartSceneAsync(GearGear[] gears, bool isDispNumber, int index = 0)
  {
    this.menu.SetNumber(gears[0], isDispNumber);
    IEnumerator e = this.menu.InitDetailedScreen(gears[0]);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }
}
