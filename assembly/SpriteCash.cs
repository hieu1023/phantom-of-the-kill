﻿// Decompiled with JetBrains decompiler
// Type: SpriteCash
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System;
using System.Collections;

[Serializable]
public class SpriteCash
{
  public UnityEngine.Sprite sprite;
  public Future<UnityEngine.Sprite> fSprite;
  public int id;
  public bool isLoading;

  public IEnumerator LoadSprite()
  {
    this.sprite = (UnityEngine.Sprite) null;
    this.isLoading = true;
    IEnumerator e = this.fSprite.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.sprite = this.fSprite.Result;
    this.isLoading = false;
  }
}
