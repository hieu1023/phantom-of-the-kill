﻿// Decompiled with JetBrains decompiler
// Type: Gacha0063KisekiExtention
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UniLinq;
using UnityEngine;

public class Gacha0063KisekiExtention : MonoBehaviour
{
  private bool isSheetDetail = true;
  [SerializeField]
  private GameObject singleGachaMode;
  [SerializeField]
  private GameObject multiGachaMode;
  [SerializeField]
  private GameObject ExGachaMode;
  [SerializeField]
  private GameObject singleGachaModeEx;
  [SerializeField]
  private GameObject multiGachaModeEx;
  [SerializeField]
  private GameObject stepUp;
  [SerializeField]
  private GameObject buyKiseki;
  [SerializeField]
  private UIButton detailButton;
  [SerializeField]
  private UISprite BelowNineGacha;
  [SerializeField]
  private UISprite singleDigitGacha;
  [SerializeField]
  private UISprite doubleDigitsGacha;
  [SerializeField]
  private UISprite singleDigitKiseki;
  [SerializeField]
  private UISprite doubleDigitsKiseki;
  [SerializeField]
  private List<UISprite> tripleDigitKiseki;
  [SerializeField]
  private List<UISprite> quadDigitKiseki;
  [SerializeField]
  private GameObject singleGachaSingleDigit;
  [SerializeField]
  private GameObject singleGachaDoubleDigit;
  [SerializeField]
  private GameObject singleGachaTripleDigit;
  [SerializeField]
  private GameObject singleGachaQuadDigit;
  [SerializeField]
  private GameObject gachaDoubleDigit;
  [SerializeField]
  private GameObject gachaTripleDigit;
  [SerializeField]
  private GameObject gachaQuadDigit;
  [SerializeField]
  private UISprite singleGachaDigitKiseki;
  [SerializeField]
  private UISprite singleGachaSingleDigitKiseki;
  [SerializeField]
  private UISprite singleGachaDoubleDigitKiseki;
  [SerializeField]
  private List<UISprite> singleGachaTripleKiseki;
  [SerializeField]
  private List<UISprite> singleGachaQuadKiseki;
  [SerializeField]
  private UISprite stepNowText;
  [SerializeField]
  private UISprite stepMaxText;
  [SerializeField]
  private UILabel dateLimitText;
  [SerializeField]
  private UILabel hourLimitText;
  [SerializeField]
  private GameObject Sheet;
  [SerializeField]
  private GameObject[] panel;
  [SerializeField]
  private GameObject SheetChange;
  [SerializeField]
  private UISprite Slash;
  [SerializeField]
  private UISprite Denominator;
  [SerializeField]
  private UISprite Numerator;
  public Gacha0063Menu Menu;
  private bool m_TimeOut;
  private bool isAboveDay;

  private void Init()
  {
    this.singleGachaMode.SetActive(false);
    this.multiGachaMode.SetActive(false);
    this.Sheet.SetActive(false);
    this.SheetChange.SetActive(false);
    this.ExGachaMode.SetActive(true);
    this.m_TimeOut = false;
  }

  public void UpdateSheetInfo()
  {
    GachaG007PlayerSheet[] gachaG007PlayerSheetArray = SMManager.Get<GachaG007PlayerSheet[]>();
    this.stepUp.SetActive(false);
    this.buyKiseki.SetActive(false);
    this.Sheet.SetActive(true);
    this.SheetChange.SetActive(false);
    if (gachaG007PlayerSheetArray[0].total_count.HasValue)
    {
      int? totalCount = gachaG007PlayerSheetArray[0].total_count;
      int num = 1;
      if (totalCount.GetValueOrDefault() > num & totalCount.HasValue && gachaG007PlayerSheetArray[0].current_count > 0)
      {
        this.Slash.gameObject.SetActive(false);
        this.SheetChange.SetActive(true);
        if (gachaG007PlayerSheetArray[0].total_count.HasValue)
        {
          this.Slash.gameObject.SetActive(true);
          this.ChangeSprite(this.Denominator, string.Format("slc_StepUpSmall_num{0}.png__GUI__006-3_sozai__006-3_sozai_prefab", (object) gachaG007PlayerSheetArray[0].total_count));
        }
        if (gachaG007PlayerSheetArray[0].current_count > 0)
          this.ChangeSprite(this.Numerator, string.Format("slc_StepUp_num{0}.png__GUI__006-3_sozai__006-3_sozai_prefab", (object) gachaG007PlayerSheetArray[0].current_count));
      }
    }
    GachaG007PlayerPanel[] array = ((IEnumerable<GachaG007PlayerPanel>) gachaG007PlayerSheetArray[0].player_panels).OrderBy<GachaG007PlayerPanel, int>((Func<GachaG007PlayerPanel, int>) (x => x.position)).ToArray<GachaG007PlayerPanel>();
    int num1 = ((IEnumerable<GachaG007PlayerPanel>) array).Count<GachaG007PlayerPanel>();
    for (int index = 0; index < num1; ++index)
    {
      this.panel[index].transform.Clear();
      Gacha0063DirPanel gacha0063DirPanel = !array[index].highlight ? this.Menu.scene.dirPanel.Clone(this.panel[index].transform).GetComponent<Gacha0063DirPanel>() : this.Menu.scene.dirPanelSpecial.Clone(this.panel[index].transform).GetComponent<Gacha0063DirPanel>();
      gacha0063DirPanel.onObject.SetActive(true);
      gacha0063DirPanel.offObject.SetActive(false);
      if (array[index].is_opened)
      {
        gacha0063DirPanel.onObject.SetActive(false);
        gacha0063DirPanel.offObject.SetActive(true);
      }
    }
  }

  private void InitSheetGacha()
  {
    this.UpdateSheetInfo();
  }

  private void DispStepUp(bool canDisp)
  {
    this.stepUp.SetActive(canDisp);
    this.buyKiseki.SetActive(!canDisp);
  }

  private void SetStepUp(int? now, int? max)
  {
    if (!now.HasValue || !max.HasValue)
    {
      this.DispStepUp(false);
    }
    else
    {
      this.DispStepUp(true);
      this.ChangeSprite(this.stepNowText, "slc_StepUp_num" + now.ToString() + ".png__GUI__006-3_sozai__006-3_sozai_prefab");
      this.ChangeSprite(this.stepMaxText, "slc_StepUpSmall_num" + max.ToString() + ".png__GUI__006-3_sozai__006-3_sozai_prefab");
    }
  }

  private void DispGachaModeEx(int num)
  {
    if (num == 1)
    {
      this.singleGachaModeEx.SetActive(true);
      this.multiGachaModeEx.SetActive(false);
    }
    else
    {
      this.singleGachaModeEx.SetActive(false);
      this.multiGachaModeEx.SetActive(true);
    }
  }

  private void SetGachaSprite(int num)
  {
    if (num > 99)
      Debug.LogWarning((object) "SetGachaSprite　3桁以上の場合、連数が表示されません。");
    bool flag = num >= 10;
    this.singleDigitGacha.transform.parent.gameObject.SetActive(flag);
    this.doubleDigitsGacha.transform.parent.gameObject.SetActive(flag);
    this.BelowNineGacha.transform.parent.gameObject.SetActive(!flag);
    if (flag)
    {
      this.ChangeSprite(this.singleDigitGacha, "slc_gacha_num" + (num % 10).ToString() + ".png__GUI__006-3_sozai__006-3_sozai_prefab");
      this.ChangeSprite(this.doubleDigitsGacha, "slc_gacha_num" + (num / 10).ToString() + ".png__GUI__006-3_sozai__006-3_sozai_prefab");
    }
    else
      this.ChangeSprite(this.BelowNineGacha, "slc_gacha_num" + num.ToString() + ".png__GUI__006-3_sozai__006-3_sozai_prefab");
  }

  private void SetKisekiSprite(int amount)
  {
    bool flag1 = amount >= 1000;
    bool flag2 = amount >= 100 && amount < 1000;
    bool flag3 = amount >= 10 && amount < 100;
    this.singleDigitKiseki.gameObject.SetActive(flag3);
    this.singleGachaSingleDigit.gameObject.SetActive(amount < 10);
    this.singleGachaDoubleDigit.gameObject.SetActive(flag3);
    this.singleGachaTripleDigit.gameObject.SetActive(flag2);
    this.singleGachaQuadDigit.gameObject.SetActive(flag1);
    this.gachaDoubleDigit.gameObject.SetActive(flag3 || amount < 10);
    this.gachaTripleDigit.gameObject.SetActive(flag2);
    this.gachaQuadDigit.gameObject.SetActive(flag1);
    byte[] bytes = Encoding.UTF8.GetBytes(amount.ToString());
    if (flag1)
    {
      for (int index = 0; index < this.quadDigitKiseki.Count; ++index)
      {
        string str = Encoding.UTF8.GetString(bytes, index, 1);
        this.ChangeSprite(this.quadDigitKiseki[index], "slc_hime_num" + str + ".png__GUI__006-3_sozai__006-3_sozai_prefab");
        this.ChangeSprite(this.singleGachaQuadKiseki[index], "slc_hime_num" + str + ".png__GUI__006-3_sozai__006-3_sozai_prefab");
      }
    }
    else if (flag2)
    {
      for (int index = 0; index < this.tripleDigitKiseki.Count; ++index)
      {
        string str = Encoding.UTF8.GetString(bytes, index, 1);
        this.ChangeSprite(this.tripleDigitKiseki[index], "slc_hime_num" + str + ".png__GUI__006-3_sozai__006-3_sozai_prefab");
        this.ChangeSprite(this.singleGachaTripleKiseki[index], "slc_hime_num" + str + ".png__GUI__006-3_sozai__006-3_sozai_prefab");
      }
    }
    else if (flag3)
    {
      UISprite doubleDigitsKiseki = this.doubleDigitsKiseki;
      int num = amount / 10;
      string newName1 = "slc_hime_num" + num.ToString() + ".png__GUI__006-3_sozai__006-3_sozai_prefab";
      this.ChangeSprite(doubleDigitsKiseki, newName1);
      UISprite singleDigitKiseki1 = this.singleDigitKiseki;
      num = amount % 10;
      string newName2 = "slc_hime_num" + num.ToString() + ".png__GUI__006-3_sozai__006-3_sozai_prefab";
      this.ChangeSprite(singleDigitKiseki1, newName2);
      UISprite doubleDigitKiseki = this.singleGachaDoubleDigitKiseki;
      num = amount / 10;
      string newName3 = "slc_hime_num" + num.ToString() + ".png__GUI__006-3_sozai__006-3_sozai_prefab";
      this.ChangeSprite(doubleDigitKiseki, newName3);
      UISprite singleDigitKiseki2 = this.singleGachaSingleDigitKiseki;
      num = amount % 10;
      string newName4 = "slc_hime_num" + num.ToString() + ".png__GUI__006-3_sozai__006-3_sozai_prefab";
      this.ChangeSprite(singleDigitKiseki2, newName4);
    }
    else
    {
      this.ChangeSprite(this.doubleDigitsKiseki, "slc_hime_num" + amount.ToString() + ".png__GUI__006-3_sozai__006-3_sozai_prefab");
      this.ChangeSprite(this.singleGachaDigitKiseki, "slc_hime_num" + amount.ToString() + ".png__GUI__006-3_sozai__006-3_sozai_prefab");
    }
  }

  private void SetGacha(int num, int amount)
  {
    this.DispGachaModeEx(num);
    this.SetGachaSprite(num);
    this.SetKisekiSprite(amount);
  }

  private void DispDetailButton(bool canDisp)
  {
    this.detailButton.gameObject.SetActive(canDisp);
  }

  private void SetDetailButton(GachaModule module, GameObject detailPopup)
  {
    if (module.description.title == null)
      this.DispDetailButton(false);
    else
      EventDelegate.Add(this.detailButton.onClick, (EventDelegate.Callback) (() =>
      {
        if (this.Menu.IsPushAndSet())
          return;
        Singleton<CommonRoot>.GetInstance().loadingMode = 1;
        Singleton<CommonRoot>.GetInstance().isLoading = true;
        this.StartCoroutine(this.OpenDetailPopup(module, detailPopup));
      }));
  }

  private IEnumerator OpenDetailPopup(GachaModule module, GameObject detailPopup)
  {
    GachaDescription description = module.description;
    IEnumerator e = Singleton<PopupManager>.GetInstance().openAlert(detailPopup, false, false, (EventDelegate) null, false, true, false, true).GetComponent<Popup00631Menu>().InitGachaDetail(description.title, description.bodies);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Singleton<CommonRoot>.GetInstance().isLoading = false;
    Singleton<CommonRoot>.GetInstance().loadingMode = 0;
  }

  public IEnumerator InitDetail(
    GachaModule module,
    GameObject detailPopup,
    Gacha0063Menu menu)
  {
    bool canDisp = !string.IsNullOrEmpty(module.description.title);
    this.DispDetailButton(canDisp);
    if (canDisp)
    {
      this.Menu = menu;
      this.SetDetailButton(module, detailPopup);
      yield break;
    }
  }

  private void DispTimeLimit(bool canDisp)
  {
    this.dateLimitText.transform.parent.gameObject.SetActive(canDisp);
    this.hourLimitText.transform.parent.gameObject.SetActive(canDisp);
  }

  private void SwitchTimeLimit(bool isAboveDay)
  {
    this.isAboveDay = isAboveDay;
    this.dateLimitText.transform.parent.gameObject.SetActive(isAboveDay);
    this.hourLimitText.transform.parent.gameObject.SetActive(!isAboveDay);
  }

  public void UpdateLimitTime(GachaModule module, DateTime serverTime)
  {
    TimeSpan? nullable1 = new TimeSpan?();
    DateTime? endAt = module.period.end_at;
    DateTime dateTime = serverTime;
    TimeSpan? nullable2 = endAt.HasValue ? new TimeSpan?(endAt.GetValueOrDefault() - dateTime) : new TimeSpan?();
    int days = nullable2.Value.Days;
    int hours = nullable2.Value.Hours;
    int minutes = nullable2.Value.Minutes;
    TimeSpan timeSpan = nullable2.Value;
    int seconds = timeSpan.Seconds;
    timeSpan = nullable2.Value;
    if (timeSpan.Milliseconds < 0 && !this.m_TimeOut)
    {
      this.m_TimeOut = true;
      this.UpdateGacha();
    }
    else if (days > 0)
    {
      this.dateLimitText.SetTextLocalize(Consts.Format(Consts.GetInstance().GACHA_0063KISEKI_TIME_LIMIT_DAY, (IDictionary) new Hashtable()
      {
        {
          (object) "day",
          (object) days
        }
      }));
    }
    else
    {
      if (this.isAboveDay)
        this.SwitchTimeLimit(false);
      this.hourLimitText.SetTextLocalize(Consts.Format(Consts.GetInstance().GACHA_0063KISEKI_TIME_LIMIT, (IDictionary) new Hashtable()
      {
        {
          (object) "hour",
          (object) hours.ToString("00")
        },
        {
          (object) "min",
          (object) minutes.ToString("00")
        },
        {
          (object) "sec",
          (object) seconds.ToString("00")
        }
      }));
    }
  }

  public void SetTimiLimit(GachaModule module)
  {
    if (module.period.display_count_down && module.period.end_at.HasValue)
    {
      if (module.period.end_at.Value.Day > 0)
        this.SwitchTimeLimit(true);
      else
        this.SwitchTimeLimit(false);
    }
    else
      this.DispTimeLimit(false);
  }

  public void SetKisekiEx(GachaModule module, Gacha0063Menu menu)
  {
    Gacha0063SheetModel gacha0063SheetModel = new Gacha0063SheetModel(module);
    this.Init();
    this.Menu = menu;
    if (gacha0063SheetModel.IsSheetGachaOpen)
      this.InitSheetGacha();
    else
      this.SetStepUp(module.stepup.current_count, module.stepup.total_count);
    this.SetGacha(module.gacha[0].roll_count, module.gacha[0].payment_amount);
  }

  private void UpdateGacha()
  {
    Singleton<NGSceneManager>.GetInstance().sceneBase.IsPush = true;
    Singleton<NGSceneManager>.GetInstance().destroyLoadedScenes();
    Singleton<NGGameDataManager>.GetInstance().lastReferenceUnitID = -1;
    Singleton<NGGameDataManager>.GetInstance().lastReferenceUnitIndex = -1;
    Singleton<NGSceneManager>.GetInstance().clearStack();
    Singleton<NGSceneManager>.GetInstance().changeScene("gacha006_3", false, (object[]) Array.Empty<object>());
  }

  private void ChangeSprite(UISprite target, string newName)
  {
    target.spriteName = newName;
    UISpriteData atlasSprite = target.GetAtlasSprite();
    if (atlasSprite == null)
    {
      Debug.LogWarning((object) "Atlas内のSprite取得失敗");
    }
    else
    {
      target.width = atlasSprite.width;
      target.height = atlasSprite.height;
    }
  }

  public void IbtnProgressSheet()
  {
    if (this.Menu.IsPushAndSet() || !this.isSheetDetail)
      return;
    this.Menu.scene.ScrollView.enabled = false;
    this.Menu.isSheetPopup = true;
    this.isSheetDetail = false;
    this.StartCoroutine(this.ShowProgressSheet());
  }

  private IEnumerator ShowProgressSheet()
  {
    Gacha0063KisekiExtention kisekiExtention = this;
    Singleton<CommonRoot>.GetInstance().SetFooterEnable(false);
    kisekiExtention.detailButton.isEnabled = false;
    GameObject popup = kisekiExtention.Menu.popupSheet.Clone((Transform) null);
    popup.SetActive(false);
    Popup0063SheetMenu script = popup.GetComponent<Popup0063SheetMenu>();
    GachaG007PlayerSheet[] gachaG007PlayerSheetArray = SMManager.Get<GachaG007PlayerSheet[]>();
    GachaG007PlayerPanel[] array = ((IEnumerable<GachaG007PlayerPanel>) gachaG007PlayerSheetArray[0].player_panels).OrderBy<GachaG007PlayerPanel, int>((Func<GachaG007PlayerPanel, int>) (x => x.position)).ToArray<GachaG007PlayerPanel>();
    IEnumerator e = script.Init(kisekiExtention, array, gachaG007PlayerSheetArray[0]);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    // ISSUE: reference to a compiler-generated method
    script.SetCallback(new System.Action(kisekiExtention.\u003CShowProgressSheet\u003Eb__62_1));
    Singleton<PopupManager>.GetInstance().open(popup, false, false, true, true, false, false, "SE_1006");
    popup.SetActive(true);
  }
}
