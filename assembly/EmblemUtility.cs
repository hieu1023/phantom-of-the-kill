﻿// Decompiled with JetBrains decompiler
// Type: EmblemUtility
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using System;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class EmblemUtility : MonoBehaviour
{
  public static Future<UnityEngine.Sprite> LoadEmblemSprite(int emblemID)
  {
    int num = 99999;
    string path = string.Format("Prefabs/colosseum/colosseum_title/{0}_title", (object) (emblemID == 0 ? num : emblemID));
    if (Singleton<NGGameDataManager>.GetInstance().IsSea && emblemID == 0)
      path += "_sea";
    return Singleton<ResourceManager>.GetInstance().Load<UnityEngine.Sprite>(path, 1f);
  }

  public static GuildEmblemUnit GuildEnblemData(int emblemID)
  {
    return ((IEnumerable<GuildEmblemUnit>) MasterData.GuildEmblemUnitList).FirstOrDefault<GuildEmblemUnit>((Func<GuildEmblemUnit, bool>) (x => x.ID == emblemID));
  }

  public static Future<UnityEngine.Sprite> LoadGuildEmblemSprite(int emblemID)
  {
    return Singleton<ResourceManager>.GetInstance().Load<UnityEngine.Sprite>(string.Format("Prefabs/guild/guild_title/{0}_title", (object) (emblemID == 0 ? 99999 : emblemID)), 1f);
  }
}
