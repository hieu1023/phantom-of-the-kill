﻿// Decompiled with JetBrains decompiler
// Type: UnitSortAndFilterGroupButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using UnityEngine;

public class UnitSortAndFilterGroupButton : UnitSortAndFilterButton
{
  [SerializeField]
  private GameObject slcSortFilterTextBG;
  [SerializeField]
  private UILabel txtLabel;
  [SerializeField]
  private SpriteSelectDirect spriteSelectDirect;
  public bool isSelected;
  private UnitGroupHead groupType;
  private int groupID;

  public UnitGroupHead GroupType
  {
    get
    {
      return this.groupType;
    }
  }

  public int GroupID
  {
    get
    {
      return this.groupID;
    }
  }

  protected override void Awake()
  {
    base.Awake();
  }

  protected override void Update()
  {
  }

  public void Init(
    UnitSortAndFilter menu,
    UnitGroupHead type,
    int id,
    string text,
    string spriteName)
  {
    this.Menu = menu;
    this.groupType = type;
    this.groupID = id;
    this.txtLabel.SetTextLocalize(text);
    this.spriteSelectDirect.SetSpriteName<string>(spriteName, true);
    if (type != UnitGroupHead.group_all)
      return;
    this.slcSortFilterTextBG.SetActive(false);
  }

  public override void TextColorGray(bool flag)
  {
    Color color = Color.gray;
    if (flag)
      color = Color.white;
    this.txtLabel.color = color;
  }

  public override void PressButton()
  {
    if (this.isSelected)
      this.Menu.RemoveGroupInfo(this.groupType, this.groupID);
    else
      this.Menu.AddGroupInfo(this.groupType, this.groupID);
  }
}
