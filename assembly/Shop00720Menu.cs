﻿// Decompiled with JetBrains decompiler
// Type: Shop00720Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Shop00720Menu : BackButtonMenuBase
{
  private bool isWaitForShot = true;
  private List<UITweener> tweeners = new List<UITweener>();
  private List<SlotModuleSlot> slotModuleSlots = new List<SlotModuleSlot>();
  [SerializeField]
  private GameObject Title;
  [SerializeField]
  private GameObject IbtnBack;
  [SerializeField]
  private GameObject MedalInfo;
  [SerializeField]
  private GameObject IbtnCheckReward;
  [SerializeField]
  private GameObject IbtnSkip;
  [SerializeField]
  private Shop00720Menu.SlotButtonSetting[] SlotButtons;
  private GameObject slot;
  private Shop00720EffectController effect;
  private Shop00720Prefabs prefabs;
  private bool isInitalize;
  private int currentSlot;
  private int currentDeck;

  public IEnumerator Initialize(Shop00720Scene scene)
  {
    Shop00720Menu shop00720Menu = this;
    shop00720Menu.currentSlot = 0;
    shop00720Menu.SetEventOnClick();
    Future<GameObject> fPopUp = Res.Animations.Slot_Machines.machines.Load<GameObject>();
    IEnumerator e = fPopUp.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    shop00720Menu.slot = UnityEngine.Object.Instantiate<GameObject>(fPopUp.Result);
    shop00720Menu.effect = shop00720Menu.slot.GetComponent<Shop00720EffectController>();
    shop00720Menu.prefabs = new Shop00720Prefabs();
    e = shop00720Menu.prefabs.GetPrefabs();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    shop00720Menu.SetTweeners(new List<GameObject>()
    {
      shop00720Menu.Title,
      shop00720Menu.IbtnBack,
      shop00720Menu.MedalInfo
    });
    shop00720Menu.effect.textureNameList_1 = shop00720Menu.GetReelPattern(1);
    shop00720Menu.effect.textureNameList_2 = shop00720Menu.GetReelPattern(2);
    shop00720Menu.effect.textureNameList_3 = shop00720Menu.GetReelPattern(3);
    PlayerDeck[] playerDeckArray = SMManager.Get<PlayerDeck[]>();
    PlayerUnit unit = ((IEnumerable<PlayerUnit>) playerDeckArray[Persist.deckOrganized.Data.number].player_units).FirstOrDefault<PlayerUnit>();
    if (unit == (PlayerUnit) null)
      unit = ((IEnumerable<PlayerUnit>) playerDeckArray[0].player_units).First<PlayerUnit>();
    e = shop00720Menu.effect.CutInInitialize(unit);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  private void SetEventOnClick()
  {
    foreach (Shop00720Menu.SlotButtonSetting slotButton in this.SlotButtons)
    {
      if (!((UnityEngine.Object) slotButton.Button == (UnityEngine.Object) null))
      {
        UIButton component = slotButton.Button.GetComponent<UIButton>();
        if (!((UnityEngine.Object) component == (UnityEngine.Object) null))
        {
          Shop00720Menu.SlotNShot slotNshot = new Shop00720Menu.SlotNShot(this, slotButton.Consecutive);
          EventDelegate.Set(component.onClick, new EventDelegate.Callback(slotNshot.OnClick));
        }
      }
    }
  }

  private string[] GetReelPattern(int row)
  {
    return ((IEnumerable<SlotS001MedalReelDetail>) MasterData.SlotS001MedalReelDetailList).Where<SlotS001MedalReelDetail>((Func<SlotS001MedalReelDetail, bool>) (w => w.reel_detail_id == row)).Select<SlotS001MedalReelDetail, int>((Func<SlotS001MedalReelDetail, int>) (s => s.icon_id)).Select<int, string>((Func<int, string>) (s => this.GetIconFileName(s))).ToArray<string>();
  }

  private string GetIconFileName(int id)
  {
    return ((IEnumerable<SlotS001MedalReelIcon>) MasterData.SlotS001MedalReelIconList).SingleOrDefault<SlotS001MedalReelIcon>((Func<SlotS001MedalReelIcon, bool>) (sd => sd.ID == id)).file_name;
  }

  public void Ready()
  {
    Debug.Log((object) nameof (Ready));
    this.setActiveButtonBack(true);
    foreach (UITweener tweener in this.tweeners)
      tweener.PlayForward();
    this.SetMedalInfo(Player.Current.medal);
    this.slotModuleSlots.Clear();
    foreach (SlotModule slotModule in SMManager.Get<SlotModule[]>())
    {
      if (slotModule != null)
      {
        foreach (SlotModuleSlot slotModuleSlot in slotModule.slot)
        {
          if (slotModuleSlot != null)
            this.slotModuleSlots.Add(slotModuleSlot);
        }
      }
    }
    if (this.slotModuleSlots.Count == 0)
    {
      Debug.LogError((object) "NOTHING SLOTMODULES!!!");
      this.effect.loadState = false;
      this.isWaitForShot = true;
      this.isInitalize = true;
    }
    else
    {
      this.currentDeck = this.slotModuleSlots.First<SlotModuleSlot>().deck_id;
      foreach (Shop00720Menu.SlotButtonSetting slotButton in this.SlotButtons)
      {
        Shop00720Menu.SlotButtonSetting sbs = slotButton;
        SlotModuleSlot slotModuleSlot = this.slotModuleSlots.Find((Predicate<SlotModuleSlot>) (s => sbs.Consecutive == s.roll_count));
        if (sbs.Consecutive == 1 && slotModuleSlot == null)
          slotModuleSlot = this.slotModuleSlots.FirstOrDefault<SlotModuleSlot>();
        if (slotModuleSlot == null)
        {
          sbs.enabled = false;
          sbs.slotId = 0;
        }
        else
        {
          sbs.enabled = true;
          sbs.slotId = slotModuleSlot.id;
        }
        int num1 = (int) (Math.Pow(10.0, (double) sbs.Cost.Length) - 1.0);
        int num2 = sbs.enabled ? slotModuleSlot.payment_amount : 0;
        if (num1 < num2)
        {
          sbs.enabled = false;
          num2 = num1;
        }
        int index = sbs.Cost.Length - 1;
        do
        {
          int num3 = (int) Math.Pow(10.0, (double) index);
          sbs.Cost[index].SetSprite(string.Format("num_{0}.png__GUI__007-20_sozai__007-20_sozai_prefab", (object) (num2 / num3)));
          num2 %= num3;
          --index;
        }
        while (index >= 0);
        UIButton component = sbs.Button.GetComponent<UIButton>();
        if (!sbs.enabled || slotModuleSlot.payment_amount > Player.Current.medal)
        {
          component.isEnabled = false;
          foreach (UIWidget paymentAmount in sbs.PaymentAmounts)
            paymentAmount.color = new Color(0.5f, 0.5f, 0.5f);
        }
        else
        {
          component.isEnabled = true;
          foreach (UIWidget paymentAmount in sbs.PaymentAmounts)
            paymentAmount.color = new Color(1f, 1f, 1f);
        }
      }
      this.effect.loadState = false;
      this.isWaitForShot = true;
      this.isInitalize = true;
    }
  }

  private void SetTweeners(List<GameObject> list)
  {
    foreach (GameObject gameObject in list)
      this.tweeners = this.tweeners.Concat<UITweener>((IEnumerable<UITweener>) gameObject.GetComponents<UITweener>()).ToList<UITweener>();
  }

  private void SetMedalInfo(int num)
  {
    this.MedalInfo.GetComponentInChildren<UILabel>().SetTextLocalize(num);
  }

  public void OnIbtnBack()
  {
    if (!this.isWaitForShot || this.IsPushAndSet())
      return;
    this.backScene();
  }

  public override void onBackButton()
  {
    this.OnIbtnBack();
  }

  public void OnIbtnShotN(int count)
  {
    this.isWaitForShot = false;
    this.currentSlot = 0;
    bool flag = false;
    foreach (Shop00720Menu.SlotButtonSetting slotButton in this.SlotButtons)
    {
      if (slotButton.Consecutive == count)
      {
        this.currentSlot = slotButton.slotId;
        flag = true;
        break;
      }
    }
    if (!flag)
    {
      Debug.LogError((object) string.Format("{0}連スロット情報が準備できていません", (object) count));
      this.isWaitForShot = true;
    }
    else
    {
      this.setActiveButtonBack(false);
      this.StartCoroutine(this.MedalPay());
    }
  }

  private void setActiveButtonBack(bool bactive)
  {
    UIButton component = this.IbtnBack.GetComponent<UIButton>();
    if (!((UnityEngine.Object) component != (UnityEngine.Object) null))
      return;
    component.isEnabled = bactive;
  }

  private void StartSlotEff()
  {
    foreach (UITweener tweener in this.tweeners)
      tweener.PlayReverse();
    this.effect.Bet();
  }

  private IEnumerator MedalPay()
  {
    Shop00720Menu shop00720Menu = this;
    Singleton<CommonRoot>.GetInstance().loadingMode = 2;
    // ISSUE: reference to a compiler-generated method
    Future<WebAPI.Response.SlotS001MedalPay> feature = WebAPI.SlotS001MedalPay(shop00720Menu.currentSlot, new System.Action<WebAPI.Response.UserError>(shop00720Menu.\u003CMedalPay\u003Eb__29_0));
    IEnumerator e = feature.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    WebAPI.Response.SlotS001MedalPay result = feature.Result;
    if (result != null)
    {
      shop00720Menu.StartSlotEff();
      Singleton<CommonRoot>.GetInstance().loadingMode = 0;
      e = shop00720Menu.SetResult(result);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      shop00720Menu.effect.loadState = true;
    }
  }

  private IEnumerator SetResult(WebAPI.Response.SlotS001MedalPay result)
  {
    this.effect.transitionPlanList = result.animation_pattern;
    this.effect.stopTextureId_1 = result.result_reel_index[0];
    this.effect.stopTextureId_2 = result.result_reel_index[1];
    this.effect.stopTextureId_3 = result.result_reel_index[2];
    this.effect.rarity = ((IEnumerable<WebAPI.Response.SlotS001MedalPayResult>) result.result).Select<WebAPI.Response.SlotS001MedalPayResult, SlotS001MedalRarity>((Func<WebAPI.Response.SlotS001MedalPayResult, SlotS001MedalRarity>) (s => ((IEnumerable<SlotS001MedalRarity>) MasterData.SlotS001MedalRarityList).SingleOrDefault<SlotS001MedalRarity>((Func<SlotS001MedalRarity, bool>) (sd => sd.ID == s.rarity_id)))).Max<SlotS001MedalRarity>((Func<SlotS001MedalRarity, int>) (m => m.index));
    IEnumerator e = this.effect.Renpatu(result.result);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public void OnIbtnSkip()
  {
    this.effect.Bet();
    this.effect.Skip();
    if (!this.effect.Slot_script.isReady)
      return;
    this.Ready();
  }

  public void OnIbtnCheckReward()
  {
    if (this.IsPushAndSet())
      return;
    this.StartCoroutine(this.ShowRewards(this.prefabs, this.currentDeck));
  }

  private IEnumerator ShowRewards(Shop00720Prefabs prefabs, int deckID)
  {
    GameObject prefab = prefabs.DirSlotList.Clone((Transform) null);
    prefab.SetActive(false);
    IEnumerator e = prefab.GetComponent<Shop00720RewardList>().Init(prefabs, deckID);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Singleton<PopupManager>.GetInstance().open(prefab, false, false, true, true, false, false, "SE_1006");
    prefab.SetActive(true);
  }

  private void OnDestroy()
  {
    UnityEngine.Object.Destroy((UnityEngine.Object) this.slot);
  }

  protected override void Update()
  {
    if (!this.isInitalize)
      return;
    base.Update();
    if (this.effect.Slot_script.isReady)
    {
      this.IbtnSkip.SetActive(false);
      foreach (Shop00720Menu.SlotButtonSetting slotButton in this.SlotButtons)
        slotButton.Button.SetActive(true);
      this.IbtnCheckReward.SetActive(true);
      Singleton<CommonRoot>.GetInstance().SetFooterEnable(true);
    }
    else if (this.effect.Slot_script.isEnd)
    {
      Singleton<CommonRoot>.GetInstance().SetFooterEnable(true);
    }
    else
    {
      this.IbtnSkip.SetActive(true);
      foreach (Shop00720Menu.SlotButtonSetting slotButton in this.SlotButtons)
        slotButton.Button.SetActive(false);
      this.IbtnCheckReward.SetActive(false);
      Singleton<CommonRoot>.GetInstance().SetFooterEnable(false);
    }
  }

  [Serializable]
  private class SlotButtonSetting
  {
    public UISprite[] Cost = new UISprite[0];
    public UISprite[] PaymentAmounts = new UISprite[0];
    [NonSerialized]
    public bool enabled;
    [NonSerialized]
    public int slotId;
    public int Consecutive;
    public GameObject Button;
  }

  private class SlotNShot
  {
    private Shop00720Menu shop_;
    private int count_;

    public SlotNShot(Shop00720Menu shop, int count)
    {
      this.shop_ = shop;
      this.count_ = count;
    }

    public void OnClick()
    {
      this.shop_.OnIbtnShotN(this.count_);
    }
  }
}
