﻿// Decompiled with JetBrains decompiler
// Type: CommonFooter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using Earth;
using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class CommonFooter : CommonFooterBase
{
  private GameObject coming_soon_Popup;
  [SerializeField]
  private GameObject footerMissionBadge;
  [SerializeField]
  private GameObject mypageMenuPopup;
  [SerializeField]
  private GameObject dirSeaButtonRoot;
  [SerializeField]
  private UIButton seaHomeButton;
  [SerializeField]
  private UIButton seaQuestButton;
  [SerializeField]
  private UIButton seaDeckButton;
  [SerializeField]
  private UIButton seaAlbumButton;
  [SerializeField]
  private GameObject badgeSea;
  [SerializeField]
  private int L_id;
  [SerializeField]
  private bool hardMode;
  [SerializeField]
  private UIButton colosseumButton;
  [SerializeField]
  private GameObject colosseumConditoin;
  [SerializeField]
  private GameObject pvpCampaign;
  [SerializeField]
  private GameObject baseHome;
  [SerializeField]
  private GameObject baseOther;
  private NGTweenParts _tp_base_home;
  private NGTweenParts _tp_base_other;
  private Modified<Player> modifiedPlayer;
  private Modified<SM.GuildSignal> modifiedGuildSignal;
  private bool isOpenPopup;

  public bool IsActiveMyPageMenuPopup
  {
    get
    {
      return this.mypageMenuPopup.activeSelf;
    }
  }

  private void Awake()
  {
    this._tp_base_home = this.baseHome.GetComponent<NGTweenParts>();
    this._tp_base_other = this.baseOther.GetComponent<NGTweenParts>();
  }

  private void Start()
  {
    this.modifiedGuildSignal = SMManager.Observe<SM.GuildSignal>();
    this.baseHome.SetActive(false);
    this.baseOther.SetActive(true);
  }

  public void ActiveHomeFooter(bool active)
  {
    this._tp_base_home.forceActive(active);
    this._tp_base_other.forceActive(!active);
  }

  public void CloseSubMenu()
  {
    this.mypageMenuPopup.SetActive(false);
    if (!this.isOpenPopup)
      return;
    Singleton<PopupManager>.GetInstance().dismissWithoutAnim(false, true);
    this.IsPush = false;
    this.isOpenPopup = false;
  }

  public void onButtonMypageMenu()
  {
    if (this.mypageMenuPopup.activeSelf || !Singleton<TutorialRoot>.GetInstance().IsTutorialFinish() || this.IsPush)
      return;
    this.mypageMenuPopup.SetActive(true);
  }

  public void onButtonMypageClose()
  {
    if (!this.mypageMenuPopup.activeSelf || !Singleton<TutorialRoot>.GetInstance().IsTutorialFinish() || this.IsPush)
      return;
    this.mypageMenuPopup.SetActive(false);
  }

  public void onButtonOpenMypage()
  {
    if (!Singleton<TutorialRoot>.GetInstance().IsTutorialFinish() || this.IsPush)
      return;
    this.pvpCampaign.SetActive(Singleton<NGGameDataManager>.GetInstance().IsOpenPvpCampaign);
    if (!this.mypageMenuPopup.activeSelf)
    {
      this.mypageMenuPopup.SetActive(true);
    }
    else
    {
      if (!Singleton<NGSceneManager>.GetInstance().isSceneInitialized)
        return;
      this.mypageMenuPopup.SetActive(false);
      if (Singleton<NGSceneManager>.GetInstance().sceneName == "mypage")
      {
        MypageScene sceneBase = Singleton<NGSceneManager>.GetInstance().sceneBase as MypageScene;
        if ((UnityEngine.Object) sceneBase != (UnityEngine.Object) null && sceneBase.isAnimePlaying)
          return;
      }
      this.IsPush = true;
      this.preChangeScene(true, (string) null);
      Singleton<NGGameDataManager>.GetInstance().IsSea = false;
      MypageScene.ChangeScene(false, false, false);
    }
  }

  public void onButtonOpenSeaMypage()
  {
    if (!Singleton<NGSceneManager>.GetInstance().isSceneInitialized || this.IsPush)
      return;
    this.mypageMenuPopup.SetActive(false);
    this.preChangeScene(true, (string) null);
    Sea030HomeScene.ChangeScene(false, false);
  }

  public void onButtonOpenGroundMypage()
  {
    if (!Singleton<NGSceneManager>.GetInstance().isSceneInitialized || this.IsPushAndSet())
      return;
    this.mypageMenuPopup.SetActive(false);
    this.preChangeScene(true, (string) null);
    MypageScene sceneBase = Singleton<NGSceneManager>.GetInstance().sceneBase as MypageScene;
    if ((bool) (UnityEngine.Object) sceneBase)
      sceneBase.onStartEarthCloud();
    else
      EarthDataManager.startEarthScene((MonoBehaviour) this);
  }

  public void onButtonOpenSeaQuest()
  {
    if (!Singleton<NGSceneManager>.GetInstance().isSceneInitialized || this.IsPush)
      return;
    this.mypageMenuPopup.SetActive(false);
    string clearStackName = "sea030_quest";
    if (!(Singleton<NGSceneManager>.GetInstance().sceneName != clearStackName) && Singleton<NGGameDataManager>.GetInstance().IsSea)
      return;
    Sea030_questScene.ChangeScene(this.preChangeScene(false, clearStackName), true, false);
  }

  public void onButtonSeaAlbum()
  {
    if (this.IsPush)
      return;
    this.mypageMenuPopup.SetActive(false);
    string clearStackName = "sea030_album";
    if (!(Singleton<NGSceneManager>.GetInstance().sceneName != clearStackName) && Singleton<NGGameDataManager>.GetInstance().IsSea)
      return;
    Sea030AlbumScene.ChangeScene(this.preChangeScene(false, clearStackName));
  }

  public void onButtonSeaDeck()
  {
    if (this.IsPush)
      return;
    this.mypageMenuPopup.SetActive(false);
    string clearStackName = "unit004_6_0822_sea";
    if (!(Singleton<NGSceneManager>.GetInstance().sceneName != clearStackName) && Singleton<NGGameDataManager>.GetInstance().IsSea)
      return;
    int num = this.preChangeScene(false, clearStackName) ? 1 : 0;
    Singleton<NGGameDataManager>.GetInstance().IsSea = true;
    Singleton<NGGameDataManager>.GetInstance().QuestType = new CommonQuestType?();
    Unit0046Scene.changeScene(num != 0, (QuestLimitationBase[]) null, (string) null, true);
  }

  public void onButtonOpenEventQuest()
  {
    if (!Singleton<NGSceneManager>.GetInstance().isSceneInitialized || this.IsPush)
      return;
    this.mypageMenuPopup.SetActive(false);
    string clearStackName = "quest002_17";
    if (!(Singleton<NGSceneManager>.GetInstance().sceneName != clearStackName) && !Singleton<NGGameDataManager>.GetInstance().IsSea)
      return;
    int num = this.preChangeScene(false, clearStackName) ? 1 : 0;
    Singleton<NGGameDataManager>.GetInstance().IsSea = false;
    Quest00217Scene.ChangeScene(num != 0);
  }

  public void onButtonUnit()
  {
    if (!Singleton<TutorialRoot>.GetInstance().IsTutorialFinish() || this.IsPush || (!Singleton<NGSceneManager>.GetInstance().isSceneInitialized || Singleton<NGSceneManager>.GetInstance().changeSceneQueueCount > 0))
      return;
    this.mypageMenuPopup.SetActive(false);
    string clearStackName = "unit004_top";
    if (!(Singleton<NGSceneManager>.GetInstance().sceneName != clearStackName) && !Singleton<NGGameDataManager>.GetInstance().IsSea)
      return;
    int num = this.preChangeScene(false, clearStackName) ? 1 : 0;
    Singleton<NGGameDataManager>.GetInstance().IsSea = false;
    Unit004topScene.ChangeSceneNoClearStack(num != 0);
  }

  public void onButtonWeapon()
  {
    string str = "bugu005_1";
    if (!Singleton<TutorialRoot>.GetInstance().IsTutorialFinish() || this.IsPush)
      return;
    this.mypageMenuPopup.SetActive(false);
    if (!(Singleton<NGSceneManager>.GetInstance().sceneName != str) && !Singleton<NGGameDataManager>.GetInstance().IsSea)
      return;
    bool isStack = this.preChangeScene(false, str);
    Singleton<NGGameDataManager>.GetInstance().IsSea = false;
    this.changeScene(str, isStack, false);
  }

  public void onButtonGacha()
  {
    string str = "gacha006_3";
    if (!Singleton<TutorialRoot>.GetInstance().IsTutorialFinish() || this.IsPush || (!Singleton<NGSceneManager>.GetInstance().isSceneInitialized || Singleton<NGSceneManager>.GetInstance().changeSceneQueueCount > 0))
      return;
    this.mypageMenuPopup.SetActive(false);
    if (!(Singleton<NGSceneManager>.GetInstance().sceneName != str) && !Singleton<NGGameDataManager>.GetInstance().IsSea)
      return;
    bool isStack = this.preChangeScene(false, str);
    Singleton<NGGameDataManager>.GetInstance().IsSea = false;
    Singleton<NGSceneManager>.GetInstance().changeScene(str, isStack, (object[]) Array.Empty<object>());
  }

  public void onButtonShop()
  {
    if (!Singleton<TutorialRoot>.GetInstance().IsTutorialFinish() || this.IsPush || (!Singleton<NGSceneManager>.GetInstance().isSceneInitialized || Singleton<NGSceneManager>.GetInstance().changeSceneQueueCount > 0))
      return;
    this.mypageMenuPopup.SetActive(false);
    string clearStackName = "shop007_1";
    if (!(Singleton<NGSceneManager>.GetInstance().sceneName != clearStackName) && !Singleton<NGGameDataManager>.GetInstance().IsSea)
      return;
    int num = this.preChangeScene(false, clearStackName) ? 1 : 0;
    Singleton<NGGameDataManager>.GetInstance().IsSea = false;
    Shop0071Scene.changeScene(num != 0);
  }

  public void onButtonMission()
  {
    if (!Singleton<TutorialRoot>.GetInstance().IsTutorialFinish() || this.IsPush || (!Singleton<NGSceneManager>.GetInstance().isSceneInitialized || Singleton<NGSceneManager>.GetInstance().changeSceneQueueCount > 0))
      return;
    Singleton<CommonRoot>.GetInstance().DailyMissionController.Show();
  }

  public void onButtonMulti()
  {
    if (this.IsPush)
      return;
    this.mypageMenuPopup.SetActive(false);
    string clearStackName = "versus026_1";
    if (!(Singleton<NGSceneManager>.GetInstance().sceneName != clearStackName) && !Singleton<NGGameDataManager>.GetInstance().IsSea)
      return;
    int num = this.preChangeScene(false, clearStackName) ? 1 : 0;
    Singleton<NGGameDataManager>.GetInstance().IsSea = false;
    Versus0261Scene.ChangeScene0261(num != 0);
  }

  public void onButtonCharacter()
  {
    if (this.IsPush)
      return;
    this.mypageMenuPopup.SetActive(false);
    string clearStackName = "quest002_14";
    if (!(Singleton<NGSceneManager>.GetInstance().sceneName != clearStackName) && !Singleton<NGGameDataManager>.GetInstance().IsSea)
      return;
    int num = this.preChangeScene(false, clearStackName) ? 1 : 0;
    Singleton<NGGameDataManager>.GetInstance().IsSea = false;
    Quest00214Scene.ChangeScene(num != 0);
  }

  public void onButtonQuest()
  {
    if (this.IsPushAndSet())
      return;
    this.mypageMenuPopup.SetActive(false);
    NGSoundManager instance = Singleton<NGSoundManager>.GetInstance();
    if ((UnityEngine.Object) instance != (UnityEngine.Object) null)
      instance.playSE("SE_1002", false, 0.0f, -1);
    PlayerStoryQuestS[] playerStoryQuestSArray = SMManager.Get<PlayerStoryQuestS[]>();
    if (playerStoryQuestSArray != null && ((IEnumerable<PlayerStoryQuestS>) playerStoryQuestSArray).Any<PlayerStoryQuestS>((Func<PlayerStoryQuestS, bool>) (x => x.quest_story_s.quest_xl_QuestStoryXL == 4)))
    {
      this.StartCoroutine(this.openStorySelectPopup());
    }
    else
    {
      Singleton<CommonRoot>.GetInstance().getBackgroundComponent<QuestBG>().CloudAnim(false);
      this.StartCoroutine(this.GetComponent<BGChange>().asyncBgAnim(QuestBG.AnimApply.MyPage, 1f));
      Quest00240723Scene.ChangeScene0024(true, this.L_id, this.hardMode, false);
    }
  }

  public void onButtonColosseum()
  {
    if (this.IsPush)
      return;
    PlayerUnit[] playerUnitArray = SMManager.Get<PlayerUnit[]>();
    if (((IEnumerable<PlayerUnit>) playerUnitArray).Count<PlayerUnit>() < 3)
    {
      this.StartCoroutine(this.openPopup008161UnitInsufficiency());
    }
    else
    {
      Player player = SMManager.Get<Player>();
      PlayerUnit[] array = ((IEnumerable<PlayerUnit>) playerUnitArray).OrderBy<PlayerUnit, int>((Func<PlayerUnit, int>) (x => x.cost)).ToArray<PlayerUnit>();
      int num = 0;
      for (int index = 0; index < 3; ++index)
        num += array[index].cost;
      if (player.max_cost >= num)
      {
        string clearStackName = "colosseum023_4";
        this.mypageMenuPopup.SetActive(false);
        if (!(Singleton<NGSceneManager>.GetInstance().sceneName != clearStackName) && !Singleton<NGGameDataManager>.GetInstance().IsSea)
          return;
        bool isStack = this.preChangeScene(false, clearStackName);
        Singleton<NGGameDataManager>.GetInstance().IsSea = false;
        Colosseum0234Scene.ChangeScene(true, isStack);
      }
      else
        this.StartCoroutine(this.openPopup008161CostInsufficiency());
    }
  }

  public void onButtonInfo()
  {
    if (this.IsPush)
      return;
    string str = "mypage001_8_1";
    this.mypageMenuPopup.SetActive(false);
    if (!(Singleton<NGSceneManager>.GetInstance().sceneName != str) && !Singleton<NGGameDataManager>.GetInstance().IsSea)
      return;
    bool isStack = this.preChangeScene(false, str);
    Singleton<NGGameDataManager>.GetInstance().IsSea = false;
    Singleton<NGSceneManager>.GetInstance().changeScene(str, isStack, (object[]) Array.Empty<object>());
  }

  public void onButtonPanelMission()
  {
    if (this.IsPush)
      return;
    string str = "dailymission027_1";
    this.mypageMenuPopup.SetActive(false);
    if (!(Singleton<NGSceneManager>.GetInstance().sceneName != str) && !Singleton<NGGameDataManager>.GetInstance().IsSea)
      return;
    bool isStack = this.preChangeScene(false, str);
    Singleton<NGGameDataManager>.GetInstance().IsSea = false;
    Singleton<NGSceneManager>.GetInstance().changeScene(str, isStack, (object[]) Array.Empty<object>());
  }

  public void onButtonPresent()
  {
    if (this.IsPush)
      return;
    string str = "mypage001_7";
    this.mypageMenuPopup.SetActive(false);
    if (!(Singleton<NGSceneManager>.GetInstance().sceneName != str) && !Singleton<NGGameDataManager>.GetInstance().IsSea)
      return;
    bool isStack = this.preChangeScene(false, str);
    Singleton<NGGameDataManager>.GetInstance().IsSea = false;
    Singleton<NGSceneManager>.GetInstance().changeScene(str, isStack, (object[]) Array.Empty<object>());
  }

  private IEnumerator openPopup008161UnitInsufficiency()
  {
    Future<GameObject> prefabF = Res.Prefabs.popup.popup_008_16_1__anim_popup01.Load<GameObject>();
    IEnumerator e = prefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    GameObject pObj = Singleton<PopupManager>.GetInstance().open(prefabF.Result, false, false, false, true, false, false, "SE_1006");
    pObj.SetActive(false);
    e = pObj.GetComponent<Friend008161Menu>().Init(Consts.GetInstance().COLOSSEUM_ALERT_TITLE_UNIT, Consts.GetInstance().COLOSSEUM_ALERT_MESSAGE_UNIT);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    pObj.SetActive(true);
  }

  private IEnumerator openPopup008161CostInsufficiency()
  {
    Future<GameObject> prefabF = Res.Prefabs.popup.popup_008_16_1__anim_popup01.Load<GameObject>();
    IEnumerator e = prefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    GameObject pObj = Singleton<PopupManager>.GetInstance().open(prefabF.Result, false, false, false, true, false, false, "SE_1006");
    pObj.SetActive(false);
    e = pObj.GetComponent<Friend008161Menu>().Init(Consts.GetInstance().COLOSSEUM_ALERT_TITLE_COST, Consts.GetInstance().COLOSSEUM_ALERT_MESSAGE_COST);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    pObj.SetActive(true);
  }

  public void onButtonMenu()
  {
    if (this.IsPush)
      return;
    string str = "story001_9_1";
    this.mypageMenuPopup.SetActive(false);
    if (!(Singleton<NGSceneManager>.GetInstance().sceneName != str) && !Singleton<NGGameDataManager>.GetInstance().IsSea)
      return;
    bool isStack = this.preChangeScene(false, str);
    Singleton<NGGameDataManager>.GetInstance().IsSea = false;
    Singleton<NGSceneManager>.GetInstance().changeScene(str, isStack, (object[]) Array.Empty<object>());
  }

  public void onButtonGuild()
  {
    if (!Singleton<TutorialRoot>.GetInstance().IsTutorialFinish() || this.IsPush || (!Singleton<NGSceneManager>.GetInstance().isSceneInitialized || Singleton<NGSceneManager>.GetInstance().changeSceneQueueCount > 0))
      return;
    this.mypageMenuPopup.SetActive(false);
    string clearStackName = "guild028_1";
    if (!(Singleton<NGSceneManager>.GetInstance().sceneName != clearStackName) && !Singleton<NGGameDataManager>.GetInstance().IsSea)
      return;
    bool stack = this.preChangeScene(false, clearStackName);
    Singleton<NGGameDataManager>.GetInstance().IsSea = false;
    Guild0281Scene.ChangeSceneGuildTop(false, (Guild0281Menu) null, stack);
  }

  private bool preChangeScene(bool clearSceneStackAll = false, string clearStackName = null)
  {
    NGSceneManager instance1 = Singleton<NGSceneManager>.GetInstance();
    bool flag1 = false;
    if (instance1.sceneName == "mypage")
    {
      flag1 = true;
      instance1.destroyCurrentScene();
    }
    if (!string.IsNullOrEmpty(clearStackName))
    {
      if (instance1.sceneName == clearStackName)
      {
        flag1 = true;
        instance1.destroyCurrentScene();
      }
      if (instance1.clearStack(clearStackName) > 0)
        flag1 = true;
    }
    bool flag2 = false;
    NGGameDataManager instance2 = Singleton<NGGameDataManager>.GetInstance();
    instance2.lastReferenceUnitID = -1;
    instance2.lastReferenceUnitIndex = -1;
    NGSceneBase sceneBase = instance1.sceneBase;
    if ((UnityEngine.Object) sceneBase != (UnityEngine.Object) null)
      sceneBase.IsPush = true;
    if (clearSceneStackAll)
    {
      instance1.destroyLoadedScenes();
    }
    else
    {
      flag2 = instance1.clearStackBeforeTopGlobalBack();
      instance1.destoryNonStackScenes();
    }
    return !flag1 && flag2;
  }

  private void Update()
  {
    if (this.modifiedPlayer == null)
    {
      this.modifiedPlayer = SMManager.Observe<Player>();
      this.modifiedPlayer.NotifyChanged();
    }
    if (this.modifiedPlayer.IsChangedOnce())
    {
      this.footerMissionBadge.SetActive(this.modifiedPlayer.Value.is_open_mission);
      bool flag = this.modifiedPlayer.Value.IsSea();
      bool isColloseumOpen = this.modifiedPlayer.Value.IsColosseum();
      this.seaHomeButton.isEnabled = flag;
      this.seaQuestButton.isEnabled = flag;
      this.seaAlbumButton.isEnabled = flag;
      this.seaDeckButton.isEnabled = flag;
      this.badgeSea.SetActive(flag);
      this.dirSeaButtonRoot.SetActive(flag);
      this.SetColosseumButtonActive(isColloseumOpen);
    }
    if (this.modifiedGuildSignal == null || Singleton<NGGameDataManager>.GetInstance().IsEarth || (SM.GuildSignal.Current == null || !this.modifiedGuildSignal.IsChangedOnce()))
      return;
    if (SM.GuildSignal.Current.existGvgEvent(GuildEventType.change_map_info) && Singleton<NGSceneManager>.GetInstance().sceneName == "guild028_2" && (UnityEngine.Object) Singleton<NGSceneManager>.GetInstance().sceneBase != (UnityEngine.Object) null)
    {
      Guild0282Menu component = Singleton<NGSceneManager>.GetInstance().sceneBase.GetComponent<Guild0282Menu>();
      if ((UnityEngine.Object) component != (UnityEngine.Object) null)
      {
        GuildEventGvg currentGvgEvent = SM.GuildSignal.Current.getCurrentGvgEvent(GuildEventType.change_map_info);
        PlayerAffiliation.Current.guild = currentGvgEvent.guild;
        this.StartCoroutine(component.MapReload(currentGvgEvent));
      }
    }
    this.SetGuildBadge();
  }

  private void SetColosseumButtonActive(bool isColloseumOpen)
  {
    this.colosseumButton.isEnabled = isColloseumOpen;
    this.colosseumConditoin.SetActive(!isColloseumOpen);
  }

  private void SetGuildBadge()
  {
    bool flg = false;
    bool flag1 = false;
    if (PlayerAffiliation.Current == null)
      return;
    if (!Persist.guildSetting.Exists)
    {
      Persist.guildSetting.Data.reset();
      Persist.guildSetting.Flush();
    }
    if (!Persist.guildSetting.Exists)
      Singleton<CommonRoot>.GetInstance().SetGuildFooterBadge(false, false, false);
    else if (!PlayerAffiliation.Current.isGuildMember())
    {
      if (SM.GuildSignal.Current.existRelationshipEventWithoutMyself(GuildEventType.leave_membership))
        Singleton<CommonRoot>.GetInstance().SetGuildFooterBadge(true, false, false);
      else
        Singleton<CommonRoot>.GetInstance().SetGuildFooterBadge(false, false, false);
      Singleton<CommonRoot>.GetInstance().SetGuildFooterBadge(GuildUtil.FooterGuildBadge.label, false);
      if (!Persist.gvgBattleEnvironment.Exists)
        return;
      Persist.gvgBattleEnvironment.Delete();
    }
    else
    {
      bool flag2 = Singleton<NGSceneManager>.GetInstance().sceneName.Equals("guild028_1") || Singleton<NGSceneManager>.GetInstance().sceneName.Equals("guild028_3") || Singleton<NGSceneManager>.GetInstance().sceneName.Equals("raid_top");
      int num = !((UnityEngine.Object) Singleton<NGSceneManager>.GetInstance().sceneBase != (UnityEngine.Object) null) ? 0 : (Singleton<NGSceneManager>.GetInstance().sceneBase.currentSceneGuildChatDisplayingStatus == NGSceneBase.GuildChatDisplayingStatus.Opened ? 1 : 0);
      GuildRole? role = PlayerAffiliation.Current.role;
      GuildRole guildRole1 = GuildRole.master;
      if (!(role.GetValueOrDefault() == guildRole1 & role.HasValue))
      {
        role = PlayerAffiliation.Current.role;
        GuildRole guildRole2 = GuildRole.sub_master;
        if (!(role.GetValueOrDefault() == guildRole2 & role.HasValue))
        {
          GuildUtil.setBadgeState(GuildUtil.GuildBadgeInfoType.newApplicant, false);
          GuildUtil.setBadgeState(GuildUtil.GuildBadgeInfoType.newMember, false);
          goto label_20;
        }
      }
      if (SM.GuildSignal.Current.existNewApplicant())
      {
        GuildUtil.setBadgeState(GuildUtil.GuildBadgeInfoType.newApplicant, true);
        flag1 = true;
      }
      if (PlayerAffiliation.Current.guild.auto_approval.auto_approval && SM.GuildSignal.Current.existNewMember())
      {
        GuildUtil.setBadgeState(GuildUtil.GuildBadgeInfoType.newMember, true);
        flag1 = true;
      }
label_20:
      if (!flag2 && SM.GuildSignal.Current.existPlayershipEventType(GuildEventType.apply_applicant))
        flg = true;
      if (((IEnumerable<GuildEventGift>) SM.GuildSignal.Current.gift_events).Where<GuildEventGift>((Func<GuildEventGift, bool>) (x => x.event_type == GuildEventType.incoming_gift)).FirstOrDefault<GuildEventGift>() != null)
      {
        GuildUtil.setBadgeState(GuildUtil.GuildBadgeInfoType.newGift, true);
        flag1 = true;
      }
      if (!flag2 && SM.GuildSignal.Current.existPayloadEvent(GuildEventType.level_up))
      {
        GuildUtil.setBadgeState(GuildUtil.GuildBadgeInfoType.guildLevelup, true);
        flag1 = true;
      }
      if (!flag2 && SM.GuildSignal.Current.existBaseEvent(GuildEventType.base_rank_up))
      {
        GuildUtil.setBadgeState(GuildUtil.GuildBadgeInfoType.baseRankUp, true);
        flag1 = true;
      }
      if (num == 0 && SM.GuildSignal.Current.existChatEvent(GuildEventType.post_new_chat))
      {
        GuildUtil.setBadgeState(GuildUtil.GuildBadgeInfoType.postNewChat, true);
        flag1 = true;
      }
      if (GuildUtil.getBadgeState(GuildUtil.GuildBadgeInfoType.newApplicant) || !flag2 && GuildUtil.getBadgeState(GuildUtil.GuildBadgeInfoType.startHuntingEvent) || (!flag2 && GuildUtil.getBadgeState(GuildUtil.GuildBadgeInfoType.receiveHuntingReward) || !flag2 && GuildUtil.getBadgeState(GuildUtil.GuildBadgeInfoType.newMember)) || (GuildUtil.getBadgeState(GuildUtil.GuildBadgeInfoType.newGift) || GuildUtil.getBadgeState(GuildUtil.GuildBadgeInfoType.guildLevelup) || GuildUtil.getBadgeState(GuildUtil.GuildBadgeInfoType.baseRankUp)))
        flg = true;
      if (GuildUtil.getBadgeState(GuildUtil.GuildBadgeInfoType.postNewChat))
        Singleton<CommonRoot>.GetInstance().SetGuildFooterBadge(GuildUtil.FooterGuildBadge.chat, true);
      else
        Singleton<CommonRoot>.GetInstance().SetGuildFooterBadge(GuildUtil.FooterGuildBadge.bikkuri, flg);
      if (!GuildUtil.getBadgeState(GuildUtil.GuildBadgeInfoType.changeRole) && SM.GuildSignal.Current.existRoleChange())
      {
        GuildUtil.setBadgeState(GuildUtil.GuildBadgeInfoType.changeRole, true);
        flag1 = true;
      }
      if (!GuildUtil.getBadgeState(GuildUtil.GuildBadgeInfoType.newTitle) && SM.GuildSignal.Current.existNewTitle())
      {
        GuildUtil.setBadgeState(GuildUtil.GuildBadgeInfoType.newTitle, true);
        flag1 = true;
      }
      GuildEvent guildEvent = ((IEnumerable<GuildEvent>) SM.GuildSignal.Current.guild_events).Where<GuildEvent>((Func<GuildEvent, bool>) (x => x.event_type == GuildEventType.gvg_entry || x.event_type == GuildEventType.gvg_matched || (x.event_type == GuildEventType.gvg_started || x.event_type == GuildEventType.gvg_finished) || (x.event_type == GuildEventType.gvg_entry_expired || x.event_type == GuildEventType.gvg_entry_cancel || x.event_type == GuildEventType.guild_raid_started) || x.event_type == GuildEventType.guild_raid_end)).OrderByDescending<GuildEvent, DateTime?>((Func<GuildEvent, DateTime?>) (x => x.created_at)).FirstOrDefault<GuildEvent>();
      if (guildEvent != null)
      {
        GuildUtil.setBadgeState(GuildUtil.GuildBadgeInfoType.gvg_entry, false);
        GuildUtil.setBadgeState(GuildUtil.GuildBadgeInfoType.gvg_matched, false);
        GuildUtil.setBadgeState(GuildUtil.GuildBadgeInfoType.gvg_started, false);
        GuildUtil.setBadgeState(GuildUtil.GuildBadgeInfoType.guild_raid, false);
        if (guildEvent.event_type == GuildEventType.gvg_entry)
          GuildUtil.setBadgeState(GuildUtil.GuildBadgeInfoType.gvg_entry, true);
        else if (guildEvent.event_type == GuildEventType.gvg_entry_cancel)
          GuildUtil.setBadgeState(GuildUtil.GuildBadgeInfoType.gvg_entry, false);
        else if (guildEvent.event_type == GuildEventType.gvg_matched)
          GuildUtil.setBadgeState(GuildUtil.GuildBadgeInfoType.gvg_matched, true);
        else if (guildEvent.event_type == GuildEventType.gvg_started)
          GuildUtil.setBadgeState(GuildUtil.GuildBadgeInfoType.gvg_started, true);
        else if (guildEvent.event_type == GuildEventType.guild_raid_started)
          GuildUtil.setBadgeState(GuildUtil.GuildBadgeInfoType.guild_raid, true);
        else if (guildEvent.event_type == GuildEventType.guild_raid_end)
          GuildUtil.setBadgeState(GuildUtil.GuildBadgeInfoType.guild_raid, false);
        flag1 = true;
        if (Persist.gvgBattleEnvironment.Exists)
          Persist.gvgBattleEnvironment.Delete();
      }
      GuildUtil.GuildBadgeLabelType labelType = GuildUtil.GuildBadgeLabelType.none;
      if (GuildUtil.getBadgeState(GuildUtil.GuildBadgeInfoType.gvg_entry))
        labelType = GuildUtil.GuildBadgeLabelType.entry;
      else if (GuildUtil.getBadgeState(GuildUtil.GuildBadgeInfoType.gvg_matched))
        labelType = GuildUtil.GuildBadgeLabelType.prepare;
      else if (GuildUtil.getBadgeState(GuildUtil.GuildBadgeInfoType.gvg_started))
        labelType = GuildUtil.GuildBadgeLabelType.battle;
      else if (GuildUtil.getBadgeState(GuildUtil.GuildBadgeInfoType.guild_raid))
        labelType = GuildUtil.GuildBadgeLabelType.raidBoss;
      if (labelType == GuildUtil.GuildBadgeLabelType.none)
      {
        Singleton<CommonRoot>.GetInstance().SetGuildFooterBadge(GuildUtil.FooterGuildBadge.label, false);
      }
      else
      {
        Singleton<CommonRoot>.GetInstance().SetGuildFooterBadgeLabel(labelType);
        Singleton<CommonRoot>.GetInstance().SetGuildFooterBadge(GuildUtil.FooterGuildBadge.label, true);
      }
      if (!flag1)
        return;
      Persist.guildSetting.Flush();
    }
  }

  public IEnumerator openStorySelectPopup()
  {
    CommonFooter commonFooter = this;
    commonFooter.IsPush = true;
    commonFooter.isOpenPopup = true;
    Singleton<NGSceneManager>.GetInstance().sceneBase.IsPush = true;
    MypageScene mypage = (MypageScene) null;
    if (Singleton<NGSceneManager>.GetInstance().sceneName == "mypage")
    {
      mypage = Singleton<NGSceneManager>.GetInstance().sceneBase as MypageScene;
      mypage.menu.SetMypageBannerHilight(false);
    }
    Future<GameObject> prefabF = new ResourceObject("Prefabs/mypage/dir_story_menu").Load<GameObject>();
    IEnumerator e = prefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    GameObject popup = prefabF.Result.Clone((Transform) null);
    popup.SetActive(false);
    PopupMypageStorySelect script = popup.GetComponent<PopupMypageStorySelect>();
    // ISSUE: reference to a compiler-generated method
    // ISSUE: reference to a compiler-generated method
    e = script.Initialize(new System.Action<int, bool>(commonFooter.\u003CopenStorySelectPopup\u003Eb__56_0), new System.Action(commonFooter.\u003CopenStorySelectPopup\u003Eb__56_1), (UnityEngine.Object) mypage != (UnityEngine.Object) null ? mypage.menu : (MypageMenu) null, (MonoBehaviour) commonFooter);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Singleton<PopupManager>.GetInstance().open(popup, false, false, true, true, true, false, "SE_1006");
    Singleton<NGSoundManager>.GetInstance().playSE("SE_1056", false, 0.0f, -1);
    yield return (object) new WaitForSeconds(1f);
    script.isPush = false;
  }
}
