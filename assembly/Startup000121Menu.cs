﻿// Decompiled with JetBrains decompiler
// Type: Startup000121Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Startup000121Menu : NGMenuBase
{
  private List<Startup000121NewsScrollParts> scrollNewsPartList = new List<Startup000121NewsScrollParts>();
  private List<Startup000121EventsScrollParts> scrollEventPartList = new List<Startup000121EventsScrollParts>();
  private List<Startup000121FunctionsScrollParts> scrollFunctionPartList = new List<Startup000121FunctionsScrollParts>();
  public bool isContinue = true;
  public string listSceneName = "startup000_12";
  private string[] scrollVoice = new string[3]
  {
    "durin_navi_0029",
    "durin_navi_0070",
    "durin_navi_0069"
  };
  [SerializeField]
  protected UILabel TxtDate;
  [SerializeField]
  protected UILabel TxtDate2;
  [SerializeField]
  protected UILabel TxtNewstitle;
  [SerializeField]
  protected UILabel TxtNewstitle2;
  [SerializeField]
  protected UILabel TxtTime;
  [SerializeField]
  protected UILabel TxtTime2;
  [SerializeField]
  protected UILabel TxtTitle;
  public GameObject[] dirButtons;
  public GameObject[] dirNonActiveButtons;
  public GameObject[] containers;
  public GameObject[] scrollContainers;
  public UIPanel panel;
  public UIWidget widget;
  public NGxScroll[] scrolls;
  private GameObject scroll;
  public bool isNews;
  public bool isEvent;
  public bool isFunction;
  public int selectContainer;
  [SerializeField]
  private string nextScenename;

  public List<Startup000121NewsScrollParts> ScrollNewsPartList
  {
    get
    {
      return this.scrollNewsPartList;
    }
    set
    {
      this.scrollNewsPartList = value;
    }
  }

  public List<Startup000121EventsScrollParts> ScrollEventPartList
  {
    get
    {
      return this.scrollEventPartList;
    }
    set
    {
      this.scrollEventPartList = value;
    }
  }

  public List<Startup000121FunctionsScrollParts> ScrollFunctionPartList
  {
    get
    {
      return this.scrollFunctionPartList;
    }
    set
    {
      this.scrollFunctionPartList = value;
    }
  }

  public string NextScenename
  {
    get
    {
      return this.nextScenename;
    }
    set
    {
      this.nextScenename = value;
    }
  }

  public void IbtnClose()
  {
    if (Singleton<NGGameDataManager>.GetInstance().InfoOrLoginBonusJump())
      return;
    if (Singleton<NGGameDataManager>.GetInstance().IsSea)
    {
      Singleton<CommonRoot>.GetInstance().isLoading = true;
      Sea030HomeScene.ChangeScene(false, false);
    }
    else
      MypageScene.ChangeScene(false, false, false);
  }

  public void ActivContainers(int num)
  {
    this.selectContainer = num;
    for (int index = 0; index < this.dirButtons.Length; ++index)
    {
      this.dirButtons[index].SetActive(index == num);
      this.dirNonActiveButtons[index].SetActive(index != num);
      this.containers[index].SetActive(index == num);
    }
  }

  public void IbtnNews()
  {
    if (this.isNews)
    {
      Singleton<NGSoundManager>.GetInstance().stopVoice(-1);
      Singleton<NGSoundManager>.GetInstance().playVoiceByStringID("VO_9999", this.scrollVoice[0], -1, 0.0f, (string) null);
    }
    this.ActivContainers(0);
    this.panel.SetDirty();
    this.scrolls[0].ResolvePosition();
  }

  public void IbtnEvent()
  {
    if (this.isEvent)
    {
      Singleton<NGSoundManager>.GetInstance().stopVoice(-1);
      Singleton<NGSoundManager>.GetInstance().playVoiceByStringID("VO_9999", this.scrollVoice[1], -1, 0.0f, (string) null);
    }
    this.ActivContainers(1);
    this.panel.SetDirty();
    this.scrolls[1].ResolvePosition();
  }

  public void IbtnFunctionadd()
  {
    if (this.isFunction)
    {
      Singleton<NGSoundManager>.GetInstance().stopVoice(-1);
      Singleton<NGSoundManager>.GetInstance().playVoiceByStringID("VO_9999", this.scrollVoice[2], -1, 0.0f, (string) null);
    }
    this.ActivContainers(2);
    this.panel.SetDirty();
    this.scrolls[2].ResolvePosition();
  }

  public IEnumerator InitNoticeList(OfficialInformationArticle[] infos, int num)
  {
    Startup000121Menu startup000121Menu = this;
    Future<GameObject> scroll_1;
    IEnumerator e;
    switch (num)
    {
      case 0:
        scroll_1 = Res.Prefabs.startup000_12_1.vscroll_732_2.Load<GameObject>();
        e = scroll_1.Wait();
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        startup000121Menu.scroll = scroll_1.Result;
        scroll_1 = (Future<GameObject>) null;
        break;
      case 1:
        scroll_1 = Res.Prefabs.startup000_12_1.vscroll_732_4.Load<GameObject>();
        e = scroll_1.Wait();
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        startup000121Menu.scroll = scroll_1.Result;
        scroll_1 = (Future<GameObject>) null;
        break;
      case 2:
        scroll_1 = Res.Prefabs.startup000_12_1.vscroll_732_3.Load<GameObject>();
        e = scroll_1.Wait();
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        startup000121Menu.scroll = scroll_1.Result;
        scroll_1 = (Future<GameObject>) null;
        break;
    }
    startup000121Menu.scrolls[num].Clear();
    for (int i = infos.Length - 1; i >= 0; --i)
    {
      GameObject prefab = UnityEngine.Object.Instantiate<GameObject>(startup000121Menu.scroll);
      startup000121Menu.scrolls[num].Add(prefab, false);
      switch (num)
      {
        case 0:
          prefab.GetComponent<Startup000121NewsScrollParts>().Set(infos[i], startup000121Menu.NextScenename);
          prefab.GetComponent<Startup000121NewsScrollParts>().SetData(infos[i], (NGMenuBase) startup000121Menu);
          startup000121Menu.ScrollNewsPartList.Add(prefab.GetComponent<Startup000121NewsScrollParts>());
          break;
        case 1:
          prefab.GetComponent<Startup000121EventsScrollParts>().Set(infos[i], startup000121Menu.NextScenename);
          e = prefab.GetComponent<Startup000121EventsScrollParts>().SetDataOld(infos[i], (NGMenuBase) startup000121Menu);
          while (e.MoveNext())
            yield return e.Current;
          e = (IEnumerator) null;
          startup000121Menu.ScrollEventPartList.Add(prefab.GetComponent<Startup000121EventsScrollParts>());
          break;
        case 2:
          prefab.GetComponent<Startup000121FunctionsScrollParts>().Set(infos[i], startup000121Menu.NextScenename);
          prefab.GetComponent<Startup000121FunctionsScrollParts>().SetData(infos[i], (NGMenuBase) startup000121Menu);
          startup000121Menu.ScrollFunctionPartList.Add(prefab.GetComponent<Startup000121FunctionsScrollParts>());
          break;
      }
      prefab = (GameObject) null;
    }
  }

  public IEnumerator CheckNew()
  {
    Startup000121Menu startup000121Menu = this;
    startup000121Menu.isNews = false;
    startup000121Menu.isEvent = false;
    startup000121Menu.isFunction = false;
    IEnumerator e = ServerTime.WaitSync();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    // ISSUE: reference to a compiler-generated method
    startup000121Menu.ScrollNewsPartList.ForEach(new System.Action<Startup000121NewsScrollParts>(startup000121Menu.\u003CCheckNew\u003Eb__44_0));
    // ISSUE: reference to a compiler-generated method
    startup000121Menu.ScrollEventPartList.ForEach(new System.Action<Startup000121EventsScrollParts>(startup000121Menu.\u003CCheckNew\u003Eb__44_1));
    // ISSUE: reference to a compiler-generated method
    startup000121Menu.ScrollFunctionPartList.ForEach(new System.Action<Startup000121FunctionsScrollParts>(startup000121Menu.\u003CCheckNew\u003Eb__44_2));
    Singleton<NGSoundManager>.GetInstance().stopVoice(-1);
    switch (startup000121Menu.selectContainer)
    {
      case 0:
        if (!startup000121Menu.isNews)
          break;
        Singleton<NGSoundManager>.GetInstance().playVoiceByStringID("VO_9999", startup000121Menu.scrollVoice[0], -1, 0.0f, (string) null);
        break;
      case 1:
        if (!startup000121Menu.isEvent)
          break;
        Singleton<NGSoundManager>.GetInstance().playVoiceByStringID("VO_9999", startup000121Menu.scrollVoice[1], -1, 0.0f, (string) null);
        break;
      case 2:
        if (!startup000121Menu.isFunction)
          break;
        Singleton<NGSoundManager>.GetInstance().playVoiceByStringID("VO_9999", startup000121Menu.scrollVoice[2], -1, 0.0f, (string) null);
        break;
    }
  }

  public void SetScrolls()
  {
    for (int index = 0; index < this.scrolls.Length; ++index)
      this.scrolls[index].ResolvePosition();
  }
}
