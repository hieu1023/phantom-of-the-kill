﻿// Decompiled with JetBrains decompiler
// Type: Shop007222Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using UnityEngine;

public class Shop007222Menu : BackButtonMenuBase
{
  [SerializeField]
  private UILabel txtPrice;
  [SerializeField]
  private UILabel txtProductName;
  [SerializeField]
  private UILabel txtCompensation;
  [SerializeField]
  private GameObject dynHime;
  private Shop00722Menu m_menu;
  private PlayerShopArticle m_article;

  public void Init(PlayerShopArticle article, Shop00722Menu menu)
  {
    this.m_menu = menu;
    this.m_article = article;
    this.txtCompensation.gameObject.SetActive(false);
    if (article.article.pay_type == CommonPayType.coin || article.article.pay_type == CommonPayType.paid_coin)
    {
      this.dynHime.SetActive(true);
      this.txtPrice.SetTextLocalize(Consts.Format(Consts.GetInstance().SHOP_00721_TXT_PRICE_COIN, (IDictionary) new Hashtable()
      {
        {
          (object) "money",
          (object) article.article.price
        }
      }));
      if (article.article.pay_type == CommonPayType.paid_coin)
        this.txtCompensation.gameObject.SetActive(true);
    }
    else if (article.article.pay_type == CommonPayType.currency)
    {
      this.dynHime.SetActive(false);
      this.txtPrice.SetTextLocalize(Consts.Format(Consts.GetInstance().SHOP_00721_TXT_PRICE, (IDictionary) new Hashtable()
      {
        {
          (object) "money",
          (object) article.article.price
        }
      }));
    }
    else
    {
      this.dynHime.SetActive(false);
      this.txtPrice.SetTextLocalize(Consts.Format(Consts.GetInstance().SHOP_00721_TXT_PRICE_COIN, (IDictionary) new Hashtable()
      {
        {
          (object) "money",
          (object) article.article.price
        }
      }));
    }
    this.txtProductName.SetTextLocalize(article.article.name);
  }

  public void IbtnPopupYes()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<PopupManager>.GetInstance().dismiss(true);
    Player player = SMManager.Get<Player>();
    switch (this.m_article.article.pay_type)
    {
      case CommonPayType.coin:
        if (!player.CheckKiseki(this.m_article.article.price))
        {
          this.StartCoroutine(PopupUtility._999_3_1(Consts.GetInstance().SHOP_99931_TXT_DESCRIPTION, "", "", Gacha99931Menu.PaymentType.ALL));
          break;
        }
        this.StartCoroutine(this.ShopBuy());
        break;
      case CommonPayType.paid_coin:
        if (!player.CheckCompensationKiseki(this.m_article.article.price))
        {
          this.StartCoroutine(PopupUtility._999_3_1(Consts.GetInstance().SHOP_99931_TXT_DESCRIPTION_COMPENSATION, "", "", Gacha99931Menu.PaymentType.Compensation));
          break;
        }
        this.StartCoroutine(this.ShopBuy());
        break;
      default:
        this.StartCoroutine(this.ShopBuy());
        break;
    }
  }

  private IEnumerator ShopBuy()
  {
    Shop007222Menu shop007222Menu = this;
    Singleton<CommonRoot>.GetInstance().loadingMode = 1;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    // ISSUE: reference to a compiler-generated method
    Future<WebAPI.Response.ShopBuy> paramF = WebAPI.ShopBuy(shop007222Menu.m_article.article.ID, 1, new System.Action<WebAPI.Response.UserError>(shop007222Menu.\u003CShopBuy\u003Eb__8_0)).Then<WebAPI.Response.ShopBuy>((Func<WebAPI.Response.ShopBuy, WebAPI.Response.ShopBuy>) (result =>
    {
      Singleton<NGGameDataManager>.GetInstance().Parse(result);
      return result;
    }));
    IEnumerator e = paramF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if (paramF.Result != null)
    {
      e = OnDemandDownload.WaitLoadHasUnitResource(false, false);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      string str = "KISEKI";
      EventTracker.TrackSpend("COIN", str + "_SHOP_ID_" + (object) shop007222Menu.m_article.article.ID, shop007222Menu.m_article.article.price);
      EventTracker.TrackEvent("SHOP", str + "_SHOP", "ID_" + (object) shop007222Menu.m_article.article.ID, 1);
      Singleton<CommonRoot>.GetInstance().isLoading = false;
      Singleton<CommonRoot>.GetInstance().loadingMode = 0;
      Singleton<PopupManager>.GetInstance().onDismiss();
      Future<GameObject> prefab = Res.Prefabs.popup.popup_007_22_3__anim_popup01.Load<GameObject>();
      e = prefab.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      Singleton<PopupManager>.GetInstance().open(prefab.Result, false, false, false, true, false, false, "SE_1006").GetComponent<Shop007223Menu>().Init(shop007222Menu.m_article, shop007222Menu.m_menu, (uint) paramF.Result.player_presents.Length > 0U);
    }
  }

  public void IbtnNo()
  {
    Singleton<PopupManager>.GetInstance().onDismiss();
  }

  public override void onBackButton()
  {
    this.IbtnNo();
  }
}
