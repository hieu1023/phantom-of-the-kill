﻿// Decompiled with JetBrains decompiler
// Type: Popup0228Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System;
using System.Collections;

public class Popup0228Menu : NGBattleMenuBase
{
  private void rebirth()
  {
    BattleTimeManager manager = this.battleManager.getManager<BattleTimeManager>();
    this.env.rebirthUnits(this.env.core.playerUnits.value, manager);
    manager.setPhaseState(BL.Phase.turn_initialize, false);
    manager.setScheduleAction((System.Action) (() =>
    {
      ++this.env.core.continueCount;
      this.battleManager.saveEnvironment(false);
      this.battleManager.StartCoroutine(this.SendContinueCount(this.env.core.continueCount));
    }), 0.0f, (System.Action) null, (Func<bool>) null, false);
  }

  public void IbtnYes()
  {
    if (this.env.core.continueCount < this.env.core.battleInfo.Coin)
    {
      this.rebirth();
      this.battleManager.popupDismiss(false, false);
    }
    else
    {
      if (this.IsPushAndSet())
        return;
      this.StopCoroutine(this.OpenItemList());
      this.StartCoroutine(this.OpenItemList());
    }
  }

  private IEnumerator OpenItemList()
  {
    Popup0228Menu popup0228Menu = this;
    IEnumerator e = PurchaseBehavior.OpenItemList(true);
    while (e.MoveNext())
    {
      if (PurchaseBehavior.IsOpen)
        popup0228Menu.IsPush = false;
      yield return e.Current;
    }
    e = (IEnumerator) null;
    popup0228Menu.IsPush = false;
  }

  public void IbtnNo()
  {
    if (this.IsPushAndSet())
      return;
    this.battleManager.getManager<BattleTimeManager>().setPhaseState(BL.Phase.gameover, true);
    this.battleManager.popupDismiss(false, false);
    this.StopCoroutine(this.OpenItemList());
  }

  private IEnumerator SendContinueCount(int count)
  {
    IEnumerator e = WebAPI.SilentBattleContinueCount(count, (System.Action<WebAPI.Response.UserError>) null).Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }
}
