﻿// Decompiled with JetBrains decompiler
// Type: Story00912Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;
using UnityEngine;

public class Story00912Scene : NGSceneBase
{
  [SerializeField]
  private Story00912Menu menu;

  public IEnumerator onStartSceneAsync()
  {
    IEnumerator e = this.onStartSceneAsync(100111, 100111, 100111, 910011321);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public IEnumerator onStartSceneAsync(int id, int id2, int id3, int questSId)
  {
    Story00912Scene story00912Scene = this;
    Future<GameObject> bgF = Res.Prefabs.BackGround.GachaUnitBackground.Load<GameObject>();
    IEnumerator e = bgF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    story00912Scene.backgroundPrefab = bgF.Result;
    e = story00912Scene.menu.Init(id, id2, id3, questSId);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    story00912Scene.menu.ScrollContainerResolvePosition();
  }
}
