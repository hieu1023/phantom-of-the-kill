﻿// Decompiled with JetBrains decompiler
// Type: Guild028116Popup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using UnityEngine;

public class Guild028116Popup : BackButtonMenuBase
{
  private GuildInfoPopup guildPopupInfo;
  private GuildRegistration guildRegistration;
  private GuildDirectory guildDirectory;
  [SerializeField]
  private UILabel popupTitle;
  [SerializeField]
  private UILabel popupDesc;
  [SerializeField]
  private UILabel guildName;
  [SerializeField]
  private UILabel guildTitle;
  [SerializeField]
  private UI2DSprite guildTitleImage;
  private bool changeGuild;

  public void Initialize(GuildRegistration guild, GuildInfoPopup popup, bool fromChangeGuild = false)
  {
    this.changeGuild = fromChangeGuild;
    this.guildDirectory = (GuildDirectory) null;
    this.guildRegistration = guild;
    this.Initialize(this.guildRegistration.guild_name, popup);
    this.StartCoroutine(this.SetGuildData(guild.appearance));
  }

  public void Initialize(GuildDirectory guild, GuildInfoPopup popup, bool fromChangeGuild = false)
  {
    this.changeGuild = fromChangeGuild;
    this.guildRegistration = (GuildRegistration) null;
    this.guildDirectory = guild;
    this.Initialize(this.guildDirectory.guild_name, popup);
    this.StartCoroutine(this.SetGuildData(guild.appearance));
  }

  private void Initialize(string name, GuildInfoPopup popup)
  {
    this.guildPopupInfo = popup;
    this.guildName.SetTextLocalize(name);
    this.popupTitle.SetTextLocalize(Consts.Format(Consts.GetInstance().POPUP_028_1_1_6_TITLE, (IDictionary) null));
    this.popupDesc.SetTextLocalize(Consts.Format(Consts.GetInstance().POPUP_028_1_1_6_DESC, (IDictionary) null));
  }

  private IEnumerator SetGuildData(GuildAppearance data)
  {
    Future<UnityEngine.Sprite> futureGuildTitleImage = EmblemUtility.LoadGuildEmblemSprite(data._current_emblem);
    IEnumerator e = futureGuildTitleImage.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.guildTitleImage.sprite2D = futureGuildTitleImage.Result;
    if (EmblemUtility.GuildEnblemData(data._current_emblem) != null)
      this.guildTitle.SetTextLocalize(EmblemUtility.GuildEnblemData(data._current_emblem).name);
    else
      this.guildTitle.SetTextLocalize(string.Empty);
  }

  public override void onBackButton()
  {
    Singleton<PopupManager>.GetInstance().dismiss(false);
  }

  public void onButtonYes()
  {
    this.StartCoroutine(this.SendCancelRequest());
  }

  private IEnumerator SendCancelRequest()
  {
    Singleton<PopupManager>.GetInstance().closeAllWithoutAnim(false);
    while (Singleton<PopupManager>.GetInstance().isOpenNoFinish)
      yield return (object) null;
    Singleton<CommonRoot>.GetInstance().loadingMode = 1;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    bool applied = false;
    bool noApplicant = false;
    bool maintenance = false;
    string target_guild_id = PlayerAffiliation.Current.guild_id;
    if (PlayerAffiliation.Current.status == GuildMembershipStatus.applicant && PlayerAffiliation.Current.guild_id == null || this.changeGuild)
    {
      this.changeGuild = true;
      target_guild_id = PlayerAffiliation.Current.applicant_guild_id;
    }
    Future<WebAPI.Response.GuildApplicantsCancel> ft = WebAPI.GuildApplicantsCancel(target_guild_id, (System.Action<WebAPI.Response.UserError>) (e =>
    {
      if (e.Code.Equals("GLD001"))
        applied = true;
      else if (e.Code.Equals("GLD006"))
      {
        noApplicant = true;
      }
      else
      {
        Singleton<CommonRoot>.GetInstance().isLoading = false;
        Singleton<CommonRoot>.GetInstance().loadingMode = 0;
        WebAPI.DefaultUserErrorCallback(e);
        if (!e.Code.Equals("GLD014"))
          return;
        maintenance = true;
      }
    }));
    IEnumerator e1 = ft.Wait();
    while (e1.MoveNext())
      yield return e1.Current;
    e1 = (IEnumerator) null;
    if (maintenance)
    {
      Singleton<CommonRoot>.GetInstance().isLoading = true;
      Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
    }
    else if (ft.Result != null || noApplicant || applied)
    {
      Singleton<CommonRoot>.GetInstance().isLoading = false;
      Singleton<CommonRoot>.GetInstance().loadingMode = 0;
      if (applied)
      {
        Singleton<PopupManager>.GetInstance().dismiss(false);
        Future<GameObject> fObj = Res.Prefabs.popup.popup_028_guild_common_ok__anim_popup01.Load<GameObject>();
        e1 = fObj.Wait();
        while (e1.MoveNext())
          yield return e1.Current;
        e1 = (IEnumerator) null;
        Singleton<PopupManager>.GetInstance().open(fObj.Result, false, false, false, true, false, false, "SE_1006").GetComponent<GuildOkPopup>().Initialize(Consts.Format(Consts.GetInstance().POPUP_028_1_1_6_TITLE, (IDictionary) null), Consts.Format(Consts.GetInstance().POPUP_028_1_1_6_NO_APPLICANT, (IDictionary) null), new Vector2?(), (System.Action) (() =>
        {
          Singleton<CommonRoot>.GetInstance().isLoading = true;
          Guild0281Scene.ChangeSceneGuildTop(true, (Guild0281Menu) null, false);
        }));
      }
      else
      {
        if (this.guildPopupInfo.CancelRequeestCallback != null)
          this.guildPopupInfo.CancelRequeestCallback();
        GameObject gameObject = Singleton<PopupManager>.GetInstance().open(this.guildPopupInfo.guildCancelRequestResultPopup, false, false, false, true, false, false, "SE_1006");
        if ((UnityEngine.Object) gameObject.GetComponent<Guild0281161Popup>().GetComponent<UIWidget>() != (UnityEngine.Object) null)
          gameObject.GetComponent<Guild0281161Popup>().GetComponent<UIWidget>().alpha = 0.0f;
        gameObject.GetComponent<Guild0281161Popup>().Initialize(this.changeGuild);
      }
    }
  }
}
