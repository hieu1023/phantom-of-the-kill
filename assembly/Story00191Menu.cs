﻿// Decompiled with JetBrains decompiler
// Type: Story00191Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using DeviceKit;
using GameCore;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Story00191Menu : BackButtonMenuBase
{
  [SerializeField]
  protected UILabel TxtTitle;
  [SerializeField]
  private GameObject BtnCredit;
  [SerializeField]
  private GameObject FriendBadge;
  [SerializeField]
  private GameObject LoginBonusBadge;
  [SerializeField]
  private GameObject LoginBonusMonthly;
  private GameObject modalWindow;
  private GameObject dataDownloadPrefab;

  public virtual void Foreground()
  {
  }

  public virtual void IbtnBase()
  {
  }

  public virtual void IbtnBaseWithoutIcon()
  {
  }

  public virtual void VScrollBar()
  {
  }

  public IEnumerator onInitSceneAsync()
  {
    if ((UnityEngine.Object) this.dataDownloadPrefab == (UnityEngine.Object) null)
    {
      Future<GameObject> popupF = new ResourceObject("Prefabs/popup/popup_001_9_1_VoiceDL__anim_popup01").Load<GameObject>();
      IEnumerator e = popupF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.dataDownloadPrefab = popupF.Result;
      popupF = (Future<GameObject>) null;
    }
  }

  public void InitAsync()
  {
    this.LoginBonusMonthly.SetActive(SMManager.Get<Player>().IsLoginBonusMonthly());
  }

  public void Init()
  {
    this.FriendBadgeSetting();
    this.LoginBonusBadgeSetting();
  }

  private void FriendBadgeSetting()
  {
    if ((UnityEngine.Object) this.FriendBadge == (UnityEngine.Object) null)
      return;
    this.FriendBadge.SetActive(Singleton<NGGameDataManager>.GetInstance().ReceivedFriendRequestCount > 0);
  }

  private void LoginBonusBadgeSetting()
  {
    if ((UnityEngine.Object) this.LoginBonusBadge == (UnityEngine.Object) null)
      return;
    this.LoginBonusBadge.SetActive(Singleton<NGGameDataManager>.GetInstance().hasFillableLoginbonus);
  }

  public virtual void IbtnBack()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<NGSceneManager>.GetInstance().clearStack();
    Singleton<NGSceneManager>.GetInstance().destroyCurrentScene();
    MypageScene.ChangeScene(false, false, false);
  }

  public override void onBackButton()
  {
    this.IbtnBack();
  }

  public void IbtnTitle()
  {
    this.modalWindow = ModalWindow.ShowYesNo(Consts.GetInstance().titleback_title, Consts.GetInstance().titleback_text, (System.Action) (() => StartScript.Restart()), (System.Action) (() => this.DeleteModalWindow())).gameObject;
  }

  private void DeleteModalWindow()
  {
    UnityEngine.Object.Destroy((UnityEngine.Object) this.modalWindow);
  }

  public void IbtnStory()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<NGSceneManager>.GetInstance().changeScene("story009_0", false, (object[]) Array.Empty<object>());
  }

  public void IbtnColosseumTitle()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<NGSceneManager>.GetInstance().changeScene("title024_1", true, (object) SMManager.Get<Player>().id);
  }

  public void IbtnBook()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<CommonRoot>.GetInstance().loadingMode = 4;
    Singleton<NGSceneManager>.GetInstance().changeScene("guide011_1", true, (object[]) Array.Empty<object>());
  }

  public void IbtnFriend()
  {
    if (this.IsPushAndSet())
      return;
    Friend0081Scene.ChangeScene(true);
  }

  public void IbtnLoginBonus()
  {
    if (this.IsPushAndSet())
      return;
    Startup000LoginBonusConfirmScene.changeScene(true);
  }

  public void IbtnOption()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<NGSceneManager>.GetInstance().changeScene("setting010_1", true, (object[]) Array.Empty<object>());
  }

  public void IbtnTransfer()
  {
    if (this.IsPushAndSet())
      return;
    Transfer01272Scene.ChangeScene(true);
  }

  public void IbtnHelp()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<NGSceneManager>.GetInstance().changeScene("help015_1", true, (object[]) Array.Empty<object>());
  }

  public void IbtnBeginnerNavi()
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("help015_5", true, (object[]) Array.Empty<object>());
  }

  public void IbtnPurchase()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<NGSceneManager>.GetInstance().changeScene("purchase016_2", true, (object[]) Array.Empty<object>());
  }

  private void ShowAchievementsUI()
  {
    Singleton<SocialManager>.GetInstance().ShowAchievementsUI();
  }

  public void IbtnAchievements()
  {
    this.ShowAchievementsUI();
  }

  public void IbtnUsepolicy()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<NGSceneManager>.GetInstance().changeScene("mypage001_17", true, (object[]) Array.Empty<object>());
  }

  public void IbtnCopyright()
  {
    if (this.IsPushAndSet())
      return;
    Mypage00113Scene.changeScene(true);
  }

  public void IbtnCredit()
  {
  }

  public void IbtnBulkDownload()
  {
    if (this.IsPushAndSet())
      return;
    this.StartCoroutine(this.bulkDownLoadCheck());
  }

  public void showBtnCredit(bool isShow)
  {
    this.BtnCredit.SetActive(isShow);
  }

  public void IbtnOffisialsite()
  {
    Debug.Log((object) "Go to the offical site.");
    App.OpenUrl(Consts.GetInstance().OFFICAL_SITE_URL);
  }

  private IEnumerator bulkDownLoadCheck()
  {
    Story00191Menu story00191Menu = this;
    CommonRoot common = Singleton<CommonRoot>.GetInstance();
    common.isLoading = true;
    yield return (object) new WaitForSeconds(0.5f);
    long requiredSize = OnDemandDownload.SizeOfLoadAllUnits();
    common.isLoading = false;
    Consts instance = Consts.GetInstance();
    if (requiredSize > 0L)
    {
      // ISSUE: reference to a compiler-generated method
      PopupCommonYesNo.Show(instance.bulk_download_title, instance.bulkDownloadText(requiredSize), new System.Action(story00191Menu.\u003CbulkDownLoadCheck\u003Eb__38_0), (System.Action) (() => {}));
    }
    else
      story00191Menu.StartCoroutine(PopupCommon.Show(instance.bulk_download_title, instance.bulk_downloaded_text, (System.Action) (() => {})));
  }

  private IEnumerator doBulkDownload()
  {
    yield return (object) new WaitForSeconds(0.5f);
    CommonRoot common = Singleton<CommonRoot>.GetInstance();
    common.isLoading = true;
    yield return (object) new WaitForSeconds(0.5f);
    Debug.Log((object) "start bulk download");
    IEnumerator e = OnDemandDownload.WaitLoadAllUnits(false);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    common.isLoading = false;
  }

  public void IbtnAsct()
  {
    if (this.IsPushAndSet())
      return;
    this.StartCoroutine(PopupUtility._007_18());
  }

  public void IbtnAs()
  {
    if (this.IsPushAndSet())
      return;
    this.StartCoroutine(PopupUtility._007_19());
  }

  public void IbtnBlacklist()
  {
  }

  public void IbtnInviteFriend()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<NGSceneManager>.GetInstance().changeScene("story009_12", true, (object[]) Array.Empty<object>());
  }

  public void IbtnLobi()
  {
    App.OpenUrl(Consts.GetInstance().LOBI_OPEN_WEBPAGE_SCHEME);
  }

  public void IbtnHintsAndTips()
  {
  }

  public void IbtnDataFix()
  {
    if (this.IsPush)
      return;
    Singleton<NGGameDataManager>.GetInstance().isCallHomeUpdateAllData = true;
    Singleton<NGGameDataManager>.GetInstance().refreshHomeHome = true;
    Singleton<CommonRoot>.GetInstance().loadingMode = 4;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    Singleton<NGGameDataManager>.GetInstance().StartSceneAsyncProxy((System.Action<NGGameDataManager.StartSceneProxyResult>) (_ =>
    {
      Singleton<CommonRoot>.GetInstance().isLoading = false;
      Singleton<CommonRoot>.GetInstance().loadingMode = 0;
    }));
  }

  public void IbtnDetaDownload()
  {
    if (this.IsPush)
      return;
    GameObject prefab = this.dataDownloadPrefab.Clone(Singleton<CommonRoot>.GetInstance().LoadTmpObj.transform);
    prefab.GetComponent<Popup00191DataDownloadMenu>().Init(this);
    Singleton<PopupManager>.GetInstance().open(prefab, false, false, true, true, false, false, "SE_1006");
  }

  public IEnumerator DownloadContents(List<string> paths, bool fileCheckDisable = false)
  {
    IEnumerator e = OnDemandDownload.waitLoadSomethingResource((IEnumerable<string>) paths, false, fileCheckDisable);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public IEnumerator DeleteContents(List<string> fileNames)
  {
    IEnumerator e = ResourceDownloader.DeleteContents(fileNames);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }
}
