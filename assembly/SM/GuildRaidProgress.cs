﻿// Decompiled with JetBrains decompiler
// Type: SM.GuildRaidProgress
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace SM
{
  [Serializable]
  public class GuildRaidProgress : KeyCompare
  {
    public int loop_count;
    public int boss_total_damage;
    public int quest_s_id;
    public int order;

    public GuildRaidProgress()
    {
    }

    public GuildRaidProgress(Dictionary<string, object> json)
    {
      this._hasKey = false;
      this.loop_count = (int) (long) json[nameof (loop_count)];
      this.boss_total_damage = (int) (long) json[nameof (boss_total_damage)];
      this.quest_s_id = (int) (long) json[nameof (quest_s_id)];
      this.order = (int) (long) json[nameof (order)];
    }
  }
}
