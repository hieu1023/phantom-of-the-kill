﻿// Decompiled with JetBrains decompiler
// Type: SM.BattleEndGet_sea_album_piece_counts
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace SM
{
  [Serializable]
  public class BattleEndGet_sea_album_piece_counts : KeyCompare
  {
    public int album_piece_id;
    public int count;

    public BattleEndGet_sea_album_piece_counts()
    {
    }

    public BattleEndGet_sea_album_piece_counts(Dictionary<string, object> json)
    {
      this._hasKey = false;
      this.album_piece_id = (int) (long) json[nameof (album_piece_id)];
      this.count = (int) (long) json[nameof (count)];
    }
  }
}
