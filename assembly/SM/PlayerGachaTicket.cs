﻿// Decompiled with JetBrains decompiler
// Type: SM.PlayerGachaTicket
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using System;
using System.Collections.Generic;

namespace SM
{
  [Serializable]
  public class PlayerGachaTicket : KeyCompare
  {
    public int ticket_id;
    public int quantity;

    public GachaTicket ticket
    {
      get
      {
        return MasterData.GachaTicket[this.ticket_id];
      }
    }

    public PlayerGachaTicket()
    {
    }

    public PlayerGachaTicket(Dictionary<string, object> json)
    {
      this._hasKey = false;
      this.ticket_id = (int) (long) json[nameof (ticket_id)];
      this.quantity = (int) (long) json[nameof (quantity)];
    }
  }
}
