﻿// Decompiled with JetBrains decompiler
// Type: SM.PlayerGameKit2AchievementResult
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace SM
{
  [Serializable]
  public class PlayerGameKit2AchievementResult : KeyCompare
  {
    public string achievement_id;
    public float progress;

    public PlayerGameKit2AchievementResult()
    {
    }

    public PlayerGameKit2AchievementResult(Dictionary<string, object> json)
    {
      this._hasKey = false;
      this.achievement_id = (string) json[nameof (achievement_id)];
      this.progress = (float) (double) json[nameof (progress)];
    }
  }
}
