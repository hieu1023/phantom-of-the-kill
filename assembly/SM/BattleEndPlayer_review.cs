﻿// Decompiled with JetBrains decompiler
// Type: SM.BattleEndPlayer_review
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace SM
{
  [Serializable]
  public class BattleEndPlayer_review : KeyCompare
  {
    public string message;
    public string id;
    public string title;

    public BattleEndPlayer_review()
    {
    }

    public BattleEndPlayer_review(Dictionary<string, object> json)
    {
      this._hasKey = false;
      this.message = (string) json[nameof (message)];
      this.id = (string) json[nameof (id)];
      this.title = (string) json[nameof (title)];
    }
  }
}
