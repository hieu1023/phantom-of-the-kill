﻿// Decompiled with JetBrains decompiler
// Type: SM.GvgStage
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace SM
{
  [Serializable]
  public class GvgStage : KeyCompare
  {
    public int turns;
    public int stage_id;
    public int annihilation_point;
    public int point;

    public GvgStage()
    {
    }

    public GvgStage(Dictionary<string, object> json)
    {
      this._hasKey = false;
      this.turns = (int) (long) json[nameof (turns)];
      this.stage_id = (int) (long) json[nameof (stage_id)];
      this.annihilation_point = (int) (long) json[nameof (annihilation_point)];
      this.point = (int) (long) json[nameof (point)];
    }
  }
}
