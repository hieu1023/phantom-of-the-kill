﻿// Decompiled with JetBrains decompiler
// Type: SM.PlayerUnitTransmigrateMemory
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UniLinq;

namespace SM
{
  [Serializable]
  public class PlayerUnitTransmigrateMemory : KeyCompare
  {
    public int?[] player_unit_ids;

    public PlayerUnitTransmigrateMemory()
    {
    }

    public PlayerUnitTransmigrateMemory(Dictionary<string, object> json)
    {
      this._hasKey = false;
      this.player_unit_ids = ((IEnumerable<object>) json[nameof (player_unit_ids)]).Select<object, int?>((Func<object, int?>) (s =>
      {
        long? nullable = (long?) s;
        return !nullable.HasValue ? new int?() : new int?((int) nullable.GetValueOrDefault());
      })).ToArray<int?>();
    }
  }
}
