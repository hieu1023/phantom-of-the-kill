﻿// Decompiled with JetBrains decompiler
// Type: SM.PlayerAlbum
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace SM
{
  [Serializable]
  public class PlayerAlbum : KeyCompare
  {
    public bool is_end;
    public PlayerAlbumPiece[] player_album_piece;
    public int id;
    public int album_id;

    public PlayerAlbum()
    {
    }

    public PlayerAlbum(Dictionary<string, object> json)
    {
      this._hasKey = false;
      this.is_end = (bool) json[nameof (is_end)];
      List<PlayerAlbumPiece> playerAlbumPieceList = new List<PlayerAlbumPiece>();
      foreach (object obj in (List<object>) json[nameof (player_album_piece)])
        playerAlbumPieceList.Add(obj == null ? (PlayerAlbumPiece) null : new PlayerAlbumPiece((Dictionary<string, object>) obj));
      this.player_album_piece = playerAlbumPieceList.ToArray();
      this.id = (int) (long) json[nameof (id)];
      this.album_id = (int) (long) json[nameof (album_id)];
    }
  }
}
