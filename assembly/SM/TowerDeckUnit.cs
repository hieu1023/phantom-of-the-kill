﻿// Decompiled with JetBrains decompiler
// Type: SM.TowerDeckUnit
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace SM
{
  [Serializable]
  public class TowerDeckUnit : KeyCompare
  {
    public int position_id;
    public int player_unit_id;

    public TowerDeckUnit()
    {
    }

    public TowerDeckUnit(Dictionary<string, object> json)
    {
      this._hasKey = false;
      this.position_id = (int) (long) json[nameof (position_id)];
      this.player_unit_id = (int) (long) json[nameof (player_unit_id)];
    }
  }
}
