﻿// Decompiled with JetBrains decompiler
// Type: SM.PvPCampaign
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace SM
{
  [Serializable]
  public class PvPCampaign : KeyCompare
  {
    public string image_url;
    public string button_image_url;
    public Campaign campaign;

    public PvPCampaign()
    {
    }

    public PvPCampaign(Dictionary<string, object> json)
    {
      this._hasKey = false;
      this.image_url = (string) json[nameof (image_url)];
      this.button_image_url = (string) json[nameof (button_image_url)];
      this.campaign = json[nameof (campaign)] == null ? (Campaign) null : new Campaign((Dictionary<string, object>) json[nameof (campaign)]);
    }
  }
}
