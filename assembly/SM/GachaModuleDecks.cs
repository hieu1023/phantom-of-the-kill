﻿// Decompiled with JetBrains decompiler
// Type: SM.GachaModuleDecks
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace SM
{
  [Serializable]
  public class GachaModuleDecks : KeyCompare
  {
    public GachaModuleDecksProbabilities[] probabilities;
    public GachaModuleDecksEntities[] entities;
    public int id;

    public GachaModuleDecks()
    {
    }

    public GachaModuleDecks(Dictionary<string, object> json)
    {
      this._hasKey = false;
      List<GachaModuleDecksProbabilities> decksProbabilitiesList = new List<GachaModuleDecksProbabilities>();
      foreach (object obj in (List<object>) json[nameof (probabilities)])
        decksProbabilitiesList.Add(obj == null ? (GachaModuleDecksProbabilities) null : new GachaModuleDecksProbabilities((Dictionary<string, object>) obj));
      this.probabilities = decksProbabilitiesList.ToArray();
      List<GachaModuleDecksEntities> moduleDecksEntitiesList = new List<GachaModuleDecksEntities>();
      foreach (object obj in (List<object>) json[nameof (entities)])
        moduleDecksEntitiesList.Add(obj == null ? (GachaModuleDecksEntities) null : new GachaModuleDecksEntities((Dictionary<string, object>) obj));
      this.entities = moduleDecksEntitiesList.ToArray();
      this.id = (int) (long) json[nameof (id)];
    }
  }
}
