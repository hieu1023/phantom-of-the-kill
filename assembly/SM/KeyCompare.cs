﻿// Decompiled with JetBrains decompiler
// Type: SM.KeyCompare
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace SM
{
  [Serializable]
  public class KeyCompare
  {
    protected bool _hasKey;
    protected object _key;

    public bool hasKey
    {
      get
      {
        return this._hasKey;
      }
    }

    public object Key
    {
      get
      {
        return this._key;
      }
    }
  }
}
