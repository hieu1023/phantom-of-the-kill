﻿// Decompiled with JetBrains decompiler
// Type: SM.PlayerGuildTown
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using System;
using System.Collections.Generic;

namespace SM
{
  [Serializable]
  public class PlayerGuildTown : KeyCompare
  {
    public int _master;
    public int id;

    public MapTown master
    {
      get
      {
        if (MasterData.MapTown.ContainsKey(this._master))
          return MasterData.MapTown[this._master];
        Debug.LogError((object) ("Key not Found: MasterData.MapTown[" + (object) this._master + "]"));
        return (MapTown) null;
      }
    }

    public PlayerGuildTown()
    {
    }

    public PlayerGuildTown(Dictionary<string, object> json)
    {
      this._hasKey = true;
      this._master = (int) (long) json[nameof (master)];
      this._key = (object) (this.id = (int) (long) json[nameof (id)]);
    }
  }
}
