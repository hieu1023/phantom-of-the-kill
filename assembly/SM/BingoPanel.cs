﻿// Decompiled with JetBrains decompiler
// Type: SM.BingoPanel
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace SM
{
  [Serializable]
  public class BingoPanel : KeyCompare
  {
    public int panel_id;
    public int reward_group_id;
    public int id;
    public string name;
    public int bingo_id;

    public BingoPanel()
    {
    }

    public BingoPanel(Dictionary<string, object> json)
    {
      this._hasKey = false;
      this.panel_id = (int) (long) json[nameof (panel_id)];
      this.reward_group_id = (int) (long) json[nameof (reward_group_id)];
      this.id = (int) (long) json[nameof (id)];
      this.name = (string) json[nameof (name)];
      this.bingo_id = (int) (long) json[nameof (bingo_id)];
    }
  }
}
