﻿// Decompiled with JetBrains decompiler
// Type: SM.WeeklyPack
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace SM
{
  [Serializable]
  public class WeeklyPack : KeyCompare
  {
    public int coin_group_id;
    public int purchase_limit;
    public int id;
    public string name;
    public DateTime? end_at;

    public WeeklyPack()
    {
    }

    public WeeklyPack(Dictionary<string, object> json)
    {
      this._hasKey = false;
      this.coin_group_id = (int) (long) json[nameof (coin_group_id)];
      this.purchase_limit = (int) (long) json[nameof (purchase_limit)];
      this.id = (int) (long) json[nameof (id)];
      this.name = (string) json[nameof (name)];
      this.end_at = json[nameof (end_at)] == null ? new DateTime?() : new DateTime?(DateTime.Parse((string) json[nameof (end_at)]));
    }
  }
}
