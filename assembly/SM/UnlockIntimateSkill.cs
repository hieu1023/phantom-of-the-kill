﻿// Decompiled with JetBrains decompiler
// Type: SM.UnlockIntimateSkill
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace SM
{
  [Serializable]
  public class UnlockIntimateSkill : KeyCompare
  {
    public int skill_id;
    public int unit_id;

    public UnlockIntimateSkill()
    {
    }

    public UnlockIntimateSkill(Dictionary<string, object> json)
    {
      this._hasKey = false;
      this.skill_id = (int) (long) json[nameof (skill_id)];
      this.unit_id = (int) (long) json[nameof (unit_id)];
    }
  }
}
