﻿// Decompiled with JetBrains decompiler
// Type: SM.GachaModuleNewentity
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace SM
{
  [Serializable]
  public class GachaModuleNewentity : KeyCompare
  {
    public int reward_quantity;
    public int view_priority;
    public int id;
    public int reward_id;
    public int reward_type_id;

    public GachaModuleNewentity()
    {
    }

    public GachaModuleNewentity(Dictionary<string, object> json)
    {
      this._hasKey = false;
      this.reward_quantity = (int) (long) json[nameof (reward_quantity)];
      this.view_priority = (int) (long) json[nameof (view_priority)];
      this.id = (int) (long) json[nameof (id)];
      this.reward_id = (int) (long) json[nameof (reward_id)];
      this.reward_type_id = (int) (long) json[nameof (reward_type_id)];
    }
  }
}
