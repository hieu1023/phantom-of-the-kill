﻿// Decompiled with JetBrains decompiler
// Type: SM.RouletteModule
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace SM
{
  [Serializable]
  public class RouletteModule : KeyCompare
  {
    public RouletteModuleRoulette[] roulette;

    public RouletteModule()
    {
    }

    public RouletteModule(Dictionary<string, object> json)
    {
      this._hasKey = false;
      List<RouletteModuleRoulette> rouletteModuleRouletteList = new List<RouletteModuleRoulette>();
      foreach (object obj in (List<object>) json[nameof (roulette)])
        rouletteModuleRouletteList.Add(obj == null ? (RouletteModuleRoulette) null : new RouletteModuleRoulette((Dictionary<string, object>) obj));
      this.roulette = rouletteModuleRouletteList.ToArray();
    }
  }
}
