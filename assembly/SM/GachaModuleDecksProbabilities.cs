﻿// Decompiled with JetBrains decompiler
// Type: SM.GachaModuleDecksProbabilities
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace SM
{
  [Serializable]
  public class GachaModuleDecksProbabilities : KeyCompare
  {
    public int rarity_id;
    public float probability;

    public GachaModuleDecksProbabilities()
    {
    }

    public GachaModuleDecksProbabilities(Dictionary<string, object> json)
    {
      this._hasKey = false;
      this.rarity_id = (int) (long) json[nameof (rarity_id)];
      this.probability = (float) (double) json[nameof (probability)];
    }
  }
}
