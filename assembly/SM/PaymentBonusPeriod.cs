﻿// Decompiled with JetBrains decompiler
// Type: SM.PaymentBonusPeriod
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace SM
{
  [Serializable]
  public class PaymentBonusPeriod : KeyCompare
  {
    public DateTime start_at;
    public int id;
    public DateTime end_at;

    public PaymentBonusPeriod()
    {
    }

    public PaymentBonusPeriod(Dictionary<string, object> json)
    {
      this._hasKey = false;
      this.start_at = DateTime.Parse((string) json[nameof (start_at)]);
      this.id = (int) (long) json[nameof (id)];
      this.end_at = DateTime.Parse((string) json[nameof (end_at)]);
    }
  }
}
