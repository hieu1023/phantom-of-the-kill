﻿// Decompiled with JetBrains decompiler
// Type: SM.PlayerQuestScoreExtraS
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace SM
{
  [Serializable]
  public class PlayerQuestScoreExtraS : KeyCompare
  {
    public int quest_extra_s;
    public int score_max;

    public PlayerQuestScoreExtraS()
    {
    }

    public PlayerQuestScoreExtraS(Dictionary<string, object> json)
    {
      this._hasKey = false;
      this.quest_extra_s = (int) (long) json[nameof (quest_extra_s)];
      this.score_max = (int) (long) json[nameof (score_max)];
    }
  }
}
