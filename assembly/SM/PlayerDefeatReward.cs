﻿// Decompiled with JetBrains decompiler
// Type: SM.PlayerDefeatReward
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace SM
{
  [Serializable]
  public class PlayerDefeatReward : KeyCompare
  {
    public int defeat_reward_id;

    public PlayerDefeatReward()
    {
    }

    public PlayerDefeatReward(Dictionary<string, object> json)
    {
      this._hasKey = false;
      this.defeat_reward_id = (int) (long) json[nameof (defeat_reward_id)];
    }
  }
}
