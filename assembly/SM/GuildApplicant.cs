﻿// Decompiled with JetBrains decompiler
// Type: SM.GuildApplicant
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace SM
{
  [Serializable]
  public class GuildApplicant : KeyCompare
  {
    public GuildPlayerInfo player;
    public DateTime applied_at;

    public GuildApplicant()
    {
    }

    public GuildApplicant(Dictionary<string, object> json)
    {
      this._hasKey = false;
      this.player = json[nameof (player)] == null ? (GuildPlayerInfo) null : new GuildPlayerInfo((Dictionary<string, object>) json[nameof (player)]);
      this.applied_at = DateTime.Parse((string) json[nameof (applied_at)]);
    }
  }
}
