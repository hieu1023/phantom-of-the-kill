﻿// Decompiled with JetBrains decompiler
// Type: SM.PlayerGearReisouSchema
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace SM
{
  [Serializable]
  public class PlayerGearReisouSchema : KeyCompare
  {
    public int gear_level;
    public int gear_id;
    public int id;

    public PlayerItem getReisouItemForSchema()
    {
      return this.getReisouItemForSchema(this);
    }

    public PlayerItem getReisouItemForSchema(PlayerGearReisouSchema schema)
    {
      return new PlayerItem()
      {
        entity_id = schema.gear_id,
        _entity_type = 3,
        for_battle = true,
        gear_level = schema.gear_level,
        gear_level_limit = 99,
        favorite = false,
        gear_exp_next = 1,
        is_new = false,
        broken = false,
        id = schema.id,
        quantity = 1,
        gear_buildup_param = new PlayerGearBuildupParam()
      };
    }

    public PlayerGearReisouSchema()
    {
    }

    public PlayerGearReisouSchema(Dictionary<string, object> json)
    {
      this._hasKey = false;
      this.gear_level = (int) (long) json[nameof (gear_level)];
      this.gear_id = (int) (long) json[nameof (gear_id)];
      this.id = (int) (long) json[nameof (id)];
    }
  }
}
