﻿// Decompiled with JetBrains decompiler
// Type: SM.StepupPackSet
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace SM
{
  [Serializable]
  public class StepupPackSet : KeyCompare
  {
    public int coin_group_id;
    public int step;
    public string name;

    public StepupPackSet()
    {
    }

    public StepupPackSet(Dictionary<string, object> json)
    {
      this._hasKey = false;
      this.coin_group_id = (int) (long) json[nameof (coin_group_id)];
      this.step = (int) (long) json[nameof (step)];
      this.name = (string) json[nameof (name)];
    }
  }
}
