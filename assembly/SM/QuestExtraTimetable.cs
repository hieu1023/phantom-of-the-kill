﻿// Decompiled with JetBrains decompiler
// Type: SM.QuestExtraTimetable
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UniLinq;

namespace SM
{
  [Serializable]
  public class QuestExtraTimetable : KeyCompare
  {
    public int[] emphasis;
    public QuestExtraTimetableNotice[] notice;

    public QuestExtraTimetable()
    {
    }

    public QuestExtraTimetable(Dictionary<string, object> json)
    {
      this._hasKey = false;
      this.emphasis = ((IEnumerable<object>) json[nameof (emphasis)]).Select<object, int>((Func<object, int>) (s => (int) (long) s)).ToArray<int>();
      List<QuestExtraTimetableNotice> extraTimetableNoticeList = new List<QuestExtraTimetableNotice>();
      foreach (object obj in (List<object>) json[nameof (notice)])
        extraTimetableNoticeList.Add(obj == null ? (QuestExtraTimetableNotice) null : new QuestExtraTimetableNotice((Dictionary<string, object>) obj));
      this.notice = extraTimetableNoticeList.ToArray();
    }
  }
}
