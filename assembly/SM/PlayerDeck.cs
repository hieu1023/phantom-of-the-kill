﻿// Decompiled with JetBrains decompiler
// Type: SM.PlayerDeck
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System;
using System.Collections.Generic;
using UniLinq;

namespace SM
{
  [Serializable]
  public class PlayerDeck : KeyCompare
  {
    public int member_limit;
    public int deck_type_id;
    public int?[] player_unit_ids;
    public int cost_limit;
    public int deck_number;

    public PlayerUnit[] player_units
    {
      get
      {
        Dictionary<int, PlayerUnit> dic = ((IEnumerable<PlayerUnit>) SMManager.Get<PlayerUnit[]>()).ToDictionary<PlayerUnit, int>((Func<PlayerUnit, int>) (unit => unit.id));
        return ((IEnumerable<int?>) this.player_unit_ids).Select<int?, PlayerUnit>((Func<int?, PlayerUnit>) (id => id.HasValue ? dic[id.Value] : (PlayerUnit) null)).ToArray<PlayerUnit>();
      }
    }

    public int total_combat
    {
      get
      {
        return ((IEnumerable<PlayerUnit>) this.player_units).Where<PlayerUnit>((Func<PlayerUnit, bool>) (unit => unit != (PlayerUnit) null)).Sum<PlayerUnit>((Func<PlayerUnit, int>) (unit => Judgement.NonBattleParameter.FromPlayerUnit(unit, false).Combat));
      }
    }

    public int cost
    {
      get
      {
        return ((IEnumerable<PlayerUnit>) this.player_units).Where<PlayerUnit>((Func<PlayerUnit, bool>) (unit => unit != (PlayerUnit) null)).Sum<PlayerUnit>((Func<PlayerUnit, int>) (unit => unit.cost));
      }
    }

    public PlayerDeck()
    {
    }

    public PlayerDeck(Dictionary<string, object> json)
    {
      this._hasKey = false;
      this.member_limit = (int) (long) json[nameof (member_limit)];
      this.deck_type_id = (int) (long) json[nameof (deck_type_id)];
      this.player_unit_ids = ((IEnumerable<object>) json[nameof (player_unit_ids)]).Select<object, int?>((Func<object, int?>) (s =>
      {
        long? nullable = (long?) s;
        return !nullable.HasValue ? new int?() : new int?((int) nullable.GetValueOrDefault());
      })).ToArray<int?>();
      this.cost_limit = (int) (long) json[nameof (cost_limit)];
      this.deck_number = (int) (long) json[nameof (deck_number)];
    }
  }
}
