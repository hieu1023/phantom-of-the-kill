﻿// Decompiled with JetBrains decompiler
// Type: SM.GuildLevelBonus
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace SM
{
  [Serializable]
  public class GuildLevelBonus : KeyCompare
  {
    public int item;
    public int player_exp;
    public int unit_exp;
    public bool campaign_flag;
    public int money;

    public GuildLevelBonus()
    {
    }

    public GuildLevelBonus(Dictionary<string, object> json)
    {
      this._hasKey = false;
      this.item = (int) (long) json[nameof (item)];
      this.player_exp = (int) (long) json[nameof (player_exp)];
      this.unit_exp = (int) (long) json[nameof (unit_exp)];
      this.campaign_flag = (bool) json[nameof (campaign_flag)];
      this.money = (int) (long) json[nameof (money)];
    }
  }
}
