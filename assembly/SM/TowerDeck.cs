﻿// Decompiled with JetBrains decompiler
// Type: SM.TowerDeck
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace SM
{
  [Serializable]
  public class TowerDeck : KeyCompare
  {
    public TowerDeckUnit[] tower_deck_units;

    public TowerDeck()
    {
    }

    public TowerDeck(Dictionary<string, object> json)
    {
      this._hasKey = false;
      List<TowerDeckUnit> towerDeckUnitList = new List<TowerDeckUnit>();
      foreach (object obj in (List<object>) json[nameof (tower_deck_units)])
        towerDeckUnitList.Add(obj == null ? (TowerDeckUnit) null : new TowerDeckUnit((Dictionary<string, object>) obj));
      this.tower_deck_units = towerDeckUnitList.ToArray();
    }
  }
}
