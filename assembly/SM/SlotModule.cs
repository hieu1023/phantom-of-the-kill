﻿// Decompiled with JetBrains decompiler
// Type: SM.SlotModule
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace SM
{
  [Serializable]
  public class SlotModule : KeyCompare
  {
    public SlotModuleSlot[] slot;

    public SlotModule()
    {
    }

    public SlotModule(Dictionary<string, object> json)
    {
      this._hasKey = false;
      List<SlotModuleSlot> slotModuleSlotList = new List<SlotModuleSlot>();
      foreach (object obj in (List<object>) json[nameof (slot)])
        slotModuleSlotList.Add(obj == null ? (SlotModuleSlot) null : new SlotModuleSlot((Dictionary<string, object>) obj));
      this.slot = slotModuleSlotList.ToArray();
    }
  }
}
