﻿// Decompiled with JetBrains decompiler
// Type: SM.PlayerLoginBonusRewards
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace SM
{
  [Serializable]
  public class PlayerLoginBonusRewards : KeyCompare
  {
    public string client_reward_message;
    public int reward_quantity;
    public string client_next_reward_message;
    public int reward_id;
    public int reward_type_id;

    public PlayerLoginBonusRewards()
    {
    }

    public PlayerLoginBonusRewards(Dictionary<string, object> json)
    {
      this._hasKey = false;
      this.client_reward_message = json[nameof (client_reward_message)] == null ? (string) null : (string) json[nameof (client_reward_message)];
      this.reward_quantity = (int) (long) json[nameof (reward_quantity)];
      this.client_next_reward_message = json[nameof (client_next_reward_message)] == null ? (string) null : (string) json[nameof (client_next_reward_message)];
      this.reward_id = (int) (long) json[nameof (reward_id)];
      this.reward_type_id = (int) (long) json[nameof (reward_type_id)];
    }
  }
}
