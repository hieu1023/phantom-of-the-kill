﻿// Decompiled with JetBrains decompiler
// Type: SM.GuildLogTransition
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace SM
{
  [Serializable]
  public class GuildLogTransition : KeyCompare
  {
    public string scene_name;
    public int arg1;
    public int arg2;
    public int arg3;
    public int arg4;

    public GuildLogTransition()
    {
    }

    public GuildLogTransition(Dictionary<string, object> json)
    {
      this._hasKey = false;
      this.scene_name = (string) json[nameof (scene_name)];
      this.arg1 = (int) (long) json[nameof (arg1)];
      this.arg2 = (int) (long) json[nameof (arg2)];
      this.arg3 = (int) (long) json[nameof (arg3)];
      this.arg4 = (int) (long) json[nameof (arg4)];
    }
  }
}
