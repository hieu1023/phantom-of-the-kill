﻿// Decompiled with JetBrains decompiler
// Type: SM.PlayerEmblem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace SM
{
  [Serializable]
  public class PlayerEmblem : KeyCompare
  {
    public DateTime created_at;
    public int emblem_id;
    public bool is_new;

    public PlayerEmblem()
    {
    }

    public PlayerEmblem(Dictionary<string, object> json)
    {
      this._hasKey = false;
      this.created_at = DateTime.Parse((string) json[nameof (created_at)]);
      this.emblem_id = (int) (long) json[nameof (emblem_id)];
      this.is_new = (bool) json[nameof (is_new)];
    }
  }
}
