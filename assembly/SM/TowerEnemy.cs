﻿// Decompiled with JetBrains decompiler
// Type: SM.TowerEnemy
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace SM
{
  [Serializable]
  public class TowerEnemy : KeyCompare
  {
    public float hitpoint_rate;
    public int id;

    public TowerEnemy()
    {
    }

    public TowerEnemy(Dictionary<string, object> json)
    {
      this._hasKey = false;
      this.hitpoint_rate = (float) (double) json[nameof (hitpoint_rate)];
      this.id = (int) (long) json[nameof (id)];
    }
  }
}
