﻿// Decompiled with JetBrains decompiler
// Type: SM.UserAchievements
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace SM
{
  [Serializable]
  public class UserAchievements : KeyCompare
  {
    public int achievement_id;
    public int value;
    public bool unlocked;

    public UserAchievements()
    {
    }

    public UserAchievements(Dictionary<string, object> json)
    {
      this._hasKey = false;
      this.achievement_id = (int) (long) json[nameof (achievement_id)];
      this.value = (int) (long) json[nameof (value)];
      this.unlocked = (bool) json[nameof (unlocked)];
    }
  }
}
