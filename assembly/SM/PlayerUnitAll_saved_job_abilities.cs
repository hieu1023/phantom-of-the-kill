﻿// Decompiled with JetBrains decompiler
// Type: SM.PlayerUnitAll_saved_job_abilities
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace SM
{
  [Serializable]
  public class PlayerUnitAll_saved_job_abilities : KeyCompare
  {
    public int job_ability_id;
    public int level;

    public PlayerUnitAll_saved_job_abilities()
    {
    }

    public PlayerUnitAll_saved_job_abilities(Dictionary<string, object> json)
    {
      this._hasKey = false;
      this.job_ability_id = (int) (long) json[nameof (job_ability_id)];
      this.level = (int) (long) json[nameof (level)];
    }
  }
}
