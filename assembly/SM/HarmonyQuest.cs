﻿// Decompiled with JetBrains decompiler
// Type: SM.HarmonyQuest
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace SM
{
  [Serializable]
  public class HarmonyQuest : KeyCompare
  {
    public int quest_m_id;
    public bool is_disable;
    public bool is_playable;

    public HarmonyQuest()
    {
    }

    public HarmonyQuest(Dictionary<string, object> json)
    {
      this._hasKey = false;
      this.quest_m_id = (int) (long) json[nameof (quest_m_id)];
      this.is_disable = (bool) json[nameof (is_disable)];
      this.is_playable = (bool) json[nameof (is_playable)];
    }
  }
}
