﻿// Decompiled with JetBrains decompiler
// Type: SM.BattleEndTrust_upper_limit
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace SM
{
  [Serializable]
  public class BattleEndTrust_upper_limit : KeyCompare
  {
    public int same_character_id;
    public int before_value;
    public int after_value;

    public BattleEndTrust_upper_limit()
    {
    }

    public BattleEndTrust_upper_limit(Dictionary<string, object> json)
    {
      this._hasKey = false;
      this.same_character_id = (int) (long) json[nameof (same_character_id)];
      this.before_value = (int) (long) json[nameof (before_value)];
      this.after_value = (int) (long) json[nameof (after_value)];
    }
  }
}
