﻿// Decompiled with JetBrains decompiler
// Type: SM.PlayerMissionHistory
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace SM
{
  [Serializable]
  public class PlayerMissionHistory : KeyCompare
  {
    public int mission_id;
    public int story_category;
    public string reward_title;
    public bool is_clear;

    public PlayerMissionHistory()
    {
    }

    public PlayerMissionHistory(Dictionary<string, object> json)
    {
      this._hasKey = false;
      this.mission_id = (int) (long) json[nameof (mission_id)];
      this.story_category = (int) (long) json[nameof (story_category)];
      this.reward_title = json[nameof (reward_title)] == null ? (string) null : (string) json[nameof (reward_title)];
      this.is_clear = (bool) json[nameof (is_clear)];
    }
  }
}
