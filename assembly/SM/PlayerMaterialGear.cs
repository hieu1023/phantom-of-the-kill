﻿// Decompiled with JetBrains decompiler
// Type: SM.PlayerMaterialGear
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using System;
using System.Collections.Generic;
using UniLinq;

namespace SM
{
  [Serializable]
  public class PlayerMaterialGear : KeyCompare
  {
    public bool isEarthMode;
    private GearGear _gear;
    public string player_id;
    public int gear_id;
    public int id;
    public int quantity;

    public override bool Equals(object rhs)
    {
      return this.Equals(rhs as PlayerMaterialGear);
    }

    public override int GetHashCode()
    {
      return 0;
    }

    public bool Equals(PlayerMaterialGear rhs)
    {
      if ((object) rhs == null)
        return false;
      if ((object) this == (object) rhs)
        return true;
      return !(this.GetType() != rhs.GetType()) && this.id == rhs.id && this.entity_type == rhs.entity_type && this.player_id == rhs.player_id;
    }

    public static bool operator ==(PlayerMaterialGear lhs, PlayerMaterialGear rhs)
    {
      return (object) lhs == null ? (object) rhs == null : lhs.Equals(rhs);
    }

    public static bool operator !=(PlayerMaterialGear lhs, PlayerMaterialGear rhs)
    {
      return !(lhs == rhs);
    }

    public bool ForBattle
    {
      get
      {
        return false;
      }
    }

    public string name
    {
      get
      {
        return this.gear.name;
      }
    }

    public MasterDataTable.CommonRewardType entity_type
    {
      get
      {
        return MasterDataTable.CommonRewardType.gear;
      }
    }

    public GearGear gear
    {
      get
      {
        if (this._gear == null)
          this._gear = MasterData.GearGear[this.gear_id];
        return this._gear;
      }
    }

    public SupplySupply supply
    {
      get
      {
        return (SupplySupply) null;
      }
    }

    public int hp_incremental
    {
      get
      {
        return 0;
      }
    }

    public int strength_incremental
    {
      get
      {
        return this.gear.strength_incremental;
      }
    }

    public int vitality_incremental
    {
      get
      {
        return this.gear.vitality_incremental;
      }
    }

    public int intelligence_incremental
    {
      get
      {
        return this.gear.intelligence_incremental;
      }
    }

    public int mind_incremental
    {
      get
      {
        return this.gear.mind_incremental;
      }
    }

    public int agility_incremental
    {
      get
      {
        return this.gear.agility_incremental;
      }
    }

    public int dexterity_incremental
    {
      get
      {
        return this.gear.dexterity_incremental;
      }
    }

    public int lucky_incremental
    {
      get
      {
        return this.gear.lucky_incremental;
      }
    }

    public GearGearSkill[] skills
    {
      get
      {
        return (GearGearSkill[]) null;
      }
    }

    public static PlayerMaterialGear CreateForKey(int id)
    {
      PlayerMaterialGear playerMaterialGear = new PlayerMaterialGear();
      playerMaterialGear._hasKey = true;
      int num1;
      int num2 = num1 = id;
      playerMaterialGear.id = num1;
      playerMaterialGear._key = (object) num2;
      return playerMaterialGear;
    }

    public bool isWeapon()
    {
      return !this.isSupply() && !this.isCompse() && !this.isExchangable();
    }

    public bool isSupply()
    {
      return this.entity_type == MasterDataTable.CommonRewardType.supply;
    }

    public bool isCompse()
    {
      return !this.isSupply() && (this.gear.kind.Enum == GearKindEnum.smith && this.gear.compose_kind.kind.Enum != GearKindEnum.smith || (this.gear.kind.Enum == GearKindEnum.drilling || this.gear.kind.Enum == GearKindEnum.special_drilling) || this.gear.kind.Enum == GearKindEnum.sea_present);
    }

    public bool isDilling()
    {
      return !this.isSupply() && (this.gear.kind.Enum == GearKindEnum.drilling || this.gear.kind.Enum == GearKindEnum.sea_present);
    }

    public bool isSpecialDilling()
    {
      return !this.isSupply() && this.gear.kind.Enum == GearKindEnum.special_drilling;
    }

    public bool isExchangable()
    {
      return this.gear != null && !this.isSupply() && (this.gear.kind.Enum == GearKindEnum.smith && this.gear.compose_kind.kind.Enum == GearKindEnum.smith);
    }

    public bool isWeaponMaterial()
    {
      return this.gear != null && !this.gear.isMaterial();
    }

    public CommonElement GetElement()
    {
      CommonElement commonElement = CommonElement.none;
      IEnumerable<GearGearSkill> source = ((IEnumerable<GearGearSkill>) this.skills).Where<GearGearSkill>((Func<GearGearSkill, bool>) (x => ((IEnumerable<BattleskillEffect>) x.skill.Effects).Any<BattleskillEffect>((Func<BattleskillEffect, bool>) (ef => ef.effect_logic.Enum == BattleskillEffectLogicEnum.invest_element))));
      if (source.Any<GearGearSkill>())
        commonElement = source.First<GearGearSkill>().skill.element;
      return commonElement;
    }

    public PlayerMaterialGear()
    {
    }

    public PlayerMaterialGear(Dictionary<string, object> json)
    {
      this._hasKey = true;
      this.player_id = (string) json[nameof (player_id)];
      this.gear_id = (int) (long) json[nameof (gear_id)];
      this._key = (object) (this.id = (int) (long) json[nameof (id)]);
      this.quantity = (int) (long) json[nameof (quantity)];
    }
  }
}
