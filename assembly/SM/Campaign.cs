﻿// Decompiled with JetBrains decompiler
// Type: SM.Campaign
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace SM
{
  [Serializable]
  public class Campaign : KeyCompare
  {
    public string name;
    public int value;
    public int campaign_type_id;

    public Campaign()
    {
    }

    public Campaign(Dictionary<string, object> json)
    {
      this._hasKey = false;
      this.name = (string) json[nameof (name)];
      this.value = (int) (long) json[nameof (value)];
      this.campaign_type_id = (int) (long) json[nameof (campaign_type_id)];
    }
  }
}
