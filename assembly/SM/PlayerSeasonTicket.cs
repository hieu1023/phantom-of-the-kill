﻿// Decompiled with JetBrains decompiler
// Type: SM.PlayerSeasonTicket
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace SM
{
  [Serializable]
  public class PlayerSeasonTicket : KeyCompare
  {
    public string player_id;
    public int max_quantity;
    public int season_ticket_id;
    public int quantity;

    public PlayerSeasonTicket()
    {
    }

    public PlayerSeasonTicket(Dictionary<string, object> json)
    {
      this._hasKey = false;
      this.player_id = (string) json[nameof (player_id)];
      this.max_quantity = (int) (long) json[nameof (max_quantity)];
      this.season_ticket_id = (int) (long) json[nameof (season_ticket_id)];
      this.quantity = (int) (long) json[nameof (quantity)];
    }
  }
}
