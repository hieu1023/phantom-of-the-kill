﻿// Decompiled with JetBrains decompiler
// Type: SM.SeaPlayer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace SM
{
  [Serializable]
  public class SeaPlayer : KeyCompare
  {
    public int dp_auto_healing_sec;
    public int dp_full_remain;
    public int dp;
    public int dp_max;

    public SeaPlayer()
    {
    }

    public SeaPlayer(Dictionary<string, object> json)
    {
      this._hasKey = false;
      this.dp_auto_healing_sec = (int) (long) json[nameof (dp_auto_healing_sec)];
      this.dp_full_remain = (int) (long) json[nameof (dp_full_remain)];
      this.dp = (int) (long) json[nameof (dp)];
      this.dp_max = (int) (long) json[nameof (dp_max)];
    }
  }
}
