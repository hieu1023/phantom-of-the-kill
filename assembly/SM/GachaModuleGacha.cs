﻿// Decompiled with JetBrains decompiler
// Type: SM.GachaModuleGacha
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace SM
{
  [Serializable]
  public class GachaModuleGacha : KeyCompare
  {
    public int count;
    public int? payment_id;
    public int deck_id;
    public DateTime? start_at;
    public string description;
    public int roll_count;
    public DateTime? end_at;
    public int daily_count;
    public bool is_one_hundred_ream;
    public int payment_amount;
    public int? limit;
    public GachaDescription details;
    public int? remain_count_for_reward;
    public int? daily_limit;
    public int payment_type_id;
    public string button_url;
    public int id;
    public int? max_roll_count;
    public string name;

    public GachaModuleGacha()
    {
    }

    public GachaModuleGacha(Dictionary<string, object> json)
    {
      this._hasKey = false;
      this.count = (int) (long) json[nameof (count)];
      long? nullable1;
      int? nullable2;
      if (json[nameof (payment_id)] != null)
      {
        nullable1 = (long?) json[nameof (payment_id)];
        nullable2 = nullable1.HasValue ? new int?((int) nullable1.GetValueOrDefault()) : new int?();
      }
      else
        nullable2 = new int?();
      this.payment_id = nullable2;
      this.deck_id = (int) (long) json[nameof (deck_id)];
      this.start_at = json[nameof (start_at)] == null ? new DateTime?() : new DateTime?(DateTime.Parse((string) json[nameof (start_at)]));
      this.description = (string) json[nameof (description)];
      this.roll_count = (int) (long) json[nameof (roll_count)];
      this.end_at = json[nameof (end_at)] == null ? new DateTime?() : new DateTime?(DateTime.Parse((string) json[nameof (end_at)]));
      this.daily_count = (int) (long) json[nameof (daily_count)];
      this.is_one_hundred_ream = (bool) json[nameof (is_one_hundred_ream)];
      this.payment_amount = (int) (long) json[nameof (payment_amount)];
      int? nullable3;
      if (json[nameof (limit)] != null)
      {
        nullable1 = (long?) json[nameof (limit)];
        nullable3 = nullable1.HasValue ? new int?((int) nullable1.GetValueOrDefault()) : new int?();
      }
      else
        nullable3 = new int?();
      this.limit = nullable3;
      this.details = json[nameof (details)] == null ? (GachaDescription) null : new GachaDescription((Dictionary<string, object>) json[nameof (details)]);
      int? nullable4;
      if (json[nameof (remain_count_for_reward)] != null)
      {
        nullable1 = (long?) json[nameof (remain_count_for_reward)];
        nullable4 = nullable1.HasValue ? new int?((int) nullable1.GetValueOrDefault()) : new int?();
      }
      else
        nullable4 = new int?();
      this.remain_count_for_reward = nullable4;
      int? nullable5;
      if (json[nameof (daily_limit)] != null)
      {
        nullable1 = (long?) json[nameof (daily_limit)];
        nullable5 = nullable1.HasValue ? new int?((int) nullable1.GetValueOrDefault()) : new int?();
      }
      else
        nullable5 = new int?();
      this.daily_limit = nullable5;
      this.payment_type_id = (int) (long) json[nameof (payment_type_id)];
      this.button_url = json[nameof (button_url)] == null ? (string) null : (string) json[nameof (button_url)];
      this.id = (int) (long) json[nameof (id)];
      int? nullable6;
      if (json[nameof (max_roll_count)] != null)
      {
        nullable1 = (long?) json[nameof (max_roll_count)];
        nullable6 = nullable1.HasValue ? new int?((int) nullable1.GetValueOrDefault()) : new int?();
      }
      else
        nullable6 = new int?();
      this.max_roll_count = nullable6;
      this.name = (string) json[nameof (name)];
    }
  }
}
