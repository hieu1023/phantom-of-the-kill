﻿// Decompiled with JetBrains decompiler
// Type: SM.PlayerGuildTownSlotPosition
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace SM
{
  [Serializable]
  public class PlayerGuildTownSlotPosition : KeyCompare
  {
    public int y;
    public int x;
    public int master_id;

    public PlayerGuildTownSlotPosition()
    {
    }

    public PlayerGuildTownSlotPosition(Dictionary<string, object> json)
    {
      this._hasKey = false;
      this.y = (int) (long) json[nameof (y)];
      this.x = (int) (long) json[nameof (x)];
      this.master_id = (int) (long) json[nameof (master_id)];
    }
  }
}
