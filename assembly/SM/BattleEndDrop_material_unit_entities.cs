﻿// Decompiled with JetBrains decompiler
// Type: SM.BattleEndDrop_material_unit_entities
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace SM
{
  [Serializable]
  public class BattleEndDrop_material_unit_entities : KeyCompare
  {
    public int reward_quantity;
    public bool is_new;
    public int? reward_id;
    public int reward_type_id;

    public BattleEndDrop_material_unit_entities()
    {
    }

    public BattleEndDrop_material_unit_entities(Dictionary<string, object> json)
    {
      this._hasKey = false;
      this.reward_quantity = (int) (long) json[nameof (reward_quantity)];
      this.is_new = (bool) json[nameof (is_new)];
      int? nullable1;
      if (json[nameof (reward_id)] != null)
      {
        long? nullable2 = (long?) json[nameof (reward_id)];
        nullable1 = nullable2.HasValue ? new int?((int) nullable2.GetValueOrDefault()) : new int?();
      }
      else
        nullable1 = new int?();
      this.reward_id = nullable1;
      this.reward_type_id = (int) (long) json[nameof (reward_type_id)];
    }
  }
}
