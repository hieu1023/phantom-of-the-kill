﻿// Decompiled with JetBrains decompiler
// Type: SM.Description
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace SM
{
  [Serializable]
  public class Description : KeyCompare
  {
    public DescriptionBodies[] bodies;
    public string title;

    public Description()
    {
    }

    public Description(Dictionary<string, object> json)
    {
      this._hasKey = false;
      List<DescriptionBodies> descriptionBodiesList = new List<DescriptionBodies>();
      foreach (object obj in (List<object>) json[nameof (bodies)])
        descriptionBodiesList.Add(obj == null ? (DescriptionBodies) null : new DescriptionBodies((Dictionary<string, object>) obj));
      this.bodies = descriptionBodiesList.ToArray();
      this.title = json[nameof (title)] == null ? (string) null : (string) json[nameof (title)];
    }
  }
}
