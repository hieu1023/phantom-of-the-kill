﻿// Decompiled with JetBrains decompiler
// Type: SM.PlayerSelectTicketSummaryPlayer_exchange_count_list
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace SM
{
  [Serializable]
  public class PlayerSelectTicketSummaryPlayer_exchange_count_list : KeyCompare
  {
    public int exchange_count;
    public int reward_id;

    public PlayerSelectTicketSummaryPlayer_exchange_count_list()
    {
    }

    public PlayerSelectTicketSummaryPlayer_exchange_count_list(Dictionary<string, object> json)
    {
      this._hasKey = false;
      this.exchange_count = (int) (long) json[nameof (exchange_count)];
      this.reward_id = (int) (long) json[nameof (reward_id)];
    }
  }
}
