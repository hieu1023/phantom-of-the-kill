﻿// Decompiled with JetBrains decompiler
// Type: SM.LastPlayPlayerStoryQuestSIds
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace SM
{
  [Serializable]
  public class LastPlayPlayerStoryQuestSIds : KeyCompare
  {
    public int? heaven_quest_s_id;
    public int? lost_ragnarok_quest_s_id;

    public LastPlayPlayerStoryQuestSIds()
    {
    }

    public LastPlayPlayerStoryQuestSIds(Dictionary<string, object> json)
    {
      this._hasKey = false;
      long? nullable1;
      int? nullable2;
      if (json[nameof (heaven_quest_s_id)] != null)
      {
        nullable1 = (long?) json[nameof (heaven_quest_s_id)];
        nullable2 = nullable1.HasValue ? new int?((int) nullable1.GetValueOrDefault()) : new int?();
      }
      else
        nullable2 = new int?();
      this.heaven_quest_s_id = nullable2;
      int? nullable3;
      if (json[nameof (lost_ragnarok_quest_s_id)] != null)
      {
        nullable1 = (long?) json[nameof (lost_ragnarok_quest_s_id)];
        nullable3 = nullable1.HasValue ? new int?((int) nullable1.GetValueOrDefault()) : new int?();
      }
      else
        nullable3 = new int?();
      this.lost_ragnarok_quest_s_id = nullable3;
    }
  }
}
