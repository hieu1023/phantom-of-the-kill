﻿// Decompiled with JetBrains decompiler
// Type: SM.Bingo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace SM
{
  [Serializable]
  public class Bingo : KeyCompare
  {
    public int cleared_bingo_id;
    public int complete_reward_group_id;
    public int id;
    public string name;

    public Bingo()
    {
    }

    public Bingo(Dictionary<string, object> json)
    {
      this._hasKey = false;
      this.cleared_bingo_id = (int) (long) json[nameof (cleared_bingo_id)];
      this.complete_reward_group_id = (int) (long) json[nameof (complete_reward_group_id)];
      this.id = (int) (long) json[nameof (id)];
      this.name = (string) json[nameof (name)];
    }
  }
}
