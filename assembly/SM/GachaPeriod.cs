﻿// Decompiled with JetBrains decompiler
// Type: SM.GachaPeriod
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace SM
{
  [Serializable]
  public class GachaPeriod : KeyCompare
  {
    public bool display_count_down;
    public DateTime? start_at;
    public DateTime? end_at;

    public GachaPeriod()
    {
    }

    public GachaPeriod(Dictionary<string, object> json)
    {
      this._hasKey = false;
      this.display_count_down = (bool) json[nameof (display_count_down)];
      this.start_at = json[nameof (start_at)] == null ? new DateTime?() : new DateTime?(DateTime.Parse((string) json[nameof (start_at)]));
      this.end_at = json[nameof (end_at)] == null ? new DateTime?() : new DateTime?(DateTime.Parse((string) json[nameof (end_at)]));
    }
  }
}
