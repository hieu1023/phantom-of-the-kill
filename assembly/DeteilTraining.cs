﻿// Decompiled with JetBrains decompiler
// Type: DeteilTraining
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System;
using System.Collections.Generic;
using UnityEngine;

public class DeteilTraining : MonoBehaviour
{
  private bool isShow = true;
  [SerializeField]
  private UIButton ibtn_conpose;
  [SerializeField]
  private GameObject compose_dir_campaign_icon;
  [SerializeField]
  private UIButton ibtn_evolution;
  [SerializeField]
  private UIButton ibtn_enforce;
  [SerializeField]
  private GameObject enforce_dir_campaign_icon;
  [SerializeField]
  private UIButton ibtn_reincarnation;
  [SerializeField]
  private GameObject reincarnation_dir_campaign_icon;
  [SerializeField]
  private UIButton ibtn_job_change;
  private bool isJobChangeClicked;
  private PlayerUnit unit;
  private PlayerUnit[] unitList;

  public void Initialize(PlayerUnit unit, PlayerUnit[] unitList)
  {
    NGGameDataManager.Boost boostInfo = Singleton<NGGameDataManager>.GetInstance().BoostInfo;
    this.unit = unit;
    this.unitList = unitList;
    this.ibtn_evolution.isEnabled = unit.unit.IsEvolution;
    this.compose_dir_campaign_icon.SetActive(boostInfo != null && boostInfo.DiscountUnitCompose != new Decimal(10, 0, 0, false, (byte) 1));
    this.ibtn_reincarnation.isEnabled = unit.level >= unit.unit.rarity.reincarnation_level;
    this.reincarnation_dir_campaign_icon.SetActive(boostInfo != null && boostInfo.DiscountUnitTransmigrate != new Decimal(10, 0, 0, false, (byte) 1));
    this.ibtn_enforce.isEnabled = unit.unit.IsNormalUnit && unit.buildup_limit > 0;
    this.enforce_dir_campaign_icon.SetActive(boostInfo != null && boostInfo.DiscountUnitBuildup != new Decimal(10, 0, 0, false, (byte) 1));
    if ((UnityEngine.Object) this.ibtn_job_change != (UnityEngine.Object) null)
      this.ibtn_job_change.isEnabled = JobChangeUtil.getJobChangePatterns(unit) != null;
    this.Hide();
  }

  protected void getUnits()
  {
    this.unit = Array.Find<PlayerUnit>(SMManager.Get<PlayerUnit[]>(), (Predicate<PlayerUnit>) (x => x.id == this.unit.id));
    List<PlayerUnit> list = new List<PlayerUnit>();
    ((IEnumerable<PlayerUnit>) this.unitList).ForEach<PlayerUnit>((System.Action<PlayerUnit>) (x =>
    {
      PlayerUnit playerUnit = Array.Find<PlayerUnit>(SMManager.Get<PlayerUnit[]>(), (Predicate<PlayerUnit>) (y => y.id == x.id));
      if (!(playerUnit != (PlayerUnit) null))
        return;
      list.Add(playerUnit);
    }));
    this.unitList = list.ToArray();
  }

  public void ChangeSceneUnit()
  {
    this.getUnits();
    Unit0042Scene.changeScene(false, this.unit, this.unitList, false, false);
  }

  public void IbtnCompose()
  {
    if (Singleton<NGGameDataManager>.GetInstance().IsSea)
      Singleton<CommonRoot>.GetInstance().headerType = CommonRoot.HeaderType.Normal;
    Unit00484Scene.changeScene(false, this.unit, new PlayerUnit[5], Unit00468Scene.Mode.Unit0048, new System.Action(this.ChangeSceneUnit));
    this.Hide();
  }

  public void IbtnEvolution()
  {
    Singleton<CommonRoot>.GetInstance().isTouchBlock = true;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    if (Singleton<NGGameDataManager>.GetInstance().IsSea)
      Singleton<CommonRoot>.GetInstance().headerType = CommonRoot.HeaderType.Normal;
    if (!this.unit.unit.can_awake_unit_flag)
      Unit00499Scene.changeScene(false, this.unit, Unit00499Scene.Mode.Evolution, new System.Action(this.ChangeSceneUnit));
    else if (this.unit.unit.ID == 101414)
      Unit00499Scene.changeScene(false, this.unit, Unit00499Scene.Mode.AwakeUnit, new System.Action(this.ChangeSceneUnit));
    else
      Unit00499Scene.changeScene(false, this.unit, Unit00499Scene.Mode.CommonAwakeUnit, new System.Action(this.ChangeSceneUnit));
    this.Hide();
  }

  public void IbtnEnforce()
  {
    if (Singleton<NGGameDataManager>.GetInstance().IsSea)
      Singleton<CommonRoot>.GetInstance().headerType = CommonRoot.HeaderType.Normal;
    Unit00420Scene.changeScene(false, this.unit, new PlayerUnit[5], new System.Action(this.ChangeSceneUnit));
    this.Hide();
  }

  public void IbtnReincarnation()
  {
    Singleton<CommonRoot>.GetInstance().isTouchBlock = true;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    if (Singleton<NGGameDataManager>.GetInstance().IsSea)
      Singleton<CommonRoot>.GetInstance().headerType = CommonRoot.HeaderType.Normal;
    Unit00499Scene.changeScene(false, this.unit, Unit00499Scene.Mode.Transmigration, new System.Action(this.ChangeSceneUnit));
    this.Hide();
  }

  public void IbtnJobChange()
  {
    this.isJobChangeClicked = true;
    Unit004JobChangeScene.changeScene(false, this.unit.id, new int?(), new System.Action(this.ChangeSceneUnit));
    this.Hide();
  }

  public void Show()
  {
    if (this.gameObject.activeSelf)
      return;
    this.gameObject.SetActive(true);
    ((IEnumerable<UITweener>) this.gameObject.GetComponentsInChildren<UITweener>()).ForEach<UITweener>((System.Action<UITweener>) (c =>
    {
      c.enabled = true;
      c.onFinished.Clear();
      c.PlayForward();
    }));
  }

  public void Hide()
  {
    if (!this.gameObject.activeSelf)
      return;
    if (Singleton<NGGameDataManager>.GetInstance().IsSea)
    {
      this.gameObject.SetActive(false);
    }
    else
    {
      UITweener[] tweens = this.gameObject.GetComponentsInChildren<UITweener>();
      if (tweens.Length == 0)
        return;
      int finishCount = 0;
      EventDelegate.Callback onFinish = (EventDelegate.Callback) (() =>
      {
        if (++finishCount < tweens.Length)
          return;
        this.gameObject.SetActive(false);
      });
      ((IEnumerable<UITweener>) tweens).ForEach<UITweener>((System.Action<UITweener>) (c =>
      {
        c.onFinished.Clear();
        c.AddOnFinished(onFinish);
        c.PlayReverse();
      }));
    }
  }
}
