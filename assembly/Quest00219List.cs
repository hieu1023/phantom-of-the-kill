﻿// Decompiled with JetBrains decompiler
// Type: Quest00219List
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;
using System.Collections;
using UnityEngine;

[AddComponentMenu("Scenes/QuestExtra/M_ListItem")]
public class Quest00219List : MonoBehaviour
{
  [SerializeField]
  public UIButton Dock;
  [SerializeField]
  public UILabel Name;
  [SerializeField]
  public UISprite Clear;
  [SerializeField]
  public UISprite New;

  public IEnumerator Init(PlayerExtraQuestS extra, bool isClear, bool isNew)
  {
    this.Clear.gameObject.SetActive(isClear);
    this.New.gameObject.SetActive(isNew);
    this.Name.SetTextLocalize(MasterData.QuestExtraM[extra.quest_extra_s.quest_m_QuestExtraM].name);
    yield break;
  }
}
