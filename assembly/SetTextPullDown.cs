﻿// Decompiled with JetBrains decompiler
// Type: SetTextPullDown
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class SetTextPullDown : MonoBehaviour
{
  [SerializeField]
  public UILabel label;
  [SerializeField]
  public UIPopupList popup;
  [SerializeField]
  public UIInput input;

  public void SetText()
  {
    this.label.text = this.popup.value;
  }

  public void SetTextInput()
  {
    this.label.text = this.input.value;
  }
}
