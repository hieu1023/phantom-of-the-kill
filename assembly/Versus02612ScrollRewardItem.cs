﻿// Decompiled with JetBrains decompiler
// Type: Versus02612ScrollRewardItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Versus02612ScrollRewardItem : MonoBehaviour
{
  private int[] canDetailItems = new int[9]
  {
    1,
    24,
    3,
    26,
    35,
    2,
    19,
    21,
    29
  };
  [SerializeField]
  private UILabel txtDetail;
  [SerializeField]
  private UIButton ibtnDetail;
  [SerializeField]
  private GameObject objLine;
  [SerializeField]
  private GameObject linkTarget;
  private GameObject detailPopup;
  private GameObject reisouPopupPrefab;
  private MasterDataTable.CommonRewardType rewardType;
  private int rewardID;
  private bool isDetail;
  private bool isReisou;

  public IEnumerator CreateItem(
    MasterDataTable.CommonRewardType rewardType,
    int rewardID,
    string txt,
    bool isLineObj,
    bool isUnitRarityCenter = false)
  {
    this.txtDetail.SetTextLocalize(txt);
    this.objLine.SetActive(isLineObj);
    this.isDetail = false;
    this.rewardType = rewardType;
    this.rewardID = rewardID;
    CreateIconObject target = this.linkTarget.GetOrAddComponent<CreateIconObject>();
    IEnumerator e = target.CreateThumbnail(rewardType, rewardID, 0, true, true, new CommonQuestType?(), false);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if (isUnitRarityCenter && (rewardType == MasterDataTable.CommonRewardType.unit || rewardType == MasterDataTable.CommonRewardType.material_unit))
      target.GetIcon().GetComponent<UnitIcon>().RarityCenter();
    this.isReisou = false;
    if (rewardType == MasterDataTable.CommonRewardType.gear)
    {
      this.isReisou = MasterData.GearGear[rewardID].isReisou();
      this.ibtnDetail.gameObject.SetActive(true);
    }
    if (((IEnumerable<int>) this.canDetailItems).Any<int>((Func<int, bool>) (x => (MasterDataTable.CommonRewardType) x == rewardType)) && !this.isReisou)
    {
      this.ibtnDetail.gameObject.SetActive(true);
      Future<GameObject> prefabF = Res.Prefabs.popup.popup_000_7_4_2__anim_popup01.Load<GameObject>();
      e = prefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.detailPopup = prefabF.Result;
      prefabF = (Future<GameObject>) null;
    }
  }

  public void IbtnDetail()
  {
    if (this.isDetail)
      return;
    this.isDetail = true;
    this.StartCoroutine(this.onDetail());
  }

  private IEnumerator onDetail()
  {
    if (this.isReisou)
    {
      yield return (object) this.OpenReisouDetailPopupAsync(this.rewardID);
      this.isDetail = false;
    }
    else
    {
      GameObject detail = this.detailPopup.Clone((Transform) null);
      detail.SetActive(false);
      IEnumerator e = detail.GetComponent<ItemDetailPopupBase>().SetInfo(this.rewardType, this.rewardID, 0);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      Singleton<PopupManager>.GetInstance().open(detail, false, false, true, true, false, false, "SE_1006");
      detail.SetActive(true);
      this.isDetail = false;
    }
  }

  protected IEnumerator OpenReisouDetailPopupAsync(int rewardID)
  {
    GearGear gear = (GearGear) null;
    MasterData.GearGear.TryGetValue(rewardID, out gear);
    PlayerItem playerItem = new PlayerItem(gear, MasterDataTable.CommonRewardType.gear);
    GameCore.ItemInfo itemInfo = new GameCore.ItemInfo(playerItem);
    IEnumerator e;
    if ((UnityEngine.Object) this.reisouPopupPrefab == (UnityEngine.Object) null)
    {
      Future<GameObject> popupPrefabF = new ResourceObject("Prefabs/UnitGUIs/PopupReisouSkillDetails").Load<GameObject>();
      e = popupPrefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.reisouPopupPrefab = popupPrefabF.Result;
      popupPrefabF = (Future<GameObject>) null;
    }
    GameObject popup = this.reisouPopupPrefab.Clone((Transform) null);
    PopupReisouDetails script = popup.GetComponent<PopupReisouDetails>();
    popup.SetActive(false);
    e = script.Init(itemInfo, playerItem, false);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    popup.SetActive(true);
    Singleton<PopupManager>.GetInstance().open(popup, false, false, true, true, false, false, "SE_1006");
    yield return (object) null;
    script.scrollResetPosition();
  }
}
