﻿// Decompiled with JetBrains decompiler
// Type: UnitStatusInformation.Equips
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace UnitStatusInformation
{
  [Flags]
  public enum Equips
  {
    MainWeapon = 15, // 0x0000000F
    Unique = 16, // 0x00000010
    Shield = 32, // 0x00000020
    Accessory = 64, // 0x00000040
  }
}
