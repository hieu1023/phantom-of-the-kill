﻿// Decompiled with JetBrains decompiler
// Type: UnitStatusInformation.Util
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Reflection;
using UniLinq;
using UnityEngine;

namespace UnitStatusInformation
{
  public static class Util
  {
    public static void SetTextIntegersWithStateColor<T>(
      Dictionary<string, UILabel> dest,
      T now,
      T prev)
      where T : class
    {
      System.Type type = typeof (T);
      FieldInfo[] array1 = ((IEnumerable<FieldInfo>) type.GetFields()).Where<FieldInfo>((Func<FieldInfo, bool>) (x => x.FieldType.IsValueType)).ToArray<FieldInfo>();
      PropertyInfo[] array2 = ((IEnumerable<PropertyInfo>) type.GetProperties()).Where<PropertyInfo>((Func<PropertyInfo, bool>) (x => x.PropertyType.IsValueType)).ToArray<PropertyInfo>();
      foreach (KeyValuePair<string, UILabel> keyValuePair in dest)
      {
        KeyValuePair<string, UILabel> c = keyValuePair;
        if ((UnityEngine.Object) c.Value == (UnityEngine.Object) null)
        {
          Debug.LogError((object) ("\"int " + type.Name + "." + c.Key + "\" set UILabel is null"));
        }
        else
        {
          FieldInfo fieldInfo = Array.Find<FieldInfo>(array1, (Predicate<FieldInfo>) (x => x.Name == c.Key));
          int now1;
          int prev1;
          if (fieldInfo != (FieldInfo) null)
          {
            now1 = (int) fieldInfo.GetValue((object) now);
            prev1 = (int) fieldInfo.GetValue((object) prev);
          }
          else
          {
            PropertyInfo propertyInfo = Array.Find<PropertyInfo>(array2, (Predicate<PropertyInfo>) (x => x.Name == c.Key));
            if (propertyInfo != (PropertyInfo) null)
            {
              now1 = (int) propertyInfo.GetValue((object) now);
              prev1 = (int) propertyInfo.GetValue((object) prev);
            }
            else
            {
              Debug.LogError((object) ("Not Found Field or Property \"int " + c.Key + "\" in " + type.Name));
              continue;
            }
          }
          Util.SetTextIntegerWithStateColor(c.Value, now1, prev1);
        }
      }
    }

    public static void SetTextIntegerWithStateColor(UILabel label, int now, int prev)
    {
      if (!((UnityEngine.Object) label != (UnityEngine.Object) null))
        return;
      label.text = now.ToString();
      label.color = now == prev ? Color.white : (now > prev ? Color.yellow : Color.red);
    }

    public static void SetTextIntegers<T>(Dictionary<string, UILabel> dest, T dat, Color fontColor) where T : class
    {
      System.Type type = typeof (T);
      FieldInfo[] array1 = ((IEnumerable<FieldInfo>) type.GetFields()).Where<FieldInfo>((Func<FieldInfo, bool>) (x => x.FieldType.IsValueType)).ToArray<FieldInfo>();
      PropertyInfo[] array2 = ((IEnumerable<PropertyInfo>) type.GetProperties()).Where<PropertyInfo>((Func<PropertyInfo, bool>) (x => x.PropertyType.IsValueType)).ToArray<PropertyInfo>();
      foreach (KeyValuePair<string, UILabel> keyValuePair in dest)
      {
        KeyValuePair<string, UILabel> c = keyValuePair;
        if ((UnityEngine.Object) c.Value == (UnityEngine.Object) null)
        {
          Debug.LogError((object) ("\"int " + type.Name + "." + c.Key + "\" set UILabel is null"));
        }
        else
        {
          FieldInfo fieldInfo = Array.Find<FieldInfo>(array1, (Predicate<FieldInfo>) (x => x.Name == c.Key));
          int num;
          if (fieldInfo != (FieldInfo) null)
          {
            num = (int) fieldInfo.GetValue((object) dat);
          }
          else
          {
            PropertyInfo propertyInfo = Array.Find<PropertyInfo>(array2, (Predicate<PropertyInfo>) (x => x.Name == c.Key));
            if (propertyInfo != (PropertyInfo) null)
            {
              num = (int) propertyInfo.GetValue((object) dat);
            }
            else
            {
              Debug.LogError((object) ("Not Found Field or Property \"int " + c.Key + "\" in " + type.Name));
              continue;
            }
          }
          c.Value.text = num.ToString();
          c.Value.color = fontColor;
        }
      }
    }
  }
}
