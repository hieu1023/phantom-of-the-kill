﻿// Decompiled with JetBrains decompiler
// Type: Quest00220Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

[AddComponentMenu("Scenes/QuestExtra/S_Menu")]
public class Quest00220Menu : QuestStageMenuBase
{
  private bool isKeyQuest;

  protected override void Update()
  {
    if (!this.SceneStart || this.hscrollButtons == null)
      return;
    base.Update();
    this.UpdateButton();
  }

  public IEnumerator Initialize(
    PlayerExtraQuestS[] ExtraData,
    int L,
    int M,
    int S,
    bool forcus,
    bool isKeyQuest)
  {
    Quest00220Menu quest00220Menu = this;
    quest00220Menu.isKeyQuest = isKeyQuest;
    PlayerExtraQuestS[] ExtraDataS = ((IEnumerable<PlayerExtraQuestS>) ExtraData).S(L, M, false);
    bool? isOpen = new bool?();
    QuestScoreCampaignProgress[] campaignProgressArray = SMManager.Get<QuestScoreCampaignProgress[]>();
    if (campaignProgressArray != null && campaignProgressArray.Length != 0)
    {
      QuestScoreCampaignProgress campaignProgress = ((IEnumerable<QuestScoreCampaignProgress>) campaignProgressArray).FirstOrDefault<QuestScoreCampaignProgress>((Func<QuestScoreCampaignProgress, bool>) (x => x.quest_extra_l == L));
      if (campaignProgress != null)
        isOpen = new bool?(campaignProgress.is_open);
    }
    quest00220Menu.popSelectedSID(ref S, ref forcus, false);
    IEnumerator e = quest00220Menu.InitExtraQuest(ExtraDataS, L, M, S, forcus, isOpen);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if (Persist.newTutorial.Data.beginnersQuest)
    {
      Persist.newTutorial.Data.beginnersQuest = false;
      Persist.newTutorial.Flush();
      Singleton<TutorialRoot>.GetInstance().ForceShowAdvice("newchapter_quest_tutorial", (System.Action) null);
    }
  }

  protected override void SetTextLimitation(int s_id)
  {
    AssocList<int, QuestExtraLimitation> questExtraLimitation = MasterData.QuestExtraLimitation;
    QuestExtraLimitationLabel[] limitationLabelList = MasterData.QuestExtraLimitationLabelList;
    Func<KeyValuePair<int, QuestExtraLimitation>, bool> predicate = (Func<KeyValuePair<int, QuestExtraLimitation>, bool>) (n => n.Value.quest_s_id_QuestExtraS == s_id);
    KeyValuePair<int, QuestExtraLimitation>[] array = questExtraLimitation.Where<KeyValuePair<int, QuestExtraLimitation>>(predicate).ToArray<KeyValuePair<int, QuestExtraLimitation>>();
    if (((IEnumerable<KeyValuePair<int, QuestExtraLimitation>>) array).Count<KeyValuePair<int, QuestExtraLimitation>>() == 0)
    {
      this.EntryInfoScript.IsDisplay = false;
    }
    else
    {
      int target_id = ((IEnumerable<KeyValuePair<int, QuestExtraLimitation>>) array).First<KeyValuePair<int, QuestExtraLimitation>>().Value.ID;
      this.EntryInfoScript.TextNormal = ((IEnumerable<QuestExtraLimitationLabel>) limitationLabelList).Where<QuestExtraLimitationLabel>((Func<QuestExtraLimitationLabel, bool>) (n => n.ID == target_id)).First<QuestExtraLimitationLabel>().label;
      this.EntryInfoScript.Normal = true;
      this.EntryInfoScript.Hurd = false;
    }
    QuestExtraS questExtraS = MasterData.QuestExtraS[s_id];
    if (questExtraS != null && questExtraS.gender_restriction != UnitGender.none)
    {
      this.TxtGenderRestriction.gameObject.SetActive(true);
      this.TxtGenderRestriction.SetTextLocalize(Consts.Format(Consts.GetInstance().QUEST_002_GENDERRESTRICTION, (IDictionary) new Hashtable()
      {
        {
          (object) "gender",
          (object) UnitGenderText.GetText(questExtraS.gender_restriction)
        }
      }));
    }
    else
      this.TxtGenderRestriction.gameObject.SetActive(false);
  }

  public override void IbtnBack()
  {
    if (this.IsPushAndSet())
      return;
    this.btnBack.enabled = false;
    this.tweenSettingDefault();
    QuestConverterData questS;
    if (this.isKeyQuest)
      Quest002171Scene.ChangeScene(false);
    else if ((questS = this.StageDataS.First<QuestConverterData>()).seek_type == QuestExtra.SeekType.L)
    {
      QuestScoreCampaignProgress[] campaignProgressArray = SMManager.Get<QuestScoreCampaignProgress[]>();
      if (campaignProgressArray != null)
      {
        if (((IEnumerable<QuestScoreCampaignProgress>) campaignProgressArray).FirstOrDefault<QuestScoreCampaignProgress>((Func<QuestScoreCampaignProgress, bool>) (x => x.quest_extra_l == questS.id_L)) != null)
          Quest00226Scene.ChangeScene(questS.id_S, false);
        else
          Quest00219Scene.ChangeScene(questS.id_S, false);
      }
      else
        Quest00219Scene.ChangeScene(questS.id_S, false);
    }
    else
    {
      if (questS.seek_type != QuestExtra.SeekType.M)
        return;
      QuestExtraLL questLl = MasterData.QuestExtraM[questS.id_M].quest_ll;
      if (questLl == null)
        Quest00217Scene.ChangeScene(false, questS.top_category);
      else
        Quest00218Scene.backOrChangeScene(questLl.ID, new int?(questS.id_S));
    }
  }
}
