﻿// Decompiled with JetBrains decompiler
// Type: SpriteSelectDirectButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[RequireComponent(typeof (UISprite))]
public class SpriteSelectDirectButton : SpriteSelectDirect
{
  [SerializeField]
  private UIButton button;

  public override void SetSpriteName<T>(T n, bool resizeTarget = true)
  {
    base.SetSpriteName<T>(n, resizeTarget);
    this.button.normalSprite = this.spriteName;
    this.button.pressedSprite = this.spriteName;
  }
}
