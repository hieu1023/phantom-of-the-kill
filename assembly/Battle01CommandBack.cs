﻿// Decompiled with JetBrains decompiler
// Type: Battle01CommandBack
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;
using UnityEngine;

public class Battle01CommandBack : BattleBackButtonMenuBase
{
  private Battle01SelectNode selectNode;
  private PVPManager _pvpManager;

  private PVPManager pvpManager
  {
    get
    {
      if ((Object) this._pvpManager == (Object) null)
        this._pvpManager = Singleton<PVPManager>.GetInstance();
      return this._pvpManager;
    }
  }

  private void Awake()
  {
    EventDelegate.Set(this.GetComponent<UIButton>().onClick, new EventDelegate((MonoBehaviour) this, "onClick"));
  }

  protected override IEnumerator Start_Battle()
  {
    // ISSUE: reference to a compiler-generated field
    int num = this.\u003C\u003E1__state;
    Battle01CommandBack battle01CommandBack = this;
    if (num != 0)
      return false;
    // ISSUE: reference to a compiler-generated field
    this.\u003C\u003E1__state = -1;
    battle01CommandBack.selectNode = NGUITools.FindInParents<Battle01SelectNode>(battle01CommandBack.transform);
    return false;
  }

  public override void onBackButton()
  {
    this.onClick();
  }

  public void onClick()
  {
    if (Singleton<PopupManager>.GetInstance().isOpen || !this.battleManager.isBattleEnable || this.battleManager.isPvp && this.env.core.phaseState.state != BL.Phase.pvp_wait_preparing && this.pvpManager.isSending || this.battleManager.getController<BattleStateController>().isWaitCurrentAIActionCancel)
      return;
    if (this.env.core.phaseState.state == BL.Phase.pvp_disposition && this.env.core.unitCurrent.unit != (BL.Unit) null)
      this.env.core.currentUnitPosition.cancelMove(this.env);
    this.selectNode.onBack();
  }
}
