﻿// Decompiled with JetBrains decompiler
// Type: AssetBundleSpriteLoader
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;
using UnityEngine;

public class AssetBundleSpriteLoader : MonoBehaviour
{
  [Tooltip("Resouces以下のパスを拡張子なしで入力")]
  public string Path = string.Empty;
  public bool AutoLoad = true;

  private IEnumerator Start()
  {
    if (this.AutoLoad)
      yield return (object) this.Load();
  }

  public IEnumerator Load()
  {
    string[] strArray = this.Path.Split('.');
    IEnumerator e;
    if (strArray[strArray.Length - 1].Equals("mat"))
    {
      e = this.LoadMaterial(strArray[0]);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    else
    {
      e = this.LoadSprite(strArray[0]);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
  }

  private IEnumerator LoadSprite(string path)
  {
    AssetBundleSpriteLoader bundleSpriteLoader = this;
    Future<UnityEngine.Sprite> loader = Singleton<ResourceManager>.GetInstance().Load<UnityEngine.Sprite>(path, 1f);
    IEnumerator e = loader.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    UnityEngine.Sprite result = loader.Result;
    UI2DSprite component1 = bundleSpriteLoader.GetComponent<UI2DSprite>();
    if ((Object) component1 != (Object) null)
    {
      component1.sprite2D = result;
    }
    else
    {
      SpriteRenderer component2 = bundleSpriteLoader.GetComponent<SpriteRenderer>();
      if ((Object) component2 != (Object) null)
        component2.sprite = result;
    }
  }

  private IEnumerator LoadMaterial(string path)
  {
    AssetBundleSpriteLoader bundleSpriteLoader = this;
    Future<UnityEngine.Material> loader = Singleton<ResourceManager>.GetInstance().Load<UnityEngine.Material>(path, 1f);
    IEnumerator e = loader.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    UnityEngine.Material result = loader.Result;
    UI2DSprite component1 = bundleSpriteLoader.GetComponent<UI2DSprite>();
    if ((Object) component1 != (Object) null)
    {
      component1.material = result;
    }
    else
    {
      SpriteRenderer component2 = bundleSpriteLoader.GetComponent<SpriteRenderer>();
      if ((Object) component2 != (Object) null)
        component2.material = result;
    }
  }
}
