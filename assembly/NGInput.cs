﻿// Decompiled with JetBrains decompiler
// Type: NGInput
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

public class NGInput : Singleton<NGInput>
{
  [SerializeField]
  private bool disable;
  private float timer;

  public bool Disable
  {
    get
    {
      return this.disable;
    }
    set
    {
      if (value)
      {
        this.timer = 0.5f;
        if (!this.disable)
          this.OpenDisable();
        this.disable = true;
      }
      else
      {
        if ((double) this.timer >= 0.0)
          return;
        this.disable = false;
      }
    }
  }

  private void OpenDisable()
  {
    Singleton<CommonRoot>.GetInstance().isTouchBlockAutoClose = true;
    this.StartCoroutine(this.CloseDisable());
  }

  private IEnumerator CloseDisable()
  {
    do
    {
      this.timer -= Time.deltaTime;
      yield return (object) null;
    }
    while ((double) this.timer >= 0.0);
    this.Disable = false;
  }

  protected override void Initialize()
  {
    this.disable = false;
  }

  public bool GetKeyUp(string code)
  {
    return !this.Disable && Input.GetKeyUp(code);
  }

  public bool GetKeyUp(KeyCode code)
  {
    return !this.Disable && Input.GetKeyUp(code);
  }
}
