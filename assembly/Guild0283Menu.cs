﻿// Decompiled with JetBrains decompiler
// Type: Guild0283Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using UnityEngine;

public class Guild0283Menu : BackButtonMenuBase
{
  [SerializeField]
  private NGxScroll ngxScroll;
  [SerializeField]
  private UILabel menuTitle;
  [SerializeField]
  private UIButton buttonTitle;
  [SerializeField]
  private UIButton buttonSetting;
  [SerializeField]
  private UIButton buttonApplicants;
  [SerializeField]
  private UILabel lblUnavailableApplicants;
  [SerializeField]
  private UIButton buttonLeave;
  [SerializeField]
  private UILabel lblUnavailableLeave;
  [SerializeField]
  private UIButton buttonDismiss;
  [SerializeField]
  private UILabel lblUnavailableDismiss;
  [SerializeField]
  private UIButton buttonDefenseMember;
  [SerializeField]
  private UILabel lblUnavailableDefenseMember;
  [SerializeField]
  private GameObject applicantBadge;
  [SerializeField]
  private GameObject titleBadge;
  [SerializeField]
  private UIButton buttonResearch;
  [SerializeField]
  private GameObject buttonResearchInBattleText;
  private GameObject guildSettingPopup;
  private GameObject guildSettingConfirmPopup;
  private GameObject guildBrakeupUnavailablePopup;
  private GameObject guildBrakeupPopup;
  private GameObject guildBrakeupConfirmPopup;
  private GameObject guildResignUnavailablePopup;
  private GameObject guildResignPopup;
  private GameObject guildResignConfirmPopup;
  private GameObject guildNgWordPopup;
  private GameObject commonOkPopup;
  private GameObject defenseMemberSelectPopup;
  private GameObject memberPrefab;

  public GameObject GuildSettingPopup
  {
    get
    {
      return this.guildSettingPopup;
    }
  }

  public GameObject GuildSettingConfirmPopup
  {
    get
    {
      return this.guildSettingConfirmPopup;
    }
  }

  public GameObject GuildBreakupUnavailablePopup
  {
    get
    {
      return this.guildBrakeupUnavailablePopup;
    }
  }

  public GameObject GuildBrakeupPopup
  {
    get
    {
      return this.guildBrakeupPopup;
    }
  }

  public GameObject GuildBrakeUpConfirmPopup
  {
    get
    {
      return this.guildBrakeupConfirmPopup;
    }
  }

  public GameObject GuildResignUnavailablePopup
  {
    get
    {
      return this.guildResignUnavailablePopup;
    }
  }

  public GameObject GuildResignPopup
  {
    get
    {
      return this.guildResignPopup;
    }
  }

  public GameObject GuildResignConfirmPopup
  {
    get
    {
      return this.guildResignConfirmPopup;
    }
  }

  public GameObject GuildNgWordPopup
  {
    get
    {
      return this.guildNgWordPopup;
    }
  }

  public GameObject CommonOkPupup
  {
    get
    {
      return this.commonOkPopup;
    }
  }

  public GameObject DefenseMemberSelectPopup
  {
    get
    {
      return this.defenseMemberSelectPopup;
    }
  }

  public GameObject MemberPrefab
  {
    get
    {
      return this.memberPrefab;
    }
  }

  public IEnumerator InitializeAsync()
  {
    Future<WebAPI.Response.GuildTop> guildTop = WebAPI.GuildTop(-1, false, (System.Action<WebAPI.Response.UserError>) (e =>
    {
      WebAPI.DefaultUserErrorCallback(e);
      Singleton<CommonRoot>.GetInstance().isLoading = true;
      Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
    }));
    IEnumerator e1 = guildTop.Wait();
    while (e1.MoveNext())
      yield return e1.Current;
    e1 = (IEnumerator) null;
    if (PlayerAffiliation.Current.guild_id == null)
    {
      Singleton<CommonRoot>.GetInstance().isLoading = true;
      Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
    }
    else
    {
      this.menuTitle.SetTextLocalize(Consts.Format(Consts.GetInstance().POPUP_028_3_MENU_TITLE, (IDictionary) null));
      this.lblUnavailableApplicants.SetTextLocalize(Consts.GetInstance().POPUP_028_3_UNAVAILABLE);
      this.lblUnavailableLeave.SetTextLocalize(Consts.GetInstance().POPUP_028_3_UNAVAILABLE);
      this.lblUnavailableDismiss.SetTextLocalize(Consts.GetInstance().POPUP_028_3_UNAVAILABLE);
      this.lblUnavailableDefenseMember.SetTextLocalize(Consts.GetInstance().POPUP_028_3_UNAVAILABLE);
      e1 = this.ResourceLoad();
      while (e1.MoveNext())
        yield return e1.Current;
      e1 = (IEnumerator) null;
      GuildRole? role = PlayerAffiliation.Current.role;
      GuildRole guildRole1 = GuildRole.master;
      int num1;
      if (!(role.GetValueOrDefault() == guildRole1 & role.HasValue))
      {
        role = PlayerAffiliation.Current.role;
        GuildRole guildRole2 = GuildRole.sub_master;
        num1 = role.GetValueOrDefault() == guildRole2 & role.HasValue ? 1 : 0;
      }
      else
        num1 = 1;
      bool flag1 = num1 != 0;
      role = PlayerAffiliation.Current.role;
      GuildRole guildRole3 = GuildRole.master;
      bool flag2 = role.GetValueOrDefault() == guildRole3 & role.HasValue;
      this.buttonSetting.transform.parent.gameObject.SetActive(flag1);
      this.buttonApplicants.transform.parent.gameObject.SetActive(flag1);
      this.buttonResearch.transform.parent.gameObject.SetActive(!flag1);
      this.buttonDefenseMember.transform.parent.gameObject.SetActive(flag2);
      if (flag1)
        this.applicantBadge.SetActive(Persist.guildSetting.Exists && GuildUtil.getBadgeState(GuildUtil.GuildBadgeInfoType.newApplicant));
      else
        this.applicantBadge.SetActive(false);
      GameObject gameObject = this.buttonDismiss.transform.parent.gameObject;
      role = PlayerAffiliation.Current.role;
      GuildRole guildRole4 = GuildRole.master;
      int num2 = role.GetValueOrDefault() == guildRole4 & role.HasValue ? 1 : 0;
      gameObject.SetActive(num2 != 0);
      this.titleBadge.SetActive(Persist.guildSetting.Exists && GuildUtil.getBadgeState(GuildUtil.GuildBadgeInfoType.newTitle));
      if (PlayerAffiliation.Current.onGvgOperation)
      {
        this.SetGuildDontNotMoveButton();
      }
      else
      {
        DateTime dateTime1 = ServerTime.NowAppTime();
        if (guildTop.Result.raid_period != null)
        {
          DateTime? entryEndAt = guildTop.Result.raid_period.entry_end_at;
          DateTime dateTime2 = dateTime1;
          if ((entryEndAt.HasValue ? (entryEndAt.GetValueOrDefault() <= dateTime2 ? 1 : 0) : 0) != 0)
          {
            DateTime? endAt = guildTop.Result.raid_period.end_at;
            DateTime dateTime3 = dateTime1;
            if ((endAt.HasValue ? (endAt.GetValueOrDefault() >= dateTime3 ? 1 : 0) : 0) != 0)
              goto label_20;
          }
        }
        if (!guildTop.Result.raid_aggregating)
          goto label_21;
label_20:
        this.SetGuildDontNotMoveButton();
      }
label_21:
      this.ngxScroll.ResolvePosition();
    }
  }

  private void SetGuildDontNotMoveButton()
  {
    if (this.buttonApplicants.transform.parent.gameObject.activeSelf)
    {
      this.buttonApplicants.isEnabled = false;
      this.lblUnavailableApplicants.transform.parent.gameObject.SetActive(true);
    }
    this.buttonLeave.isEnabled = false;
    this.lblUnavailableLeave.transform.parent.gameObject.SetActive(true);
    if (this.buttonDismiss.transform.parent.gameObject.activeSelf)
    {
      this.buttonDismiss.isEnabled = false;
      this.lblUnavailableDismiss.transform.parent.gameObject.SetActive(true);
    }
    if (this.buttonDefenseMember.transform.parent.gameObject.activeSelf)
    {
      this.buttonDefenseMember.isEnabled = false;
      this.lblUnavailableDefenseMember.transform.parent.gameObject.SetActive(true);
    }
    if (!this.buttonResearch.transform.parent.gameObject.activeSelf)
      return;
    this.buttonResearch.isEnabled = false;
    this.buttonResearchInBattleText.SetActive(true);
  }

  public void Initialize()
  {
  }

  private IEnumerator ResourceLoad()
  {
    Future<GameObject> fgObj;
    IEnumerator e;
    if ((UnityEngine.Object) this.guildSettingPopup == (UnityEngine.Object) null)
    {
      fgObj = Res.Prefabs.popup.popup_028_guild_option_edit__anim_popup01.Load<GameObject>();
      e = fgObj.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.guildSettingPopup = fgObj.Result;
      fgObj = (Future<GameObject>) null;
    }
    if ((UnityEngine.Object) this.guildSettingConfirmPopup == (UnityEngine.Object) null)
    {
      fgObj = Res.Prefabs.popup.popup_028_guild_option_edit_confirm__anim_popup01.Load<GameObject>();
      e = fgObj.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.guildSettingConfirmPopup = fgObj.Result;
      fgObj = (Future<GameObject>) null;
    }
    if ((UnityEngine.Object) this.guildBrakeupUnavailablePopup == (UnityEngine.Object) null)
    {
      fgObj = Res.Prefabs.popup.popup_028_3_1__anim_popup01.Load<GameObject>();
      e = fgObj.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.guildBrakeupUnavailablePopup = fgObj.Result;
      this.InitWidgetAlpha((MonoBehaviour) this.guildBrakeupUnavailablePopup.GetComponent<Guild02831Popup>());
      fgObj = (Future<GameObject>) null;
    }
    if ((UnityEngine.Object) this.guildBrakeupPopup == (UnityEngine.Object) null)
    {
      fgObj = Res.Prefabs.popup.popup_028_3_2_1__anim_popup01.Load<GameObject>();
      e = fgObj.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.guildBrakeupPopup = fgObj.Result;
      this.InitWidgetAlpha((MonoBehaviour) this.guildBrakeupPopup.GetComponent<Guild028321Popup>());
      fgObj = (Future<GameObject>) null;
    }
    if ((UnityEngine.Object) this.guildBrakeupConfirmPopup == (UnityEngine.Object) null)
    {
      fgObj = Res.Prefabs.popup.popup_028_3_2_2__anim_popup01.Load<GameObject>();
      e = fgObj.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.guildBrakeupConfirmPopup = fgObj.Result;
      this.InitWidgetAlpha((MonoBehaviour) this.guildBrakeupConfirmPopup.GetComponent<Guild028322Popup>());
      fgObj = (Future<GameObject>) null;
    }
    if ((UnityEngine.Object) this.guildResignUnavailablePopup == (UnityEngine.Object) null)
    {
      fgObj = Res.Prefabs.popup.popup_028_3_3__anim_popup01.Load<GameObject>();
      e = fgObj.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.guildResignUnavailablePopup = fgObj.Result;
      this.InitWidgetAlpha((MonoBehaviour) this.guildResignUnavailablePopup.GetComponent<Guild02833Popup>());
      fgObj = (Future<GameObject>) null;
    }
    if ((UnityEngine.Object) this.guildResignPopup == (UnityEngine.Object) null)
    {
      fgObj = Res.Prefabs.popup.popup_028_3_4_1__anim_popup01.Load<GameObject>();
      e = fgObj.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.guildResignPopup = fgObj.Result;
      this.InitWidgetAlpha((MonoBehaviour) this.guildResignPopup.GetComponent<Guild028341Popup>());
      fgObj = (Future<GameObject>) null;
    }
    if ((UnityEngine.Object) this.guildResignConfirmPopup == (UnityEngine.Object) null)
    {
      fgObj = Res.Prefabs.popup.popup_028_3_4_2__anim_popup01.Load<GameObject>();
      e = fgObj.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.guildResignConfirmPopup = fgObj.Result;
      this.InitWidgetAlpha((MonoBehaviour) this.guildResignConfirmPopup.GetComponent<Guild028342Popup>());
      fgObj = (Future<GameObject>) null;
    }
    if ((UnityEngine.Object) this.guildNgWordPopup == (UnityEngine.Object) null)
    {
      fgObj = Res.Prefabs.popup.popup_028_guild_ng_word__anim_popup01.Load<GameObject>();
      e = fgObj.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.guildNgWordPopup = fgObj.Result;
      this.InitWidgetAlpha((MonoBehaviour) this.guildNgWordPopup.GetComponent<Guild028NgWordPopup>());
      fgObj = (Future<GameObject>) null;
    }
    if ((UnityEngine.Object) this.commonOkPopup == (UnityEngine.Object) null)
    {
      fgObj = Res.Prefabs.popup.popup_028_guild_common_ok__anim_popup01.Load<GameObject>();
      e = fgObj.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.commonOkPopup = fgObj.Result;
      fgObj = (Future<GameObject>) null;
    }
    if ((UnityEngine.Object) this.defenseMemberSelectPopup == (UnityEngine.Object) null)
    {
      fgObj = Res.Prefabs.popup.popup_028_guild_defense_member_select__anim_popup01.Load<GameObject>();
      e = fgObj.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.defenseMemberSelectPopup = fgObj.Result;
      fgObj = (Future<GameObject>) null;
    }
    if ((UnityEngine.Object) this.memberPrefab == (UnityEngine.Object) null)
    {
      fgObj = Res.Prefabs.guild.guild_member_list.Load<GameObject>();
      e = fgObj.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.memberPrefab = fgObj.Result;
      fgObj = (Future<GameObject>) null;
    }
  }

  private void InitWidgetAlpha(MonoBehaviour component)
  {
    if (!((UnityEngine.Object) component.GetComponent<UIWidget>() != (UnityEngine.Object) null))
      return;
    component.GetComponent<UIWidget>().alpha = 0.0f;
  }

  public override void onBackButton()
  {
    if (this.IsPushAndSet())
      return;
    this.backScene();
  }

  public void onGuildTitleButton()
  {
    Guild0284Scene.ChangeScene();
  }

  private IEnumerator ShowGuildOption()
  {
    // ISSUE: reference to a compiler-generated field
    int num = this.\u003C\u003E1__state;
    Guild0283Menu menu = this;
    Guild028OptionEditPopup optionEditPopup;
    if (num != 0)
    {
      if (num != 1)
        return false;
      // ISSUE: reference to a compiler-generated field
      this.\u003C\u003E1__state = -1;
      optionEditPopup.SetPulldownEventCallback();
      return false;
    }
    // ISSUE: reference to a compiler-generated field
    this.\u003C\u003E1__state = -1;
    GameObject prefab = menu.guildSettingPopup.Clone((Transform) null);
    optionEditPopup = prefab.GetComponent<Guild028OptionEditPopup>();
    prefab.SetActive(false);
    optionEditPopup.Initialize(menu);
    prefab.SetActive(true);
    Singleton<PopupManager>.GetInstance().open(prefab, false, false, true, true, false, false, "SE_1006");
    // ISSUE: reference to a compiler-generated field
    this.\u003C\u003E2__current = (object) null;
    // ISSUE: reference to a compiler-generated field
    this.\u003C\u003E1__state = 1;
    return true;
  }

  public void onGuildOptionButton()
  {
    this.StartCoroutine(this.ShowGuildOption());
  }

  public void onMemberRequestCheckButton()
  {
    Guild0285Scene.ChangeScene();
  }

  public void onGuildResignButton()
  {
    GuildRole? role = PlayerAffiliation.Current.role;
    GuildRole guildRole = GuildRole.master;
    if (role.GetValueOrDefault() == guildRole & role.HasValue)
      Singleton<PopupManager>.GetInstance().open(this.guildResignUnavailablePopup, false, false, false, true, false, false, "SE_1006").GetComponent<Guild02833Popup>().Initialize();
    else
      Singleton<PopupManager>.GetInstance().open(this.guildResignPopup, false, false, false, true, false, false, "SE_1006").GetComponent<Guild028341Popup>().Initialize(this);
  }

  public void onGuildBrakeupButton()
  {
    if (PlayerAffiliation.Current.guild.memberships.Length >= 2)
      Singleton<PopupManager>.GetInstance().open(this.guildBrakeupUnavailablePopup, false, false, false, true, false, false, "SE_1006").GetComponent<Guild02831Popup>().Initialize();
    else
      Singleton<PopupManager>.GetInstance().open(this.guildBrakeupPopup, false, false, false, true, false, false, "SE_1006").GetComponent<Guild028321Popup>().Initialize(this);
  }

  public void onGuildDefenseMember()
  {
    this.StartCoroutine(this.ShowDefenseMemberSelectList());
  }

  private IEnumerator ShowDefenseMemberSelectList()
  {
    GameObject clone = this.DefenseMemberSelectPopup.Clone((Transform) null);
    clone.SetActive(false);
    IEnumerator e = clone.GetComponent<GuildDefenseMemberListPopup>().Initialize(this.memberPrefab, PlayerAffiliation.Current.guild);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    GameObject obj = Singleton<PopupManager>.GetInstance().open(clone, false, false, true, true, false, false, "SE_1006");
    while (!((UnityEngine.Object) obj == (UnityEngine.Object) null))
      yield return (object) null;
  }

  public void onGuildResearch()
  {
    if (this.IsPushAndSet())
      return;
    Guild02812Scene.ChangeScene();
  }

  public void showOkPopup(string title, string message, System.Action ok = null)
  {
    Singleton<PopupManager>.GetInstance().open(this.commonOkPopup, false, false, false, true, false, false, "SE_1006").GetComponent<GuildOkPopup>().Initialize(title, message, new Vector2?(), ok);
  }
}
