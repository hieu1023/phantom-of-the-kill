﻿// Decompiled with JetBrains decompiler
// Type: Unit004ReincarnationTypeTicketSelectionCoupon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System.Collections;
using UnityEngine;

public class Unit004ReincarnationTypeTicketSelectionCoupon : MonoBehaviour
{
  [SerializeField]
  private UILabel txtTitle_;
  [SerializeField]
  private GameObject linkThum_;
  [SerializeField]
  private UILabel txtLimitDate_;
  [SerializeField]
  private UILabel txtNeed_;
  [SerializeField]
  private UILabel txtProgress_;
  [SerializeField]
  private UILabel txtProgressRed_;
  private Unit004ReincarnationTypeTicketSelectionMenu menu_;
  private PlayerUnitTypeTicket coupon_;
  private UnitTypeTicket ticket_;

  public IEnumerator coInitialize(
    Unit004ReincarnationTypeTicketSelectionMenu menu,
    PlayerUnitTypeTicket coupon)
  {
    this.menu_ = menu;
    this.coupon_ = coupon;
    this.ticket_ = MasterData.UnitTypeTicket[coupon.ticket_id];
    Future<GameObject> ldicon = Res.Icons.UniqueIconPrefab.Load<GameObject>();
    yield return (object) ldicon.Wait();
    GameObject result = ldicon.Result;
    if ((Object) result != (Object) null)
    {
      UniqueIcons ticketIcon = result.Clone(this.linkThum_.transform).GetComponent<UniqueIcons>();
      yield return (object) ticketIcon.SetReincarnationTypeTicket(this.ticket_.ID, 0);
      ticketIcon.LabelActivated = false;
      ticketIcon.BackGroundActivated = false;
      ticketIcon = (UniqueIcons) null;
    }
    this.txtTitle_.SetTextLocalize(this.ticket_.name);
    this.txtLimitDate_.SetTextLocalize(string.Format(Consts.GetInstance().UNIT_004_REINCARNATION_TYPE_TICKET_END_AT, (object) this.ticket_.end_at));
    this.txtNeed_.SetTextLocalize(this.ticket_.cost);
    if (coupon.quantity >= this.ticket_.cost)
    {
      this.txtProgress_.SetTextLocalize(coupon.quantity);
      this.txtProgress_.gameObject.SetActive(true);
      this.txtProgressRed_.gameObject.SetActive(false);
    }
    else
    {
      this.txtProgress_.gameObject.SetActive(false);
      this.txtProgressRed_.SetTextLocalize(coupon.quantity);
      this.txtProgressRed_.gameObject.SetActive(true);
    }
  }

  public void onClickedSelect()
  {
    this.menu_.selectedCoupon(this.ticket_);
  }

  public void onClickedDescription()
  {
    this.menu_.onClickedDescription(this.ticket_);
  }
}
