﻿// Decompiled with JetBrains decompiler
// Type: Popup004ExtraSkillEquipedByOthers
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System.Collections;
using UnityEngine;

public class Popup004ExtraSkillEquipedByOthers : BackButtonMenuBase
{
  [SerializeField]
  private GameObject linkCharacter;
  [SerializeField]
  private UILabel txtDescription2;
  private System.Action<PlayerAwakeSkill, PlayerUnit, PlayerUnit> decideAction;
  private PlayerAwakeSkill targetSkill;
  private PlayerUnit targetUnit;
  private PlayerUnit nowEquipmentUnit;

  public IEnumerator Init(
    PlayerAwakeSkill skill,
    PlayerUnit nowEquipUnit,
    PlayerUnit targetUnit,
    System.Action<PlayerAwakeSkill, PlayerUnit, PlayerUnit> decideAct)
  {
    this.targetSkill = skill;
    this.targetUnit = targetUnit;
    this.nowEquipmentUnit = nowEquipUnit;
    this.decideAction = decideAct;
    Future<GameObject> prefabF = Res.Prefabs.UnitIcon.normal.Load<GameObject>();
    IEnumerator e = prefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    UnitIcon unitIconScript = prefabF.Result.CloneAndGetComponent<UnitIcon>(this.linkCharacter.transform);
    UnitUnit unit = nowEquipUnit.unit;
    e = unitIconScript.SetUnit(unit, unit.GetElement(), false);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    unitIconScript.SetRarities(nowEquipUnit);
    unitIconScript.setLevelText(nowEquipUnit);
    unitIconScript.PlayerUnit = nowEquipUnit;
    unitIconScript.ShowBottomInfo(UnitSortAndFilter.SORT_TYPES.Level);
    this.txtDescription2.SetTextLocalize(Consts.Format(Consts.GetInstance().popup_004_ExtraSkillEquipByOthers_Description_text, (IDictionary) new Hashtable()
    {
      {
        (object) "name",
        (object) nowEquipUnit.unit.name
      }
    }));
  }

  public void IbtnDecide()
  {
    Singleton<PopupManager>.GetInstance().onDismiss();
    if (this.decideAction == null)
      return;
    this.decideAction(this.targetSkill, this.nowEquipmentUnit, this.targetUnit);
  }

  public void IbtnCancle()
  {
    Singleton<PopupManager>.GetInstance().onDismiss();
  }

  public override void onBackButton()
  {
    this.IbtnCancle();
  }
}
