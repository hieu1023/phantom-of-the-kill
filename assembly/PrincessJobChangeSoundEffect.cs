﻿// Decompiled with JetBrains decompiler
// Type: PrincessJobChangeSoundEffect
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class PrincessJobChangeSoundEffect : PrincessEvolutionSoundEffect
{
  public string voiceFile { get; set; }

  public string selectorLabel { get; set; }

  public string voiceCompleted { get; set; }

  public void VoiceCompleted()
  {
    NGSoundManager instance = Singleton<NGSoundManager>.GetInstance();
    if ((Object) instance == (Object) null || string.IsNullOrEmpty(this.voiceFile) || string.IsNullOrEmpty(this.voiceCompleted))
      return;
    instance.playVoiceByStringID(this.voiceFile, this.voiceCompleted, -1, 0.0f, this.selectorLabel);
  }
}
