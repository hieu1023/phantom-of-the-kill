﻿// Decompiled with JetBrains decompiler
// Type: Unit00486Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Unit00486Menu : UnitSelectMenuBase
{
  private List<int> lockMaterialIds = new List<int>();
  [SerializeField]
  protected UILabel TxtTitle;
  [SerializeField]
  protected UIButton ibtnEnter;
  [Header("上昇前現在値ラベル")]
  [SerializeField]
  protected UILabel TxtBeforeNowHP;
  [SerializeField]
  protected UILabel TxtBeforeNowPower;
  [SerializeField]
  protected UILabel TxtBeforeNowMagic;
  [SerializeField]
  protected UILabel TxtBeforeNowProtect;
  [SerializeField]
  protected UILabel TxtBeforeNowSprit;
  [SerializeField]
  protected UILabel TxtBeforeNowSpeed;
  [SerializeField]
  protected UILabel TxtBeforeNowTechnique;
  [SerializeField]
  protected UILabel TxtBeforeNowLucky;
  [Header("上昇値ラベル")]
  [SerializeField]
  protected UILabel TxtIncHP;
  [SerializeField]
  protected UILabel TxtIncPower;
  [SerializeField]
  protected UILabel TxtIncMagic;
  [SerializeField]
  protected UILabel TxtIncProtect;
  [SerializeField]
  protected UILabel TxtIncSprit;
  [SerializeField]
  protected UILabel TxtIncSpeed;
  [SerializeField]
  protected UILabel TxtIncTechnique;
  [SerializeField]
  protected UILabel TxtIncLucky;
  private UnitTypeParameter unitTypeParameter;
  private List<UnitIconInfo> firstSelectedUnitIcons;
  protected Player player;
  protected int playerUnitMax;
  private PlayerMaterialUnit[] materials;
  public float currentMaxTrust;
  private bool singleCancelUpdateInfomation;
  private UnitIconInfo currentUnitInfo;

  public Player Player
  {
    get
    {
      return this.player;
    }
  }

  public float selectedUnityValue { get; protected set; }

  public float selectedBuildupUnityValue { get; protected set; }

  public virtual IEnumerator Init(
    Player player,
    PlayerUnit basePlayerUnit,
    PlayerUnit[] playerUnits,
    PlayerMaterialUnit[] playerMaterialUnits,
    PlayerUnit[] selectUnits,
    PlayerDeck[] playerDeck,
    bool isEquip,
    int selMax,
    float currentMaxTrust = 0.0f)
  {
    Unit00486Menu unit00486Menu = this;
    unit00486Menu.currentMaxTrust = currentMaxTrust;
    IEnumerator e = unit00486Menu.Initialize();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    unit00486Menu.player = player;
    unit00486Menu.baseUnit = basePlayerUnit;
    unit00486Menu.playerUnitMax = playerUnits.Length;
    unit00486Menu.unitTypeParameter = unit00486Menu.baseUnit.UnitTypeParameter;
    unit00486Menu.SelectMax = selMax;
    unit00486Menu.InitializeInfoEx((IEnumerable<PlayerUnit>) ((IEnumerable<PlayerUnit>) playerUnits).Where<PlayerUnit>((Func<PlayerUnit, bool>) (x => x != this.baseUnit && this.isComposeUnit(this.baseUnit, x))).ToArray<PlayerUnit>(), (IEnumerable<PlayerMaterialUnit>) ((IEnumerable<PlayerMaterialUnit>) playerMaterialUnits).Where<PlayerMaterialUnit>((Func<PlayerMaterialUnit, bool>) (x => this.isComposeUnit(this.baseUnit, x))).ToArray<PlayerMaterialUnit>(), Persist.unit00486SortAndFilter, isEquip, false, true, true, true, false, (System.Action) (() => this.InitializeAllUnitInfosExtend(playerDeck)));
    unit00486Menu.CreateSelectIconInfo(selectUnits, true);
    e = unit00486Menu.CreateUnitIcon();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    unit00486Menu.UpdateInfomation();
    unit00486Menu.InitializeEnd();
  }

  public override IEnumerator Initialize()
  {
    Unit00486Menu unit00486Menu = this;
    Singleton<CommonRoot>.GetInstance().isTouchBlock = true;
    Future<GameObject> prefabF = Res.Prefabs.UnitIcon.detail.Load<GameObject>();
    IEnumerator e = prefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    unit00486Menu.unitPrefab = prefabF.Result;
    unit00486Menu.sortType = UnitSortAndFilter.SORT_TYPES.BranchOfAnArmy;
    unit00486Menu.orderType = SortAndFilter.SORT_TYPE_ORDER_BUY.ASCENDING;
    unit00486Menu.isInitialize = false;
    unit00486Menu.scroll.Clear();
  }

  public IEnumerator UpdateInfoAndScrollExtend(
    PlayerUnit[] playerUnits,
    PlayerMaterialUnit[] playerMaterialUnits,
    PlayerUnit[] materialUnits)
  {
    Unit00486Menu unit00486Menu = this;
    IEnumerator e = unit00486Menu.UpdateInfoAndScroll(playerUnits, playerMaterialUnits);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    unit00486Menu.CreateSelectIconInfo(materialUnits, false);
  }

  public virtual void SetUpperParamterLabel(
    UILabel label,
    int composeParam,
    int maxParam,
    int incParam)
  {
    if (maxParam > composeParam + incParam)
    {
      if (incParam > 0)
        label.color = Color.yellow;
      else
        label.color = Color.white;
    }
    else
      label.color = Color.red;
    int num = composeParam + incParam;
    if (num == 0)
      label.SetTextLocalize(num);
    else
      label.SetTextLocalize(Consts.Format(Consts.GetInstance().UNIT486_PLUS, (IDictionary) new Hashtable()
      {
        {
          (object) "point",
          (object) num
        }
      }));
  }

  private void SetBeforeUpperNowParameter()
  {
    this.TxtBeforeNowHP.SetTextLocalize(this.baseUnit.self_total_hp);
    this.TxtBeforeNowPower.SetTextLocalize(this.baseUnit.self_total_strength);
    this.TxtBeforeNowMagic.SetTextLocalize(this.baseUnit.self_total_intelligence);
    this.TxtBeforeNowProtect.SetTextLocalize(this.baseUnit.self_total_vitality);
    this.TxtBeforeNowSprit.SetTextLocalize(this.baseUnit.self_total_mind);
    this.TxtBeforeNowSpeed.SetTextLocalize(this.baseUnit.self_total_agility);
    this.TxtBeforeNowTechnique.SetTextLocalize(this.baseUnit.self_total_dexterity);
    this.TxtBeforeNowLucky.SetTextLocalize(this.baseUnit.self_total_lucky);
  }

  protected virtual void SetUpperParameter(PlayerUnit[] materialPlayerUnits)
  {
    int composeValue1 = CalcUnitCompose.getComposeValue(this.baseUnit, materialPlayerUnits, CalcUnitCompose.ComposeType.HP);
    int composeValue2 = CalcUnitCompose.getComposeValue(this.baseUnit, materialPlayerUnits, CalcUnitCompose.ComposeType.STRENGTH);
    int composeValue3 = CalcUnitCompose.getComposeValue(this.baseUnit, materialPlayerUnits, CalcUnitCompose.ComposeType.INTELLIGENCE);
    int composeValue4 = CalcUnitCompose.getComposeValue(this.baseUnit, materialPlayerUnits, CalcUnitCompose.ComposeType.VITALITY);
    int composeValue5 = CalcUnitCompose.getComposeValue(this.baseUnit, materialPlayerUnits, CalcUnitCompose.ComposeType.MIND);
    int composeValue6 = CalcUnitCompose.getComposeValue(this.baseUnit, materialPlayerUnits, CalcUnitCompose.ComposeType.AGILITY);
    int composeValue7 = CalcUnitCompose.getComposeValue(this.baseUnit, materialPlayerUnits, CalcUnitCompose.ComposeType.DEXTERITY);
    int composeValue8 = CalcUnitCompose.getComposeValue(this.baseUnit, materialPlayerUnits, CalcUnitCompose.ComposeType.LUCKY);
    this.selectedUnityValue = Mathf.Min(CalcUnitCompose.getComposeUnity(this.baseUnit, materialPlayerUnits, true), (float) PlayerUnit.GetUnityValueMax());
    this.selectedBuildupUnityValue = Mathf.Min(CalcUnitCompose.getComposeUnity(this.baseUnit, materialPlayerUnits, false), (float) PlayerUnit.GetUnityValueMax());
    this.SetUpperParamterLabel(this.TxtIncHP, this.baseUnit.hp.compose, this.baseUnit.compose_hp_max, composeValue1);
    this.SetUpperParamterLabel(this.TxtIncPower, this.baseUnit.strength.compose, this.baseUnit.compose_strength_max, composeValue2);
    this.SetUpperParamterLabel(this.TxtIncMagic, this.baseUnit.intelligence.compose, this.baseUnit.compose_intelligence_max, composeValue3);
    this.SetUpperParamterLabel(this.TxtIncProtect, this.baseUnit.vitality.compose, this.baseUnit.compose_vitality_max, composeValue4);
    this.SetUpperParamterLabel(this.TxtIncSprit, this.baseUnit.mind.compose, this.baseUnit.compose_mind_max, composeValue5);
    this.SetUpperParamterLabel(this.TxtIncSpeed, this.baseUnit.agility.compose, this.baseUnit.compose_agility_max, composeValue6);
    this.SetUpperParamterLabel(this.TxtIncTechnique, this.baseUnit.dexterity.compose, this.baseUnit.compose_dexterity_max, composeValue7);
    this.SetUpperParamterLabel(this.TxtIncLucky, this.baseUnit.lucky.compose, this.baseUnit.compose_lucky_max, composeValue8);
  }

  protected virtual void SetPrice(PlayerUnit[] materialPlayerUnits)
  {
    long num = CalcUnitCompose.priceCompose(this.baseUnit, materialPlayerUnits);
    this.TxtNumberzeny.SetTextLocalize(num.ToString());
    if (num > this.player.money)
    {
      this.TxtNumberzeny.color = Color.red;
      this.ibtnEnter.isEnabled = false;
    }
    else
    {
      this.TxtNumberzeny.color = Color.white;
      if (materialPlayerUnits.Length == 0)
        return;
      this.ibtnEnter.isEnabled = true;
    }
  }

  protected override void UnitInfoUpdate(UnitIconInfo info, bool enable, int cnt)
  {
    info.gray = enable;
    if (info.unit.IsNormalUnit)
      info.tempSelectedCount = cnt != -1 ? 1 : 0;
    if (cnt == -1)
      info.ComposeUnSelect();
    else
      info.ComposeSelect();
  }

  public void resetCurrentUnitInfo()
  {
    this.currentUnitInfo = (UnitIconInfo) null;
    this.singleCancelUpdateInfomation = false;
  }

  public override void UpdateInfomation()
  {
    if (this.singleCancelUpdateInfomation)
    {
      this.singleCancelUpdateInfomation = false;
    }
    else
    {
      List<PlayerUnit> playerUnitList = new List<PlayerUnit>();
      foreach (UnitIconInfo selectedUnitIcon in this.selectedUnitIcons)
      {
        if (this.currentUnitInfo != selectedUnitIcon)
        {
          for (int index = 0; index < selectedUnitIcon.tempSelectedCount; ++index)
            playerUnitList.Add(selectedUnitIcon.playerUnit);
        }
      }
      PlayerUnit[] array = playerUnitList.ToArray();
      this.SetPrice(array);
      this.SetBeforeUpperNowParameter();
      this.SetUpperParameter(array);
      this.TxtNumberselect.SetTextLocalize(string.Format("{0}/{1}", (object) this.selectedUnitIcons.Count, (object) this.SelectMax));
      this.TxtNumberselect.color = this.selectedUnitIcons.Count > this.SelectMax - 1 ? Color.red : Color.white;
      this.TxtNumberpossession.SetTextLocalize(string.Format("{0}/{1}", (object) this.playerUnitMax, (object) this.player.max_units));
    }
  }

  public virtual bool isComposeUnit(PlayerUnit baseUnit, PlayerUnit unit)
  {
    UnitUnit unit_unit = unit.unit;
    if (unit_unit.IsBuildup)
      return false;
    UnitUnit base_unit = baseUnit.unit;
    if (unit_unit.same_character_id == base_unit.same_character_id && base_unit.rarity.index <= unit_unit.rarity.index || unit_unit.character_UnitCharacter == base_unit.character_UnitCharacter && (double) baseUnit.trust_rate < (double) baseUnit.trust_max_rate)
      return true;
    IEnumerable<PlayerUnitSkills> source = ((IEnumerable<PlayerUnitSkills>) baseUnit.skills).Where<PlayerUnitSkills>((Func<PlayerUnitSkills, bool>) (x => x.level < x.skill.upper_level));
    bool isSkill = source.Any<PlayerUnitSkills>((Func<PlayerUnitSkills, bool>) (unitBase => ((IEnumerable<PlayerUnitSkills>) unit.skills).Count<PlayerUnitSkills>((Func<PlayerUnitSkills, bool>) (x => unitBase.skill_id == x.skill_id)) > 0));
    if (unit_unit.same_character_id == base_unit.same_character_id && source.Count<PlayerUnitSkills>() > 0)
      isSkill = true;
    else
      ((IEnumerable<PlayerUnitSkills>) baseUnit.skills).Where<PlayerUnitSkills>((Func<PlayerUnitSkills, bool>) (x => x.level < x.skill.upper_level)).ForEach<PlayerUnitSkills>((System.Action<PlayerUnitSkills>) (y =>
      {
        if (base_unit.same_character_id == unit_unit.same_character_id)
          return;
        int[] array1 = ((IEnumerable<PlayerUnitSkills>) unit.skills).Where<PlayerUnitSkills>((Func<PlayerUnitSkills, bool>) (z => y.skill_id == z.skill_id)).Select<PlayerUnitSkills, int>((Func<PlayerUnitSkills, int>) (x => x.skill_id)).ToArray<int>();
        if (array1.Length == 0)
          return;
        int[] array2 = ((IEnumerable<UnitSkill>) base_unit.RememberUnitSkills(baseUnit._unit_type)).Select<UnitSkill, int>((Func<UnitSkill, int>) (x => x.ID)).ToArray<int>();
        for (int index = 0; index < array1.Length; ++index)
        {
          if (((IEnumerable<int>) array2).Contains<int>(array1[index]))
          {
            isSkill = true;
            break;
          }
        }
      }));
    bool flag1 = false;
    if (unit_unit.hp_compose != 0 && baseUnit.hp.compose < baseUnit.compose_hp_max)
      flag1 = true;
    else if (unit_unit.strength_compose != 0 && baseUnit.strength.compose < baseUnit.compose_strength_max)
      flag1 = true;
    else if (unit_unit.vitality_compose != 0 && baseUnit.vitality.compose < baseUnit.compose_vitality_max)
      flag1 = true;
    else if (unit_unit.intelligence_compose != 0 && baseUnit.intelligence.compose < baseUnit.compose_intelligence_max)
      flag1 = true;
    else if (unit_unit.mind_compose != 0 && baseUnit.mind.compose < baseUnit.compose_mind_max)
      flag1 = true;
    else if (unit_unit.agility_compose != 0 && baseUnit.agility.compose < baseUnit.compose_agility_max)
      flag1 = true;
    else if (unit_unit.dexterity_compose != 0 && baseUnit.dexterity.compose < baseUnit.compose_dexterity_max)
      flag1 = true;
    else if ((base_unit.same_character_id == unit_unit.same_character_id || unit_unit.lucky_compose != 0) && baseUnit.lucky.compose < baseUnit.compose_lucky_max)
      flag1 = true;
    bool flag2 = false;
    bool flag3 = false;
    if (baseUnit.breakthrough_count < base_unit.breakthrough_limit)
    {
      if (unit_unit.IsBreakThrough)
        flag2 = unit_unit.CheckBreakThroughMaterial(baseUnit);
      else
        flag3 = base_unit.same_character_id == unit_unit.same_character_id && base_unit.rarity.index <= unit_unit.rarity.index;
    }
    bool flag4 = UnitDetailIcon.IsSkillUpMaterial(unit_unit, baseUnit);
    bool flag5 = false;
    bool flag6 = false;
    bool flag7 = false;
    if (unit_unit.same_character_id == base_unit.same_character_id)
    {
      if (base_unit.trust_target_flag && (double) baseUnit.trust_rate < (double) baseUnit.trust_max_rate)
        flag5 = true;
      if (baseUnit.unity_value < PlayerUnit.GetUnityValueMax())
        flag6 = true;
      if (baseUnit.lucky.compose < this.unitTypeParameter.lucky_compose_max)
        flag7 = true;
    }
    return isSkill | flag1 | flag2 | flag4 | flag3 | flag5 | flag6 | flag7;
  }

  public virtual bool isComposeUnit(PlayerUnit baseUnit, PlayerMaterialUnit unit)
  {
    UnitUnit unit1 = unit.unit;
    if (unit1.IsBuildup)
      return false;
    bool flag1 = false;
    UnitUnit unit2 = baseUnit.unit;
    if (unit1.hp_compose != 0 && baseUnit.hp.compose < baseUnit.compose_hp_max)
      flag1 = true;
    else if (unit1.strength_compose != 0 && baseUnit.strength.compose < baseUnit.compose_strength_max)
      flag1 = true;
    else if (unit1.vitality_compose != 0 && baseUnit.vitality.compose < baseUnit.compose_vitality_max)
      flag1 = true;
    else if (unit1.intelligence_compose != 0 && baseUnit.intelligence.compose < baseUnit.compose_intelligence_max)
      flag1 = true;
    else if (unit1.mind_compose != 0 && baseUnit.mind.compose < baseUnit.compose_mind_max)
      flag1 = true;
    else if (unit1.agility_compose != 0 && baseUnit.agility.compose < baseUnit.compose_agility_max)
      flag1 = true;
    else if (unit1.dexterity_compose != 0 && baseUnit.dexterity.compose < baseUnit.compose_dexterity_max)
      flag1 = true;
    else if ((unit2.same_character_id == unit1.same_character_id || unit1.lucky_compose != 0) && baseUnit.lucky.compose < baseUnit.compose_lucky_max)
      flag1 = true;
    bool flag2 = false;
    if (unit1.IsBreakThrough && baseUnit.breakthrough_count < unit2.breakthrough_limit)
      flag2 = unit1.CheckBreakThroughMaterial(baseUnit);
    bool flag3 = false;
    if (unit2.trust_target_flag && unit1.IsTrustMaterial(baseUnit) && (double) baseUnit.trust_rate < (double) baseUnit.trust_max_rate)
      flag3 = true;
    bool flag4 = UnitDetailIcon.IsSkillUpMaterial(unit1, baseUnit);
    bool flag5 = false;
    if (unit1.IsBuildUpMaterial(baseUnit))
      flag5 = true;
    bool flag6 = unit1.is_unity_value_up && (double) baseUnit.unityTotal + (double) this.selectedUnityValue + (double) this.selectedBuildupUnityValue < (double) PlayerUnit.GetUnityValueMax() && unit1.FindValueUpPattern(unit2, (Func<UnitFamily[]>) (() => baseUnit.Families)) != null;
    return flag1 | flag2 | flag4 | flag3 | flag5 | flag6;
  }

  protected void CreateSelectIconInfo(PlayerUnit[] selectUnits, bool updateFirst)
  {
    bool flag = this.SelectedUnitIsMax();
    this.selectedUnitIcons.Clear();
    foreach (PlayerUnit selectUnit1 in selectUnits)
    {
      PlayerUnit selectUnit = selectUnit1;
      UnitIconInfo unitIconInfo = this.allUnitInfos.FirstOrDefault<UnitIconInfo>((Func<UnitIconInfo, bool>) (y =>
      {
        if (!(y.playerUnit == selectUnit) || y.playerUnit.favorite)
          return false;
        return !y.playerUnit.unit.IsNormalUnit || OverkillersUtil.checkDelete(y.playerUnit);
      }));
      if (unitIconInfo != null)
      {
        if (unitIconInfo.playerUnit.UnitIconInfo == null)
          unitIconInfo.playerUnit.UnitIconInfo = selectUnit.UnitIconInfo;
        int num = !updateFirst ? unitIconInfo.SelectedCount : selectUnit.UnitIconInfo.SelectedCount;
        unitIconInfo.SelectedCount = num;
        unitIconInfo.tempSelectedCount = num;
        unitIconInfo.gray = true;
        if (flag && (UnityEngine.Object) unitIconInfo.icon != (UnityEngine.Object) null)
          unitIconInfo.icon.Gray = false;
        this.selectedUnitIcons.Add(unitIconInfo);
      }
    }
    this.selectedUnitIcons = this.selectedUnitIcons.OrderBy<UnitIconInfo, int>((Func<UnitIconInfo, int>) (ui => ui.select)).ToList<UnitIconInfo>();
    if (!updateFirst)
      return;
    this.firstSelectedUnitIcons = new List<UnitIconInfo>((IEnumerable<UnitIconInfo>) this.selectedUnitIcons);
  }

  public override void InitializeAllUnitInfosExtend(PlayerDeck[] playerDeck)
  {
    base.InitializeAllUnitInfosExtend(playerDeck);
    this.allUnitInfos.Where<UnitIconInfo>((Func<UnitIconInfo, bool>) (x => x.unit.IsMaterialUnit)).ForEach<UnitIconInfo>((System.Action<UnitIconInfo>) (x => x.isTrustMaterial = x.unit.IsTrustMaterial(this.baseUnit)));
  }

  protected override bool IsSelectEx(UnitIconBase unitIconBase)
  {
    UnitIconInfo unitInfoAll = this.GetUnitInfoAll(unitIconBase.PlayerUnit);
    return (!this.SelectedUnitIsMax() || this.selectedUnitIcons.Contains(unitInfoAll)) && !(unitIconBase as UnitDetailIcon).unit.IsNormalUnit;
  }

  protected override void SelectEx(UnitIconBase unitIconBase, UnitIconInfo unitIconInfo)
  {
    this.currentUnitInfo = unitIconInfo;
    this.UpdateInfomation();
    this.singleCancelUpdateInfomation = true;
    this.StartCoroutine(this.ShowUnit00468CombinePopup(unitIconBase as UnitDetailIcon, unitIconInfo));
  }

  private IEnumerator ShowUnit00468CombinePopup(
    UnitDetailIcon unitDetailIcon,
    UnitIconInfo unitIconInfo)
  {
    Unit00486Menu menu = this;
    Future<GameObject> f = new ResourceObject("Prefabs/Popup_Common/popup_BundleIntegration_Base").Load<GameObject>();
    IEnumerator e = f.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Singleton<PopupManager>.GetInstance().open(f.Result, false, false, false, true, false, false, "SE_1006").GetComponent<Unit00486CombinePopup>().Init(unitDetailIcon, unitIconInfo, menu);
  }

  protected virtual void returnScene(
    List<UnitIconInfo> list,
    PlayerUnit _basePlayerUnit,
    bool isEnter)
  {
    List<PlayerUnit> playerUnitList = new List<PlayerUnit>();
    foreach (UnitIconInfo unitIconInfo in list)
    {
      if (!unitIconInfo.playerUnit.favorite && (!unitIconInfo.playerUnit.unit.IsNormalUnit || OverkillersUtil.checkDelete(unitIconInfo.playerUnit)))
      {
        if (isEnter)
          unitIconInfo.SelectedCount = unitIconInfo.tempSelectedCount;
        unitIconInfo.isTempSelectedCount = false;
        unitIconInfo.tempSelectedCount = 0;
        playerUnitList.Add(unitIconInfo.playerUnit);
      }
    }
    if (this.exceptionBackScene == null)
      Unit00484Scene.changeScene(false, _basePlayerUnit, playerUnitList.ToArray(), Unit00468Scene.Mode.Unit0048);
    else
      Unit00484Scene.changeScene(false, _basePlayerUnit, playerUnitList.ToArray(), Unit00468Scene.Mode.Unit0048, this.exceptionBackScene);
    Singleton<NGSceneManager>.GetInstance().destroyScene("unit004_8_6");
  }

  public override void IbtnBack()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<PopupManager>.GetInstance().closeAll(false);
    this.returnScene(this.firstSelectedUnitIcons, this.baseUnit, false);
  }

  public virtual void IbtnClear()
  {
    this.IbtnClearS();
    this.allUnitInfos.ForEach((System.Action<UnitIconInfo>) (x => x.tempSelectedCount = 0));
    this.displayUnitInfos.ForEach((System.Action<UnitIconInfo>) (x => x.tempSelectedCount = 0));
    this.firstSelectedUnitIcons.Clear();
  }

  public virtual void IbtnEnter()
  {
    if (this.IsPushAndSet())
      return;
    this.StartCoroutine(this.doReturnScene());
  }

  private IEnumerator doReturnScene()
  {
    Unit00486Menu unit00486Menu = this;
    bool flag = false;
    int sameCharacterId = unit00486Menu.baseUnit.unit.same_character_id;
    foreach (UnitIconInfo selectedUnitIcon in unit00486Menu.selectedUnitIcons)
    {
      UnitUnit unit;
      if ((unit = selectedUnitIcon.playerUnit.unit).same_character_id == sameCharacterId && unit.exist_overkillers_slot)
      {
        flag = true;
        break;
      }
    }
    if (flag)
    {
      int nCommand = 0;
      Consts instance = Consts.GetInstance();
      PopupCommonNoYes.Show(instance.POPUP_ALERT_TITLE_COMBINE_OVERKILLERS_SLOTS, instance.POPUP_ALERT_MESSAGE_COMINE_OVERKILLERS_SLOTS, (System.Action) (() => nCommand = 1), (System.Action) (() => nCommand = -1), NGUIText.Alignment.Center, (string) null, NGUIText.Alignment.Left, false);
      while (nCommand == 0)
        yield return (object) null;
      if (nCommand < 0)
        yield break;
    }
    unit00486Menu.returnScene(unit00486Menu.selectedUnitIcons, unit00486Menu.baseUnit, true);
  }

  public override void onBackButton()
  {
    this.IbtnBack();
  }
}
