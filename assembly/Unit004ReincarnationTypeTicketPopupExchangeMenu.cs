﻿// Decompiled with JetBrains decompiler
// Type: Unit004ReincarnationTypeTicketPopupExchangeMenu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Unit004ReincarnationTypeTicketPopupExchangeMenu : BackButtonMenuBase
{
  private List<UIButton> pauseButtons_ = new List<UIButton>();
  [SerializeField]
  private GameObject topIconUnit_;
  [SerializeField]
  private UILabel txtName_;
  [SerializeField]
  private UIPopupList popupType_;
  [SerializeField]
  private UILabel popupValue_;
  [SerializeField]
  private UIButton btnOk_;
  [SerializeField]
  private UIButton btnCancel_;
  private UnitTypeTicket ticket_;
  private PlayerUnit playerUnit_;
  private bool isInitialized_;
  private bool isOpenPopup_;
  private UnitTypeEnum selectType_;
  private Unit004ReincarnationTypeUnitSelectionMenu menu_;

  public IEnumerator coInitialize(
    UnitTypeTicket ticket,
    PlayerUnit playerUnit,
    Unit004ReincarnationTypeUnitSelectionMenu menu)
  {
    Unit004ReincarnationTypeTicketPopupExchangeMenu popupExchangeMenu = this;
    popupExchangeMenu.isInitialized_ = false;
    popupExchangeMenu.ticket_ = ticket;
    popupExchangeMenu.playerUnit_ = playerUnit;
    popupExchangeMenu.menu_ = menu;
    Future<GameObject> ldPrefab = Res.Prefabs.UnitIcon.normal.Load<GameObject>();
    IEnumerator e = ldPrefab.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    UnitIcon ui = ldPrefab.Result.Clone(popupExchangeMenu.topIconUnit_.transform).GetComponent<UnitIcon>();
    UnitUnit unit = playerUnit.unit;
    e = ui.SetPlayerUnit(playerUnit, new PlayerUnit[0], (PlayerUnit) null, false, false);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    ui.setLevelText(playerUnit);
    ui.ShowBottomInfo(UnitSortAndFilter.SORT_TYPES.Level);
    ui.princessType.DispPrincessType(true);
    ui.buttonBoxCollider.enabled = false;
    popupExchangeMenu.txtName_.SetTextLocalize(unit.name);
    popupExchangeMenu.popupType_.items.Clear();
    foreach (MasterDataTable.UnitType unitType in MasterData.UnitTypeList)
    {
      if (unitType.Enum != UnitTypeEnum.random)
        popupExchangeMenu.popupType_.items.Add(unitType.name);
    }
    popupExchangeMenu.popupType_.value = popupExchangeMenu.popupType_.items[0];
    popupExchangeMenu.popupType_.gameObject.SetActive(true);
    popupExchangeMenu.popupValue_.SetTextLocalize(popupExchangeMenu.popupType_.value);
    // ISSUE: reference to a compiler-generated method
    popupExchangeMenu.selectType_ = ((IEnumerable<MasterDataTable.UnitType>) MasterData.UnitTypeList).FirstOrDefault<MasterDataTable.UnitType>(new Func<MasterDataTable.UnitType, bool>(popupExchangeMenu.\u003CcoInitialize\u003Eb__13_0)).Enum;
    popupExchangeMenu.setBtnOkEnable();
    popupExchangeMenu.isInitialized_ = true;
  }

  private void setBtnOkEnable()
  {
    bool flag = true;
    if (flag && this.selectType_ == this.playerUnit_.unit_type.Enum)
      flag = false;
    if (flag)
    {
      DateTime now = ServerTime.NowAppTimeAddDelta();
      IEnumerable<UnitTypeDeck> source = ((IEnumerable<UnitTypeDeck>) MasterData.UnitTypeDeckList).Where<UnitTypeDeck>((Func<UnitTypeDeck, bool>) (x =>
      {
        int? groupIdUnitUnit = x.group_id_UnitUnit;
        int id = this.playerUnit_.unit.ID;
        if (groupIdUnitUnit.GetValueOrDefault() == id & groupIdUnitUnit.HasValue)
        {
          if (x.start_at.HasValue)
          {
            DateTime? startAt = x.start_at;
            DateTime dateTime = now;
            if ((startAt.HasValue ? (startAt.GetValueOrDefault() <= dateTime ? 1 : 0) : 0) == 0)
              goto label_8;
          }
          if (!x.end_at.HasValue)
            return true;
          DateTime? endAt = x.end_at;
          DateTime dateTime1 = now;
          return endAt.HasValue && endAt.GetValueOrDefault() >= dateTime1;
        }
label_8:
        return false;
      }));
      if (source.Any<UnitTypeDeck>())
      {
        if (!source.Where<UnitTypeDeck>((Func<UnitTypeDeck, bool>) (x => (UnitTypeEnum) x.category_id == this.selectType_ && x.appearance > 0)).Any<UnitTypeDeck>())
          flag = false;
      }
      else if (!((IEnumerable<UnitTypeDeck>) MasterData.UnitTypeDeckList).Where<UnitTypeDeck>((Func<UnitTypeDeck, bool>) (x =>
      {
        if (!x.group_id_UnitUnit.HasValue)
        {
          if (x.start_at.HasValue)
          {
            DateTime? startAt = x.start_at;
            DateTime dateTime = now;
            if ((startAt.HasValue ? (startAt.GetValueOrDefault() <= dateTime ? 1 : 0) : 0) == 0)
              goto label_8;
          }
          if (!x.end_at.HasValue)
            return true;
          DateTime? endAt = x.end_at;
          DateTime dateTime1 = now;
          return endAt.HasValue && endAt.GetValueOrDefault() >= dateTime1;
        }
label_8:
        return false;
      })).Where<UnitTypeDeck>((Func<UnitTypeDeck, bool>) (x => (UnitTypeEnum) x.category_id == this.selectType_ && x.appearance > 0)).Any<UnitTypeDeck>())
        flag = false;
    }
    this.btnOk_.isEnabled = flag;
  }

  public void onPopupValueChanged()
  {
    if (!this.isInitialized_)
      return;
    string curtxt = this.popupType_.value;
    if (!this.popupType_.items.Contains(curtxt))
      return;
    this.selectType_ = ((IEnumerable<MasterDataTable.UnitType>) MasterData.UnitTypeList).FirstOrDefault<MasterDataTable.UnitType>((Func<MasterDataTable.UnitType, bool>) (x => x.name == curtxt)).Enum;
    this.setBtnOkEnable();
    this.popupValue_.SetTextLocalize(curtxt);
  }

  public void onOpenPopup()
  {
    if (this.isOpenPopup_)
      return;
    if (!this.popupType_.items.Contains(this.popupType_.value))
      this.popupType_.value = this.popupType_.items[0];
    this.isOpenPopup_ = true;
    if (this.btnOk_.isEnabled)
    {
      this.pauseButtons_.Add(this.btnOk_);
      this.btnOk_.isEnabled = false;
    }
    if (this.btnCancel_.isEnabled)
    {
      this.pauseButtons_.Add(this.btnCancel_);
      this.btnCancel_.isEnabled = false;
    }
    this.StartCoroutine("waitPopupClose");
  }

  private IEnumerator waitPopupClose()
  {
    yield return (object) null;
    while (this.popupType_.isOpen)
      yield return (object) null;
    foreach (UIButtonColor pauseButton in this.pauseButtons_)
      pauseButton.isEnabled = true;
    this.pauseButtons_.Clear();
    this.isOpenPopup_ = false;
    this.setBtnOkEnable();
  }

  private void OnDestroy()
  {
    if (!this.isOpenPopup_)
      return;
    this.isOpenPopup_ = false;
    this.StopCoroutine("waitPopupClose");
  }

  public override void onBackButton()
  {
    this.onClickCancel();
  }

  public void onClickOk()
  {
    if (this.IsPushAndSet())
      return;
    this.close();
    Unit004ReincarnationTypeScene.changeScene(true, this.ticket_, this.playerUnit_, this.selectType_);
  }

  public void onClickCancel()
  {
    if (this.IsPushAndSet())
      return;
    this.close();
  }

  public void close()
  {
    this.menu_.isPopupOpen = false;
    Singleton<PopupManager>.GetInstance().dismiss(false);
  }
}
