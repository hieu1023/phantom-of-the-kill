﻿// Decompiled with JetBrains decompiler
// Type: UniWebViewHelper
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class UniWebViewHelper
{
  public static int screenHeight
  {
    get
    {
      return Screen.height;
    }
  }

  public static int screenWidth
  {
    get
    {
      return Screen.width;
    }
  }

  public static int screenScale
  {
    get
    {
      return 1;
    }
  }

  public static string streamingAssetURLForPath(string path)
  {
    return string.Empty;
  }
}
