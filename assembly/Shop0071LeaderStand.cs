﻿// Decompiled with JetBrains decompiler
// Type: Shop0071LeaderStand
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Shop0071LeaderStand : MonoBehaviour
{
  private NGSoundManager soundManager;
  private PlayerUnit _playerUnit;

  private void OnDisable()
  {
    if (!((UnityEngine.Object) this.soundManager != (UnityEngine.Object) null))
      return;
    this.soundManager.stopVoice(-1);
  }

  public IEnumerator SetLeaderCharacter(int id, int job_id)
  {
    Shop0071LeaderStand shop0071LeaderStand = this;
    UnitUnit unitdata = MasterData.UnitUnit[id];
    Future<UnityEngine.Sprite> CharacterF = unitdata.LoadSpriteLarge(job_id, 1f);
    IEnumerator e = CharacterF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    UnityEngine.Sprite result = CharacterF.Result;
    shop0071LeaderStand.GetComponent<NGxMaskSprite>().sprite2D = result;
    shop0071LeaderStand.soundManager = Singleton<NGSoundManager>.GetInstance();
    if ((UnityEngine.Object) shop0071LeaderStand.soundManager != (UnityEngine.Object) null)
    {
      shop0071LeaderStand.soundManager.stopVoice(-1);
      shop0071LeaderStand.soundManager.playVoiceByID(unitdata.unitVoicePattern, 62, -1, 0.0f);
    }
  }

  public void SetLeaderCharacter()
  {
    UnitUnit unitUnit = ShopTopUnit.GetShopTopUnit();
    int job_id;
    if (unitUnit == null)
    {
      PlayerUnit displayPlayerUnit = this.GetDisplayPlayerUnit();
      unitUnit = displayPlayerUnit.unit;
      job_id = displayPlayerUnit.job_id;
    }
    else
      job_id = unitUnit.job_UnitJob;
    this.StartCoroutine(this.SetLeaderCharacter(unitUnit.ID, job_id));
  }

  private PlayerUnit GetDisplayPlayerUnit()
  {
    int mypage_unit_id = MypageUnitUtil.getUnitId();
    if (mypage_unit_id == 0)
      return this.GetDeckLeaderPlayerUnit();
    PlayerUnit playerUnit = ((IEnumerable<PlayerUnit>) SMManager.Get<PlayerUnit[]>()).FirstOrDefault<PlayerUnit>((Func<PlayerUnit, bool>) (x => x.id == mypage_unit_id));
    if (!(playerUnit == (PlayerUnit) null))
      return playerUnit;
    MypageUnitUtil.setDefaultUnitNotFound();
    return this.GetDeckLeaderPlayerUnit();
  }

  private PlayerUnit GetDeckLeaderPlayerUnit()
  {
    PlayerDeck[] playerDeckArray = SMManager.Get<PlayerDeck[]>();
    PlayerUnit playerUnit = ((IEnumerable<PlayerUnit>) playerDeckArray[Persist.deckOrganized.Data.number].player_units).FirstOrDefault<PlayerUnit>();
    if (playerUnit == (PlayerUnit) null)
      playerUnit = ((IEnumerable<PlayerUnit>) playerDeckArray[0].player_units).First<PlayerUnit>();
    return playerUnit;
  }
}
