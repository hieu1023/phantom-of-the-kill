﻿// Decompiled with JetBrains decompiler
// Type: Gacha00611Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;
using UnityEngine;

public class Gacha00611Scene : NGSceneBase
{
  private Gacha00611Menu menu;
  private bool isPlay;

  public static void changeScene(
    bool stack,
    bool newflag,
    int countWeapon,
    GameCore.ItemInfo revTargetData)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("gacha006_11", (stack ? 1 : 0) != 0, (object) newflag, (object) countWeapon, (object) revTargetData);
  }

  public static void ChangeScene(
    bool stack,
    bool newFlag,
    int countWeapon,
    GameCore.ItemInfo TargetData,
    GameCore.ItemInfo baseData,
    GameCore.ItemInfo TargetReisouInfo,
    GameCore.ItemInfo baseReisouInfo,
    System.Action backSceneCallback,
    bool showEquipUnit)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("gacha006_11", (stack ? 1 : 0) != 0, (object) newFlag, (object) countWeapon, (object) TargetData, (object) baseData, (object) TargetReisouInfo, (object) baseReisouInfo, (object) backSceneCallback, (object) true);
  }

  public override IEnumerator onInitSceneAsync()
  {
    Gacha00611Scene gacha00611Scene = this;
    Future<GameObject> bgF = Res.Prefabs.BackGround.GachaTopBackground.Load<GameObject>();
    IEnumerator e = bgF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    gacha00611Scene.backgroundPrefab = bgF.Result;
    gacha00611Scene.backgroundPrefab.GetComponent<UI2DSprite>().color = Consts.GetInstance().GACHA_RESULT_BACKGROUND_COLOR;
  }

  public IEnumerator onStartSceneAsync(bool NewFlag, GameCore.ItemInfo TargetData)
  {
    IEnumerator e = this.onStartSceneAsync(NewFlag, 0, TargetData);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public IEnumerator onStartSceneAsync(
    bool NewFlag,
    int countWeapon,
    GameCore.ItemInfo TargetData)
  {
    IEnumerator e = this.onStartSceneAsync(NewFlag, countWeapon, TargetData, (GameCore.ItemInfo) null, (GameCore.ItemInfo) null, (GameCore.ItemInfo) null, (System.Action) null, false);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public IEnumerator onStartSceneAsync(
    bool NewFlag,
    GameCore.ItemInfo TargetData,
    GameCore.ItemInfo baseData,
    GameCore.ItemInfo TargetReisouInfo,
    GameCore.ItemInfo baseReisouInfo,
    System.Action backSceneCallback)
  {
    IEnumerator e = this.onStartSceneAsync(NewFlag, 0, TargetData, baseData, TargetReisouInfo, baseReisouInfo, backSceneCallback, false);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public IEnumerator onStartSceneAsync(
    bool NewFlag,
    int countWeapon,
    GameCore.ItemInfo TargetData,
    GameCore.ItemInfo baseData,
    GameCore.ItemInfo TargetReisouInfo,
    GameCore.ItemInfo baseReisouInfo,
    System.Action backSceneCallback,
    bool showEquipUnit)
  {
    Gacha00611Scene gacha00611Scene = this;
    RenderSettings.ambientLight = Singleton<NGGameDataManager>.GetInstance().baseAmbientLight;
    IEnumerator e;
    if ((UnityEngine.Object) gacha00611Scene.menu == (UnityEngine.Object) null)
    {
      Future<GameObject> handler = Res.Prefabs.gacha006_11.MainPanel.Load<GameObject>();
      e = handler.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      gacha00611Scene.menu = handler.Result.Clone(gacha00611Scene.transform).GetComponent<Gacha00611Menu>();
      gacha00611Scene.menu.GetComponent<UIPanel>().SetAnchor(gacha00611Scene.transform);
      handler = (Future<GameObject>) null;
    }
    gacha00611Scene.menuBase = (NGMenuBase) gacha00611Scene.menu;
    gacha00611Scene.menuBase.IsPush = false;
    e = gacha00611Scene.menu.Initialize(TargetData, NewFlag, countWeapon, true, baseData, TargetReisouInfo, baseReisouInfo, showEquipUnit);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    gacha00611Scene.menu.SetChangeScene(backSceneCallback);
    gacha00611Scene.menu.BackSceneButton.gameObject.SetActive(true);
  }

  public void onStartScene()
  {
    Singleton<PopupManager>.GetInstance().closeAll(false);
  }

  public void onStartScene(bool NewFlag, GameCore.ItemInfo TargetData)
  {
    Singleton<PopupManager>.GetInstance().closeAll(false);
  }

  public void onStartScene(bool NewFlag, int countWeapon, GameCore.ItemInfo TargetData)
  {
    Singleton<PopupManager>.GetInstance().closeAll(false);
  }

  public void onStartScene(
    bool NewFlag,
    int countWeapon,
    GameCore.ItemInfo TargetData,
    GameCore.ItemInfo baseData,
    GameCore.ItemInfo TargetReisouInfo,
    GameCore.ItemInfo baseReisouInfo,
    System.Action backSceneCallback,
    bool showEquipUnit)
  {
    Singleton<PopupManager>.GetInstance().closeAll(false);
    this.isPlay = true;
  }

  public override void onSceneInitialized()
  {
    if (!this.isPlay)
      return;
    this.menu.StartAnime();
  }

  public override void onEndScene()
  {
    this.menu.StopAnime();
  }
}
