﻿// Decompiled with JetBrains decompiler
// Type: Unit00420Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Unit00420Menu : BackButtonMenuBase
{
  private PlayerUnit[] selectUnits = new PlayerUnit[5];
  public const int MAX_CELL_COUNT = 30;
  [Header("ベース姫情報(dir_Status01)")]
  [SerializeField]
  private GameObject[] slcPrincessTypes;
  [SerializeField]
  private GameObject dynTumn;
  [SerializeField]
  private UILabel TxtHimeEnforceNumer;
  [SerializeField]
  private UILabel TxtHimeEnforceDenom;
  [SerializeField]
  private UILabel TxtHimeEnforceMaxNumer;
  [SerializeField]
  private UIButton ibtnReset;
  [SerializeField]
  private GameObject[] StatusGaugeBases;
  [Header("素材ユニット情報(dir_Status02)")]
  [SerializeField]
  private GameObject ibtn_change_UpperParameter;
  [SerializeField]
  private UIDragScrollView uiDragScrollView;
  [SerializeField]
  private UIScrollView scrollView;
  [SerializeField]
  private GameObject scrollBar;
  [SerializeField]
  private UIGrid grid;
  [SerializeField]
  private GameObject dir_attention;
  [Header("画面下部(Bottom)")]
  [SerializeField]
  private UILabel TxtZeny;
  [SerializeField]
  private UIButton ibtnReinforceAnimFade01;
  private bool isSetfirstGridPosition;
  private Vector3 firstGridPosition;
  private GameObject rarityAlertPopupPrefab;
  private GameObject resetAlertPopupPrefab;
  private GameObject unitIconPrefab;
  private GameObject upperParameterPrefab;
  private GameObject upperParameterLabelPrefab;
  private GameObject skillTypeIconPrefab;
  private GameObject statusGaugePrefab;
  private GameObject DirSozaiItemPrefab;
  private PlayerUnit baseUnit;
  private PlayerUnit[] duplicationSelectUnits;
  public System.Action exceptionBackScene;

  public IEnumerator Init(PlayerUnit basePlayerUnit, PlayerUnit[] materialPlayerUnits)
  {
    Unit00420Menu unit00420Menu = this;
    unit00420Menu.baseUnit = basePlayerUnit;
    unit00420Menu.selectUnits = ((IEnumerable<PlayerUnit>) materialPlayerUnits).Where<PlayerUnit>((Func<PlayerUnit, bool>) (x => x != (PlayerUnit) null)).ToArray<PlayerUnit>();
    List<PlayerUnit> playerUnitList = new List<PlayerUnit>();
    foreach (PlayerUnit selectUnit in unit00420Menu.selectUnits)
    {
      for (int index = 0; index < selectUnit.UnitIconInfo.SelectedCount; ++index)
        playerUnitList.Add(selectUnit);
    }
    unit00420Menu.duplicationSelectUnits = playerUnitList.ToArray();
    IEnumerator e = unit00420Menu.LoadResource();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    yield return (object) unit00420Menu.Show_dir_Status01();
    foreach (Component component in unit00420Menu.grid.transform)
      UnityEngine.Object.Destroy((UnityEngine.Object) component.gameObject);
    if (unit00420Menu.selectUnits.Length == 0 || ((IEnumerable<PlayerUnit>) unit00420Menu.selectUnits).Count<PlayerUnit>((Func<PlayerUnit, bool>) (x => x.UnitIconInfo.SelectedCount > 0)) <= 0)
    {
      unit00420Menu.uiDragScrollView.enabled = false;
      unit00420Menu.dir_attention.SetActive(true);
      unit00420Menu.scrollBar.SetActive(false);
      unit00420Menu.ibtn_change_UpperParameter.SetActive(false);
      for (int index = 0; index < 30; ++index)
        unit00420Menu.DirSozaiItemPrefab.CloneAndGetComponent<SozaiItem>(unit00420Menu.grid.transform).SetOnlyWLine();
    }
    else
    {
      unit00420Menu.uiDragScrollView.enabled = true;
      unit00420Menu.dir_attention.SetActive(false);
      unit00420Menu.scrollBar.SetActive(true);
      unit00420Menu.ibtn_change_UpperParameter.SetActive(true);
      for (int i = 0; i < 30; ++i)
      {
        SozaiItem component1 = unit00420Menu.DirSozaiItemPrefab.CloneAndGetComponent<SozaiItem>(unit00420Menu.grid.transform);
        if (i >= unit00420Menu.selectUnits.Length)
        {
          component1.SetOnlyWLine();
        }
        else
        {
          PlayerUnit materialPlayerUnit = unit00420Menu.selectUnits[i];
          if (materialPlayerUnit.UnitIconInfo.SelectedCount <= 0)
          {
            component1.SetOnlyWLine();
          }
          else
          {
            unit00420Menu.upperParameterPrefab.CloneAndGetComponent<UpperParameter>(component1.DirSozaiBase.transform).Init(materialPlayerUnit);
            unit00420Menu.upperParameterLabelPrefab.CloneAndGetComponent<UpperParameterLabel>(component1.DirSozaiBase2.transform).Init(basePlayerUnit, materialPlayerUnit);
            UnitIcon component2 = unit00420Menu.unitIconPrefab.CloneAndGetComponent<UnitIcon>(component1.LinkSozaiBase.transform);
            component1.UnitIcon = component2;
            // ISSUE: reference to a compiler-generated method
            component2.onClick = new System.Action<UnitIconBase>(unit00420Menu.\u003CInit\u003Eb__30_2);
            component2.SelectByCheckAndNumber(materialPlayerUnit.UnitIconInfo);
            component2.setBottom(materialPlayerUnit);
            component2.ShowBottomInfo(UnitSortAndFilter.SORT_TYPES.Level);
            PlayerMaterialUnit playerMaterialUnit = ((IEnumerable<PlayerMaterialUnit>) SMManager.Get<PlayerMaterialUnit[]>()).FirstOrDefault<PlayerMaterialUnit>((Func<PlayerMaterialUnit, bool>) (x => x._unit == materialPlayerUnit.unit.ID));
            if (playerMaterialUnit != null)
            {
              component1.TxtPossessionNum.gameObject.SetActive(true);
              component1.TxtPossessionNum.SetTextLocalize(Consts.Format(Consts.GetInstance().unit_004_9_9_possession_text, (IDictionary) new Hashtable()
              {
                {
                  (object) "Count",
                  (object) playerMaterialUnit.quantity
                }
              }));
            }
            e = component2.SetMaterialUnit(materialPlayerUnit, false, materialPlayerUnits);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
          }
        }
      }
    }
    if (!unit00420Menu.isSetfirstGridPosition)
    {
      unit00420Menu.isSetfirstGridPosition = true;
      unit00420Menu.firstGridPosition = unit00420Menu.grid.transform.localPosition;
    }
    unit00420Menu.grid.transform.localPosition = unit00420Menu.firstGridPosition;
    unit00420Menu.grid.repositionNow = true;
    unit00420Menu.scrollView.ResetPosition();
    unit00420Menu.SetWidthLine();
    unit00420Menu.onIbtnChangeUpperParameter();
    unit00420Menu.TxtZeny.SetTextLocalize(CalcUnitCompose.priceStringth(basePlayerUnit, unit00420Menu.duplicationSelectUnits).ToString());
    unit00420Menu.ibtnReinforceAnimFade01.isEnabled = false;
    if (unit00420Menu.duplicationSelectUnits.Length != 0)
      unit00420Menu.ibtnReinforceAnimFade01.isEnabled = true;
  }

  private IEnumerator LoadResource()
  {
    Future<GameObject> popupPrefabF;
    IEnumerator e;
    if ((UnityEngine.Object) this.rarityAlertPopupPrefab == (UnityEngine.Object) null)
    {
      popupPrefabF = Res.Prefabs.popup.popup_004_8_3_2__anim_popup02.Load<GameObject>();
      e = popupPrefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.rarityAlertPopupPrefab = popupPrefabF.Result;
      popupPrefabF = (Future<GameObject>) null;
    }
    if ((UnityEngine.Object) this.resetAlertPopupPrefab == (UnityEngine.Object) null)
    {
      popupPrefabF = Res.Prefabs.popup.popup_004_unit_reinforce_reset__anim_popup01.Load<GameObject>();
      e = popupPrefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.resetAlertPopupPrefab = popupPrefabF.Result;
      popupPrefabF = (Future<GameObject>) null;
    }
    if ((UnityEngine.Object) this.unitIconPrefab == (UnityEngine.Object) null)
    {
      popupPrefabF = Res.Prefabs.UnitIcon.normal.Load<GameObject>();
      e = popupPrefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.unitIconPrefab = popupPrefabF.Result;
      popupPrefabF = (Future<GameObject>) null;
    }
    if ((UnityEngine.Object) this.upperParameterPrefab == (UnityEngine.Object) null)
    {
      popupPrefabF = new ResourceObject("Prefabs/UnitIcon/UpperParameter").Load<GameObject>();
      e = popupPrefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.upperParameterPrefab = popupPrefabF.Result;
      popupPrefabF = (Future<GameObject>) null;
    }
    if ((UnityEngine.Object) this.upperParameterLabelPrefab == (UnityEngine.Object) null)
    {
      popupPrefabF = new ResourceObject("Prefabs/UnitIcon/UpperParameterLabel").Load<GameObject>();
      e = popupPrefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.upperParameterLabelPrefab = popupPrefabF.Result;
      popupPrefabF = (Future<GameObject>) null;
    }
    if ((UnityEngine.Object) this.skillTypeIconPrefab == (UnityEngine.Object) null)
    {
      popupPrefabF = Res.Prefabs.BattleSkillIcon._battleSkillIcon.Load<GameObject>();
      e = popupPrefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.skillTypeIconPrefab = popupPrefabF.Result;
      popupPrefabF = (Future<GameObject>) null;
    }
    if ((UnityEngine.Object) this.statusGaugePrefab == (UnityEngine.Object) null)
    {
      popupPrefabF = Res.Prefabs.unit004_20.StatusGauge.Load<GameObject>();
      e = popupPrefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.statusGaugePrefab = popupPrefabF.Result;
      popupPrefabF = (Future<GameObject>) null;
    }
    if ((UnityEngine.Object) this.DirSozaiItemPrefab == (UnityEngine.Object) null)
    {
      popupPrefabF = new ResourceObject("Prefabs/unit004_8_4/dir_Sozai_Item").Load<GameObject>();
      e = popupPrefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.DirSozaiItemPrefab = popupPrefabF.Result;
      popupPrefabF = (Future<GameObject>) null;
    }
  }

  private IEnumerator Show_dir_Status01()
  {
    ((IEnumerable<GameObject>) this.slcPrincessTypes).ToggleOnce(this.baseUnit.unit_type.ID - 1);
    this.dynTumn.transform.Clear();
    UnitIcon unitIconScript = this.unitIconPrefab.CloneAndGetComponent<UnitIcon>(this.dynTumn);
    PlayerUnit[] playerUnits = new PlayerUnit[1]
    {
      this.baseUnit
    };
    IEnumerator e = unitIconScript.SetPlayerUnit(this.baseUnit, playerUnits, (PlayerUnit) null, true, false);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    unitIconScript.ShowBottomInfo(UnitSortAndFilter.SORT_TYPES.Level);
    unitIconScript.setLevelText(this.baseUnit);
    this.TxtHimeEnforceNumer.SetTextLocalize(this.baseUnit.buildup_count + ((IEnumerable<PlayerUnit>) this.selectUnits).Sum<PlayerUnit>((Func<PlayerUnit, int>) (x => x.UnitIconInfo.SelectedCount)));
    this.TxtHimeEnforceNumer.color = this.selectUnits.Length == 0 ? Color.white : Color.yellow;
    this.TxtHimeEnforceDenom.SetTextLocalize(this.baseUnit.buildup_limit);
    this.TxtHimeEnforceMaxNumer.SetTextLocalize(Consts.Format(Consts.GetInstance().UNIT_00420_ENFORCE_MAX_NUMER, (IDictionary) new Hashtable()
    {
      {
        (object) "max",
        (object) this.baseUnit.buildupLimitBreakCnt
      }
    }));
    this.setStatusGauge(this.statusGaugePrefab, this.duplicationSelectUnits);
  }

  private void SetWidthLine()
  {
    int num = 0;
    foreach (Transform transform in this.grid.transform)
    {
      if (num % this.grid.maxPerLine == 0)
        transform.GetComponent<SozaiItem>().WLine.SetActive(true);
      else
        transform.GetComponent<SozaiItem>().WLine.SetActive(false);
      ++num;
    }
  }

  public void IbtnBack()
  {
    if (this.IsPushAndSet())
      return;
    if (this.exceptionBackScene == null)
      this.backScene();
    else
      this.exceptionBackScene();
  }

  public override void onBackButton()
  {
    this.IbtnBack();
  }

  public void onIbtnChangeUpperParameter()
  {
    foreach (Component component1 in this.grid.transform)
    {
      SozaiItem component2 = component1.GetComponent<SozaiItem>();
      if (!((UnityEngine.Object) component2.UnitIcon == (UnityEngine.Object) null))
      {
        component2.UnitIcon.Dir_select_check.SetActive(component2.DirSozaiBase.activeSelf);
        component2.DirSozaiBase.SetActive(!component2.DirSozaiBase.activeSelf);
      }
    }
  }

  private void setStatusGauge(GameObject prefabGauge, PlayerUnit[] materialPlayerUnits)
  {
    CalcUnitCompose.ComposeType[] composeTypeArray = new CalcUnitCompose.ComposeType[8]
    {
      CalcUnitCompose.ComposeType.HP,
      CalcUnitCompose.ComposeType.STRENGTH,
      CalcUnitCompose.ComposeType.INTELLIGENCE,
      CalcUnitCompose.ComposeType.VITALITY,
      CalcUnitCompose.ComposeType.MIND,
      CalcUnitCompose.ComposeType.AGILITY,
      CalcUnitCompose.ComposeType.DEXTERITY,
      CalcUnitCompose.ComposeType.LUCKY
    };
    int[] numArray1 = new int[8]
    {
      this.baseUnit.self_total_hp,
      this.baseUnit.self_total_strength,
      this.baseUnit.self_total_intelligence,
      this.baseUnit.self_total_vitality,
      this.baseUnit.self_total_mind,
      this.baseUnit.self_total_agility,
      this.baseUnit.self_total_dexterity,
      this.baseUnit.self_total_lucky
    };
    int[] numArray2 = new int[8]
    {
      this.baseUnit.hp.level_up_max_status,
      this.baseUnit.strength.level_up_max_status,
      this.baseUnit.intelligence.level_up_max_status,
      this.baseUnit.vitality.level_up_max_status,
      this.baseUnit.mind.level_up_max_status,
      this.baseUnit.agility.level_up_max_status,
      this.baseUnit.dexterity.level_up_max_status,
      this.baseUnit.lucky.level_up_max_status
    };
    int[] numArray3 = new int[8]
    {
      this.baseUnit.hp.compose,
      this.baseUnit.strength.compose,
      this.baseUnit.intelligence.compose,
      this.baseUnit.vitality.compose,
      this.baseUnit.mind.compose,
      this.baseUnit.agility.compose,
      this.baseUnit.dexterity.compose,
      this.baseUnit.lucky.compose
    };
    int[] numArray4 = new int[8]
    {
      this.baseUnit.hp.buildup,
      this.baseUnit.strength.buildup,
      this.baseUnit.intelligence.buildup,
      this.baseUnit.vitality.buildup,
      this.baseUnit.mind.buildup,
      this.baseUnit.agility.buildup,
      this.baseUnit.dexterity.buildup,
      this.baseUnit.lucky.buildup
    };
    int[] numArray5 = new int[8]
    {
      this.baseUnit.hp.buildup,
      this.baseUnit.strength.buildup,
      this.baseUnit.intelligence.buildup,
      this.baseUnit.vitality.buildup,
      this.baseUnit.mind.buildup,
      this.baseUnit.agility.buildup,
      this.baseUnit.dexterity.buildup,
      this.baseUnit.lucky.buildup
    };
    int[] numArray6 = new int[8]
    {
      this.baseUnit.hp.buildupMaxCnt(this.baseUnit),
      this.baseUnit.strength.buildupMaxCnt(this.baseUnit),
      this.baseUnit.intelligence.buildupMaxCnt(this.baseUnit),
      this.baseUnit.vitality.buildupMaxCnt(this.baseUnit),
      this.baseUnit.mind.buildupMaxCnt(this.baseUnit),
      this.baseUnit.agility.buildupMaxCnt(this.baseUnit),
      this.baseUnit.dexterity.buildupMaxCnt(this.baseUnit),
      this.baseUnit.lucky.buildupMaxCnt(this.baseUnit)
    };
    bool[] flagArray = new bool[8]
    {
      this.baseUnit.hp.is_max,
      this.baseUnit.strength.is_max,
      this.baseUnit.intelligence.is_max,
      this.baseUnit.vitality.is_max,
      this.baseUnit.mind.is_max,
      this.baseUnit.agility.is_max,
      this.baseUnit.dexterity.is_max,
      this.baseUnit.lucky.is_max
    };
    UnitTypeParameter unitTypeParameter = this.baseUnit.UnitTypeParameter;
    int gaugeMax = ((IEnumerable<int>) new int[8]
    {
      this.baseUnit.hp.initial + this.baseUnit.hp.inheritance + this.baseUnit.hp.level_up_max_status + unitTypeParameter.hp_compose_max,
      this.baseUnit.strength.initial + this.baseUnit.strength.inheritance + this.baseUnit.strength.level_up_max_status + unitTypeParameter.strength_compose_max,
      this.baseUnit.intelligence.initial + this.baseUnit.intelligence.inheritance + this.baseUnit.intelligence.level_up_max_status + unitTypeParameter.intelligence_compose_max,
      this.baseUnit.vitality.initial + this.baseUnit.vitality.inheritance + this.baseUnit.vitality.level_up_max_status + unitTypeParameter.vitality_compose_max,
      this.baseUnit.mind.initial + this.baseUnit.mind.inheritance + this.baseUnit.mind.level_up_max_status + unitTypeParameter.mind_compose_max,
      this.baseUnit.agility.initial + this.baseUnit.agility.inheritance + this.baseUnit.agility.level_up_max_status + unitTypeParameter.agility_compose_max,
      this.baseUnit.dexterity.initial + this.baseUnit.dexterity.inheritance + this.baseUnit.dexterity.level_up_max_status + unitTypeParameter.dexterity_compose_max,
      this.baseUnit.lucky.initial + this.baseUnit.lucky.inheritance + this.baseUnit.lucky.level_up_max_status + unitTypeParameter.lucky_compose_max
    }).Max();
    int length = this.StatusGaugeBases.Length;
    for (int index = 0; index < length; ++index)
    {
      GameObject statusGaugeBase = this.StatusGaugeBases[index];
      statusGaugeBase.transform.Clear();
      prefabGauge.CloneAndGetComponent<Unit00420StatusGauge>(statusGaugeBase).Init(this.baseUnit, materialPlayerUnits, composeTypeArray[index], numArray1[index], numArray2[index], numArray3[index], numArray4[index], numArray5[index], numArray6[index], flagArray[index], gaugeMax);
      NGTween.playTweens(NGTween.findTweenersAll(statusGaugeBase, false), 0, false);
    }
  }

  public void OnChangeUnit00487Scene()
  {
    if (this.IsPushAndSet())
      return;
    foreach (Component component in this.grid.transform)
      UnityEngine.Object.Destroy((UnityEngine.Object) component.gameObject);
    Singleton<PopupManager>.GetInstance().closeAll(false);
    Unit00487Scene.changeScene(false, this.baseUnit, this.selectUnits, this.exceptionBackScene);
  }

  public void IbtnReset()
  {
    if (this.IsPushAndSet())
      return;
    this.StartCoroutine(this.ShowResetAlertPopup());
  }

  private IEnumerator ShowResetAlertPopup()
  {
    Unit00420Menu bMenu = this;
    PlayerMaterialUnit rUnit = ((IEnumerable<PlayerMaterialUnit>) SMManager.Get<PlayerMaterialUnit[]>()).FirstOrDefault<PlayerMaterialUnit>((Func<PlayerMaterialUnit, bool>) (x => x.unit.is_buildup_only == 2));
    GameObject popup = bMenu.resetAlertPopupPrefab.Clone((Transform) null);
    popup.SetActive(false);
    IEnumerator e = popup.GetComponent<Unit00420ResetPopup>().Init(bMenu, bMenu.baseUnit, rUnit);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Singleton<PopupManager>.GetInstance().open(popup, false, false, true, true, false, false, "SE_1006");
    popup.SetActive(true);
  }

  public void IbtnEnforce()
  {
    if (this.IsPushAndSet())
      return;
    bool isAlert = ((IEnumerable<PlayerUnit>) this.selectUnits).Any<PlayerUnit>((Func<PlayerUnit, bool>) (x => x.unit.rarity.index >= 2));
    bool isMemoryAlert = false;
    int?[] nullableArray = PlayerTransmigrateMemoryPlayerUnitIds.Current != null ? PlayerTransmigrateMemoryPlayerUnitIds.Current.transmigrate_memory_player_unit_ids : new int?[0];
    foreach (PlayerUnit selectUnit in this.selectUnits)
    {
      PlayerUnit unit = selectUnit;
      if (unit != (PlayerUnit) null && !isMemoryAlert)
      {
        isMemoryAlert = ((IEnumerable<int?>) nullableArray).Any<int?>((Func<int?, bool>) (x =>
        {
          if (!x.HasValue)
            return false;
          int? nullable = x;
          int id = unit.id;
          return nullable.GetValueOrDefault() == id & nullable.HasValue;
        }));
        if (isMemoryAlert)
          break;
      }
    }
    if (isAlert | isMemoryAlert)
    {
      Unit004832Menu component = Singleton<PopupManager>.GetInstance().open(this.rarityAlertPopupPrefab, false, false, false, true, false, false, "SE_1006").GetComponent<Unit004832Menu>();
      component.mode = Unit00468Scene.Mode.Unit00420;
      component.Init(new Func<List<PlayerUnit>, WebAPI.Response.UnitBuildup, Dictionary<string, object>>(this.setShowPopupData), this.baseUnit, this.selectUnits, isAlert, isMemoryAlert);
    }
    else
      Debug.LogError((object) "Unit00420Menu: 強化素材が想定しているレアリティではありません");
  }

  private Dictionary<string, object> setShowPopupData(
    List<PlayerUnit> resultList,
    WebAPI.Response.UnitBuildup param)
  {
    Dictionary<string, object> dictionary = new Dictionary<string, object>();
    dictionary["unlockQuests"] = (object) new UnlockQuest[0];
    Func<List<int>, List<int>, int> func = (Func<List<int>, List<int>, int>) ((list1, list2) =>
    {
      int num1 = 0;
      foreach (int num2 in list1)
      {
        if (!list2.Contains(num2))
          num1 = num2;
      }
      return num1;
    });
    List<int> list3 = ((IEnumerable<PlayerUnitSkills>) resultList[0].GetAcquireSkills()).Select<PlayerUnitSkills, int>((Func<PlayerUnitSkills, int>) (x => x.skill_id)).ToList<int>();
    List<int> list4 = ((IEnumerable<PlayerUnitSkills>) resultList[1].GetAcquireSkills()).Select<PlayerUnitSkills, int>((Func<PlayerUnitSkills, int>) (x => x.skill_id)).ToList<int>();
    dictionary["beforeSkillId"] = (object) func(list3, list4);
    dictionary["afterSkillId"] = (object) func(list4, list3);
    return dictionary;
  }
}
