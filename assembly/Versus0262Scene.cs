﻿// Decompiled with JetBrains decompiler
// Type: Versus0262Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using System;
using System.Collections;
using UnityEngine;

public class Versus0262Scene : NGSceneBase
{
  [SerializeField]
  private Versus0262Menu menu;
  private static bool is_loading_draw;
  private WebAPI.Response.PvpBoot pvpInfo;

  public static void ChangeScene0262(bool stack, PvpMatchingTypeEnum type, bool loading_draw = false)
  {
    Versus0262Scene.is_loading_draw = loading_draw;
    Singleton<NGSceneManager>.GetInstance().changeScene("versus026_2", (stack ? 1 : 0) != 0, (object) type);
  }

  public static void ChangeScene0262(
    bool stack,
    PvpMatchingTypeEnum type,
    WebAPI.Response.PvpBoot pvpInfo)
  {
    Versus0262Scene.is_loading_draw = false;
    Singleton<NGSceneManager>.GetInstance().changeScene("versus026_2", (stack ? 1 : 0) != 0, (object) type, (object) pvpInfo);
  }

  public override IEnumerator onInitSceneAsync()
  {
    Versus0262Scene versus0262Scene = this;
    Future<GameObject> bgF = Res.Prefabs.BackGround.MultiBackground.Load<GameObject>();
    IEnumerator e = bgF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    versus0262Scene.backgroundPrefab = bgF.Result;
  }

  public IEnumerator onStartSceneAsync(PvpMatchingTypeEnum type)
  {
    Singleton<CommonRoot>.GetInstance().isLoading = Versus0262Scene.is_loading_draw;
    IEnumerator e1;
    if (this.pvpInfo == null)
    {
      if (Singleton<NGGameDataManager>.GetInstance().isCallHomeUpdateAllData)
      {
        Future<WebAPI.Response.HomeStartUp2> handler = WebAPI.HomeStartUp2((System.Action<WebAPI.Response.UserError>) (e =>
        {
          WebAPI.DefaultUserErrorCallback(e);
          Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
        }));
        e1 = handler.Wait();
        while (e1.MoveNext())
          yield return e1.Current;
        e1 = (IEnumerator) null;
        if (handler.Result == null)
        {
          yield break;
        }
        else
        {
          Singleton<NGGameDataManager>.GetInstance().isCallHomeUpdateAllData = false;
          handler = (Future<WebAPI.Response.HomeStartUp2>) null;
        }
      }
      else
      {
        e1 = WebAPI.HomeStartUp((System.Action<WebAPI.Response.UserError>) null).Wait();
        while (e1.MoveNext())
          yield return e1.Current;
        e1 = (IEnumerator) null;
      }
      Future<WebAPI.Response.PvpBoot> futureF = WebAPI.PvpBoot((System.Action<WebAPI.Response.UserError>) (e =>
      {
        WebAPI.DefaultUserErrorCallback(e);
        Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
      }));
      e1 = futureF.Wait();
      while (e1.MoveNext())
        yield return e1.Current;
      e1 = (IEnumerator) null;
      if (futureF.Result == null)
      {
        yield break;
      }
      else
      {
        this.pvpInfo = futureF.Result;
        futureF = (Future<WebAPI.Response.PvpBoot>) null;
      }
    }
    e1 = this.onStartSceneAsync(type, this.pvpInfo);
    while (e1.MoveNext())
      yield return e1.Current;
    e1 = (IEnumerator) null;
    Singleton<CommonRoot>.GetInstance().isLoading = false;
  }

  public IEnumerator onStartSceneAsync(
    PvpMatchingTypeEnum type,
    WebAPI.Response.PvpBoot pvpInfo)
  {
    IEnumerator e = this.menu.Init(type, pvpInfo);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public override void onSceneInitialized()
  {
    base.onSceneInitialized();
    Singleton<CommonRoot>.GetInstance().loadingMode = 0;
  }
}
