﻿// Decompiled with JetBrains decompiler
// Type: Shop99982Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;
using UnityEngine;

public class Shop99982Menu : BackButtonMenuBase
{
  private GameObject prefab99983;
  public string year;
  public string month;
  public string day;

  private void update()
  {
  }

  public void Init(string year, string month, string day)
  {
    this.year = year;
    this.month = month;
    this.day = day;
  }

  public void IbtnNo()
  {
    if (this.IsPushAndSet())
      return;
    PurchaseBehavior.PopupDismiss(false);
  }

  public override void onBackButton()
  {
    this.IbtnNo();
  }

  public void IbtnPopupYes()
  {
    PurchaseBehavior.SendAge(int.Parse(this.year), int.Parse(this.month), int.Parse(this.day));
    PurchaseBehavior.PopupDismiss(false);
    PurchaseBehavior.PopupDismiss(false);
  }

  private IEnumerator popup99983()
  {
    string year = this.year.ToConverter();
    string month = this.month.ToConverter();
    string day = this.day.ToConverter();
    Future<GameObject> prefab = Res.Prefabs.popup.popup_999_8_3__anim_popup01.Load<GameObject>();
    IEnumerator e = prefab.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.prefab99983 = Singleton<PopupManager>.GetInstance().open(prefab.Result, false, false, false, true, false, false, "SE_1006");
    this.prefab99983.GetComponent<SetPopupText>().SetText(string.Format(Consts.GetInstance().SHOP_99982_MENU, (object) year, (object) month, (object) day), false);
  }
}
