﻿// Decompiled with JetBrains decompiler
// Type: Versus02612Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

public class Versus02612Scene : NGSceneBase
{
  [SerializeField]
  private Versus02612Menu menu;

  public static void ChangeScene(bool stack, int id, int best_class)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("versus026_12", (stack ? 1 : 0) != 0, (object) id, (object) best_class);
  }

  public IEnumerator onStartSceneAsync(int id, int best_class)
  {
    IEnumerator e = this.menu.Init(id, best_class);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }
}
