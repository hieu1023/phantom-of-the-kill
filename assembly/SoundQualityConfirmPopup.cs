﻿// Decompiled with JetBrains decompiler
// Type: SoundQualityConfirmPopup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.IO;
using UnityEngine;

public class SoundQualityConfirmPopup : BackButtonMenuBase
{
  public bool IsNormal;

  public override void onBackButton()
  {
    this.OnNo();
  }

  public void OnYes()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<PopupManager>.GetInstance().onDismiss();
    ResourceDownloader.ClearDLC();
    CachedFile.Clear();
    Caching.ClearCache();
    File.Delete(ResourceManager.dlcVersionPath);
    File.Delete(ResourceManager.pathsJsonPath);
    Persist.normalDLC.Data.IsSound = this.IsNormal;
    Persist.normalDLC.Data.IsSoundSetup = true;
    Persist.normalDLC.Flush();
    StartScript.Restart();
  }

  public void OnNo()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<PopupManager>.GetInstance().onDismiss();
  }
}
