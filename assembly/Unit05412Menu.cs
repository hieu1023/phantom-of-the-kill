﻿// Decompiled with JetBrains decompiler
// Type: Unit05412Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit05412Menu : Unit00412Menu
{
  [SerializeField]
  private UILabel TxtUnitCount;

  protected override void Update()
  {
    base.Update();
    this.ScrollUpdate();
  }

  public override IEnumerator Init(
    Player player,
    PlayerUnit[] playerUnits,
    bool isEquip,
    bool forBattle = true)
  {
    Unit05412Menu unit05412Menu = this;
    unit05412Menu.SetIconType(UnitMenuBase.IconType.Normal);
    IEnumerator e = unit05412Menu.Initialize();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    unit05412Menu.InitializeInfo((IEnumerable<PlayerUnit>) playerUnits, (IEnumerable<PlayerMaterialUnit>) null, (Persist<Persist.UnitSortAndFilterInfo>) null, isEquip, false, forBattle, false, false, (System.Action) null, 0);
    e = unit05412Menu.CreateUnitIcon();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    unit05412Menu.InitializeEnd();
    unit05412Menu.TxtNumber.SetTextLocalize(string.Format("{0}", (object) playerUnits.Length));
  }

  protected override void CreateUnitIconAction(int info_index, int unit_index)
  {
    UnitIconBase allUnitIcon;
    ((UnitIcon) (allUnitIcon = this.allUnitIcons[unit_index])).SetEarthButtonDetalEvent(this.allUnitInfos[info_index].playerUnit, this.getUnits());
    System.Action<UnitIconBase> action = (System.Action<UnitIconBase>) (ui => Unit0544Scene.ChangeScene(true, this.allUnitInfos[info_index].playerUnit, 1));
    allUnitIcon.onClick = action;
  }
}
