﻿// Decompiled with JetBrains decompiler
// Type: TutorialUnitBuguPage
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialUnitBuguPage : TutorialPageBase
{
  public override IEnumerator Show()
  {
    Singleton<NGSceneManager>.GetInstance().destroyLoadedScenes();
    Singleton<NGGameDataManager>.GetInstance().lastReferenceUnitID = -1;
    Singleton<NGGameDataManager>.GetInstance().lastReferenceUnitIndex = -1;
    Singleton<NGGameDataManager>.GetInstance().IsSea = false;
    Unit004topScene.ChangeScene(false);
    yield break;
  }

  public override void Advise()
  {
    Singleton<TutorialRoot>.GetInstance().ForceShowAdviceInNextButton("newchapter_reincarnation1_tutorial", new Dictionary<string, Func<Transform, UIButton>>()
    {
      {
        "chapter_unit_top",
        (Func<Transform, UIButton>) (root => root.GetChildInFind("Top").GetComponentInChildren<UIButton>())
      }
    }, (System.Action) (() => this.NextPage()));
  }
}
