﻿// Decompiled with JetBrains decompiler
// Type: SprGearElement
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using UnityEngine;

public class SprGearElement : MonoBehaviour
{
  public UI2DSprite iconSprite;

  public void Initialize(CommonElementIcon icons, CommonElement element)
  {
    this.iconSprite.sprite2D = icons.getIcon(element);
  }
}
