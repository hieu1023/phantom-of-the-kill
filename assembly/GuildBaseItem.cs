﻿// Decompiled with JetBrains decompiler
// Type: GuildBaseItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[Serializable]
public class GuildBaseItem
{
  [SerializeField]
  private GameObject enemyObject;
  [SerializeField]
  private GameObject friendObject;
  [SerializeField]
  private GameObject parentObject;
  [SerializeField]
  private UILabel nameLabel;

  public void Initialize(bool isEnemy, string name)
  {
    this.enemyObject.SetActive(isEnemy);
    this.friendObject.SetActive(!isEnemy);
    this.nameLabel.SetTextLocalize(name);
  }

  public void SetActive(bool flag)
  {
    this.parentObject.SetActive(flag);
  }
}
