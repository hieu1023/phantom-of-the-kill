﻿// Decompiled with JetBrains decompiler
// Type: Client.Core
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System;

namespace Client
{
  [Serializable]
  public class Core : BL
  {
    public BE _be;

    public Core(BE be)
    {
      this._be = be;
    }

    public override BL.SkillResultUnit createSkillResultUnit(BL.UnitPosition up)
    {
      return (BL.SkillResultUnit) new SkillResultUnit(up, this._be);
    }

    public override void resetUnitStatus(BL.UnitPosition up, int row, int column, float direction)
    {
      base.resetUnitStatus(up, row, column, direction);
      if (up is BL.AIUnit)
        return;
      this._be.unitResource[up.unit].unitParts_.resetStatus(up.row, up.column, up.direction);
    }
  }
}
