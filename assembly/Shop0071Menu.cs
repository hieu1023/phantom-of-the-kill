﻿// Decompiled with JetBrains decompiler
// Type: Shop0071Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Shop0071Menu : BackButtonMenuBase
{
  [SerializeField]
  protected UILabel TxtOwnnumber;
  [SerializeField]
  private UIButton slc_colosseum_button;
  [SerializeField]
  private UIButton slc_colosseum_button_before_slot;
  [SerializeField]
  private UIButton btnUnitCouponShop_;
  [SerializeField]
  private UIButton btnMaterialCouponShop_;
  [SerializeField]
  private GameObject btnUnitCouponBikkuri;
  [SerializeField]
  private GameObject btnMaterialCouponBikkuri;
  [SerializeField]
  private List<GameObject> ShopButtonGroup;
  [SerializeField]
  private GameObject limitedShopButtonNewIcon;
  [SerializeField]
  public UI2DSprite IdleSprite;
  [SerializeField]
  public UI2DSprite PressSprite;
  [SerializeField]
  public FloatButton BtnFormation;
  [SerializeField]
  private GameObject dynBannerLimit;
  private Modified<CoinBonus[]> coinBonus;
  private PlayerShopArticle shop;
  public Shop0071LeaderStand leaderStand;
  private GameObject modalPopup;
  private GameObject bannerLimitEmphasiePrefab;
  private BannerLimitEmphasie bannerLimitEmphasie;
  [SerializeField]
  private GameObject kisekiPurchaseBikkuri;
  [SerializeField]
  private GameObject slcKisekiBonus;
  [SerializeField]
  private GameObject slcBeginnerPack;

  private void Start()
  {
    this.coinBonus = SMManager.Observe<CoinBonus[]>();
  }

  public void UpdateStoneButtonCampaignIcon()
  {
    if (Singleton<NGGameDataManager>.GetInstance().newbiePacks)
    {
      this.slcBeginnerPack.SetActive(true);
      this.slcKisekiBonus.SetActive(false);
    }
    else
    {
      this.slcKisekiBonus.SetActive(true);
      this.slcBeginnerPack.SetActive(false);
    }
  }

  public void UpdateBikkuriIcon()
  {
    if (Singleton<NGGameDataManager>.GetInstance().receivableGift)
      this.kisekiPurchaseBikkuri.SetActive(true);
    else
      this.kisekiPurchaseBikkuri.SetActive(false);
  }

  private void ModalWindowNoBtn()
  {
    UnityEngine.Object.DestroyObject((UnityEngine.Object) this.modalPopup);
  }

  public IEnumerator Init(WebAPI.Response.ShopStatus shopStatus, bool specialShopError)
  {
    Shop0071Menu shop0071Menu = this;
    if (specialShopError)
      shop0071Menu.modalPopup = ModalWindow.Show(Consts.GetInstance().SHOP_0071_SPECIAL_SHOP_CLOSE_TITLE, Consts.GetInstance().SHOP_0071_SPECIAL_SHOP_CLOSE_DESCRIPTION, new System.Action(shop0071Menu.ModalWindowNoBtn)).gameObject;
    IEnumerator e;
    Future<Texture2D> spriteF;
    if (((IEnumerable<Shop>) shopStatus.shops).Any<Shop>((Func<Shop, bool>) (x => x.id == 5000)))
    {
      e = ServerTime.WaitSync();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      if (shopStatus.limited_shop_banner_url.Length <= 0)
      {
        spriteF = Singleton<ResourceManager>.GetInstance().Load<Texture2D>("Prefabs/Banners/MypageBanner/186/MypageBanner_idle", 1f);
        e = spriteF.Wait();
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        Texture2D result = spriteF.Result;
        UnityEngine.Sprite sprite = UnityEngine.Sprite.Create(result, new Rect(0.0f, 0.0f, (float) result.width, (float) result.height), new Vector2(0.5f, 0.5f), 1f, 100U, SpriteMeshType.FullRect);
        sprite.name = result.name;
        shop0071Menu.IdleSprite.sprite2D = sprite;
        shop0071Menu.BtnFormation.isEnabled = false;
        shop0071Menu.limitedShopButtonNewIcon.SetActive(false);
        spriteF = (Future<Texture2D>) null;
      }
      else
      {
        e = Singleton<NGGameDataManager>.GetInstance().GetWebImage(shopStatus.limited_shop_banner_url, shop0071Menu.IdleSprite);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        e = Singleton<NGGameDataManager>.GetInstance().GetWebImage(shopStatus.limited_shop_banner_url, shop0071Menu.PressSprite);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        // ISSUE: reference to a compiler-generated method
        EventDelegate.Set(shop0071Menu.BtnFormation.onOver, new EventDelegate.Callback(shop0071Menu.\u003CInit\u003Eb__26_1));
        // ISSUE: reference to a compiler-generated method
        EventDelegate.Set(shop0071Menu.BtnFormation.onOut, new EventDelegate.Callback(shop0071Menu.\u003CInit\u003Eb__26_2));
        shop0071Menu.onOut();
        shop0071Menu.BtnFormation.isEnabled = true;
        DateTime dateTime1 = ServerTime.NowAppTime();
        DateTime shopLastAccessTime = Persist.lastAccessTime.Data.limitedShopLastAccessTime;
        DateTime? shopLatestStartTime = Singleton<NGGameDataManager>.GetInstance().limitedShopLatestStartTime;
        DateTime? shopLatestEndTime = Singleton<NGGameDataManager>.GetInstance().limitedShopLatestEndTime;
        GameObject shopButtonNewIcon = shop0071Menu.limitedShopButtonNewIcon;
        int num;
        if (shopLatestStartTime.HasValue && shopLatestEndTime.HasValue)
        {
          DateTime dateTime2 = dateTime1;
          DateTime? nullable1 = shopLatestStartTime;
          if ((nullable1.HasValue ? (dateTime2 > nullable1.GetValueOrDefault() ? 1 : 0) : 0) != 0)
          {
            DateTime dateTime3 = dateTime1;
            DateTime? nullable2 = shopLatestEndTime;
            if ((nullable2.HasValue ? (dateTime3 < nullable2.GetValueOrDefault() ? 1 : 0) : 0) != 0)
            {
              DateTime dateTime4 = shopLastAccessTime;
              DateTime? nullable3 = shopLatestStartTime;
              if ((nullable3.HasValue ? (dateTime4 < nullable3.GetValueOrDefault() ? 1 : 0) : 0) != 0)
              {
                DateTime dateTime5 = shopLastAccessTime;
                DateTime? nullable4 = shopLatestEndTime;
                num = nullable4.HasValue ? (dateTime5 < nullable4.GetValueOrDefault() ? 1 : 0) : 0;
                goto label_23;
              }
            }
          }
        }
        num = 0;
label_23:
        shopButtonNewIcon.SetActive(num != 0);
      }
      SMManager.Get<PlayerSelectTicketSummary[]>();
      bool flag1 = shop0071Menu.HasTicket(TicketType.UNIT);
      bool flag2 = shop0071Menu.HasTicket(TicketType.ITEM);
      shop0071Menu.btnUnitCouponBikkuri.SetActive(flag1);
      shop0071Menu.btnMaterialCouponBikkuri.SetActive(flag2);
      if ((UnityEngine.Object) shop0071Menu.bannerLimitEmphasiePrefab == (UnityEngine.Object) null)
      {
        Future<GameObject> prefabF = Res.Prefabs.shop007_1.dir_banner_limit_Emphasie.Load<GameObject>();
        e = prefabF.Wait();
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        shop0071Menu.bannerLimitEmphasiePrefab = prefabF.Result;
        GameObject self = UnityEngine.Object.Instantiate<GameObject>(shop0071Menu.bannerLimitEmphasiePrefab);
        shop0071Menu.bannerLimitEmphasie = self.GetComponent<BannerLimitEmphasie>();
        self.SetParent(shop0071Menu.dynBannerLimit);
        prefabF = (Future<GameObject>) null;
      }
      if ((UnityEngine.Object) shop0071Menu.bannerLimitEmphasie != (UnityEngine.Object) null)
      {
        e = shop0071Menu.bannerLimitEmphasie.Init(ServerTime.NowAppTime(), shopStatus.limited_shop_end_at);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
      }
    }
    else
    {
      spriteF = Singleton<ResourceManager>.GetInstance().Load<Texture2D>("Prefabs/Banners/MypageBanner/186/MypageBanner_idle", 1f);
      e = spriteF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      Texture2D result = spriteF.Result;
      UnityEngine.Sprite sprite = UnityEngine.Sprite.Create(result, new Rect(0.0f, 0.0f, (float) result.width, (float) result.height), new Vector2(0.5f, 0.5f), 1f, 100U, SpriteMeshType.FullRect);
      sprite.name = result.name;
      shop0071Menu.IdleSprite.sprite2D = sprite;
      shop0071Menu.BtnFormation.isEnabled = false;
      shop0071Menu.limitedShopButtonNewIcon.SetActive(false);
      if ((UnityEngine.Object) shop0071Menu.bannerLimitEmphasiePrefab != (UnityEngine.Object) null)
      {
        UnityEngine.Object.Destroy((UnityEngine.Object) shop0071Menu.bannerLimitEmphasiePrefab);
        shop0071Menu.bannerLimitEmphasiePrefab = (GameObject) null;
        shop0071Menu.bannerLimitEmphasie = (BannerLimitEmphasie) null;
      }
      spriteF = (Future<Texture2D>) null;
    }
    shop0071Menu.UpdateStoneButtonCampaignIcon();
    shop0071Menu.UpdateBikkuriIcon();
  }

  private bool HasTicket(TicketType type)
  {
    PlayerSelectTicketSummary[] player_unit_tickets = SMManager.Get<PlayerSelectTicketSummary[]>();
    if (player_unit_tickets != null)
    {
      for (int i = 0; i < player_unit_tickets.Length; i++)
      {
        if (player_unit_tickets[i] != null && player_unit_tickets[i].quantity > 0)
        {
          MasterDataTable.SelectTicket selectTicket = ((IEnumerable<MasterDataTable.SelectTicket>) MasterData.SelectTicketList).FirstOrDefault<MasterDataTable.SelectTicket>((Func<MasterDataTable.SelectTicket, bool>) (x => x.ID == player_unit_tickets[i].ticket_id));
          if (selectTicket != null && (TicketType) selectTicket.category_id.ID == type)
            return true;
        }
      }
    }
    return false;
  }

  public void onOver()
  {
    this.IdleSprite.gameObject.SetActive(false);
    this.PressSprite.gameObject.SetActive(true);
  }

  public void onOut()
  {
    this.IdleSprite.gameObject.SetActive(true);
    this.PressSprite.gameObject.SetActive(false);
  }

  public void onStartScene()
  {
    Player.Current.GetFeatureColosseum();
    Player.Current.GetReleaseColosseum();
    bool releaseSlot = Player.Current.GetReleaseSlot();
    if (releaseSlot)
      this.InitButtonColosseum(this.slc_colosseum_button, true, true);
    else
      this.InitButtonColosseum(this.slc_colosseum_button_before_slot, true, true);
    this.ShopButtonGroup.ToggleOnce(releaseSlot ? 0 : 1);
    Queue<string> advices = new Queue<string>();
    advices.Enqueue(releaseSlot ? "shop007_1_base_slot" : "shop007_1_base");
    advices.Enqueue("shop007_1_colosseum");
    this.showAdvice(advices);
  }

  private void InitButtonColosseum(UIButton button, bool feature, bool release)
  {
    button.GetComponent<Collider>().enabled = release;
    button.enabled = true;
    button.gameObject.SetActive(feature);
  }

  private void showAdvice(Queue<string> advices)
  {
    if (advices.Count == 0 || Singleton<TutorialRoot>.GetInstance().ShowAdvice(advices.Dequeue(), 0, (System.Action) (() => this.showAdvice(advices))))
      return;
    this.showAdvice(advices);
  }

  private void showAdvice(Queue<string> advices, Queue<int> advicesId)
  {
    if (advices.Count == 0 || Singleton<TutorialRoot>.GetInstance().ShowAdvice(advices.Dequeue(), advicesId.Dequeue(), (System.Action) (() => this.showAdvice(advices, advicesId))))
      return;
    this.showAdvice(advices, advicesId);
  }

  public void SetInitalView(string stone, string status, string unit, string item)
  {
    this.TxtOwnnumber.SetTextLocalize(stone);
    this.leaderStand.SetLeaderCharacter();
  }

  public void SetTextData(string stone, string status, string unit, string item)
  {
    this.TxtOwnnumber.SetTextLocalize(stone);
  }

  private IEnumerator popup999111AP()
  {
    Future<GameObject> popupF = Res.Prefabs.popup.popup_999_11_1_1__anim_popup01.Load<GameObject>();
    IEnumerator e = popupF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Singleton<PopupManager>.GetInstance().open(popupF.Result, false, false, false, true, false, false, "SE_1006");
  }

  private IEnumerator popup00712()
  {
    Future<GameObject> prefab = Res.Prefabs.popup.popup_007_12__popup01.Load<GameObject>();
    IEnumerator e = prefab.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Singleton<PopupManager>.GetInstance().open(prefab.Result, false, false, false, true, false, false, "SE_1006").GetComponent<Shop00712Menu>().setUserData();
  }

  private IEnumerator popupAlert999111()
  {
    // ISSUE: reference to a compiler-generated field
    int num = this.\u003C\u003E1__state;
    Shop0071Menu shop0071Menu = this;
    if (num != 0)
      return false;
    // ISSUE: reference to a compiler-generated field
    this.\u003C\u003E1__state = -1;
    shop0071Menu.StartCoroutine(PopupUtility._999_11_1());
    return false;
  }

  private IEnumerator popup00714()
  {
    Modified<Player> player = SMManager.Observe<Player>();
    Future<GameObject> prefab = Res.Prefabs.popup.popup_007_14__anim_popup01.Load<GameObject>();
    IEnumerator e = prefab.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    GameObject gameObject = Singleton<PopupManager>.GetInstance().open(prefab.Result, false, false, false, true, false, false, "SE_1006");
    int quantity = MasterData.ShopContent[100000022].quantity;
    gameObject.GetComponent<Shop00714Menu>().Init(MasterData.ShopArticle[10000002].price, player.Value.max_units, player.Value.max_units + quantity);
    gameObject.GetComponent<Shop00714SetText>().SetText(player.Value.max_units, player.Value.max_units + quantity);
  }

  private IEnumerator popupAlert999121()
  {
    // ISSUE: reference to a compiler-generated field
    int num = this.\u003C\u003E1__state;
    Shop0071Menu shop0071Menu = this;
    if (num != 0)
      return false;
    // ISSUE: reference to a compiler-generated field
    this.\u003C\u003E1__state = -1;
    shop0071Menu.StartCoroutine(PopupUtility._999_12_1());
    return false;
  }

  private IEnumerator popup00716()
  {
    Modified<Player> player = SMManager.Observe<Player>();
    Future<GameObject> prefab = Res.Prefabs.popup.popup_007_16__anim_popup01.Load<GameObject>();
    IEnumerator e = prefab.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    int quantity = MasterData.ShopContent[100000033].quantity;
    int price = MasterData.ShopArticle[10000003].price;
    GameObject gameObject = Singleton<PopupManager>.GetInstance().open(prefab.Result, false, false, false, true, false, false, "SE_1006");
    gameObject.GetComponent<Shop00716Menu>().Init(price, player.Value.max_items, player.Value.max_items + quantity);
    gameObject.GetComponent<Shop00716SetText>().SetText(player.Value.max_items, player.Value.max_items + quantity);
  }

  protected override void Update()
  {
    base.Update();
  }

  public override void onBackButton()
  {
    this.IbtnBack();
  }

  public void IbtnBack()
  {
    if (this.IsPushAndSet())
      return;
    this.backScene();
  }

  public virtual void IbtnBuyKiseki()
  {
    if (this.IsPushAndSet())
      return;
    this.StartCoroutine(PopupUtility.BuyKiseki(false));
  }

  public virtual void IbtnFonds()
  {
    if (this.IsPushAndSet())
      return;
    Debug.Log((object) "資金決済法に基づく表示画面ジャンプ");
    this.StartCoroutine(PopupUtility._007_19());
  }

  public virtual void IbtnItemPlus()
  {
    if (this.IsPushAndSet())
      return;
    this.popupItemPlus();
  }

  public void popupItemPlus()
  {
    if (SMManager.Observe<Player>().Value.CheckLimitMaxItem())
    {
      Debug.Log((object) "装備品所持枠上限通知画面ジャンプ");
      this.StartCoroutine(this.popupAlert999121());
    }
    else
    {
      Debug.Log((object) "装備品所持枠拡張確認画面ジャンプ");
      this.StartCoroutine(this.popup00716());
    }
  }

  public virtual void IbtnMedalshop()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<NGSceneManager>.GetInstance().changeScene("shop007_4_1", true, (object[]) Array.Empty<object>());
  }

  public virtual void IbtnMedalSlot()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<NGSceneManager>.GetInstance().changeScene("shop007_20", true, (object[]) Array.Empty<object>());
  }

  public virtual void IbtnSpecific()
  {
    if (this.IsPushAndSet())
      return;
    this.StartCoroutine(PopupUtility._007_18());
  }

  public virtual void IbtnStatusPlus()
  {
    if (this.IsPushAndSet())
      return;
    if (SMManager.Observe<Player>().Value.CheckApFull())
    {
      this.StartCoroutine(this.popup999111AP());
    }
    else
    {
      Debug.Log((object) "ステータス回復確認画面ジャンプ");
      this.StartCoroutine(this.popup00712());
    }
  }

  public virtual void IbtnUnitPlus()
  {
    if (this.IsPushAndSet())
      return;
    this.popupUnitPlus();
  }

  public void popupUnitPlus()
  {
    if (SMManager.Observe<Player>().Value.CheckLimitMaxUnit())
    {
      Debug.Log((object) "ユニット所持枠上限通知画面ジャンプ");
      this.StartCoroutine(this.popupAlert999111());
    }
    else
    {
      Debug.Log((object) "ユニット所持枠拡張確認画面ジャンプ");
      this.StartCoroutine(this.popup00714());
    }
  }

  public virtual void IbtnZenyshop()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<NGSceneManager>.GetInstance().changeScene("shop007_4", true, (object[]) Array.Empty<object>());
  }

  public virtual void IbtnChara()
  {
  }

  public void IbtnBattleMedalshop()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<NGSceneManager>.GetInstance().changeScene("shop007_4_3", true, (object[]) Array.Empty<object>());
  }

  public void IbtnSpecialShop()
  {
    Shop00721Scene.changeScene(true, true);
  }

  public void IbtnUnitItemPlus()
  {
    if (this.IsPushAndSet())
      return;
    this.StartCoroutine(this.coPopupUnitItemPlus());
  }

  private IEnumerator coPopupUnitItemPlus()
  {
    Shop0071Menu menu = this;
    Future<GameObject> prefab = Res.Prefabs.popup.popup_007_possession_frame_extension__anim_popup01.Load<GameObject>();
    IEnumerator e = prefab.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    GameObject go = Singleton<PopupManager>.GetInstance().open(prefab.Result, false, false, false, true, false, false, "SE_1006");
    go.GetComponent<Shop0071PopupUnitItemPlus>().initialize(menu);
    while (go.activeSelf)
      yield return (object) null;
  }

  public void IbtnUnitCouponShop()
  {
    if (this.IsPushAndSet())
      return;
    Shop007231Scene.changeScene(true);
  }

  public void IbtnMaterialCouponShop()
  {
    if (this.IsPushAndSet())
      return;
    ShopMaterialExchangeScene.changeScene(true);
  }
}
