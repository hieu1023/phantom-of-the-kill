﻿// Decompiled with JetBrains decompiler
// Type: Bugu00539ChangeSceneParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class Bugu00539ChangeSceneParam
{
  public List<GameCore.ItemInfo> num_list;
  public bool is_new;
  public string[] anim_pattern;
  public GameCore.ItemInfo baseItem;
  public GameCore.ItemInfo baseReisou;
  public System.Action backSceneCallback;

  public Bugu00539ChangeSceneParam(
    List<GameCore.ItemInfo> numList,
    bool isNew,
    string[] animPattern,
    GameCore.ItemInfo baseItem,
    GameCore.ItemInfo baseReisou,
    System.Action backScene)
  {
    this.num_list = numList;
    this.is_new = isNew;
    this.anim_pattern = animPattern;
    this.baseItem = baseItem;
    this.baseReisou = baseReisou;
    this.backSceneCallback = backScene;
  }
}
