﻿// Decompiled with JetBrains decompiler
// Type: SeaHomeUnitAnimeCallback
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class SeaHomeUnitAnimeCallback : MonoBehaviour
{
  [NonSerialized]
  public SeaHomeUnitController controller;

  private void StartMove(float acceleration)
  {
    this.controller.StartMove(acceleration);
  }

  private void LoopMove()
  {
    this.controller.LoopMove();
  }

  private void CheckStopMove()
  {
    this.controller.CheckStopMove();
  }

  private void StopStartMove(float acceleration)
  {
    this.controller.StopStartMove(acceleration);
  }

  private void StopMove()
  {
    this.controller.StopMove();
  }

  private void EndDance()
  {
    this.controller.SetUnitStatus(SeaHomeUnitController.UnitStatus.Stand);
  }
}
