﻿// Decompiled with JetBrains decompiler
// Type: Guide01142Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Guide01142Menu : Unit00443Menu
{
  private bool isArrowBtn = true;
  private readonly int DISPLAY_OBJECT_MAX = 4;
  [SerializeField]
  private GearGear gear_;
  [SerializeField]
  protected UILabel TxtNumber;
  [SerializeField]
  protected GameObject dirNumber;
  public NGxScroll scroll;
  [SerializeField]
  private GameObject rightArrow;
  [SerializeField]
  private GameObject leftArrow;
  private int currentIndex;
  private int lastIndex;
  private GameObject[] detailObject;
  private Dictionary<GameObject, Guide0112BuguDetail> detailPrefabDict;
  private bool firstInit;
  [SerializeField]
  private UICenterOnChild centerOnChild;
  [Header("Use by guide detail")]
  private GameObject buguDetail;
  public UILabel title;
  public UI2DSprite raritySIcons;
  private int objectCnt;
  private List<GameObject> objectList;
  private GearGear[] gearList;
  private int[] quantityList;
  private bool isDispNumber;
  private bool isScrollViewDragStart;
  private int scrollStartCurrent;
  private Guide0112BuguDetail currentBuguDatail;
  private int currentInfoPageIndex;
  private int chacheCount;

  public IEnumerator onStartSceneAsync(GearGear gear, bool isDispNumber, int index = 0)
  {
    this.leftArrow.SetActive(false);
    this.rightArrow.SetActive(false);
    this.scroll.scrollView.enabled = false;
    IEnumerator e = this.GearsInit(new GearGear[1]
    {
      gear
    }, (int[]) null, isDispNumber, index);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public IEnumerator onStartSceneAsync(GearGear[] gears, bool isDispNumber, int index = 0)
  {
    this.leftArrow.SetActive(false);
    this.rightArrow.SetActive(false);
    IEnumerator e = this.GearsInit(gears, (int[]) null, isDispNumber, index);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public IEnumerator onStartSceneAsync(GearGear gear, int quantity, int index)
  {
    Guide01142Menu guide01142Menu = this;
    guide01142Menu.leftArrow.SetActive(false);
    guide01142Menu.rightArrow.SetActive(false);
    guide01142Menu.scroll.scrollView.enabled = false;
    IEnumerator e = guide01142Menu.GearsInit(new GearGear[1]
    {
      gear
    }, new int[1]{ quantity }, false, index);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public IEnumerator GearsInit(
    GearGear[] gear,
    int[] quantity,
    bool isDispNumber,
    int index)
  {
    Guide01142Menu m = this;
    if (m.firstInit)
    {
      m.SetChangeActiveComponent(true);
      m.SetMenuInformation(m.currentIndex);
    }
    else
    {
      m.gearList = gear;
      m.quantityList = quantity;
      m.isDispNumber = isDispNumber;
      Future<GameObject> prefabF = Res.Prefabs.guide011_4_2.guid_bugu_detail.Load<GameObject>();
      IEnumerator e = prefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      m.buguDetail = prefabF.Result;
      m.objectCnt = m.gearList.Length;
      m.currentIndex = index;
      m.currentInfoPageIndex = 0;
      m.chacheCount = 0;
      if (m.objectCnt > m.DISPLAY_OBJECT_MAX)
        m.objectCnt = m.DISPLAY_OBJECT_MAX;
      m.objectList = new List<GameObject>();
      m.detailObject = new GameObject[m.objectCnt];
      m.detailPrefabDict = new Dictionary<GameObject, Guide0112BuguDetail>();
      for (int index1 = 0; index1 < m.objectCnt; ++index1)
      {
        m.detailObject[index1] = UnityEngine.Object.Instantiate<GameObject>(m.buguDetail);
        m.objectList.Add(m.detailObject[index1]);
        m.scroll.Add(m.detailObject[index1], false);
        Guide0112BuguDetail component = m.detailObject[index1].GetComponent<Guide0112BuguDetail>();
        m.detailPrefabDict.Add(m.detailObject[index1], component);
      }
      yield return (object) null;
      m.scroll.ResolvePosition();
      m.scroll.scrollView.transform.localPosition = new Vector3(-m.scroll.grid.cellWidth * (float) m.currentIndex, 0.0f, 0.0f);
      foreach (GameObject index1 in m.detailObject)
        m.detailPrefabDict[index1].SetContainerPosition(m);
      yield return (object) null;
      e = m.CreatePage(m.currentIndex, false);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      yield return (object) null;
      int start = m.currentIndex - 1 < 0 ? 0 : m.currentIndex - 1;
      int end = m.currentIndex + 1 >= m.gearList.Length ? m.gearList.Length - 1 : m.currentIndex + 1;
      for (int i = start; i <= end; ++i)
      {
        if (i != m.currentIndex)
        {
          e = m.CreatePage(i, false);
          while (e.MoveNext())
            yield return e.Current;
          e = (IEnumerator) null;
        }
      }
      m.SetMenuInformation(m.currentIndex);
      m.CenterOnChild(m.currentIndex);
      yield return (object) null;
      for (int index1 = 0; index1 < m.objectList.Count; ++index1)
        m.objectList[index1].transform.localPosition = end < m.gearList.Length - 1 ? new Vector3(m.scroll.grid.cellWidth * (float) (end + index1 + 1), 0.0f, 0.0f) : new Vector3(m.scroll.grid.cellWidth * (float) (start - (index1 + 1)), 0.0f, 0.0f);
      Singleton<CommonRoot>.GetInstance().HideLoadingLayer();
      ItemIcon.IsPoolCache = true;
      m.firstInit = true;
    }
  }

  public virtual IEnumerator CreatePage(int gearIndex, bool isChangePage = false)
  {
    Guide01142Menu m = this;
    GameObject go = m.objectList[0];
    Guide0112BuguDetail d = m.detailPrefabDict[go];
    m.objectList.RemoveAt(0);
    Vector3 gridPos = m.scroll.grid.transform.localPosition;
    go.transform.localPosition = new Vector3(m.scroll.grid.cellWidth * (float) gearIndex, 0.0f, 0.0f);
    yield return (object) null;
    IEnumerator e;
    if (m.quantityList != null && m.quantityList.Length >= gearIndex)
    {
      e = d.Init(m, m.gearList[gearIndex], m.quantityList[gearIndex]);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    else
    {
      e = d.Init(m, m.gearList[gearIndex], m.isDispNumber);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    d.index = gearIndex;
    go.transform.localPosition = new Vector3(m.scroll.grid.cellWidth * (float) gearIndex, 0.0f, 0.0f);
    m.scroll.grid.transform.localPosition = gridPos;
    d.SetForcusInfoPage(m.currentInfoPageIndex);
    if (PerformanceConfig.GetInstance().IsLowMemory && m.chacheCount > 0)
    {
      GC.Collect();
      Singleton<ResourceManager>.GetInstance().ClearCache();
      Resources.UnloadUnusedAssets();
      m.chacheCount = 0;
    }
    if (isChangePage)
      ++m.chacheCount;
  }

  private void UpdateObjectList()
  {
    foreach (GameObject index1 in this.detailObject)
    {
      int index2 = this.detailPrefabDict[index1].index;
      if ((index2 < this.currentIndex - 1 || index2 > this.currentIndex + 1) && !this.objectList.Contains(index1))
        this.objectList.Add(index1);
    }
  }

  protected override void Update()
  {
    if (!this.firstInit)
      return;
    base.Update();
    this.UpdateCurrentItem();
    if (this.currentInfoPageIndex == this.currentBuguDatail.GetForcusInfoPage())
      return;
    this.currentInfoPageIndex = this.currentBuguDatail.GetForcusInfoPage();
    foreach (KeyValuePair<GameObject, Guide0112BuguDetail> keyValuePair in this.detailPrefabDict)
    {
      if (!((UnityEngine.Object) keyValuePair.Value == (UnityEngine.Object) this.currentBuguDatail))
        keyValuePair.Value.SetForcusInfoPage(this.currentInfoPageIndex);
    }
  }

  private void UpdateCurrentItem()
  {
    int num1 = this.currentIndex;
    if ((double) this.scroll.scrollView.transform.localPosition.x < 0.0)
    {
      int num2 = (int) Mathf.Abs((this.scroll.scrollView.transform.localPosition.x - this.scroll.grid.cellWidth / 2f) / this.scroll.grid.cellWidth);
      num1 = num2 <= this.gearList.Length ? num2 : this.gearList.Length - 1;
    }
    if (this.currentIndex != num1)
    {
      int num2 = this.currentIndex < num1 ? 1 : 0;
      bool flag = true;
      this.currentIndex = num1;
      if (this.currentIndex < 0)
      {
        this.currentIndex = 0;
        flag = false;
      }
      if (this.currentIndex >= this.gearList.Length)
      {
        this.currentIndex = this.gearList.Length - 1;
        flag = false;
      }
      this.SetMenuInformation(this.currentIndex);
      this.UpdateObjectList();
      if (num2 != 0)
      {
        if (this.currentIndex < this.gearList.Length - 1)
          this.StartCoroutine(this.CreatePage(this.currentIndex + 1, true));
      }
      else if (this.currentIndex > 0)
        this.StartCoroutine(this.CreatePage(this.currentIndex - 1, true));
      if (flag)
        Singleton<NGSoundManager>.GetInstance().playSE("SE_1005", false, 0.0f, -1);
    }
    if (this.scroll.scrollView.isDragging)
    {
      if (this.isScrollViewDragStart)
        return;
      this.currentBuguDatail.SetEnableInfoSlideSE(false);
      this.isScrollViewDragStart = true;
      this.scrollStartCurrent = this.currentIndex;
    }
    else
    {
      if (this.isScrollViewDragStart && this.scrollStartCurrent == this.currentIndex)
      {
        int currentIndex = this.currentIndex;
        double num2 = -(double) this.scroll.grid.cellWidth * (double) this.currentIndex;
        float num3 = this.scroll.grid.cellWidth * 0.25f;
        float num4 = (float) num2 - num3;
        float num5 = (float) num2 + num3;
        if ((double) this.scroll.scrollView.transform.localPosition.x <= (double) num4)
          ++currentIndex;
        else if ((double) this.scroll.scrollView.transform.localPosition.x >= (double) num5)
          --currentIndex;
        int num6 = currentIndex <= this.gearList.Length ? currentIndex : this.gearList.Length - 1;
        this.currentBuguDatail.SetEnableInfoSlideSE(true);
        this.CenterOnChild(num6);
      }
      this.isScrollViewDragStart = false;
    }
  }

  private void SetMenuInformation(int idx)
  {
    if (idx < 0 || idx > this.gearList.Length - 1)
      return;
    foreach (GameObject index in this.detailObject)
    {
      Guide0112BuguDetail guide0112BuguDetail = this.detailPrefabDict[index];
      if (guide0112BuguDetail.index == idx)
      {
        this.currentBuguDatail = guide0112BuguDetail;
        break;
      }
    }
    this.currentBuguDatail.SetGearInformation();
    this.currentBuguDatail.SetEnableInfoSlideSE(true);
    this.rightArrow.SetActive(true);
    this.leftArrow.SetActive(true);
    if (idx == 0)
      this.leftArrow.SetActive(false);
    if (idx != this.gearList.Length - 1)
      return;
    this.rightArrow.SetActive(false);
  }

  private void CenterOnChild(int num)
  {
    if (num < 0)
      return;
    foreach (GameObject index in this.detailObject)
    {
      if (this.detailPrefabDict[index].index == num)
      {
        this.centerOnChild.CenterOn(index.transform);
        break;
      }
    }
  }

  public void IbtnLeftArrow()
  {
    if (!this.isArrowBtn)
      return;
    this.isArrowBtn = false;
    this.StartCoroutine(this.IsArrowBtnOn());
    int num = this.currentIndex - 1;
    if (num < 0)
      return;
    this.CenterOnChild(num);
  }

  public void IbtnRightArrow()
  {
    if (!this.isArrowBtn)
      return;
    this.isArrowBtn = false;
    this.StartCoroutine(this.IsArrowBtnOn());
    int num = this.currentIndex + 1;
    if (num > this.detailObject.Length - 1)
      return;
    this.CenterOnChild(num);
  }

  protected IEnumerator IsArrowBtnOn()
  {
    yield return (object) new WaitForSeconds(0.2f);
    this.isArrowBtn = true;
  }

  public override void EndScene()
  {
    this.SetChangeActiveComponent(false);
  }

  private void SetChangeActiveComponent(bool isActive)
  {
    this.rightArrow.SetActive(isActive);
    this.leftArrow.SetActive(isActive);
    foreach (KeyValuePair<GameObject, Guide0112BuguDetail> keyValuePair in this.detailPrefabDict)
    {
      if (!((UnityEngine.Object) keyValuePair.Value == (UnityEngine.Object) this.currentBuguDatail))
        keyValuePair.Value.SetActiveZoomButton(isActive);
    }
  }
}
