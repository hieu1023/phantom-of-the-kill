﻿// Decompiled with JetBrains decompiler
// Type: Tower029TowerClearAnim
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System;
using System.Collections;
using UnityEngine;

public class Tower029TowerClearAnim : ResultMenuBase
{
  private GameObject animPrefab;
  private GameObject effectObj;
  private int floor;
  private bool isEndAnim;

  public override IEnumerator Init(
    BattleInfo info,
    WebAPI.Response.TowerBattleFinish result)
  {
    if ((UnityEngine.Object) this.animPrefab == (UnityEngine.Object) null)
    {
      Future<GameObject> f = new ResourceObject(string.Format("Prefabs/tower/dir_TowerClearAnim", (object[]) Array.Empty<object>())).Load<GameObject>();
      IEnumerator e = f.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.animPrefab = f.Result;
      f = (Future<GameObject>) null;
    }
    this.floor = result.floor;
  }

  public override IEnumerator Run()
  {
    Tower029TowerClearAnim tower029TowerClearAnim = this;
    if (!((UnityEngine.Object) tower029TowerClearAnim.animPrefab == (UnityEngine.Object) null))
    {
      tower029TowerClearAnim.effectObj = tower029TowerClearAnim.animPrefab.Clone((Transform) null);
      // ISSUE: reference to a compiler-generated method
      tower029TowerClearAnim.effectObj.GetComponent<Tower029TowerClearAnimPopup>().Initialize(tower029TowerClearAnim.floor, new System.Action(tower029TowerClearAnim.\u003CRun\u003Eb__6_0));
      Singleton<PopupManager>.GetInstance().open(tower029TowerClearAnim.effectObj, false, false, true, true, false, false, "SE_1006");
      while (!tower029TowerClearAnim.isEndAnim)
        yield return (object) null;
      yield return (object) new WaitForSeconds(0.14f);
      Singleton<PopupManager>.GetInstance().dismiss(false);
    }
  }

  private delegate IEnumerator Runner();
}
