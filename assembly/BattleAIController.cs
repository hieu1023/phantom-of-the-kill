﻿// Decompiled with JetBrains decompiler
// Type: BattleAIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using DeviceKit;
using GameCore;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class BattleAIController : BattleMonoBehaviour
{
  private NGBattleAIManager aiManager;
  private BattleTimeManager btm;
  private const float waitTime = 0.5f;
  private bool mIsAction;
  private bool isTerminate;

  protected override IEnumerator Start_Battle()
  {
    // ISSUE: reference to a compiler-generated field
    int num = this.\u003C\u003E1__state;
    BattleAIController battleAiController = this;
    if (num != 0)
      return false;
    // ISSUE: reference to a compiler-generated field
    this.\u003C\u003E1__state = -1;
    battleAiController.aiManager = battleAiController.GetComponent<NGBattleAIManager>();
    battleAiController.btm = battleAiController.battleManager.getManager<BattleTimeManager>();
    return false;
  }

  public bool isAction
  {
    get
    {
      return this.mIsAction;
    }
  }

  public bool isCompleted
  {
    get
    {
      if (!this.battleManager.isPvp)
        return this.aiManager.ai.isCompleted;
      return this.env.core.aiActionOrder.value != null && this.env.core.aiActionOrder.value.Count > 0;
    }
  }

  public bool startAI(List<BL.UnitPosition> units, bool isCharm, int max = 0)
  {
    if (this.mIsAction || this.aiManager.ai.isInitialized)
      return false;
    App.SetAutoSleep(false);
    List<BL.UnitPosition> units1 = new List<BL.UnitPosition>();
    foreach (BL.UnitPosition unit in units)
    {
      if (unit.unit.IsCharm == isCharm)
        units1.Add(unit);
    }
    this.aiManager.ai.initUnits(units1, max);
    Singleton<CommonRoot>.GetInstance().isActive3DUIMask = true;
    return true;
  }

  public void startAIAction(float waitForShowAiStopButton = 3f, System.Action endAction = null)
  {
    if (this.mIsAction || this.env.core.aiActionOrder.value == null)
      return;
    Singleton<CommonRoot>.GetInstance().isActive3DUIMask = true;
    this.isTerminate = false;
    this.StartCoroutine(this.actionAIUnits(waitForShowAiStopButton, endAction));
  }

  public void stopAIAction()
  {
    App.SetAutoSleep(true);
    if (this.mIsAction)
    {
      Debug.LogWarning((object) " === stopAIAction");
      this.isTerminate = true;
    }
    this.aiManager.ai.cleanup();
    Singleton<CommonRoot>.GetInstance().isActive3DUIMask = false;
  }

  public void enqueueAIActionOrder(BL.AIUnit aiUnit)
  {
    if (this.env.core.aiActionOrder.value == null)
      this.env.core.aiActionOrder.value = new Queue<BL.AIUnit>();
    this.env.core.aiActionOrder.value.Enqueue(aiUnit);
    this.env.core.aiActionOrder.commit();
  }

  public void clearAIActionOrder()
  {
    this.env.core.aiActionOrder.value = (Queue<BL.AIUnit>) null;
    this.env.core.aiActionOrder.commit();
  }

  private bool enable
  {
    get
    {
      if (!this.battleManager.isPvp && !this.battleManager.isBattleEnable)
        return false;
      foreach (BL.UnitPosition up in this.env.core.unitPositions.value)
      {
        if (up.isMoving(this.env))
          return false;
      }
      return !this.btm.isRunning;
    }
  }

  private bool moveUnit(BL.UnitPosition up, int row, int column)
  {
    if (up.row == row && up.column == column)
      return false;
    List<BL.Panel> moveRoute = this.env.core.getRouteNonCache(up, this.env.core.getFieldPanel(up, false), this.env.core.getFieldPanel(row, column), up.movePanels, (HashSet<BL.Panel>) null);
    if (moveRoute.Count > 0)
      this.btm.setScheduleAction((System.Action) (() => up.startMoveRoute(moveRoute, this.battleManager.defaultUnitSpeed, this.env)), 0.5f, (System.Action) null, (Func<bool>) (() =>
      {
        foreach (BL.UnitPosition up1 in this.env.core.unitPositions.value)
        {
          if (up1.isMoving(this.env))
            return false;
        }
        return true;
      }), false);
    return true;
  }

  private IEnumerator actionAIUnits(float waitForShowAiStopButton, System.Action endAction)
  {
    BattleAIController battleAiController1 = this;
    CommonRoot root = Singleton<CommonRoot>.GetInstance();
    battleAiController1.mIsAction = true;
    while (!battleAiController1.isCompleted)
    {
      waitForShowAiStopButton -= Time.deltaTime;
      yield return (object) null;
      if (battleAiController1.isTerminate)
        goto label_58;
    }
    while (battleAiController1.env.core.aiActionOrder.value != null && battleAiController1.env.core.aiActionOrder.value.Count > 0)
    {
      BattleAIController battleAiController = battleAiController1;
      NGSceneManager sm = Singleton<NGSceneManager>.GetInstance();
      bool isPvpNextScene = sm.sceneName != battleAiController1.battleManager.topScene;
      while (!battleAiController1.enable)
      {
        waitForShowAiStopButton -= Time.deltaTime;
        yield return (object) null;
        if (battleAiController1.isTerminate)
          goto label_58;
      }
      BL.AIUnit aiUnit = battleAiController1.env.core.aiActionOrder.value.Peek();
      BL.UnitPosition up = aiUnit.unitPosition;
      if (battleAiController1.env.core.phaseState.state == BL.Phase.player && battleAiController1.env.core.phaseState.turnCount == 1)
        battleAiController1.env.unitResource[up.unit].PlayVoiceDuelStart(up.unit);
      up.movePanels = (HashSet<BL.Panel>) null;
      battleAiController1.btm.setCurrentUnit(up.unit, 0.1f, false);
      if (aiUnit.actionResults == null & isPvpNextScene)
        battleAiController1.btm.setScheduleAction((System.Action) (() => sm.backScene()), 0.0f, (System.Action) null, (Func<bool>) null, false);
      while (!battleAiController1.enable)
      {
        waitForShowAiStopButton -= Time.deltaTime;
        yield return (object) null;
        if (battleAiController1.isTerminate)
          goto label_58;
      }
      if (aiUnit.action != null)
        battleAiController1.btm.setScheduleAction((System.Action) (() => aiUnit.action()), 0.0f, (System.Action) null, (Func<bool>) null, false);
      if (aiUnit.actionResults != null)
      {
        if (!Singleton<NGBattleManager>.GetInstance().noDuelScene)
        {
          foreach (ActionResult actionResult in aiUnit.actionResults.Where<ActionResult>((Func<ActionResult, bool>) (ar => ar is DuelResult)))
            Singleton<NGDuelDataManager>.GetInstance().StartBackGroundPreload(actionResult as DuelResult);
        }
        foreach (ActionResult actionResult in aiUnit.actionResults)
        {
          ActionResult ar = actionResult;
          DuelResult duelResult = ar as DuelResult;
          BL.BattleSkillResult skillResult = ar as BL.BattleSkillResult;
          int row = ar.isMove ? ar.row : aiUnit.row;
          int column = ar.isMove ? ar.column : aiUnit.column;
          if (battleAiController1.moveUnit(up, row, column))
          {
            do
            {
              waitForShowAiStopButton -= Time.deltaTime;
              yield return (object) null;
              if (battleAiController1.isTerminate)
                goto label_52;
            }
            while (!battleAiController1.enable);
          }
          if (duelResult != null)
          {
            if (duelResult.isHeal & isPvpNextScene)
              battleAiController1.btm.setScheduleAction(closure_5 ?? (closure_5 = (System.Action) (() => sm.backScene())), 0.0f, (System.Action) null, (Func<bool>) null, false);
            BL.Unit attack = duelResult.attack;
            BL.Unit defense = duelResult.defense;
            if (!isPvpNextScene)
              battleAiController1.btm.setScheduleAction((System.Action) (() => closure_4.env.core.lookDirection(attack, defense)), 0.5f, (System.Action) null, (Func<bool>) null, false);
            do
            {
              waitForShowAiStopButton -= Time.deltaTime;
              yield return (object) null;
              if (battleAiController1.isTerminate)
                goto label_52;
            }
            while (!battleAiController1.enable);
            if (duelResult.isHeal)
            {
              AttackStatus attackAttackStatus = duelResult.attackAttackStatus;
              BE env = battleAiController1.env;
              BL.MagicBullet magicBullet = attackAttackStatus.magicBullet;
              int healAttack = attackAttackStatus.heal_attack;
              BL.Unit unit = attack;
              List<BL.Unit> targets = new List<BL.Unit>();
              targets.Add(defense);
              BattleTimeManager btm = battleAiController1.btm;
              env.useMagicBullet(magicBullet, healAttack, unit, targets, btm);
            }
            else
            {
              battleAiController1.btm.setScheduleAction((System.Action) (() =>
              {
                if (closure_7.isTerminate)
                  return;
                closure_7.battleManager.startDuel(duelResult, (DuelEnvironment) null, !isPvpNextScene);
              }), 0.0f, (System.Action) null, (Func<bool>) null, false);
              do
              {
                waitForShowAiStopButton -= Time.deltaTime;
                yield return (object) null;
                if (battleAiController1.isTerminate)
                  goto label_52;
              }
              while (battleAiController1.btm.isRunning || !battleAiController1.battleManager.isBattleEnable);
            }
          }
          else if (skillResult != null)
            battleAiController1.env.useSkill(skillResult.invocation, skillResult.skill, skillResult.targets, skillResult.panels, skillResult, battleAiController1.btm, skillResult.random);
          if (ar.terminate)
          {
            battleAiController1.btm.setScheduleAction(closure_9 ?? (closure_9 = (System.Action) (() => up.completeActionUnit(battleAiController.env.core, true, false))), 0.0f, (System.Action) null, (Func<bool>) null, false);
            break;
          }
          do
          {
            waitForShowAiStopButton -= Time.deltaTime;
            yield return (object) null;
            if (battleAiController1.isTerminate)
              goto label_52;
          }
          while (!battleAiController1.enable);
          skillResult = (BL.BattleSkillResult) null;
          ar = (ActionResult) null;
          continue;
label_52:
          goto label_58;
        }
      }
      battleAiController1.env.core.aiActionOrder.value.Dequeue();
      do
      {
        waitForShowAiStopButton -= Time.deltaTime;
        yield return (object) null;
        if (battleAiController1.isTerminate)
          goto label_58;
      }
      while (!battleAiController1.enable);
    }
label_58:
    Debug.Log((object) ("wait ai stop button touch " + (object) waitForShowAiStopButton + "sec"));
    if ((double) waitForShowAiStopButton > 0.0)
      yield return (object) new WaitForSeconds(waitForShowAiStopButton);
    if (battleAiController1.isTerminate)
      battleAiController1.btm.setCurrentUnit((BL.Unit) null, 0.1f, false);
    if (endAction != null)
      endAction();
    battleAiController1.aiManager.ai.cleanup();
    battleAiController1.mIsAction = false;
    root.isActive3DUIMask = false;
  }

  public void clearCache()
  {
    this.aiManager.clearCache();
  }
}
