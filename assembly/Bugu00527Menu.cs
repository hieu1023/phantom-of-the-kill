﻿// Decompiled with JetBrains decompiler
// Type: Bugu00527Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Bugu00527Menu : Bugu005SelectItemListMenuBase
{
  private List<InventoryItem> FirstSelectItemList = new List<InventoryItem>();
  private bool firstInit = true;
  private const int SELECT_MAX = 10;
  [SerializeField]
  protected UILabel NumSpendZenie3;
  [SerializeField]
  protected UILabel NumSelectedCount3;
  [SerializeField]
  protected UILabel NumProsession3;
  [SerializeField]
  protected UILabel TxtReisouAddValue;
  [SerializeField]
  private UIButton ibtnYes;
  private Bugu00527Menu.DrillingType drillingType;
  private GameCore.ItemInfo BaseItem;
  private List<int> equipedReisouIdList;

  public void SetFirstSelectItem(
    Bugu00527Menu.DrillingType dType,
    List<InventoryItem> items,
    GameCore.ItemInfo target)
  {
    this.drillingType = dType;
    this.BaseItem = target;
    this.baseInfo = this.BaseItem;
    this.FirstSelectItemList = items;
    this.SelectMax = 10;
  }

  public override Persist<Persist.ItemSortAndFilterInfo> GetPersist()
  {
    return Persist.bugu0052DrillingMaterialSortAndFilter;
  }

  protected override List<PlayerItem> GetItemList()
  {
    List<PlayerItem> playerItemList = new List<PlayerItem>();
    this.equipedReisouIdList = new List<int>();
    bool flag1 = false;
    if (this.BaseItem.playerItem.gear_level_limit == this.BaseItem.playerItem.gear_level_limit_max && this.BaseItem.playerItem.gear_level == this.BaseItem.playerItem.gear_level_limit)
      flag1 = true;
    bool flag2 = false;
    if (this.BaseItem.isEquipReisou && this.BaseItem.isEquipReisouLvMax)
      flag2 = true;
    if (flag1 & flag2)
      return playerItemList;
    foreach (PlayerItem playerItem in SMManager.Get<PlayerItem[]>())
    {
      if (playerItem.isWeapon() && playerItem.equipped_reisou_player_gear_id != 0)
        this.equipedReisouIdList.Add(playerItem.equipped_reisou_player_gear_id);
      if ((!flag1 || playerItem.isReisou()) && (!flag2 || !playerItem.isReisou()))
      {
        if (this.drillingType == Bugu00527Menu.DrillingType.Normal)
        {
          if (this.IsActiveDrillingItem(playerItem))
            playerItemList.Add(playerItem);
        }
        else if (this.IsActiveSpecialDrillingItem(playerItem))
          playerItemList.Add(playerItem);
      }
    }
    return playerItemList;
  }

  private bool IsActiveDrillingItem(PlayerItem item)
  {
    return this.BaseItem != null && item.id != this.BaseItem.itemID && item.gear != null && this.IsActiveDrillingIcon(item.gear);
  }

  private bool IsActiveDrillingMaterial(PlayerMaterialGear item)
  {
    return this.BaseItem != null && item.id != this.BaseItem.itemID && item.gear != null && this.IsActiveDrillingIcon(item.gear);
  }

  private bool IsActiveDrillingIcon(GearGear gear)
  {
    if (gear.disappearance_num.HasValue || !gear.kind.isEquip && gear.kind.Enum != GearKindEnum.drilling && gear.kind.Enum != GearKindEnum.sea_present || gear.isReisou() && (!this.BaseItem.isEquipReisou || this.BaseItem.isEquipReisouLvMax))
      return false;
    return gear.kind.Enum == this.BaseItem.gear.kind.Enum || gear.kind.Enum == GearKindEnum.drilling || gear.kind.Enum == GearKindEnum.sea_present;
  }

  private bool IsActiveSpecialDrillingItem(PlayerItem item)
  {
    if (this.BaseItem == null || item.id == this.BaseItem.itemID || item.gear == null)
      return false;
    if (item.isReisou())
    {
      if (!this.BaseItem.isEquipReisou)
        return false;
      if (!this.BaseItem.isEquipReisouLvMax)
        return item.gear.kind.Enum == this.BaseItem.gear.kind.Enum;
    }
    return GearGear.CanSpecialDrill(this.BaseItem.gear, item.gear);
  }

  private bool IsActiveSpecialDrillingMaterial(PlayerMaterialGear item)
  {
    return this.BaseItem != null && item.id != this.BaseItem.itemID && item.gear != null && (item.gear.kind.Enum != GearKindEnum.special_drilling || this.BaseItem.gear.rarity_GearRarity == item.gear.rarity_GearRarity && !this.BaseItem.playerItem.isLimitMax()) && GearGear.CanSpecialDrill(this.BaseItem.gear, item.gear);
  }

  protected override List<PlayerMaterialGear> GetMaterialList()
  {
    List<PlayerMaterialGear> res = new List<PlayerMaterialGear>();
    if (this.drillingType == Bugu00527Menu.DrillingType.Normal)
      ((IEnumerable<PlayerMaterialGear>) SMManager.Get<PlayerMaterialGear[]>()).Where<PlayerMaterialGear>((Func<PlayerMaterialGear, bool>) (x => this.IsActiveDrillingMaterial(x) || this.IsActiveSpecialDrillingMaterial(x))).ForEach<PlayerMaterialGear>((System.Action<PlayerMaterialGear>) (x =>
      {
        int num = x.quantity < this.SelectMax ? x.quantity : this.SelectMax;
        for (int index = 0; index < num; ++index)
          res.Add(x);
      }));
    else
      ((IEnumerable<PlayerMaterialGear>) SMManager.Get<PlayerMaterialGear[]>()).Where<PlayerMaterialGear>((Func<PlayerMaterialGear, bool>) (x => this.IsActiveSpecialDrillingMaterial(x))).ForEach<PlayerMaterialGear>((System.Action<PlayerMaterialGear>) (x =>
      {
        int num = x.quantity < this.SelectMax ? x.quantity : this.SelectMax;
        for (int index = 0; index < num; ++index)
          res.Add(x);
      }));
    return res;
  }

  protected override IEnumerator InitExtension()
  {
    // ISSUE: reference to a compiler-generated field
    int num = this.\u003C\u003E1__state;
    Bugu00527Menu bugu00527Menu = this;
    if (num != 0)
      return false;
    // ISSUE: reference to a compiler-generated field
    this.\u003C\u003E1__state = -1;
    if (bugu00527Menu.firstInit)
    {
      bugu00527Menu.firstInit = false;
      bugu00527Menu.SelectItemList.Clear();
      if (bugu00527Menu.FirstSelectItemList != null && bugu00527Menu.FirstSelectItemList.Count > 0)
      {
        // ISSUE: reference to a compiler-generated method
        bugu00527Menu.FirstSelectItemList = bugu00527Menu.FirstSelectItemList.Where<InventoryItem>(new Func<InventoryItem, bool>(bugu00527Menu.\u003CInitExtension\u003Eb__21_0)).ToList<InventoryItem>();
      }
      // ISSUE: reference to a compiler-generated method
      bugu00527Menu.FirstSelectItemList.ForEach(new System.Action<InventoryItem>(bugu00527Menu.\u003CInitExtension\u003Eb__21_1));
    }
    else if (bugu00527Menu.SelectItemList != null && bugu00527Menu.SelectItemList.Count > 0)
    {
      // ISSUE: reference to a compiler-generated method
      List<InventoryItem> list = bugu00527Menu.SelectItemList.Where<InventoryItem>(new Func<InventoryItem, bool>(bugu00527Menu.\u003CInitExtension\u003Eb__21_2)).ToList<InventoryItem>();
      bugu00527Menu.SelectItemList.Clear();
      // ISSUE: reference to a compiler-generated method
      System.Action<InventoryItem> action = new System.Action<InventoryItem>(bugu00527Menu.\u003CInitExtension\u003Eb__21_3);
      list.ForEach(action);
    }
    return false;
  }

  protected override void CreateItemIconAdvencedSetting(int inventoryItemIdx, int allItemIdx)
  {
    base.CreateItemIconAdvencedSetting(inventoryItemIdx, allItemIdx);
    ItemIcon itemIcon = this.AllItemIcon[allItemIdx];
    itemIcon.MaxUpMark = this.BaseItem != null && !this.BaseItem.playerItem.isLimitMax() && itemIcon.ItemInfo.gear != null && GearGear.CanSpecialDrill(this.BaseItem.gear, itemIcon.ItemInfo.gear);
    if (itemIcon.ItemInfo == null || !itemIcon.ItemInfo.isReisou)
      return;
    itemIcon.ItemInfo.ForBattle = this.equipedReisouIdList.FirstOrDefault<int>((Func<int, bool>) (x => x == itemIcon.ItemInfo.itemID)) > 0;
    itemIcon.ForBattle = itemIcon.ItemInfo.ForBattle;
    if (!itemIcon.ForBattle)
      return;
    itemIcon.onClick = (System.Action<ItemIcon>) (_ => {});
    itemIcon.Gray = true;
  }

  protected override void BottomInfoUpdate()
  {
    Player player = SMManager.Get<Player>();
    NGGameDataManager.Boost boostInfo = Singleton<NGGameDataManager>.GetInstance().BoostInfo;
    int num = (int) ((boostInfo == null ? new Decimal(10, 0, 0, false, (byte) 1) : boostInfo.DiscountGearDrilling) * (Decimal) CalcItemCost.GetDrillingCost(this.BaseItem, this.SelectItemList));
    this.NumSpendZenie3.SetTextLocalize(num);
    this.NumSpendZenie3.color = (long) num < player.money ? Color.white : Color.red;
    this.NumSelectedCount3.SetTextLocalize("{0}/{1}".F((object) this.SelectItemList.Count<InventoryItem>(), (object) 10));
    this.NumProsession3.SetTextLocalize((int) ((boostInfo == null ? new Decimal(10, 0, 0, false, (byte) 1) : boostInfo.BootRateGearDrilling) * this.SelectItemList.Sum<InventoryItem>((Func<InventoryItem, Decimal>) (x => !x.Item.isReisou ? Math.Floor((Decimal) GearDrilling.GetGearDrilling(x.Item.gearLevel, x.Item.gear.rarity.index) * (Decimal) x.Item.gear.drilling_rate) : Decimal.Zero))));
    this.TxtReisouAddValue.SetTextLocalize((int) (new Decimal(10, 0, 0, false, (byte) 1) * this.SelectItemList.Sum<InventoryItem>((Func<InventoryItem, Decimal>) (x => x.Item.isReisou ? Math.Floor((Decimal) ReisouDrilling.GetReisouDrilling(x.Item.gear.rarity.index) + (Decimal) x.Item.playerItem.gear_total_exp) * (Decimal) x.Item.gear.drilling_rate : Decimal.Zero))));
    this.ibtnYes.isEnabled = this.SelectItemList.Count<InventoryItem>() > 0 && player.money >= (long) num;
  }

  protected override void ChangeDetailScene(GameCore.ItemInfo item)
  {
    if (item == null)
      return;
    if (item.isReisou)
      this.OpenReisouDetailPopup(item);
    else if (item.isWeapon)
      Unit00443Scene.changeSceneForDrillingMaterial(true, item);
    else if (item.isWeaponMaterial)
      Guide01142Scene.changeScene(true, item);
    else
      Bugu00561Scene.changeScene(true, item);
  }

  protected override void SelectItemProc(GameCore.ItemInfo item)
  {
    InventoryItem byItemIndex = this.InventoryItems.FindByItemIndex(item);
    if (byItemIndex != null)
    {
      if (byItemIndex.select)
        this.RemoveSelectItem(byItemIndex);
      else if (this.SelectItemList.Count < this.SelectMax)
        this.AddSelectItem(byItemIndex);
    }
    this.UpdateSelectItemIndexWithInfo();
  }

  public override void IbtnBack()
  {
    if (this.IsPush)
      return;
    Singleton<PopupManager>.GetInstance().closeAll(false);
    if (Singleton<NGGameDataManager>.GetInstance().IsColosseum)
      Singleton<CommonRoot>.GetInstance().SetFooterEnable(true);
    Singleton<NGSceneManager>.GetInstance().destroyCurrentScene();
    Bugu0059Scene.changeScene(false, this.BaseItem, this.FirstSelectItemList.Where<InventoryItem>((Func<InventoryItem, bool>) (x =>
    {
      InventoryItem inventoryItem = this.InventoryItems.FirstOrDefault<InventoryItem>((Func<InventoryItem, bool>) (i => i.Item.itemID == x.Item.itemID));
      return inventoryItem != null && !inventoryItem.Item.favorite && !inventoryItem.Item.ForBattle;
    })).ToList<InventoryItem>());
  }

  public void IbtnDecision()
  {
    if (this.IsPush)
      return;
    Singleton<NGSceneManager>.GetInstance().destroyCurrentScene();
    Bugu0059Scene.changeScene(false, this.BaseItem, this.SelectItemList);
  }

  public enum DrillingType
  {
    Normal,
    Special,
  }
}
