﻿// Decompiled with JetBrains decompiler
// Type: UpperParameterLabel
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class UpperParameterLabel : MonoBehaviour
{
  [SerializeField]
  private NGxBlinkEx blink;
  [SerializeField]
  private GameObject blinkBreakthrough;
  [SerializeField]
  private GameObject blinkSkillUp;
  [SerializeField]
  private GameObject blinkDeardegreeup;
  [SerializeField]
  private GameObject blinkUnityValue;
  [SerializeField]
  private UILabel txtBlinkUnityValue;
  [SerializeField]
  private GameObject breakThrough;
  [SerializeField]
  private GameObject skillUp;
  [SerializeField]
  private GameObject deardegreeup;
  [SerializeField]
  private GameObject unityValue;
  [SerializeField]
  private UILabel txtUnityValue;
  private const int DEFAULT_UNITY_VALUE = 0;

  private void InitializeLabel()
  {
    this.blink.gameObject.SetActive(false);
    this.breakThrough.SetActive(false);
    this.skillUp.SetActive(false);
    this.unityValue.SetActive(false);
  }

  public void Init(PlayerUnit basePlayerUnit, PlayerUnit materialUnit)
  {
    this.InitializeLabel();
    UnitUnit baseU = basePlayerUnit.unit;
    UnitUnit matU = materialUnit.unit;
    float num = 0.0f;
    bool flag1 = basePlayerUnit.unity_value < PlayerUnit.GetUnityValueMax();
    if (flag1)
    {
      if (matU.IsNormalUnit)
      {
        if (baseU.same_character_id == matU.same_character_id)
        {
          num = Mathf.Min(materialUnit.unityTotal + 1f, (float) PlayerUnit.GetUnityValueMax());
          goto label_8;
        }
      }
      else if (matU.is_unity_value_up && (double) basePlayerUnit.unityTotal < (double) PlayerUnit.GetUnityValueMax())
      {
        UnitUnit unitUnit = matU;
        UnitUnit target = baseU;
        Func<UnitFamily[]> funcGetFamilies = (Func<UnitFamily[]>) (() => basePlayerUnit.Families);
        UnityValueUpPattern valueUpPattern;
        if ((valueUpPattern = unitUnit.FindValueUpPattern(target, funcGetFamilies)) != null)
        {
          num = (float) valueUpPattern.up_value;
          goto label_8;
        }
      }
      flag1 = false;
    }
label_8:
    string text = (double) num > 0.0 ? string.Format(Consts.GetInstance().unit_004_8_4_plus_unity_value, (object) (Math.Floor((double) num * 10.0) / 10.0)) : (string) null;
    bool flag2 = !matU.IsBreakThrough ? baseU.same_character_id == matU.same_character_id && baseU.rarity.index <= matU.rarity.index && basePlayerUnit.breakthrough_count < baseU.breakthrough_limit : matU.CheckBreakThroughMaterial(basePlayerUnit);
    IEnumerable<PlayerUnitSkills> source = ((IEnumerable<PlayerUnitSkills>) basePlayerUnit.skills).Where<PlayerUnitSkills>((Func<PlayerUnitSkills, bool>) (x => x.level < x.skill.upper_level));
    bool isSkill = false;
    if (materialUnit.skills != null)
      isSkill = source.Any<PlayerUnitSkills>((Func<PlayerUnitSkills, bool>) (unitBase => ((IEnumerable<PlayerUnitSkills>) materialUnit.skills).Count<PlayerUnitSkills>((Func<PlayerUnitSkills, bool>) (x => unitBase.skill_id == x.skill_id)) > 0));
    if (matU.same_character_id == baseU.same_character_id && source.Count<PlayerUnitSkills>() > 0)
      isSkill = true;
    else
      ((IEnumerable<PlayerUnitSkills>) basePlayerUnit.skills).Where<PlayerUnitSkills>((Func<PlayerUnitSkills, bool>) (x => x.level < x.skill.upper_level)).ForEach<PlayerUnitSkills>((System.Action<PlayerUnitSkills>) (y =>
      {
        if (baseU.same_character_id == matU.same_character_id || materialUnit.skills == null || ((IEnumerable<PlayerUnitSkills>) materialUnit.skills).Where<PlayerUnitSkills>((Func<PlayerUnitSkills, bool>) (z => y.skill_id == z.skill_id)).Count<PlayerUnitSkills>() == 0)
          return;
        isSkill = true;
      }));
    bool flag3 = UnitDetailIcon.IsSkillUpMaterial(materialUnit.unit, basePlayerUnit);
    bool flag4 = false;
    if (baseU.trust_target_flag && (double) basePlayerUnit.trust_rate < (double) basePlayerUnit.trust_max_rate && matU.same_character_id == baseU.same_character_id)
      flag4 = true;
    bool flag5 = baseU.trust_target_flag && (double) basePlayerUnit.trust_rate < (double) basePlayerUnit.trust_max_rate && matU.character.ID == baseU.character.ID;
    bool flag6 = false;
    if (baseU.trust_target_flag && (double) basePlayerUnit.trust_rate < (double) basePlayerUnit.trust_max_rate && matU.IsTrustMaterial(basePlayerUnit))
      flag6 = true;
    if (flag4 | flag6 | flag5)
    {
      UISprite component1 = this.deardegreeup.GetComponent<UISprite>();
      UISprite component2 = this.blinkDeardegreeup.GetComponent<UISprite>();
      component1.spriteName = !basePlayerUnit.unit.IsSea ? (!basePlayerUnit.unit.IsResonanceUnit ? (component2.spriteName = "slc_DearDegree_up") : (component2.spriteName = "slc_Relevance_up")) : (component2.spriteName = "slc_DearDegree_up");
      UISpriteData atlasSprite1 = component1.GetAtlasSprite();
      component1.width = atlasSprite1.width;
      component1.height = atlasSprite1.height;
      UISpriteData atlasSprite2 = component2.GetAtlasSprite();
      component2.width = atlasSprite2.width;
      component2.height = atlasSprite2.height;
    }
    GameObject[] gameObjectArray1 = new GameObject[4]
    {
      this.breakThrough,
      this.skillUp,
      this.deardegreeup,
      this.unityValue
    };
    bool[] flagArray = new bool[4]
    {
      flag2,
      isSkill | flag3,
      flag4 | flag6 | flag5,
      flag1
    };
    if (((IEnumerable<bool>) flagArray).Where<bool>((Func<bool, bool>) (b => b)).Count<bool>() > 1)
    {
      for (int index = 0; index < flagArray.Length; ++index)
      {
        if ((UnityEngine.Object) gameObjectArray1[index] != (UnityEngine.Object) null)
          gameObjectArray1[index].SetActive(false);
      }
      GameObject[] gameObjectArray2 = new GameObject[4]
      {
        this.blinkBreakthrough,
        this.blinkSkillUp,
        this.blinkDeardegreeup,
        this.blinkUnityValue
      };
      List<GameObject> gameObjectList = new List<GameObject>();
      for (int index = 0; index < flagArray.Length; ++index)
      {
        GameObject gameObject = gameObjectArray2[index];
        if (!((UnityEngine.Object) gameObject == (UnityEngine.Object) null))
        {
          bool flag7 = flagArray[index];
          if (flag7)
            gameObjectList.Add(gameObject);
          gameObject.SetActive(flag7);
        }
      }
      this.blink.SetChildren(gameObjectList.ToArray());
      this.blink.gameObject.SetActive(true);
      if (!((UnityEngine.Object) this.txtBlinkUnityValue != (UnityEngine.Object) null) || string.IsNullOrEmpty(text))
        return;
      this.txtBlinkUnityValue.SetTextLocalize(text);
    }
    else
    {
      for (int index = 0; index < flagArray.Length; ++index)
      {
        if ((UnityEngine.Object) gameObjectArray1[index] != (UnityEngine.Object) null)
          gameObjectArray1[index].SetActive(flagArray[index]);
      }
      if (!((UnityEngine.Object) this.txtUnityValue != (UnityEngine.Object) null) || string.IsNullOrEmpty(text))
        return;
      this.txtUnityValue.SetTextLocalize(text);
    }
  }
}
