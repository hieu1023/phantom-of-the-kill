﻿// Decompiled with JetBrains decompiler
// Type: Shop0078Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;
using UnityEngine;

public class Shop0078Menu : BackButtonMenuBase
{
  [SerializeField]
  protected UILabel TxtDescription;
  [SerializeField]
  protected UILabel TxtDescription01;
  [SerializeField]
  protected UILabel TxtDescription02;
  [SerializeField]
  protected UILabel TxtPopuptitle;
  [SerializeField]
  private GameObject linkParent;
  private System.Action callBack;

  public void InitDataSet(
    string name,
    int item_quantity,
    int player_item_quantity,
    System.Action ok = null,
    bool isMaterialExchange = false)
  {
    this.TxtDescription.SetTextLocalize(name);
    this.TxtDescription01.SetTextLocalize(Consts.Format(isMaterialExchange ? Consts.GetInstance().SHOP_0078_TXT_DESCRIPTION02 : Consts.GetInstance().SHOP_0078_TXT_DESCRIPTION01, (IDictionary) new Hashtable()
    {
      {
        (object) "quantity",
        (object) item_quantity.ToLocalizeNumberText()
      }
    }));
    string text = Consts.Format(Consts.GetInstance().SHOP_007X_TXT_DESCRIPTION_ADD_QUANTITY, (IDictionary) new Hashtable()
    {
      {
        (object) "quantity",
        (object) player_item_quantity.ToLocalizeNumberText()
      },
      {
        (object) "quantityNext",
        (object) (player_item_quantity + item_quantity).ToLocalizeNumberText()
      }
    });
    if (isMaterialExchange)
      this.TxtPopuptitle.SetTextLocalize(Consts.GetInstance().VERSUS_0026872POPUP_TITLE2);
    this.TxtDescription02.SetTextLocalize(text);
    if (ok == null)
      return;
    this.callBack = ok;
  }

  public virtual void IbtnPopupOk()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<PopupManager>.GetInstance().onDismiss();
    if (this.callBack == null)
      return;
    this.callBack();
  }

  public override void onBackButton()
  {
    this.IbtnPopupOk();
  }

  public void InitObj(GameObject obj)
  {
    obj.Clone(this.linkParent.transform);
  }
}
