﻿// Decompiled with JetBrains decompiler
// Type: Quest0028Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Quest0028Scene : NGSceneBase
{
  private List<PlayerItem> SupplyList = new List<PlayerItem>();
  private bool story_only;
  public Quest0028Menu menu;
  [SerializeField]
  private GameObject bg;
  private PlayerHelper friend_;

  public static void changeScene(bool stack, PlayerStoryQuestS story_quest, bool story_only = false)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("quest002_8", (stack ? 1 : 0) != 0, (object) story_quest, (object) story_only);
  }

  public static void changeScene(bool stack, PlayerExtraQuestS extra_quest, bool story_only = false)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("quest002_8", (stack ? 1 : 0) != 0, (object) extra_quest, (object) story_only);
  }

  public static void changeScene(bool stack, PlayerCharacterQuestS char_quest, bool story_only = false)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("quest002_8", (stack ? 1 : 0) != 0, (object) char_quest, (object) story_only);
  }

  public static void changeScene(bool stack, PlayerQuestSConverter char_quest, bool story_only = false)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("quest002_8", (stack ? 1 : 0) != 0, (object) char_quest, (object) story_only);
  }

  public static void changeScene(bool stack, PlayerSeaQuestS sea_quest, bool story_only = false)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("quest002_8_sea", (stack ? 1 : 0) != 0, (object) sea_quest, (object) story_only);
  }

  public IEnumerator onStartSceneAsync()
  {
    PlayerHelper playerHelper = SMManager.Get<PlayerHelper[]>()[1];
    playerHelper.leader_unit._unit = 500211;
    this.friend_ = playerHelper;
    IEnumerator e = this.onStartSceneAsync(SMManager.Get<PlayerStoryQuestS[]>()[0], (PlayerExtraQuestS) null, (PlayerCharacterQuestS) null, (PlayerQuestSConverter) null, (PlayerSeaQuestS) null, false);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public IEnumerator onStartSceneAsync(PlayerStoryQuestS story_quest, bool story_only = false)
  {
    IEnumerator e = this.onStartSceneAsync(story_quest, (PlayerExtraQuestS) null, (PlayerCharacterQuestS) null, (PlayerQuestSConverter) null, (PlayerSeaQuestS) null, story_only);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public IEnumerator onStartSceneAsync(PlayerExtraQuestS extra_quest, bool story_only = false)
  {
    IEnumerator e = this.onStartSceneAsync((PlayerStoryQuestS) null, extra_quest, (PlayerCharacterQuestS) null, (PlayerQuestSConverter) null, (PlayerSeaQuestS) null, story_only);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public IEnumerator onStartSceneAsync(PlayerCharacterQuestS char_quest, bool story_only = false)
  {
    IEnumerator e = this.onStartSceneAsync((PlayerStoryQuestS) null, (PlayerExtraQuestS) null, char_quest, (PlayerQuestSConverter) null, (PlayerSeaQuestS) null, story_only);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public IEnumerator onStartSceneAsync(PlayerQuestSConverter quest, bool story_only = false)
  {
    IEnumerator e = this.onStartSceneAsync((PlayerStoryQuestS) null, (PlayerExtraQuestS) null, (PlayerCharacterQuestS) null, quest, (PlayerSeaQuestS) null, story_only);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public IEnumerator onStartSceneAsync(PlayerSeaQuestS sea_quest, bool story_only = false)
  {
    IEnumerator e = this.onStartSceneAsync((PlayerStoryQuestS) null, (PlayerExtraQuestS) null, (PlayerCharacterQuestS) null, (PlayerQuestSConverter) null, sea_quest, story_only);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public override void onSceneInitialized()
  {
    if (!this.story_only && Singleton<CommonRoot>.GetInstance().isLoading)
      Singleton<CommonRoot>.GetInstance().isLoading = false;
    base.onSceneInitialized();
  }

  public IEnumerator onBackSceneAsync(PlayerStoryQuestS story_quest, bool story_only = false)
  {
    IEnumerator e;
    if (!this.menu.IsPlayingStory)
    {
      e = this.onStartSceneAsync(story_quest, (PlayerExtraQuestS) null, (PlayerCharacterQuestS) null, (PlayerQuestSConverter) null, (PlayerSeaQuestS) null, story_only);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    else
    {
      e = this.onBackSceneAsync();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
  }

  public IEnumerator onBackSceneAsync(PlayerExtraQuestS extra_quest, bool story_only = false)
  {
    IEnumerator e;
    if (!this.menu.IsPlayingStory)
    {
      e = this.onStartSceneAsync((PlayerStoryQuestS) null, extra_quest, (PlayerCharacterQuestS) null, (PlayerQuestSConverter) null, (PlayerSeaQuestS) null, story_only);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    else
    {
      e = this.onBackSceneAsync();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
  }

  public IEnumerator onBackSceneAsync(PlayerCharacterQuestS char_quest, bool story_only = false)
  {
    IEnumerator e;
    if (!this.menu.IsPlayingStory)
    {
      e = this.onStartSceneAsync((PlayerStoryQuestS) null, (PlayerExtraQuestS) null, char_quest, (PlayerQuestSConverter) null, (PlayerSeaQuestS) null, story_only);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    else
    {
      e = this.onBackSceneAsync();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
  }

  public IEnumerator onBackSceneAsync(PlayerQuestSConverter quest, bool story_only = false)
  {
    IEnumerator e;
    if (!this.menu.IsPlayingStory)
    {
      e = this.onStartSceneAsync((PlayerStoryQuestS) null, (PlayerExtraQuestS) null, (PlayerCharacterQuestS) null, quest, (PlayerSeaQuestS) null, story_only);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    else
    {
      e = this.onBackSceneAsync();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
  }

  public IEnumerator onBackSceneAsync(PlayerSeaQuestS sea_quest, bool story_only = false)
  {
    IEnumerator e;
    if (!this.menu.IsPlayingStory)
    {
      e = this.onStartSceneAsync((PlayerStoryQuestS) null, (PlayerExtraQuestS) null, (PlayerCharacterQuestS) null, (PlayerQuestSConverter) null, sea_quest, story_only);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    else
    {
      e = this.onBackSceneAsync();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
  }

  public IEnumerator onBackSceneAsync()
  {
    if (this.menu.IsPlayingStory)
    {
      Singleton<CommonRoot>.GetInstance().isLoading = true;
      Future<BattleEnd> f = WebAPI.BattleFinish(new WebAPI.Request.BattleFinish()
      {
        quest_type = this.menu.battleInfo.quest_type,
        win = true,
        is_game_over = false,
        battle_uuid = this.menu.battleInfo.battleId,
        player_money = 0,
        battle_turn = 0,
        continue_count = 0,
        week_element_attack_count = 0,
        week_kind_attack_count = 0
      }, (BE) null, (System.Action<WebAPI.Response.UserError>) (e =>
      {
        NGSceneManager instance = Singleton<NGSceneManager>.GetInstance();
        instance.clearStack();
        instance.destroyCurrentScene();
        instance.changeScene(Singleton<CommonRoot>.GetInstance().startScene, false, (object[]) Array.Empty<object>());
      }));
      IEnumerator e1 = f.Wait();
      while (e1.MoveNext())
        yield return e1.Current;
      e1 = (IEnumerator) null;
      if (f.Result != null)
      {
        Singleton<NGGameDataManager>.GetInstance().refreshHomeHome = true;
        BattleUI05Scene.ChangeScene(this.menu.battleInfo, true, f.Result);
        EventTracker.TrackEvent("BATTLE", "CONTINUE", 0);
        this.menu.IsPlayingStory = false;
        this.menu.mainPanel.SetActive(true);
      }
    }
  }

  public void setFriend(PlayerHelper friend)
  {
    this.friend_ = friend;
  }

  public IEnumerator onStartSceneAsync(
    PlayerStoryQuestS story_quest,
    PlayerExtraQuestS extra_quest,
    PlayerCharacterQuestS char_quest,
    PlayerQuestSConverter quest,
    PlayerSeaQuestS sea_quest,
    bool story_only)
  {
    Quest0028Scene quest0028Scene = this;
    quest0028Scene.story_only = story_only;
    Singleton<NGGameDataManager>.GetInstance().QuestType = story_quest == null ? (extra_quest == null ? (char_quest == null ? (quest == null ? (sea_quest == null ? new CommonQuestType?() : new CommonQuestType?(CommonQuestType.Sea)) : (quest.questS.data_type != QuestSConverter.DataType.Character ? new CommonQuestType?(CommonQuestType.Harmony) : new CommonQuestType?(CommonQuestType.Character))) : new CommonQuestType?(CommonQuestType.Character)) : new CommonQuestType?(CommonQuestType.Extra)) : new CommonQuestType?(CommonQuestType.Story);
    Quest0028Menu.isGoingToBattle = false;
    if (Singleton<NGGameDataManager>.GetInstance().IsSea && sea_quest == null)
      quest0028Scene.headerType = CommonRoot.HeaderType.Normal;
    else if (!Singleton<NGGameDataManager>.GetInstance().IsSea && sea_quest != null)
      quest0028Scene.headerType = CommonRoot.HeaderType.Sea;
    PlayerItem[] playerItemArray = SMManager.Get<PlayerItem[]>().AllBattleSupplies();
    quest0028Scene.SupplyList = ((IEnumerable<PlayerItem>) playerItemArray).ToList<PlayerItem>();
    PlayerDeck[] decks = !Singleton<NGGameDataManager>.GetInstance().IsSea || sea_quest == null ? SMManager.Get<PlayerDeck[]>() : PlayerSeaDeck.convertDeckData(SMManager.Get<PlayerSeaDeck[]>());
    string path = "";
    if (story_quest != null)
      path = quest0028Scene.setStoryPath(story_quest.quest_story_s);
    else if (extra_quest != null)
    {
      path = quest0028Scene.setExtraPath(extra_quest.quest_extra_s);
      Debug.LogWarning((object) path);
    }
    else if (char_quest != null)
      path = quest0028Scene.setCharaPath(char_quest.quest_character_s);
    else if (quest != null)
      path = quest0028Scene.setQuestConverterPath(quest.questS);
    else if (sea_quest != null)
      path = quest0028Scene.setSeaPath(sea_quest.quest_sea_s);
    else
      Debug.LogError((object) "!BUG! QUEST NOT FOUND");
    Future<GameObject> bgF = Res.Prefabs.BackGround.SortieBackGround.Load<GameObject>();
    IEnumerator e = bgF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    quest0028Scene.bg = bgF.Result;
    Future<UnityEngine.Sprite> bgSpriteF = Singleton<ResourceManager>.GetInstance().LoadOrNull<UnityEngine.Sprite>(path);
    e = bgSpriteF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    e = ServerTime.WaitSync();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if ((UnityEngine.Object) bgSpriteF.Result != (UnityEngine.Object) null)
    {
      quest0028Scene.bg.GetComponent<UI2DSprite>().sprite2D = bgSpriteF.Result;
      UI2DSprite component = quest0028Scene.bg.GetComponent<UI2DSprite>();
      component.sprite2D = bgSpriteF.Result;
      Rect textureRect = bgSpriteF.Result.textureRect;
      component.width = Mathf.FloorToInt(textureRect.width);
      textureRect = bgSpriteF.Result.textureRect;
      component.height = Mathf.FloorToInt(textureRect.height);
      quest0028Scene.backgroundPrefab = quest0028Scene.bg;
    }
    quest0028Scene.menu.setEventSetHelper(new System.Action<PlayerHelper>(quest0028Scene.setFriend));
    e = quest0028Scene.menu.InitPlayerDecks(decks, quest0028Scene.SupplyList, quest0028Scene.friend_, story_quest, extra_quest, char_quest, quest, sea_quest, story_only);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    quest0028Scene.isActiveFooter = !Singleton<NGGameDataManager>.GetInstance().IsSea;
  }

  private string setStoryPath(QuestStoryS quest)
  {
    return quest.GetBackgroundPath();
  }

  private string setExtraPath(QuestExtraS quest)
  {
    return quest.GetBackgroundPath();
  }

  private string setCharaPath(QuestCharacterS quest)
  {
    return quest.GetBackgroundPath();
  }

  private string setQuestConverterPath(QuestSConverter quest)
  {
    string backgroundImageName = quest.quest_m.background_image_name;
    return backgroundImageName == null ? Consts.GetInstance().DEFULAT_BACKGROUND : string.Format(Consts.GetInstance().BACKGROUND_BASE_PATH, (object) backgroundImageName);
  }

  private string setSeaPath(QuestSeaS quest)
  {
    return quest.GetBackgroundPath();
  }

  public override void onEndScene()
  {
    this.menu.EndScene();
    Singleton<CommonRoot>.GetInstance().releaseBackground();
  }

  public override IEnumerator onDestroySceneAsync()
  {
    if (!Quest0028Menu.isGoingToBattle)
    {
      Singleton<NGGameDataManager>.GetInstance().IsFromUnityPopupStageList = false;
      Singleton<NGSceneManager>.GetInstance().ClearSavedChangeSceneParam();
      Singleton<NGGameDataManager>.GetInstance().QuestType = new CommonQuestType?();
      yield break;
    }
  }
}
