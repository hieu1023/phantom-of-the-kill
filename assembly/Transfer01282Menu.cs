﻿// Decompiled with JetBrains decompiler
// Type: Transfer01282Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Transfer01282Menu : NGMenuBase
{
  [SerializeField]
  private GameObject Migrate;
  [SerializeField]
  private GameObject FgGIDMigrate;

  public void IbtnMigtate()
  {
    this.Migrate.SetActive(true);
    this.Migrate.GetComponent<Startup00017Menu>().InitDataCode();
    this.gameObject.SetActive(false);
  }

  public void IbtnFgGIDMigtate()
  {
    this.FgGIDMigrate.SetActive(true);
    this.FgGIDMigrate.GetComponent<Startup00018Menu>().InitDataCode();
    this.gameObject.SetActive(false);
  }

  public virtual void IbtnPopupBack()
  {
    this.gameObject.SetActive(false);
  }
}
