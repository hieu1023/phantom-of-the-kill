﻿// Decompiled with JetBrains decompiler
// Type: SM_PlayerQuestKeyExtension
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;
using System;
using System.Collections.Generic;
using UniLinq;

public static class SM_PlayerQuestKeyExtension
{
  public static int AmountHavingTargetKey(
    this PlayerQuestKey[] self,
    int entity_id,
    MasterDataTable.CommonRewardType entity_type)
  {
    int num = 0;
    if (entity_type == MasterDataTable.CommonRewardType.quest_key && self != null)
    {
      PlayerQuestKey playerQuestKey = ((IEnumerable<PlayerQuestKey>) self).FirstOrDefault<PlayerQuestKey>((Func<PlayerQuestKey, bool>) (k => k.quest_key_id == entity_id));
      if (playerQuestKey != null)
        num = playerQuestKey.quantity;
    }
    return num;
  }
}
