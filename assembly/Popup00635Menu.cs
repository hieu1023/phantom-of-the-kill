﻿// Decompiled with JetBrains decompiler
// Type: Popup00635Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Popup00635Menu : BackButtonMenuBase
{
  private System.Action playGachaTicket;
  [SerializeField]
  private UILabel ticketName;
  [SerializeField]
  private UILabel description;
  [SerializeField]
  private UILabel ticketCount;
  [SerializeField]
  private UILabel m_Cost;

  public void Init(GachaModuleGacha gachaData, System.Action playGacha)
  {
    int index = gachaData.payment_id.Value;
    GachaTicket gachaTicket = MasterData.GachaTicket[index];
    Dictionary<int, PlayerGachaTicket> dictionary = ((IEnumerable<PlayerGachaTicket>) SMManager.Get<Player>().gacha_tickets).ToDictionary<PlayerGachaTicket, int>((Func<PlayerGachaTicket, int>) (x => x.ticket_id));
    this.ticketName.SetTextLocalize(gachaTicket.name);
    this.SetDescription(Consts.Format(Consts.GetInstance().GACHA_00635TICKET_DESCRIPTION, (IDictionary) null));
    this.ticketCount.SetTextLocalize(dictionary[index].quantity);
    this.playGachaTicket = playGacha;
    this.m_Cost.SetTextLocalize(gachaData.payment_amount);
  }

  public void SetDescription(string txt)
  {
    this.description.SetTextLocalize(txt);
  }

  public void IbtnPlayTicketGacha()
  {
    if (this.IsPushAndSet())
      return;
    this.playGachaTicket();
  }

  public void IbtnBack()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<PopupManager>.GetInstance().dismiss(false);
  }

  public override void onBackButton()
  {
    this.IbtnBack();
  }
}
