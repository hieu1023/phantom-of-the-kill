﻿// Decompiled with JetBrains decompiler
// Type: GuildChatStampGroupSelectItemController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class GuildChatStampGroupSelectItemController : MonoBehaviour
{
  public int iconStampID;
  public int stampGroupID;
  public bool isSelected;
  [SerializeField]
  private UISprite stampImage;
  [SerializeField]
  private UISprite backgroundImage;

  private void Start()
  {
  }

  private void Update()
  {
  }

  public void InitializeGuildChatStampGroupItem(int stampGroupID, int iconStampID)
  {
    this.stampGroupID = stampGroupID;
    this.iconStampID = iconStampID;
    Singleton<CommonRoot>.GetInstance().guildChatManager.SetStampSprite(this.stampImage, this.iconStampID);
  }

  public void SetSelected(bool isSelected)
  {
    this.isSelected = isSelected;
    if (isSelected)
    {
      this.stampImage.color = Color.white;
      this.backgroundImage.gameObject.SetActive(true);
    }
    else
    {
      this.stampImage.color = new Color(0.5f, 0.5f, 0.5f);
      this.backgroundImage.gameObject.SetActive(false);
    }
  }

  public void OnStampGroupItemClicked()
  {
    Singleton<CommonRoot>.GetInstance().guildChatManager.stampSelectViewController.SelectStampGroup(this.stampGroupID);
  }
}
