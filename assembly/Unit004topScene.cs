﻿// Decompiled with JetBrains decompiler
// Type: Unit004topScene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;

public class Unit004topScene : NGSceneBase
{
  private bool isInit = true;
  public Unit004topMenu menu;

  public static void ChangeScene(bool isStack)
  {
    NGSceneManager instance = Singleton<NGSceneManager>.GetInstance();
    instance.clearStack();
    instance.changeScene("unit004_top", isStack, (object[]) Array.Empty<object>());
  }

  public static void ChangeSceneNoClearStack(bool isStack)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("unit004_top", isStack, (object[]) Array.Empty<object>());
  }

  public void onStartScene()
  {
    Singleton<CommonRoot>.GetInstance().isLoading = false;
    Singleton<CommonRoot>.GetInstance().loadingMode = 0;
  }

  public override IEnumerator onInitSceneAsync()
  {
    if (this.isInit)
    {
      IEnumerator e = this.menu.Init();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    this.isInit = false;
  }

  private void IbtnCpmposite()
  {
    Debug.Log((object) "click default event IbtnCpmposite");
  }

  private void IbtnEvolution()
  {
    Debug.Log((object) "click default event IbtnEvolution");
  }

  private void IbtnFormation()
  {
    Debug.Log((object) "click default event IbtnFormation");
  }

  private void IbtnOverview()
  {
    Debug.Log((object) "click default event IbtnOverview");
  }

  private void IbtnSell()
  {
    Debug.Log((object) "click default event IbtnSell");
  }

  private void IbtnEquip()
  {
    Debug.Log((object) "click default event IbtnEquip");
  }
}
