﻿// Decompiled with JetBrains decompiler
// Type: BE
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using System;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

[Serializable]
public class BE
{
  public Stack<List<BL.Unit>> waveEnemiesStack = new Stack<List<BL.Unit>>();
  public Stack<List<Tuple<BL.DropData, int>>> waveDropStack = new Stack<List<Tuple<BL.DropData, int>>>();
  [NonSerialized]
  private BE.DefaultDict<BL.DropData, BE.DropDataResource> dropDataResource_;
  public BL core;
  [NonSerialized]
  private BE.DefaultDict<BL.Panel, BE.PanelResource> panelResource_;
  [NonSerialized]
  private BE.DefaultDict<BL.Stage, BE.StageResource> stageResource_;
  [NonSerialized]
  private BE.DefaultDict<int, BE.ItemResource> itemResource_;
  [NonSerialized]
  private BE.DefaultDict<int, BE.SkillResource> skillResource_;
  [NonSerialized]
  private BE.DefaultDict<int, BE.AilmentSkillResource> ailmentSkillResource_;
  [NonSerialized]
  private BE.DefaultDict<BL.Unit, BE.UnitResource> unitResource_;
  [NonSerialized]
  private BE.DefaultDict<BL.Weapon, BE.WeaponResource> weaponResource_;

  public BE.DefaultDict<BL.DropData, BE.DropDataResource> dropDataResource
  {
    get
    {
      return this.getDefaultDict<BL.DropData, BE.DropDataResource>(ref this.dropDataResource_);
    }
  }

  public BE.DefaultDict<TKey, TValue> getDefaultDict<TKey, TValue>(
    ref BE.DefaultDict<TKey, TValue> v)
    where TValue : new()
  {
    if (v == null)
      v = new BE.DefaultDict<TKey, TValue>();
    return v;
  }

  public BE.DefaultDict<BL.Panel, BE.PanelResource> panelResource
  {
    get
    {
      return this.getDefaultDict<BL.Panel, BE.PanelResource>(ref this.panelResource_);
    }
  }

  public BE.DefaultDict<BL.Stage, BE.StageResource> stageResource
  {
    get
    {
      return this.getDefaultDict<BL.Stage, BE.StageResource>(ref this.stageResource_);
    }
  }

  public Vector3 limitFieldPosition(Vector3 v)
  {
    BE.PanelResource panelResource1 = this.panelResource[this.core.getFieldPanel(0, 0)];
    BE.PanelResource panelResource2 = this.panelResource[this.core.getFieldPanel(this.core.getFieldHeight() - 1, 0)];
    BE.PanelResource panelResource3 = this.panelResource[this.core.getFieldPanel(0, this.core.getFieldWidth() - 1)];
    BE.PanelResource panelResource4 = (BE.PanelResource) null;
    BE.PanelResource panelResource5 = (BE.PanelResource) null;
    if ((double) v.x < (double) panelResource1.gameObject.transform.position.x)
      panelResource4 = panelResource1;
    else if ((double) v.x > (double) panelResource3.gameObject.transform.position.x)
      panelResource4 = panelResource3;
    if ((double) v.z < (double) panelResource1.gameObject.transform.position.z)
      panelResource5 = panelResource1;
    else if ((double) v.z > (double) panelResource2.gameObject.transform.position.z)
      panelResource5 = panelResource2;
    double num1 = panelResource4 == null ? (double) v.x : (double) panelResource4.gameObject.transform.position.x;
    float y = panelResource1.gameObject.transform.position.y;
    float num2 = panelResource5 == null ? v.z : panelResource5.gameObject.transform.position.z;
    double num3 = (double) y;
    double num4 = (double) num2;
    return new Vector3((float) num1, (float) num3, (float) num4);
  }

  public BE.DefaultDict<int, BE.ItemResource> itemResource
  {
    get
    {
      return this.getDefaultDict<int, BE.ItemResource>(ref this.itemResource_);
    }
  }

  public void useItem(BL.Item item, BL.Unit unit, BattleTimeManager btm)
  {
    btm.setScheduleAction((System.Action) (() =>
    {
      int prev_hp = unit.hp;
      bool isRebirth = item.item.skill.target_type == BattleskillTargetType.dead_player_single && unit.isDead;
      this.core.useItemWith(item, unit, (System.Action<List<BL.Unit>>) (effectTargets =>
      {
        Debug.LogWarning((object) (" ==== useItem#effectTargets:" + (object) effectTargets + " isRebirth:" + isRebirth.ToString()));
        Singleton<NGBattleManager>.GetInstance().battleEffects.itemFieldEffectStart(effectTargets, item, (System.Action) (() =>
        {
          if (isRebirth)
          {
            unit.rebirthBE(this);
          }
          else
          {
            if (prev_hp == unit.hp || !unit.isView)
              return;
            this.unitResource[unit].unitParts_.dispHpNumber(prev_hp, unit.hp);
          }
        }));
      }), this.core);
    }), 0.0f, (System.Action) null, (Func<bool>) null, false);
  }

  public void useMagicBullet(
    BL.MagicBullet mb,
    int attack,
    BL.Unit unit,
    List<BL.Unit> targets,
    BattleTimeManager btm)
  {
    btm.setScheduleAction((System.Action) (() =>
    {
      Dictionary<BL.Unit, int> target_prev_hps = new Dictionary<BL.Unit, int>();
      foreach (BL.Unit target in targets)
        target_prev_hps.Add(target, target.hp);
      this.core.useMagicBulletWith(mb, attack, unit, targets, (System.Action<BL.Unit, int>) ((addUnit, unitPrevHp) =>
      {
        List<BL.Unit> list = targets.ToList<BL.Unit>();
        if (addUnit != (BL.Unit) null && !list.Contains(addUnit))
        {
          list.Add(addUnit);
          target_prev_hps.Add(addUnit, unitPrevHp);
        }
        foreach (BL.Unit index in list)
        {
          if (index.isView)
            this.unitResource[index].unitParts_.dispHpNumber(target_prev_hps[index], index.hp);
        }
        Singleton<NGBattleManager>.GetInstance().battleEffects.mbFieldEffectStart(unit, mb, targets);
      }), this.core);
    }), 0.0f, (System.Action) null, (Func<bool>) null, false);
  }

  public BE.DefaultDict<int, BE.SkillResource> skillResource
  {
    get
    {
      return this.getDefaultDict<int, BE.SkillResource>(ref this.skillResource_);
    }
  }

  public BE.DefaultDict<int, BE.AilmentSkillResource> ailmentSkillResource
  {
    get
    {
      return this.getDefaultDict<int, BE.AilmentSkillResource>(ref this.ailmentSkillResource_);
    }
  }

  public void useSkill(
    BL.Unit unit,
    BL.Skill skill,
    List<BL.Unit> targets,
    List<BL.Panel> panels,
    BL.BattleSkillResult bsr,
    BattleTimeManager btm,
    XorShift random = null)
  {
    Func<BattleskillEffect, bool> func1;
    Func<BattleskillEffect, bool> func2;
    btm.setScheduleAction((System.Action) (() =>
    {
      Dictionary<BL.Unit, int> target_prev_hps = new Dictionary<BL.Unit, int>();
      Dictionary<BL.Unit, bool> isRebirths = new Dictionary<BL.Unit, bool>();
      foreach (BL.Unit target in targets)
        isRebirths.Add(target, skill.targetType == BattleskillTargetType.dead_player_single && target.isDead);
      foreach (BL.ISkillEffectListUnit allUnit in BattleFuncs.getAllUnits(false, true, false))
        target_prev_hps.Add(allUnit.originalUnit, allUnit.hp);
      foreach (BL.Unit key in isRebirths.Keys)
      {
        if (!target_prev_hps.ContainsKey(key))
          target_prev_hps.Add(key, key.hp);
      }
      Tuple<int, int> usePosition = (Tuple<int, int>) null;
      if (unit != (BL.Unit) null)
      {
        BL.UnitPosition unitPosition = this.core.getUnitPosition(unit);
        usePosition = Tuple.Create<int, int>(unitPosition.row, unitPosition.column);
      }
      this.core.useSkillWith(unit, skill, targets, panels, bsr, (System.Action<BL.UseSkillWithResult>) (r =>
      {
        List<Tuple<BE.UnitResource, int, int>> dispHp = new List<Tuple<BE.UnitResource, int, int>>();
        foreach (BL.Unit displayNumberTarget in r.displayNumberTargets)
        {
          if (isRebirths.ContainsKey(displayNumberTarget) && isRebirths[displayNumberTarget])
            displayNumberTarget.rebirthBE(this);
          else if (displayNumberTarget.isView && target_prev_hps.ContainsKey(displayNumberTarget))
          {
            BE.UnitResource unitResource = this.unitResource[displayNumberTarget];
            dispHp.Add(Tuple.Create<BE.UnitResource, int, int>(unitResource, target_prev_hps[displayNumberTarget], displayNumberTarget.hp));
          }
        }
        if (r.dispHpUnit != (BL.Unit) null && r.dispHpUnit.isView && !r.displayNumberTargets.Contains(r.dispHpUnit))
        {
          BE.UnitResource unitResource = this.unitResource[r.dispHpUnit];
          dispHp.Add(Tuple.Create<BE.UnitResource, int, int>(unitResource, r.prevHp, r.dispHpUnit.hp));
        }
        foreach (BL.Unit key in target_prev_hps.Keys)
        {
          if (key.isView)
            this.unitResource[key].unitParts_.SetEffectMode(true, false);
        }
        System.Action action = (System.Action) (() =>
        {
          foreach (Tuple<BE.UnitResource, int, int> tuple in dispHp)
          {
            BattleUnitParts unitParts = tuple.Item1.unitParts_;
            unitParts.dispHpNumber(tuple.Item2, tuple.Item3);
            unitParts.setHpGauge(tuple.Item2, tuple.Item3);
          }
        });
        BE.SkillResource skillResource = this.skillResource[skill.skill.field_effect.ID];
        if (!r.lateDispHp && (UnityEngine.Object) skillResource.invokedEffectPrefab == (UnityEngine.Object) null)
        {
          action();
          action = (System.Action) null;
        }
        BL.Unit unit1 = !skill.skill.field_effect.invoked_effect_target || targets.Count < 1 ? unit : targets[0];
        Quaternion? nullable = new Quaternion?();
        if (unit != (BL.Unit) null && ((IEnumerable<BattleskillEffect>) skill.skill.Effects).Any<BattleskillEffect>(func1 ?? (func1 = (Func<BattleskillEffect, bool>) (x => x.EffectLogic.Enum == BattleskillEffectLogicEnum.skill_chain && x.checkLevel(skill.level) && x.GetInt(BattleskillEffectLogicArgumentEnum.excluding_slanting) == 3))))
        {
          BL.UnitPosition unitPosition1 = this.core.getUnitPosition(unit);
          if (targets.Any<BL.Unit>())
          {
            BL.UnitPosition unitPosition2 = this.core.getUnitPosition(targets[0]);
            nullable = new Quaternion?(Quaternion.Euler(0.0f, Mathf.Atan2((float) (unitPosition2.column - unitPosition1.column), (float) (unitPosition2.row - unitPosition1.row)) * 57.29578f, 0.0f));
          }
          else if (panels.Any<BL.Panel>())
            nullable = new Quaternion?(Quaternion.Euler(0.0f, Mathf.Atan2((float) (panels[0].column - unitPosition1.column), (float) (panels[0].row - unitPosition1.row)) * 57.29578f, 0.0f));
        }
        else if (unit != (BL.Unit) null && ((IEnumerable<BattleskillEffect>) skill.skill.Effects).Any<BattleskillEffect>(func2 ?? (func2 = (Func<BattleskillEffect, bool>) (x => x.EffectLogic.Enum == BattleskillEffectLogicEnum.skill_chain && x.checkLevel(skill.level) && x.GetInt(BattleskillEffectLogicArgumentEnum.excluding_slanting) == 4))))
        {
          BL.UnitPosition unitPosition = this.core.getUnitPosition(unit);
          nullable = new Quaternion?(Quaternion.Euler(0.0f, Mathf.Atan2((float) (unitPosition.column - usePosition.Item2), (float) (unitPosition.row - usePosition.Item1)) * 57.29578f, 0.0f));
        }
        BattleEffects battleEffects = Singleton<NGBattleManager>.GetInstance().battleEffects;
        BL.Unit unit2 = unit;
        BL.Skill skill1 = skill;
        List<BL.Unit> effectTargets = r.effectTargets;
        List<BL.Unit> invokeUnits = new List<BL.Unit>();
        invokeUnits.Add(unit1);
        System.Action targetAction = action;
        List<Quaternion?> invokedEffectRotate;
        if (!nullable.HasValue)
        {
          invokedEffectRotate = (List<Quaternion?>) null;
        }
        else
        {
          invokedEffectRotate = new List<Quaternion?>();
          invokedEffectRotate.Add(nullable);
        }
        battleEffects.skillFieldEffectStart(unit2, skill1, effectTargets, invokeUnits, targetAction, invokedEffectRotate);
        btm.setEnableWait(1.5f);
        if (((IEnumerable<BattleskillEffect>) skill.skill.Effects).Any<BattleskillEffect>((Func<BattleskillEffect, bool>) (x => x.effect_logic_BattleskillEffectLogic == 1001677)))
          btm.setEnableWait(new Func<bool>(this.unitResource[unit].unitParts_.checkEffectCompleted));
        btm.setScheduleAction((System.Action) (() =>
        {
          foreach (BL.Unit key in target_prev_hps.Keys)
          {
            if (key.isView)
              this.unitResource[key].unitParts_.SetEffectMode(false, false);
          }
        }), 0.0f, (System.Action) null, (Func<bool>) null, false);
      }), this.core, random);
      BL.UnitPosition unitPosition3 = this.core.getUnitPosition(unit);
      if (unitPosition3.completedCount >= 1)
      {
        List<List<BL.ExecuteSkillEffectResult>> result;
        List<BL.UnitPosition> unitPositionList = this.core.completedPositionExecuteSkillEffects(unitPosition3, out result, (HashSet<BL.ISkillEffectListUnit>) null);
        BattleStateController controller = Singleton<NGBattleManager>.GetInstance().getController<BattleStateController>();
        int num = 0;
        bool flag = false;
        foreach (BL.UnitPosition up in unitPositionList)
        {
          foreach (BL.ExecuteSkillEffectResult es in result[num++])
          {
            if (es.targets.Count > 0)
            {
              controller.doExecuteFacilitySkillEffects(up, es);
              flag = true;
            }
          }
        }
        if (flag)
          btm.setEnableWait(1.5f);
      }
      if (!(unit != (BL.Unit) null) || !unit.isPlayerForce || !((IEnumerable<BattleskillEffect>) skill.skill.Effects).Any<BattleskillEffect>((Func<BattleskillEffect, bool>) (x => x.EffectLogic.Enum == BattleskillEffectLogicEnum.random_choice && x.checkLevel(skill.level))))
        return;
      Singleton<NGBattleManager>.GetInstance().saveEnvironment(false);
    }), 0.0f, (System.Action) null, (Func<bool>) null, false);
  }

  public BE.DefaultDict<BL.Unit, BE.UnitResource> unitResource
  {
    get
    {
      return this.getDefaultDict<BL.Unit, BE.UnitResource>(ref this.unitResource_);
    }
  }

  public void rebirthUnits(List<BL.Unit> units, BattleTimeManager btm)
  {
    btm.setScheduleAction((System.Action) (() =>
    {
      foreach (BL.Unit unit in units)
      {
        unit.rebirth(this.core, true, true, false);
        unit.rebirthBE(this);
      }
    }), 0.0f, (System.Action) null, (Func<bool>) null, false);
  }

  public void setCurrentUnit_(BL.Unit unit)
  {
    this.core.setCurrentUnitWith(unit, (System.Action<BL.UnitPosition>) (up =>
    {
      if (up == null || this.core.phaseState.state == BL.Phase.pvp_disposition || (this.core.phaseState.state == BL.Phase.pvp_wait_preparing || this.core.phaseState.state == BL.Phase.enemy))
        return;
      up.cancelMove(this);
    }));
  }

  public void pushWaveStageDatas()
  {
    this.pushWaveEnemies();
    this.pushWaveDropData();
  }

  private void pushWaveEnemies()
  {
    this.waveEnemiesStack.Push(this.core.enemyUnits.value);
  }

  private void pushWaveDropData()
  {
    List<Tuple<BL.DropData, int>> tupleList = new List<Tuple<BL.DropData, int>>();
    for (int row = 0; row < this.core.getFieldHeight(); ++row)
    {
      for (int column = 0; column < this.core.getFieldWidth(); ++column)
      {
        BL.Panel fieldPanel = this.core.getFieldPanel(row, column);
        if (fieldPanel.hasEvent && fieldPanel.fieldEvent.isCompleted)
          tupleList.Add(new Tuple<BL.DropData, int>(fieldPanel.fieldEvent, fieldPanel.fieldEventId));
      }
    }
    this.waveDropStack.Push(tupleList);
  }

  public BE.DefaultDict<BL.Weapon, BE.WeaponResource> weaponResource
  {
    get
    {
      return this.getDefaultDict<BL.Weapon, BE.WeaponResource>(ref this.weaponResource_);
    }
  }

  [Serializable]
  public class DropDataResource
  {
    [NonSerialized]
    private GameObject mPrefab;

    public GameObject prefab
    {
      get
      {
        return this.mPrefab;
      }
      set
      {
        this.mPrefab = value;
      }
    }
  }

  public class DefaultDict<TKey, TValue> where TValue : new()
  {
    private Dictionary<TKey, TValue> dict = new Dictionary<TKey, TValue>();

    public bool ContainsKey(TKey key)
    {
      return this.dict.ContainsKey(key);
    }

    public TValue this[TKey key]
    {
      get
      {
        TValue obj1;
        if (this.dict.TryGetValue(key, out obj1))
          return obj1;
        TValue obj2 = new TValue();
        this.dict.Add(key, obj2);
        return obj2;
      }
      set
      {
        this.dict[key] = value;
      }
    }

    public void cleanup()
    {
      this.dict.Clear();
    }
  }

  public class PanelResource
  {
    private GameObject mGameObject;

    public GameObject gameObject
    {
      get
      {
        return this.mGameObject;
      }
      set
      {
        this.mGameObject = value;
      }
    }
  }

  public class StageResource
  {
    private GameObject mPrefab;

    public GameObject prefab
    {
      get
      {
        return this.mPrefab;
      }
      set
      {
        this.mPrefab = value;
      }
    }
  }

  public class ItemResource
  {
    [NonSerialized]
    private GameObject mTargetEffectPrefab;

    public GameObject targetEffectPrefab
    {
      get
      {
        return this.mTargetEffectPrefab;
      }
      set
      {
        this.mTargetEffectPrefab = value;
      }
    }
  }

  public class SkillResource
  {
    [NonSerialized]
    private GameObject mEffectPrefab;
    [NonSerialized]
    private GameObject mTargetEffectPrefab;
    [NonSerialized]
    private GameObject mInvokedEffectPrefab;

    public GameObject effectPrefab
    {
      get
      {
        return this.mEffectPrefab;
      }
      set
      {
        this.mEffectPrefab = value;
      }
    }

    public GameObject targetEffectPrefab
    {
      get
      {
        return this.mTargetEffectPrefab;
      }
      set
      {
        this.mTargetEffectPrefab = value;
      }
    }

    public GameObject invokedEffectPrefab
    {
      get
      {
        return this.mInvokedEffectPrefab;
      }
      set
      {
        this.mInvokedEffectPrefab = value;
      }
    }
  }

  public class AilmentSkillResource
  {
    [NonSerialized]
    private GameObject mTargetEffectPrefab;

    public GameObject targetEffectPrefab
    {
      get
      {
        return this.mTargetEffectPrefab;
      }
      set
      {
        this.mTargetEffectPrefab = value;
      }
    }
  }

  public class UnitResource
  {
    [NonSerialized]
    private GameObject mGameObject;
    [NonSerialized]
    private GameObject mPrefab;
    [NonSerialized]
    private GameObject mEquipPrefab_a;
    [NonSerialized]
    private GameObject mEquipPrefab_b;
    [NonSerialized]
    private GameObject mBikePrefab;
    [NonSerialized]
    private UnityEngine.Material mFaceMaterial;
    [NonSerialized]
    private UnityEngine.Material[] mCompleteMaterials;
    [NonSerialized]
    private UnityEngine.Material[] mCompleteEquipAMaterials;
    [NonSerialized]
    private UnityEngine.Material[] mCompleteEquipBMaterials;
    [NonSerialized]
    private UnityEngine.Material[] mCompleteBikeMaterials;
    [NonSerialized]
    private BattleUnitParts mUnitParts;
    [NonSerialized]
    private bool isSelectVoice;

    public GameObject gameObject
    {
      get
      {
        return this.mGameObject;
      }
      set
      {
        this.mGameObject = value;
      }
    }

    public GameObject prefab
    {
      get
      {
        return this.mPrefab;
      }
      set
      {
        this.mPrefab = value;
      }
    }

    public GameObject equipPrefab_a
    {
      get
      {
        return this.mEquipPrefab_a;
      }
      set
      {
        this.mEquipPrefab_a = value;
      }
    }

    public GameObject equipPrefab_b
    {
      get
      {
        return this.mEquipPrefab_b;
      }
      set
      {
        this.mEquipPrefab_b = value;
      }
    }

    public GameObject bikePrefab
    {
      get
      {
        return this.mBikePrefab;
      }
      set
      {
        this.mBikePrefab = value;
      }
    }

    public UnityEngine.Material faceMaterial
    {
      get
      {
        return this.mFaceMaterial;
      }
      set
      {
        this.mFaceMaterial = value;
      }
    }

    public UnityEngine.Material[] completeMaterials
    {
      get
      {
        return this.mCompleteMaterials;
      }
      set
      {
        this.mCompleteMaterials = value;
      }
    }

    public UnityEngine.Material[] completeBikeMaterials
    {
      get
      {
        return this.mCompleteBikeMaterials;
      }
      set
      {
        this.mCompleteBikeMaterials = value;
      }
    }

    public UnityEngine.Material[] completeEquipAMaterials
    {
      get
      {
        return this.mCompleteEquipAMaterials;
      }
      set
      {
        this.mCompleteEquipAMaterials = value;
      }
    }

    public UnityEngine.Material[] completeEquipBMaterials
    {
      get
      {
        return this.mCompleteEquipBMaterials;
      }
      set
      {
        this.mCompleteEquipBMaterials = value;
      }
    }

    public BattleUnitParts unitParts_
    {
      get
      {
        if ((UnityEngine.Object) this.gameObject == (UnityEngine.Object) null)
          return (BattleUnitParts) null;
        if ((UnityEngine.Object) this.mUnitParts == (UnityEngine.Object) null)
          this.mUnitParts = this.gameObject.GetComponent<BattleUnitParts>();
        return this.mUnitParts;
      }
    }

    public void PlayVoiceDuelStart(BL.Unit unit)
    {
      if (this.isSelectVoice || Singleton<NGSoundManager>.GetInstance().PlayVoicePriorityFirst(unit.unit, 75, 0, 0.0f) == -1)
        return;
      this.isSelectVoice = true;
    }

    public void cleanup()
    {
      this.mGameObject = (GameObject) null;
      this.mPrefab = (GameObject) null;
      this.mEquipPrefab_a = (GameObject) null;
      this.mEquipPrefab_b = (GameObject) null;
      this.mBikePrefab = (GameObject) null;
      this.mFaceMaterial = (UnityEngine.Material) null;
      this.mCompleteMaterials = (UnityEngine.Material[]) null;
      this.mCompleteEquipAMaterials = (UnityEngine.Material[]) null;
      this.mCompleteEquipBMaterials = (UnityEngine.Material[]) null;
      this.mCompleteBikeMaterials = (UnityEngine.Material[]) null;
      this.mUnitParts = (BattleUnitParts) null;
    }
  }

  public class WeaponResource
  {
    [NonSerialized]
    private GameObject mPrefab;

    public GameObject prefab
    {
      get
      {
        return this.mPrefab;
      }
      set
      {
        this.mPrefab = value;
      }
    }
  }
}
