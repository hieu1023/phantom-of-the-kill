﻿// Decompiled with JetBrains decompiler
// Type: Battle01PVPSetupTime
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;
using UnityEngine;

public class Battle01PVPSetupTime : BattleMonoBehaviour
{
  [SerializeField]
  private SpriteNumber setupTime;
  private BL.BattleModified<BL.StructValue<int>> timeLimitModified;

  protected override IEnumerator Start_Battle()
  {
    // ISSUE: reference to a compiler-generated field
    int num = this.\u003C\u003E1__state;
    Battle01PVPSetupTime battle01PvpSetupTime = this;
    if (num != 0)
      return false;
    // ISSUE: reference to a compiler-generated field
    this.\u003C\u003E1__state = -1;
    if (battle01PvpSetupTime.battleManager.gameEngine == null)
      return false;
    battle01PvpSetupTime.timeLimitModified = BL.Observe<BL.StructValue<int>>(battle01PvpSetupTime.battleManager.gameEngine.timeLimit);
    return false;
  }

  protected override void Update_Battle()
  {
    if (this.battleManager.gameEngine == null || !this.timeLimitModified.isChangedOnce())
      return;
    this.setupTime.setNumber(this.timeLimitModified.value.value);
  }
}
