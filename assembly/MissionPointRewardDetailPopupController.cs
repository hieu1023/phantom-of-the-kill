﻿// Decompiled with JetBrains decompiler
// Type: MissionPointRewardDetailPopupController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MissionPointRewardDetailPopupController : MonoBehaviour
{
  private static string descriptionTemplate = "ミッション達成数合計[FFFF00]★{0}以上[-]で獲得可能";
  private DailyMissionController controller;
  [SerializeField]
  private UIButton receiveButton;
  [SerializeField]
  private UIButton notClearedButton;
  [SerializeField]
  private UIButton haveReceivedButton;
  [SerializeField]
  private UIScrollView scrollView;
  [SerializeField]
  private UIGrid grid;
  [SerializeField]
  private UILabel description;
  private List<PointReward> pointRewardList;
  private int rewardBoxID;
  private System.Action updateAction;

  public IEnumerator Init(
    DailyMissionController controller,
    PointRewardBox pointRewardBox,
    int currentAcquiredPoint,
    bool isReceived,
    System.Action updateAction)
  {
    this.controller = controller;
    this.rewardBoxID = pointRewardBox.ID;
    this.updateAction = updateAction;
    this.description.SetTextLocalize(string.Format(MissionPointRewardDetailPopupController.descriptionTemplate, (object) pointRewardBox.point));
    this.receiveButton.gameObject.SetActive(false);
    this.notClearedButton.gameObject.SetActive(false);
    this.notClearedButton.isEnabled = false;
    this.haveReceivedButton.gameObject.SetActive(false);
    this.haveReceivedButton.isEnabled = false;
    if (isReceived)
    {
      this.haveReceivedButton.gameObject.SetActive(true);
    }
    else
    {
      bool flag = currentAcquiredPoint >= pointRewardBox.point;
      this.notClearedButton.gameObject.SetActive(!flag);
      this.receiveButton.gameObject.SetActive(flag);
    }
    this.pointRewardList = new List<PointReward>();
    string[] pointRewardItemIDs = pointRewardBox.point_reward_ids.Split(':');
    for (int i = 0; i < pointRewardItemIDs.Length; ++i)
    {
      GameObject gameObject = controller.missionPointRewardDetailItemPrefab.Clone(this.grid.transform);
      gameObject.transform.localPosition = new Vector3(gameObject.transform.localPosition.x, -this.grid.cellHeight * (float) i, gameObject.transform.localPosition.z);
      MissionPointRewardDetailItemController component = gameObject.GetComponent<MissionPointRewardDetailItemController>();
      int pointRewardItemID = int.Parse(pointRewardItemIDs[i]);
      PointReward pointRewardData = ((IEnumerable<PointReward>) MasterData.PointRewardList).FirstOrDefault<PointReward>((Func<PointReward, bool>) (x => x.ID == pointRewardItemID));
      this.pointRewardList.Add(pointRewardData);
      IEnumerator e = component.Init(pointRewardData);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    this.grid.Reposition();
    this.scrollView.ResetPosition();
  }

  public void OnClickCancelButton()
  {
    Singleton<PopupManager>.GetInstance().dismiss(false);
  }

  public void OnClickReceiveButton()
  {
    Singleton<PopupManager>.GetInstance().closeAll(false);
    this.controller.StartCoroutine(this.ReceivePointReward());
  }

  public IEnumerator ReceivePointReward()
  {
    CommonRoot commonRoot = Singleton<CommonRoot>.GetInstance();
    commonRoot.ShowLoadingLayer(1, false);
    Future<WebAPI.Response.DailymissionPointRewardReceive> future = WebAPI.DailymissionPointRewardReceive(this.rewardBoxID, (System.Action<WebAPI.Response.UserError>) (e =>
    {
      WebAPI.DefaultUserErrorCallback(e);
      Singleton<PopupManager>.GetInstance().closeAll(false);
      commonRoot = Singleton<CommonRoot>.GetInstance();
      commonRoot.HideLoadingLayer();
      commonRoot.DailyMissionController.Hide();
    }));
    IEnumerator e1 = future.Wait();
    while (e1.MoveNext())
      yield return e1.Current;
    e1 = (IEnumerator) null;
    commonRoot.HideLoadingLayer();
    if (future.HasResult && future.Result != null)
    {
      e1 = Singleton<PopupManager>.GetInstance().open(this.controller.getPointRewardEffectPopupPrefab, false, false, false, true, false, false, "SE_1006").GetComponent<MissionGetPointRewardEffectPopupController>().Init(this.pointRewardList, this.updateAction);
      while (e1.MoveNext())
        yield return e1.Current;
      e1 = (IEnumerator) null;
      Singleton<NGSoundManager>.GetInstance().playSE("SE_0534", false, 0.0f, -1);
    }
  }
}
