﻿// Decompiled with JetBrains decompiler
// Type: Startup00014StampFrame
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class Startup00014StampFrame : MonoBehaviour
{
  [SerializeField]
  private List<float> position_x = new List<float>();
  [SerializeField]
  private UITexture frame;
  [SerializeField]
  private UITexture arrow;

  public void ArrowPosition(int num)
  {
    this.arrow.transform.localPosition = new Vector3(this.position_x[num], 0.0f, 0.0f);
  }

  public void ChangeFrame(Texture texture)
  {
    this.frame.mainTexture = texture;
    this.frame.SetDimensions(this.frame.mainTexture.width, this.frame.mainTexture.height);
  }

  public void ArrowEnable(bool flag)
  {
    this.arrow.enabled = flag;
  }
}
