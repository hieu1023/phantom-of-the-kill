﻿// Decompiled with JetBrains decompiler
// Type: Shop00721SpecialShopProduction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using UnityEngine;

public class Shop00721SpecialShopProduction : MonoBehaviour
{
  [SerializeField]
  private GameObject soldout;
  [SerializeField]
  private GameObject slcHime;
  [SerializeField]
  private UILabel txt_Compensation;
  [SerializeField]
  private FloatButton BtnFormation;
  [SerializeField]
  private UI2DSprite BtnSprite;
  [SerializeField]
  private UILabel txtTime;
  [SerializeField]
  private UILabel txtProductNum;
  [SerializeField]
  private UILabel txtPrice;
  private Shop00721Menu m_menu;
  private int m_shopID;
  private int m_idx;
  private PlayerShopArticle m_article;

  public FloatButton Btn
  {
    get
    {
      return this.BtnFormation;
    }
  }

  public void Disable()
  {
    if (!((UnityEngine.Object) this.BtnFormation != (UnityEngine.Object) null))
      return;
    this.BtnFormation.isEnabled = false;
  }

  public void Enable()
  {
    if (!((UnityEngine.Object) this.BtnFormation != (UnityEngine.Object) null))
      return;
    this.BtnFormation.isEnabled = true;
  }

  private void SetTimeString(DateTime serverTime, DateTime? endTime)
  {
    if (!endTime.HasValue)
    {
      this.txtTime.gameObject.SetActive(false);
      this.txtTime.SetTextLocalize("");
    }
    else
    {
      this.txtTime.gameObject.SetActive(true);
      this.txtTime.SetTextLocalize(Consts.Format(Consts.GetInstance().SHOP_0074_SCROLL_UPDATE_SERVER_TIME, (IDictionary) new Hashtable()
      {
        {
          (object) "time",
          (object) (endTime.Value - serverTime).DisplayString()
        }
      }));
    }
  }

  private void SetRechargeTime(DateTime serverTime, PlayerShopArticle article)
  {
    TimeSpan timeSpan = article.recharge_at.Value - serverTime;
    int num1 = timeSpan.Hours < 0 ? 0 : timeSpan.Hours;
    int num2 = timeSpan.Minutes < 0 ? 0 : timeSpan.Minutes;
    int num3 = timeSpan.Seconds < 0 ? 0 : timeSpan.Seconds;
    if (num1 == 0 && num2 == 0 && num3 > 0)
    {
      num1 = 0;
      num2 = 1;
    }
    this.txtProductNum.gameObject.SetActive(true);
    this.txtProductNum.SetTextLocalize(Consts.Format(Consts.GetInstance().SHOP_00721_SPECIAL_SHOP_PRODUCTION_UPDATE_SERVER_TIME, (IDictionary) new Hashtable()
    {
      {
        (object) "time",
        (object) string.Format(Consts.GetInstance().SHOP_00721_SPECIAL_SHOP_PRODUCTION_UPDATE_SERVER_TIME2, (object) num1, (object) num2)
      }
    }));
  }

  private void SetLimitsStat(DateTime serverTime, PlayerShopArticle article)
  {
    this.BtnFormation.isEnabled = true;
    this.soldout.SetActive(false);
    this.txtProductNum.gameObject.SetActive(false);
    if (article.article.limit.HasValue || article.article.daily_limit.HasValue)
    {
      if (article.article.limit.HasValue && article.limit.Value <= 0 || article.article.daily_limit.HasValue && article.limit.Value <= 0)
        this.soldout.SetActive(true);
      else if (article.recharge_at.HasValue && serverTime < article.recharge_at.Value)
      {
        this.SetRechargeTime(serverTime, article);
      }
      else
      {
        this.txtProductNum.gameObject.SetActive(true);
        if (article.article.unit == "")
          this.txtProductNum.SetTextLocalize(string.Format(Consts.GetInstance().SHOP_0074_SCROLL_ARTICLE_LIMIT_VALUE, (object) article.limit.Value));
        else
          this.txtProductNum.SetTextLocalize(Consts.Format(Consts.GetInstance().SHOP_0074_SCROLL_ARTICLE_LIMIT_VALUE_EXT, (IDictionary) new Hashtable()
          {
            {
              (object) "num",
              (object) article.limit.Value
            },
            {
              (object) "unit",
              (object) article.article.unit
            }
          }));
      }
    }
    else
    {
      if (!article.recharge_at.HasValue || !(serverTime < article.recharge_at.Value))
        return;
      this.SetRechargeTime(serverTime, article);
    }
  }

  public IEnumerator Init(
    DateTime serverTime,
    int shopID,
    PlayerShopArticle article,
    Shop00721Menu menu,
    int idx)
  {
    this.m_idx = idx;
    this.m_menu = menu;
    this.m_shopID = shopID;
    this.m_article = article;
    IEnumerator e = Singleton<NGGameDataManager>.GetInstance().GetWebImage(article.banner_url, this.BtnSprite);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.SetTimeString(serverTime, article.end_at);
    this.SetLimitsStat(serverTime, article);
    this.txt_Compensation.gameObject.SetActive(false);
    if (article.article.pay_type == CommonPayType.coin || article.article.pay_type == CommonPayType.paid_coin)
    {
      this.slcHime.SetActive(true);
      this.txtPrice.SetTextLocalize(Consts.Format(Consts.GetInstance().SHOP_00721_TXT_PRICE_COIN, (IDictionary) new Hashtable()
      {
        {
          (object) "money",
          (object) article.article.price
        }
      }));
      if (article.article.pay_type == CommonPayType.paid_coin)
        this.txt_Compensation.gameObject.SetActive(true);
    }
    else if (article.article.pay_type == CommonPayType.currency)
    {
      this.slcHime.SetActive(false);
      this.txtPrice.SetTextLocalize(Consts.Format(Consts.GetInstance().SHOP_00721_TXT_PRICE, (IDictionary) new Hashtable()
      {
        {
          (object) "money",
          (object) article.article.price
        }
      }));
    }
    else
    {
      this.slcHime.SetActive(false);
      this.txtPrice.SetTextLocalize(Consts.Format(Consts.GetInstance().SHOP_00721_TXT_PRICE_COIN, (IDictionary) new Hashtable()
      {
        {
          (object) "money",
          (object) article.article.price
        }
      }));
    }
  }

  public void IbtnProduct()
  {
    if ((UnityEngine.Object) this.m_menu != (UnityEngine.Object) null)
      this.m_menu.CurrentShopIDX = this.m_idx;
    Shop00722Scene.changeScene(true, this.m_shopID, this.m_article, this.gameObject);
  }
}
