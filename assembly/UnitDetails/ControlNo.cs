﻿// Decompiled with JetBrains decompiler
// Type: UnitDetails.ControlNo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

namespace UnitDetails
{
  internal enum ControlNo
  {
    Limited,
    Guest,
    Memory,
    Material,
    ReincarnationType,
    OverkillersUnit,
    OverkillersBase,
    OverkillersSlot,
    OverkillersEdit,
    OverkillersMove,
    SelfAbility,
    ToutaPlusNoEnable,
    MyGvgAtkDeck,
    MyGvgDefDeck,
  }
}
