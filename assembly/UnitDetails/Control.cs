﻿// Decompiled with JetBrains decompiler
// Type: UnitDetails.Control
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace UnitDetails
{
  [Flags]
  public enum Control
  {
    Zero = 0,
    Limited = 1,
    Guest = 2,
    Memory = 4,
    Material = 8,
    ReincarnationType = 16, // 0x00000010
    OverkillersUnit = 32, // 0x00000020
    OverkillersBase = 64, // 0x00000040
    OverkillersSlot = 128, // 0x00000080
    OverkillersEdit = 256, // 0x00000100
    OverkillersMove = 512, // 0x00000200
    SelfAbility = 1024, // 0x00000400
    ToutaPlusNoEnable = 2048, // 0x00000800
    MyGvgAtkDeck = 4096, // 0x00001000
    MyGvgDefDeck = 8192, // 0x00002000
  }
}
