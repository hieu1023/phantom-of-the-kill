﻿// Decompiled with JetBrains decompiler
// Type: unit00497Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

public class unit00497Scene : NGSceneBase
{
  [SerializeField]
  private unit00497Menu menu;
  private PrincesEvolutionParam changeSceneParam;
  private string nowBgmName;

  public PrincesEvolutionParam ScenePara
  {
    get
    {
      return this.changeSceneParam;
    }
  }

  public static void ChangeScene(bool stack, PrincesEvolutionParam param)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("unit004_9_7", (stack ? 1 : 0) != 0, (object) param);
  }

  public IEnumerator onStartSceneAsync(PrincesEvolutionParam param)
  {
    this.changeSceneParam = param;
    IEnumerator e = this.menu.Init(param);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public void onStartScene()
  {
    Singleton<CommonRoot>.GetInstance().isLoading = false;
    Singleton<PopupManager>.GetInstance().closeAll(false);
    this.nowBgmName = Singleton<NGSoundManager>.GetInstance().GetBgmName(0);
    Singleton<NGSoundManager>.GetInstance().StopBgm(-1, 0.5f);
  }

  public void onStartScene(PrincesEvolutionParam param)
  {
    this.onStartScene();
  }

  public override void onEndScene()
  {
    base.onEndScene();
    Singleton<PopupManager>.GetInstance().open((GameObject) null, false, false, false, true, false, false, "SE_1006");
    Singleton<NGSoundManager>.GetInstance().PlayBgm(this.nowBgmName, 0, 0.0f, 0.5f, 0.5f);
  }

  public override IEnumerator onEndSceneAsync()
  {
    yield return (object) new WaitForSeconds(0.5f);
    this.menu.effect.gameObject.SetActive(false);
  }
}
