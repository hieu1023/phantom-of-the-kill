﻿// Decompiled with JetBrains decompiler
// Type: GearIconHistoryWithNumber
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;

public class GearIconHistoryWithNumber : WithNumber
{
  public override void pressButton()
  {
    if (!this.withNumberInfo.icon.withNumberInfo.buttonOn)
      return;
    base.pressButton();
    Singleton<NGSceneManager>.GetInstance().changeScene(this.withNumberInfo.icon.withNumberInfo.unitData.Gear.kind.Enum == GearKindEnum.smith || this.withNumberInfo.icon.withNumberInfo.unitData.Gear.kind.Enum == GearKindEnum.drilling || (this.withNumberInfo.icon.withNumberInfo.unitData.Gear.kind.Enum == GearKindEnum.special_drilling || this.withNumberInfo.icon.withNumberInfo.unitData.Gear.kind.Enum == GearKindEnum.sea_present) ? (this.withNumberInfo.icon.withNumberInfo.unitData.Gear.compose_kind.kind.Enum == GearKindEnum.smith || this.withNumberInfo.icon.withNumberInfo.unitData.Gear.compose_kind.kind.Enum == GearKindEnum.drilling || (this.withNumberInfo.icon.withNumberInfo.unitData.Gear.compose_kind.kind.Enum == GearKindEnum.special_drilling || this.withNumberInfo.icon.withNumberInfo.unitData.Gear.compose_kind.kind.Enum == GearKindEnum.sea_present) ? "guide011_4_2c" : "guide011_4_2b") : "guide011_4_2", true, (object) this.withNumberInfo.icon.withNumberInfo.unitData.Gear, (object) true, (object) 0);
  }
}
