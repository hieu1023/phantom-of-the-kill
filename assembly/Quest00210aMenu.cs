﻿// Decompiled with JetBrains decompiler
// Type: Quest00210aMenu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Quest00210aMenu : BackButtonMenuBase
{
  private List<SupplyItem> SupplyItems = new List<SupplyItem>();
  private List<SupplyItem> SaveDeck = new List<SupplyItem>();
  private List<int> takeItem = new List<int>();
  private int iconWidth = ItemIcon.Width;
  private int iconHeight = ItemIcon.Height;
  private int iconColumnValue = ItemIcon.ColumnValue;
  private int iconRowValue = ItemIcon.RowValue;
  private int iconScreenValue = ItemIcon.ScreenValue;
  private int iconMaxValue = ItemIcon.MaxValue;
  private List<ItemIcon> allSupplyIcon = new List<ItemIcon>();
  private List<SupplyItem> showSupplyItems = new List<SupplyItem>();
  [SerializeField]
  private NGxScroll2 scroll;
  private int select;
  private bool isInitialize;
  private bool limitedOnly;
  private Quest00210Scene.Mode mode;
  private float scrool_start_y;
  private GameObject SelectCountPopup;

  public IEnumerator InitSupplyList(Quest00210Menu.Param param, bool fromTower)
  {
    Quest00210aMenu quest00210aMenu = this;
    Singleton<CommonRoot>.GetInstance().isTouchBlock = true;
    quest00210aMenu.isInitialize = false;
    quest00210aMenu.limitedOnly = param.limitedOnly;
    quest00210aMenu.mode = param.mode;
    quest00210aMenu.scroll.Clear();
    quest00210aMenu.select = param.select;
    quest00210aMenu.SupplyItems = param.SupplyItems;
    quest00210aMenu.SaveDeck = param.SaveDeck;
    quest00210aMenu.showSupplyItems = ((IEnumerable<SupplyItem>) quest00210aMenu.SupplyItems.ShowSupplyItems()).ToList<SupplyItem>();
    if (param.removeButton)
    {
      SupplyItem supplyItem = quest00210aMenu.SupplyItems[0].Copy();
      supplyItem.removeButton = true;
      quest00210aMenu.showSupplyItems.Insert(0, supplyItem);
    }
    quest00210aMenu.takeItem.Clear();
    foreach (SupplyItem supplyItem in quest00210aMenu.SaveDeck)
    {
      if (supplyItem.DeckIndex > 0 && supplyItem.DeckIndex != quest00210aMenu.select)
        quest00210aMenu.takeItem.Add(supplyItem.Supply.ID);
    }
    Future<GameObject> countSelectF = Res.Prefabs.popup.popup_002_10_3.Load<GameObject>();
    IEnumerator e = countSelectF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    quest00210aMenu.SelectCountPopup = countSelectF.Result;
    Future<GameObject> prefabF = Res.Prefabs.ItemIcon.prefab.Load<GameObject>();
    e = prefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    GameObject result = prefabF.Result;
    quest00210aMenu.allSupplyIcon.Clear();
    for (int index = 0; index < Mathf.Min(ItemIcon.MaxValue, quest00210aMenu.showSupplyItems.Count); ++index)
    {
      ItemIcon component = UnityEngine.Object.Instantiate<GameObject>(result).GetComponent<ItemIcon>();
      quest00210aMenu.allSupplyIcon.Add(component);
    }
    quest00210aMenu.scroll.Reset();
    for (int index = 0; index < Mathf.Min(quest00210aMenu.iconMaxValue, quest00210aMenu.allSupplyIcon.Count); ++index)
    {
      quest00210aMenu.scroll.Add(quest00210aMenu.allSupplyIcon[index].gameObject, quest00210aMenu.iconWidth, quest00210aMenu.iconHeight, false);
      quest00210aMenu.allSupplyIcon[index].gameObject.SetActive(true);
    }
    quest00210aMenu.scroll.CreateScrollPoint(quest00210aMenu.iconHeight, quest00210aMenu.showSupplyItems.Count);
    quest00210aMenu.scroll.ResolvePosition();
    for (int index = 0; index < Mathf.Min(ItemIcon.MaxValue, quest00210aMenu.showSupplyItems.Count); ++index)
      quest00210aMenu.ResetItemIcon(index);
    for (int i = 0; i < Mathf.Min(ItemIcon.MaxValue, quest00210aMenu.showSupplyItems.Count); ++i)
    {
      e = quest00210aMenu.CreateItemIcon(i, i);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    for (int index = 0; index < quest00210aMenu.allSupplyIcon.Count; ++index)
      quest00210aMenu.allSupplyIcon[index].gameObject.SetActive(true);
    quest00210aMenu.scrool_start_y = quest00210aMenu.scroll.scrollView.transform.localPosition.y;
    quest00210aMenu.StartCoroutine(quest00210aMenu.LoadObject());
    quest00210aMenu.isInitialize = true;
    Singleton<CommonRoot>.GetInstance().isTouchBlock = false;
  }

  public void SelectRelease()
  {
    if (this.IsPushAndSet())
      return;
    this.scroll.Clear();
    this.backSceneByMode();
  }

  public void CountSelectPopUp(SupplyItem shotItem, UnityEngine.Sprite icon)
  {
    if (this.IsPushAndSet())
      return;
    Singleton<PopupManager>.GetInstance().open(this.SelectCountPopup, false, false, false, true, false, false, "SE_1006").GetComponent<Quest002103Menu>().ChangePopUp(this.SupplyItems, shotItem, icon, this.select, this.SaveDeck);
  }

  private IEnumerator CreateItemIconRange(int value)
  {
    for (int index = 0; index < this.allSupplyIcon.Count; ++index)
      this.allSupplyIcon[index].gameObject.SetActive(false);
    for (int i = 0; i < value; ++i)
    {
      IEnumerator e = this.CreateItemIcon(i, i);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    for (int index = 0; index < this.allSupplyIcon.Count; ++index)
      this.allSupplyIcon[index].gameObject.SetActive(true);
    Singleton<CommonRoot>.GetInstance().isTouchBlock = false;
  }

  private void ScrollUpdate()
  {
    if (!this.isInitialize || this.showSupplyItems.Count <= this.iconScreenValue)
      return;
    int num1 = this.iconHeight * 2;
    float num2 = this.scroll.scrollView.transform.localPosition.y - this.scrool_start_y;
    float num3 = (float) (Mathf.Max(0, this.showSupplyItems.Count - this.iconScreenValue - 1) / this.iconColumnValue * this.iconHeight);
    float num4 = (float) (this.iconHeight * this.iconRowValue);
    if ((double) num2 < 0.0)
      num2 = 0.0f;
    if ((double) num2 > (double) num3)
      num2 = num3;
    bool flag;
    do
    {
      flag = false;
      int item_index = 0;
      foreach (GameObject gameObject in this.scroll)
      {
        GameObject item = gameObject;
        float num5 = item.transform.localPosition.y + num2;
        if ((double) num5 > (double) num1)
        {
          int? nullable = this.showSupplyItems.FirstIndexOrNull<SupplyItem>((Func<SupplyItem, bool>) (v => (UnityEngine.Object) v.icon != (UnityEngine.Object) null && (UnityEngine.Object) v.icon.gameObject == (UnityEngine.Object) item));
          int info_index = nullable.Value + this.iconMaxValue;
          if (nullable.HasValue && info_index < (this.showSupplyItems.Count + 4) / 5 * 5)
          {
            item.transform.localPosition = new Vector3(item.transform.localPosition.x, item.transform.localPosition.y - num4, 0.0f);
            if (info_index >= this.showSupplyItems.Count)
              item.SetActive(false);
            else if (this.showSupplyItems[info_index].removeButton || ItemIcon.IsCache(this.showSupplyItems[info_index]))
              this.CreateItemIconCache(info_index, item_index);
            else
              this.StartCoroutine(this.CreateItemIcon(info_index, item_index));
            flag = true;
          }
        }
        else if ((double) num5 < -((double) num4 - (double) num1))
        {
          int num6 = this.iconMaxValue;
          if (!item.activeSelf)
          {
            item.SetActive(true);
            num6 = 0;
          }
          int? nullable = this.showSupplyItems.FirstIndexOrNull<SupplyItem>((Func<SupplyItem, bool>) (v => (UnityEngine.Object) v.icon != (UnityEngine.Object) null && (UnityEngine.Object) v.icon.gameObject == (UnityEngine.Object) item));
          int info_index = nullable.Value - num6;
          if (nullable.HasValue && info_index >= 0)
          {
            item.transform.localPosition = new Vector3(item.transform.localPosition.x, item.transform.localPosition.y + num4, 0.0f);
            if (this.showSupplyItems[info_index].removeButton || ItemIcon.IsCache(this.showSupplyItems[info_index]))
              this.CreateItemIconCache(info_index, item_index);
            else
              this.StartCoroutine(this.CreateItemIcon(info_index, item_index));
            flag = true;
          }
        }
        ++item_index;
      }
    }
    while (flag);
  }

  private void ResetItemIcon(int index)
  {
    this.allSupplyIcon[index].gameObject.SetActive(false);
  }

  private IEnumerator CreateItemIcon(int info_index, int item_index)
  {
    ItemIcon itemIcon = this.allSupplyIcon[item_index];
    SupplyItem showSupplyItem = this.showSupplyItems[info_index];
    this.showSupplyItems.Where<SupplyItem>((Func<SupplyItem, bool>) (a => (UnityEngine.Object) a.icon == (UnityEngine.Object) itemIcon)).ForEach<SupplyItem>((System.Action<SupplyItem>) (b => b.icon = (ItemIcon) null));
    showSupplyItem.icon = itemIcon;
    if (showSupplyItem.removeButton)
    {
      itemIcon.InitByRemoveButton();
    }
    else
    {
      IEnumerator e = itemIcon.InitBySupplyItem(this.showSupplyItems[info_index]);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    this.CreateItemIconSharing(info_index, item_index);
  }

  private void CreateItemIconCache(int info_index, int item_index)
  {
    ItemIcon itemIcon = this.allSupplyIcon[item_index];
    SupplyItem showSupplyItem = this.showSupplyItems[info_index];
    this.showSupplyItems.Where<SupplyItem>((Func<SupplyItem, bool>) (a => (UnityEngine.Object) a.icon == (UnityEngine.Object) itemIcon)).ForEach<SupplyItem>((System.Action<SupplyItem>) (b => b.icon = (ItemIcon) null));
    showSupplyItem.icon = itemIcon;
    if (showSupplyItem.removeButton)
      itemIcon.InitByRemoveButton();
    else
      itemIcon.InitBySupplyItemCache(showSupplyItem);
    this.CreateItemIconSharing(info_index, item_index);
  }

  private void CreateItemIconSharing(int info_index, int item_index)
  {
    ItemIcon itemIcon = this.allSupplyIcon[item_index];
    SupplyItem s_item = this.showSupplyItems[info_index];
    if (s_item.removeButton)
    {
      itemIcon.onClick = (System.Action<ItemIcon>) (supplyicon => this.SelectRelease());
      itemIcon.ForBattle = false;
      itemIcon.Gray = false;
    }
    else if (this.takeItem.Contains(s_item.Supply.ID))
    {
      itemIcon.onClick = (System.Action<ItemIcon>) (supplyicon => {});
      itemIcon.ForBattle = true;
      itemIcon.Gray = true;
    }
    else
    {
      itemIcon.onClick = !this.limitedOnly ? (System.Action<ItemIcon>) (supplyicon => this.CountSelectPopUp(s_item, supplyicon.IconSprite)) : (System.Action<ItemIcon>) (supplyicon =>
      {
        foreach (SupplyItem supplyItem in this.SupplyItems)
        {
          if (supplyItem.Supply.ID == s_item.Supply.ID)
          {
            supplyItem.SelectCount = 1;
            supplyItem.DeckIndex = this.select;
            break;
          }
        }
        this.backSceneByMode();
      });
      itemIcon.ForBattle = false;
      itemIcon.Gray = false;
    }
    if (s_item.removeButton)
      EventDelegate.Set(itemIcon.supply.button.onLongPress, (EventDelegate.Callback) (() => {}));
    else
      EventDelegate.Set(itemIcon.supply.button.onLongPress, (EventDelegate.Callback) (() => Bugu00561Scene.changeScene(true, new GameCore.ItemInfo(((IEnumerable<PlayerItem>) SMManager.Get<PlayerItem[]>()).Where<PlayerItem>((Func<PlayerItem, bool>) (x => x.supply != null && x.supply.ID == s_item.Supply.ID)).FirstOrDefault<PlayerItem>()))));
  }

  private IEnumerator LoadObject()
  {
    if (this.showSupplyItems.Count > this.iconMaxValue)
    {
      for (int i = this.iconMaxValue; i < this.showSupplyItems.Count; ++i)
      {
        IEnumerator e = ItemIcon.LoadSprite(this.showSupplyItems[i]);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
      }
    }
  }

  protected override void Update()
  {
    base.Update();
    this.ScrollUpdate();
  }

  public void onEndScene()
  {
  }

  public virtual void Foreground()
  {
    Debug.Log((object) "click default event Foreground");
  }

  private void backSceneByMode()
  {
    switch (this.mode)
    {
      case Quest00210Scene.Mode.Tower:
        Tower029SupplyEditScene.ChangeScene(this.SupplyItems);
        break;
      case Quest00210Scene.Mode.Raid:
        Quest00210Scene.changeScene(false, this.mode, this.SupplyItems);
        break;
      default:
        Quest00210Scene.changeScene(false, this.SupplyItems);
        break;
    }
  }

  public virtual void IbtnBack()
  {
    if (this.IsPushAndSet())
      return;
    this.SupplyItems = this.SaveDeck.Copy();
    this.backSceneByMode();
  }

  public override void onBackButton()
  {
    this.IbtnBack();
  }

  public virtual void VScrollBar()
  {
    Debug.Log((object) "click default event VScrollBar");
  }
}
