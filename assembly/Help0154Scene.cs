﻿// Decompiled with JetBrains decompiler
// Type: Help0154Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;
using System;
using System.Collections;

public class Help0154Scene : NGSceneBase
{
  public Help0154Menu menu;

  public static void ChangeScene(bool stack)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("help015_4", stack, (object[]) Array.Empty<object>());
  }

  public IEnumerator onStartSceneAsync()
  {
    // ISSUE: reference to a compiler-generated field
    int num = this.\u003C\u003E1__state;
    Help0154Scene help0154Scene = this;
    if (num != 0)
      return false;
    // ISSUE: reference to a compiler-generated field
    this.\u003C\u003E1__state = -1;
    if (Singleton<NGGameDataManager>.GetInstance().IsEarth)
      help0154Scene.bgmName = "bgm104";
    return false;
  }

  public void onStartScene()
  {
    this.menu.InitContact(SMManager.Get<Player>());
    Singleton<CommonRoot>.GetInstance().isLoading = false;
  }
}
