﻿// Decompiled with JetBrains decompiler
// Type: JobChangeUtil
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using JobChangeData;
using MasterDataTable;
using SM;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UniLinq;

public static class JobChangeUtil
{
  public static PlayerUnit[] getTargets()
  {
    return ((IEnumerable<PlayerUnit>) SMManager.Get<PlayerUnit[]>()).Where<PlayerUnit>((Func<PlayerUnit, bool>) (pu => JobChangeUtil.getJobChangePatterns(pu) != null)).ToArray<PlayerUnit>();
  }

  public static PlayerUnit[] createJobChangePattern(
    PlayerUnit playerUnit,
    out bool isExist)
  {
    JobChangePatterns jobChangePatterns = JobChangeUtil.getJobChangePatterns(playerUnit);
    if (jobChangePatterns == null)
    {
      isExist = false;
      return Enumerable.Repeat<PlayerUnit>(JobChangeUtil.createPlayerUnit(playerUnit, playerUnit.job_id), DefValues.NUM_CHANGETYPE).ToArray<PlayerUnit>();
    }
    isExist = true;
    List<PlayerUnit> playerUnitList = new List<PlayerUnit>(DefValues.NUM_CHANGETYPE)
    {
      JobChangeUtil.createPlayerUnit(playerUnit, jobChangePatterns.job1_UnitJob),
      JobChangeUtil.createPlayerUnit(playerUnit, jobChangePatterns.job2_UnitJob)
    };
    if (jobChangePatterns.job3_UnitJob.HasValue)
    {
      playerUnitList.Add(JobChangeUtil.createPlayerUnit(playerUnit, jobChangePatterns.job3_UnitJob.Value));
      if (jobChangePatterns.job4_UnitJob.HasValue)
        playerUnitList.Add(JobChangeUtil.createPlayerUnit(playerUnit, jobChangePatterns.job4_UnitJob.Value));
    }
    return playerUnitList.ToArray();
  }

  public static JobChangePatterns getJobChangePatterns(PlayerUnit playerUnit)
  {
    return Array.Find<JobChangePatterns>(MasterData.JobChangePatternsList, (Predicate<JobChangePatterns>) (p => p.unit_UnitUnit == playerUnit._unit && p.job_UnitJob == playerUnit.job_id));
  }

  public static PlayerMaterialUnit[] createPlayerMaterialUnits(
    JobChangeMaterials materials)
  {
    List<PlayerMaterialUnit> playerMaterialUnitList = new List<PlayerMaterialUnit>(DefValues.NUM_MATERIALSLOT);
    int id = 1;
    if (materials.material1_UnitUnit.HasValue)
      playerMaterialUnitList.Add(JobChangeUtil.createPlayerMaterialUnit(id++, materials.material1_UnitUnit.Value, materials.quantity1));
    if (materials.material2_UnitUnit.HasValue)
      playerMaterialUnitList.Add(JobChangeUtil.createPlayerMaterialUnit(id++, materials.material2_UnitUnit.Value, materials.quantity2));
    if (materials.material3_UnitUnit.HasValue)
      playerMaterialUnitList.Add(JobChangeUtil.createPlayerMaterialUnit(id++, materials.material3_UnitUnit.Value, materials.quantity3));
    if (materials.material4_UnitUnit.HasValue)
      playerMaterialUnitList.Add(JobChangeUtil.createPlayerMaterialUnit(id++, materials.material4_UnitUnit.Value, materials.quantity4));
    if (materials.material5_UnitUnit.HasValue)
      playerMaterialUnitList.Add(JobChangeUtil.createPlayerMaterialUnit(id, materials.material5_UnitUnit.Value, materials.quantity5));
    return playerMaterialUnitList.ToArray();
  }

  private static PlayerMaterialUnit createPlayerMaterialUnit(
    int id,
    int materialId,
    int quantity)
  {
    PlayerMaterialUnit playerMaterialUnit1 = new PlayerMaterialUnit();
    PlayerMaterialUnit playerMaterialUnit2 = Array.Find<PlayerMaterialUnit>(SMManager.Get<PlayerMaterialUnit[]>(), (Predicate<PlayerMaterialUnit>) (x => x._unit == materialId));
    playerMaterialUnit1.id = playerMaterialUnit2 != null ? playerMaterialUnit2.id : id;
    playerMaterialUnit1._unit = materialId;
    playerMaterialUnit1.quantity = quantity;
    return playerMaterialUnit1;
  }

  public static bool checkCompletedMaterials(
    PlayerMaterialUnit[] playerMaterials,
    PlayerMaterialUnit[] materials)
  {
    foreach (PlayerMaterialUnit material in materials)
    {
      PlayerMaterialUnit m = material;
      if (m == null || m.quantity <= 0)
        return false;
      PlayerMaterialUnit playerMaterialUnit = Array.Find<PlayerMaterialUnit>(playerMaterials, (Predicate<PlayerMaterialUnit>) (p => p._unit == m._unit));
      if (playerMaterialUnit == null || playerMaterialUnit.quantity < m.quantity)
        return false;
    }
    return true;
  }

  public static PlayerUnit createPlayerUnit(PlayerUnit original, int jobId)
  {
    PlayerUnit playerUnit = original.Clone();
    if (playerUnit.job_id != jobId)
    {
      playerUnit.clearInitialGear();
      JobChangeUtil.setJobParam(playerUnit, jobId);
      if (original.equippedGear != (PlayerItem) null && !original.equippedGear.gear.enableEquipmentUnit(playerUnit))
        playerUnit.primary_equipped_gear = new PlayerItem();
    }
    return playerUnit;
  }

  private static void setJobParam(PlayerUnit target, int jobId)
  {
    target.job_id = jobId;
    MasterDataTable.UnitJob jobData = target.getJobData();
    UnitUnit unit = target.unit;
    target.hp = JobChangeUtil.deepClone<PlayerUnitHp>(target.hp);
    target.hp.initial = unit.hp_initial + jobData.hp_initial;
    target.strength = JobChangeUtil.deepClone<PlayerUnitStrength>(target.strength);
    target.strength.initial = unit.strength_initial + jobData.strength_initial;
    target.vitality = JobChangeUtil.deepClone<PlayerUnitVitality>(target.vitality);
    target.vitality.initial = unit.vitality_initial + jobData.vitality_initial;
    target.intelligence = JobChangeUtil.deepClone<PlayerUnitIntelligence>(target.intelligence);
    target.intelligence.initial = unit.intelligence_initial + jobData.intelligence_initial;
    target.mind = JobChangeUtil.deepClone<PlayerUnitMind>(target.mind);
    target.mind.initial = unit.mind_initial + jobData.mind_initial;
    target.agility = JobChangeUtil.deepClone<PlayerUnitAgility>(target.agility);
    target.agility.initial = unit.agility_initial + jobData.agility_initial;
    target.dexterity = JobChangeUtil.deepClone<PlayerUnitDexterity>(target.dexterity);
    target.dexterity.initial = unit.dexterity_initial + jobData.dexterity_initial;
    target.lucky = JobChangeUtil.deepClone<PlayerUnitLucky>(target.lucky);
    target.lucky.initial = unit.lucky_initial + jobData.lucky_initial;
    target.job_abilities = ((IEnumerable<JobCharacteristics>) jobData.JobAbilities).Select<JobCharacteristics, PlayerUnitJob_abilities>((Func<JobCharacteristics, PlayerUnitJob_abilities>) (j => JobChangeUtil.createUnitJobAbility(j))).ToArray<PlayerUnitJob_abilities>();
    target.move = jobData.movement;
  }

  private static T deepClone<T>(T src) where T : class
  {
    using (MemoryStream memoryStream = new MemoryStream())
    {
      BinaryFormatter binaryFormatter = new BinaryFormatter();
      binaryFormatter.Serialize((Stream) memoryStream, (object) src);
      memoryStream.Seek(0L, SeekOrigin.Begin);
      return (T) binaryFormatter.Deserialize((Stream) memoryStream);
    }
  }

  private static PlayerUnitJob_abilities createUnitJobAbility(
    JobCharacteristics dat)
  {
    return new PlayerUnitJob_abilities()
    {
      job_ability_id = dat.ID,
      skill_id = dat.skill_BattleskillSkill,
      level = 0
    };
  }

  public static bool checkDiff3DModel(PlayerUnit a, PlayerUnit b)
  {
    return a._unit != b._unit || a.job_id != b.job_id;
  }

  public static int GetJobIdChangePatternsConditions(
    PlayerUnit playerUnit,
    WebAPI.Response.UnitPreviewJob previewJobResponse)
  {
    int num1 = 0;
    List<JobChangePatterns> list = ((IEnumerable<JobChangePatterns>) MasterData.JobChangePatternsList).Where<JobChangePatterns>((Func<JobChangePatterns, bool>) (x => x.unit_UnitUnit == playerUnit._unit)).OrderBy<JobChangePatterns, int>((Func<JobChangePatterns, int>) (x => x.ID)).ToList<JobChangePatterns>();
    if (list.Count<JobChangePatterns>() == 4)
    {
      MasterDataTable.UnitJob job1 = list[1].job;
      MasterDataTable.UnitJob job2 = list[2].job;
      MasterDataTable.UnitJob job3 = list[3].job;
      if (!((IEnumerable<int>) previewJobResponse.changed_job_ids).Contains<int>(job3.ID))
      {
        int num2 = job1.JobAbilities.Length + job2.JobAbilityIds.Length;
        int num3 = 0;
        foreach (JobCharacteristics jobAbility in job1.JobAbilities)
        {
          JobCharacteristics job1Ability = jobAbility;
          WebAPI.Response.UnitPreviewJobJob_abilities previewJobJobAbilities = ((IEnumerable<WebAPI.Response.UnitPreviewJobJob_abilities>) previewJobResponse.job_abilities).First<WebAPI.Response.UnitPreviewJobJob_abilities>((Func<WebAPI.Response.UnitPreviewJobJob_abilities, bool>) (x => x.job_ability_id == job1Ability.ID));
          if (previewJobJobAbilities.level >= MasterData.BattleskillSkill[previewJobJobAbilities.skill_id].upper_level)
            ++num3;
        }
        foreach (JobCharacteristics jobAbility in job2.JobAbilities)
        {
          JobCharacteristics job2Ability = jobAbility;
          WebAPI.Response.UnitPreviewJobJob_abilities previewJobJobAbilities = ((IEnumerable<WebAPI.Response.UnitPreviewJobJob_abilities>) previewJobResponse.job_abilities).First<WebAPI.Response.UnitPreviewJobJob_abilities>((Func<WebAPI.Response.UnitPreviewJobJob_abilities, bool>) (x => x.job_ability_id == job2Ability.ID));
          if (previewJobJobAbilities.level >= MasterData.BattleskillSkill[previewJobJobAbilities.skill_id].upper_level)
            ++num3;
        }
        if (num3 < num2)
          return job3.ID;
      }
    }
    return num1;
  }
}
