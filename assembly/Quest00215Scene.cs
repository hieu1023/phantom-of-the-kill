﻿// Decompiled with JetBrains decompiler
// Type: Quest00215Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

public class Quest00215Scene : NGSceneBase
{
  [SerializeField]
  private Quest00215Menu menu;
  private bool doneDisplay;

  public IEnumerator onStartSceneAsync()
  {
    if (!this.doneDisplay)
    {
      IEnumerator e = this.menu.Init(100111);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.menu.ScrollContainerResolvePosition();
      this.doneDisplay = true;
    }
  }

  public IEnumerator onStartSceneAsync(int id)
  {
    if (!this.doneDisplay)
    {
      IEnumerator e = this.menu.Init(id);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.menu.ScrollContainerResolvePosition();
      this.doneDisplay = true;
    }
  }
}
