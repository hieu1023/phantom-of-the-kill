﻿// Decompiled with JetBrains decompiler
// Type: Bugu005ItemListMenuBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Bugu005ItemListMenuBase : BackButtonMenuBase
{
  protected int iconWidth = ItemIcon.Width;
  protected int iconHeight = ItemIcon.Height;
  protected int iconColumnValue = ItemIcon.ColumnValue;
  protected int iconRowValue = ItemIcon.RowValue;
  protected int iconScreenValue = ItemIcon.ScreenValue;
  protected int iconMaxValue = ItemIcon.MaxValue;
  protected int itemCount = -1;
  protected int itemFavoriteCount = -1;
  protected int itemEquipCount = -1;
  protected List<ItemIcon> AllItemIcon = new List<ItemIcon>();
  protected List<InventoryItem> InventoryItems = new List<InventoryItem>();
  protected List<InventoryItem> DisplayItems = new List<InventoryItem>();
  protected bool[] filter = new bool[43];
  private ItemSortAndFilter.SORT_TYPES sortCategory = ItemSortAndFilter.SORT_TYPES.BranchOfWeapon;
  public bool isEquipFirst = true;
  protected GameObject ItemIconPrefab;
  protected bool InitializeEnd;
  protected float scroolStartY;
  protected bool isUpdateIcon;
  private SortAndFilter.SORT_TYPE_ORDER_BUY orderBuySort;
  protected GameObject SortPopupPrefab;
  [SerializeField]
  protected NGxScroll2 scroll;
  [SerializeField]
  protected ItemSortAndFilter.SORT_TYPES CurrentSortType;
  [SerializeField]
  protected UISprite SortSprite;
  [SerializeField]
  private GameObject dir_noList;
  protected GameObject reisouPopupPrefab;
  protected long? revisionItemList_;
  protected long? revisionMaterialList_;
  protected GameCore.ItemInfo baseInfo;

  public bool[] Filter
  {
    get
    {
      return this.filter;
    }
    set
    {
      this.filter = value;
    }
  }

  public ItemSortAndFilter.SORT_TYPES SortCategory
  {
    get
    {
      return this.sortCategory;
    }
    set
    {
      this.sortCategory = value;
    }
  }

  public SortAndFilter.SORT_TYPE_ORDER_BUY OrderBuySort
  {
    get
    {
      return this.orderBuySort;
    }
    set
    {
      this.orderBuySort = value;
    }
  }

  protected Future<GameObject> GetSortAndFilterPopupGameObject()
  {
    return Res.Prefabs.popup.popup_Item_Sort__anim_popup01.Load<GameObject>();
  }

  public virtual Persist<Persist.ItemSortAndFilterInfo> GetPersist()
  {
    return (Persist<Persist.ItemSortAndFilterInfo>) null;
  }

  protected virtual List<PlayerItem> GetItemList()
  {
    return (List<PlayerItem>) null;
  }

  protected virtual long GetRevisionItemList()
  {
    return 0;
  }

  protected virtual List<PlayerMaterialGear> GetMaterialList()
  {
    return (List<PlayerMaterialGear>) null;
  }

  protected virtual long GetRevisionMaterialList()
  {
    return 0;
  }

  protected virtual void UpdateInvetoryItem(InventoryItem invItem, PlayerItem item)
  {
    invItem.Item.Set(item);
  }

  protected virtual void UpdateInvetoryItem(InventoryItem invItem, PlayerMaterialGear item)
  {
    invItem.Item.Set(item);
  }

  protected virtual void UpdateInventoryItemList()
  {
    InventoryItem[] array1 = this.InventoryItems.Where<InventoryItem>((Func<InventoryItem, bool>) (x =>
    {
      if (x.Item == null || x.removeButton)
        return false;
      return x.Item.isWeapon || x.Item.isSupply;
    })).ToArray<InventoryItem>();
    if (array1 != null && ((IEnumerable<InventoryItem>) array1).Any<InventoryItem>())
    {
      PlayerItem[] array2 = SMManager.Get<PlayerItem[]>();
      foreach (InventoryItem inventoryItem in array1)
      {
        InventoryItem invItem = inventoryItem;
        PlayerItem playerItem = Array.Find<PlayerItem>(array2, (Predicate<PlayerItem>) (x => x.id == invItem.Item.itemID));
        if (playerItem != (PlayerItem) null)
          this.UpdateInvetoryItem(invItem, playerItem);
      }
    }
    InventoryItem[] array3 = this.InventoryItems.Where<InventoryItem>((Func<InventoryItem, bool>) (x =>
    {
      if (x.Item == null || x.removeButton)
        return false;
      return x.Item.isCompse || x.Item.isExchangable;
    })).ToArray<InventoryItem>();
    if (array3 != null && ((IEnumerable<InventoryItem>) array3).Any<InventoryItem>())
    {
      PlayerMaterialGear[] array2 = SMManager.Get<PlayerMaterialGear[]>();
      foreach (InventoryItem inventoryItem in array3)
      {
        InventoryItem invItem = inventoryItem;
        PlayerMaterialGear playerMaterialGear = Array.Find<PlayerMaterialGear>(array2, (Predicate<PlayerMaterialGear>) (x => x.id == invItem.Item.itemID));
        if (playerMaterialGear != (PlayerMaterialGear) null)
          this.UpdateInvetoryItem(invItem, playerMaterialGear);
      }
    }
    this.DisplayIconAndBottomInfoUpdate();
    this.isUpdateIcon = true;
  }

  protected virtual void CreateItemIconAdvencedSetting(int inventoryItemIdx, int allItemIdx)
  {
    ItemIcon itemIcon = this.AllItemIcon[allItemIdx];
    InventoryItem displayItem = this.DisplayItems[inventoryItemIdx];
    itemIcon.onClick = (System.Action<ItemIcon>) (playeritem => this.ChangeDetailScene(playeritem.ItemInfo));
    if (displayItem.Item.isSupply || displayItem.Item.isExchangable || (displayItem.Item.isCompse || displayItem.Item.isWeaponMaterial))
    {
      itemIcon.QuantitySupply = true;
      itemIcon.EnableQuantity(displayItem.Item.quantity);
    }
    else
      itemIcon.QuantitySupply = false;
    itemIcon.ForBattle = displayItem.Item.ForBattle;
    itemIcon.Favorite = displayItem.Item.favorite;
    itemIcon.Gray = false;
    itemIcon.SelectedQuantity(0);
    itemIcon.Deselect();
    itemIcon.EnableLongPressEvent(new System.Action<GameCore.ItemInfo>(this.ChangeDetailScene));
  }

  protected virtual IEnumerator InitExtension()
  {
    yield break;
  }

  protected virtual void BottomInfoUpdate()
  {
  }

  protected virtual void AllItemIconUpdate()
  {
  }

  public virtual void IbtnBack()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<PopupManager>.GetInstance().closeAll(false);
    if (Singleton<NGGameDataManager>.GetInstance().IsColosseum)
    {
      Singleton<NGSceneManager>.GetInstance().destroyCurrentScene();
      Singleton<CommonRoot>.GetInstance().SetFooterEnable(true);
    }
    this.backScene();
  }

  public void resetRevisions()
  {
    this.revisionItemList_ = new long?();
    this.revisionMaterialList_ = new long?();
  }

  public virtual IEnumerator Init()
  {
    Bugu005ItemListMenuBase menu = this;
    Singleton<CommonRoot>.GetInstance().isTouchBlock = true;
    menu.InitializeEnd = false;
    IEnumerator e = menu.LoadItemIconPrefab();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if ((UnityEngine.Object) menu.SortPopupPrefab == (UnityEngine.Object) null)
    {
      Future<GameObject> sortPopupPrefabF = menu.GetSortAndFilterPopupGameObject();
      if (sortPopupPrefabF != null)
      {
        e = sortPopupPrefabF.Wait();
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        menu.SortPopupPrefab = sortPopupPrefabF.Result;
      }
      sortPopupPrefabF = (Future<GameObject>) null;
    }
    menu.SortPopupPrefab.GetComponent<ItemSortAndFilter>().Initialize(menu, false);
    List<PlayerItem> itemList = menu.GetItemList();
    List<PlayerMaterialGear> materialList = menu.GetMaterialList();
    int itemListCnt = menu.GetItemListCnt(itemList, materialList);
    int itemListFavoriteCnt = menu.GetItemListFavoriteCnt(itemList);
    long revisionItemList = menu.GetRevisionItemList();
    long revisionMaterialList = menu.GetRevisionMaterialList();
    int itemListEquipCount = menu.GetItemListEquipCount(itemList);
    if (menu.itemCount != itemListCnt || !menu.revisionItemList_.HasValue || (menu.revisionItemList_.Value != revisionItemList || !menu.revisionMaterialList_.HasValue) || (menu.revisionMaterialList_.Value != revisionMaterialList || menu.itemFavoriteCount != itemListFavoriteCnt && !menu.Filter[26]) || menu.itemEquipCount != itemListEquipCount)
    {
      menu.itemCount = itemListCnt;
      menu.itemFavoriteCount = itemListFavoriteCnt;
      menu.revisionItemList_ = new long?(revisionItemList);
      menu.revisionMaterialList_ = new long?(revisionMaterialList);
      menu.InventoryItems.Clear();
      menu.CreateInvetoryItem(itemList, materialList);
      e = menu.InitExtension();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      menu.CreatePlayerItems();
      menu.BottomInfoUpdate();
    }
    else
      menu.UpdateInventoryItemList();
    menu.InitializeEnd = true;
    Singleton<CommonRoot>.GetInstance().isTouchBlock = false;
    if ((UnityEngine.Object) menu.dir_noList != (UnityEngine.Object) null)
      menu.dir_noList.SetActive(menu.DisplayItems.Count <= 0);
  }

  public virtual void onEndScene()
  {
    Singleton<PopupManager>.GetInstance().closeAll(false);
  }

  protected IEnumerator LoadItemIconPrefab()
  {
    if ((UnityEngine.Object) this.ItemIconPrefab == (UnityEngine.Object) null)
    {
      Future<GameObject> prefabF = Res.Prefabs.ItemIcon.prefab.Load<GameObject>();
      IEnumerator e = prefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.ItemIconPrefab = prefabF.Result;
      prefabF = (Future<GameObject>) null;
    }
  }

  protected IEnumerator LoadSpriteCache()
  {
    if (this.InventoryItems.Count > this.iconMaxValue)
    {
      for (int i = this.iconMaxValue; i < this.InventoryItems.Count; ++i)
      {
        IEnumerator e = ItemIcon.LoadSprite(this.InventoryItems[i].Item);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
      }
    }
  }

  protected int GetItemListCnt(List<PlayerItem> itemList, List<PlayerMaterialGear> materialItemList)
  {
    int tmpItmCnt = 0;
    itemList?.ForEach((System.Action<PlayerItem>) (x =>
    {
      if (x.isWeapon())
        ++tmpItmCnt;
      else
        tmpItmCnt += x.quantity;
    }));
    materialItemList?.ForEach((System.Action<PlayerMaterialGear>) (x => tmpItmCnt += x.quantity));
    return tmpItmCnt;
  }

  protected int GetItemListFavoriteCnt(List<PlayerItem> itemList)
  {
    return itemList != null ? itemList.Count<PlayerItem>((Func<PlayerItem, bool>) (x => x.favorite)) : 0;
  }

  protected int GetItemListEquipCount(List<PlayerItem> itemList)
  {
    return itemList != null ? itemList.Count<PlayerItem>((Func<PlayerItem, bool>) (x => x.ForBattle)) : 0;
  }

  protected void CreateInvetoryItem(
    List<PlayerItem> itemList,
    List<PlayerMaterialGear> materialItemList)
  {
    if (itemList != null)
    {
      foreach (PlayerItem playerItem in itemList)
        this.InventoryItems.Add(new InventoryItem(playerItem));
    }
    if (materialItemList == null)
      return;
    foreach (PlayerMaterialGear materialItem in materialItemList)
    {
      PlayerMaterialGear item = materialItem;
      this.InventoryItems.Add(new InventoryItem(item, this.InventoryItems.Count<InventoryItem>((Func<InventoryItem, bool>) (x => x.Item.itemID == item.id))));
    }
  }

  protected void DisplayIconAndBottomInfoUpdate()
  {
    this.AllItemIconUpdate();
    this.BottomInfoUpdate();
  }

  protected void CreatePlayerItems()
  {
    this.scroll.Clear();
    this.AllItemIcon.Clear();
    for (int index = 0; index < Mathf.Min(ItemIcon.MaxValue, this.InventoryItems.Count); ++index)
      this.AllItemIcon.Add(UnityEngine.Object.Instantiate<GameObject>(this.ItemIconPrefab).GetComponent<ItemIcon>());
    this.Sort(this.SortCategory, this.OrderBuySort, this.isEquipFirst);
    this.scroolStartY = this.scroll.scrollView.transform.localPosition.y;
    this.StartCoroutine(this.LoadSpriteCache());
  }

  protected override void Update()
  {
    base.Update();
    this.ScrollUpdate();
  }

  public override void onBackButton()
  {
    this.IbtnBack();
  }

  protected virtual void ChangeDetailScene(GameCore.ItemInfo item)
  {
    if (item == null)
      return;
    if (item.isReisou)
      this.OpenReisouDetailPopup(item);
    else if (item.isWeapon)
      Unit00443Scene.changeScene(true, item);
    else if (item.isWeaponMaterial)
      Guide01142Scene.changeScene(true, item);
    else
      Bugu00561Scene.changeScene(true, item);
  }

  protected void OpenReisouDetailPopup(GameCore.ItemInfo item)
  {
    if (this.IsPushAndSet())
      return;
    this.StartCoroutine(this.OpenReisouDetailPopupAsync(item));
  }

  protected IEnumerator OpenReisouDetailPopupAsync(GameCore.ItemInfo item)
  {
    Bugu005ItemListMenuBase itemListMenuBase = this;
    if (item != null)
    {
      IEnumerator e;
      if ((UnityEngine.Object) itemListMenuBase.reisouPopupPrefab == (UnityEngine.Object) null)
      {
        Future<GameObject> popupPrefabF = new ResourceObject("Prefabs/UnitGUIs/PopupReisouSkillDetails").Load<GameObject>();
        e = popupPrefabF.Wait();
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        itemListMenuBase.reisouPopupPrefab = popupPrefabF.Result;
        popupPrefabF = (Future<GameObject>) null;
      }
      GameObject popup = itemListMenuBase.reisouPopupPrefab.Clone((Transform) null);
      PopupReisouDetails script = popup.GetComponent<PopupReisouDetails>();
      popup.SetActive(false);
      e = script.Init(item, (PlayerItem) null, true);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      popup.SetActive(true);
      Singleton<PopupManager>.GetInstance().open(popup, false, false, true, true, false, false, "SE_1006");
      yield return (object) null;
      script.scrollResetPosition();
      itemListMenuBase.IsPushOff();
    }
  }

  public virtual void IbtnSort()
  {
    if (this.IsPush)
      return;
    this.ShowSortAndFilterPopup();
  }

  private void ShowSortAndFilterPopup()
  {
    if (!Singleton<PopupManager>.GetInstance().isOpen)
    {
      if (!((UnityEngine.Object) this.SortPopupPrefab != (UnityEngine.Object) null))
        return;
      GameObject prefab = this.SortPopupPrefab.Clone((Transform) null);
      ItemSortAndFilter sortAndFilter = prefab.GetComponent<ItemSortAndFilter>();
      sortAndFilter.Initialize(this, true);
      sortAndFilter.SetItemNum(this.InventoryItems.FilterBy(this.filter).ToList<InventoryItem>(), this.InventoryItems);
      sortAndFilter.SortFilterItemNum = (System.Action) (() => sortAndFilter.SetItemNum(this.InventoryItems.FilterBy(this.filter).ToList<InventoryItem>(), this.InventoryItems));
      Singleton<PopupManager>.GetInstance().open(prefab, false, false, true, true, false, false, "SE_1006");
    }
    else
      this.IsPush = false;
  }

  public virtual void Sort(
    ItemSortAndFilter.SORT_TYPES type,
    SortAndFilter.SORT_TYPE_ORDER_BUY order,
    bool isEquipFirst)
  {
    this.CurrentSortType = type;
    if ((UnityEngine.Object) this.SortSprite != (UnityEngine.Object) null)
      this.SortSprite = ItemSortAndFilter.SortSpriteLabel(type, this.SortSprite);
    List<InventoryItem> self = new List<InventoryItem>();
    for (int index = this.InventoryItems.Count - 1; index >= 0; --index)
    {
      if (this.baseInfo != null && !this.baseInfo.playerItem.isLimitMax() && (this.InventoryItems[index].Item.gear != null && GearGear.CanSpecialDrill(this.baseInfo.gear, this.InventoryItems[index].Item.gear)))
      {
        self.Add(this.InventoryItems[index]);
        this.InventoryItems.Remove(this.InventoryItems[index]);
      }
    }
    List<InventoryItem> list = self.FilterBy(this.filter).SortBy(type, order, isEquipFirst).ToList<InventoryItem>();
    this.DisplayItems = this.InventoryItems.FilterBy(this.filter).SortBy(type, order, isEquipFirst).ToList<InventoryItem>();
    for (int index = 0; index < list.Count; ++index)
    {
      this.DisplayItems.Insert(index, list[index]);
      this.InventoryItems.Insert(index, list[index]);
    }
    this.scroll.Reset();
    this.AllItemIcon.ForEach((System.Action<ItemIcon>) (x =>
    {
      x.transform.parent = this.transform;
      x.gameObject.SetActive(false);
    }));
    for (int index = 0; index < Mathf.Min(this.iconMaxValue, this.DisplayItems.Count); ++index)
    {
      this.scroll.Add(this.AllItemIcon[index].gameObject, this.iconWidth, this.iconHeight, false);
      this.AllItemIcon[index].gameObject.SetActive(true);
    }
    this.InventoryItems.ForEach((System.Action<InventoryItem>) (v => v.icon = (ItemIcon) null));
    this.StartCoroutine(this.CreateItemIconRange(Mathf.Min(this.iconMaxValue, this.DisplayItems.Count)));
    this.scroll.CreateScrollPoint(this.iconHeight, this.DisplayItems.Count);
    this.scroll.ResolvePosition();
    if (!((UnityEngine.Object) this.dir_noList != (UnityEngine.Object) null))
      return;
    this.dir_noList.SetActive(this.DisplayItems.Count <= 0);
  }

  private void ScrollIconUpdate(int inventoryItemsIndex, int count)
  {
    if (this.DisplayItems[inventoryItemsIndex].removeButton || ItemIcon.IsCache(this.DisplayItems[inventoryItemsIndex].Item))
      this.CreateItemIconCache(inventoryItemsIndex, count);
    else
      this.StartCoroutine(this.CreateItemIcon(inventoryItemsIndex, count));
  }

  private void ScrollUpdate()
  {
    if ((!this.InitializeEnd || this.DisplayItems.Count <= this.iconScreenValue) && !this.isUpdateIcon)
      return;
    int num1 = this.iconHeight * 2;
    float num2 = this.scroll.scrollView.transform.localPosition.y - this.scroolStartY;
    float num3 = (float) (Mathf.Max(0, this.DisplayItems.Count - this.iconScreenValue - 1) / this.iconColumnValue * this.iconHeight);
    float num4 = (float) (this.iconHeight * this.iconRowValue);
    if ((double) num2 < 0.0)
      num2 = 0.0f;
    if ((double) num2 > (double) num3)
      num2 = num3;
    bool flag;
    do
    {
      flag = false;
      int count = 0;
      foreach (GameObject gameObject in this.scroll)
      {
        GameObject item = gameObject;
        float num5 = item.transform.localPosition.y + num2;
        int? nullable = this.DisplayItems.FirstIndexOrNull<InventoryItem>((Func<InventoryItem, bool>) (v => (UnityEngine.Object) v.icon != (UnityEngine.Object) null && (UnityEngine.Object) v.icon.gameObject == (UnityEngine.Object) item));
        if ((double) num5 > (double) num1)
        {
          item.transform.localPosition = new Vector3(item.transform.localPosition.x, item.transform.localPosition.y - num4, 0.0f);
          if (nullable.HasValue && nullable.Value + this.iconMaxValue < (this.DisplayItems.Count + 4) / 5 * 5)
          {
            if (nullable.Value + this.iconMaxValue >= this.DisplayItems.Count)
              item.SetActive(false);
            else
              this.ScrollIconUpdate(nullable.Value + this.iconMaxValue, count);
            flag = true;
          }
        }
        else if ((double) num5 < -((double) num4 - (double) num1))
        {
          int num6 = this.iconMaxValue;
          if (!item.activeSelf)
          {
            item.SetActive(true);
            num6 = 0;
          }
          if (nullable.HasValue && nullable.Value - num6 >= 0)
          {
            item.transform.localPosition = new Vector3(item.transform.localPosition.x, item.transform.localPosition.y + num4, 0.0f);
            this.ScrollIconUpdate(nullable.Value - num6, count);
            flag = true;
          }
        }
        else if (this.isUpdateIcon)
          this.ScrollIconUpdate(nullable.Value, count);
        ++count;
      }
    }
    while (flag);
    if (!this.isUpdateIcon)
      return;
    this.isUpdateIcon = false;
  }

  private void ResetItemIcon(int allItemIdx)
  {
    this.AllItemIcon[allItemIdx].gameObject.SetActive(false);
  }

  protected IEnumerator CreateItemIconRange(int max)
  {
    Singleton<CommonRoot>.GetInstance().isTouchBlock = true;
    for (int index = 0; index < this.AllItemIcon.Count; ++index)
      this.AllItemIcon[index].gameObject.SetActive(false);
    for (int i = 0; i < max; ++i)
    {
      if (ItemIcon.IsCache(this.DisplayItems[i].Item))
      {
        this.CreateItemIconCache(i, i);
      }
      else
      {
        IEnumerator e = this.CreateItemIcon(i, i);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
      }
    }
    for (int index = 0; index < max; ++index)
      this.AllItemIcon[index].gameObject.SetActive(true);
    Singleton<CommonRoot>.GetInstance().isTouchBlock = false;
  }

  private IEnumerator CreateItemIcon(int inventoryItemIdx, int allItemIdx)
  {
    ItemIcon itemIcon = this.AllItemIcon[allItemIdx];
    this.DisplayItems.Where<InventoryItem>((Func<InventoryItem, bool>) (a => (UnityEngine.Object) a.icon == (UnityEngine.Object) itemIcon)).ForEach<InventoryItem>((System.Action<InventoryItem>) (b => b.icon = (ItemIcon) null));
    this.DisplayItems[inventoryItemIdx].icon = itemIcon;
    if (this.DisplayItems[inventoryItemIdx].removeButton)
    {
      itemIcon.InitByRemoveButton();
    }
    else
    {
      IEnumerator e = itemIcon.InitByItemInfo(this.DisplayItems[inventoryItemIdx].Item);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    this.CreateItemIconAdvencedSetting(inventoryItemIdx, allItemIdx);
    itemIcon.ShowBottomInfo(this.CurrentSortType);
  }

  private void CreateItemIconCache(int inventoryItemIdx, int allItemIdx)
  {
    ItemIcon itemIcon = this.AllItemIcon[allItemIdx];
    this.DisplayItems.Where<InventoryItem>((Func<InventoryItem, bool>) (a => (UnityEngine.Object) a.icon == (UnityEngine.Object) itemIcon)).ForEach<InventoryItem>((System.Action<InventoryItem>) (b => b.icon = (ItemIcon) null));
    this.DisplayItems[inventoryItemIdx].icon = itemIcon;
    if (this.DisplayItems[inventoryItemIdx].removeButton)
      itemIcon.InitByRemoveButton();
    else
      itemIcon.InitByItemInfoCache(this.DisplayItems[inventoryItemIdx].Item);
    this.CreateItemIconAdvencedSetting(inventoryItemIdx, allItemIdx);
    itemIcon.ShowBottomInfo(this.CurrentSortType);
  }

  private enum SortFilterPopupMode
  {
    None,
    Full,
    Material,
    Bugu,
    AlchemistMaterial,
    CompseMaterial,
  }
}
