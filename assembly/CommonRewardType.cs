﻿// Decompiled with JetBrains decompiler
// Type: CommonRewardType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class CommonRewardType
{
  public readonly int type_;
  public readonly int id_;
  public readonly int quantity_;
  public readonly bool is_new_;
  public readonly PlayerUnit unit;
  public readonly PlayerMaterialUnit materialUnit;
  public readonly PlayerItem gear;
  public readonly PlayerMaterialGear materialGear;
  public readonly GearGear gearGear;
  public readonly UnitUnit unitUnit;
  public readonly SupplySupply supplySupply;
  public readonly bool isUnit;
  public readonly bool isMaterialUnit;
  public readonly bool isGear;
  public readonly bool isMaterialGear;
  public readonly bool isSupply;
  public GameObject Icon;
  public UnitIcon UnitIconScript;
  public ItemIcon ItemIconScript;

  public CommonRewardType(int type, int id, int quantity, bool is_new = false, bool is_reserves = false)
  {
    this.type_ = type;
    this.id_ = id;
    this.quantity_ = quantity;
    this.is_new_ = is_new;
    this.isUnit = this.type_ == 1;
    this.isMaterialUnit = this.type_ == 24;
    this.isGear = this.type_ == 3;
    this.isMaterialGear = type == 26 || type == 35;
    this.isSupply = this.type_ == 2;
    if (this.isUnit)
    {
      this.unit = Array.Find<PlayerUnit>(!is_reserves ? SMManager.Get<PlayerUnit[]>() : GachaResultData.GetInstance().GetData().GetPlayerUnitReserves(), (Predicate<PlayerUnit>) (Unit => Unit.id == id));
      MasterData.UnitUnit.TryGetValue(id, out this.unitUnit);
    }
    else if (this.isMaterialUnit)
    {
      this.materialUnit = Array.Find<PlayerMaterialUnit>(SMManager.Get<PlayerMaterialUnit[]>(), (Predicate<PlayerMaterialUnit>) (Unit => Unit.id == id));
      MasterData.UnitUnit.TryGetValue(id, out this.unitUnit);
    }
    else if (this.isGear)
    {
      this.gear = Array.Find<PlayerItem>(SMManager.Get<PlayerItem[]>(), (Predicate<PlayerItem>) (Gear => Gear.id == id));
      MasterData.GearGear.TryGetValue(id, out this.gearGear);
    }
    else if (this.isMaterialGear)
    {
      this.materialGear = Array.Find<PlayerMaterialGear>(SMManager.Get<PlayerMaterialGear[]>(), (Predicate<PlayerMaterialGear>) (Gear => Gear.id == id));
      MasterData.GearGear.TryGetValue(id, out this.gearGear);
    }
    else
    {
      if (!this.isSupply)
        return;
      this.supplySupply = MasterData.SupplySupply[id];
    }
  }

  public void ThenUnit(System.Action<PlayerUnit> callback)
  {
    if (!this.isUnit)
      return;
    callback(this.unit);
  }

  public void ThenMaterialUnit(System.Action<PlayerMaterialUnit> callback)
  {
    if (!this.isMaterialUnit)
      return;
    callback(this.materialUnit);
  }

  public void ThenGear(System.Action<PlayerItem> callback)
  {
    if (!this.isGear)
      return;
    callback(this.gear);
  }

  public void ThenMaterialGear(System.Action<PlayerMaterialGear> callback)
  {
    if (!this.isMaterialGear)
      return;
    callback(this.materialGear);
  }

  public void UnitIcon(System.Action<UnitIcon> callback)
  {
    if (!((UnityEngine.Object) this.UnitIconScript != (UnityEngine.Object) null))
      return;
    callback(this.UnitIconScript);
  }

  public void ItemIcon(System.Action<ItemIcon> callback)
  {
    if (!((UnityEngine.Object) this.ItemIconScript != (UnityEngine.Object) null))
      return;
    callback(this.ItemIconScript);
  }

  public void ThenSupply(System.Action<SupplySupply> callback)
  {
    if (!this.isGear)
      return;
    callback(this.supplySupply);
  }

  public GameObject GetIcon()
  {
    return this.Icon;
  }

  public UnitIcon GetUnitIcon()
  {
    return this.UnitIconScript;
  }

  public ItemIcon GetItemIcon()
  {
    return this.ItemIconScript;
  }

  public IEnumerator CreateIcon(Transform parent)
  {
    Future<GameObject> PrefabF;
    IEnumerator e;
    if (this.isUnit || this.isMaterialUnit)
    {
      PrefabF = Res.Prefabs.UnitIcon.normal.Load<GameObject>();
      e = PrefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.Icon = PrefabF.Result.Clone(parent);
      this.UnitIconScript = this.Icon.GetComponent<UnitIcon>();
      this.UnitIconScript.NewUnit = this.is_new_;
      PrefabF = (Future<GameObject>) null;
    }
    if (this.isGear || this.isMaterialGear)
    {
      PrefabF = Res.Prefabs.ItemIcon.prefab.Load<GameObject>();
      e = PrefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.Icon = PrefabF.Result.Clone(parent);
      this.ItemIconScript = this.Icon.GetComponent<ItemIcon>();
      this.ItemIconScript.NewItem = this.is_new_;
      PrefabF = (Future<GameObject>) null;
    }
  }

  public static string GetRewardName(MasterDataTable.CommonRewardType type, int id, int quantity, bool isGuild = false)
  {
    string empty1 = string.Empty;
    string str;
    switch (type)
    {
      case MasterDataTable.CommonRewardType.unit:
      case MasterDataTable.CommonRewardType.material_unit:
        str = Consts.Format(Consts.GetInstance().MYPAGE_0017_UNIT, (IDictionary) new Hashtable()
        {
          {
            (object) "Name",
            (object) MasterData.UnitUnit[id].name
          },
          {
            (object) "Count",
            (object) quantity
          }
        });
        break;
      case MasterDataTable.CommonRewardType.supply:
        str = Consts.Format(Consts.GetInstance().MYPAGE_0017_SUPPLY, (IDictionary) new Hashtable()
        {
          {
            (object) "Name",
            (object) MasterData.SupplySupply[id].name
          },
          {
            (object) "Count",
            (object) quantity
          }
        });
        break;
      case MasterDataTable.CommonRewardType.gear:
      case MasterDataTable.CommonRewardType.material_gear:
        str = Consts.Format(Consts.GetInstance().MYPAGE_0017_GEAR, (IDictionary) new Hashtable()
        {
          {
            (object) "Name",
            (object) MasterData.GearGear[id].name
          },
          {
            (object) "Count",
            (object) quantity
          }
        });
        break;
      case MasterDataTable.CommonRewardType.money:
        str = Consts.Format(Consts.GetInstance().MYPAGE_0017_ZENY, (IDictionary) new Hashtable()
        {
          {
            (object) "Count",
            (object) quantity
          }
        });
        break;
      case MasterDataTable.CommonRewardType.player_exp:
        str = Consts.Format(Consts.GetInstance().MYPAGE_0017_EXP, (IDictionary) new Hashtable()
        {
          {
            (object) "Count",
            (object) quantity
          }
        });
        break;
      case MasterDataTable.CommonRewardType.unit_exp:
        str = Consts.Format(Consts.GetInstance().MYPAGE_0017_UNITEXP, (IDictionary) new Hashtable()
        {
          {
            (object) "Count",
            (object) quantity
          }
        });
        break;
      case MasterDataTable.CommonRewardType.coin:
        str = Consts.Format(Consts.GetInstance().MYPAGE_0017_STONE, (IDictionary) new Hashtable()
        {
          {
            (object) "Count",
            (object) quantity
          }
        });
        break;
      case MasterDataTable.CommonRewardType.recover:
        str = Consts.Format(Consts.GetInstance().MYPAGE_0017_AP_RECOVER, (IDictionary) new Hashtable()
        {
          {
            (object) "Count",
            (object) quantity
          }
        });
        break;
      case MasterDataTable.CommonRewardType.max_unit:
        str = Consts.Format(Consts.GetInstance().MYPAGE_0017_MAX_UNIT, (IDictionary) new Hashtable()
        {
          {
            (object) "Count",
            (object) quantity
          }
        });
        break;
      case MasterDataTable.CommonRewardType.max_item:
        str = Consts.Format(Consts.GetInstance().MYPAGE_0017_MAX_ITEM, (IDictionary) new Hashtable()
        {
          {
            (object) "Count",
            (object) quantity
          }
        });
        break;
      case MasterDataTable.CommonRewardType.medal:
        str = Consts.Format(Consts.GetInstance().MYPAGE_0017_MEDAL, (IDictionary) new Hashtable()
        {
          {
            (object) "Count",
            (object) quantity
          }
        });
        break;
      case MasterDataTable.CommonRewardType.friend_point:
        str = Consts.Format(Consts.GetInstance().MYPAGE_0017_POINT, (IDictionary) new Hashtable()
        {
          {
            (object) "Count",
            (object) quantity
          }
        });
        break;
      case MasterDataTable.CommonRewardType.emblem:
        if (isGuild)
        {
          str = Consts.Format(Consts.GetInstance().MYPAGE_0017_EMBLEM, (IDictionary) new Hashtable()
          {
            {
              (object) "Name",
              (object) MasterData.GuildEmblemUnit[id].name
            }
          });
          break;
        }
        str = Consts.Format(Consts.GetInstance().MYPAGE_0017_EMBLEM, (IDictionary) new Hashtable()
        {
          {
            (object) "Name",
            (object) MasterData.EmblemEmblem[id].name
          }
        });
        break;
      case MasterDataTable.CommonRewardType.battle_medal:
        str = Consts.Format(Consts.GetInstance().MYPAGE_0017_BATTLE_MEDAL, (IDictionary) new Hashtable()
        {
          {
            (object) "Count",
            (object) quantity
          }
        });
        break;
      case MasterDataTable.CommonRewardType.cp_recover:
        str = Consts.Format(Consts.GetInstance().MYPAGE_0017_CP_RECOVER, (IDictionary) new Hashtable()
        {
          {
            (object) "Count",
            (object) quantity
          }
        });
        break;
      case MasterDataTable.CommonRewardType.quest_key:
        if (!MasterData.QuestkeyQuestkey.ContainsKey(id))
        {
          str = Consts.Format(Consts.GetInstance().MYPAGE_0017_KEY_DEFAULT, (IDictionary) new Hashtable()
          {
            {
              (object) "Count",
              (object) quantity
            }
          });
          break;
        }
        str = Consts.Format(Consts.GetInstance().MYPAGE_0017_KEY, (IDictionary) new Hashtable()
        {
          {
            (object) "Name",
            (object) MasterData.QuestkeyQuestkey[id].name
          },
          {
            (object) "Count",
            (object) quantity
          }
        });
        break;
      case MasterDataTable.CommonRewardType.gacha_ticket:
        string empty2 = string.Empty;
        str = Consts.Format(Consts.GetInstance().MYPAGE_0017_GACHATICKET_NAME, (IDictionary) new Hashtable()
        {
          {
            (object) "Name",
            (object) (!MasterData.GachaTicket.ContainsKey(id) ? Consts.GetInstance().MYPAGE_0017_GACHATICKET_COMMONNAME : MasterData.GachaTicket[id].short_name)
          },
          {
            (object) "Count",
            (object) quantity
          }
        });
        break;
      case MasterDataTable.CommonRewardType.season_ticket:
        str = Consts.Format(Consts.GetInstance().MYPAGE_0017_SEASONTICKET, (IDictionary) new Hashtable()
        {
          {
            (object) "Count",
            (object) quantity
          }
        });
        break;
      case MasterDataTable.CommonRewardType.mp_recover:
        str = Consts.Format(Consts.GetInstance().MYPAGE_0017_MP_RECOVER, (IDictionary) new Hashtable()
        {
          {
            (object) "Count",
            (object) quantity
          }
        });
        break;
      case MasterDataTable.CommonRewardType.unit_ticket:
        string empty3 = string.Empty;
        str = Consts.Format(Consts.GetInstance().MYPAGE_0017_UNITTICKET_NAME, (IDictionary) new Hashtable()
        {
          {
            (object) "Name",
            (object) (!MasterData.SelectTicket.ContainsKey(id) ? Consts.GetInstance().MYPAGE_0017_UNITTICKET_COMMONNAME : MasterData.SelectTicket[id].short_name)
          },
          {
            (object) "Count",
            (object) quantity
          }
        });
        break;
      case MasterDataTable.CommonRewardType.awake_skill:
        str = Consts.Format(Consts.GetInstance().MYPAGE_0017_TRUST_SKILL, (IDictionary) new Hashtable()
        {
          {
            (object) "Name",
            (object) MasterData.BattleskillSkill[id].name
          },
          {
            (object) "Count",
            (object) quantity
          }
        });
        break;
      case MasterDataTable.CommonRewardType.dp_recover:
        str = Consts.Format(Consts.GetInstance().MYPAGE_0017_DP_RECOVER, (IDictionary) new Hashtable()
        {
          {
            (object) "Count",
            (object) quantity
          }
        });
        break;
      case MasterDataTable.CommonRewardType.guild_medal:
        str = Consts.Format(Consts.GetInstance().MYPAGE_0017_GUILD_MEDAL, (IDictionary) new Hashtable()
        {
          {
            (object) "Count",
            (object) quantity
          }
        });
        break;
      case MasterDataTable.CommonRewardType.guild_town:
        str = Consts.Format(Consts.GetInstance().MYPAGE_0017_GUILD_TOWN, (IDictionary) new Hashtable()
        {
          {
            (object) "Name",
            (object) MasterData.MapTown[id].name
          }
        });
        break;
      case MasterDataTable.CommonRewardType.guild_facility:
        str = Consts.Format(Consts.GetInstance().MYPAGE_0017_GUILD_FACILITY, (IDictionary) new Hashtable()
        {
          {
            (object) "Name",
            (object) CommonRewardType.GetFacilityRewardName(id)
          },
          {
            (object) "Count",
            (object) quantity
          }
        });
        break;
      case MasterDataTable.CommonRewardType.reincarnation_type_ticket:
        string empty4 = string.Empty;
        str = Consts.Format(Consts.GetInstance().MYPAGE_0017_UNITTYPETICKET_NAME, (IDictionary) new Hashtable()
        {
          {
            (object) "Name",
            (object) (!MasterData.UnitTypeTicket.ContainsKey(id) ? Consts.GetInstance().MYPAGE_0017_UNITTYPETICKET_COMMONNAME : MasterData.UnitTypeTicket[id].name)
          },
          {
            (object) "Count",
            (object) quantity
          }
        });
        break;
      case MasterDataTable.CommonRewardType.gear_body:
        str = Consts.Format(Consts.GetInstance().MYPAGE_0017_MATERIAL_GEAR, (IDictionary) new Hashtable()
        {
          {
            (object) "Name",
            (object) MasterData.GearGear[id].name
          },
          {
            (object) "Count",
            (object) quantity
          }
        });
        break;
      case MasterDataTable.CommonRewardType.raid_medal:
        str = Consts.Format(Consts.GetInstance().MYPAGE_0017_RAID_MEDAL, (IDictionary) new Hashtable()
        {
          {
            (object) "Count",
            (object) quantity
          }
        });
        break;
      default:
        str = Consts.Format(Consts.GetInstance().MYPAGE_0017_OTHER, (IDictionary) new Hashtable()
        {
          {
            (object) "Count",
            (object) quantity
          }
        });
        break;
    }
    return str;
  }

  public static string GetRewardName(MasterDataTable.CommonRewardType type, int id)
  {
    string empty1 = string.Empty;
    string str;
    switch (type)
    {
      case MasterDataTable.CommonRewardType.unit:
      case MasterDataTable.CommonRewardType.material_unit:
        str = MasterData.UnitUnit[id].name;
        break;
      case MasterDataTable.CommonRewardType.supply:
        str = MasterData.SupplySupply[id].name;
        break;
      case MasterDataTable.CommonRewardType.gear:
      case MasterDataTable.CommonRewardType.material_gear:
      case MasterDataTable.CommonRewardType.gear_body:
        str = MasterData.GearGear[id].name;
        break;
      case MasterDataTable.CommonRewardType.money:
        str = Consts.GetInstance().UNIQUE_ICON_ZENY;
        break;
      case MasterDataTable.CommonRewardType.player_exp:
        str = Consts.GetInstance().UNIQUE_ICON_EXP;
        break;
      case MasterDataTable.CommonRewardType.unit_exp:
        str = Consts.GetInstance().UNIQUE_ICON_UNITEXT_LONG;
        break;
      case MasterDataTable.CommonRewardType.coin:
        str = Consts.GetInstance().UNIQUE_ICON_KISEKI;
        break;
      case MasterDataTable.CommonRewardType.recover:
        str = Consts.GetInstance().UNIQUE_ICON_APRECOVER;
        break;
      case MasterDataTable.CommonRewardType.max_unit:
        str = Consts.GetInstance().UNIQUE_ICON_MAXUNIT;
        break;
      case MasterDataTable.CommonRewardType.max_item:
        str = Consts.GetInstance().UNIQUE_ICON_MAXITEM;
        break;
      case MasterDataTable.CommonRewardType.medal:
        str = Consts.GetInstance().UNIQUE_ICON_MEDAL;
        break;
      case MasterDataTable.CommonRewardType.friend_point:
        str = Consts.GetInstance().UNIQUE_ICON_POINT;
        break;
      case MasterDataTable.CommonRewardType.emblem:
        str = MasterData.EmblemEmblem[id].name;
        break;
      case MasterDataTable.CommonRewardType.battle_medal:
        str = Consts.GetInstance().UNIQUE_ICON_BATTLE_MEDAL;
        break;
      case MasterDataTable.CommonRewardType.cp_recover:
        str = Consts.GetInstance().UNIQUE_ICON_CPRECOVER;
        break;
      case MasterDataTable.CommonRewardType.quest_key:
        str = MasterData.QuestkeyQuestkey[id].name;
        break;
      case MasterDataTable.CommonRewardType.gacha_ticket:
        string empty2 = string.Empty;
        str = !MasterData.GachaTicket.ContainsKey(id) ? Consts.GetInstance().MYPAGE_0017_GACHATICKET_COMMONNAME : MasterData.GachaTicket[id].short_name;
        break;
      case MasterDataTable.CommonRewardType.season_ticket:
        str = Consts.GetInstance().UNIQUE_ICON_SEASONTICKET;
        break;
      case MasterDataTable.CommonRewardType.mp_recover:
        str = Consts.GetInstance().UNIQUE_ICON_MPRECOVER;
        break;
      case MasterDataTable.CommonRewardType.unit_ticket:
        string empty3 = string.Empty;
        str = !MasterData.SelectTicket.ContainsKey(id) ? Consts.GetInstance().MYPAGE_0017_UNITTICKET_COMMONNAME : MasterData.SelectTicket[id].short_name;
        break;
      case MasterDataTable.CommonRewardType.awake_skill:
        str = MasterData.BattleskillSkill[id].name;
        break;
      case MasterDataTable.CommonRewardType.dp_recover:
        str = Consts.GetInstance().UNIQUE_ICON_DPRECOVER;
        break;
      case MasterDataTable.CommonRewardType.reincarnation_type_ticket:
        string empty4 = string.Empty;
        str = !MasterData.UnitTypeTicket.ContainsKey(id) ? Consts.GetInstance().MYPAGE_0017_UNITTYPETICKET_COMMONNAME : MasterData.UnitTypeTicket[id].name;
        break;
      case MasterDataTable.CommonRewardType.raid_medal:
        str = Consts.GetInstance().UNIQUE_ICON_RAID_MEDAL;
        break;
      default:
        str = Consts.GetInstance().UNIQUE_ICON_OTHER;
        break;
    }
    return str;
  }

  public static string GetRewardQuantity(MasterDataTable.CommonRewardType type, int id, int quantity)
  {
    string empty = string.Empty;
    string str;
    switch (type)
    {
      case MasterDataTable.CommonRewardType.unit:
      case MasterDataTable.CommonRewardType.material_unit:
        str = Consts.Format(Consts.GetInstance().SPECIAL_SHOP_UNIT_NUM, (IDictionary) new Hashtable()
        {
          {
            (object) "Count",
            (object) quantity
          }
        });
        break;
      case MasterDataTable.CommonRewardType.supply:
        str = Consts.Format(Consts.GetInstance().SPECIAL_SHOP_SUPPLY_NUM, (IDictionary) new Hashtable()
        {
          {
            (object) "Count",
            (object) quantity
          }
        });
        break;
      case MasterDataTable.CommonRewardType.gear:
      case MasterDataTable.CommonRewardType.material_gear:
      case MasterDataTable.CommonRewardType.gear_body:
        str = Consts.Format(Consts.GetInstance().SPECIAL_SHOP_GEAR_NUM, (IDictionary) new Hashtable()
        {
          {
            (object) "Count",
            (object) quantity
          }
        });
        break;
      case MasterDataTable.CommonRewardType.money:
        str = Consts.Format(Consts.GetInstance().SPECIAL_SHOP_ZENY_NUM, (IDictionary) new Hashtable()
        {
          {
            (object) "Count",
            (object) quantity
          }
        });
        break;
      case MasterDataTable.CommonRewardType.player_exp:
        str = Consts.Format(Consts.GetInstance().SPECIAL_SHOP_EXP_NUM, (IDictionary) new Hashtable()
        {
          {
            (object) "Count",
            (object) quantity
          }
        });
        break;
      case MasterDataTable.CommonRewardType.coin:
        str = Consts.Format(Consts.GetInstance().SPECIAL_SHOP_STONE_NUM, (IDictionary) new Hashtable()
        {
          {
            (object) "Count",
            (object) quantity
          }
        });
        break;
      case MasterDataTable.CommonRewardType.recover:
        str = Consts.Format(Consts.GetInstance().SPECIAL_SHOP_AP_NUM, (IDictionary) new Hashtable()
        {
          {
            (object) "Count",
            (object) quantity
          }
        });
        break;
      case MasterDataTable.CommonRewardType.max_unit:
        str = Consts.Format(Consts.GetInstance().SPECIAL_SHOP_OTHER_NUM, (IDictionary) new Hashtable()
        {
          {
            (object) "Count",
            (object) quantity
          }
        });
        break;
      case MasterDataTable.CommonRewardType.max_item:
        str = Consts.Format(Consts.GetInstance().SPECIAL_SHOP_OTHER_NUM, (IDictionary) new Hashtable()
        {
          {
            (object) "Count",
            (object) quantity
          }
        });
        break;
      case MasterDataTable.CommonRewardType.medal:
        str = Consts.Format(Consts.GetInstance().SPECIAL_SHOP_MEDAL_NUM, (IDictionary) new Hashtable()
        {
          {
            (object) "Count",
            (object) quantity
          }
        });
        break;
      case MasterDataTable.CommonRewardType.friend_point:
        str = Consts.Format(Consts.GetInstance().SPECIAL_SHOP_POINT_NUM, (IDictionary) new Hashtable()
        {
          {
            (object) "Count",
            (object) quantity
          }
        });
        break;
      case MasterDataTable.CommonRewardType.emblem:
        str = Consts.GetInstance().SPECIAL_SHOP_EMBLEM_NUM;
        break;
      case MasterDataTable.CommonRewardType.battle_medal:
        str = Consts.Format(Consts.GetInstance().SPECIAL_SHOP_BATTLE_MEDAL_NUM, (IDictionary) new Hashtable()
        {
          {
            (object) "Count",
            (object) quantity
          }
        });
        break;
      case MasterDataTable.CommonRewardType.cp_recover:
        str = Consts.Format(Consts.GetInstance().SPECIAL_SHOP_CP_NUM, (IDictionary) new Hashtable()
        {
          {
            (object) "Count",
            (object) quantity
          }
        });
        break;
      case MasterDataTable.CommonRewardType.quest_key:
        str = Consts.Format(Consts.GetInstance().SPECIAL_SHOP_KEY_NUM, (IDictionary) new Hashtable()
        {
          {
            (object) "Count",
            (object) quantity
          }
        });
        break;
      case MasterDataTable.CommonRewardType.gacha_ticket:
        str = Consts.Format(Consts.GetInstance().SPECIAL_SHOP_GACHATICKET_NUM, (IDictionary) new Hashtable()
        {
          {
            (object) "Count",
            (object) quantity
          }
        });
        break;
      case MasterDataTable.CommonRewardType.season_ticket:
        str = Consts.Format(Consts.GetInstance().SPECIAL_SHOP_SEASONTICKETNUM, (IDictionary) new Hashtable()
        {
          {
            (object) "Count",
            (object) quantity
          }
        });
        break;
      case MasterDataTable.CommonRewardType.mp_recover:
        str = Consts.Format(Consts.GetInstance().SPECIAL_SHOP_MP_NUM, (IDictionary) new Hashtable()
        {
          {
            (object) "Count",
            (object) quantity
          }
        });
        break;
      case MasterDataTable.CommonRewardType.unit_ticket:
        str = Consts.Format(Consts.GetInstance().SPECIAL_SHOP_UNITTICKET_NUM, (IDictionary) new Hashtable()
        {
          {
            (object) "Count",
            (object) quantity
          }
        });
        break;
      case MasterDataTable.CommonRewardType.awake_skill:
        str = Consts.Format(Consts.GetInstance().SPECIAL_SHOP_TRUST_SKILL_NUM, (IDictionary) new Hashtable()
        {
          {
            (object) "Count",
            (object) quantity
          }
        });
        break;
      case MasterDataTable.CommonRewardType.dp_recover:
        str = Consts.Format(Consts.GetInstance().SPECIAL_SHOP_DP_NUM, (IDictionary) new Hashtable()
        {
          {
            (object) "Count",
            (object) quantity
          }
        });
        break;
      case MasterDataTable.CommonRewardType.reincarnation_type_ticket:
        str = Consts.Format(Consts.GetInstance().SPECIAL_SHOP_UNITTYPETICKET_NUM, (IDictionary) new Hashtable()
        {
          {
            (object) "Count",
            (object) quantity
          }
        });
        break;
      case MasterDataTable.CommonRewardType.raid_medal:
        str = Consts.Format(Consts.GetInstance().SPECIAL_SHOP_MEDAL_NUM, (IDictionary) new Hashtable()
        {
          {
            (object) "Count",
            (object) quantity
          }
        });
        break;
      default:
        str = Consts.Format(Consts.GetInstance().SPECIAL_SHOP_OTHER_NUM, (IDictionary) new Hashtable()
        {
          {
            (object) "Count",
            (object) quantity
          }
        });
        break;
    }
    return str;
  }

  private static string GetFacilityRewardName(int id)
  {
    MapFacility mapFacility;
    if (MasterData.MapFacility.TryGetValue(id, out mapFacility))
      return mapFacility.name;
    FacilityLevel facilityLevel = ((IEnumerable<FacilityLevel>) MasterData.FacilityLevelList).FirstOrDefault<FacilityLevel>((Func<FacilityLevel, bool>) (x => x.unit_UnitUnit == id));
    return facilityLevel != null ? facilityLevel.facility.name : string.Empty;
  }
}
