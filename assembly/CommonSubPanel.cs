﻿// Decompiled with JetBrains decompiler
// Type: CommonSubPanel
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;
using Util;

public class CommonSubPanel : MonoBehaviour
{
  [SerializeField]
  private GameObject subPanel;

  private void OnEnable()
  {
    if (!IOSUtil.IsDeviceGenerationiPhoneX || (Object) this.subPanel == (Object) null)
      return;
    this.subPanel.SetActive(true);
  }

  private void OnDisable()
  {
    if (!IOSUtil.IsDeviceGenerationiPhoneX || (Object) this.subPanel == (Object) null)
      return;
    this.subPanel.SetActive(false);
  }
}
