﻿// Decompiled with JetBrains decompiler
// Type: ShopGiftMonthReachingItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;
using System.Collections;
using UnityEngine;

public class ShopGiftMonthReachingItem : MonoBehaviour
{
  [SerializeField]
  private Transform IconParent;
  [SerializeField]
  private UILabel description;

  public IEnumerator Init(GameObject withLoupeIcon, MonthlyPackExtraReward reward)
  {
    IEnumerator e = withLoupeIcon.Clone(this.IconParent).GetComponent<ItemIconDetail>().Init((MasterDataTable.CommonRewardType) reward.reward_type_id, reward.reward_id, reward.reward_quantity, true, (TransformSizeInfo) null, (TransformSizeInfo) null, (SpriteTransformSizeInfo) null, (TransformSizeInfo) null);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.description.text = string.Format("{0}日目に確定", (object) reward.days);
  }
}
