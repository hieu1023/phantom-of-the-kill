﻿// Decompiled with JetBrains decompiler
// Type: Quest00214PopupReleaseCondition
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Quest00214PopupReleaseCondition : BackButtonMenuBase
{
  [SerializeField]
  private UILabel TxtDescription;

  public void Init(string message)
  {
    this.TxtDescription.SetText(message);
  }

  public void IbtnOk()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<PopupManager>.GetInstance().onDismiss();
  }

  public override void onBackButton()
  {
    this.IbtnOk();
  }
}
