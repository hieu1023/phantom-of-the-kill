﻿// Decompiled with JetBrains decompiler
// Type: BattleTerrainSkillEffect
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleTerrainSkillEffect : MonoBehaviour
{
  [SerializeField]
  private float mWaitLoopEffSec = 1f;
  private HashSet<int> mTargetLandTags = new HashSet<int>();
  [SerializeField]
  private GameObject mActivateEffect;
  [SerializeField]
  private GameObject mLoopEffect;
  private bool isActive;
  private Coroutine mActiveLoopHandle;

  private void Awake()
  {
    this.mActivateEffect.SetActive(false);
    this.mLoopEffect.SetActive(false);
  }

  public void SetCompatibleSkillByEffective(BL.Unit unit)
  {
    this.mTargetLandTags.Clear();
    this.AddCompatibleSkill(unit.skillEffects.Where(BattleskillSkillType.passive, false));
  }

  public void AddCompatibleSkill(IEnumerable<BattleskillSkill> skills)
  {
    foreach (BattleskillSkill skill in skills)
      this.AddCompatibleSkill(skill);
  }

  public void AddCompatibleSkill(BattleskillSkill skill)
  {
    foreach (BattleskillEffect effect in skill.Effects)
    {
      BattleFuncs.PackedSkillEffect packedSkillEffect = BattleFuncs.PackedSkillEffect.Create(effect);
      if (packedSkillEffect.HasKey(BattleskillEffectLogicArgumentEnum.land_tag1))
      {
        for (BattleskillEffectLogicArgumentEnum key = BattleskillEffectLogicArgumentEnum.land_tag1; key <= BattleskillEffectLogicArgumentEnum.land_tag10; ++key)
        {
          int num = packedSkillEffect.GetInt(key);
          if (num != 0)
            this.mTargetLandTags.Add(num);
          else
            break;
        }
      }
    }
  }

  public void UpdateState(BL.Panel panel)
  {
    if (this.isCompatiblePanel(panel))
    {
      if (this.isActive)
        return;
      this.isActive = true;
      this.mActivateEffect.SetActive(Singleton<NGBattleManager>.GetInstance().isBattleEnable);
      this.stopWaitForLoopEffect();
      this.mActiveLoopHandle = this.StartCoroutine(this.activeLoopEffect(this.mWaitLoopEffSec));
    }
    else
    {
      this.stopWaitForLoopEffect();
      this.isActive = false;
      this.mActivateEffect.SetActive(false);
      this.mLoopEffect.SetActive(false);
    }
  }

  private bool isCompatiblePanel(BL.Panel panel)
  {
    BattleLandform landform = panel.landform;
    return this.mTargetLandTags.Contains(landform.tag1) || this.mTargetLandTags.Contains(landform.tag2) || this.mTargetLandTags.Contains(landform.tag3);
  }

  private void stopWaitForLoopEffect()
  {
    if (this.mActiveLoopHandle == null)
      return;
    this.StopCoroutine(this.mActiveLoopHandle);
    this.mActiveLoopHandle = (Coroutine) null;
  }

  private IEnumerator activeLoopEffect(float waitSec)
  {
    yield return (object) new WaitForSeconds(waitSec);
    this.mActivateEffect.SetActive(false);
    this.mLoopEffect.SetActive(true);
    this.mActiveLoopHandle = (Coroutine) null;
  }
}
