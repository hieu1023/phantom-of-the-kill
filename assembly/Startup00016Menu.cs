﻿// Decompiled with JetBrains decompiler
// Type: Startup00016Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;
using UnityEngine;

public class Startup00016Menu : NGMenuBase
{
  [SerializeField]
  private UITextList textList;
  private string text;
  private bool flag;

  public void IbtnPopupClose()
  {
  }

  public void InitScene()
  {
    this.text = Consts.GetInstance().PRIVACY_POLICY;
    this.textList.Add(this.text);
    this.textList.scrollValue = 0.0f;
    this.flag = true;
  }

  public IEnumerator InitSceneAsync()
  {
    this.text = Consts.GetInstance().PRIVACY_POLICY;
    this.textList.Add(this.text);
    this.textList.scrollValue = 0.0f;
    this.flag = true;
    yield break;
  }

  private void Update()
  {
    if (!this.flag || (double) this.textList.scrollValue == 0.0)
      return;
    this.flag = false;
    this.textList.scrollValue = 0.0f;
  }
}
