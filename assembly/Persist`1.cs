﻿// Decompiled with JetBrains decompiler
// Type: Persist`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using GameCore.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using UniLinq;

public class Persist<T> where T : new()
{
  private static HashSet<char> permitChars = new HashSet<char>((IEnumerable<char>) "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz_-.");
  public string fileName;
  public object value;

  public Persist(string fileName)
  {
    this.fileName = fileName;
  }

  private static string Sanitize(string str)
  {
    return str.Select<char, char>((Func<char, char>) (c => !Persist<T>.permitChars.Contains(c) ? '_' : c)).ToStringForChars();
  }

  public string FilePath
  {
    get
    {
      return Path.Combine(PersistentPath.Value, Persist<T>.Sanitize(this.fileName));
    }
  }

  public string FileFallbackPath
  {
    get
    {
      return Path.Combine(PersistentPath.Fallback, Persist<T>.Sanitize(this.fileName));
    }
  }

  public bool Exists
  {
    get
    {
      return File.Exists(this.FilePath);
    }
  }

  public void Delete()
  {
    if (!File.Exists(this.FilePath))
      return;
    File.Delete(this.FilePath);
  }

  public void Clear()
  {
    this.value = (object) null;
  }

  private object DeserializeObjectFromFile(string filePath)
  {
    object obj = (object) null;
    try
    {
      obj = EasySerializer.DeserializeObjectFromFile(filePath);
    }
    catch (SerializationException ex)
    {
      if (filePath.Contains(Persist<T>.Sanitize(Persist.auth.fileName)))
        throw ex;
    }
    return obj;
  }

  public T Data
  {
    get
    {
      if (this.value == null)
        this.value = this.DeserializeObjectFromFile(this.FilePath) ?? (object) new T();
      return (T) this.value;
    }
    set
    {
      this.value = (object) value;
    }
  }

  public void Flush()
  {
    EasySerializer.SerializeObjectToFile((object) this.Data, this.FilePath);
  }
}
