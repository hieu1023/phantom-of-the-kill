﻿// Decompiled with JetBrains decompiler
// Type: NGxMaskSpriteWithScale
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[RequireComponent(typeof (UI2DSprite))]
public class NGxMaskSpriteWithScale : MonoBehaviour
{
  private bool enableMask = true;
  public float scale = 1f;
  private Color beforeColor = Color.white;
  [SerializeField]
  private Texture2D _maskTexture;
  public UI2DSprite MainUI2DSprite;
  public bool isMultiMaskColor;
  private Texture2D maskTextureTemp;
  private Texture2D lastMaskTexture;
  public int xOffsetPixel;
  public int yOffsetPixel;
  public bool isTopFit;
  [HideInInspector]
  public float topOffset;
  [SerializeField]
  private float xOffsetPixelForAnimation;
  [SerializeField]
  private float yOffsetPixelForAnimation;
  private float beforeXOffsetPixelForAnimation;
  private float beforeYOffsetPixelForAnimation;

  public Texture2D maskTexture
  {
    get
    {
      return this._maskTexture;
    }
    set
    {
      this._maskTexture = value;
      this.maskTextureTemp = this._maskTexture;
      this.FitMask();
    }
  }

  public float spriteAlpha
  {
    get
    {
      return (Object) this.MainUI2DSprite != (Object) null ? this.MainUI2DSprite.color.a * this.MainUI2DSprite.finalAlpha : 0.0f;
    }
  }

  public void Start()
  {
    if (!((Object) this.MainUI2DSprite != (Object) null) || !((Object) this.MainUI2DSprite.material != (Object) null))
      return;
    this.FitMask();
  }

  private void setMask()
  {
    this.maskTexture = this._maskTexture;
  }

  private void Update()
  {
    if ((Object) this.MainUI2DSprite == (Object) null)
      return;
    UnityEngine.Material material = this.MainUI2DSprite.material;
    if ((Object) material == (Object) null)
      return;
    if ((double) this.beforeXOffsetPixelForAnimation != (double) this.xOffsetPixelForAnimation || (double) this.beforeYOffsetPixelForAnimation != (double) this.yOffsetPixelForAnimation)
      this.FitMask();
    Color color = this.MainUI2DSprite.color;
    color.a *= this.MainUI2DSprite.finalAlpha;
    if (!(this.beforeColor != color))
      return;
    material.SetColor("_Color", color);
    this.beforeColor = color;
    this.MainUI2DSprite.material = (UnityEngine.Material) null;
    this.MainUI2DSprite.material = material;
    this.MainUI2DSprite.SetDirty();
  }

  public void SetMaskEnable(bool enable)
  {
    this.enableMask = enable;
    this._maskTexture = !enable ? Resources.Load<Texture2D>("sprites/1x1_black") : this.maskTextureTemp;
    this.FitMask();
  }

  public void FitMask()
  {
    if ((Object) this.MainUI2DSprite.mainTexture == (Object) null)
    {
      Debug.LogWarning((object) ("mainTexture is null " + this.gameObject.name));
    }
    else
    {
      if ((Object) this.maskTexture == (Object) null)
        return;
      if (this.enableMask)
      {
        this.MainUI2DSprite.width = this.maskTexture.width;
        this.MainUI2DSprite.height = this.maskTexture.height;
      }
      else
      {
        this.MainUI2DSprite.width = this.MainUI2DSprite.mainTexture.width;
        this.MainUI2DSprite.height = this.MainUI2DSprite.mainTexture.height;
      }
      NGxMaskSpriteWithScale maskSpriteWithScale = this;
      float width1 = (float) this.MainUI2DSprite.width;
      float height1 = (float) this.MainUI2DSprite.height;
      float width2 = (float) this.MainUI2DSprite.mainTexture.width;
      float height2 = (float) this.MainUI2DSprite.mainTexture.height;
      double num1 = (double) height2 / (double) height1;
      float b = width2 / width1;
      float num2 = Mathf.Max((float) num1, b);
      float num3 = Mathf.Min((float) num1, b);
      float num4 = (float) num1 / num2 / num3 / maskSpriteWithScale.scale;
      float num5 = b / num2 / num3 / maskSpriteWithScale.scale;
      float num6 = ((float) maskSpriteWithScale.xOffsetPixel + this.xOffsetPixelForAnimation) / width2;
      float num7 = ((float) maskSpriteWithScale.yOffsetPixel + this.yOffsetPixelForAnimation) / height2;
      if (maskSpriteWithScale.isTopFit)
      {
        maskSpriteWithScale.topOffset = (float) (((double) height1 * (double) num3 - (double) height1) / 2.0);
        num7 += maskSpriteWithScale.topOffset / height2;
      }
      else
        maskSpriteWithScale.topOffset = 0.0f;
      float num8 = num6 - (float) (((double) num4 - 1.0) / 2.0);
      float num9 = num7 - (float) (((double) num5 - 1.0) / 2.0);
      string str = string.Format("dynamic {0}x{1} x({2}) y({3}) {4}", (object) num4, (object) num5, (object) num8, (object) num9, (object) (UIDrawCall.Clipping) ((Object) this.MainUI2DSprite.panel != (Object) null ? (int) this.MainUI2DSprite.panel.clipping : 0));
      if (!((Object) this.MainUI2DSprite == (Object) null) && !((Object) this.MainUI2DSprite.material == (Object) null) && (!(str != this.MainUI2DSprite.material.name) && !((Object) this.lastMaskTexture != (Object) this.maskTexture)))
        return;
      UnityEngine.Shader shader = !this.isMultiMaskColor ? (!((Object) this.MainUI2DSprite.panel != (Object) null) || this.MainUI2DSprite.panel.clipping != UIDrawCall.Clipping.SoftClip ? (!((Object) this.MainUI2DSprite.panel != (Object) null) || this.MainUI2DSprite.panel.clipping != UIDrawCall.Clipping.AlphaClip ? UnityEngine.Shader.Find("Unlit/AlphaMask") : UnityEngine.Shader.Find("Unlit/AlphaMask (AlphaClip)")) : UnityEngine.Shader.Find("Unlit/AlphaMask (SoftClip)")) : (!((Object) this.MainUI2DSprite.panel != (Object) null) || this.MainUI2DSprite.panel.clipping != UIDrawCall.Clipping.SoftClip ? (!((Object) this.MainUI2DSprite.panel != (Object) null) || this.MainUI2DSprite.panel.clipping != UIDrawCall.Clipping.AlphaClip ? UnityEngine.Shader.Find("Unlit/AlphaMaskMultiColor") : UnityEngine.Shader.Find("Unlit/AlphaMaskMultiColor (AlphaClip)")) : UnityEngine.Shader.Find("Unlit/AlphaMaskMultiColor (SoftClip)"));
      UnityEngine.Material material = new UnityEngine.Material(shader);
      material.name = str;
      material.SetTexture("_MaskTex", (Texture) maskSpriteWithScale.maskTexture);
      material.SetFloat("_xScale", num4);
      material.SetFloat("_yScale", num5);
      material.SetFloat("_xOffset", num8);
      material.SetFloat("_yOffset", num9);
      this.MainUI2DSprite.shader = shader;
      this.MainUI2DSprite.material = material;
      this.MainUI2DSprite.mainTexture.wrapMode = TextureWrapMode.Clamp;
      this.lastMaskTexture = this.maskTexture;
      this.beforeXOffsetPixelForAnimation = this.xOffsetPixelForAnimation;
      this.beforeYOffsetPixelForAnimation = this.yOffsetPixelForAnimation;
      this.MainUI2DSprite.MarkAsChanged();
    }
  }
}
