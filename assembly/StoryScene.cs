﻿// Decompiled with JetBrains decompiler
// Type: StoryScene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;

public class StoryScene : NGSceneBase
{
  public StoryExecuter executer;

  public static void changeScene009_3(bool stack)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("story009_3", stack, (object[]) Array.Empty<object>());
  }

  public override IEnumerator onInitSceneAsync()
  {
    if ((UnityEngine.Object) this.executer != (UnityEngine.Object) null)
    {
      this.executer.gameObject.SetActive(true);
      yield return (object) this.executer.initialize((string) null, (System.Action) null);
    }
  }

  public void onStartScene()
  {
    this.executer.render();
  }
}
