﻿// Decompiled with JetBrains decompiler
// Type: FaceBookWrapper
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using Facebook.Unity;
using GameCore.FastMiniJSON;
using System;
using System.Collections.Generic;

public static class FaceBookWrapper
{
  private static bool init = false;
  private static bool init_complete = false;
  private static string lintStr = "";
  private static string linkNameStr = "";
  private static string linkCaptionStr = "";
  private static string linkDescriptionStr = "";

  public static void TutorialComplete()
  {
    if (!FaceBookWrapper.init_complete)
      return;
    FB.LogAppEvent("fb_mobile_tutorial_completion", new float?(), (Dictionary<string, object>) null);
  }

  public static void Purchase(float purchase, string currency, string productID)
  {
    if (!FaceBookWrapper.init_complete)
      return;
    FB.LogPurchase(purchase, currency, (Dictionary<string, object>) null);
  }

  public static void Feed(
    string link,
    string linkName,
    string linkCaption,
    string linkDescription)
  {
    if (!FaceBookWrapper.init_complete)
      return;
    FaceBookWrapper.lintStr = link;
    FaceBookWrapper.linkNameStr = linkName;
    FaceBookWrapper.linkCaptionStr = linkCaption;
    FaceBookWrapper.linkDescriptionStr = linkDescription;
    if (FB.IsLoggedIn)
      FB.FeedShare("", (Uri) null, "", "", "", (Uri) null, "", new FacebookDelegate<IShareResult>(FaceBookWrapper.OnActionShared));
    else
      FB.LogInWithPublishPermissions((IEnumerable<string>) new string[1]
      {
        "publish_actions"
      }, new FacebookDelegate<ILoginResult>(FaceBookWrapper.OnLoguinWithFeed));
  }

  public static void OnLoguinWithFeed(ILoginResult result)
  {
    if (result.Error != null)
      Debug.LogError((object) ("OnActionShared: Error: " + result.Error));
    if (result == null || result.Error != null)
      return;
    FB.FeedShare("", (Uri) null, "", "", "", (Uri) null, "", new FacebookDelegate<IShareResult>(FaceBookWrapper.OnActionShared));
  }

  public static void OnActionShared(IShareResult result)
  {
    if (result.Error != null)
      Debug.LogError((object) ("OnActionShared: Error: " + result.Error));
    if (result == null || result.Error != null)
      return;
    Dictionary<string, object> dictionary = Json.Deserialize(result.RawResult) as Dictionary<string, object>;
    object obj = (object) 0;
    if (dictionary == null || dictionary.Count <= 0 || dictionary.TryGetValue("cancelled", out obj))
    {
      Debug.LogWarning((object) "Request cancelled");
    }
    else
    {
      if (!dictionary.TryGetValue("id", out obj) && !dictionary.TryGetValue("post_id", out obj))
        return;
      Debug.LogWarning((object) "Request Send");
    }
  }

  public static void setup()
  {
    if (FaceBookWrapper.init)
      return;
    FaceBookWrapper.init = true;
    FB.Init((InitDelegate) (() =>
    {
      FaceBookWrapper.init_complete = true;
      FB.ActivateApp();
    }), (HideUnityDelegate) null, (string) null);
  }
}
