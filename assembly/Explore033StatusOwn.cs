﻿// Decompiled with JetBrains decompiler
// Type: Explore033StatusOwn
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;

public class Explore033StatusOwn : Battle0181CharacterStatus
{
  public override IEnumerator Init(
    BL.UnitPosition up,
    AttackStatus attackStatus,
    int firstAttack,
    bool isColosseum,
    bool isDemoMode)
  {
    // ISSUE: reference to a compiler-generated field
    int num = this.\u003C\u003E1__state;
    Explore033StatusOwn explore033StatusOwn = this;
    if (num != 0)
      return false;
    // ISSUE: reference to a compiler-generated field
    this.\u003C\u003E1__state = -1;
    explore033StatusOwn.current = up;
    explore033StatusOwn.maxHp = explore033StatusOwn.current.unit.initialHp;
    explore033StatusOwn.currentHp = explore033StatusOwn.current.unit.exploreHp;
    explore033StatusOwn.hpGauge.setValue(explore033StatusOwn.currentHp, explore033StatusOwn.maxHp, false, -1f, -1f);
    explore033StatusOwn.setHPNumbers(explore033StatusOwn.currentHp.ToString());
    explore033StatusOwn.txt_consumeHp.SetTextLocalize("");
    explore033StatusOwn.txt_characterName_ElementOn.SetTextLocalize("探索チーム");
    explore033StatusOwn.isHpDamaged = false;
    return false;
  }

  public override void Healed(int heal)
  {
  }
}
