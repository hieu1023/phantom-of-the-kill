﻿// Decompiled with JetBrains decompiler
// Type: Story00913Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Story00913Scene : NGSceneBase
{
  public Story00913Menu menu;
  public UILabel TxtTitle;
  [SerializeField]
  private NGxScroll ScrollContainer;

  public IEnumerator onStartSceneAsync()
  {
    PlayerSeaQuestS[] self = SMManager.Get<PlayerSeaQuestS[]>();
    IEnumerator e1;
    if (self == null)
    {
      Singleton<CommonRoot>.GetInstance().isLoading = true;
      Future<WebAPI.Response.QuestProgressSea> ft = WebAPI.QuestProgressSea((System.Action<WebAPI.Response.UserError>) (e => WebAPI.DefaultUserErrorCallback(e)));
      e1 = ft.Wait();
      while (e1.MoveNext())
        yield return e1.Current;
      e1 = (IEnumerator) null;
      if (ft.Result == null)
      {
        Singleton<CommonRoot>.GetInstance().isLoading = false;
        yield break;
      }
      else
      {
        self = SMManager.Get<PlayerSeaQuestS[]>();
        ft = (Future<WebAPI.Response.QuestProgressSea>) null;
      }
    }
    this.ScrollContainer.Clear();
    if (self == null || self.Length == 0)
    {
      Singleton<CommonRoot>.GetInstance().isLoading = false;
    }
    else
    {
      e1 = this.menu.InitChapterButton(self.DisplayScrollM(((IEnumerable<PlayerSeaQuestS>) self).Where<PlayerSeaQuestS>((Func<PlayerSeaQuestS, bool>) (x => x != null)).FirstOrDefault<PlayerSeaQuestS>().quest_sea_s.quest_xl.ID));
      while (e1.MoveNext())
        yield return e1.Current;
      e1 = (IEnumerator) null;
      this.ScrollContainer.ResolvePosition();
      Singleton<CommonRoot>.GetInstance().isLoading = false;
    }
  }
}
