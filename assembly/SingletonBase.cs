﻿// Decompiled with JetBrains decompiler
// Type: SingletonBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public abstract class SingletonBase : MonoBehaviour
{
  protected abstract void Initialize();

  protected virtual void Finlaize()
  {
  }

  protected abstract void clearInstance();

  public void forceDestroy()
  {
    this.clearInstance();
    this.Finlaize();
  }
}
