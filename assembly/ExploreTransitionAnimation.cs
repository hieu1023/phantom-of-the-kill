﻿// Decompiled with JetBrains decompiler
// Type: ExploreTransitionAnimation
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ExploreTransitionAnimation : MonoBehaviour
{
  [SerializeField]
  private SpriteTransitionController mTrantionController;
  [SerializeField]
  private Texture2D mMaskIn;
  [SerializeField]
  private Texture2D mMaskOut;
  public bool IsExploreOnly;

  public void SetInMode()
  {
    this.mTrantionController.mask = this.mMaskIn;
  }

  public void SetOutMode()
  {
    this.mTrantionController.mask = this.mMaskOut;
  }

  private void playSound(string seName)
  {
    if (this.IsExploreOnly && !Singleton<ExploreSceneManager>.GetInstance().IsSceneActive || string.IsNullOrEmpty(seName))
      return;
    Singleton<NGSoundManager>.GetInstance().playSE(seName.Split('.')[0], false, 0.0f, -1);
  }
}
