﻿// Decompiled with JetBrains decompiler
// Type: MypageSlidePanelOpen
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class MypageSlidePanelOpen : MonoBehaviour
{
  private System.Action<MypageSlidePanelOpen> endAction;

  public void Init(System.Action<MypageSlidePanelOpen> action)
  {
    this.endAction = action;
  }

  public void StartEffect()
  {
  }

  public void EndEffect()
  {
    this.endAction(this);
  }
}
