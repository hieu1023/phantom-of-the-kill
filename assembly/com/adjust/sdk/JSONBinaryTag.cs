﻿// Decompiled with JetBrains decompiler
// Type: com.adjust.sdk.JSONBinaryTag
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

namespace com.adjust.sdk
{
  public enum JSONBinaryTag
  {
    Array = 1,
    Class = 2,
    Value = 3,
    IntValue = 4,
    DoubleValue = 5,
    BoolValue = 6,
    FloatValue = 7,
  }
}
