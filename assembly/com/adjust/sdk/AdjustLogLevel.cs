﻿// Decompiled with JetBrains decompiler
// Type: com.adjust.sdk.AdjustLogLevel
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

namespace com.adjust.sdk
{
  public enum AdjustLogLevel
  {
    Verbose = 1,
    Debug = 2,
    Info = 3,
    Warn = 4,
    Error = 5,
    Assert = 6,
    Suppress = 7,
  }
}
