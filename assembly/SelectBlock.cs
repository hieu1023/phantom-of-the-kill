﻿// Decompiled with JetBrains decompiler
// Type: SelectBlock
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class SelectBlock
{
  public List<SelectBlock.Data> data = new List<SelectBlock.Data>();
  public int selected = -1;
  public GameObject obj;

  public class Data
  {
    public string msg = "";
    public string label = "";
  }
}
