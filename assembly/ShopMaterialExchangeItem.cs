﻿// Decompiled with JetBrains decompiler
// Type: ShopMaterialExchangeItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using SM;
using System.Collections;
using UnityEngine;

public class ShopMaterialExchangeItem : MonoBehaviour
{
  [SerializeField]
  private UIButton btnSelect_;
  [SerializeField]
  private UIButton btnInfo_;
  [SerializeField]
  private UILabel txtName_;
  [SerializeField]
  private UILabel txtInfo_;
  [SerializeField]
  private UILabel txtCount_;
  [SerializeField]
  private GameObject txtCountSprite;
  [SerializeField]
  private UILabel materialNum;
  [SerializeField]
  private GameObject topIcon_;
  private ShopMaterialExchangeListMenu menu_;
  public SelectTicketSelectSample sample_;
  private int limitCount;
  private int currNums;

  public IEnumerator coInitialize(
    ShopMaterialExchangeListMenu menu,
    SelectTicketSelectSample sample,
    PlayerSelectTicketSummary playerUnitTicket,
    SM.SelectTicket unitTicket)
  {
    ShopMaterialExchangeItem materialExchangeItem = this;
    materialExchangeItem.menu_ = menu;
    materialExchangeItem.sample_ = sample;
    materialExchangeItem.txtName_.SetTextLocalize(materialExchangeItem.sample_.reward_title);
    materialExchangeItem.txtInfo_.SetTextLocalize(materialExchangeItem.sample_.reward_info);
    materialExchangeItem.SetButtonEnabled(playerUnitTicket.quantity, unitTicket.exchange_limit, materialExchangeItem.limitCount);
    IEnumerator coroutine = materialExchangeItem.menu_.SetIcon(materialExchangeItem.sample_, materialExchangeItem.topIcon_.transform);
    yield return (object) materialExchangeItem.StartCoroutine(coroutine);
    materialExchangeItem.currNums = (int) coroutine.Current;
    bool flag = materialExchangeItem.sample_.entity_type != MasterDataTable.CommonRewardType.deck;
    materialExchangeItem.txtCount_.gameObject.SetActive(flag);
    materialExchangeItem.txtCountSprite.gameObject.SetActive(flag);
    materialExchangeItem.txtCount_.SetTextLocalize(materialExchangeItem.currNums);
    materialExchangeItem.materialNum.SetTextLocalize(materialExchangeItem.sample_.reward_value);
    materialExchangeItem.btnSelect_.onClick.Clear();
    materialExchangeItem.btnSelect_.onClick.Add(new EventDelegate((EventDelegate.Callback) (() => this.menu_.onClickSelect(this.sample_))));
    if (materialExchangeItem.sample_.entity_type == MasterDataTable.CommonRewardType.deck)
    {
      materialExchangeItem.btnInfo_.gameObject.SetActive(true);
      materialExchangeItem.btnInfo_.onClick.Clear();
      materialExchangeItem.btnInfo_.onClick.Add(new EventDelegate((EventDelegate.Callback) (() => menu.onBtnInfo(this.sample_))));
    }
    else
      materialExchangeItem.btnInfo_.gameObject.SetActive(false);
  }

  public void UpdateInfo(
    PlayerSelectTicketSummary playerUnitTicket,
    SelectTicketSelectSample exchangeUnitSample,
    int ticketQuantity,
    SM.SelectTicket unitTicket)
  {
    this.SetButtonEnabled(ticketQuantity, unitTicket.exchange_limit, this.limitCount);
  }

  private void SetButtonEnabled(int quantity, bool isLimit, int count = 0)
  {
    bool flag = true;
    if (isLimit)
      flag = count > 0;
    this.btnSelect_.isEnabled = flag && quantity > 0;
  }

  public void UpdateNums(int nums)
  {
    this.currNums += nums;
    this.txtCount_.SetTextLocalize(this.currNums);
  }
}
