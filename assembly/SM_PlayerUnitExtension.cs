﻿// Decompiled with JetBrains decompiler
// Type: SM_PlayerUnitExtension
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System;
using System.Collections.Generic;
using UniLinq;

public static class SM_PlayerUnitExtension
{
  public static int AmountHavingTargetUnit(
    this PlayerUnit[] self,
    int entity_id,
    MasterDataTable.CommonRewardType entity_type)
  {
    int ret = 0;
    int quantity = 0;
    if (entity_type == MasterDataTable.CommonRewardType.unit)
      ((IEnumerable<PlayerUnit>) ((IEnumerable<PlayerUnit>) self).Where<PlayerUnit>((Func<PlayerUnit, bool>) (pi => pi.unit != null && pi.unit.ID == entity_id)).ToArray<PlayerUnit>()).ForEach<PlayerUnit>((System.Action<PlayerUnit>) (ar => ret = ++quantity));
    return ret;
  }
}
