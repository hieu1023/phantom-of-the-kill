﻿// Decompiled with JetBrains decompiler
// Type: UI3DModelCreate
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System.Collections;
using UnityEngine;

public class UI3DModelCreate : MonoBehaviour
{
  [SerializeField]
  private GameObject base_model_;
  [SerializeField]
  protected GameObject unit_model_;
  [SerializeField]
  private GameObject equip_model_;
  [SerializeField]
  private GameObject tiara_model_;
  [SerializeField]
  private GameObject equip_gear_model_;
  [SerializeField]
  private GameObject equip_shield_model_;
  [SerializeField]
  private GameObject vehicle_model_;
  [SerializeField]
  protected Animator unit_animator_;
  [SerializeField]
  protected Animator vehicle_animator_;
  private Future<GameObject> unit_;
  private Future<GameObject> equip_;
  private Future<GameObject> equip_gear_;
  private Future<GameObject> equip_shield_;
  private Future<GameObject> vehicle_;
  private Future<GameObject> tiara_;
  protected Future<RuntimeAnimatorController> unit_runtime_;
  protected Future<RuntimeAnimatorController> vehicle_runtime_;
  protected clipEffectPlayer clip_effect_player;
  protected bool is_left_hand;
  protected bool is_dual_wield;
  protected bool is_rainbow;
  public bool winAnimator_;

  public GameObject BaseModel
  {
    get
    {
      return this.base_model_;
    }
    set
    {
      this.base_model_ = value;
    }
  }

  public GameObject UnitModel
  {
    get
    {
      return this.unit_model_;
    }
    set
    {
      this.unit_model_ = value;
    }
  }

  public GameObject VehicleModel
  {
    get
    {
      return this.vehicle_model_;
    }
    set
    {
      this.vehicle_model_ = value;
    }
  }

  public GameObject EquipGearModel
  {
    get
    {
      return this.equip_gear_model_;
    }
    set
    {
      this.equip_gear_model_ = value;
    }
  }

  public GameObject EquipShieldModel
  {
    get
    {
      return this.equip_shield_model_;
    }
  }

  public GameObject EquipModel
  {
    get
    {
      return this.equip_model_;
    }
    set
    {
      this.equip_model_ = value;
    }
  }

  public GameObject TiaraModel
  {
    get
    {
      return this.tiara_model_;
    }
    set
    {
      this.tiara_model_ = value;
    }
  }

  public Animator UnitAnimator
  {
    get
    {
      return this.unit_animator_;
    }
    set
    {
      this.unit_animator_ = value;
    }
  }

  public Animator VehicleAnimator
  {
    get
    {
      return this.vehicle_animator_;
    }
    set
    {
      this.vehicle_animator_ = value;
    }
  }

  public void SetNull()
  {
    this.unit_ = (Future<GameObject>) null;
    this.equip_ = (Future<GameObject>) null;
    this.equip_gear_ = (Future<GameObject>) null;
    this.vehicle_ = (Future<GameObject>) null;
    this.unit_animator_ = (Animator) null;
    this.vehicle_animator_ = (Animator) null;
    this.unit_model_ = (GameObject) null;
    this.equip_model_ = (GameObject) null;
    this.equip_gear_model_ = (GameObject) null;
    this.equip_shield_model_ = (GameObject) null;
    this.vehicle_model_ = (GameObject) null;
    this.unit_runtime_ = (Future<RuntimeAnimatorController>) null;
    this.vehicle_runtime_ = (Future<RuntimeAnimatorController>) null;
  }

  protected IEnumerator InitUnitUnitAnimator(UnitUnit unit_data, int job_id)
  {
    this.unit_runtime_ = !this.winAnimator_ ? unit_data.LoadInitialDuelAnimator(job_id, 0) : unit_data.LoadInitialWinAnimator(job_id, 0);
    IEnumerator e = this.unit_runtime_.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.vehicle_runtime_ = unit_data.LoadAnimatorControllerDuelVehicle(unit_data.GetInitialGear(job_id).model_kind);
    e = this.vehicle_runtime_.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  protected IEnumerator InitPlayerUnitAnimator(PlayerUnit player_unit_data)
  {
    this.unit_runtime_ = !this.winAnimator_ ? player_unit_data.LoadDuelAnimator(0) : player_unit_data.LoadWinAnimator(0);
    IEnumerator e = this.unit_runtime_.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.vehicle_runtime_ = player_unit_data.unit.LoadAnimatorControllerDuelVehicle(player_unit_data.equippedWeaponGearOrInitial.model_kind);
    e = this.vehicle_runtime_.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  protected IEnumerator InitUnitUnitModel(UnitUnit unit_data, int job_id)
  {
    this.unit_ = unit_data.LoadModelDuel(job_id);
    IEnumerator e = this.unit_.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.vehicle_ = unit_data.LoadModelDuelVehicle();
    e = this.vehicle_.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.equip_ = unit_data.LoadModelDuelEquip();
    e = this.equip_.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.tiara_ = unit_data.LoadModelDuelEquipB();
    e = this.tiara_.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  protected IEnumerator InitUnitModel(PlayerUnit playerUnit)
  {
    this.unit_ = playerUnit.LoadModelDuel(0);
    IEnumerator e = this.unit_.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.vehicle_ = playerUnit.unit.LoadModelDuelVehicle();
    e = this.vehicle_.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.equip_ = playerUnit.unit.LoadModelDuelEquip();
    e = this.equip_.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.tiara_ = playerUnit.unit.LoadModelDuelEquipB();
    e = this.tiara_.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  private IEnumerator InitGear(UnitUnit unit_data)
  {
    this.unit_ = unit_data.LoadModelDuel();
    IEnumerator e = this.unit_.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.vehicle_ = unit_data.LoadModelDuelVehicle();
    e = this.vehicle_.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.equip_ = unit_data.LoadModelDuelEquip();
    e = this.equip_.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.tiara_ = unit_data.LoadModelDuelEquipB();
    e = this.tiara_.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  protected IEnumerator LoadGear(GearGear gear_data)
  {
    if (Singleton<ResourceManager>.GetInstance().Contains(string.Format("Gears/{0}/3D/prefab", (object) gear_data.resource_reference_gear_id.ID)))
    {
      this.equip_gear_ = gear_data.LoadModel();
      IEnumerator e = this.equip_gear_.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
  }

  public IEnumerator CreateModel(UnitUnit unit_data, int job_id = 0)
  {
    this.SetNull();
    GearGear initialGear = unit_data.GetInitialGear(job_id);
    IEnumerator e = this.InitUnitUnitAnimator(unit_data, job_id);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    e = this.InitUnitUnitModel(unit_data, job_id);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if (unit_data.non_disp_weapon == 0)
    {
      e = this.LoadGear(initialGear);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    this.is_left_hand = unit_data.isLeftHandInitialWeapon(job_id);
    this.is_dual_wield = unit_data.isDualWieldInitialWeapon(job_id);
    this.is_rainbow = unit_data.rainbow_on;
    Object.Destroy((Object) this.BaseModel);
    Object.Destroy((Object) this.UnitModel);
    this.Create();
    this.SetGear(initialGear.kind, unit_data.character.category);
    this.clip_effect_player.DeteilUnit = unit_data;
  }

  public IEnumerator CreateModel(PlayerUnit player_unit_data, int? setupShield = null)
  {
    this.SetNull();
    IEnumerator e = this.InitPlayerUnitAnimator(player_unit_data);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    e = this.InitUnitModel(player_unit_data);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if (player_unit_data.unit.non_disp_weapon == 0)
    {
      e = this.LoadGear(player_unit_data.equippedWeaponGearOrInitial);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      if (setupShield.HasValue)
      {
        GearGear shieldGearOrNull = player_unit_data.equippedShieldGearOrNull;
        if (shieldGearOrNull == null)
          MasterData.GearGear.TryGetValue(setupShield.Value, out shieldGearOrNull);
        if (shieldGearOrNull != null)
        {
          this.equip_shield_ = shieldGearOrNull.LoadModel();
          yield return (object) this.equip_shield_.Wait();
        }
      }
    }
    this.is_left_hand = player_unit_data.isLeftHandWeapon;
    this.is_dual_wield = player_unit_data.isDualWieldWeapon;
    this.is_rainbow = player_unit_data.unit.rainbow_on;
    this.Create();
    this.SetGear(player_unit_data.equippedWeaponGearOrInitial.kind, player_unit_data.unit.character.category);
    this.SetShield();
    this.clip_effect_player.DeteilUnit = player_unit_data.unit;
  }

  protected void Create()
  {
    this.base_model_ = ((bool) (Object) this.vehicle_.Result ? this.vehicle_.Result : this.unit_.Result).Clone(this.transform);
    this.unit_model_ = this.base_model_;
    this.unit_animator_ = this.unit_model_.GetComponentInChildren<Animator>();
    this.unit_animator_.runtimeAnimatorController = this.unit_runtime_.Result;
    this.clip_effect_player = this.unit_animator_.gameObject.GetComponent<clipEffectPlayer>();
    if ((Object) this.clip_effect_player == (Object) null)
      this.clip_effect_player = this.unit_animator_.gameObject.AddComponent<clipEffectPlayer>();
    if ((Object) this.vehicle_.Result != (Object) null)
    {
      Transform childInFind = this.unit_model_.transform.GetChildInFind("ridePoint");
      if ((Object) childInFind != (Object) null)
      {
        this.vehicle_model_ = this.unit_model_;
        this.vehicle_animator_ = this.vehicle_model_.GetComponentInChildren<Animator>();
        this.vehicle_animator_.runtimeAnimatorController = this.vehicle_runtime_.Result;
        this.unit_model_ = this.unit_.Result.Clone(childInFind);
        this.unit_animator_ = this.unit_model_.GetComponentInChildren<Animator>();
        this.unit_animator_.runtimeAnimatorController = this.unit_runtime_.Result;
        this.unit_model_.transform.localPosition = new Vector3(0.0f, -0.8f, 0.0f);
        this.clip_effect_player = this.unit_animator_.gameObject.GetComponent<clipEffectPlayer>();
        if ((Object) this.clip_effect_player == (Object) null)
          this.clip_effect_player = this.unit_animator_.gameObject.AddComponent<clipEffectPlayer>();
      }
      else
        Debug.LogWarning((object) "raidePoint が　みつからない。");
    }
    SkinnedMeshRenderer componentInChildren = this.unit_model_.GetComponentInChildren<SkinnedMeshRenderer>();
    MaterialPropertyBlock materialPropertyBlock = new MaterialPropertyBlock();
    materialPropertyBlock.SetFloat("_rainbow_On", this.is_rainbow ? 1f : 0.0f);
    MaterialPropertyBlock properties = materialPropertyBlock;
    componentInChildren.SetPropertyBlock(properties);
    Transform childInFind1 = this.unit_model_.transform.GetChildInFind("equippoint_a");
    if ((Object) childInFind1 != (Object) null)
      this.equip_model_ = this.equip_.Result.Clone(childInFind1);
    Transform childInFind2 = this.unit_model_.transform.GetChildInFind("equippoint_b");
    if (!((Object) childInFind2 != (Object) null))
      return;
    this.tiara_model_ = this.tiara_.Result.Clone(childInFind2);
  }

  public void ResetAnimator()
  {
    this.unit_animator_.runtimeAnimatorController = this.unit_runtime_.Result;
    this.unit_animator_.Play("wait", 0, 0.0f);
    if (!((Object) this.vehicle_animator_ != (Object) null))
      return;
    this.vehicle_animator_.Play("wait", 0, 0.0f);
  }

  public void SetGear(GearKind gear_data, UnitCategory category)
  {
    Object.Destroy((Object) this.equip_gear_model_);
    this.equip_gear_model_ = (GameObject) null;
    if (this.equip_gear_ == null)
      return;
    if (this.is_dual_wield)
    {
      Transform childInFind1 = this.unit_model_.transform.GetChildInFind("weaponl");
      Transform childInFind2 = this.unit_model_.transform.GetChildInFind("weaponr");
      this.SetGearPoint(childInFind1, gear_data, category);
      this.SetGearPoint(childInFind2, gear_data, category);
    }
    else
      this.SetGearPoint(this.unit_model_.transform.GetChildInFind(this.is_left_hand ? "weaponl" : "weaponr"), gear_data, category);
  }

  private void SetGearPoint(Transform gearPoint, GearKind gear_data, UnitCategory category)
  {
    if ((Object) gearPoint == (Object) null)
      return;
    if (this.equip_gear_.HasResult)
      this.equip_gear_model_ = this.equip_gear_.Result.Clone(gearPoint);
    if ((Object) this.equip_gear_model_ == (Object) null)
      return;
    switch (gear_data.Enum)
    {
      case GearKindEnum.sword:
      case GearKindEnum.axe:
      case GearKindEnum.spear:
      case GearKindEnum.staff:
      case GearKindEnum.shield:
      case GearKindEnum.unique_wepon:
      case GearKindEnum.smith:
        if (category == UnitCategory.player)
        {
          this.equip_gear_model_.transform.localRotation = Quaternion.Euler(0.0f, 180f, 0.0f);
          break;
        }
        this.equip_gear_model_.transform.localRotation = Quaternion.Euler(0.0f, 0.0f, 180f);
        break;
      case GearKindEnum.bow:
        this.equip_gear_model_.transform.localRotation = Quaternion.Euler(0.0f, -90f, -90f);
        break;
      case GearKindEnum.gun:
        this.equip_gear_model_.transform.localRotation = Quaternion.Euler(0.0f, 90f, -90f);
        break;
    }
  }

  private void SetShield()
  {
    if ((Object) this.equip_shield_model_ != (Object) null)
    {
      Object.Destroy((Object) this.equip_shield_model_);
      this.equip_shield_model_ = (GameObject) null;
    }
    if (this.equip_shield_ == null)
      return;
    this.equip_shield_model_ = this.equip_shield_.Result.Clone(this.unit_model_.transform.GetChildInFind("weaponl"));
    this.equip_shield_model_.SetActive(false);
  }

  private void OnEnable()
  {
    RenderSettings.ambientLight = Consts.GetInstance().UI3DMODEL_AMBIENT_COLOR;
  }

  private void OnDisable()
  {
    if (UI3DModel.countEnabled != 0)
      return;
    RenderSettings.ambientLight = Consts.GetInstance().UI3DMODEL_DEFAULT_AMBIENT_COLOR;
  }
}
