﻿// Decompiled with JetBrains decompiler
// Type: AnchorCustomAdjustment
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class AnchorCustomAdjustment : MonoBehaviour
{
  [SerializeField]
  private AnchorCustomAdjustment.AnchorSetting[] settings_ = new AnchorCustomAdjustment.AnchorSetting[1];

  private void Start()
  {
    AnchorAdjustmentController.AdjustAnchor(this.settings_);
  }

  private void OnDisable()
  {
    AnchorAdjustmentController.AdjustAnchor(this.settings_);
  }

  public void resetAnchors()
  {
    AnchorAdjustmentController.AdjustAnchor(this.settings_);
  }

  private IEnumerator ResetAnchorsCoroutine()
  {
    List<AnchorCustomAdjustment.AnchorSetting> listset = ((IEnumerable<AnchorCustomAdjustment.AnchorSetting>) this.settings_).Where<AnchorCustomAdjustment.AnchorSetting>((Func<AnchorCustomAdjustment.AnchorSetting, bool>) (s => (UnityEngine.Object) s.widget_ != (UnityEngine.Object) null || (UnityEngine.Object) s.panel_ != (UnityEngine.Object) null)).ToList<AnchorCustomAdjustment.AnchorSetting>();
    Dictionary<string, Transform> dictarget = new Dictionary<string, Transform>();
    foreach (AnchorCustomAdjustment.AnchorSetting anchorSetting in listset)
    {
      AnchorCustomAdjustment.AnchorSetting s = anchorSetting;
      Transform transform = (Transform) null;
      while (!string.IsNullOrEmpty(s.targetParentName_) && !dictarget.TryGetValue(s.targetParentName_, out transform))
      {
        yield return (object) null;
        transform = ((UnityEngine.Object) s.widget_ != (UnityEngine.Object) null ? s.widget_.transform : s.panel_.transform).GetParentInFind(s.targetParentName_);
        dictarget.Add(s.targetParentName_, transform);
      }
      if ((UnityEngine.Object) s.widget_ != (UnityEngine.Object) null)
      {
        if ((UnityEngine.Object) transform != (UnityEngine.Object) null)
        {
          if (s.isTargetLeft_)
            s.widget_.leftAnchor.target = transform;
          if (s.isTargetRight_)
            s.widget_.rightAnchor.target = transform;
          if (s.isTargetTop_)
            s.widget_.topAnchor.target = transform;
          if (s.isTargetBottom_)
            s.widget_.bottomAnchor.target = transform;
        }
        s.widget_.ResetAnchors();
      }
      if ((UnityEngine.Object) s.panel_ != (UnityEngine.Object) null)
      {
        if ((UnityEngine.Object) transform != (UnityEngine.Object) null)
        {
          if (s.isTargetLeft_)
            s.panel_.leftAnchor.target = transform;
          if (s.isTargetRight_)
            s.panel_.rightAnchor.target = transform;
          if (s.isTargetTop_)
            s.panel_.topAnchor.target = transform;
          if (s.isTargetBottom_)
            s.panel_.bottomAnchor.target = transform;
        }
        s.panel_.Update();
      }
      s = (AnchorCustomAdjustment.AnchorSetting) null;
    }
    List<AnchorCustomAdjustment.AnchorSetting> anchorSettingList = new List<AnchorCustomAdjustment.AnchorSetting>((IEnumerable<AnchorCustomAdjustment.AnchorSetting>) listset);
    anchorSettingList.Reverse();
    foreach (AnchorCustomAdjustment.AnchorSetting anchorSetting in anchorSettingList)
    {
      if ((UnityEngine.Object) anchorSetting.widget_ != (UnityEngine.Object) null)
        anchorSetting.widget_.Update();
      if ((UnityEngine.Object) anchorSetting.panel_ != (UnityEngine.Object) null)
        anchorSetting.panel_.Update();
    }
  }

  [Serializable]
  public class AnchorSetting
  {
    public bool isTargetLeft_ = true;
    public bool isTargetRight_ = true;
    public bool isTargetTop_ = true;
    public bool isTargetBottom_ = true;
    public UIWidget widget_;
    public UIPanel panel_;
    public string targetParentName_;
  }
}
