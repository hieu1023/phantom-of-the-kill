﻿// Decompiled with JetBrains decompiler
// Type: Quest00214Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Quest00214Menu : BackButtonMenuBase
{
  private List<Quest00214DirCombi> allQuestCombiCells = new List<Quest00214DirCombi>();
  private List<Quest00214DirTrio> allQuestTrioCells = new List<Quest00214DirTrio>();
  private List<UnitIcon> unitIcons = new List<UnitIcon>();
  private List<UnitIconInfo> allUnitInfos = new List<UnitIconInfo>();
  private List<bool> unitIsNewList = new List<bool>();
  private int targetUnitID = -1;
  private List<int> unitIdList = new List<int>();
  [SerializeField]
  protected UILabel TxtTitle;
  [SerializeField]
  protected NGxScroll2 ScrollContainerChara;
  [SerializeField]
  private NGxScroll ScrollContainerCombi;
  [SerializeField]
  private GameObject ibtnCombi;
  [SerializeField]
  private GameObject ibtnChara;
  [SerializeField]
  private UIButton CombiButton;
  [SerializeField]
  private UIButton CharaButton;
  public Quest00214aMenu CharaQuest;
  private PlayerCharacterQuestS[] AllCharaQuest;
  public bool onMask;
  public bool isSelect;
  private WebAPI.Response.QuestProgressCharacter apiResponse;
  private GameObject unitIconPrefab;
  private GameObject releaseConditionPrefab;
  private GameObject popupReleaseConditonPrefab;
  private GameObject dirCombiPrefab;
  private GameObject dirTrioPrefab;
  private QuestCharacterMReleaseCondition[] questMRleaseCondition;
  private bool isInitialize;
  private bool isUpdateIcon;
  private int iconWidth;
  private int iconHeight;
  private int iconColumnValue;
  private int iconRowValue;
  private int iconScreenValue;
  private int iconMaxValue;
  private float scrool_start_y;
  protected GameObject unitPrefab;
  private HashSet<int> newCharactersSet;
  private PlayerCharacterQuestM[] characters;

  public virtual void VScrollBar()
  {
    Debug.Log((object) "click default event VScrollBar");
  }

  public virtual void Foreground()
  {
    Debug.Log((object) "click default event Foreground");
  }

  public void InitCombiQuestButton()
  {
    this.ibtnCombi.SetActive(Player.Current.IsCombiQuest());
  }

  public IEnumerator InitCharacterQuestButton(
    int? unitOrQuestId = null,
    bool isCurrentCombi = false,
    bool isSameUnit = false)
  {
    Quest00214Menu quest00214Menu = this;
    Singleton<CommonRoot>.GetInstance().isTouchBlock = true;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    quest00214Menu.CharaQuest.gameObject.SetActive(false);
    quest00214Menu.ScrollContainerChara.gameObject.SetActive(true);
    IEnumerator e = ServerTime.WaitSync();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Future<WebAPI.Response.QuestProgressCharacter> apiF = WebAPI.QuestProgressCharacter((System.Action<WebAPI.Response.UserError>) (error =>
    {
      WebAPI.DefaultUserErrorCallback(error);
      Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
    }));
    e = apiF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if (apiF.Result != null)
    {
      WebAPI.SetLatestResponsedAt("QuestProgressCharacter");
      WebAPI.SetLatestResponsedAt("QuestProgressHarmony");
      quest00214Menu.apiResponse = apiF.Result;
      quest00214Menu.characters = SMManager.Get<PlayerCharacterQuestM[]>();
      PlayerHarmonyQuestM[] harmonies = SMManager.Get<PlayerHarmonyQuestM[]>();
      PlayerCharacterQuestS[] characterQuestS = SMManager.Get<PlayerCharacterQuestS[]>();
      PlayerHarmonyQuestS[] harmonyS = SMManager.Get<PlayerHarmonyQuestS[]>();
      characterQuestS = characterQuestS.SelectReleased();
      harmonyS = harmonyS.SelectReleased();
      Future<GameObject> unitIconPrefabF = Res.Prefabs.UnitIcon.normal.Load<GameObject>();
      e = unitIconPrefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      quest00214Menu.unitIconPrefab = unitIconPrefabF.Result;
      Future<GameObject> releaseConditionPrefabF = Res.Prefabs.quest002_14.Condition.Load<GameObject>();
      e = releaseConditionPrefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      quest00214Menu.releaseConditionPrefab = releaseConditionPrefabF.Result;
      quest00214Menu.ScrollContainerChara.Clear();
      quest00214Menu.characters = ((IEnumerable<PlayerCharacterQuestM>) quest00214Menu.characters).OrderByDescending<PlayerCharacterQuestM, int>((Func<PlayerCharacterQuestM, int>) (x =>
      {
        QuestCharacterM questMId = x.quest_m_id;
        return questMId == null ? 0 : questMId.priority;
      })).ToArray<PlayerCharacterQuestM>();
      quest00214Menu.questMRleaseCondition = MasterData.QuestCharacterMReleaseConditionList;
      if ((UnityEngine.Object) quest00214Menu.popupReleaseConditonPrefab == (UnityEngine.Object) null)
      {
        Future<GameObject> popupReleaseConditonF = Res.Prefabs.popup.popup_002_14__chara_quest_release_condition__anim_popup01.Load<GameObject>();
        e = popupReleaseConditonF.Wait();
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        quest00214Menu.popupReleaseConditonPrefab = popupReleaseConditonF.Result;
        popupReleaseConditonF = (Future<GameObject>) null;
      }
      quest00214Menu.SetIconType();
      e = quest00214Menu.Initialize();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      quest00214Menu.allUnitInfos.Clear();
      quest00214Menu.newCharactersSet = new HashSet<int>();
      foreach (PlayerCharacterQuestS playerCharacterQuestS in ((IEnumerable<PlayerCharacterQuestS>) characterQuestS).Where<PlayerCharacterQuestS>((Func<PlayerCharacterQuestS, bool>) (x => x.is_new)))
        quest00214Menu.newCharactersSet.Add(playerCharacterQuestS.quest_character_s.unit.ID);
      quest00214Menu.unitIdList = Enumerable.Range(0, quest00214Menu.characters.Length).Select<int, int>((Func<int, int>) (i => i)).ToList<int>();
      List<int> range = quest00214Menu.unitIdList.GetRange(0, quest00214Menu.characters.Length);
      if (unitOrQuestId.HasValue)
        quest00214Menu.targetUnitID = unitOrQuestId.Value;
      e = quest00214Menu.CreateCharacterInfo(quest00214Menu.characters, range, false);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      e = quest00214Menu.CreateUnitIcon(quest00214Menu.characters.Length);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      quest00214Menu.ScrollContainerCombi.Clear();
      Future<GameObject> cell = Res.Prefabs.quest002_14.dir_Conbi.Load<GameObject>();
      e = cell.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      quest00214Menu.dirCombiPrefab = cell.Result;
      Future<GameObject> cell_trio = Res.Prefabs.quest002_14.dir_Trio.Load<GameObject>();
      e = cell_trio.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      quest00214Menu.dirTrioPrefab = cell_trio.Result;
      QuestHarmonyS[] questHarmonySList = MasterData.QuestHarmonySList;
      PlayerHarmonyQuestM[] playerHarmonyQuestMArray = harmonies;
      for (int index = 0; index < playerHarmonyQuestMArray.Length; ++index)
      {
        PlayerHarmonyQuestM harmony = playerHarmonyQuestMArray[index];
        QuestHarmonyS questSTable = ((IEnumerable<QuestHarmonyS>) questHarmonySList).FirstOrDefault<QuestHarmonyS>((Func<QuestHarmonyS, bool>) (x => x.quest_m_QuestHarmonyM == harmony.quest_m_id.ID));
        if (questSTable != null)
        {
          if (questSTable.target_unit2 == null)
          {
            GameObject gameObject = quest00214Menu.dirCombiPrefab.Clone((Transform) null);
            quest00214Menu.ScrollContainerCombi.Add(gameObject, false);
            Quest00214DirCombi questCell = gameObject.GetComponent<Quest00214DirCombi>();
            e = questCell.Init(questSTable, quest00214Menu.apiResponse.player_harmony_quests, new System.Action<int, int>(quest00214Menu.SelectHarmony));
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            quest00214Menu.allQuestCombiCells.Add(questCell);
            questCell = (Quest00214DirCombi) null;
          }
          else
          {
            GameObject gameObject = quest00214Menu.dirTrioPrefab.Clone((Transform) null);
            quest00214Menu.ScrollContainerCombi.Add(gameObject, false);
            Quest00214DirTrio questCellTrio = gameObject.GetComponent<Quest00214DirTrio>();
            e = questCellTrio.Init(questSTable, harmony.is_playable, quest00214Menu.apiResponse.player_harmony_quests, new System.Action<int, int[]>(quest00214Menu.SelectHarmony));
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            quest00214Menu.allQuestTrioCells.Add(questCellTrio);
            questCellTrio = (Quest00214DirTrio) null;
          }
        }
      }
      playerHarmonyQuestMArray = (PlayerHarmonyQuestM[]) null;
      quest00214Menu.ScrollContainerCombi.ResolvePosition();
      quest00214Menu.ScrollContainerChara.ResolvePosition();
      quest00214Menu.isSelect = false;
      quest00214Menu.IconEnable = !quest00214Menu.isSelect;
      if (unitOrQuestId.HasValue)
      {
        if (isCurrentCombi)
        {
          int questId = unitOrQuestId.Value;
          PlayerHarmonyQuestS playerHarmonyQuestS = ((IEnumerable<PlayerHarmonyQuestS>) harmonyS).FirstOrDefault<PlayerHarmonyQuestS>((Func<PlayerHarmonyQuestS, bool>) (s => s._quest_harmony_s == questId));
          if (playerHarmonyQuestS != null)
          {
            QuestHarmonyS qs = playerHarmonyQuestS.quest_harmony_s;
            if (qs.target_unit2_UnitUnit.HasValue)
              quest00214Menu.SelectHarmony(qs.unit_UnitUnit, new int[2]
              {
                qs.target_unit_UnitUnit,
                qs.target_unit2_UnitUnit.Value
              });
            else
              quest00214Menu.SelectHarmony(qs.unit_UnitUnit, qs.target_unit_UnitUnit);
            int? nullable = ((IEnumerable<PlayerHarmonyQuestM>) harmonies).FirstIndexOrNull<PlayerHarmonyQuestM>((Func<PlayerHarmonyQuestM, bool>) (m => m._quest_m_id == qs.quest_m_QuestHarmonyM));
            if (nullable.HasValue)
              quest00214Menu.ScrollContainerCombi.ResolvePosition(nullable.Value);
          }
        }
        else if (isSameUnit)
        {
          int unitId = unitOrQuestId.Value;
          int? nullable = ((IEnumerable<PlayerCharacterQuestM>) quest00214Menu.characters).FirstIndexOrNull<PlayerCharacterQuestM>((Func<PlayerCharacterQuestM, bool>) (x => x.unit_id.same_character_id == unitId));
          if (nullable.HasValue)
          {
            quest00214Menu.Select(quest00214Menu.characters[nullable.Value]._unit_id);
            quest00214Menu.ScrollContainerChara.ResolvePosition(nullable.Value, quest00214Menu.characters.Length);
          }
        }
        else
        {
          int unitId = unitOrQuestId.Value;
          quest00214Menu.Select(unitId);
          int? nullable = ((IEnumerable<PlayerCharacterQuestM>) quest00214Menu.characters).FirstIndexOrNull<PlayerCharacterQuestM>((Func<PlayerCharacterQuestM, bool>) (x => x.unit_id.ID == unitId));
          if (nullable.HasValue)
            quest00214Menu.ScrollContainerChara.ResolvePosition(nullable.Value, quest00214Menu.characters.Length);
        }
      }
      quest00214Menu.ScrollContainerCombi.gameObject.SetActive(false);
      yield return (object) null;
      Singleton<CommonRoot>.GetInstance().isTouchBlock = false;
    }
  }

  private void Select(int unitId)
  {
    this.isSelect = true;
    this.IconEnable = !this.isSelect;
    this.CharaQuest.gameObject.SetActive(true);
    this.StartCoroutine(this.CharaQuest.Init(unitId, this.apiResponse, this.unitIconPrefab, this.releaseConditionPrefab));
  }

  private void SelectHarmony(int unitId, int targetUnitId)
  {
    this.isSelect = true;
    this.CellEnable = false;
    this.CharaQuest.gameObject.SetActive(true);
    int[] targetUnitIds = new int[1]{ targetUnitId };
    this.StartCoroutine(this.CharaQuest.Init(unitId, targetUnitIds, this.apiResponse, this.unitIconPrefab, this.releaseConditionPrefab, false));
  }

  private void SelectHarmony(int unitId, int[] targetUnitIds)
  {
    this.isSelect = true;
    this.CellEnable = false;
    this.CharaQuest.gameObject.SetActive(true);
    this.StartCoroutine(this.CharaQuest.Init(unitId, targetUnitIds, this.apiResponse, this.unitIconPrefab, this.releaseConditionPrefab, true));
  }

  public bool IconEnable
  {
    set
    {
      this.unitIcons.ForEach((System.Action<UnitIcon>) (x => x.buttonBoxCollider.enabled = value));
      this.CombiButton.enabled = value;
    }
  }

  public bool CellEnable
  {
    set
    {
      this.allQuestCombiCells.ForEach((System.Action<Quest00214DirCombi>) (x => x.buttonBoxCollider.enabled = value));
      this.allQuestTrioCells.ForEach((System.Action<Quest00214DirTrio>) (x => x.buttonBoxCollider.enabled = value));
      this.CharaButton.enabled = value;
    }
  }

  public string SetTxtTitle
  {
    set
    {
      this.TxtTitle.SetTextLocalize(value);
    }
  }

  public void IbtnCombi()
  {
    this.ChangeIbtnChara();
    this.SetTxtTitle = Consts.GetInstance().QUEST_00214_COMBI_TITLE;
    this.ScrollContainerCombi.gameObject.SetActive(true);
    this.ScrollContainerChara.gameObject.SetActive(false);
  }

  public void IbtnChara()
  {
    this.ChangeIbtnCombi();
    this.SetTxtTitle = Consts.GetInstance().QUEST_00214_CHARACTER_TITLE;
    this.ScrollContainerCombi.gameObject.SetActive(false);
    this.ScrollContainerChara.gameObject.SetActive(true);
  }

  public void ChangeIbtnCombi()
  {
    this.ibtnCombi.SetActive(true);
    this.ibtnChara.SetActive(false);
  }

  public void ChangeIbtnChara()
  {
    this.ibtnCombi.SetActive(false);
    this.ibtnChara.SetActive(true);
  }

  public override void onBackButton()
  {
    if (this.isSelect || this.IsPushAndSet())
      return;
    MypageScene.ChangeScene(false, false, false);
  }

  protected override void Update()
  {
    base.Update();
    this.ScrollUpdate();
  }

  public virtual IEnumerator Initialize()
  {
    Singleton<CommonRoot>.GetInstance().isTouchBlock = true;
    Future<GameObject> prefabF = Res.Prefabs.UnitIcon.normal.Load<GameObject>();
    IEnumerator e = prefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.unitPrefab = prefabF.Result;
    this.isInitialize = false;
    this.ScrollContainerChara.Clear();
  }

  public void InitializeEnd()
  {
    this.StartCoroutine(this.LoadObject());
    this.isInitialize = true;
    Singleton<CommonRoot>.GetInstance().isTouchBlock = false;
  }

  public void SetIconType()
  {
    this.iconWidth = UnitIcon.Width;
    this.iconHeight = UnitIcon.Height;
    this.iconColumnValue = UnitIcon.ColumnValue;
    this.iconRowValue = UnitIcon.RowValue;
    this.iconScreenValue = UnitIcon.ScreenValue;
    this.iconMaxValue = UnitIcon.MaxValue;
  }

  protected IEnumerator LoadObject()
  {
    Quest00214Menu quest00214Menu = this;
    yield return (object) null;
    SMManager.Get<PlayerCharacterQuestS[]>();
    if (quest00214Menu.allUnitInfos.Count > quest00214Menu.iconMaxValue)
      quest00214Menu.StartCoroutine(quest00214Menu.LoadObjectNormal());
  }

  private IEnumerator LoadObjectNormal()
  {
    yield return (object) null;
    for (int i = this.iconMaxValue; i < this.allUnitInfos.Count; ++i)
    {
      IEnumerator e = UnitIcon.LoadSprite(this.allUnitInfos[i].unit, 0);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
  }

  protected void ScrollUpdate()
  {
    if ((!this.isInitialize || this.allUnitInfos.Count <= this.iconScreenValue) && !this.isUpdateIcon)
      return;
    int num1 = this.iconHeight * 2;
    float num2 = this.ScrollContainerChara.scrollView.transform.localPosition.y - this.scrool_start_y;
    float num3 = (float) (Mathf.Max(0, this.allUnitInfos.Count - this.iconScreenValue - 1) / this.iconColumnValue * this.iconHeight);
    float num4 = (float) (this.iconHeight * this.iconRowValue);
    if ((double) num2 < 0.0)
      num2 = 0.0f;
    if ((double) num2 > (double) num3)
      num2 = num3;
    while (true)
    {
      do
      {
        bool flag = false;
        int unit_index = 0;
        foreach (GameObject gameObject in this.ScrollContainerChara)
        {
          GameObject unit = gameObject;
          float num5 = unit.transform.localPosition.y + num2;
          if ((double) num5 > (double) num1)
          {
            int? nullable = this.allUnitInfos.FirstIndexOrNull<UnitIconInfo>((Func<UnitIconInfo, bool>) (v => (UnityEngine.Object) v.icon != (UnityEngine.Object) null && (UnityEngine.Object) v.icon.gameObject == (UnityEngine.Object) unit));
            int info_index = nullable.HasValue ? nullable.Value + this.iconMaxValue : (this.allUnitInfos.Count + 4) / 5 * 5;
            if (nullable.HasValue && info_index < (this.allUnitInfos.Count + 4) / 5 * 5)
            {
              unit.transform.localPosition = new Vector3(unit.transform.localPosition.x, unit.transform.localPosition.y - num4, 0.0f);
              if (info_index >= this.allUnitInfos.Count)
                unit.SetActive(false);
              else
                this.ScrollIconUpdate(info_index, unit_index);
              flag = true;
            }
          }
          else if ((double) num5 < -((double) num4 - (double) num1))
          {
            int num6 = this.iconMaxValue;
            if (!unit.activeSelf)
            {
              unit.SetActive(true);
              num6 = 0;
            }
            int? nullable = this.allUnitInfos.FirstIndexOrNull<UnitIconInfo>((Func<UnitIconInfo, bool>) (v => (UnityEngine.Object) v.icon != (UnityEngine.Object) null && (UnityEngine.Object) v.icon.gameObject == (UnityEngine.Object) unit));
            int info_index = nullable.HasValue ? nullable.Value - num6 : -1;
            if (nullable.HasValue && info_index >= 0)
            {
              unit.transform.localPosition = new Vector3(unit.transform.localPosition.x, unit.transform.localPosition.y + num4, 0.0f);
              this.ScrollIconUpdate(info_index, unit_index);
              flag = true;
            }
          }
          ++unit_index;
        }
        if (!flag)
          goto label_27;
      }
      while (!this.isUpdateIcon);
      this.isUpdateIcon = false;
    }
label_27:;
  }

  private void ScrollIconUpdate(int info_index, int unit_index)
  {
    this.ResetUnitIcon(unit_index);
    if (UnitIcon.IsCache(this.allUnitInfos[info_index].unit, 0))
      this.CreateUnitIconCache(info_index, unit_index);
    else
      this.StartCoroutine(this.CreateUnitIcon(info_index, unit_index));
  }

  protected virtual void ResetUnitIcon(int index)
  {
    if (this.unitIcons == null || this.unitIcons.Count == 0)
      return;
    UnitIcon unitIcon = this.unitIcons[index];
    unitIcon.ResetUnit();
    unitIcon.gameObject.SetActive(false);
    this.allUnitInfos.Where<UnitIconInfo>((Func<UnitIconInfo, bool>) (a => (UnityEngine.Object) a.icon == (UnityEngine.Object) unitIcon)).ForEach<UnitIconInfo>((System.Action<UnitIconInfo>) (b => b.icon = (UnitIconBase) null));
  }

  public IEnumerator CreateUnitIcon(int createIconMaxValue)
  {
    IEnumerator e = this.CreateUnitIconBase(this.unitPrefab, createIconMaxValue);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  private IEnumerator CreateUnitIconBase(GameObject prefab, int createIconMaxValue)
  {
    for (int index = 0; index < Mathf.Min(this.iconMaxValue, this.allUnitInfos.Count); ++index)
    {
      UnitIcon component = UnityEngine.Object.Instantiate<GameObject>(prefab).GetComponent<UnitIcon>();
      if (this.allUnitInfos[index].unit != null)
        component.unit = this.allUnitInfos[index].unit;
      this.unitIcons.Add(component);
      component.ForBattle = this.allUnitInfos[index].for_battle;
      component.TowerEntry = this.allUnitInfos[index].is_tower_entry;
      if (Singleton<NGGameDataManager>.GetInstance().IsEarth)
        component.UnitRental = false;
      else
        component.UnitRental = this.allUnitInfos[index].is_rental;
      if (this.allUnitInfos[index].unit != null)
        component.CanAwake = this.allUnitInfos[index].unit.can_awake_unit_flag;
      component.SetupDeckStatusBlink();
      component.Equip = this.allUnitInfos[index].equip;
      component.princessType.DispPrincessType(this.allUnitInfos[index].pricessType);
    }
    this.SetIcons(createIconMaxValue);
    for (int index = 0; index < Mathf.Min(this.iconMaxValue, this.allUnitInfos.Count); ++index)
      this.ResetUnitIcon(index);
    for (int i = 0; i < Mathf.Min(this.iconMaxValue, this.allUnitInfos.Count); ++i)
    {
      IEnumerator e = this.CreateUnitIcon(i, i);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
  }

  protected void SetIcons(int createIconMaxValue)
  {
    if (this.unitIcons.Count < 1)
      return;
    this.ScrollContainerChara.Reset();
    this.unitIcons.ForEach((System.Action<UnitIcon>) (x =>
    {
      x.transform.parent = this.transform;
      x.gameObject.SetActive(false);
    }));
    for (int startIndex = 0; startIndex < Mathf.Min(this.iconMaxValue, createIconMaxValue); ++startIndex)
    {
      this.ScrollContainerChara.Add(this.unitIcons[startIndex].gameObject, this.iconWidth, this.iconHeight, startIndex, false);
      this.unitIcons[startIndex].gameObject.SetActive(true);
    }
    this.ScrollContainerChara.CreateScrollPoint(this.iconHeight, createIconMaxValue);
    this.ScrollContainerChara.ResolvePosition();
    this.scrool_start_y = this.ScrollContainerChara.scrollView.transform.localPosition.y;
  }

  protected virtual IEnumerator CreateUnitIcon(int info_index, int unit_index)
  {
    UnitIcon unitIcon = this.unitIcons[unit_index];
    this.allUnitInfos.Where<UnitIconInfo>((Func<UnitIconInfo, bool>) (a => (UnityEngine.Object) a.icon == (UnityEngine.Object) unitIcon)).ForEach<UnitIconInfo>((System.Action<UnitIconInfo>) (b => b.icon = (UnitIconBase) null));
    this.allUnitInfos[info_index].icon = (UnitIconBase) unitIcon;
    if (this.allUnitInfos[info_index].is_unknown)
    {
      unitIcon.UnknownUnit();
    }
    else
    {
      IEnumerator e = unitIcon.SetUnit(this.allUnitInfos[info_index].unit, this.allUnitInfos[info_index].unit.GetElement(), false);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    unitIcon.Gray = this.allUnitInfos[info_index].gray;
    if (this.allUnitInfos[info_index].callback != null)
      unitIcon.onClick = (System.Action<UnitIconBase>) (x => this.allUnitInfos[info_index].callback());
    unitIcon.NewUnit = this.allUnitInfos[info_index].is_new;
    unitIcon.RarityCenter();
    unitIcon.BottomModeValue = this.allUnitInfos[info_index].unit.awake_unit_flag ? UnitIconBase.BottomMode.AwakeUnit : UnitIconBase.BottomMode.Normal;
    unitIcon.gameObject.SetActive(true);
  }

  protected virtual void CreateUnitIconCache(int info_index, int unit_index)
  {
    UnitIcon unitIcon = this.unitIcons[unit_index];
    this.allUnitInfos.Where<UnitIconInfo>((Func<UnitIconInfo, bool>) (a => (UnityEngine.Object) a.icon == (UnityEngine.Object) unitIcon)).ForEach<UnitIconInfo>((System.Action<UnitIconInfo>) (b => b.icon = (UnitIconBase) null));
    this.allUnitInfos[info_index].icon = (UnitIconBase) unitIcon;
    if (this.allUnitInfos[info_index].is_unknown)
      unitIcon.UnknownUnit();
    else
      unitIcon.SetUnitCache(this.allUnitInfos[info_index].unit, this.allUnitInfos[info_index].unit.GetElement(), 0);
    unitIcon.Gray = this.allUnitInfos[info_index].gray;
    if (this.allUnitInfos[info_index].callback != null)
      unitIcon.onClick = (System.Action<UnitIconBase>) (x => this.allUnitInfos[info_index].callback());
    unitIcon.NewUnit = this.allUnitInfos[info_index].is_new;
    unitIcon.RarityCenter();
    unitIcon.BottomModeValue = this.allUnitInfos[info_index].unit.awake_unit_flag ? UnitIconBase.BottomMode.AwakeUnit : UnitIconBase.BottomMode.Normal;
    unitIcon.gameObject.SetActive(true);
  }

  private IEnumerator CreateCharacterInfo(
    PlayerCharacterQuestM[] characters,
    List<int> createId,
    bool isWait = false)
  {
    if (isWait)
      yield return (object) null;
    foreach (int index in createId)
      this.allUnitInfos.Add(this.SetUnitIconInfo(new UnitIconInfo(), characters[index]));
  }

  private UnitIconInfo SetUnitIconInfo(
    UnitIconInfo iconInfo,
    PlayerCharacterQuestM questM)
  {
    UnitIconInfo unitIconInfo = iconInfo;
    bool isPlayable = questM.is_playable;
    QuestCharacterM quest_m = questM.quest_m_id;
    QuestCharacterMReleaseCondition questCondition = ((IEnumerable<QuestCharacterMReleaseCondition>) this.questMRleaseCondition).FirstOrDefault<QuestCharacterMReleaseCondition>((Func<QuestCharacterMReleaseCondition, bool>) (x => x.quest_m.ID == quest_m.ID));
    UnitUnit unit = questM.unit_id;
    bool flag = this.newCharactersSet.Contains(unit.ID);
    unitIconInfo.unit = unit;
    if (questCondition == null | isPlayable)
    {
      unitIconInfo.is_new = flag;
      if (!isPlayable)
        unitIconInfo.gray = true;
      unitIconInfo.callback = (System.Action) (() => this.Select(unit.ID));
    }
    else
    {
      unitIconInfo.is_unknown = true;
      unitIconInfo.callback = (System.Action) (() => Singleton<PopupManager>.GetInstance().open(this.popupReleaseConditonPrefab, false, false, false, true, false, false, "SE_1006").GetComponent<Quest00214PopupReleaseCondition>().Init(questCondition.required_condition));
    }
    return unitIconInfo;
  }
}
