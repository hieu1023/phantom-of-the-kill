﻿// Decompiled with JetBrains decompiler
// Type: Shop0071Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shop0071Scene : NGSceneBase
{
  [SerializeField]
  private Shop0071Menu menu;
  private WebAPI.Response.ShopStatus shopStatus;

  public static void changeScene(bool stack)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("shop007_1", stack, (object[]) Array.Empty<object>());
  }

  public static void changeScene(
    bool stack,
    WebAPI.Response.ShopStatus shopStatus,
    bool specialShopError)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("shop007_1", (stack ? 1 : 0) != 0, (object) shopStatus, (object) specialShopError);
  }

  public override IEnumerator onInitSceneAsync()
  {
    Shop0071Scene shop0071Scene = this;
    Future<GameObject> bgF = Res.Prefabs.BackGround.ShopBackground.Load<GameObject>();
    IEnumerator e = bgF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    shop0071Scene.backgroundPrefab = bgF.Result;
  }

  private IEnumerator startSceneAsync(
    WebAPI.Response.ShopStatus sStatus,
    bool specialShopError)
  {
    IEnumerator e = this.menu.Init(sStatus, specialShopError);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    UnitUnit shopTopUnit = ShopTopUnit.GetShopTopUnit();
    if (shopTopUnit != null)
    {
      e = OnDemandDownload.WaitLoadUnitResource((IEnumerable<UnitUnit>) new UnitUnit[1]
      {
        shopTopUnit
      }, false, (IEnumerable<string>) null, false);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
  }

  public IEnumerator onStartSceneAsync()
  {
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    Future<WebAPI.Response.ShopStatus> shoplistF = WebAPI.ShopStatus((System.Action<WebAPI.Response.UserError>) (e =>
    {
      WebAPI.DefaultUserErrorCallback(e);
      Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
    })).Then<WebAPI.Response.ShopStatus>((Func<WebAPI.Response.ShopStatus, WebAPI.Response.ShopStatus>) (result =>
    {
      Singleton<NGGameDataManager>.GetInstance().Parse(result);
      return result;
    }));
    IEnumerator e1 = shoplistF.Wait();
    while (e1.MoveNext())
      yield return e1.Current;
    e1 = (IEnumerator) null;
    if (shoplistF.Result != null)
    {
      WebAPI.SetLatestResponsedAt("ShopStatus");
      this.shopStatus = shoplistF.Result;
      e1 = this.startSceneAsync(this.shopStatus, false);
      while (e1.MoveNext())
        yield return e1.Current;
      e1 = (IEnumerator) null;
    }
  }

  public IEnumerator onStartSceneAsync(
    WebAPI.Response.ShopStatus sStatus,
    bool specialShopError)
  {
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    this.shopStatus = sStatus;
    IEnumerator e = this.startSceneAsync(this.shopStatus, specialShopError);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public void onStartScene()
  {
    Player player = SMManager.Get<Player>();
    this.menu.SetInitalView(player.coin.ToString(), CommonHeaderAP.GetApString(player.ap + player.ap_overflow, player.ap_max), player.max_units.ToString(), player.max_items.ToString());
    this.menu.onStartScene();
    this.StartCoroutine(this.hideLoading());
  }

  public void onStartScene(WebAPI.Response.ShopStatus sStatus, bool specialShopError)
  {
    this.onStartScene();
  }

  public IEnumerator onBackSceneAsync()
  {
    IEnumerator e = this.startSceneAsync(this.shopStatus, false);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public IEnumerator onBackSceneAsync(
    WebAPI.Response.ShopStatus sStatus,
    bool specialShopError)
  {
    IEnumerator e = this.startSceneAsync(this.shopStatus, false);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public void onBackScene()
  {
  }

  public void onBackScene(WebAPI.Response.ShopStatus sStatus, bool specialShopError)
  {
  }

  private void Update()
  {
    Player player = SMManager.Get<Player>();
    if (player == null)
      return;
    this.menu.SetTextData(player.coin.ToString(), CommonHeaderAP.GetApString(player.ap + player.ap_overflow, player.ap_max), player.max_units.ToString(), player.max_items.ToString());
  }

  private IEnumerator hideLoading()
  {
    yield return (object) new WaitForEndOfFrame();
    Singleton<CommonRoot>.GetInstance().isLoading = false;
  }
}
