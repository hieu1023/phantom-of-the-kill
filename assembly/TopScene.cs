﻿// Decompiled with JetBrains decompiler
// Type: TopScene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using AppSetup;
using DeviceKit;
using GameCore;
using Gsc;
using gu3.Device;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Util;

public class TopScene : MonoBehaviour
{
  private bool isIntoTouch = true;
  private const string toNextScene = "startup000_6";
  private const string filterSortKey = "FilterSortDataReset";
  [SerializeField]
  private GameObject mainMenu;
  [SerializeField]
  private GameObject eventMenu;
  [SerializeField]
  private string eventDateStart;
  [SerializeField]
  private string eventDateEnd;
  [SerializeField]
  private UIRoot uiRoot;
  [SerializeField]
  private GameObject popupPanel;
  private GameObject menu;
  private GameObject CodeError;
  private GameObject CodeErrorFgGID;
  private GameObject InputBlock;
  private GameObject SameTerminal;
  private GameObject Unknown;
  private GameObject Success;
  private GameObject BackCollider;
  [SerializeField]
  private Startup0008Data userPolicyData;
  [SerializeField]
  private UniWebViewController m_webview;
  [SerializeField]
  private FGGIDConnectInitializer m_FggIdConnect;
  private Startup0008TopPageMenu userPolicy;
  private Startup00016Menu privacy;
  private Startup00017Menu data_load;
  private Startup00018Menu data_load_fggid;
  private Transfer01281Menu data_load_warning;
  private Transfer01282Menu data_load_select;
  private UILabel txtApplicationVersion;
  private UILabel txtScreenSize;
  [SerializeField]
  private string defaultBgmName;
  [SerializeField]
  private string eventBgmName;
  private GameObject PGSSignInButton;
  private SetupFPSController popupSelectFPS;
  private SetupSoundController popupSelectSound;
  private PopupConfirm popupConfirmFPSAndSound;
  private TopBackGroundAnimation backGroundAnim;
  private TopPressBtnAnimation btnAnim;
  private UILabel txtUserID;
  private UILabel txtVersion;
  private GameObject nowPopup;
  private bool enablePressStartBtn;
  public TopScene.PopupCondition pCond;
  [SerializeField]
  private CriWareInitializer criWareInitializer;

  public bool EnablePressStartBtn
  {
    set
    {
      this.enablePressStartBtn = value;
    }
    get
    {
      return this.enablePressStartBtn;
    }
  }

  private void Awake()
  {
    Singleton<NGSoundManager>.GetInstance().IsTitleScene = true;
    GameObject self = this.isEventPeriod(this.eventDateStart, this.eventDateEnd) ? UnityEngine.Object.Instantiate<GameObject>(this.eventMenu) : UnityEngine.Object.Instantiate<GameObject>(this.mainMenu);
    self.SetParent(this.uiRoot.gameObject);
    self.GetComponent<UIPanel>().SetAnchor(this.uiRoot.gameObject, 59, 0, -59, 0);
    if (IOSUtil.IsDeviceGenerationiPhoneX)
      SafeAreaBandRoot.HideSafeAreaBand();
    StartupMenuObject component = self.GetComponent<StartupMenuObject>();
    this.menu = component.menu;
    this.CodeError = component.CodeError;
    this.CodeErrorFgGID = component.CodeErrorFgGID;
    this.InputBlock = component.InputBlock;
    this.SameTerminal = component.SameTerminal;
    this.Unknown = component.Unknown;
    this.Success = component.Success;
    this.BackCollider = component.BackCollider;
    this.userPolicy = component.userPolicy;
    this.privacy = component.privacy;
    this.data_load = component.data_load;
    this.data_load_fggid = component.data_load_fggid;
    this.data_load_warning = component.data_load_warning;
    this.data_load_select = component.data_load_select;
    this.txtApplicationVersion = component.txtApplicationVersion;
    this.txtScreenSize = component.txtScreenSize;
    this.PGSSignInButton = component.PGSSignInButton;
    this.backGroundAnim = component.backGroundAnim;
    this.btnAnim = component.btnAnim;
    this.txtUserID = component.txtUserID;
    this.txtVersion = component.txtVersion;
    this.popupSelectFPS = component.PopupSelectFPS;
    this.popupSelectSound = component.PopupSelectSound;
    this.popupConfirmFPSAndSound = component.PopupConfirmFPSAndSound;
    this.m_webview.m_followerObjects = new GameObject[1]
    {
      component.dirFggMission
    };
    EventDelegate.Set(component.ibtnFggMissionConnected.onClick, (EventDelegate.Callback) (() =>
    {
      this.openWebview();
      if (!IOSUtil.IsDeviceGenerationiPhoneX)
        return;
      SafeAreaBandRoot.ShowSafeAreaBand();
    }));
    EventDelegate.Set(component.ibtnFggMissionDisconnected.onClick, (EventDelegate.Callback) (() =>
    {
      this.openWebview();
      if (IOSUtil.IsDeviceGenerationiPhoneX)
        SafeAreaBandRoot.ShowSafeAreaBand();
      StatusBarHelper.SetVisibilityForIPhoneX(true);
    }));
    EventDelegate.Set(component.iconGoogle.onClick, (EventDelegate.Callback) (() => this.SignInPGS()));
    EventDelegate.Set(component.fggMissionClose.onClick, (EventDelegate.Callback) (() => this.m_webview.GetComponent<FGGIDConnectInitializer>().Initialize()));
    EventDelegate.Set(component.ibtnSight1.onClick, (EventDelegate.Callback) (() => this.DataLoadOn()));
    EventDelegate.Set(component.ibtnSight2.onClick, (EventDelegate.Callback) (() => this.PrivacyOn()));
    EventDelegate.Set(component.ibtnSight3.onClick, (EventDelegate.Callback) (() => this.DeleteCacheOn()));
    EventDelegate.Set(component.popup008Back.onClick, (EventDelegate.Callback) (() => this.UserPolicyDissent()));
    EventDelegate.Set(component.popup008Next.onClick, (EventDelegate.Callback) (() => this.UserPolicyAgree()));
    EventDelegate.Set(component.popup016Close.onClick, (EventDelegate.Callback) (() => this.PrivacyOff()));
    EventDelegate.Set(component.popup0178OK.onClick, (EventDelegate.Callback) (() => this.SuccessOff()));
    EventDelegate.Set(component.popup018Decide.onClick, (EventDelegate.Callback) (() => this.IbtnPopupFgGIDDecide()));
    EventDelegate.Set(component.popup018Back.onClick, (EventDelegate.Callback) (() => this.DataLoadFgGIDOff()));
    EventDelegate.Set(component.popup01281Back.onClick, (EventDelegate.Callback) (() => this.DataLoadWarningOff()));
    EventDelegate.Set(component.popup01281Next.onClick, (EventDelegate.Callback) (() => this.IbtnPopupNext()));
    EventDelegate.Set(component.popup01282Back.onClick, (EventDelegate.Callback) (() => this.DataLoadSelectOff()));
    EventDelegate.Set(component.popup01282Fggid.onClick, (EventDelegate.Callback) (() => this.IbtnPopupFgGIDMigrate()));
    EventDelegate.Set(component.popup01282Code.onClick, (EventDelegate.Callback) (() => this.IbtnPopupMigrate()));
    EventDelegate.Set(component.popup01288OK.onClick, (EventDelegate.Callback) (() => this.CodeErrorOff()));
    EventDelegate.Set(component.popup01289OK.onClick, (EventDelegate.Callback) (() => this.CodeErrorFgGIDOff()));
    EventDelegate.Set(component.popup019Decide.onClick, (EventDelegate.Callback) (() => this.IbtnPopupDecide()));
    EventDelegate.Set(component.popup019Back.onClick, (EventDelegate.Callback) (() => this.DataLoadOff()));
    EventDelegate.Set(component.popup012811OK.onClick, (EventDelegate.Callback) (() => this.InputBlockOff()));
    EventDelegate.Set(component.popup012812OK.onClick, (EventDelegate.Callback) (() => this.SameTerminalOff()));
    EventDelegate.Set(component.popup012813OK.onClick, (EventDelegate.Callback) (() => this.UnknownOff()));
    this.FilterSortDataCheck();
    EventDelegate.Set(component.buttonPressStart.onClick, (EventDelegate.Callback) (() => this.OnTouchStartHeaven()));
    ModalWindow.setupRootPanel(this.uiRoot);
    AppSetupFPS.SetDefault();
  }

  private bool isEventPeriod(string timeStart, string timeEnd)
  {
    DateTime dateTime1 = Convert.ToDateTime(timeStart);
    DateTime dateTime2 = Convert.ToDateTime(timeEnd);
    DateTime now = DateTime.Now;
    return DateTime.Compare(now, dateTime1) > 0 && DateTime.Compare(now, dateTime2) < 0;
  }

  private void FilterSortDataCheck()
  {
    if (!PlayerPrefs.HasKey("FilterSortDataReset"))
      PlayerPrefs.SetInt("FilterSortDataReset", 0);
    if (PlayerPrefs.GetInt("FilterSortDataReset") == 1)
      return;
    Persist.unit00410SortAndFilter.Delete();
    Persist.unit00411SortAndFilter.Delete();
    Persist.unit00412SortAndFilter.Delete();
    Persist.unit00468SortAndFilter.Delete();
    Persist.unit0048SortAndFilter.Delete();
    Persist.unit00481SortAndFilter.Delete();
    Persist.unit00491SortAndFilter.Delete();
    Persist.unit004912SortAndFilter.Delete();
    Persist.unit004ReincarnationTypeAndFilter.Delete();
    Persist.unit004431SortAndFilter.Delete();
    Persist.unit00486SortAndFilter.Delete();
    Persist.unit00487SortAndFilter.Delete();
    Persist.unit005411SortAndFilter.Delete();
    Persist.unit005468SortAndFilter.Delete();
    Persist.tower029UnitListSortAndFilter.Delete();
    Persist.unit004ExtraSkillEquipUnitListSortAndFilter.Delete();
    Persist.unit004StorageSortAndFilter.Delete();
    Persist.unit004JobChangeUnitSelectSortAndFilter.Delete();
    Persist.mypageEditorSortAndFilter.Delete();
    Persist.friendSupportSortAndFilter.Delete();
    Persist.bugu0052SortAndFilter.Delete();
    Persist.bugu005SupplyListSortAndFilter.Delete();
    Persist.bugu005MaterialListSortAndFilter.Delete();
    Persist.bugu005WeaponMaterialListSortAndFilter.Delete();
    Persist.bugu0052CompositeSortAndFilter.Delete();
    Persist.bugu0052RepairSortAndFilter.Delete();
    Persist.bugu0052SellSortAndFilter.Delete();
    Persist.bugu0052DrillingBaseSortAndFilter.Delete();
    Persist.bugu0052DrillingMaterialSortAndFilter.Delete();
    Persist.bugu0052BuildupBaseSortAndFilter.Delete();
    Persist.bugu0052BuildupMaterialSortAndFilter.Delete();
    Persist.unit0044SortAndFilter.Delete();
    Persist.bugu0552SortAndFilter.Delete();
    Persist.bugu055SellSortAndFilter.Delete();
    Persist.bugu0544SortAndFilter.Delete();
    Persist.unit004ExtraSkillSortAndFilter.Delete();
    Persist.unit004ExtraSkillEquipListSortAndFilter.Delete();
    PlayerPrefs.SetInt("FilterSortDataReset", 1);
  }

  private void Update()
  {
  }

  private void TransPopupCondition()
  {
    switch (this.pCond)
    {
      case TopScene.PopupCondition.NONE:
        this.ibtnBackEnd();
        break;
      case TopScene.PopupCondition.USER_POLICY:
        this.UserPolicyDissent();
        break;
      case TopScene.PopupCondition.USER_POLICY_DISSENT:
        this.UserPolicyCaution();
        break;
      case TopScene.PopupCondition.PLIVACY:
        this.PrivacyOff();
        break;
      case TopScene.PopupCondition.DATA_LOAD:
        this.DataLoadOff();
        break;
      case TopScene.PopupCondition.DATA_LOAD_WARNING:
        this.DataLoadWarningOff();
        break;
      case TopScene.PopupCondition.DATA_LOAD_SUCCESS:
        this.SuccessOff();
        break;
      case TopScene.PopupCondition.CLEAR_CACHE:
        this.popupDelete();
        break;
      case TopScene.PopupCondition.CLEAR_CACHE_WARNING:
        this.popupDelete();
        break;
      case TopScene.PopupCondition.GAME_END:
        this.popupDelete();
        break;
      case TopScene.PopupCondition.WEBVIEW:
        this.CloseWebView();
        break;
      case TopScene.PopupCondition.DATA_LOAD_SELECT:
        this.DataLoadSelectOff();
        break;
      case TopScene.PopupCondition.DATA_LOAD_FGGID:
        this.DataLoadFgGIDOff();
        break;
    }
  }

  private void ibtnBackEnd()
  {
    this.pCond = TopScene.PopupCondition.GAME_END;
    Consts instance = Consts.GetInstance();
    this.nowPopup = ModalWindow.ShowYesNo(instance.gamequit_title, instance.gamequit_text, new System.Action(this.Quit), (System.Action) (() =>
    {
      this.pCond = TopScene.PopupCondition.NONE;
      this.nowPopup = (GameObject) null;
    })).gameObject;
  }

  private void popupDelete()
  {
    UnityEngine.Object.DestroyObject((UnityEngine.Object) this.nowPopup);
    this.pCond = TopScene.PopupCondition.NONE;
  }

  private void openWebview()
  {
    if (this.pCond != TopScene.PopupCondition.NONE)
      return;
    this.pCond = TopScene.PopupCondition.WEBVIEW;
    this.m_webview.GetComponent<UniWebViewController>().Navigate();
  }

  private void Quit()
  {
  }

  public void SuccessOff()
  {
    this.Success.gameObject.SetActive(false);
    this.BackCollider.gameObject.SetActive(false);
    StartScript.Restart();
  }

  public void DataLoadOn()
  {
    this.data_load_warning.gameObject.SetActive(true);
    this.BackCollider.gameObject.SetActive(true);
    this.pCond = TopScene.PopupCondition.DATA_LOAD_WARNING;
  }

  public void IbtnPopupNext()
  {
    this.data_load_warning.gameObject.SetActive(false);
    this.BackCollider.gameObject.SetActive(true);
    this.data_load.gameObject.SetActive(true);
    this.pCond = TopScene.PopupCondition.DATA_LOAD;
  }

  public void IbtnPopupMigrate()
  {
    this.data_load_select.gameObject.SetActive(false);
    this.data_load.gameObject.SetActive(true);
    this.data_load.InitDataCode();
    this.BackCollider.gameObject.SetActive(true);
    this.pCond = TopScene.PopupCondition.DATA_LOAD;
  }

  public void IbtnPopupFgGIDMigrate()
  {
    this.data_load_select.gameObject.SetActive(false);
    this.data_load_fggid.gameObject.SetActive(true);
    this.data_load_fggid.InitDataCode();
    this.BackCollider.gameObject.SetActive(true);
    this.pCond = TopScene.PopupCondition.DATA_LOAD_FGGID;
  }

  public void DataLoadWarningOff()
  {
    this.data_load_warning.gameObject.SetActive(false);
    this.BackCollider.gameObject.SetActive(false);
    this.pCond = TopScene.PopupCondition.NONE;
  }

  public void IbtnPopupDecide()
  {
    this.data_load.MigrateAPI();
    this.data_load.gameObject.SetActive(false);
    this.pCond = TopScene.PopupCondition.DATA_LOAD;
  }

  public void IbtnPopupFgGIDDecide()
  {
    this.data_load_fggid.FgGIDMigrateAPI();
    this.data_load_fggid.gameObject.SetActive(false);
    this.pCond = TopScene.PopupCondition.DATA_LOAD_FGGID;
  }

  public void DataLoadSelectOff()
  {
    this.data_load_select.gameObject.SetActive(false);
    this.BackCollider.gameObject.SetActive(false);
    this.pCond = TopScene.PopupCondition.NONE;
  }

  public void DataLoadOff()
  {
    this.data_load.gameObject.SetActive(false);
    this.BackCollider.gameObject.SetActive(false);
    if (this.CodeError.activeSelf || this.InputBlock.activeSelf || this.SameTerminal.activeSelf)
      this.pCond = TopScene.PopupCondition.LOCK;
    else if (this.Success.activeSelf)
    {
      this.pCond = TopScene.PopupCondition.DATA_LOAD_SUCCESS;
      this.SuccessOff();
    }
    else
      this.pCond = TopScene.PopupCondition.NONE;
  }

  public void DataLoadFgGIDOff()
  {
    this.data_load_fggid.gameObject.SetActive(false);
    this.BackCollider.gameObject.SetActive(false);
    if (this.CodeErrorFgGID.activeSelf || this.InputBlock.activeSelf || this.SameTerminal.activeSelf)
      this.pCond = TopScene.PopupCondition.LOCK;
    else if (this.Success.activeSelf)
    {
      this.pCond = TopScene.PopupCondition.DATA_LOAD_SUCCESS;
      this.SuccessOff();
    }
    else
      this.pCond = TopScene.PopupCondition.NONE;
  }

  public void DeleteCacheOn()
  {
    Consts instance = Consts.GetInstance();
    if (Persist.battleEnvironment.Exists || Persist.pvpSuspend.Exists)
    {
      ModalWindow.ShowYesNo(instance.cache_clear_warning_title, instance.cache_clear_warning_body, (System.Action) (() =>
      {
        this.pCond = TopScene.PopupCondition.NONE;
        Persist.cacheInfo.Data.hasDeleted = true;
        Persist.cacheInfo.Flush();
        this.StartCoroutine(this.startClearCacheWithAutoSleep());
      }), (System.Action) (() => this.pCond = TopScene.PopupCondition.NONE));
      this.pCond = TopScene.PopupCondition.CLEAR_CACHE_WARNING;
    }
    else
    {
      string cacheTextNoMobile = instance.clear_cache_text_no_mobile;
      this.nowPopup = ModalWindow.ShowYesNo(instance.clear_cache_title, cacheTextNoMobile, (System.Action) (() => this.StartCoroutine(this.startClearCacheWithAutoSleep())), (System.Action) (() => this.pCond = TopScene.PopupCondition.NONE)).gameObject;
      this.pCond = TopScene.PopupCondition.CLEAR_CACHE;
    }
  }

  private IEnumerator startClearCacheWithAutoSleep()
  {
    this.pCond = TopScene.PopupCondition.LOCK;
    App.SetAutoSleep(false);
    IEnumerator e = this.startClearCache();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    App.SetAutoSleep(true);
  }

  private IEnumerator startClearCache()
  {
    Consts c = Consts.GetInstance();
    ModalWindow window = ModalWindow.Show(c.clear_cache_title, c.clearCacheProgress(0, 0), (System.Action) (() => this.pCond = TopScene.PopupCondition.NONE));
    window.DisableOkButton();
    IEnumerator e = ResourceDownloader.CleanCache((System.Action<int, int>) ((numerator, denominator) => window.SetText(c.clearCacheProgress(numerator, denominator))));
    while (e.MoveNext())
      yield return e.Current;
    window.SetText(c.clear_cache_done);
    window.EnableOkButton();
  }

  public void OpenWebView()
  {
    if ((UnityEngine.Object) this.m_webview == (UnityEngine.Object) null)
      return;
    this.pCond = TopScene.PopupCondition.WEBVIEW;
  }

  public void CloseWebView()
  {
    if ((UnityEngine.Object) this.m_webview == (UnityEngine.Object) null)
      return;
    this.pCond = TopScene.PopupCondition.NONE;
    if (!((UnityEngine.Object) this.m_FggIdConnect != (UnityEngine.Object) null))
      return;
    this.m_FggIdConnect.Initialize();
  }

  public void PrivacyOn()
  {
    this.privacy.gameObject.SetActive(true);
    this.BackCollider.gameObject.SetActive(true);
    this.pCond = TopScene.PopupCondition.PLIVACY;
  }

  public void PrivacyOff()
  {
    this.privacy.gameObject.SetActive(false);
    this.BackCollider.gameObject.SetActive(false);
    this.pCond = TopScene.PopupCondition.NONE;
  }

  public void UserPolicyOn()
  {
    this.userPolicy.gameObject.SetActive(true);
    this.StartCoroutine(this.userPolicy.ScrollValue());
    this.BackCollider.gameObject.SetActive(true);
    this.pCond = TopScene.PopupCondition.USER_POLICY;
  }

  public void UserPolicyAgree()
  {
    this.userPolicy.gameObject.SetActive(false);
    this.BackCollider.gameObject.SetActive(false);
    Persist.userPolicy.Data.SetUserPolicy(true);
    Persist.userPolicy.Flush();
    this.StartCoroutine(this.UserPolicy());
  }

  public void UserPolicyDissent()
  {
    this.pCond = TopScene.PopupCondition.USER_POLICY_DISSENT;
    this.userPolicy.gameObject.SetActive(false);
    this.StartCoroutine(PopupCommon.ShowMiniGame(this.userPolicyData.titleDissent, this.userPolicyData.dissent, this.popupPanel.transform, (System.Action) (() =>
    {
      this.BackCollider.gameObject.SetActive(false);
      this.pCond = TopScene.PopupCondition.NONE;
    })));
  }

  public void UserPolicyCaution()
  {
    this.BackCollider.gameObject.SetActive(false);
    this.pCond = TopScene.PopupCondition.NONE;
    PopupCommon componentInChildren = this.popupPanel.GetComponentInChildren<PopupCommon>();
    if (!((UnityEngine.Object) componentInChildren != (UnityEngine.Object) null))
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) componentInChildren.gameObject);
  }

  public void SelectFPSOn()
  {
    this.BackCollider.gameObject.SetActive(true);
    this.popupSelectFPS.gameObject.SetActive(true);
    this.pCond = TopScene.PopupCondition.SELECT_FPS;
  }

  public void SelectFPSOff()
  {
    this.BackCollider.gameObject.SetActive(false);
    this.popupSelectFPS.gameObject.SetActive(false);
    this.pCond = TopScene.PopupCondition.NONE;
  }

  public void CodeErrorOff()
  {
    this.BackCollider.SetActive(false);
    this.CodeError.SetActive(false);
    this.pCond = TopScene.PopupCondition.NONE;
  }

  public void CodeErrorFgGIDOff()
  {
    this.BackCollider.SetActive(false);
    this.CodeErrorFgGID.SetActive(false);
    this.pCond = TopScene.PopupCondition.NONE;
  }

  public void InputBlockOff()
  {
    this.InputBlock.SetActive(false);
    this.BackCollider.SetActive(false);
    this.pCond = TopScene.PopupCondition.NONE;
  }

  public void SameTerminalOff()
  {
    this.SameTerminal.SetActive(false);
    this.BackCollider.SetActive(false);
    this.pCond = TopScene.PopupCondition.NONE;
  }

  public void UnknownOff()
  {
    this.Unknown.SetActive(false);
    this.BackCollider.SetActive(false);
    this.pCond = TopScene.PopupCondition.NONE;
  }

  public void SceneChangeOPMovie()
  {
    int num = this.isIntoTouch ? 1 : 0;
  }

  private IEnumerator Start()
  {
    TopScene scene = this;
    while (!SDK.Initialized)
      yield return (object) null;
    scene.txtVersion.SetTextLocalize(Consts.Format(Consts.GetInstance().START_APPLICATION_VERSION, (IDictionary) new Hashtable()
    {
      {
        (object) "version",
        (object) Application.version
      }
    }));
    if (Persist.userInfo.Exists)
    {
      scene.txtUserID.gameObject.SetActive(true);
      scene.txtUserID.SetTextLocalize(Consts.Format(Consts.GetInstance().START_USER_ID, (IDictionary) new Hashtable()
      {
        {
          (object) "user_id",
          (object) Persist.userInfo.Data.userId
        }
      }));
    }
    else
      scene.txtUserID.gameObject.SetActive(false);
    scene.isIntoTouch = true;
    if ((UnityEngine.Object) scene.PGSSignInButton != (UnityEngine.Object) null)
      scene.PGSSignInButton.SetActive(false);
    Singleton<SocialManager>.GetInstance().Auth((System.Action<bool>) null);
    scene.privacy.InitScene();
    scene.userPolicyData = new Startup0008Data();
    IEnumerator e = scene.userPolicyData.Initialize();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    scene.userPolicy.Initialize(scene.userPolicyData.titleAgreement, scene.userPolicyData.agreementHedder, scene.userPolicyData.agreement);
    scene.menu.SetActive(true);
    string clip = scene.defaultBgmName;
    if (scene.isEventPeriod(scene.eventDateStart, scene.eventDateEnd))
      clip = scene.eventBgmName;
    if (clip != null || "" != clip)
    {
      Singleton<NGSoundManager>.GetInstance().OpeningStart();
      Singleton<NGSoundManager>.GetInstance().playBGM(clip, 0.1f);
    }
    scene.txtApplicationVersion.gameObject.SetActive(false);
    scene.txtScreenSize.gameObject.SetActive(false);
    if (NGGameDataManager.UrlSchemePresentId != -1)
      yield return (object) scene.StartCoroutine(URLScheme.Instance.RequestPresentGet());
    scene.isIntoTouch = false;
    scene.BackCollider.SetActive(false);
    scene.backGroundAnim.Init();
    scene.btnAnim.Init(scene);
  }

  private void OnDestroy()
  {
    this.criWareInitializer.atomConfig.useRandomSeedWithTime = false;
  }

  public void SignInPGS()
  {
  }

  public void OnTouchStart()
  {
    if (!this.EnablePressStartBtn)
      return;
    this.StartGame(true);
  }

  public void OnTouchStartHeaven()
  {
    if (!this.EnablePressStartBtn)
      return;
    this.StartGame(false);
  }

  private void StartGame(bool isSea)
  {
    this.BackCollider.SetActive(true);
    this.isIntoTouch = false;
    this.pCond = TopScene.PopupCondition.LOCK;
    NGGameDataManager.SeaChangeFlag = isSea;
    this.StartCoroutine(this.UserPolicy());
  }

  private void writePermissionAskData()
  {
    if (Persist.titlePermissionAsk.Exists && Persist.titlePermissionAsk.Data.alreadyShow)
      return;
    try
    {
      if (!Persist.titlePermissionAsk.Exists)
      {
        Persist.titlePermissionAsk.Data.reset();
        Persist.titlePermissionAsk.Flush();
      }
      Persist.titlePermissionAsk.Data.alreadyShow = true;
      Persist.titlePermissionAsk.Flush();
    }
    catch
    {
      Persist.titlePermissionAsk.Delete();
    }
  }

  private void changeNextScene()
  {
    this.pCond = TopScene.PopupCondition.LOCK;
    this.StartCoroutine(this.changeNextSceneLoop());
  }

  private IEnumerator changeNextSceneLoop()
  {
    BootLoader bootLoader = BootLoader.Lunch();
    while (!bootLoader.End)
      yield return (object) null;
    UnityEngine.Object.Destroy((UnityEngine.Object) bootLoader);
    Singleton<NGSoundManager>.GetInstance().IsTitleScene = false;
    WebAPI.Response.PlayerBootRelease lastPlayerBoot = WebAPI.LastPlayerBoot;
    if (!lastPlayerBoot.latest_application)
    {
      bool waitClick = true;
      ModalWindow.Show(Consts.GetInstance().TOP_SCENE_CHANGE_NEXT_SCENE_LOOP_1, Consts.GetInstance().TOP_SCENE_CHANGE_NEXT_SCENE_LOOP_2_PC, (System.Action) (() => waitClick = false));
      while (waitClick)
        yield return (object) null;
    }
    else if (!lastPlayerBoot.player_is_create)
      SceneManager.LoadScene("startup000_10_DL");
    else if (ResourceDownloader.IsDlcVersionChange())
    {
      SceneManager.LoadScene("startup000_10_2");
    }
    else
    {
      IEnumerator e = Singleton<ResourceManager>.GetInstance().InitResourceInfo();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      ResourceInfo resourceInfo = Singleton<ResourceManager>.GetInstance().ResourceInfo;
      string resourceDirectory = DLC.ResourceDirectory;
      HashSet<string> stringSet = new HashSet<string>((IEnumerable<string>) FileManager.GetEntries(resourceDirectory));
      foreach (ResourceInfo.Resource resource in resourceInfo)
      {
        ResourceInfo.Value obj = resource._value;
        switch (obj._path_type)
        {
          case ResourceInfo.PathType.AssetBundle:
            if (stringSet.Contains(obj._file_name))
            {
              CachedFile.Add(resourceDirectory + obj._file_name);
              continue;
            }
            continue;
          case ResourceInfo.PathType.StreamingAssets:
            if (stringSet.Contains(obj._file_name))
            {
              CachedFile.Add(resourceDirectory + obj._file_name);
              continue;
            }
            continue;
          default:
            continue;
        }
      }
      ResourceDownloader.Completed = true;
      SceneManager.LoadScene("main");
    }
  }

  public IEnumerator UserPolicy()
  {
    TopScene topScene = this;
    if (!topScene.isIntoTouch)
    {
      if (!Persist.userPolicy.Data.GetUserPolicy())
      {
        topScene.UserPolicyOn();
        while (true)
          yield return (object) null;
      }
      else
      {
        bool flag1 = false;
        if (!Persist.appFPS.Data.IsSetup && (UnityEngine.Object) topScene.popupSelectFPS != (UnityEngine.Object) null)
        {
          topScene.SelectFPSOn();
          while (!Persist.appFPS.Data.IsSetup)
            yield return (object) null;
          topScene.SelectFPSOff();
          flag1 = true;
        }
        bool flag2 = false;
        if ((UnityEngine.Object) topScene.popupConfirmFPSAndSound != (UnityEngine.Object) null && flag1 | flag2)
        {
          topScene.BackCollider.gameObject.SetActive(true);
          topScene.pCond = TopScene.PopupCondition.SELECT_FPS_AND_SOUND_CONFIRM;
          topScene.popupConfirmFPSAndSound.gameObject.SetActive(true);
          string str1 = "";
          string str2 = Persist.appFPS.Data.MaxFPS != 30 ? str1 + "アニメーション品質を「高 (60fps)」" : str1 + "アニメーション品質を「低 (30fps)」";
          if (str2 != "")
            str2 += "\n";
          string text = (!Persist.normalDLC.Data.IsSound ? str2 + "サウンド品質を「高音質版」" : str2 + "サウンド品質を「通常版」") + "\nに設定しました。";
          topScene.popupConfirmFPSAndSound.SelectText(text);
          while (!topScene.popupConfirmFPSAndSound.IsDecide)
            yield return (object) null;
          topScene.popupConfirmFPSAndSound.gameObject.SetActive(false);
          topScene.pCond = TopScene.PopupCondition.NONE;
          topScene.BackCollider.gameObject.SetActive(false);
        }
        topScene.isIntoTouch = true;
        Singleton<NGSoundManager>.GetInstance().playSE("SE_1001", false, 0.0f, -1);
        if ((UnityEngine.Object) topScene.backGroundAnim != (UnityEngine.Object) null)
          topScene.backGroundAnim.StartFinishAnim(new System.Action(topScene.changeNextScene));
        else
          topScene.changeNextScene();
      }
    }
  }

  private IEnumerator SetTextPolicy()
  {
    Debug.LogWarning((object) "SetTextPolicy agreement");
    yield return (object) null;
  }

  public GameObject agreementSource { get; set; }

  public enum PopupCondition
  {
    NONE,
    USER_POLICY,
    USER_POLICY_DISSENT,
    PLIVACY,
    DATA_LOAD,
    DATA_LOAD_WARNING,
    DATA_LOAD_SUCCESS,
    CLEAR_CACHE,
    CLEAR_CACHE_WARNING,
    GAME_END,
    LOCK,
    WEBVIEW,
    DATA_LOAD_SELECT,
    DATA_LOAD_FGGID,
    SELECT_FPS,
    SELECT_SOUND,
    SELECT_FPS_AND_SOUND_CONFIRM,
  }
}
