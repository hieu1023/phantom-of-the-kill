﻿// Decompiled with JetBrains decompiler
// Type: Unit004JobAfter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System.Collections;
using UnitStatusInformation;
using UnityEngine;

public class Unit004JobAfter : MonoBehaviour
{
  [SerializeField]
  private UILabel txtJobDescription;
  [Header("group 1")]
  [SerializeField]
  private UILabel txtJobNameGrp1;
  [SerializeField]
  private GameObject dir_Lv_grp1;
  [SerializeField]
  private UILabel txtActualLevelgrp1;
  [SerializeField]
  private UILabel txtNextLevelLeft;
  [SerializeField]
  private UILabel txtNextLevelRight;
  [Header("group 2")]
  [SerializeField]
  private UILabel txtJobNameGrp2;
  [SerializeField]
  private GameObject dir_Lv_grp2;
  [SerializeField]
  private UILabel txtActualLevelgrp2;
  [SerializeField]
  private GameObject[] dynSkillGenreIcons;
  [SerializeField]
  private GameObject objSkillZoom;
  private System.Action popupSkillDetail;

  public IEnumerator Init(
    bool isGrp1,
    PlayerUnitJob_abilities jobAbility,
    PlayerUnitJob_abilities beforeAbility = null,
    bool bActiveSkillZoom = false)
  {
    JobCharacteristics master = jobAbility.master;
    if (master != null)
    {
      BattleskillSkill skill = master.skill;
      this.txtJobDescription.SetTextLocalize(skill.description);
      this.objSkillZoom.SetActive(false);
      GameObject skillDetailPrefab = (GameObject) null;
      Future<GameObject> loader;
      if (bActiveSkillZoom)
      {
        loader = PopupSkillDetails.createPrefabLoader(Singleton<NGGameDataManager>.GetInstance().IsSea);
        yield return (object) loader.Wait();
        skillDetailPrefab = loader.Result;
        loader = (Future<GameObject>) null;
      }
      if (isGrp1)
      {
        int num = beforeAbility != null ? beforeAbility.level : jobAbility.level;
        int nextLevel = beforeAbility != null ? jobAbility.level : jobAbility.level + 1;
        this.dir_Lv_grp1.SetActive(true);
        this.dir_Lv_grp2.SetActive(false);
        this.txtJobNameGrp1.SetTextLocalize(skill.name);
        this.txtActualLevelgrp1.SetTextLocalize(Consts.Format(Consts.GetInstance().extra_skillI_thumb_skill_level_text, (IDictionary) new Hashtable()
        {
          {
            (object) "level",
            (object) num
          },
          {
            (object) "max",
            (object) skill.upper_level
          }
        }));
        this.txtNextLevelLeft.SetText(nextLevel.ToString());
        this.txtNextLevelRight.SetText("/" + skill.upper_level.ToString());
        if (bActiveSkillZoom)
        {
          this.popupSkillDetail = (System.Action) (() => PopupSkillDetails.show(skillDetailPrefab, new PopupSkillDetails.Param(skill, UnitParameter.SkillGroup.JobAbility, new int?(nextLevel)), false, (System.Action) null, false));
          this.objSkillZoom.SetActive(true);
        }
      }
      else
      {
        this.dir_Lv_grp1.SetActive(false);
        this.dir_Lv_grp2.SetActive(true);
        this.txtJobNameGrp2.SetTextLocalize(skill.name);
        UILabel txtActualLevelgrp2 = this.txtActualLevelgrp2;
        string thumbSkillLevelText = Consts.GetInstance().extra_skillI_thumb_skill_level_text;
        Hashtable hashtable;
        if (skill.upper_level <= 0)
        {
          hashtable = new Hashtable()
          {
            {
              (object) "level",
              jobAbility.level > 0 ? (object) jobAbility.level.ToString() : (object) Consts.GetInstance().SKILL_LEVEL_NONE
            },
            {
              (object) "max",
              (object) Consts.GetInstance().SKILL_LEVEL_NONE
            }
          };
        }
        else
        {
          hashtable = new Hashtable();
          hashtable.Add((object) "level", (object) jobAbility.level);
          hashtable.Add((object) "max", (object) skill.upper_level);
        }
        string text = Consts.Format(thumbSkillLevelText, (IDictionary) hashtable);
        txtActualLevelgrp2.SetTextLocalize(text);
        loader = Res.Icons.SkillGenreIcon.Load<GameObject>();
        IEnumerator e = loader.Wait();
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        GameObject result = loader.Result;
        loader = (Future<GameObject>) null;
        this.createGenreIcon(result, this.dynSkillGenreIcons[0].transform).GetComponent<SkillGenreIcon>().Init(skill.genre1);
        this.createGenreIcon(result, this.dynSkillGenreIcons[1].transform).GetComponent<SkillGenreIcon>().Init(skill.genre2);
        if (bActiveSkillZoom)
        {
          this.popupSkillDetail = (System.Action) (() => PopupSkillDetails.show(skillDetailPrefab, new PopupSkillDetails.Param(skill, UnitParameter.SkillGroup.JobAbility, new int?(jobAbility.level)), false, (System.Action) null, false));
          this.objSkillZoom.SetActive(true);
        }
      }
    }
  }

  private GameObject createGenreIcon(GameObject prefab, Transform trans)
  {
    GameObject gameObject = prefab.Clone(trans);
    UI2DSprite componentInChildren = gameObject.GetComponentInChildren<UI2DSprite>();
    if (!((UnityEngine.Object) componentInChildren != (UnityEngine.Object) null))
      return gameObject;
    componentInChildren.depth += 150;
    return gameObject;
  }

  public void onClickedSkillZoom()
  {
    this.popupSkillDetail();
  }
}
