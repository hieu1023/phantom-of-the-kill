﻿// Decompiled with JetBrains decompiler
// Type: Friend00820Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

public class Friend00820Scene : NGSceneBase
{
  [SerializeField]
  private Friend00820Menu menu;
  [SerializeField]
  private GameObject smsButton;
  [SerializeField]
  private GameObject lineButton;

  public IEnumerator onStartSceneAsync()
  {
    this.smsButton.SetActive(false);
    this.lineButton.SetActive(false);
    this.menu.ScrollContainerResolvePosition();
    yield break;
  }
}
