﻿// Decompiled with JetBrains decompiler
// Type: Quest00215AScene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using System.Collections;

public class Quest00215AScene : NGSceneBase
{
  public Quest00215AMenu menu;

  public IEnumerator onStartSceneAsync(int episode, UnitUnit unit)
  {
    IEnumerator e = this.menu.SetCharacter(episode, unit);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }
}
