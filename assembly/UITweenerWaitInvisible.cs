﻿// Decompiled with JetBrains decompiler
// Type: UITweenerWaitInvisible
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class UITweenerWaitInvisible : MonoBehaviour
{
  private UITweener tween;
  private float time;

  private void Start()
  {
    this.tween = this.GetComponent<UITweener>();
    this.time = 0.0f;
  }

  private void Update()
  {
    if (!(bool) (Object) this.tween)
      return;
    this.time += Time.deltaTime;
    this.GetComponent<Renderer>().enabled = this.IsStarted();
  }

  private bool IsStarted()
  {
    return (double) this.time > (double) this.tween.delay;
  }
}
