﻿// Decompiled with JetBrains decompiler
// Type: NGFullWidthGrid
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class NGFullWidthGrid : UIGrid
{
  protected override void Init()
  {
    base.Init();
    this.mPanel.UpdateAnchors();
    this.cellWidth = this.mPanel.width;
  }

  public override void Reposition()
  {
    if (!this.mInitDone)
      base.Init();
    if ((Object) this.mPanel != (Object) null)
    {
      this.mPanel.UpdateAnchors();
      if ((double) this.cellWidth != (double) this.mPanel.width)
        this.cellWidth = this.mPanel.width;
    }
    base.Reposition();
  }
}
