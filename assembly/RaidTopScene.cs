﻿// Decompiled with JetBrains decompiler
// Type: RaidTopScene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using SM;
using System;
using System.Collections;
using UnityEngine;

public class RaidTopScene : NGSceneBase
{
  [SerializeField]
  private RaidTopMenu menu;
  private bool playStory;

  public static void ChangeScene()
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("raid_top", Singleton<NGSceneManager>.GetInstance().sceneName != "raid_top", (object[]) Array.Empty<object>());
  }

  public static void ChangeSceneBattleFinish(bool isStack = true)
  {
    bool flag = true;
    Singleton<NGSceneManager>.GetInstance().changeScene("raid_top", (isStack ? 1 : 0) != 0, (object) flag);
  }

  public IEnumerator onStartSceneAsync()
  {
    IEnumerator e = this.onStartSceneAsync(false);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public IEnumerator onStartSceneAsync(bool fromBattle)
  {
    Singleton<CommonRoot>.GetInstance().isTouchBlock = true;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    this.setPlayStory();
    if (!this.playStory)
    {
      IEnumerator e = this.menu.InitializeAsync(fromBattle);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
  }

  private void setPlayStory()
  {
    Persist.eventStoryPlay.Data.SetReserveList(StoryPlaybackEventPlay.GetPlayIDList(ServerTime.NowAppTime(), this.sceneName), this.sceneName);
    this.playStory = Persist.eventStoryPlay.Data.PlayEventScript(this.sceneName, 0);
  }

  public void onStartScene()
  {
    if (this.playStory)
      return;
    Singleton<CommonRoot>.GetInstance().isTouchBlock = false;
    Singleton<CommonRoot>.GetInstance().isLoading = false;
    Singleton<CommonRoot>.GetInstance().loadingMode = 0;
    this.ShowAdvice();
    this.StartCoroutine(this.menu.playSceneStartEffect());
  }

  public void onStartScene(bool fromBattle)
  {
    this.onStartScene();
  }

  private void ShowAdvice()
  {
    Singleton<TutorialRoot>.GetInstance().ShowAdvice("guild028_1_raid_tutorial", 0, (System.Action) null);
  }

  public override IEnumerator onEndSceneAsync()
  {
    RaidTopScene raidTopScene = this;
    float startTime = Time.time;
    while (!raidTopScene.isTweenFinished && (double) Time.time - (double) startTime < (double) raidTopScene.tweenTimeoutTime)
      yield return (object) null;
    Singleton<CommonRoot>.GetInstance().loadingMode = 0;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    raidTopScene.isTweenFinished = true;
    yield return (object) null;
    // ISSUE: reference to a compiler-generated method
    yield return (object) raidTopScene.\u003C\u003En__0();
  }

  public override IEnumerator onDestroySceneAsync()
  {
    GuildUtil.gvgPopupState = GuildUtil.GvGPopupState.None;
    GuildUtil.gvgDeckAttack = (GvgDeck) null;
    GuildUtil.gvgDeckDefense = (GvgDeck) null;
    GuildUtil.gvgFriendDefense = (GvgReinforcement) null;
    return base.onDestroySceneAsync();
  }
}
