﻿// Decompiled with JetBrains decompiler
// Type: Story0592Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;

public class Story0592Scene : NGSceneBase
{
  private Story0592Menu menu;
  private bool isInit;

  public static void ChangeScene(bool stack)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("story059_2", stack, (object[]) Array.Empty<object>());
  }

  public override IEnumerator onInitSceneAsync()
  {
    Story0592Scene story0592Scene = this;
    story0592Scene.menu = story0592Scene.menuBase as Story0592Menu;
    if (!((UnityEngine.Object) story0592Scene.menu == (UnityEngine.Object) null))
    {
      story0592Scene.isInit = false;
      IEnumerator e = story0592Scene.menu.InitSceneAsync();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
  }

  public IEnumerator onStartSceneAsync()
  {
    if (!this.isInit && !((UnityEngine.Object) this.menu == (UnityEngine.Object) null))
    {
      IEnumerator e = this.menu.StartSceneAsync();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.isInit = true;
    }
  }
}
