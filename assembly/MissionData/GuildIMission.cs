﻿// Decompiled with JetBrains decompiler
// Type: MissionData.GuildIMission
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using System;

namespace MissionData
{
  internal class GuildIMission : IMission
  {
    private GuildMission data_;

    public bool isDaily
    {
      get
      {
        return false;
      }
    }

    public bool isGuild
    {
      get
      {
        return true;
      }
    }

    public bool isShow
    {
      get
      {
        DateTime dateTime = ServerTime.NowAppTimeAddDelta();
        if (this.data_.start_at.HasValue && this.data_.start_at.Value > dateTime)
          return false;
        return !this.data_.published_end_at.HasValue || !(this.data_.published_end_at.Value <= dateTime);
      }
    }

    public MissionType missionType
    {
      get
      {
        return MissionType.guild;
      }
    }

    public object original
    {
      get
      {
        return (object) this.data_;
      }
    }

    public int ID
    {
      get
      {
        return this.data_.ID;
      }
    }

    public int priority
    {
      get
      {
        return this.data_.priority;
      }
    }

    public int progress_max
    {
      get
      {
        return this.data_.achievement_count;
      }
    }

    public int condition
    {
      get
      {
        return this.data_.condition;
      }
    }

    public int own_progress_max
    {
      get
      {
        return this.data_.num;
      }
    }

    public string name
    {
      get
      {
        return this.data_.name;
      }
    }

    public string detail
    {
      get
      {
        return this.data_.detail;
      }
    }

    public string scene
    {
      get
      {
        return this.data_.scene;
      }
    }

    public int point
    {
      get
      {
        return 0;
      }
    }

    public GuildIMission(GuildMission data)
    {
      this.data_ = data;
    }
  }
}
