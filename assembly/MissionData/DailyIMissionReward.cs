﻿// Decompiled with JetBrains decompiler
// Type: MissionData.DailyIMissionReward
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;

namespace MissionData
{
  internal class DailyIMissionReward : IMissionReward
  {
    private DailyMissionReward data_;

    public bool isDaily
    {
      get
      {
        return true;
      }
    }

    public bool isGuild
    {
      get
      {
        return false;
      }
    }

    public object original
    {
      get
      {
        return (object) this.data_;
      }
    }

    public int reward_quantity
    {
      get
      {
        return this.data_.reward_quantity;
      }
    }

    public string reward_message
    {
      get
      {
        return this.data_.reward_message;
      }
    }

    public int reward_type_id
    {
      get
      {
        return this.data_.reward_type_id;
      }
    }

    public int reward_id
    {
      get
      {
        return this.data_.reward_id;
      }
    }

    public DailyIMissionReward(DailyMissionReward dat)
    {
      this.data_ = dat;
    }
  }
}
