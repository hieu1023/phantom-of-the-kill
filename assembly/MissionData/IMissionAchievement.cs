﻿// Decompiled with JetBrains decompiler
// Type: MissionData.IMissionAchievement
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

namespace MissionData
{
  public interface IMissionAchievement
  {
    bool isDaily { get; }

    bool isGuild { get; }

    bool isShow { get; }

    bool isCleared { get; }

    bool isOwnCleared { get; }

    bool isReceived { get; }

    object original { get; }

    int progress_count { get; }

    int own_progress_count { get; }

    IMissionReward[] rewards { get; }

    int mission_id { get; }

    IMission mission { get; }
  }
}
