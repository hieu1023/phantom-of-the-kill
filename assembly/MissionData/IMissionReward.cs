﻿// Decompiled with JetBrains decompiler
// Type: MissionData.IMissionReward
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

namespace MissionData
{
  public interface IMissionReward
  {
    bool isDaily { get; }

    bool isGuild { get; }

    object original { get; }

    int reward_quantity { get; }

    string reward_message { get; }

    int reward_type_id { get; }

    int reward_id { get; }
  }
}
