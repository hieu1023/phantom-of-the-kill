﻿// Decompiled with JetBrains decompiler
// Type: MissionData.DailyIMissionAchievement
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using SM;
using System;
using System.Collections.Generic;
using UniLinq;

namespace MissionData
{
  internal class DailyIMissionAchievement : IMissionAchievement
  {
    private PlayerDailyMissionAchievement data_;
    private DailyMission master_;
    private IMissionReward[] rewards_;
    private DailyIMission iMaster_;

    public bool isDaily
    {
      get
      {
        return true;
      }
    }

    public bool isGuild
    {
      get
      {
        return false;
      }
    }

    public bool isShow
    {
      get
      {
        if (this.data_.received_count >= this.data_.limit_count)
          return false;
        DailyMissionReward[] rewards = this.data_.rewards;
        return (rewards != null ? rewards.Length : 0) > 0;
      }
    }

    public bool isCleared
    {
      get
      {
        return this.data_.count >= this.data_.max_count;
      }
    }

    public bool isOwnCleared
    {
      get
      {
        return this.isCleared;
      }
    }

    public bool isReceived
    {
      get
      {
        return this.data_.received_count >= this.data_.limit_count;
      }
    }

    public object original
    {
      get
      {
        return (object) this.data_;
      }
    }

    public int progress_count
    {
      get
      {
        return this.data_.count;
      }
    }

    public int own_progress_count
    {
      get
      {
        return this.data_.count;
      }
    }

    public IMissionReward[] rewards
    {
      get
      {
        if (this.rewards_ != null)
          return this.rewards_;
        this.rewards_ = this.data_.rewards != null ? (IMissionReward[]) ((IEnumerable<DailyMissionReward>) this.data_.rewards).Select<DailyMissionReward, DailyIMissionReward>((Func<DailyMissionReward, DailyIMissionReward>) (r => new DailyIMissionReward(r))).ToArray<DailyIMissionReward>() : new IMissionReward[0];
        return this.rewards_;
      }
    }

    public int mission_id
    {
      get
      {
        return this.data_.mission_id;
      }
    }

    public IMission mission
    {
      get
      {
        return (IMission) this.iMaster_;
      }
    }

    public DailyIMissionAchievement(PlayerDailyMissionAchievement dat)
    {
      this.data_ = dat;
      this.rewards_ = (IMissionReward[]) null;
      this.iMaster_ = MasterData.DailyMission.TryGetValue(this.data_.mission_id, out this.master_) ? new DailyIMission(this.master_) : (DailyIMission) null;
    }
  }
}
