﻿// Decompiled with JetBrains decompiler
// Type: MissionData.IMission
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;

namespace MissionData
{
  public interface IMission
  {
    bool isDaily { get; }

    bool isGuild { get; }

    bool isShow { get; }

    MissionType missionType { get; }

    object original { get; }

    int ID { get; }

    int priority { get; }

    int progress_max { get; }

    int condition { get; }

    int own_progress_max { get; }

    string name { get; }

    string detail { get; }

    string scene { get; }

    int point { get; }
  }
}
