﻿// Decompiled with JetBrains decompiler
// Type: MissionData.Util
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using SM;

namespace MissionData
{
  public static class Util
  {
    public static IMissionReward Create(DailyMissionReward dat)
    {
      return dat == null ? (IMissionReward) null : (IMissionReward) new DailyIMissionReward(dat);
    }

    public static IMissionReward Create(GuildMissionReward dat)
    {
      return dat == null ? (IMissionReward) null : (IMissionReward) new GuildIMissionReward(dat);
    }

    public static IMissionAchievement Create(PlayerDailyMissionAchievement data)
    {
      return data == null ? (IMissionAchievement) null : (IMissionAchievement) new DailyIMissionAchievement(data);
    }

    public static IMissionAchievement Create(GuildMissionInfo data)
    {
      return data == null ? (IMissionAchievement) null : (IMissionAchievement) new GuildIMissionAchievement(data);
    }

    public static IMission Create(DailyMission data)
    {
      return data == null ? (IMission) null : (IMission) new DailyIMission(data);
    }

    public static IMission Create(GuildMission data)
    {
      return data == null ? (IMission) null : (IMission) new GuildIMission(data);
    }
  }
}
