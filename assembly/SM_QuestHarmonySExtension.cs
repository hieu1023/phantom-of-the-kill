﻿// Decompiled with JetBrains decompiler
// Type: SM_QuestHarmonySExtension
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using SM;
using System;
using System.Collections.Generic;
using UniLinq;

public static class SM_QuestHarmonySExtension
{
  public static PlayerHarmonyQuestS[] SelectReleased(
    this PlayerHarmonyQuestS[] self)
  {
    return ((IEnumerable<PlayerHarmonyQuestS>) self).Where<PlayerHarmonyQuestS>((Func<PlayerHarmonyQuestS, bool>) (questDetail => QuestCharacterS.CheckIsReleased(questDetail.quest_harmony_s.start_at))).ToArray<PlayerHarmonyQuestS>();
  }
}
