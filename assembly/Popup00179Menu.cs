﻿// Decompiled with JetBrains decompiler
// Type: Popup00179Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System;
using System.Collections;

public class Popup00179Menu : BackButtonMenuBase
{
  private Mypage0017Menu menu0017;
  private PlayerPresent deletePresent;

  public IEnumerator Init(PlayerPresent present, Mypage0017Menu menu)
  {
    this.menu0017 = menu;
    this.deletePresent = present;
    yield break;
  }

  private IEnumerator DeletePresent()
  {
    Popup00179Menu popup00179Menu = this;
    Singleton<PopupManager>.GetInstance().closeAll(false);
    Singleton<CommonRoot>.GetInstance().loadingMode = 1;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    Future<WebAPI.Response.PresentDelete> receive = WebAPI.PresentDelete(new int[1]
    {
      popup00179Menu.deletePresent.id
    }, (System.Action<WebAPI.Response.UserError>) (e =>
    {
      Singleton<CommonRoot>.GetInstance().loadingMode = 0;
      WebAPI.DefaultUserErrorCallback(e);
      Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
    }));
    IEnumerator e1 = receive.Wait();
    while (e1.MoveNext())
      yield return e1.Current;
    e1 = (IEnumerator) null;
    if (receive.Result != null)
    {
      popup00179Menu.menu0017.SaveScrollPosition();
      popup00179Menu.StartCoroutine(popup00179Menu.menu0017.UpdateList(SMManager.Get<PlayerPresent[]>()));
      Singleton<CommonRoot>.GetInstance().loadingMode = 0;
    }
  }

  public void IbtnYes()
  {
    this.StartCoroutine(this.DeletePresent());
  }

  public void IbtnNo()
  {
    Singleton<PopupManager>.GetInstance().onDismiss();
  }

  public override void onBackButton()
  {
    this.IbtnNo();
  }
}
