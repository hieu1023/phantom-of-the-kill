﻿// Decompiled with JetBrains decompiler
// Type: Guide01122bScene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using System.Collections;
using UnityEngine;

public class Guide01122bScene : NGSceneBase
{
  public Guide01122bMenu menu;

  public override IEnumerator onInitSceneAsync()
  {
    Guide01122bScene guide01122bScene = this;
    Future<GameObject> bgF = new ResourceObject("Prefabs/BackGround/UnitBackground_anim").Load<GameObject>();
    IEnumerator e = bgF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    guide01122bScene.backgroundPrefab = bgF.Result;
  }

  public IEnumerator onStartSceneAsync(UnitUnit unit)
  {
    IEnumerator e = this.menu.Init(unit, false, false);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.menu.SetNumber(unit);
  }
}
