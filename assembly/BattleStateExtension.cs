﻿// Decompiled with JetBrains decompiler
// Type: BattleStateExtension
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;

public static class BattleStateExtension
{
  public static void setState_(this BL.PhaseState self, BL.Phase state, BL env)
  {
    self.setStateWith(state, env, (System.Action) (() =>
    {
      if (!((UnityEngine.Object) Singleton<TutorialRoot>.GetInstance() != (UnityEngine.Object) null))
        return;
      Singleton<TutorialRoot>.GetInstance().OnBattleStateChange(env);
    }));
  }
}
