﻿// Decompiled with JetBrains decompiler
// Type: Unit0042FloatingDialogBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections.Generic;
using UnityEngine;

public class Unit0042FloatingDialogBase : MonoBehaviour
{
  [SerializeField]
  protected GameObject DialogConteiner;
  [SerializeField]
  protected GameObject dir_SpecialPoint;
  [SerializeField]
  protected GameObject dir_FamilyType;
  protected bool isShow;

  public void Show()
  {
    if (this.DialogConteiner.activeInHierarchy && this.isShow)
      return;
    Singleton<NGSoundManager>.GetInstance().playSE("SE_1006", false, 0.0f, -1);
    this.isShow = true;
    this.DialogConteiner.SetActive(true);
    ((IEnumerable<UITweener>) this.gameObject.GetComponentsInChildren<UITweener>()).ForEach<UITweener>((System.Action<UITweener>) (c =>
    {
      c.enabled = true;
      c.onFinished.Clear();
      c.PlayForward();
    }));
  }

  public void Hide()
  {
    if (!this.DialogConteiner.activeInHierarchy && !this.isShow)
      return;
    this.isShow = false;
    UITweener[] tweens = this.gameObject.GetComponentsInChildren<UITweener>();
    if (tweens.Length == 0)
      return;
    int finishCount = 0;
    EventDelegate.Callback onFinish = (EventDelegate.Callback) (() =>
    {
      if (!((UnityEngine.Object) this.DialogConteiner != (UnityEngine.Object) null) || ++finishCount < tweens.Length)
        return;
      this.DialogConteiner.SetActive(false);
    });
    ((IEnumerable<UITweener>) tweens).ForEach<UITweener>((System.Action<UITweener>) (c =>
    {
      c.onFinished.Clear();
      c.AddOnFinished(onFinish);
      c.PlayReverse();
    }));
  }

  private void Update()
  {
    if (!this.isShow || !Input.GetMouseButtonDown(0) && !Input.GetMouseButtonDown(1) && (!Input.GetMouseButtonDown(2) && (double) Input.GetAxis("Mouse ScrollWheel") == 0.0))
      return;
    this.Hide();
  }
}
