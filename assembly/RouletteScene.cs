﻿// Decompiled with JetBrains decompiler
// Type: RouletteScene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RouletteScene : NGSceneBase
{
  public RouletteMenu menu;

  public IEnumerator onStartSceneAsync()
  {
    RouletteScene rouletteScene = this;
    Singleton<CommonRoot>.GetInstance().isLoading = false;
    Singleton<CommonRoot>.GetInstance().loadingMode = 0;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    Future<GameObject> bgF = Res.Prefabs.BackGround.RouletteBackground.Load<GameObject>();
    IEnumerator e = bgF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    rouletteScene.backgroundPrefab = bgF.Result;
    Singleton<CommonRoot>.GetInstance().setBackground(rouletteScene.backgroundPrefab);
    string str = "windows";
    e = OnDemandDownload.waitLoadSomethingResource((IEnumerable<string>) new string[2]
    {
      str + "/VO_9001_acb",
      str + "/VO_9001_awb"
    }, false, false);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    e = rouletteScene.menu.Initialize();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public void onStartScene()
  {
    Singleton<CommonRoot>.GetInstance().isLoading = false;
    this.menu.PlayOttimoAnimation();
  }
}
