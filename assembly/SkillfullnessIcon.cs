﻿// Decompiled with JetBrains decompiler
// Type: SkillfullnessIcon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using UnityEngine;

public class SkillfullnessIcon : IconPrefabBase
{
  public UI2DSprite iconSprite;
  private const int KIND_NONE = 0;
  [SerializeField]
  private UnityEngine.Sprite[] icons;

  public UnityEngine.Sprite[] Icons
  {
    get
    {
      return this.icons;
    }
  }

  private void InitKindId(int kind)
  {
    this.iconSprite.sprite2D = this.icons[kind];
  }

  public void InitKindId(UnitFamily family)
  {
    this.InitKindId((int) family);
  }

  public void InitKindNone()
  {
    this.InitKindId(0);
  }
}
