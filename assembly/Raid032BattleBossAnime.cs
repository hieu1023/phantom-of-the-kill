﻿// Decompiled with JetBrains decompiler
// Type: Raid032BattleBossAnime
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using System.Collections;
using UnityEngine;

public class Raid032BattleBossAnime : MonoBehaviour
{
  [SerializeField]
  private SpriteRenderer mImage;
  [SerializeField]
  private Animator mImageAnimator;
  [SerializeField]
  private Transform mImageOffsetAnchor;

  public IEnumerator InitializeAsync(UnitUnit unit, float offsetY)
  {
    Future<UnityEngine.Sprite> future = unit.LoadSpriteLarge(1f);
    IEnumerator e = future.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.mImage.sprite = future.Result;
    Vector3 localPosition = this.mImageOffsetAnchor.localPosition;
    this.mImageOffsetAnchor.localPosition = new Vector3(localPosition.x, localPosition.y + offsetY, localPosition.z);
    this.StartAnime();
  }

  public void StartAnime()
  {
    this.mImageAnimator.SetTrigger("survive");
  }
}
