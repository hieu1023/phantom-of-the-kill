﻿// Decompiled with JetBrains decompiler
// Type: Battle01PVPBattleHeader
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

public class Battle01PVPBattleHeader : NGBattleMenuBase
{
  [SerializeField]
  private UILabel playerName;
  [SerializeField]
  private UILabel enemyName;

  protected override IEnumerator Start_Battle()
  {
    // ISSUE: reference to a compiler-generated field
    int num = this.\u003C\u003E1__state;
    Battle01PVPBattleHeader battle01PvpBattleHeader = this;
    if (num != 0)
      return false;
    // ISSUE: reference to a compiler-generated field
    this.\u003C\u003E1__state = -1;
    if (battle01PvpBattleHeader.battleManager.gameEngine != null)
    {
      battle01PvpBattleHeader.setText(battle01PvpBattleHeader.playerName, battle01PvpBattleHeader.battleManager.gameEngine.playerName);
      battle01PvpBattleHeader.setText(battle01PvpBattleHeader.enemyName, battle01PvpBattleHeader.battleManager.gameEngine.enemyName);
    }
    return false;
  }
}
