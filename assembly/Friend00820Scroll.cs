﻿// Decompiled with JetBrains decompiler
// Type: Friend00820Scroll
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Text;
using UnityEngine;

public class Friend00820Scroll : MonoBehaviour
{
  [SerializeField]
  private UIButton IbtnChapter;
  [SerializeField]
  private UILabel TxtChapter;

  public void onFbFeed()
  {
    FaceBookWrapper.Feed(MasterData.InvitationInvitationList[0].link, MasterData.InvitationInvitationList[0].title, " ", MasterData.InvitationInvitationList[0].discription);
  }

  public void onTwitter()
  {
    Application.OpenURL("http://twitter.com/intent/tweet?text=" + WWW.EscapeURL(MasterData.InvitationInvitationList[1].discription));
  }

  public void onLine()
  {
    Application.OpenURL("http://line.naver.jp/R/msg/text/?" + WWW.EscapeURL(MasterData.InvitationInvitationList[2].discription, Encoding.UTF8));
  }

  public void onSms()
  {
  }

  public void onEmail()
  {
    Application.OpenURL(string.Format("mailto:?subject={0}&body={1}", (object) WWW.EscapeURL(MasterData.InvitationInvitationList[4].title, Encoding.UTF8), (object) WWW.EscapeURL(MasterData.InvitationInvitationList[4].discription, Encoding.UTF8)));
  }
}
