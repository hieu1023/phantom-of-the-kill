﻿// Decompiled with JetBrains decompiler
// Type: NGCommon.CameraAspectRatioFitter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

namespace NGCommon
{
  [RequireComponent(typeof (Camera))]
  public class CameraAspectRatioFitter : MonoBehaviour
  {
    private const float TARGET_ASPECT_RATIO = 0.5625f;
    private Camera targetCamera;

    private float? BeforeOrthographicSize { set; get; }

    private float? BeforeFieldOfView { set; get; }

    private Camera TargetCamera
    {
      get
      {
        if ((UnityEngine.Object) this.targetCamera == (UnityEngine.Object) null)
          this.targetCamera = this.GetComponent<Camera>();
        return this.targetCamera;
      }
    }

    private void Awake()
    {
      this.UpdateCamera();
    }

    private void LateUpdate()
    {
      if (this.BeforeOrthographicSize.HasValue && (double) this.BeforeOrthographicSize.Value != (double) this.TargetCamera.orthographicSize)
      {
        this.UpdateCamera();
      }
      else
      {
        if (!this.BeforeFieldOfView.HasValue || (double) this.BeforeFieldOfView.Value == (double) this.TargetCamera.fieldOfView)
          return;
        this.UpdateCamera();
      }
    }

    private void UpdateCamera()
    {
      if (9.0 / 16.0 <= (double) this.TargetCamera.aspect)
        return;
      if (this.TargetCamera.orthographic)
      {
        this.TargetCamera.orthographicSize *= 9f / 16f / this.TargetCamera.aspect;
        this.BeforeOrthographicSize = new float?(this.TargetCamera.orthographicSize);
      }
      else
      {
        this.TargetCamera.fieldOfView = (float) (2.0 * (double) Mathf.Atan(Mathf.Tan((float) ((double) this.TargetCamera.fieldOfView * (Math.PI / 180.0) * 0.5)) * (9f / 16f) / this.TargetCamera.aspect) * 57.2957801818848);
        this.BeforeFieldOfView = new float?(this.TargetCamera.fieldOfView);
      }
    }
  }
}
