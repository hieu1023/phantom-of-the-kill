﻿// Decompiled with JetBrains decompiler
// Type: GachaSEAfterUser
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class GachaSEAfterUser : MonoBehaviour
{
  private int resultRarity;
  private int tundereRank;
  public bool result;

  public void OnUserActionEnd()
  {
    if ((Object) Singleton<NGSoundManager>.GetInstanceOrNull() == (Object) null)
      return;
    Debug.Log((object) "[GACHA] SE 0501 play");
    switch (this.tundereRank)
    {
      case 0:
        Singleton<NGSoundManager>.GetInstance().playSE("SE_0501", false, 0.0f, -1);
        break;
      case 1:
        Singleton<NGSoundManager>.GetInstance().playSE("SE_0513", false, 0.0f, -1);
        break;
      case 2:
        Singleton<NGSoundManager>.GetInstance().playSE("SE_0514", false, 0.0f, -1);
        break;
    }
  }

  public void OnUserActionEnd10()
  {
    if ((Object) Singleton<NGSoundManager>.GetInstanceOrNull() == (Object) null)
      return;
    Debug.Log((object) "[GACHA] SE_0515 play");
    Singleton<NGSoundManager>.GetInstance().playSE("SE_0515", false, 0.0f, -1);
  }

  public void setTundere(int rank)
  {
    this.tundereRank = rank;
  }

  public void setResult(int rarity)
  {
    this.resultRarity = rarity;
  }

  public void OnPlayResult()
  {
    this.result = true;
    Debug.Log((object) "[GACHA] SE 050x play");
    switch (this.resultRarity)
    {
      case 0:
        this.OnReality1();
        break;
      case 1:
        this.OnReality2();
        break;
      case 2:
        this.OnReality3();
        break;
      case 3:
        this.OnReality4();
        break;
      case 4:
        this.OnReality5();
        break;
      case 6:
        this.On10Ren();
        break;
      default:
        this.OnReality5();
        break;
    }
  }

  private void OnReality1()
  {
    if ((Object) Singleton<NGSoundManager>.GetInstanceOrNull() == (Object) null)
      return;
    Singleton<NGSoundManager>.GetInstance().playSE("SE_0502", false, 0.0f, -1);
  }

  private void OnReality2()
  {
    if ((Object) Singleton<NGSoundManager>.GetInstanceOrNull() == (Object) null)
      return;
    Singleton<NGSoundManager>.GetInstance().playSE("SE_0503", false, 0.0f, -1);
  }

  private void OnReality3()
  {
    if ((Object) Singleton<NGSoundManager>.GetInstanceOrNull() == (Object) null)
      return;
    Singleton<NGSoundManager>.GetInstance().playSE("SE_0504", false, 0.0f, -1);
  }

  private void OnReality4()
  {
    if ((Object) Singleton<NGSoundManager>.GetInstanceOrNull() == (Object) null)
      return;
    Singleton<NGSoundManager>.GetInstance().playSE("SE_0505", false, 0.0f, -1);
  }

  private void OnReality5()
  {
    if ((Object) Singleton<NGSoundManager>.GetInstanceOrNull() == (Object) null)
      return;
    Singleton<NGSoundManager>.GetInstance().playSE("SE_0506", false, 0.0f, -1);
  }

  private void On10Ren()
  {
    if ((Object) Singleton<NGSoundManager>.GetInstanceOrNull() == (Object) null)
      return;
    Singleton<NGSoundManager>.GetInstance().playSE("SE_0507", false, 0.0f, -1);
  }
}
