﻿// Decompiled with JetBrains decompiler
// Type: ScheduleEnableWait
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

public class ScheduleEnableWait : Schedule
{
  private float wait;

  public ScheduleEnableWait(float wait)
  {
    this.wait = wait;
    this.isSetBattleEnable = true;
    this.isBattleEnable = false;
  }

  public override bool completedp()
  {
    return (double) this.deltaTime > (double) this.wait;
  }
}
