﻿// Decompiled with JetBrains decompiler
// Type: Shop00714Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System;
using System.Collections;
using UnityEngine;

public class Shop00714Menu : BackButtonMenuBase
{
  private int price;
  private bool success;
  private int nextMax;
  [SerializeField]
  private UILabel txtDescription2;
  [SerializeField]
  private UILabel txtNumber;

  public void Init(int pri, int prev, int next)
  {
    this.price = pri;
    this.nextMax = next;
    this.txtDescription2.SetText(Consts.GetInstance().SHOP_00714_MENU + ":" + prev.ToLocalizeNumberText() + "→[fff000]" + next.ToLocalizeNumberText() + "[-]");
    this.txtNumber.SetTextLocalize(SMManager.Observe<Player>().Value.coin);
  }

  private IEnumerator UnitBoxPlus()
  {
    Future<WebAPI.Response.ShopBuy> paramF = WebAPI.ShopBuy(10000002, 1, (System.Action<WebAPI.Response.UserError>) (e =>
    {
      Singleton<CommonRoot>.GetInstance().loadingMode = 0;
      WebAPI.DefaultUserErrorCallback(e);
      Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
    })).Then<WebAPI.Response.ShopBuy>((Func<WebAPI.Response.ShopBuy, WebAPI.Response.ShopBuy>) (result =>
    {
      Singleton<NGGameDataManager>.GetInstance().Parse(result);
      return result;
    }));
    IEnumerator e1 = paramF.Wait();
    while (e1.MoveNext())
      yield return e1.Current;
    e1 = (IEnumerator) null;
    if (paramF.Result != null)
    {
      e1 = OnDemandDownload.WaitLoadHasUnitResource(false, false);
      while (e1.MoveNext())
        yield return e1.Current;
      e1 = (IEnumerator) null;
      EventTracker.TrackSpend("COIN", "COIN_SHOP_" + (object) 10000002, 1);
      EventTracker.TrackEvent("SHOP", "COIN_SHOP", "ID_" + (object) 10000002, 1);
      Singleton<CommonRoot>.GetInstance().isLoading = false;
      Singleton<CommonRoot>.GetInstance().loadingMode = 0;
      e1 = this.popup00715();
      while (e1.MoveNext())
        yield return e1.Current;
      e1 = (IEnumerator) null;
    }
  }

  public void IbtnPopupYes()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<PopupManager>.GetInstance().dismiss(true);
    if (SMManager.Get<Player>().CheckKiseki(this.price))
      Singleton<PopupManager>.GetInstance().monitorCoroutine(this.UnitBoxPlusAsync());
    else
      Singleton<PopupManager>.GetInstance().monitorCoroutine(PopupUtility._999_3_1(Consts.GetInstance().SHOP_99931_TXT_DESCRIPTION, "", "", Gacha99931Menu.PaymentType.ALL));
  }

  private IEnumerator UnitBoxPlusAsync()
  {
    Singleton<PopupManager>.GetInstance().closeAll(true);
    Singleton<CommonRoot>.GetInstance().loadingMode = 1;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    IEnumerator e = this.UnitBoxPlus();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  private IEnumerator popup00715()
  {
    Future<GameObject> prefab = Res.Prefabs.popup.popup_007_15__anim_popup01.Load<GameObject>();
    IEnumerator e = prefab.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Singleton<PopupManager>.GetInstance().open(prefab.Result, false, false, false, true, false, false, "SE_1006").GetComponent<Shop00715SetText>().SetText(this.nextMax);
  }

  public void IbtnNo()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<PopupManager>.GetInstance().dismiss(false);
  }

  public override void onBackButton()
  {
    this.IbtnNo();
  }
}
