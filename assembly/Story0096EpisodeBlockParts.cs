﻿// Decompiled with JetBrains decompiler
// Type: Story0096EpisodeBlockParts
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;
using UnityEngine;

public class Story0096EpisodeBlockParts : MonoBehaviour
{
  [SerializeField]
  private GameObject[] DirEpisodenum;
  [SerializeField]
  private UIButton IbtnEpisodeBlock;

  public IEnumerator setData(int id)
  {
    foreach (GameObject gameObject in this.DirEpisodenum)
      gameObject.SetActive(false);
    Future<GameObject> Prefab0097F = Res.Prefabs.popup.popup_002_15_2__anim_popup01.Load<GameObject>();
    IEnumerator e = Prefab0097F.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    GameObject Prefab0097 = Prefab0097F.Result;
    this.DirEpisodenum[id].SetActive(true);
    EventDelegate.Set(this.IbtnEpisodeBlock.onClick, (EventDelegate.Callback) (() => Singleton<PopupManager>.GetInstance().openAlert(Prefab0097, false, false, (EventDelegate) null, false, true, false, true)));
  }
}
