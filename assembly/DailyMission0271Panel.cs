﻿// Decompiled with JetBrains decompiler
// Type: DailyMission0271Panel
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using System.Collections;
using UnityEngine;

public class DailyMission0271Panel : MonoBehaviour
{
  [SerializeField]
  public UI2DSprite IconObject;
  [SerializeField]
  public UILabel txtLabel;
  [SerializeField]
  public UILabel txtMissionProgress;
  [SerializeField]
  public GameObject dirClear;
  [SerializeField]
  public GameObject dirPanel;
  [SerializeField]
  public UIButton popupButton;
  [SerializeField]
  public UIButton clearButton;
  private BingoRewardGroup reward;

  public IEnumerator Init(
    DailyMission0271PanelRoot.DailyMissionView viewModel)
  {
    this.reward = viewModel.rewards[0];
    if (viewModel.isClear)
    {
      this.changeClearState();
    }
    else
    {
      this.dirClear.SetActive(false);
      this.dirPanel.SetActive(true);
      this.txtMissionProgress.SetTextLocalize(viewModel.progressText);
    }
    this.txtLabel.SetTextLocalize(viewModel.name);
    IEnumerator e = this.getIconImageAsync();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public void changeClearState()
  {
    this.txtMissionProgress.text = "";
    this.dirClear.SetActive(true);
    this.dirPanel.SetActive(false);
  }

  private IEnumerator getIconImageAsync()
  {
    string path;
    switch (this.reward.reward_type_id)
    {
      case MasterDataTable.CommonRewardType.unit:
      case MasterDataTable.CommonRewardType.material_unit:
        path = "Icons/Unit_Icon";
        break;
      case MasterDataTable.CommonRewardType.gear:
      case MasterDataTable.CommonRewardType.material_gear:
      case MasterDataTable.CommonRewardType.gear_body:
        path = "Icons/Weapon_Icon";
        break;
      case MasterDataTable.CommonRewardType.money:
        path = "Icons/Zeny_Icon";
        break;
      case MasterDataTable.CommonRewardType.coin:
        path = "Icons/Kiseki_Icon";
        break;
      case MasterDataTable.CommonRewardType.friend_point:
        path = "Icons/ManaPoint_Icon";
        break;
      case MasterDataTable.CommonRewardType.battle_medal:
        path = "Icons/BattleMedal_Icon";
        break;
      case MasterDataTable.CommonRewardType.gacha_ticket:
        path = "Icons/GachaTicket_Icon";
        break;
      case MasterDataTable.CommonRewardType.awake_skill:
        path = MasterData.BattleskillSkill[this.reward.reward_id].getSkillIconPath((BattleFuncs.InvestSkill) null);
        break;
      default:
        path = "Icons/Common_Icon";
        break;
    }
    Future<UnityEngine.Sprite> future = Singleton<ResourceManager>.GetInstance().Load<UnityEngine.Sprite>(path, 1f);
    IEnumerator e = future.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.IconObject.sprite2D = future.Result;
  }
}
