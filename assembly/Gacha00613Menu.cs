﻿// Decompiled with JetBrains decompiler
// Type: Gacha00613Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Gacha00613Menu : BackButtonMenuBase
{
  private bool isBtnAction = true;
  private const int ICON_WIDTH = 114;
  private const int ICON_HEIGHT = 136;
  [SerializeField]
  protected UILabel TxtTitle;
  [SerializeField]
  private GameObject dir_bonus;
  [SerializeField]
  private GameObject scrollContainerForBonus;
  [SerializeField]
  private UIScrollView scrollViewForBonus;
  [SerializeField]
  private UIGrid gridForBonus;
  [SerializeField]
  private GameObject dir_next;
  [SerializeField]
  private GameObject dir_ReChallenge;
  [SerializeField]
  private GameObject dir_GachaAgain;
  [SerializeField]
  private GameObject dir_next_ClickForDetail;
  [SerializeField]
  private GameObject dir_next_ClickForDetail_bonus;
  public Gacha00613Scene Scene;
  [SerializeField]
  private UIGrid resultItemGrid;
  [SerializeField]
  private UIScrollView resultItemScrollView;
  private GameObject UnitPrefab;
  private GameObject GearPrefab;
  private GameObject SupplyPrefab;
  private GameObject retryPopupPrefab;
  private int? remainingRetryCount;
  private DateTime? expiredAt;
  private bool isShowBonus;
  private GachaResultData.ResultData.AdditionalItem[] bonusItems;
  private GachaResultData.ResultData resultData;
  public bool isConfirmResult;

  public GameObject RetryPopupPrefab
  {
    get
    {
      return this.retryPopupPrefab;
    }
  }

  public bool IsConfirmResult
  {
    get
    {
      return this.isConfirmResult;
    }
  }

  public bool IsBtnAction
  {
    get
    {
      return this.isBtnAction;
    }
  }

  public void BtnActionEnable(bool enable)
  {
    this.isBtnAction = enable;
    foreach (Transform transform1 in this.resultItemGrid.transform)
    {
      Transform transform2 = transform1.Find("button");
      if ((UnityEngine.Object) transform2 != (UnityEngine.Object) null)
        transform2.gameObject.SetActive(enable);
    }
  }

  public void IbtnBack()
  {
    if (!this.isBtnAction || this.IsPushAndSet())
      return;
    if (this.dir_ReChallenge.activeSelf)
      this.StartCoroutine(this.ShowRetryPopup());
    else if (!Singleton<TutorialRoot>.GetInstance().IsTutorialFinish())
    {
      Singleton<TutorialRoot>.GetInstance().ForceShowAdvice("newchapter_gacha2_tutorial", (System.Action) (() => Singleton<TutorialRoot>.GetInstance().CurrentAdvise()));
    }
    else
    {
      Singleton<CommonRoot>.GetInstance().isLoading = true;
      this.backScene();
    }
  }

  public override void onBackButton()
  {
    this.IbtnBack();
  }

  public IEnumerator onEndSceneAsync()
  {
    foreach (Component component in this.resultItemGrid.transform)
      UnityEngine.Object.Destroy((UnityEngine.Object) component.gameObject);
    this.resultItemGrid.transform.Clear();
    this.dir_bonus.SetActive(false);
    yield break;
  }

  public IEnumerator CreateGetListAsync(GachaResultData.ResultData resultData)
  {
    Gacha00613Menu gacha00613Menu = this;
    gacha00613Menu.remainingRetryCount = resultData.remainingRetryCount;
    gacha00613Menu.expiredAt = resultData.expiredAt;
    IEnumerator e = gacha00613Menu.ResourceLoad();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    int count = 0;
    GachaResultData.Result[] resultArray = resultData.GetResultData();
    for (int index = 0; index < resultArray.Length; ++index)
    {
      GachaResultData.Result result = resultArray[index];
      CommonRewardType crt = new CommonRewardType(result.reward_type_id, result.reward_result_id, result.reward_result_quantity, result.is_new, result.is_reserves);
      e = crt.CreateIcon(gacha00613Menu.resultItemGrid.transform);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      Gacha00613Icon hscroll = crt.GetIcon().AddComponent<Gacha00613Icon>();
      hscroll.Scene = gacha00613Menu.gameObject.GetComponent<Gacha00613Scene>();
      hscroll.Number = count;
      if (crt.isUnit)
      {
        PlayerUnit[] playerUnits = new PlayerUnit[1]
        {
          crt.unit
        };
        e = crt.UnitIconScript.SetPlayerUnit(crt.unit, playerUnits, (PlayerUnit) null, false, false);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        crt.UnitIconScript.setLevelText(crt.unit);
        if ((double) crt.unit.unityTotal >= 1.0)
          crt.UnitIconScript.setUnityValue(Convert.ToInt32(crt.unit.unityTotal));
        crt.UnitIconScript.ShowBottomInfo(UnitSortAndFilter.SORT_TYPES.Level);
        gacha00613Menu.SetEvent(crt.UnitIconScript, (MonoBehaviour) hscroll);
      }
      if (crt.isMaterialUnit)
      {
        PlayerUnit unit = PlayerUnit.CreateByPlayerMaterialUnit(crt.materialUnit, 0);
        PlayerUnit[] playerUnits = new PlayerUnit[1]
        {
          unit
        };
        e = crt.UnitIconScript.SetPlayerUnit(unit, playerUnits, (PlayerUnit) null, false, false);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        crt.UnitIconScript.setLevelText(unit);
        crt.UnitIconScript.ShowBottomInfo(UnitSortAndFilter.SORT_TYPES.Level);
        gacha00613Menu.SetEvent(crt.UnitIconScript, (MonoBehaviour) hscroll);
        unit = (PlayerUnit) null;
      }
      if (crt.isGear)
      {
        e = crt.ItemIconScript.InitByPlayerItem(crt.gear);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        gacha00613Menu.SetEvent(crt.ItemIconScript, (MonoBehaviour) hscroll);
      }
      if (crt.isMaterialGear)
      {
        e = crt.ItemIconScript.InitByPlayerMaterialGear(crt.materialGear);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        crt.ItemIconScript.EnableQuantity(0);
        gacha00613Menu.SetEvent(crt.ItemIconScript, (MonoBehaviour) hscroll);
      }
      ++count;
      crt = (CommonRewardType) null;
      hscroll = (Gacha00613Icon) null;
    }
    resultArray = (GachaResultData.Result[]) null;
    gacha00613Menu.resultItemGrid.Reposition();
    gacha00613Menu.resultItemScrollView.ResetPosition();
    if (resultData.is_retry && Singleton<TutorialRoot>.GetInstance().IsTutorialFinish())
    {
      bool flag = gacha00613Menu.isConfirmResult && (uint) gacha00613Menu.bonusItems.Length > 0U;
      gacha00613Menu.dir_bonus.SetActive(flag);
      gacha00613Menu.dir_next_ClickForDetail.SetActive(!flag);
      gacha00613Menu.dir_next_ClickForDetail_bonus.SetActive(flag);
    }
    else
    {
      bool flag = (uint) resultData.GetAdditionalData().Length > 0U;
      gacha00613Menu.dir_bonus.SetActive(flag);
      gacha00613Menu.dir_next_ClickForDetail.SetActive(!flag);
      gacha00613Menu.dir_next_ClickForDetail_bonus.SetActive(flag);
      if (gacha00613Menu.gridForBonus.transform.childCount <= 0)
      {
        gacha00613Menu.bonusItems = resultData.GetAdditionalData();
        e = gacha00613Menu.SetBonus();
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
      }
    }
  }

  public void SetEvent(UnitIcon UI, MonoBehaviour target)
  {
    UI.Button.onLongPress.Clear();
    UI.Button.onLongPress.Add(new EventDelegate(target, "IbtnIcon"));
    UI.Button.onClick.Clear();
    UI.Button.onClick.Add(new EventDelegate(target, "IbtnIcon"));
  }

  public void SetEvent(ItemIcon II, MonoBehaviour target)
  {
    II.gear.button.onLongPress.Clear();
    II.gear.button.onLongPress.Add(new EventDelegate(target, "IbtnIcon"));
    II.gear.button.onClick.Clear();
    II.gear.button.onClick.Add(new EventDelegate(target, "IbtnIcon"));
  }

  private IEnumerator ResourceLoad()
  {
    Future<GameObject> prefabF;
    IEnumerator e;
    if ((UnityEngine.Object) this.UnitPrefab == (UnityEngine.Object) null)
    {
      prefabF = Singleton<NGGameDataManager>.GetInstance().IsSea ? Res.Prefabs.Sea.UnitIcon.normal_sea.Load<GameObject>() : Res.Prefabs.UnitIcon.normal.Load<GameObject>();
      e = prefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.UnitPrefab = prefabF.Result;
      prefabF = (Future<GameObject>) null;
    }
    if ((UnityEngine.Object) this.GearPrefab == (UnityEngine.Object) null)
    {
      prefabF = Singleton<NGGameDataManager>.GetInstance().IsSea ? new ResourceObject("Prefabs/Sea/ItemIcon/prefab_sea").Load<GameObject>() : Res.Prefabs.ItemIcon.prefab.Load<GameObject>();
      e = prefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.GearPrefab = prefabF.Result;
      prefabF = (Future<GameObject>) null;
    }
    if ((UnityEngine.Object) this.SupplyPrefab == (UnityEngine.Object) null)
    {
      prefabF = Singleton<NGGameDataManager>.GetInstance().IsSea ? new ResourceObject("Prefabs/Sea/ItemIcon/prefab_sea").Load<GameObject>() : Res.Prefabs.ItemIcon.prefab.Load<GameObject>();
      e = prefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.SupplyPrefab = prefabF.Result;
      prefabF = (Future<GameObject>) null;
    }
    if ((UnityEngine.Object) this.retryPopupPrefab == (UnityEngine.Object) null)
    {
      prefabF = new ResourceObject("Prefabs/popup/popup_006_redrawn_gacha_confirm__anim_popup01").Load<GameObject>();
      e = prefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.retryPopupPrefab = prefabF.Result;
      prefabF = (Future<GameObject>) null;
    }
  }

  private IEnumerator SetBonus()
  {
    GameObject unknownUnit = this.CreateUnknownUnit((Transform) null);
    GameObject unknownItem = this.CreateUnknownItem((Transform) null);
    unknownUnit.SetActive(false);
    unknownItem.SetActive(false);
    GachaResultData.ResultData.AdditionalItem[] additionalItemArray = this.bonusItems;
    for (int index = 0; index < additionalItemArray.Length; ++index)
    {
      GachaResultData.ResultData.AdditionalItem bonus_result = additionalItemArray[index];
      GameObject go = new GameObject("bonus");
      CreateIconObject icon = go.AddComponent<CreateIconObject>();
      go.transform.parent = this.gridForBonus.transform;
      IEnumerator e = icon.CreateThumbnail((MasterDataTable.CommonRewardType) bonus_result.reward_type_id, bonus_result.reward_result_id, bonus_result.reward_result_quantity, true, false, new CommonQuestType?(), false);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      go.transform.localScale = Vector3.one;
      switch ((MasterDataTable.CommonRewardType) bonus_result.reward_type_id)
      {
        case MasterDataTable.CommonRewardType.unit:
        case MasterDataTable.CommonRewardType.material_unit:
          bonus_result.unknownObject = unknownUnit.Clone(go.transform);
          bonus_result.gameObject = icon.GetIcon();
          UnitIcon component1 = icon.GetIcon().GetComponent<UnitIcon>();
          if ((UnityEngine.Object) component1 != (UnityEngine.Object) null)
          {
            component1.BottomModeValue = UnitIconBase.BottomMode.Level;
            component1.setLevelText("1");
          }
          Vector3 position1 = bonus_result.unknownObject.transform.position;
          bonus_result.unknownObject.transform.position = new Vector3(position1.x, position1.y + 0.01f, position1.z);
          Vector3 position2 = bonus_result.gameObject.transform.position;
          bonus_result.gameObject.transform.position = new Vector3(position2.x, position2.y + 0.01f, position2.z);
          break;
        default:
          bonus_result.unknownObject = unknownItem.Clone(go.transform);
          bonus_result.gameObject = icon.GetIcon();
          break;
      }
      UIWidget component2 = bonus_result.gameObject.GetComponent<UIWidget>();
      if ((UnityEngine.Object) component2 != (UnityEngine.Object) null)
        component2.alpha = 0.0f;
      bonus_result.unknownObject.SetActive(true);
      bonus_result.gameObject.SetActive(false);
      go = (GameObject) null;
      icon = (CreateIconObject) null;
      bonus_result = (GachaResultData.ResultData.AdditionalItem) null;
    }
    additionalItemArray = (GachaResultData.ResultData.AdditionalItem[]) null;
    this.gridForBonus.Reposition();
    yield return (object) new WaitForSeconds(0.2f);
    this.scrollViewForBonus.ResetPosition();
    UnityEngine.Object.Destroy((UnityEngine.Object) unknownUnit);
    UnityEngine.Object.Destroy((UnityEngine.Object) unknownItem);
  }

  private GameObject CreateUnknownUnit(Transform parent = null)
  {
    GameObject gameObject = this.UnitPrefab.Clone(parent);
    UnitIcon component = gameObject.GetComponent<UnitIcon>();
    component.BottomModeValue = UnitIconBase.BottomMode.Nothing;
    component.BackgroundModeValue = UnitIcon.BackgroundMode.PlayerShadow;
    component.Unknown = true;
    component.NewUnit = false;
    component.transform.Find("icon").gameObject.SetActive(false);
    return gameObject;
  }

  private GameObject CreateUnknownItem(Transform parent = null)
  {
    GameObject gameObject = this.GearPrefab.Clone(parent);
    ItemIcon component = gameObject.GetComponent<ItemIcon>();
    component.SetEmpty(true);
    component.gear.unknown.SetActive(true);
    component.BottomModeValue = ItemIcon.BottomMode.Nothing;
    return gameObject;
  }

  public IEnumerator OpenBonusIcon()
  {
    if (this.bonusItems != null && this.bonusItems.Length != 0 && !this.isShowBonus)
    {
      this.BtnActionEnable(false);
      Singleton<CommonRoot>.GetInstance().SetFooterEnable(false);
      bool isSkip = false;
      GameObject touchObj = this.CreateTouchObject((EventDelegate.Callback) (() => isSkip = true), this.scrollViewForBonus.transform);
      GameObject touchObj2 = this.CreateTouchObject((EventDelegate.Callback) (() => isSkip = true), (Transform) null);
      yield return (object) new WaitForSeconds(1f);
      for (int i = 0; i < this.bonusItems.Length; ++i)
      {
        GachaResultData.ResultData.AdditionalItem bonusItem = this.bonusItems[i];
        TweenAlpha tweenAlpha1 = bonusItem.unknownObject.AddComponent<TweenAlpha>();
        tweenAlpha1.to = 0.0f;
        tweenAlpha1.from = 1f;
        tweenAlpha1.duration = 0.5f;
        bonusItem.gameObject.SetActive(true);
        TweenAlpha tweenAlpha2 = bonusItem.gameObject.AddComponent<TweenAlpha>();
        tweenAlpha2.to = 1f;
        tweenAlpha2.from = 0.0f;
        tweenAlpha2.duration = 0.5f;
        if (!isSkip)
        {
          Singleton<NGSoundManager>.GetInstance().playSE("SE_1021", false, 0.0f, i % 3);
          yield return (object) new WaitForSeconds(0.5f);
        }
      }
      UnityEngine.Object.Destroy((UnityEngine.Object) touchObj);
      UnityEngine.Object.Destroy((UnityEngine.Object) touchObj2);
      this.isShowBonus = true;
    }
  }

  private GameObject CreateTouchObject(EventDelegate.Callback callback, Transform parent = null)
  {
    Resolution currentResolution = Screen.currentResolution;
    GameObject gameObject = new GameObject("touch object");
    gameObject.transform.parent = parent ?? this.transform;
    UIWidget uiWidget = gameObject.AddComponent<UIWidget>();
    uiWidget.depth = 1000;
    uiWidget.width = currentResolution.height;
    uiWidget.height = currentResolution.width;
    BoxCollider boxCollider = gameObject.AddComponent<BoxCollider>();
    boxCollider.isTrigger = true;
    boxCollider.size = new Vector3()
    {
      x = (float) currentResolution.height,
      y = (float) currentResolution.width,
      z = 1f
    };
    UIButton uiButton = gameObject.AddComponent<UIButton>();
    uiButton.tweenTarget = (GameObject) null;
    EventDelegate.Add(uiButton.onClick, callback);
    return gameObject;
  }

  public IEnumerator ShowRetryPopup()
  {
    Gacha00613Menu menu = this;
    menu.dir_next.SetActive(false);
    menu.dir_ReChallenge.SetActive(false);
    GameObject popup = menu.retryPopupPrefab.Clone((Transform) null);
    PopupRedrawnGachaConfirm script = popup.GetComponent<PopupRedrawnGachaConfirm>();
    popup.SetActive(false);
    IEnumerator e;
    if (Singleton<TutorialRoot>.GetInstance().IsTutorialFinish())
    {
      // ISSUE: reference to a compiler-generated method
      // ISSUE: reference to a compiler-generated method
      // ISSUE: reference to a compiler-generated method
      e = script.Initialize(menu, new System.Action(menu.\u003CShowRetryPopup\u003Eb__45_0), new System.Action(menu.\u003CShowRetryPopup\u003Eb__45_1), new System.Action(menu.\u003CShowRetryPopup\u003Eb__45_2), menu.remainingRetryCount, menu.expiredAt);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    else
    {
      // ISSUE: reference to a compiler-generated method
      // ISSUE: reference to a compiler-generated method
      // ISSUE: reference to a compiler-generated method
      e = script.Initialize(menu, new System.Action(menu.\u003CShowRetryPopup\u003Eb__45_3), new System.Action(menu.\u003CShowRetryPopup\u003Eb__45_4), new System.Action(menu.\u003CShowRetryPopup\u003Eb__45_5), new int?(), new DateTime?());
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    Singleton<PopupManager>.GetInstance().open(popup, false, false, true, true, false, false, "SE_1006");
    menu.StartCoroutine(script.UpdateRemainTime());
  }

  private IEnumerator DecideGacha()
  {
    Gacha00613Menu gacha00613Menu = this;
    while (Singleton<PopupManager>.GetInstance().isOpen)
      yield return (object) null;
    int? remainingRetryCount = gacha00613Menu.remainingRetryCount;
    int num = 0;
    if (remainingRetryCount.GetValueOrDefault() == num & remainingRetryCount.HasValue)
    {
      gacha00613Menu.isConfirmResult = true;
    }
    else
    {
      if (gacha00613Menu.expiredAt.HasValue)
      {
        DateTime? expiredAt = gacha00613Menu.expiredAt;
        DateTime dateTime = ServerTime.NowAppTimeAddDelta();
        if ((expiredAt.HasValue ? (expiredAt.GetValueOrDefault() < dateTime ? 1 : 0) : 0) != 0)
        {
          gacha00613Menu.isConfirmResult = true;
          goto label_13;
        }
      }
      bool popup_yes_no_select = false;
      PopupCommonYesNo.Show(Consts.GetInstance().GACHA_DECIDE_CONFIRM_TITLE, Consts.GetInstance().GACHA_DECIDE_CONFIRM_DESCRIPTION, (System.Action) (() =>
      {
        popup_yes_no_select = true;
        this.isConfirmResult = true;
      }), (System.Action) (() =>
      {
        popup_yes_no_select = true;
        this.isConfirmResult = false;
      }));
      while (!popup_yes_no_select)
        yield return (object) null;
    }
label_13:
    if (!gacha00613Menu.isConfirmResult)
    {
      gacha00613Menu.StartCoroutine(gacha00613Menu.ShowRetryPopup());
    }
    else
    {
      Singleton<CommonRoot>.GetInstance().loadingMode = 1;
      Singleton<CommonRoot>.GetInstance().isLoading = true;
      GachaResultData.ResultData data = GachaResultData.GetInstance().GetData();
      int[] array = ((IEnumerable<PlayerUnit>) data.playerUnitReserves).Select<PlayerUnit, int>((Func<PlayerUnit, int>) (x => x._unit)).ToArray<int>();
      bool is_error = false;
      Future<WebAPI.Response.GachaDecide> paramF = WebAPI.GachaDecide(array, data.gachaId, data.gachaName, (System.Action<WebAPI.Response.UserError>) (error =>
      {
        is_error = true;
        this.StartCoroutine(PopupCommon.Show(error.Code, error.Reason, (System.Action) null));
      }));
      IEnumerator e = paramF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      if (!is_error)
      {
        WebAPI.Response.GachaDecide result = paramF.Result;
        if (result != null)
        {
          List<GachaResultData.ResultData.AdditionalItem> additionalItemList = new List<GachaResultData.ResultData.AdditionalItem>();
          WebAPI.Response.GachaDecideAdditional_items[] additionalItems = result.additional_items;
          for (int index = 0; index < additionalItems.Length; ++index)
            additionalItemList.Add(new GachaResultData.ResultData.AdditionalItem()
            {
              reward_result_id = additionalItems[index].reward_id,
              reward_type_id = additionalItems[index].reward_type_id,
              reward_result_quantity = additionalItems[index].reward_quantity
            });
          gacha00613Menu.bonusItems = additionalItemList.ToArray();
          GachaResultData.GetInstance().GetData().unlockQuests = result.unlock_quests;
        }
        if (gacha00613Menu.bonusItems.Length != 0)
        {
          UIWidget bonusUiWidget = gacha00613Menu.scrollContainerForBonus.GetComponent<UIWidget>();
          bonusUiWidget.alpha = 0.0f;
          gacha00613Menu.dir_bonus.SetActive(true);
          gacha00613Menu.dir_next_ClickForDetail.SetActive(false);
          gacha00613Menu.dir_next_ClickForDetail_bonus.SetActive(true);
          e = gacha00613Menu.SetBonus();
          while (e.MoveNext())
            yield return e.Current;
          e = (IEnumerator) null;
          yield return (object) new WaitForEndOfFrame();
          bonusUiWidget.alpha = 1f;
          bonusUiWidget = (UIWidget) null;
        }
        gacha00613Menu.dir_next.SetActive(true);
        Singleton<CommonRoot>.GetInstance().loadingMode = 0;
        Singleton<CommonRoot>.GetInstance().isLoading = false;
        e = gacha00613Menu.Scene.ResultEffect();
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
      }
    }
  }

  public IEnumerator RetryGacha()
  {
    Singleton<CommonRoot>.GetInstance().ShowLoadingLayer(0, true);
    Singleton<CommonRoot>.GetInstance().GetNormalHeaderComponent().GetComponentInChildren<CommonHeaderExp>().OnPress(false);
    yield return (object) new WaitForSeconds(0.1f);
    GachaPlay gacha = GachaPlay.GetInstance();
    GachaResultData.ResultData data = GachaResultData.GetInstance().GetData();
    IEnumerator e = gacha.ChargeGacha(data.gachaName, data.rollCount, data.gachaId, data.gachaType, data.paymentAmount);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if (!gacha.isError)
    {
      Singleton<CommonRoot>.GetInstance().SetFooterEnable(true);
      Singleton<NGSceneManager>.GetInstance().changeScene("gacha006_effect", false, (object[]) Array.Empty<object>());
    }
  }

  public void onConfirmRetry()
  {
    if (this.IsPushAndSet())
      return;
    this.StartCoroutine(this.ShowRetryPopup());
  }

  public void SetRetryBtnActive(bool active)
  {
    this.dir_next.SetActive(!active);
    this.dir_ReChallenge.SetActive(active);
  }

  public void UpdateButtonStatus(GachaResultData.ResultData resultData)
  {
    this.resultData = resultData;
    if (resultData.gachaTicketData != null)
    {
      bool flag = this.CanPlayGachaAgain(resultData);
      this.dir_GachaAgain.SetActive(flag);
      this.dir_next.SetActive(!flag);
    }
    else
    {
      this.dir_GachaAgain.SetActive(false);
      this.dir_next.SetActive(true);
    }
  }

  private bool CanPlayGachaAgain(GachaResultData.ResultData resultData)
  {
    bool flag = false;
    PlayerGachaTicket playerGachaTicket = ((IEnumerable<PlayerGachaTicket>) SMManager.Get<Player>().gacha_tickets).FirstOrDefault<PlayerGachaTicket>((Func<PlayerGachaTicket, bool>) (x => x.ticket_id == resultData.gachaTicketData.gachaData.payment_id.Value));
    if (playerGachaTicket != null && playerGachaTicket.quantity >= resultData.gachaTicketData.gachaData.payment_amount)
      flag = true;
    return flag;
  }

  public void OnClickGachaAgain()
  {
    if (this.IsPushAndSet() || !this.TicketGachaCheckUnit() || !this.GachaCheckItem())
      return;
    this.InitPlayGeneralGachaTicket(this.resultData.gachaTicketData);
  }

  private bool TicketGachaCheckUnit()
  {
    if (!SMManager.Get<Player>().CheckCapMaxUnit())
      return true;
    this.StartCoroutine(PopupUtility._999_5_1());
    return false;
  }

  private bool GachaCheckItem()
  {
    if (!SMManager.Get<Player>().CheckMaxItem())
      return true;
    this.StartCoroutine(PopupUtility._999_6_1(true));
    return false;
  }

  private void InitPlayGeneralGachaTicket(
    GachaResultData.ResultData.GachaTicketData gachaTicketData)
  {
    Popup006SliderSelectMenu menuPopup = Singleton<PopupManager>.GetInstance().open(gachaTicketData.popupPrefab, false, false, false, true, false, false, "SE_1006").GetComponent<Popup006SliderSelectMenu>();
    menuPopup.Init(gachaTicketData.gachaData, (System.Action) (() => this.StartCoroutine(this.PlayTicket(menuPopup.currentPlayTime, gachaTicketData))));
  }

  private IEnumerator PlayTicket(
    int num,
    GachaResultData.ResultData.GachaTicketData gachaTicketData)
  {
    Singleton<PopupManager>.GetInstance().dismiss(false);
    Singleton<CommonRoot>.GetInstance().loadingMode = 4;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    GachaPlay gacha = GachaPlay.GetInstance();
    IEnumerator e = gacha.TicketGacha(gachaTicketData.gachaName, num, gachaTicketData.gachaData, gachaTicketData.popupPrefab);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if (!gacha.isError)
      Singleton<NGSceneManager>.GetInstance().changeScene("gacha006_effect", false, (object[]) Array.Empty<object>());
  }
}
