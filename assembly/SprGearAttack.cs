﻿// Decompiled with JetBrains decompiler
// Type: SprGearAttack
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class SprGearAttack : MonoBehaviour
{
  public UI2DSprite iconSprite;
  private const int KIND_NONE = 1;
  [SerializeField]
  private UnityEngine.Sprite[] icons;

  public void InitGearAttackID(int index)
  {
    this.iconSprite.sprite2D = this.icons[index];
  }

  public void InitGearAttackNone()
  {
    this.InitGearAttackID(1);
  }
}
