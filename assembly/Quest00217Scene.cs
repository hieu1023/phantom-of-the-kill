﻿// Decompiled with JetBrains decompiler
// Type: Quest00217Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System;
using System.Collections;
using UnityEngine;

[AddComponentMenu("Scenes/QuestExtra/TopScene")]
public class Quest00217Scene : NGSceneBase
{
  private static readonly string DefaultSceneName = "quest002_17";
  public Quest00217Menu menu;
  private bool IsInit;
  private bool IsReturn;

  public static void ChangeScene(bool stack)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene(Quest00217Scene.DefaultSceneName, stack, (object[]) Array.Empty<object>());
  }

  public static void ChangeScene(bool stack, int activeTabIndex)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene(Quest00217Scene.DefaultSceneName, (stack ? 1 : 0) != 0, (object) activeTabIndex);
  }

  public static void backOrChangeScene(int activeTabIndex)
  {
    if (Singleton<NGSceneManager>.GetInstance().backScene(Quest00217Scene.DefaultSceneName))
      return;
    Quest00217Scene.ChangeScene(false, activeTabIndex);
  }

  public IEnumerator onStartSceneAsync()
  {
    yield return (object) this.onStartSceneAsync(0);
  }

  public IEnumerator onStartSceneAsync(int activeTabIndex)
  {
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    this.menu.isActive = false;
    yield return (object) null;
    IEnumerator e;
    if (!this.IsInit)
    {
      this.IsInit = true;
      e = this.ProgressExtra(activeTabIndex);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    else
    {
      this.IsReturn = true;
      e = this.menu.UpdateTime();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
  }

  public void onStartScene()
  {
    if (Singleton<TutorialRoot>.GetInstance().isReadHint(this.sceneName, 0))
    {
      Persist.eventStoryPlay.Data.SetReserveList(Singleton<NGGameDataManager>.GetInstance().playbackEventIds, this.sceneName);
      if (!Persist.eventStoryPlay.Data.PlayEventScript(this.sceneName, 0))
        Singleton<CommonRoot>.GetInstance().isLoading = false;
    }
    else
      Singleton<CommonRoot>.GetInstance().isLoading = false;
    this.StartCoroutine(this.waitDisplayMenu());
  }

  public void onStartScene(int activeTabIndex)
  {
    this.onStartScene();
  }

  private IEnumerator ProgressExtra(int activeTabIndex)
  {
    Future<WebAPI.Response.QuestProgressExtra> extra = WebAPI.QuestProgressExtra((System.Action<WebAPI.Response.UserError>) (error =>
    {
      WebAPI.DefaultUserErrorCallback(error);
      Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
    }));
    IEnumerator e = extra.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if (extra.Result != null)
    {
      WebAPI.SetLatestResponsedAt("QuestProgressExtra");
      PlayerExtraQuestS[] ExtraData = SMManager.Get<PlayerExtraQuestS[]>();
      QuestExtraTimetable questExtraTimetable = SMManager.Get<QuestExtraTimetable>();
      e = this.menu.Init(ExtraData, extra.Result.sort_categories, questExtraTimetable.emphasis, questExtraTimetable.notice, extra.Result.player_created_at, activeTabIndex);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
  }

  private IEnumerator waitDisplayMenu()
  {
    if (this.IsReturn)
      yield return (object) new WaitForSeconds(0.1f);
    this.menu.isActive = true;
  }
}
