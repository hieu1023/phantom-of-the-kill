﻿// Decompiled with JetBrains decompiler
// Type: MapEditCameraController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

public class MapEditCameraController : BattleCameraController
{
  public bool initialized_
  {
    get
    {
      return (Object) this.angle != (Object) null;
    }
  }

  private bool isControll_
  {
    get
    {
      return this.enabled && this.initialized_ && this.battleManager.initialized;
    }
  }

  public Camera camera_
  {
    get
    {
      return this.cCamera;
    }
  }

  protected override IEnumerator Start_Original()
  {
    IEnumerator e = base.Start_Original();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }
}
