﻿// Decompiled with JetBrains decompiler
// Type: Shop007231Description
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Shop007231Description : BackButtonMenuBase
{
  [SerializeField]
  private UILabel title;
  [SerializeField]
  private UIScrollView scroll_;
  [SerializeField]
  private UILabel txtDescription_;
  [SerializeField]
  private UILabel txtNames_;
  private SM.SelectTicket ticket_;
  private Dictionary<int, SelectTicketSelectSample> sampleDic;

  public void initialize(SM.SelectTicket ticket)
  {
    this.ticket_ = ticket;
  }

  private IEnumerator Start()
  {
    UIPanel component = this.scroll_.GetComponent<UIPanel>();
    component.ResetAnchors();
    component.Update();
    List<UnitUnit> unitUnitList = (List<UnitUnit>) null;
    if (this.ticket_.category_id == 1)
      unitUnitList = ((IEnumerable<SelectTicketChoices>) this.ticket_.choices).Where<SelectTicketChoices>((Func<SelectTicketChoices, bool>) (uc => MasterData.UnitUnit.ContainsKey(uc.reward_id))).Select<SelectTicketChoices, UnitUnit>((Func<SelectTicketChoices, UnitUnit>) (uc => MasterData.UnitUnit[uc.reward_id])).ToList<UnitUnit>();
    this.txtDescription_.width = (int) component.width;
    if (!string.IsNullOrEmpty(this.ticket_.detail))
    {
      this.txtDescription_.maxLineCount = this.ticket_.detail.Where<char>((Func<char, bool>) (c => c.Equals('\n'))).Count<char>() + 1;
      this.txtDescription_.SetTextLocalize(this.ticket_.detail);
    }
    else
      this.txtDescription_.SetTextLocalize("");
    this.txtNames_.transform.localPosition = (Vector3) new Vector2(this.txtNames_.transform.localPosition.x, (float) -this.txtDescription_.height);
    this.txtNames_.width = (int) component.width;
    Consts instance = Consts.GetInstance();
    string text = "";
    if (this.ticket_.category_id == 1)
    {
      this.title.SetTextLocalize(instance.SHOP_007231_TITLE01);
      this.txtNames_.maxLineCount = unitUnitList.Count + 1;
      text = string.Format(instance.SHOP_007231_DESCRIPTION_INSERT1, this.ticket_.unit_type_selectable ? (object) instance.SHOP_007231_DESCRIPTION_UNITTYPE_SELECT : (object) instance.SHOP_007231_DESCRIPTION_UNITTYPE_NOTSELECT);
      if (this.ticket_.exchange_limit)
      {
        string descriptionFormatUnitCount = instance.SHOP_007231_DESCRIPTION_FORMAT_UNIT_COUNT;
        foreach (UnitUnit unitUnit in unitUnitList)
        {
          UnitUnit u = unitUnit;
          SelectTicketChoices selectTicketChoices = ((IEnumerable<SelectTicketChoices>) this.ticket_.choices).First<SelectTicketChoices>((Func<SelectTicketChoices, bool>) (x => x.reward_id == u.ID));
          text += string.Format(descriptionFormatUnitCount, (object) u.name, (object) (u.rarity.index + 1), (object) selectTicketChoices.exchangeable_count);
        }
      }
      else
      {
        string descriptionFormatUnit = instance.SHOP_007231_DESCRIPTION_FORMAT_UNIT;
        foreach (UnitUnit unitUnit in unitUnitList)
          text += string.Format(descriptionFormatUnit, (object) unitUnit.name, (object) (unitUnit.rarity.index + 1));
      }
    }
    else if (this.ticket_.category_id == 2)
    {
      this.title.SetTextLocalize(instance.SHOP_007231_TITLE02);
      this.txtNames_.maxLineCount = this.ticket_.choices.Length + 1;
      text = instance.SHOP_007231_DESCRIPTION_INSERT2;
      foreach (SelectTicketChoices choice in this.ticket_.choices)
      {
        if (!MasterData.SelectTicketSelectSample[choice.id].deckID.HasValue)
          text = text + "\n " + MasterData.SelectTicketSelectSample[choice.id].reward_title + " × " + (object) MasterData.SelectTicketSelectSample[choice.id].reward_value;
      }
    }
    this.txtNames_.SetTextLocalize(text);
    this.scroll_.ResetPosition();
    yield break;
  }

  public override void onBackButton()
  {
    this.OnIbtnBack();
  }

  public void OnIbtnBack()
  {
    Singleton<PopupManager>.GetInstance().onDismiss();
  }
}
