﻿// Decompiled with JetBrains decompiler
// Type: FieldButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class FieldButton : MonoBehaviour
{
  [SerializeField]
  private GameObject on;
  [SerializeField]
  private GameObject off;
  private FieldButton.State state;

  private void Awake()
  {
    this.on.SetActive(false);
    this.off.SetActive(false);
    this.state = FieldButton.State.none;
  }

  public bool isDown
  {
    get
    {
      return this.state == FieldButton.State.on;
    }
    set
    {
      if (value)
      {
        this.state = FieldButton.State.on;
        this.onDown();
      }
      else
      {
        this.state = FieldButton.State.off;
        this.onUp();
      }
    }
  }

  public bool isActive
  {
    get
    {
      return this.gameObject.activeSelf;
    }
    set
    {
      this.gameObject.SetActive(value);
      if (!value)
        return;
      this.state = FieldButton.State.off;
      this.onUp();
    }
  }

  private void onDown()
  {
    this.on.SetActive(true);
    this.off.SetActive(false);
  }

  private void onUp()
  {
    this.on.SetActive(false);
    this.off.SetActive(true);
  }

  private enum State
  {
    none,
    on,
    off,
  }
}
