﻿// Decompiled with JetBrains decompiler
// Type: GUISelectBox`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class GUISelectBox<T>
{
  private readonly string title;
  private readonly T[] options;
  private readonly string[] labels;
  private Vector2 scrollPosition;
  private bool toggle;
  private int selectedIndex;

  public GUISelectBox(string title_, T[] options_, Func<T, string> toLabel)
  {
    this.title = title_;
    this.toggle = false;
    this.selectedIndex = 0;
    this.options = options_;
    this.labels = ((IEnumerable<T>) this.options).Select<T, string>((Func<T, string>) (x => toLabel(x))).ToArray<string>();
    if (((IEnumerable<T>) this.options).Count<T>() != 0)
      return;
    Debug.LogError((object) "option is empty");
  }

  public T Render(System.Action<T> changeCallback = null)
  {
    return changeCallback == null ? this.RenderWithIndex((System.Action<int, T>) null) : this.RenderWithIndex((System.Action<int, T>) ((_, val) => changeCallback(val)));
  }

  public T RenderWithIndex(System.Action<int, T> changeCallback = null)
  {
    if (this.toggle)
    {
      if (GUILayout.Button("back", GUILayout.Width(120f)))
        this.toggle = false;
      int num1 = Screen.width / 10;
      int num2 = Screen.width - num1;
      this.scrollPosition = GUILayout.BeginScrollView(this.scrollPosition, false, false, GUILayout.Width((float) num2), GUILayout.Height((float) (Screen.height - Screen.height / 10)));
      int num3 = GUILayout.SelectionGrid(this.selectedIndex, this.labels, 1, GUILayout.Width((float) (num2 - num1)));
      GUILayout.EndScrollView();
      if (num3 != this.selectedIndex)
      {
        this.selectedIndex = num3;
        this.toggle = false;
        if (changeCallback != null)
          changeCallback(this.selectedIndex, this.Value());
      }
    }
    else
      this.toggle = GUILayout.Toggle(this.toggle, this.title + " " + this.labels[this.selectedIndex], (GUILayoutOption[]) Array.Empty<GUILayoutOption>());
    return this.Value();
  }

  public T Value()
  {
    return this.options[this.selectedIndex];
  }

  public void setSelected(int i)
  {
    this.selectedIndex = i;
  }

  public void setSelected(Func<T, bool> match)
  {
    for (int index = 0; index < this.options.Length; ++index)
    {
      if (match(this.options[index]))
      {
        this.selectedIndex = index;
        return;
      }
    }
    this.selectedIndex = 0;
  }
}
