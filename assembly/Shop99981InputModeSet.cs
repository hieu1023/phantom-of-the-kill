﻿// Decompiled with JetBrains decompiler
// Type: Shop99981InputModeSet
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Shop99981InputModeSet : MonoBehaviour
{
  [SerializeField]
  protected UIInput nen;
  [SerializeField]
  protected UIInput getsu;
  [SerializeField]
  protected UIInput hi;

  private void Start()
  {
    this.setKeyboardTypeNumber();
  }

  private void Update()
  {
  }

  public void setKeyboardTypeNumber()
  {
    this.nen.keyboardType = UIInput.KeyboardType.NumberPad;
    this.getsu.keyboardType = UIInput.KeyboardType.NumberPad;
    this.hi.keyboardType = UIInput.KeyboardType.NumberPad;
  }
}
