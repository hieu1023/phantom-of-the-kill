﻿// Decompiled with JetBrains decompiler
// Type: PrincessSynthesisSoundEffect
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class PrincessSynthesisSoundEffect : MonoBehaviour
{
  private int resultSuccess;
  public bool result;

  public void OnStartPrincessSynthesis()
  {
    Debug.Log((object) "[TOUGOU] SE 0002 play");
    Singleton<NGSoundManager>.GetInstance().playSE("SE_0002", false, 0.0f, -1);
  }

  public void setResult(int rarity)
  {
    this.resultSuccess = rarity;
  }

  public void OnPlayResult()
  {
    this.result = true;
    Debug.Log((object) "[TOUGOU] SE 000x play");
    switch (this.resultSuccess)
    {
      case 1:
        this.OnSuccess();
        break;
      case 2:
        this.OnGreatSuccess();
        break;
      case 3:
        this.OnFailuer();
        break;
      default:
        this.OnGreatSuccess();
        break;
    }
  }

  private void OnSuccess()
  {
    Singleton<NGSoundManager>.GetInstance().playSE("SE_0003", false, 0.0f, -1);
  }

  private void OnGreatSuccess()
  {
    Singleton<NGSoundManager>.GetInstance().playSE("SE_0004", false, 0.0f, -1);
  }

  private void OnFailuer()
  {
    Singleton<NGSoundManager>.GetInstance().playSE("SE_0005", false, 0.0f, -1);
  }
}
