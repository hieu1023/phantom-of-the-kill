﻿// Decompiled with JetBrains decompiler
// Type: CoinProductExtension
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using Gsc.Auth;
using MasterDataTable;
using System;
using System.Collections.Generic;
using UniLinq;

public static class CoinProductExtension
{
  public static CoinProduct GetActiveProductData(
    this CoinProduct[] self,
    string productId)
  {
    string platform = Device.Platform;
    IEnumerable<CoinProduct> source = ((IEnumerable<CoinProduct>) self).Where<CoinProduct>((Func<CoinProduct, bool>) (x => x.platform == platform));
    if (source.Count<CoinProduct>() == 0)
      source = ((IEnumerable<CoinProduct>) self).Where<CoinProduct>((Func<CoinProduct, bool>) (x => x.platform == "windows"));
    return source.Where<CoinProduct>((Func<CoinProduct, bool>) (x => x.product_id == productId)).First<CoinProduct>();
  }
}
