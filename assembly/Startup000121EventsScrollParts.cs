﻿// Decompiled with JetBrains decompiler
// Type: Startup000121EventsScrollParts
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;
using System.Collections;
using UnityEngine;

public class Startup000121EventsScrollParts : Startup000121ScrollParts
{
  [SerializeField]
  private UI2DSprite sprite;

  public IEnumerator SetDataOld(OfficialInformationArticle info, NGMenuBase menu)
  {
    Startup000121EventsScrollParts eventsScrollParts = this;
    eventsScrollParts.article = info;
    IEnumerator e = Startup000121EventsScrollParts.LoadSprite(eventsScrollParts.sprite, info.title_img_url, 3, new System.Action(eventsScrollParts.SetSprite));
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public override void SetData(OfficialInformationArticle info, NGMenuBase menu)
  {
    this.article = info;
    ((MonoBehaviour) menu).StartCoroutine(Startup000121EventsScrollParts.LoadSprite(this.sprite, info.title_img_url, 3, new System.Action(this.SetSprite)));
  }

  private void SetSprite()
  {
    this.sprite.SetDimensions(this.sprite.mainTexture.width, this.sprite.mainTexture.height);
    this.sprite.GetComponent<BoxCollider>().size = new Vector3((float) this.sprite.mainTexture.width, (float) this.sprite.mainTexture.height, 0.0f);
    this.newSprite.transform.localPosition = new Vector3((float) (-(this.sprite.mainTexture.width / 2) + this.newSprite.GetComponent<UISprite>().width / 2 + 15), (float) (this.sprite.mainTexture.height / 2 + 14), this.newSprite.transform.localPosition.z);
  }

  private static IEnumerator LoadSprite(
    UI2DSprite sprite,
    string str,
    int count = 3,
    System.Action action = null)
  {
    for (int i = 0; i < count; ++i)
    {
      WWW www = new WWW(str);
      while ((double) www.progress == 0.0)
        yield return (object) null;
      yield return (object) www;
      if (string.IsNullOrEmpty(www.error))
      {
        sprite.sprite2D = UnityEngine.Sprite.Create(www.texture, new Rect(0.0f, 0.0f, (float) www.texture.width, (float) www.texture.height), new Vector2(0.0f, 0.0f), 1f, 100U, SpriteMeshType.FullRect);
        if (action == null)
        {
          yield break;
        }
        else
        {
          action();
          yield break;
        }
      }
      else
      {
        Debug.LogError((object) www.error);
        www = (WWW) null;
      }
    }
    sprite.sprite2D = Resources.Load<UnityEngine.Sprite>("Sprites/1x1_black");
  }
}
