﻿// Decompiled with JetBrains decompiler
// Type: StageAvailibilityCheckHelper
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class StageAvailibilityCheckHelper : MonoBehaviour
{
  public IEnumerator PopupJudge(
    QuestConverterData StageData,
    System.Action act,
    bool Event,
    bool storyOnly)
  {
    StageAvailibilityCheckHelper availibilityCheckHelper = this;
    Player player1 = SMManager.Get<Player>();
    IEnumerator e;
    if (player1.CheckMaxUnit() || player1.CheckLimitOverMaxUnitReserves())
    {
      e = PopupUtility._999_5_1();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    else if (player1.CheckMaxItem())
    {
      e = PopupUtility._999_6_1(true);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    else if (player1.CheckMaxReisou())
    {
      e = PopupUtility.popupMaxReisou();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    else
    {
      if (storyOnly)
      {
        Player player2 = SMManager.Get<Player>();
        PlayerDeck playerDeck = (StageData.type == CommonQuestType.Sea ? PlayerSeaDeck.convertDeckData(SMManager.Get<PlayerSeaDeck[]>()) : SMManager.Get<PlayerDeck[]>())[StageData.type == CommonQuestType.Sea ? Persist.seaDeckOrganized.Data.number : Persist.deckOrganized.Data.number];
        int num = Math.Max(player2.max_cost, playerDeck.cost_limit);
        if (playerDeck.cost > num)
        {
          availibilityCheckHelper.StartCoroutine(PopupCommon.Show(Consts.GetInstance().QUEST_0028_SORTIE_TITLE, Consts.GetInstance().QUEST_0028_COST_OVER_DESCRIPTION, (System.Action) null));
          yield break;
        }
      }
      if (Event)
        availibilityCheckHelper.StartCoroutine(availibilityCheckHelper.QuestTimeCompare(StageData, act, storyOnly));
      else if (player1.ap - StageData.lost_ap < 0)
      {
        if (player1.ap_max - StageData.lost_ap < 0)
        {
          e = availibilityCheckHelper.maxAPshortage_Popoup();
          while (e.MoveNext())
            yield return e.Current;
          e = (IEnumerator) null;
        }
        else
        {
          e = availibilityCheckHelper.noAP_Popup((System.Action) (() =>
          {
            act();
            if (StageData.type == CommonQuestType.Story)
            {
              PlayerStoryQuestS story_quest = ((IEnumerable<PlayerStoryQuestS>) SMManager.Get<PlayerStoryQuestS[]>()).Where<PlayerStoryQuestS>((Func<PlayerStoryQuestS, bool>) (x => x.quest_story_s.ID == StageData.id_S)).FirstOrDefault<PlayerStoryQuestS>();
              if (story_quest == null || !MasterData.QuestStoryS.ContainsKey(StageData.wave == null ? StageData.id_S : StageData.wave.first_quest_s_id))
                return;
              Quest0028Scene.changeScene(true, story_quest, storyOnly);
            }
            else
            {
              if (StageData.type != CommonQuestType.Sea)
                return;
              PlayerSeaQuestS sea_quest = ((IEnumerable<PlayerSeaQuestS>) SMManager.Get<PlayerSeaQuestS[]>()).Where<PlayerSeaQuestS>((Func<PlayerSeaQuestS, bool>) (x => x.quest_sea_s.ID == StageData.id_S)).FirstOrDefault<PlayerSeaQuestS>();
              if (sea_quest == null || !MasterData.QuestSeaS.ContainsKey(StageData.wave == null ? StageData.id_S : StageData.wave.first_quest_s_id))
                return;
              Quest0028Scene.changeScene(true, sea_quest, storyOnly);
            }
          }));
          while (e.MoveNext())
            yield return e.Current;
          e = (IEnumerator) null;
        }
      }
      else if (player1.ap - StageData.lost_ap >= 0)
      {
        act();
        if (StageData.type == CommonQuestType.Story)
        {
          PlayerStoryQuestS story_quest = ((IEnumerable<PlayerStoryQuestS>) SMManager.Get<PlayerStoryQuestS[]>()).Where<PlayerStoryQuestS>((Func<PlayerStoryQuestS, bool>) (x => x.quest_story_s.ID == StageData.id_S)).FirstOrDefault<PlayerStoryQuestS>();
          if (story_quest != null && MasterData.QuestStoryS.ContainsKey(StageData.wave == null ? StageData.id_S : StageData.wave.first_quest_s_id))
            Quest0028Scene.changeScene(true, story_quest, storyOnly);
        }
        else if (StageData.type == CommonQuestType.Sea)
        {
          PlayerSeaQuestS sea_quest = ((IEnumerable<PlayerSeaQuestS>) SMManager.Get<PlayerSeaQuestS[]>()).Where<PlayerSeaQuestS>((Func<PlayerSeaQuestS, bool>) (x => x.quest_sea_s.ID == StageData.id_S)).FirstOrDefault<PlayerSeaQuestS>();
          if (sea_quest != null && MasterData.QuestSeaS.ContainsKey(StageData.wave == null ? StageData.id_S : StageData.wave.first_quest_s_id))
            Quest0028Scene.changeScene(true, sea_quest, storyOnly);
        }
      }
    }
  }

  public IEnumerator noAP_Popup(System.Action questChangeScene)
  {
    Future<GameObject> ap_popup = Res.Prefabs.popup.popup_002_7__anim_popup01.Load<GameObject>();
    IEnumerator e = ap_popup.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Singleton<PopupManager>.GetInstance().open(ap_popup.Result, false, false, false, true, false, false, "SE_1006").GetComponent<ApPopup0027>().SetBtnAct(questChangeScene);
  }

  private IEnumerator maxAPshortage_Popoup()
  {
    Future<GameObject> popupF = Res.Prefabs.popup.popup_002_2_ap1__anim_popup01.Load<GameObject>();
    IEnumerator e = popupF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Singleton<PopupManager>.GetInstance().open(popupF.Result, false, false, false, true, false, false, "SE_1006");
  }

  public IEnumerator QuestTimeCompare(
    QuestConverterData StageData,
    System.Action act,
    bool storyOnly)
  {
    IEnumerator e = ServerTime.WaitSync();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    PlayerExtraQuestS quest = ((IEnumerable<PlayerExtraQuestS>) ((IEnumerable<PlayerExtraQuestS>) SMManager.Get<PlayerExtraQuestS[]>()).CheckMasterData().ToArray<PlayerExtraQuestS>()).Where<PlayerExtraQuestS>((Func<PlayerExtraQuestS, bool>) (x => x.quest_extra_s != null && x.quest_extra_s.ID == StageData.id_S)).FirstOrDefault<PlayerExtraQuestS>();
    bool flag = false;
    if (quest != null)
    {
      flag = ServerTime.NowAppTime() < quest.today_day_end_at;
      PlayerQuestGate playerQuestGate = quest.GetPlayerQuestGate();
      if (playerQuestGate != null)
      {
        DateTime dateTime = ServerTime.NowAppTime();
        DateTime? endAt = playerQuestGate.end_at;
        flag = endAt.HasValue && dateTime < endAt.GetValueOrDefault();
      }
    }
    if (flag)
    {
      if (SMManager.Get<Player>().ap - StageData.lost_ap < 0)
      {
        if (SMManager.Get<Player>().ap_max - StageData.lost_ap < 0)
        {
          e = this.maxAPshortage_Popoup();
          while (e.MoveNext())
            yield return e.Current;
          e = (IEnumerator) null;
        }
        else
        {
          e = this.noAP_Popup((System.Action) (() =>
          {
            if (!MasterData.QuestExtraS.ContainsKey(StageData.wave == null ? StageData.id_S : StageData.wave.first_quest_s_id))
              return;
            act();
            Quest0028Scene.changeScene(true, quest, storyOnly);
          }));
          while (e.MoveNext())
            yield return e.Current;
          e = (IEnumerator) null;
        }
      }
      else if (SMManager.Get<Player>().ap - StageData.lost_ap >= 0)
      {
        act();
        BattleStage stage = MasterData.QuestExtraS[StageData.wave == null ? StageData.id_S : StageData.wave.first_quest_s_id].stage;
        Quest0028Scene.changeScene(true, quest, storyOnly);
      }
    }
    else
    {
      Future<GameObject> time_popup = Res.Prefabs.popup.popup_002_23__anim_popup01.Load<GameObject>();
      e = time_popup.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      Singleton<PopupManager>.GetInstance().openAlert(time_popup.Result, false, false, (EventDelegate) null, false, true, false, true);
      time_popup = (Future<GameObject>) null;
    }
  }
}
