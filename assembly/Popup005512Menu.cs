﻿// Decompiled with JetBrains decompiler
// Type: Popup005512Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;
using UnityEngine;

public class Popup005512Menu : BackButtonMonoBehaiviour
{
  [SerializeField]
  private UILabel description;

  public void IbtnOk()
  {
    Singleton<PopupManager>.GetInstance().onDismiss();
  }

  public override void onBackButton()
  {
    this.IbtnOk();
  }

  public void SetTextWithMoney(long money)
  {
    this.description.SetText(Consts.Format(Consts.GetInstance().popup_005_5_12_text, (IDictionary) new Hashtable()
    {
      {
        (object) nameof (money),
        (object) money.ToLocalizeNumberText()
      }
    }));
  }
}
