﻿// Decompiled with JetBrains decompiler
// Type: Versus026DirWinLossRecordsDetailsScene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

public class Versus026DirWinLossRecordsDetailsScene : NGSceneBase
{
  [SerializeField]
  private Versus026DirWinLossRecordsDetailsMenu menu;

  public static void ChangeScene(
    bool stack,
    Versus02613Scene.BootParam param,
    string battleID,
    string playerID)
  {
    Versus02613Scene.BootArgument bootArgument = new Versus02613Scene.BootArgument("versus026_win_loss_records_details", param.current, battleID, playerID);
    param.push(bootArgument);
    Singleton<NGSceneManager>.GetInstance().changeScene(bootArgument.scene, (stack ? 1 : 0) != 0, (object) param);
  }

  public IEnumerator onStartSceneAsync(Versus02613Scene.BootParam param)
  {
    IEnumerator e = this.menu.Init(param);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }
}
