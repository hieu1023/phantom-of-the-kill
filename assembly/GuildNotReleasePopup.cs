﻿// Decompiled with JetBrains decompiler
// Type: GuildNotReleasePopup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;
using UnityEngine;

public class GuildNotReleasePopup : BackButtonMenuBase
{
  [SerializeField]
  private UILabel lblTitle;
  [SerializeField]
  private UILabel lblDesc;

  public void Initialize()
  {
    if ((Object) this.GetComponent<UIWidget>() != (Object) null)
      this.GetComponent<UIWidget>().alpha = 0.0f;
    this.lblTitle.SetTextLocalize(Consts.Format(Consts.GetInstance().GUILD_COMING_SOON_TITLE, (IDictionary) null));
    this.lblDesc.SetTextLocalize(Consts.Format(Consts.GetInstance().GUILD_COMING_SOON_DESC, (IDictionary) null));
  }

  public override void onBackButton()
  {
    Singleton<PopupManager>.GetInstance().dismiss(false);
  }
}
