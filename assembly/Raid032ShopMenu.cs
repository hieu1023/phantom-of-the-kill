﻿// Decompiled with JetBrains decompiler
// Type: Raid032ShopMenu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System.Collections;
using UnityEngine;

public class Raid032ShopMenu : ShopArticleListMenu
{
  [SerializeField]
  private UILabel lblMedalNum;
  [SerializeField]
  private UI2DSprite spriteMedal;
  private UnityEngine.Sprite medalIcon;

  public override IEnumerator Init(Future<GameObject> cellPrefab)
  {
    this.lblMedalNum.SetTextLocalize(SMManager.Get<Player>().raid_medal);
    IEnumerator e;
    if ((Object) this.medalIcon == (Object) null)
    {
      Future<UnityEngine.Sprite> ft = new ResourceObject("Icons/RaidJuel_Icon").Load<UnityEngine.Sprite>();
      e = ft.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      if ((Object) ft.Result != (Object) null)
        this.spriteMedal.sprite2D = ft.Result;
      this.medalIcon = ft.Result;
      ft = (Future<UnityEngine.Sprite>) null;
    }
    e = base.Init(cellPrefab);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  protected override IEnumerator LoadDetailPopup()
  {
    Raid032ShopMenu raid032ShopMenu = this;
    // ISSUE: reference to a compiler-generated method
    yield return (object) raid032ShopMenu.\u003C\u003En__1();
    Future<GameObject> prefabF;
    IEnumerator e;
    if ((Object) raid032ShopMenu.mapDetailPopup == (Object) null)
    {
      prefabF = new ResourceObject("Prefabs/popup/popup_031_map_detail__anim_popup01").Load<GameObject>();
      e = prefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      raid032ShopMenu.mapDetailPopup = prefabF.Result;
      prefabF = (Future<GameObject>) null;
    }
    if ((Object) raid032ShopMenu.facilityDetailPopup == (Object) null)
    {
      prefabF = new ResourceObject("Prefabs/popup/popup_031_facility_detail__anim_popup01").Load<GameObject>();
      e = prefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      raid032ShopMenu.facilityDetailPopup = prefabF.Result;
      prefabF = (Future<GameObject>) null;
    }
  }

  public override void onBackButton()
  {
    if (this.IsPushAndSet())
      return;
    this.backScene();
  }
}
