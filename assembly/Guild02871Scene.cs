﻿// Decompiled with JetBrains decompiler
// Type: Guild02871Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using UnityEngine;

public class Guild02871Scene : NGSceneBase
{
  [SerializeField]
  private Guild02871Menu menu;

  public static void ChangeScene(bool stack)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("guild028_7_1", stack, (object[]) Array.Empty<object>());
  }

  public IEnumerator onStartSceneAsync()
  {
    IEnumerator e = this.menu.InitializeAsync();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public void onStartScene()
  {
    this.menu.Initialize();
  }

  public override void onEndScene()
  {
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    DetailController.Release();
  }
}
