﻿// Decompiled with JetBrains decompiler
// Type: TutorialStoryPage
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

public class TutorialStoryPage : TutorialPageBase
{
  [SerializeField]
  private int scriptId;

  public override IEnumerator Show()
  {
    // ISSUE: reference to a compiler-generated field
    int num = this.\u003C\u003E1__state;
    TutorialStoryPage tutorialStoryPage = this;
    if (num != 0)
      return false;
    // ISSUE: reference to a compiler-generated field
    this.\u003C\u003E1__state = -1;
    // ISSUE: reference to a compiler-generated method
    tutorialStoryPage.StartCoroutine(tutorialStoryPage.\u003C\u003En__0());
    Story0093Scene.changeScene009_3(false, tutorialStoryPage.scriptId);
    tutorialStoryPage.StartCoroutine(tutorialStoryPage.observeStoryEnd());
    return false;
  }

  private IEnumerator observeStoryEnd()
  {
    TutorialStoryPage tutorialStoryPage = this;
    while (Singleton<NGSceneManager>.GetInstance().changeSceneQueueCount > 0)
      yield return (object) null;
    while (!EmptyScene.IsActive)
      yield return (object) null;
    tutorialStoryPage.NextPage();
  }
}
