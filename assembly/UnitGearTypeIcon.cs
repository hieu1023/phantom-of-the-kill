﻿// Decompiled with JetBrains decompiler
// Type: UnitGearTypeIcon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using System;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class UnitGearTypeIcon : BattleMonoBehaviour
{
  [SerializeField]
  protected UnitGearTypeIcon.ElementIcons[] gears;

  public void setUnit(BL.Unit unit)
  {
    ((IEnumerable<UnitGearTypeIcon.ElementIcons>) this.gears).ForEach<UnitGearTypeIcon.ElementIcons>((System.Action<UnitGearTypeIcon.ElementIcons>) (x => x.parent.SetActive(false)));
    UnitGearTypeIcon.ElementIcons gear = this.gears[unit.unit.kind.ID - 1];
    gear.parent.SetActive(true);
    int num = 1;
    BattleskillEffect battleskillEffect = ((IEnumerable<BL.Skill>) unit.duelSkills).SelectMany<BL.Skill, BattleskillEffect>((Func<BL.Skill, IEnumerable<BattleskillEffect>>) (x => (IEnumerable<BattleskillEffect>) x.skill.Effects)).Where<BattleskillEffect>((Func<BattleskillEffect, bool>) (x => x.effect_logic.Enum == BattleskillEffectLogicEnum.invest_element)).FirstOrDefault<BattleskillEffect>();
    if (battleskillEffect != null)
      num = (int) battleskillEffect.skill.element;
    ((IEnumerable<GameObject>) gear.icons).ForEach<GameObject>((System.Action<GameObject>) (x => x.SetActive(false)));
    gear.icons[num - 1].SetActive(true);
  }

  [Serializable]
  public struct ElementIcons
  {
    public GameObject parent;
    public GameObject[] icons;
  }
}
