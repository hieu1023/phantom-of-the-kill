﻿// Decompiled with JetBrains decompiler
// Type: Friend008161Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;
using UnityEngine;

public class Friend008161Menu : BackButtonMenuBase
{
  [SerializeField]
  private UILabel txtTitle;
  [SerializeField]
  private UILabel txtDescription;

  public IEnumerator Init(string title = "", string description = "")
  {
    if (string.IsNullOrEmpty(title))
      title = Consts.GetInstance().POPUP_0186_COPY_POPUP_TITLE;
    if (string.IsNullOrEmpty(description))
      description = Consts.GetInstance().POPUP_0186_COPY_POPUP_DESCRIPTION;
    this.txtTitle.SetText(title);
    this.txtDescription.SetText(description);
    yield break;
  }

  public virtual void IbtnOk()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<PopupManager>.GetInstance().onDismiss();
  }

  public void IbtnNo()
  {
    this.IbtnOk();
  }

  public override void onBackButton()
  {
    this.IbtnNo();
  }
}
