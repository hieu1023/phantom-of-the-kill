﻿// Decompiled with JetBrains decompiler
// Type: StoryBlock
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore.LispCore;

public class StoryBlock
{
  public ScriptBlock script = new ScriptBlock();
  public TextBlock text = new TextBlock();
  public bool next_enable = true;
  public string label;
  public SelectBlock select;

  public void addScript(string func, Cons args)
  {
    this.script.add(func, args);
  }

  public void addScriptBody(Cons body)
  {
    this.script.add(body);
  }

  public void setText(string t)
  {
    this.text.setText(t);
  }

  public void addText(string t)
  {
    this.text.addText(t);
  }

  public string getText()
  {
    return this.text.text;
  }

  public void setSelect(SelectBlock sb)
  {
    this.select = sb;
  }

  public void setSelectIndex(int index)
  {
    if (this.select == null)
      return;
    this.select.selected = index;
  }

  public void eval(Lisp engine)
  {
    this.script.eval(engine);
  }
}
