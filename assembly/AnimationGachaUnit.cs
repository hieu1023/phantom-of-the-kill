﻿// Decompiled with JetBrains decompiler
// Type: AnimationGachaUnit
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class AnimationGachaUnit : MonoBehaviour
{
  public float Interval = 0.03f;
  public EffectControllerGacha gachaCon;
  public MeshRenderer intervalMeshRenderer;
  public MeshRenderer finishMeshRenderer;
  private float nowTime;

  private void FixedUpdate()
  {
    this.nowTime += Time.fixedDeltaTime;
    if ((double) this.nowTime < (double) this.Interval)
      return;
    this.setRandomUnit();
    this.nowTime %= this.Interval;
  }

  private void setRandomUnit()
  {
  }
}
