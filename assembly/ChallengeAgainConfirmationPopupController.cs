﻿// Decompiled with JetBrains decompiler
// Type: ChallengeAgainConfirmationPopupController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

public class ChallengeAgainConfirmationPopupController : MonoBehaviour
{
  private System.Action dismissAction;
  private System.Action challengeAction;

  public IEnumerator Init(System.Action dismissAction, System.Action challengeAction)
  {
    this.challengeAction = challengeAction;
    this.dismissAction = dismissAction;
    yield return (object) null;
  }

  public void OnChanllenge()
  {
    this.challengeAction();
    Singleton<PopupManager>.GetInstance().dismiss(false);
  }

  public void OnReturn()
  {
    this.dismissAction();
    Singleton<PopupManager>.GetInstance().dismiss(false);
  }
}
