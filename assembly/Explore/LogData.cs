﻿// Decompiled with JetBrains decompiler
// Type: Explore.LogData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Explore
{
  public class LogData
  {
    public string Message { get; private set; }

    public Color Color { get; private set; }

    public LogData(string message, Color color)
    {
      this.Message = message;
      this.Color = color;
    }
  }
}
