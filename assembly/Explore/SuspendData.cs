﻿// Decompiled with JetBrains decompiler
// Type: Explore.SuspendData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

namespace Explore
{
  public class SuspendData
  {
    public string RandCnt = "0";
    public int[] DefeatEnemyIds;
    public int[] DefeatEnemyCounts;
    public int EnemyId;
    public int EnemyDamage;
    public int State;
    public int Rest;
    public int TakeOver;
  }
}
