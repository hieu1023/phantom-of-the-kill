﻿// Decompiled with JetBrains decompiler
// Type: Explore.LoginReportInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;
using System;
using System.Collections.Generic;

namespace Explore
{
  public class LoginReportInfo
  {
    public TimeSpan CalcTimeSpan { get; private set; } = new TimeSpan(0L);

    public int DefeatEnemyCount { get; private set; }

    public int PlayerExp { get; private set; }

    public int Zeny { get; private set; }

    public List<int> RewardsId { get; private set; } = new List<int>();

    public int AfterPlayerLevel { get; private set; }

    public int BeforePlayerLevel { get; private set; }

    public int BeforePlayerApMax { get; private set; }

    public int BeforePlayerMaxCost { get; private set; }

    public int BeforePlayerMaxFriend { get; private set; }

    public float ExpGaugeStartValue { get; private set; }

    public float ExpGaugeFinishValue { get; private set; }

    public int ExpGaugeLoopNum
    {
      get
      {
        return this.AfterPlayerLevel - this.BeforePlayerLevel;
      }
    }

    public void SetReport(int defeatCount, int playerExp, int zeny)
    {
      this.DefeatEnemyCount = defeatCount;
      this.PlayerExp = playerExp;
      this.Zeny = zeny;
    }

    public void AddReport(TimeSpan timeSpan, int[] rewardsId)
    {
      this.CalcTimeSpan += timeSpan;
      this.RewardsId.AddRange((IEnumerable<int>) rewardsId);
    }

    public void SetBeforePlayerStatus()
    {
      Player player = SMManager.Get<Player>();
      this.BeforePlayerLevel = player.level;
      this.ExpGaugeStartValue = (float) player.exp / (float) (player.exp + player.exp_next);
      this.BeforePlayerApMax = player.ap_max;
      this.BeforePlayerMaxCost = player.max_cost;
      this.BeforePlayerMaxFriend = player.max_friends;
    }

    public void SetAfterPlayerStatus()
    {
      Player player = SMManager.Get<Player>();
      this.AfterPlayerLevel = player.level;
      this.ExpGaugeFinishValue = (float) player.exp / (float) (player.exp + player.exp_next);
    }
  }
}
