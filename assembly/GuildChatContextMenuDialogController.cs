﻿// Decompiled with JetBrains decompiler
// Type: GuildChatContextMenuDialogController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using UnityEngine;

public class GuildChatContextMenuDialogController : MonoBehaviour
{
  private GuildChatMessageData originalData;
  [SerializeField]
  private UILabel playerNameLabel;
  [SerializeField]
  private UILabel customizedButtonLabel;

  private void Start()
  {
  }

  private void Update()
  {
  }

  public void Initialize(GuildChatMessageData data)
  {
    this.originalData = data;
    if ((Object) this.playerNameLabel != (Object) null)
      this.playerNameLabel.SetTextLocalize(this.originalData.senderName);
    if (!((Object) this.customizedButtonLabel != (Object) null))
      return;
    string sceneName = this.originalData.transition.scene_name;
    if (sceneName == "dlg_guild_bbs")
      this.customizedButtonLabel.SetTextLocalize(Consts.GetInstance().GUILD_CHAT_CUSTOMIZED_BUTTON_TITLE_BBS);
    else if (sceneName == "guild028_4")
      this.customizedButtonLabel.SetTextLocalize(Consts.GetInstance().GUILD_CHAT_CUSTOMIZED_BUTTON_TITLE_GUILD_TITLE);
    else if (sceneName == "guild028_6")
      this.customizedButtonLabel.SetTextLocalize(Consts.GetInstance().GUILD_CHAT_CUSTOMIZED_BUTTON_TITLE_GIFT);
    else if (sceneName == "guild028_2")
    {
      this.customizedButtonLabel.SetTextLocalize(Consts.GetInstance().GUILD_CHAT_CUSTOMIZED_BUTTON_TITLE_GUILD_MAP);
    }
    else
    {
      if (!(sceneName == "guild028_1"))
        return;
      this.customizedButtonLabel.SetTextLocalize(Consts.GetInstance().GUILD_CHAT_CUSTOMIZED_BUTTON_TITLE_GUILD_HQ);
    }
  }

  public void OnCloseButtonClicked()
  {
    Singleton<PopupManager>.GetInstance().dismiss(false);
  }

  public void OnMemberDetailButtonClicked()
  {
    Singleton<PopupManager>.GetInstance().dismiss(false);
    Singleton<CommonRoot>.GetInstance().guildChatManager.CloseDetailedChat();
    Guild0282Scene.ChangeSceneOrMemberFocus(this.originalData.membership, (Guild0282Menu) null);
  }

  public void OnCustomizedButtonClicked()
  {
    Singleton<PopupManager>.GetInstance().dismiss(false);
    string sceneName = this.originalData.transition.scene_name;
    if (sceneName.StartsWith("dlg"))
    {
      if (!(sceneName == "dlg_guild_bbs"))
        return;
      Singleton<CommonRoot>.GetInstance().guildChatManager.OpenBBSViewerDialog();
    }
    else if (sceneName == "guild028_4")
      Guild0284Scene.ChangeScene();
    else if (sceneName == "guild028_6")
      Guild0286Scene.ChangeScene(true);
    else if (sceneName == "guild028_2")
    {
      Singleton<CommonRoot>.GetInstance().guildChatManager.CloseDetailedChat();
      Guild0282Scene.ChangeSceneOrMemberFocus(this.originalData.membership, (Guild0282Menu) null);
    }
    else
    {
      if (!(sceneName == "guild028_1"))
        return;
      Guild0281Menu component = Singleton<NGSceneManager>.GetInstance().sceneBase.GetComponent<Guild0281Menu>();
      Guild0281Scene.ChangeSceneHQTop(false, component);
      if (!((Object) component != (Object) null))
        return;
      Singleton<CommonRoot>.GetInstance().guildChatManager.CloseDetailedChat();
    }
  }
}
