﻿// Decompiled with JetBrains decompiler
// Type: AppSetup.PopupConfirm
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace AppSetup
{
  public class PopupConfirm : MonoBehaviour
  {
    [SerializeField]
    private UIButton uiButton;
    [SerializeField]
    private UILabel label;

    public bool IsDecide { get; private set; }

    private void Start()
    {
      EventDelegate.Set(this.uiButton.onClick, (EventDelegate.Callback) (() => this.OnOK()));
      this.IsDecide = false;
    }

    public void SelectText(string text)
    {
      this.label.text = text;
    }

    public void OnOK()
    {
      this.IsDecide = true;
    }
  }
}
