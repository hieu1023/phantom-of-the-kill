﻿// Decompiled with JetBrains decompiler
// Type: AppSetup.AppSetupFPS
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace AppSetup
{
  public class AppSetupFPS
  {
    public static void SetDefault()
    {
      AppSetupFPS.SetMaxFPS(Persist.appFPS.Data.MaxFPS);
    }

    public static void SetMaxFPS(int fps)
    {
      QualitySettings.vSyncCount = 0;
      Application.targetFrameRate = fps;
    }

    public static int GetMaxFPS()
    {
      return Application.targetFrameRate;
    }
  }
}
