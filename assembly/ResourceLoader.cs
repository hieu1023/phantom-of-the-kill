﻿// Decompiled with JetBrains decompiler
// Type: ResourceLoader
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System;
using System.Collections;
using UnityEngine;

internal class ResourceLoader : IResourceLoader
{
  private IEnumerator Load_(string path, Promise<UnityEngine.Object> promise)
  {
    promise.Result = Resources.Load(path);
    yield break;
  }

  public Future<UnityEngine.Object> Load(string path, ref ResourceInfo.Resource context)
  {
    return new Future<UnityEngine.Object>((Func<Promise<UnityEngine.Object>, IEnumerator>) (promise => this.Load_(path, promise)));
  }

  public Future<UnityEngine.Object> DownloadOrCache(
    string path,
    ref ResourceInfo.Resource context)
  {
    return new Future<UnityEngine.Object>((Func<Promise<UnityEngine.Object>, IEnumerator>) (promise => this.Load_(path, promise)));
  }

  public UnityEngine.Object LoadImmediatelyForSmallObject(
    string path,
    ref ResourceInfo.Resource context)
  {
    return Resources.Load(path);
  }
}
