﻿// Decompiled with JetBrains decompiler
// Type: Popup0171818Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;

public class Popup0171818Menu : BattleBackButtonMenuBase
{
  public void IbtnYes()
  {
    if (this.env.core.phaseState.state == BL.Phase.player)
    {
      if (this.env.core.isAutoBattle.value)
        this.battleManager.getController<BattleAIController>().stopAIAction();
      foreach (BL.Unit unit in this.env.core.playerUnits.value)
      {
        if (!unit.IsCharm)
          this.env.core.getUnitPosition(unit).completeActionUnit(this.env.core, true, false);
      }
    }
    this.battleManager.popupCloseAll(false);
  }

  public override void onBackButton()
  {
    this.IbtnNo();
  }

  public void IbtnNo()
  {
    this.battleManager.popupDismiss(false, false);
  }
}
