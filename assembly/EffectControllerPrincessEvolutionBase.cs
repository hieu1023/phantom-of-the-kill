﻿// Decompiled with JetBrains decompiler
// Type: EffectControllerPrincessEvolutionBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

public class EffectControllerPrincessEvolutionBase : EffectController
{
  [SerializeField]
  protected PrincessEvolutionSoundEffect soundManager;
  [SerializeField]
  protected GameObject animationRoot;

  public PrincessEvolutionSoundEffect SoundManager
  {
    get
    {
      return this.soundManager;
    }
  }

  public void EndSE()
  {
    this.soundManager.OnPlayResult();
  }

  public virtual IEnumerator Initialize(PrincesEvolutionParam param, GameObject backBtn)
  {
    yield break;
  }
}
