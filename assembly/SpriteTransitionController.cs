﻿// Decompiled with JetBrains decompiler
// Type: SpriteTransitionController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[ExecuteInEditMode]
[RequireComponent(typeof (UI2DSprite))]
public class SpriteTransitionController : MonoBehaviour
{
  [Header("Dissolve Mask")]
  public Color color = Color.white;
  public TextureWrapMode maskWrapMode = TextureWrapMode.Clamp;
  public float xScale = 1f;
  public float yScale = 1f;
  [Header("Sprite")]
  public TextureWrapMode spriteWrapMode = TextureWrapMode.Clamp;
  public float xScaleMain = 1f;
  public float yScaleMain = 1f;
  private UI2DSprite sprite;
  public bool brighten;
  [Tooltip("Dissolve Mask")]
  public Texture2D mask;
  [Range(0.0f, 1f)]
  public float width;
  [Range(0.0f, 1f)]
  public float disolvePower;
  [Header("Alpha Mask")]
  [Tooltip("Alpha Mask")]
  public Texture2D alphaMask;
  public float xOffset;
  public float yOffset;
  public float xOffsetMain;
  public float yOffsetMain;
  private SpriteTransitionDrawCall drawcallExtension;
  public UIDrawCall drawCall;
  private bool useAlphaMask;

  private void Start()
  {
    this.sprite = this.GetComponent<UI2DSprite>();
    if ((Object) this.alphaMask != (Object) null)
    {
      this.sprite.shader = UnityEngine.Shader.Find("PUNK/UI/Sprite_Transition_Mask");
      this.useAlphaMask = true;
    }
    else
    {
      this.sprite.shader = UnityEngine.Shader.Find("PUNK/UI/Sprite_Transition");
      this.useAlphaMask = false;
    }
  }

  private void LateUpdate()
  {
    this.UpdateMaterialSettings();
  }

  private void UpdateMaterialSettings()
  {
    if ((Object) this.sprite == (Object) null)
    {
      Debug.Log((object) "Sprite is null.");
      this.sprite = this.GetComponent<UI2DSprite>();
    }
    if (!this.useAlphaMask && (Object) this.alphaMask != (Object) null)
    {
      this.sprite.shader = UnityEngine.Shader.Find("PUNK/UI/Sprite_Transition_Mask");
      this.useAlphaMask = true;
    }
    else if (this.useAlphaMask && (Object) this.alphaMask == (Object) null)
    {
      this.sprite.shader = UnityEngine.Shader.Find("PUNK/UI/Sprite_Transition");
      this.useAlphaMask = false;
    }
    else if ((Object) this.sprite.drawCall == (Object) null)
    {
      this.sprite.Invalidate(true);
    }
    else
    {
      if ((Object) this.drawCall != (Object) this.sprite.drawCall)
      {
        this.drawCall = this.sprite.drawCall;
        this.drawcallExtension = this.sprite.drawCall.gameObject.GetOrAddComponent<SpriteTransitionDrawCall>();
      }
      this.drawcallExtension.sprite = this.sprite;
      this.drawcallExtension.drawcall = this.sprite.drawCall;
      this.sprite.mainTexture.wrapMode = this.spriteWrapMode;
      if ((Object) this.alphaMask != (Object) null)
        this.alphaMask.wrapMode = this.maskWrapMode;
      this.drawcallExtension.color = this.color;
      this.drawcallExtension.brighten = this.brighten;
      this.drawcallExtension.mask = this.mask;
      this.drawcallExtension.width = this.width;
      this.drawcallExtension.disolvePower = this.disolvePower;
      this.drawcallExtension.alphaMask = this.alphaMask;
      this.drawcallExtension.xOffset = this.xOffset;
      this.drawcallExtension.yOffset = this.yOffset;
      this.drawcallExtension.xScale = this.xScale;
      this.drawcallExtension.yScale = this.yScale;
      this.drawcallExtension.xOffsetMain = this.xOffsetMain;
      this.drawcallExtension.yOffsetMain = this.yOffsetMain;
      this.drawcallExtension.xScaleMain = this.xScaleMain;
      this.drawcallExtension.yScaleMain = this.yScaleMain;
    }
  }
}
