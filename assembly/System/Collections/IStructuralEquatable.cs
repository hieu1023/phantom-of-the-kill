﻿// Decompiled with JetBrains decompiler
// Type: System.Collections.IStructuralEquatable
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

namespace System.Collections
{
  public interface IStructuralEquatable
  {
    bool Equals(object other, IEqualityComparer comparer);

    int GetHashCode(IEqualityComparer comparer);
  }
}
