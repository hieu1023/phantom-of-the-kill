﻿// Decompiled with JetBrains decompiler
// Type: Story009132Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;
using System.Collections;
using UnityEngine;

public class Story009132Scene : NGSceneBase
{
  public Story009132Menu menu;
  [SerializeField]
  private NGxScroll ScrollContainer;

  public IEnumerator onStartSceneAsync(PlayerSeaQuestS quest)
  {
    PlayerSeaQuestS[] quests = SMManager.Get<PlayerSeaQuestS[]>().S(quest.quest_sea_s.quest_xl.ID, quest.quest_sea_s.quest_l.ID, quest.quest_sea_s.quest_m.ID);
    this.ScrollContainer.Clear();
    IEnumerator e = this.menu.InitEpisodeButton(quests, quest);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.ScrollContainer.ResolvePosition();
  }
}
