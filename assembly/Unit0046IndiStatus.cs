﻿// Decompiled with JetBrains decompiler
// Type: Unit0046IndiStatus
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System.Collections.Generic;
using UnityEngine;

public class Unit0046IndiStatus : MonoBehaviour
{
  private Color GrayColor = new Color(0.6f, 0.6f, 0.6f);
  public UILabel HP;
  public UILabel PAt;
  public UILabel MAt;
  public UILabel PDe;
  public UILabel MDe;
  public const int GUN = 5;
  public const int STAFF = 6;

  public void SetText(PlayerUnit pUnit)
  {
    Judgement.NonBattleParameter nonBattleParameter = Judgement.NonBattleParameter.FromPlayerUnit(pUnit, false);
    int kindGearKind = pUnit.unit.kind_GearKind;
    this.HP.SetTextLocalize(nonBattleParameter.Hp.ToString());
    this.PAt.SetTextLocalize(nonBattleParameter.PhysicalAttack.ToString());
    this.MAt.SetTextLocalize(nonBattleParameter.MagicAttack.ToString());
    if (kindGearKind != 5 && kindGearKind != 6)
    {
      if (!pUnit.unit.magic_warrior_flag)
        this.MAt.color = this.GrayColor;
    }
    else
      this.PAt.color = this.GrayColor;
    this.PDe.SetTextLocalize(nonBattleParameter.PhysicalDefense.ToString());
    this.MDe.SetTextLocalize(nonBattleParameter.MagicDefense.ToString());
  }

  public void RemoveText()
  {
    ((IEnumerable<UILabel>) this.GetComponentsInChildren<UILabel>()).ForEach<UILabel>((System.Action<UILabel>) (x => x.SetText("-")));
  }
}
