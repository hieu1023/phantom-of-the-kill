﻿// Decompiled with JetBrains decompiler
// Type: Unit00411Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit00411Menu : UnitMenuBase
{
  [SerializeField]
  private Unit00410Menu unit00410Menu;

  protected override void Update()
  {
    base.Update();
    this.ScrollUpdate();
  }

  public virtual IEnumerator Init(Player player, PlayerUnit[] playerUnits, bool isEquip)
  {
    Unit00411Menu unit00411Menu = this;
    unit00411Menu.unit00410Menu.enabled = false;
    IEnumerator e = unit00411Menu.Initialize();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    unit00411Menu.InitializeInfoEx((IEnumerable<PlayerUnit>) playerUnits, (IEnumerable<PlayerMaterialUnit>) null, Persist.unit00411SortAndFilter, false, isEquip, true, true, true, false, (System.Action) null);
    e = unit00411Menu.CreateUnitIcon();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    unit00411Menu.TxtNumber.SetTextLocalize(string.Format("{0}/{1}", (object) playerUnits.Length, (object) player.max_units));
    unit00411Menu.SetTextPosession(playerUnits, player);
    unit00411Menu.InitializeEnd();
  }

  public void SetTextPosession(PlayerUnit[] playerUnits, Player player)
  {
    this.TxtNumber.SetTextLocalize(string.Format("{0}/{1}", (object) playerUnits.Length, (object) player.max_units));
  }

  protected override IEnumerator CreateUnitIcon(
    int info_index,
    int unit_index,
    PlayerUnit baseUnit = null)
  {
    IEnumerator e = base.CreateUnitIcon(info_index, unit_index, baseUnit);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.CreateUnitIconAction(info_index, unit_index);
  }

  protected override void CreateUnitIconCache(int info_index, int unit_index, PlayerUnit baseUnit = null)
  {
    base.CreateUnitIconCache(info_index, unit_index, (PlayerUnit) null);
    this.CreateUnitIconAction(info_index, unit_index);
  }

  protected virtual void CreateUnitIconAction(int info_index, int unit_index)
  {
    UnitIconBase unitIcon = this.allUnitIcons[unit_index];
    if (unitIcon.Unit.IsMaterialUnit)
      unitIcon.onClick = (System.Action<UnitIconBase>) (ui => this.onClickMaterialIcon(unitIcon.PlayerUnit));
    else
      unitIcon.onClick = (System.Action<UnitIconBase>) (ui => this.onClickUnitlIcon(unitIcon.PlayerUnit));
  }

  public void onClickMaterialIcon(PlayerUnit unit)
  {
    Unit0042Scene.changeScene(true, unit, this.getUnits(), false, false);
  }

  public void onClickUnitlIcon(PlayerUnit unit)
  {
    Unit0042Scene.changeScene(true, unit, this.getUnits(), false, false);
  }

  public void onBtnUnitStorageList()
  {
    if (this.IsPushAndSet())
      return;
    if (!this.unit00410Menu.enabled)
      Unit004StorageScene.changeSceneListWithInitialize(false);
    else
      Unit004StorageScene.changeSceneSell(false);
  }

  public void onBtnUnitStorageIn()
  {
    if (this.IsPushAndSet())
      return;
    Unit004StorageInScene.changeScene(true);
  }

  public void onBtnUnitSell()
  {
    if (this.IsPushAndSet())
      return;
    Unit00468Scene.changeScene00410(false, Unit00410Menu.FromType.UnitList);
  }

  public void onBtnMaterialSell()
  {
    if (this.IsPushAndSet())
      return;
    Unit00468Scene.changeScene00410(false, Unit00410Menu.FromType.MaterialList);
  }

  public override void IbtnBack()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<PopupManager>.GetInstance().closeAll(false);
    Singleton<CommonRoot>.GetInstance().startScene = "unit004_top";
    this.backScene();
  }
}
