﻿// Decompiled with JetBrains decompiler
// Type: TitleSoundEvent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class TitleSoundEvent : MonoBehaviour
{
  public void playSE(string cue_name)
  {
    NGSoundManager instance = Singleton<NGSoundManager>.GetInstance();
    if (!((Object) instance != (Object) null))
      return;
    instance.PlaySe(cue_name, false, 0.0f, -1);
  }
}
