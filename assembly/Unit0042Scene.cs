﻿// Decompiled with JetBrains decompiler
// Type: Unit0042Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnitDetails;
using UnityEngine;

public class Unit0042Scene : NGSceneBase
{
  public Unit0042Menu menu;
  protected bool init;
  private long? versionUnitList;

  public Unit0042Scene.BootParam bootParam { get; private set; }

  public static void callChangeScene(bool stack, string player_id, Unit0042Scene.BootParam param)
  {
    string sceneName = "unit004_2";
    if (!param.playerUnit.unit.IsNormalUnit)
      sceneName = "unit004_2_material";
    else if (Singleton<NGGameDataManager>.GetInstance().IsSea)
      sceneName = "unit004_2_sea";
    Singleton<NGSceneManager>.GetInstance().changeScene(sceneName, (stack ? 1 : 0) != 0, (object) player_id, (object) param.playerUnit.id, (object) param);
  }

  public static void changeScene(
    bool stack,
    PlayerUnit playerUnit,
    PlayerUnit[] playerUnits,
    bool IsMaterial = false,
    bool IsMemory = false)
  {
    Unit0042Scene.BootParam bootParam = new Unit0042Scene.BootParam();
    bootParam.playerUnit = playerUnit;
    bootParam.playerUnits = playerUnits;
    bootParam.isMaterial = IsMaterial;
    bootParam.isMemory = IsMemory;
    if (!IsMemory && playerUnit.unit.IsNormalUnit)
    {
      bootParam.setControl(Control.OverkillersBase | Control.OverkillersSlot);
      if (playerUnit.player_id == Player.Current.id)
        bootParam.setControl(Control.OverkillersEdit | Control.OverkillersMove);
    }
    Unit0042Scene.callChangeScene(stack, "", bootParam);
  }

  public static void changeSceneSelf(
    bool stack,
    PlayerUnit playerUnit,
    PlayerUnit[] playerUnits,
    bool IsMemory = false)
  {
    Unit0042Scene.BootParam bootParam = new Unit0042Scene.BootParam();
    bootParam.playerUnit = playerUnit;
    bootParam.playerUnits = playerUnits;
    bootParam.isMemory = IsMemory;
    if (!IsMemory && playerUnit.unit.IsNormalUnit)
      bootParam.setControl(Control.OverkillersSlot | Control.SelfAbility);
    Unit0042Scene.callChangeScene(stack, "", bootParam);
  }

  public static void changeSceneGuestUnit(
    bool stack,
    PlayerUnit playerUnit,
    PlayerUnit[] playerUnits)
  {
    Unit0042Scene.BootParam bootParam = new Unit0042Scene.BootParam();
    bootParam.playerUnit = playerUnit;
    bootParam.playerUnits = playerUnits;
    bootParam.isGuest = true;
    bootParam.limited = true;
    string sceneName = "unit004_2";
    if (Singleton<NGGameDataManager>.GetInstance().IsSea)
      sceneName = "unit004_2_sea";
    Singleton<NGSceneManager>.GetInstance().changeScene(sceneName, (stack ? 1 : 0) != 0, (object) "", (object) playerUnit.id, (object) bootParam);
  }

  public static void changeSceneFriendUnit(bool stack, string player_id, int targetPlayerUnitId)
  {
    Unit0042Scene.BootParam bootParam = new Unit0042Scene.BootParam();
    bootParam.playerUnit = (PlayerUnit) null;
    bootParam.playerUnits = (PlayerUnit[]) null;
    bootParam.limited = true;
    bootParam.setControl(Control.OverkillersSlot);
    string sceneName = "unit004_2";
    if (Singleton<NGGameDataManager>.GetInstance().IsSea)
      sceneName = "unit004_2_sea";
    Singleton<NGSceneManager>.GetInstance().changeScene(sceneName, (stack ? 1 : 0) != 0, (object) player_id, (object) targetPlayerUnitId, (object) bootParam);
  }

  public static void changeSceneFriendUnit(bool stack, string player_id, PlayerUnit playerUnit)
  {
    Unit0042Scene.BootParam bootParam = new Unit0042Scene.BootParam();
    bootParam.playerUnit = playerUnit;
    bootParam.playerUnits = (PlayerUnit[]) null;
    bootParam.limited = true;
    bootParam.setControl(Control.OverkillersSlot);
    string sceneName = "unit004_2";
    if (Singleton<NGGameDataManager>.GetInstance().IsSea)
      sceneName = "unit004_2_sea";
    Singleton<NGSceneManager>.GetInstance().changeScene(sceneName, (stack ? 1 : 0) != 0, (object) player_id, (object) playerUnit.id, (object) bootParam);
  }

  public static void changeSceneEvolutionUnit(
    bool stack,
    PlayerUnit playerUnit,
    PlayerUnit[] playerUnits,
    bool IsMaterial = false,
    bool IsMemory = false,
    bool IsToutaPlusNoEnable = false)
  {
    Unit0042Scene.BootParam bootParam = new Unit0042Scene.BootParam();
    bootParam.playerUnit = playerUnit;
    bootParam.playerUnits = playerUnits;
    bootParam.limited = true;
    bootParam.isMaterial = IsMaterial;
    bootParam.isMemory = IsMemory;
    bootParam.setControl(Control.OverkillersSlot | Control.SelfAbility);
    bootParam.isToutaPlusNoEnable = IsToutaPlusNoEnable;
    string empty = string.Empty;
    string sceneName = playerUnit.unit.IsNormalUnit ? "unit004_2" : "unit004_2_material";
    if (Singleton<NGGameDataManager>.GetInstance().IsSea)
      sceneName = "unit004_2_sea";
    Singleton<NGSceneManager>.GetInstance().changeScene(sceneName, (stack ? 1 : 0) != 0, (object) "", (object) playerUnit.id, (object) bootParam);
  }

  public static void changeSceneReincarnationTypeUnit(
    bool stack,
    PlayerUnit playerUnit,
    PlayerUnit[] playerUnits,
    bool IsMaterial = false,
    bool IsMemory = false)
  {
    Unit0042Scene.BootParam bootParam = new Unit0042Scene.BootParam();
    bootParam.playerUnit = playerUnit;
    bootParam.playerUnits = playerUnits;
    bootParam.limited = true;
    bootParam.isMaterial = IsMaterial;
    bootParam.isMemory = IsMemory;
    bootParam.isReincarnationType = true;
    bootParam.setControl(Control.OverkillersSlot | Control.SelfAbility);
    string sceneName = "unit004_2";
    if (Singleton<NGGameDataManager>.GetInstance().IsSea)
      sceneName = "unit004_2_sea";
    Singleton<NGSceneManager>.GetInstance().changeScene(sceneName, (stack ? 1 : 0) != 0, (object) "", (object) playerUnit.id, (object) bootParam);
  }

  public static void changeSceneMyGvgAtkDeck(
    bool stack,
    PlayerUnit playerUnit,
    bool IsMaterial = false,
    bool IsMemory = false)
  {
    Unit0042Scene.changeSceneMyGvgDeck(stack, new Unit0042Scene.BootParam()
    {
      isMyGvgAtkDeck = true
    }, playerUnit, IsMaterial, IsMemory);
  }

  public static void changeSceneMyGvgDefDeck(
    bool stack,
    PlayerUnit playerUnit,
    bool IsMaterial = false,
    bool IsMemory = false)
  {
    Unit0042Scene.changeSceneMyGvgDeck(stack, new Unit0042Scene.BootParam()
    {
      isMyGvgDefDeck = true
    }, playerUnit, IsMaterial, IsMemory);
  }

  public static void changeSceneMyGvgDeck(
    bool stack,
    Unit0042Scene.BootParam param,
    PlayerUnit playerUnit,
    bool IsMaterial = false,
    bool IsMemory = false)
  {
    param.playerUnit = playerUnit;
    param.playerUnits = (PlayerUnit[]) null;
    param.isMaterial = IsMaterial;
    param.isMemory = IsMemory;
    if (!IsMemory && playerUnit.unit.IsNormalUnit)
    {
      param.setControl(Control.OverkillersBase | Control.OverkillersSlot);
      if (playerUnit.player_id == Player.Current.id)
        param.setControl(Control.OverkillersEdit | Control.OverkillersMove);
    }
    Unit0042Scene.callChangeScene(stack, "", param);
  }

  public static void changeSceneGvgUnit(bool stack, PlayerUnit unit, PlayerUnit[] playerUnits)
  {
    Unit0042Scene.BootParam bootParam = new Unit0042Scene.BootParam();
    bootParam.playerUnit = unit;
    bootParam.playerUnits = playerUnits;
    bootParam.limited = true;
    bootParam.setControl(Control.OverkillersBase | Control.OverkillersSlot);
    string sceneName = "unit004_2";
    if (Singleton<NGGameDataManager>.GetInstance().IsSea)
      sceneName = "unit004_2_sea";
    Singleton<NGSceneManager>.GetInstance().changeScene(sceneName, (stack ? 1 : 0) != 0, (object) "", (object) unit.id, (object) bootParam);
  }

  public static void changeSceneMypageEdit(PlayerUnit playerUnit, PlayerUnit[] playerUnits)
  {
    Unit0042Scene.BootParam bootParam = new Unit0042Scene.BootParam();
    bootParam.playerUnit = playerUnit;
    bootParam.playerUnits = playerUnits;
    bootParam.limited = true;
    bootParam.setControl(Control.OverkillersBase | Control.OverkillersSlot);
    Unit0042Scene.callChangeScene(true, "", bootParam);
  }

  public static void changeOverkillersUnitDetail(PlayerUnit playerUnit, PlayerUnit[] units = null)
  {
    Unit0042Scene.BootParam bootParam = new Unit0042Scene.BootParam();
    bootParam.playerUnit = playerUnit;
    bootParam.playerUnits = units;
    bootParam.limited = true;
    bootParam.setControl(Control.OverkillersUnit);
    Singleton<NGSceneManager>.GetInstance().changeScene("unit004_UnitEquip_Detail", true, (object) "", (object) playerUnit.id, (object) bootParam);
  }

  public override IEnumerator onInitSceneAsync()
  {
    Unit0042Scene unit0042Scene = this;
    unit0042Scene.GetComponent<UIRoot>().scalingStyle = UIRoot.Scaling.FixedSize;
    Future<GameObject> bgF = new ResourceObject("Prefabs/BackGround/UnitBackground_anim").Load<GameObject>();
    IEnumerator e = bgF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    unit0042Scene.backgroundPrefab = bgF.Result;
    if (Singleton<CommonRoot>.GetInstance().headerType == CommonRoot.HeaderType.Tower)
    {
      unit0042Scene.bgmFile = TowerUtil.BgmFile;
      unit0042Scene.bgmName = TowerUtil.BgmName;
    }
    else if (Singleton<NGGameDataManager>.GetInstance().IsSea)
    {
      e = unit0042Scene.SetSeaBgm();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
  }

  public IEnumerator onStartSceneAsync(
    string player_id,
    int targetPlayerUnitId,
    Unit0042Scene.BootParam param)
  {
    Unit0042Scene unit0042Scene = this;
    Singleton<CommonRoot>.GetInstance().isTouchBlock = true;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    unit0042Scene.bootParam = param;
    yield return (object) null;
    yield return (object) null;
    PlayerUnit[] unitList;
    IEnumerator e;
    if (!param.isGuest)
    {
      if (!string.IsNullOrEmpty(player_id))
      {
        if (param.playerUnit == (PlayerUnit) null || param.controlFlags.IsOn(Control.OverkillersSlot) && param.playerUnit.isAnyOverkillersUnits && param.playerUnit.cache_overkillers_units == null)
        {
          if (!unit0042Scene.init)
          {
            Unit0042Scene.FriendInfo friendInfo;
            PlayerUnit[] leaderUnitOverKillers;
            if (Singleton<NGGameDataManager>.GetInstance().IsSea)
            {
              // ISSUE: reference to a compiler-generated method
              Future<WebAPI.Response.SeaFriendStatus> futureF = WebAPI.SeaFriendStatus(player_id, targetPlayerUnitId, new System.Action<WebAPI.Response.UserError>(unit0042Scene.\u003ConStartSceneAsync\u003Eb__23_0));
              e = futureF.Wait();
              while (e.MoveNext())
                yield return e.Current;
              e = (IEnumerator) null;
              WebAPI.Response.SeaFriendStatus result = futureF.Result;
              if (result == null)
              {
                yield break;
              }
              else
              {
                friendInfo = new Unit0042Scene.FriendInfo(result);
                leaderUnitOverKillers = result.target_leader_unit_over_killers;
                futureF = (Future<WebAPI.Response.SeaFriendStatus>) null;
              }
            }
            else
            {
              // ISSUE: reference to a compiler-generated method
              Future<WebAPI.Response.FriendStatus> futureF = WebAPI.FriendStatus(player_id, targetPlayerUnitId, new System.Action<WebAPI.Response.UserError>(unit0042Scene.\u003ConStartSceneAsync\u003Eb__23_1));
              e = futureF.Wait();
              while (e.MoveNext())
                yield return e.Current;
              e = (IEnumerator) null;
              WebAPI.Response.FriendStatus result = futureF.Result;
              if (result == null)
              {
                yield break;
              }
              else
              {
                friendInfo = new Unit0042Scene.FriendInfo(result);
                leaderUnitOverKillers = result.target_leader_unit_over_killers;
                futureF = (Future<WebAPI.Response.FriendStatus>) null;
              }
            }
            if (param.playerUnit == (PlayerUnit) null)
            {
              param.playerUnit = friendInfo.target_leader_unit;
              param.playerUnit.primary_equipped_gear = param.playerUnit.FindEquippedGear(friendInfo.target_player_items);
              param.playerUnit.primary_equipped_gear2 = param.playerUnit.FindEquippedGear2(friendInfo.target_player_items);
              param.playerUnit.primary_equipped_reisou = param.playerUnit.FindEquippedReisou(friendInfo.target_player_items, friendInfo.target_player_reisou_items);
              param.playerUnit.primary_equipped_reisou2 = param.playerUnit.FindEquippedReisou2(friendInfo.target_player_items, friendInfo.target_player_reisou_items);
              PlayerAwakeSkill playerAwakeSkill = (PlayerAwakeSkill) null;
              foreach (int? equipAwakeSkillId in param.playerUnit.equip_awake_skill_ids)
              {
                int? awakeSkillID = equipAwakeSkillId;
                playerAwakeSkill = ((IEnumerable<PlayerAwakeSkill>) friendInfo.target_player_awake_skills).FirstOrDefault<PlayerAwakeSkill>((Func<PlayerAwakeSkill, bool>) (x =>
                {
                  int id = x.id;
                  int? nullable = awakeSkillID;
                  int valueOrDefault = nullable.GetValueOrDefault();
                  return id == valueOrDefault & nullable.HasValue;
                }));
              }
              param.playerUnit.primary_equipped_awake_skill = playerAwakeSkill;
            }
            else
            {
              param.playerUnit.importOverkillersUnits(leaderUnitOverKillers, false);
              param.playerUnit.resetOverkillersParameter();
            }
            unitList = new PlayerUnit[1]{ param.playerUnit };
            e = OnDemandDownload.WaitLoadUnitResource((IEnumerable<PlayerUnit>) unitList, false, (IEnumerable<string>) null, false);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            e = unit0042Scene.menu.Init(param.playerUnit, unitList, true, param.limited, false, false, param.isMaterial, param.playerUnit.primary_equipped_gear, param.playerUnit.primary_equipped_gear2, param.isMemory);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            unit0042Scene.init = true;
            unitList = (PlayerUnit[]) null;
          }
        }
        else if (!unit0042Scene.init)
        {
          unitList = new PlayerUnit[1]{ param.playerUnit };
          e = OnDemandDownload.WaitLoadUnitResource((IEnumerable<PlayerUnit>) unitList, false, (IEnumerable<string>) null, false);
          while (e.MoveNext())
            yield return e.Current;
          e = (IEnumerator) null;
          e = unit0042Scene.menu.Init(param.playerUnit, unitList, true, param.limited, false, false, param.isMaterial, param.playerUnit.primary_equipped_gear, param.playerUnit.primary_equipped_gear2, param.isMemory);
          while (e.MoveNext())
            yield return e.Current;
          e = (IEnumerator) null;
          unit0042Scene.init = true;
          unitList = (PlayerUnit[]) null;
        }
      }
      else
      {
        unitList = (PlayerUnit[]) null;
        if (param.isMyGvgAtkDeck || param.isMyGvgDefDeck)
        {
          param.playerUnits = ((IEnumerable<PlayerUnit>) (param.isMyGvgAtkDeck ? GuildUtil.gvgDeckAttack : GuildUtil.gvgDeckDefense).player_units).Where<PlayerUnit>((Func<PlayerUnit, bool>) (x => x != (PlayerUnit) null)).ToArray<PlayerUnit>();
          unitList = param.playerUnits = unit0042Scene.filterNormalOrMaterial(param.playerUnit.unit.IsNormalUnit, param.playerUnits);
        }
        else if (param.playerUnits == null)
          unitList = new PlayerUnit[1]{ param.playerUnit };
        else
          unitList = param.playerUnits = unit0042Scene.filterNormalOrMaterial(param.playerUnit.unit.IsNormalUnit, param.playerUnits);
        bool sameUnitLength = false;
        bool diffVersion = false;
        if (!unit0042Scene.init || !(sameUnitLength = unit0042Scene.menu.CheckLength(unitList)) || (diffVersion = unit0042Scene.versionUnitList.HasValue && unit0042Scene.isModifiedPlayerUnits))
        {
          unit0042Scene.menu.scrollView.Clear();
          e = new Future<NGGameDataManager.StartSceneProxyResult>(new Func<Promise<NGGameDataManager.StartSceneProxyResult>, IEnumerator>(Singleton<NGGameDataManager>.GetInstance().StartSceneAsyncProxyImpl)).Wait();
          while (e.MoveNext())
            yield return e.Current;
          e = (IEnumerator) null;
          param.playerUnit = unit0042Scene.updateUnits(ref unitList, param.playerUnit);
          PlayerUnit centerUnit = param.playerUnit;
          if (unit0042Scene.init & sameUnitLength & diffVersion && unit0042Scene.menu.CurrentIndex >= 0 && unit0042Scene.menu.CurrentIndex < unitList.Length)
            centerUnit = unitList[unit0042Scene.menu.CurrentIndex];
          if (!param.limited)
          {
            e = unit0042Scene.downloadOwnResource(unitList);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
          }
          e = unit0042Scene.menu.Init(centerUnit, unitList, false, param.limited, false, false, param.isMaterial, (PlayerItem) null, (PlayerItem) null, param.isMemory);
          while (e.MoveNext())
            yield return e.Current;
          e = (IEnumerator) null;
          unit0042Scene.init = true;
          centerUnit = (PlayerUnit) null;
        }
        else
        {
          List<PlayerUnit> newData = ((IEnumerable<PlayerUnit>) SMManager.Get<PlayerUnit[]>()).ToList<PlayerUnit>();
          List<PlayerUnit> newList = new List<PlayerUnit>();
          ((IEnumerable<PlayerUnit>) unitList).ForEach<PlayerUnit>((System.Action<PlayerUnit>) (x =>
          {
            if (x.unit.IsNormalUnit)
            {
              PlayerUnit playerUnit = newData.FirstOrDefault<PlayerUnit>((Func<PlayerUnit, bool>) (y => y.id == x.id));
              if (!(playerUnit != (PlayerUnit) null))
                return;
              newList.Add(playerUnit);
            }
            else
              newList.Add(x);
          }));
          if (newList.Count > 0 && !param.isReincarnationType)
          {
            e = unit0042Scene.menu.UpdateAllPage(newList.ToArray(), param.limited, param.isMaterial, param.isMemory);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
          }
          else
          {
            e = unit0042Scene.menu.UpdateAllPage(((IEnumerable<PlayerUnit>) unitList).ToArray<PlayerUnit>(), param.limited, param.isMaterial, param.isMemory);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
          }
        }
        unitList = (PlayerUnit[]) null;
      }
    }
    else if (!unit0042Scene.init)
    {
      unitList = (PlayerUnit[]) null;
      if (param.playerUnits == null)
        unitList = new PlayerUnit[1]{ param.playerUnit };
      else
        unitList = param.playerUnits = unit0042Scene.filterNormalOrMaterial(param.playerUnit.unit.IsNormalUnit, param.playerUnits);
      e = OnDemandDownload.WaitLoadUnitResource((IEnumerable<PlayerUnit>) unitList, false, (IEnumerable<string>) null, false);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      unit0042Scene.menu.scrollView.Clear();
      e = unit0042Scene.menu.Init(param.playerUnit, unitList, false, param.limited, false, false, param.isMaterial, (PlayerItem) null, (PlayerItem) null, param.isMemory);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      unit0042Scene.init = true;
      unitList = (PlayerUnit[]) null;
    }
    if (Singleton<NGGameDataManager>.GetInstance().IsColosseum)
      Singleton<CommonRoot>.GetInstance().SetFooterEnable(false);
    unit0042Scene.AutoOpenUnityToutaPopupCoroutine();
    GameObject currentScrollObject = unit0042Scene.menu.CurrentScrollObject;
    if ((UnityEngine.Object) currentScrollObject != (UnityEngine.Object) null)
    {
      UITweener[] tweeners = NGTween.findTweeners(currentScrollObject, true);
      NGTween.playTweens(tweeners, NGTween.Kind.START_END, false);
      NGTween.playTweens(tweeners, NGTween.Kind.START, false);
    }
  }

  private PlayerUnit updateUnits(ref PlayerUnit[] units, PlayerUnit current)
  {
    if (!this.versionUnitList.HasValue)
    {
      this.versionUnitList = new long?(SMManager.Revision<PlayerUnit[]>());
      return current;
    }
    if (!this.isModifiedPlayerUnits)
      return current;
    PlayerUnit[] playerUnitArray = SMManager.Get<PlayerUnit[]>();
    for (int index1 = 0; index1 < units.Length; ++index1)
    {
      for (int index2 = 0; index2 < playerUnitArray.Length; ++index2)
      {
        PlayerUnit playerUnit;
        if (units[index1].id == (playerUnit = playerUnitArray[index2]).id)
        {
          units[index1] = playerUnit;
          if (current.id == playerUnit.id)
          {
            current = playerUnit;
            break;
          }
          break;
        }
      }
    }
    return current;
  }

  private bool isModifiedPlayerUnits
  {
    get
    {
      long num = SMManager.Revision<PlayerUnit[]>();
      long? versionUnitList = this.versionUnitList;
      long valueOrDefault = versionUnitList.GetValueOrDefault();
      return !(num == valueOrDefault & versionUnitList.HasValue);
    }
  }

  public IEnumerator onStartSceneAsync(Unit0042Scene.BootParam param)
  {
    this.bootParam = param;
    if (!this.init)
    {
      PlayerUnit[] units = param.playerUnits = this.filterNormalOrMaterial(param.playerUnit.unit.IsNormalUnit, param.playerUnits);
      IEnumerator e = OnDemandDownload.WaitLoadUnitResource((IEnumerable<PlayerUnit>) units, false, (IEnumerable<string>) null, false);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      e = this.menu.Init(param.playerUnit, units, false, param.limited, true, false, param.isMaterial, param.playerUnit.primary_equipped_gear, param.playerUnit.primary_equipped_gear2, param.isMemory);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.init = true;
      units = (PlayerUnit[]) null;
    }
    this.AutoOpenUnityToutaPopupCoroutine();
  }

  private PlayerUnit[] filterNormalOrMaterial(bool fillNormal, PlayerUnit[] units)
  {
    return units == null || units.Length == 0 ? units : (fillNormal ? ((IEnumerable<PlayerUnit>) units).Where<PlayerUnit>((Func<PlayerUnit, bool>) (x => x != (PlayerUnit) null && x.unit.IsNormalUnit)) : ((IEnumerable<PlayerUnit>) units).Where<PlayerUnit>((Func<PlayerUnit, bool>) (x => x != (PlayerUnit) null && !x.unit.IsNormalUnit))).ToArray<PlayerUnit>();
  }

  private IEnumerator downloadOwnResource(PlayerUnit[] units)
  {
    UnitUnit[] array = ((IEnumerable<PlayerUnit>) units).Where<PlayerUnit>((Func<PlayerUnit, bool>) (x => x != (PlayerUnit) null && x.job_abilities != null && (uint) x.job_abilities.Length > 0U)).SelectMany<PlayerUnit, UnitUnit>((Func<PlayerUnit, IEnumerable<UnitUnit>>) (y => ((IEnumerable<PlayerUnitJob_abilities>) y.job_abilities).SelectMany<PlayerUnitJob_abilities, UnitUnit>((Func<PlayerUnitJob_abilities, IEnumerable<UnitUnit>>) (z =>
    {
      HashSet<UnitUnit> unitUnitSet = new HashSet<UnitUnit>();
      if (z.master != null && z.current_levelup_pattern != null)
      {
        for (int level = z.level; level < z.master.levelup_patterns.Length; ++level)
        {
          List<KeyValuePair<UnitUnit, int>> levelupMaterials = z.getLevelupMaterials(y, level);
          foreach (UnitUnit unitUnit in (levelupMaterials != null ? levelupMaterials.Select<KeyValuePair<UnitUnit, int>, UnitUnit>((Func<KeyValuePair<UnitUnit, int>, UnitUnit>) (zl => zl.Key)) : (IEnumerable<UnitUnit>) null) ?? (IEnumerable<UnitUnit>) new List<UnitUnit>())
            unitUnitSet.Add(unitUnit);
        }
      }
      return (IEnumerable<UnitUnit>) unitUnitSet;
    })))).Distinct<UnitUnit>().ToArray<UnitUnit>();
    if (array.Length != 0)
      yield return (object) OnDemandDownload.WaitLoadUnitResource((IEnumerable<UnitUnit>) array, false, (IEnumerable<string>) null, false);
  }

  public override void onEndScene()
  {
    if (this.tweens != null)
    {
      foreach (Behaviour tween in this.tweens)
        tween.enabled = false;
      this.tweenTimeoutTime = 10f;
    }
    this.menu.onEndScene();
  }

  public override IEnumerator onEndSceneAsync()
  {
    Unit0042Scene unit0042Scene = this;
    if (Singleton<NGGameDataManager>.GetInstance().IsFromUnityPopupStageList)
      unit0042Scene.init = false;
    IEnumerator e = unit0042Scene.menu.onEndSceneAsync();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if (unit0042Scene.tweens != null)
    {
      foreach (Behaviour tween in unit0042Scene.tweens)
        tween.enabled = true;
      NGTween.setOnTweenFinished(unit0042Scene.tweens, (MonoBehaviour) unit0042Scene, "oneFrameWait");
    }
    else
      unit0042Scene.isTweenFinished = true;
  }

  protected void oneFrameWait()
  {
    this.StartCoroutine(this._oneFrameWait());
  }

  protected IEnumerator _oneFrameWait()
  {
    Unit0042Scene unit0042Scene = this;
    yield return (object) null;
    yield return (object) null;
    yield return (object) null;
    yield return (object) null;
    unit0042Scene.isTweenFinished = true;
  }

  public void onBackScene()
  {
    if (Singleton<NGGameDataManager>.GetInstance().IsSea)
    {
      Singleton<CommonRoot>.GetInstance().headerType = CommonRoot.HeaderType.Sea;
      Singleton<CommonRoot>.GetInstance().isActiveHeader = true;
    }
    this.menu.onBackScene();
    this.endLoad();
  }

  public void onBackScene(
    string player_id,
    PlayerUnit playerUnit,
    PlayerUnit[] playerUnits,
    int targetPlayerUnitId = 0)
  {
    this.onBackScene();
  }

  public void onBackScene(PlayerUnit unit, PlayerUnit[] playerUnits)
  {
    this.onBackScene();
  }

  private void errorCallback(WebAPI.Response.UserError error)
  {
    Singleton<CommonRoot>.GetInstance().isLoading = false;
    Singleton<CommonRoot>.GetInstance().isTouchBlock = false;
    WebAPI.DefaultUserErrorCallback(error);
    this.menu.IbtnBack();
  }

  private IEnumerator SetSeaBgm()
  {
    Unit0042Scene unit0042Scene = this;
    IEnumerator e = ServerTime.WaitSync();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    SeaHomeMap seaHomeMap = ((IEnumerable<SeaHomeMap>) MasterData.SeaHomeMapList).ActiveSeaHomeMap(ServerTime.NowAppTimeAddDelta());
    if (seaHomeMap != null && !string.IsNullOrEmpty(seaHomeMap.bgm_cuesheet_name) && !string.IsNullOrEmpty(seaHomeMap.bgm_cue_name))
    {
      unit0042Scene.bgmFile = seaHomeMap.bgm_cuesheet_name;
      unit0042Scene.bgmName = seaHomeMap.bgm_cue_name;
    }
  }

  public void onStartScene(string player_id, int targetPlayerUnitId, Unit0042Scene.BootParam param)
  {
    this.endLoad();
    if (param.playerUnit == (PlayerUnit) null || param.playerUnit.is_storage)
      return;
    Singleton<TutorialRoot>.GetInstance().ShowAdvice("unit004_2_player_unit", 0, (System.Action) null);
  }

  public void onStartScene(Unit0042Scene.BootParam param)
  {
    this.endLoad();
  }

  public void endLoad()
  {
    Singleton<CommonRoot>.GetInstance().isTouchBlock = false;
    Singleton<CommonRoot>.GetInstance().isLoading = false;
    GameObject currentScrollObject = this.menu.CurrentScrollObject;
    if (!((UnityEngine.Object) currentScrollObject != (UnityEngine.Object) null))
      return;
    UITweener[] tweeners = NGTween.findTweeners(currentScrollObject, true);
    NGTween.playTweens(tweeners, NGTween.Kind.START_END, false);
    NGTween.playTweens(tweeners, NGTween.Kind.START, false);
  }

  private void OnDestroy()
  {
    NGGameDataManager instance = Singleton<NGGameDataManager>.GetInstance();
    if (!((UnityEngine.Object) instance != (UnityEngine.Object) null))
      return;
    instance.resetOverkillersSlotReleaseConditions();
  }

  private void AutoOpenUnityToutaPopupCoroutine()
  {
    NGGameDataManager instance = Singleton<NGGameDataManager>.GetInstance();
    if (instance.fromUnityPopup != NGGameDataManager.FromUnityPopup.Unit0042Scene)
      return;
    instance.OnceOpenUnityPopup();
    instance.fromUnityPopup = NGGameDataManager.FromUnityPopup.None;
  }

  public class BootParam
  {
    public PlayerUnit playerUnit { get; set; }

    public PlayerUnit[] playerUnits { get; set; }

    public Control controlFlags { get; private set; }

    public bool limited
    {
      get
      {
        return this.controlFlags.IsOn(Control.Limited);
      }
      set
      {
        this.controlFlags = value ? this.controlFlags.Set(Control.Limited) : this.controlFlags.Clear(Control.Limited);
      }
    }

    public bool isGuest
    {
      get
      {
        return this.controlFlags.IsOn(Control.Guest);
      }
      set
      {
        this.controlFlags = value ? this.controlFlags.Set(Control.Guest) : this.controlFlags.Clear(Control.Guest);
      }
    }

    public bool isMemory
    {
      get
      {
        return this.controlFlags.IsOn(Control.Memory);
      }
      set
      {
        this.controlFlags = value ? this.controlFlags.Set(Control.Memory) : this.controlFlags.Clear(Control.Memory);
      }
    }

    public bool isMaterial
    {
      get
      {
        return this.controlFlags.IsOn(Control.Material);
      }
      set
      {
        this.controlFlags = value ? this.controlFlags.Set(Control.Material) : this.controlFlags.Clear(Control.Material);
      }
    }

    public bool isReincarnationType
    {
      get
      {
        return this.controlFlags.IsOn(Control.ReincarnationType);
      }
      set
      {
        this.controlFlags = value ? this.controlFlags.Set(Control.ReincarnationType) : this.controlFlags.Clear(Control.ReincarnationType);
      }
    }

    public bool isToutaPlusNoEnable
    {
      get
      {
        return this.controlFlags.IsOn(Control.ToutaPlusNoEnable);
      }
      set
      {
        this.controlFlags = value ? this.controlFlags.Set(Control.ToutaPlusNoEnable) : this.controlFlags.Clear(Control.ToutaPlusNoEnable);
      }
    }

    public bool isMyGvgAtkDeck
    {
      get
      {
        return this.controlFlags.IsOn(Control.MyGvgAtkDeck);
      }
      set
      {
        this.controlFlags = value ? this.controlFlags.Set(Control.MyGvgAtkDeck) : this.controlFlags.Clear(Control.MyGvgAtkDeck);
      }
    }

    public bool isMyGvgDefDeck
    {
      get
      {
        return this.controlFlags.IsOn(Control.MyGvgDefDeck);
      }
      set
      {
        this.controlFlags = value ? this.controlFlags.Set(Control.MyGvgDefDeck) : this.controlFlags.Clear(Control.MyGvgDefDeck);
      }
    }

    public void setControl(Control flags)
    {
      this.controlFlags = this.controlFlags.Set(flags);
    }

    public void clearControl(Control flags)
    {
      this.controlFlags = this.controlFlags.Clear(flags);
    }
  }

  private class FriendInfo
  {
    public PlayerUnit target_leader_unit;
    public PlayerItem[] target_player_items;
    public PlayerAwakeSkill[] target_player_awake_skills;
    public PlayerGearReisouSchema[] target_player_reisou_items;

    public FriendInfo(WebAPI.Response.SeaFriendStatus info)
    {
      this.target_leader_unit = info.target_leader_unit;
      this.target_player_items = info.target_player_items;
      this.target_player_awake_skills = info.target_player_awake_skills;
      this.target_player_reisou_items = info.target_player_reisou_items;
      if (this.target_leader_unit == (PlayerUnit) null)
        return;
      this.target_leader_unit.importOverkillersUnits(info.target_leader_unit_over_killers, false);
      this.target_leader_unit.resetOverkillersParameter();
    }

    public FriendInfo(WebAPI.Response.FriendStatus info)
    {
      this.target_leader_unit = info.target_leader_unit;
      this.target_player_items = info.target_player_items;
      this.target_player_awake_skills = info.target_player_awake_skills;
      this.target_player_reisou_items = info.target_player_reisou_items;
      if (this.target_leader_unit == (PlayerUnit) null)
        return;
      this.target_leader_unit.importOverkillersUnits(info.target_leader_unit_over_killers, false);
      this.target_leader_unit.resetOverkillersParameter();
    }
  }
}
