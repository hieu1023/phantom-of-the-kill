﻿// Decompiled with JetBrains decompiler
// Type: GearKindButtonIcon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using UnityEngine;

public class GearKindButtonIcon : MonoBehaviour
{
  public void Init(GearKindEnum kind)
  {
    this.GetComponent<UI2DSprite>().sprite2D = this.GetIdle(kind);
  }

  public UnityEngine.Sprite GetIdle(GearKind kind)
  {
    return this.GetIdle(kind.Enum);
  }

  public UnityEngine.Sprite GetPress(GearKind kind)
  {
    return this.GetPress(kind.Enum);
  }

  public UnityEngine.Sprite GetIdle(GearKindEnum kind)
  {
    return Resources.Load<UnityEngine.Sprite>(string.Format("Icons/Materials/GuideWeaponBtn/slc_{0}_idle", (object) kind.ToString()));
  }

  public UnityEngine.Sprite GetPress(GearKindEnum kind)
  {
    return Resources.Load<UnityEngine.Sprite>(string.Format("Icons/Materials/GuideWeaponBtn/slc_{0}_pressed", (object) kind.ToString()));
  }
}
