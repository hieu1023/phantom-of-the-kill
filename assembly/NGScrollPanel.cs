﻿// Decompiled with JetBrains decompiler
// Type: NGScrollPanel
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Reflection;
using UnityEngine;

public class NGScrollPanel : MonoBehaviour
{
  public UIPanel panel;
  private int count;

  private void Update()
  {
    if (++this.count % 3 == 0)
      return;
    this.panel.GetType().GetField("mUpdateFrame", BindingFlags.Static | BindingFlags.NonPublic).SetValue((object) this.panel, (object) Time.frameCount);
  }
}
