﻿// Decompiled with JetBrains decompiler
// Type: GuildMemberListPopup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System;
using System.Collections;
using UnityEngine;

public class GuildMemberListPopup : GuildMemberListBase
{
  protected const int Width = 518;
  protected const int Height = 157;
  protected const int ColumnValue = 1;
  protected const int RowValue = 15;
  protected const int ScreenValue = 8;
  private bool isEnemy;
  [SerializeField]
  private UILabel popupTitle;
  [SerializeField]
  protected UILabel guildMemberNum;
  [SerializeField]
  private GameObject slc_title_base_one;
  [SerializeField]
  private GameObject slc_title_base_enemy;
  private Guild0282Menu guild282Menu;
  private Guild0282GuildBaseMenu guildBaseMenu;
  private GuildRegistration guildInfo;
  private GuildMemberObject guildMemberObjs;
  private System.Action actionAfterRoleChange;

  public IEnumerator Initialize(
    bool is_enemy,
    Guild0282Menu menu,
    Guild0282GuildBaseMenu baseMenu,
    GuildMemberObject popup,
    GuildRegistration guild,
    System.Action action = null)
  {
    IEnumerator e = this.CommonInit(is_enemy, menu, baseMenu, popup, guild, action);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public IEnumerator Initialize(GuildMemberObject popup, System.Action action = null)
  {
    IEnumerator e = this.CommonInit(false, (Guild0282Menu) null, (Guild0282GuildBaseMenu) null, popup, (GuildRegistration) null, action);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  private IEnumerator CommonInit(
    bool is_enemy,
    Guild0282Menu menu,
    Guild0282GuildBaseMenu baseMenu,
    GuildMemberObject popup,
    GuildRegistration guild,
    System.Action action = null)
  {
    GuildMemberListPopup guildMemberListPopup = this;
    Singleton<CommonRoot>.GetInstance().loadingMode = 1;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    yield return (object) null;
    if ((UnityEngine.Object) guildMemberListPopup.GetComponent<UIWidget>() != (UnityEngine.Object) null)
      guildMemberListPopup.GetComponent<UIWidget>().alpha = 0.0f;
    Singleton<CommonRoot>.GetInstance().SetGuildFooterBadgeBikkuri();
    guildMemberListPopup.isEnemy = is_enemy;
    guildMemberListPopup.guildInfo = is_enemy ? guild : PlayerAffiliation.Current.guild;
    guildMemberListPopup.guild282Menu = menu;
    guildMemberListPopup.guildBaseMenu = baseMenu;
    guildMemberListPopup.guildMemberObjs = popup;
    guildMemberListPopup.actionAfterRoleChange = action;
    guildMemberListPopup.slc_title_base_one.SetActive(!guildMemberListPopup.isEnemy);
    guildMemberListPopup.slc_title_base_enemy.SetActive(guildMemberListPopup.isEnemy);
    guildMemberListPopup.popupTitle.SetTextLocalize(Consts.Format(Consts.GetInstance().POPUP_GUILD_MEMBER_TITLE, (IDictionary) null));
    IEnumerator e = guildMemberListPopup.InitMemberScroll(guildMemberListPopup.guildInfo.memberships);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    e = guildMemberListPopup.CreateSortPopup();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Singleton<CommonRoot>.GetInstance().isLoading = false;
    Singleton<CommonRoot>.GetInstance().loadingMode = 0;
  }

  protected virtual IEnumerator InitMemberScroll(GuildMembership[] members)
  {
    GuildMemberListPopup guildMemberListPopup = this;
    guildMemberListPopup.allMemberInfo.Clear();
    guildMemberListPopup.allMemberBar.Clear();
    IEnumerator e = ServerTime.WaitSync();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    DateTime nowTime = ServerTime.NowAppTime();
    guildMemberListPopup.Initialize(nowTime, 518, 157, 15, 8);
    guildMemberListPopup.CreateMemberInfo(members);
    if (guildMemberListPopup.allMemberInfo.Count > 0)
    {
      e = guildMemberListPopup.CreateScrollBase(guildMemberListPopup.guildMemberObjs.GuildMemberListPrefab);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    guildMemberListPopup.scroll.ResolvePosition();
    guildMemberListPopup.scroll.scrollView.UpdatePosition();
    guildMemberListPopup.guildMemberNum.SetTextLocalize(Consts.Format(Consts.GetInstance().POPUP_GUILD_MEMBER_NUMBER, (IDictionary) new Hashtable()
    {
      {
        (object) "now",
        (object) guildMemberListPopup.guildInfo.memberships.Length
      },
      {
        (object) "max",
        (object) PlayerAffiliation.Current.guild.appearance.membership_capacity
      }
    }));
    guildMemberListPopup.InitializeEnd();
  }

  public override void onBackButton()
  {
    if (this.IsOpenSortPopup)
      return;
    if (Singleton<NGSceneManager>.GetInstance().sceneName.Equals("guild028_1"))
    {
      if ((UnityEngine.Object) Singleton<NGSceneManager>.GetInstance().sceneBase != (UnityEngine.Object) null && (UnityEngine.Object) Singleton<NGSceneManager>.GetInstance().sceneBase.GetComponent<Guild0281Menu>() != (UnityEngine.Object) null)
        Singleton<NGSceneManager>.GetInstance().sceneBase.GetComponent<Guild0281Menu>().isOpenGuildMemberOrInfoPopup = false;
    }
    else if (Singleton<NGSceneManager>.GetInstance().sceneName.Equals("guild028_2") && (UnityEngine.Object) Singleton<NGSceneManager>.GetInstance().sceneBase != (UnityEngine.Object) null && (UnityEngine.Object) Singleton<NGSceneManager>.GetInstance().sceneBase.GetComponent<Guild0282Menu>() != (UnityEngine.Object) null)
      Singleton<NGSceneManager>.GetInstance().sceneBase.GetComponent<Guild0282Menu>().isClosePopupByBackBtn = true;
    Singleton<PopupManager>.GetInstance().dismiss(false);
  }

  protected override void Update()
  {
    base.Update();
    this.ScrollUpdate();
  }

  protected override IEnumerator CreateScroll(int info_index, int bar_index)
  {
    GuildMemberListPopup popup = this;
    Guild0282MemberScroll scroll = popup.allMemberBar[bar_index];
    MemberBarInfo memberBarInfo = popup.allMemberInfo[info_index];
    memberBarInfo.scroll = scroll;
    scroll.gameObject.SetActive(true);
    IEnumerator e = scroll.Initialize(popup.isEnemy, popup, memberBarInfo.member, popup.guild282Menu, popup.guildBaseMenu, popup.guildMemberObjs, popup.actionAfterRoleChange);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    scroll.gameObject.SetActive(true);
  }
}
