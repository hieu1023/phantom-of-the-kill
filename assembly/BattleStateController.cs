﻿// Decompiled with JetBrains decompiler
// Type: BattleStateController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using DeviceKit;
using Earth;
using GameCore;
using MasterDataTable;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class BattleStateController : BattleMonoBehaviour
{
  private BL.StructValue<bool> waitCurrentAIActionCancel = new BL.StructValue<bool>(false);
  private const string effect_condition_for_victory = "Condition_forVictory";
  private const string effect_player_phase = "PlayerPhase";
  private const string effect_neutral_phase = "NeutralPhase";
  private const string effect_enemy_phase = "EnemyPhase";
  private const string effect_stage_clear = "StageClear";
  private const string effect_wave_clear = "WaveClear";
  private const string effect_gameover = "GameOver";
  private const string effect_turnover = "dir_TurnOver";
  private const string pvpMatchStartPrefab_path = "Prefabs/battle/dir_PvpMatchStart";
  private const string gvgMatchStartPrefab_path = "Prefabs/battle/dir_PvpMatchStart_for_guild";
  private BL.BattleModified<BL.PhaseState> phaseStateModified;
  private BL.BattleModified<BL.ClassValue<List<BL.UnitPosition>>> playerListModified;
  private BL.BattleModified<BL.ClassValue<List<BL.UnitPosition>>> neutralListModified;
  private BL.BattleModified<BL.ClassValue<List<BL.UnitPosition>>> enemyListModified;
  private BL.BattleModified<BL.ClassValue<List<BL.UnitPosition>>> completedListModified;
  private BL.BattleModified<BL.ClassValue<List<BL.UnitPosition>>> spawnUnitListModified;
  private BL.BattleModified<BL.StructValue<bool>> isAutoBattleModified;
  private BL.UnitPosition currentUnitPosition;
  private int currentUnitActionCount;
  private BattleAIController aiController;
  private BattleTimeManager btm;
  private GameObject popupAllDeadPlayerPrefab;
  private GameObject spawnEffectPrefab;
  private GameObject healPrefab;
  private Dictionary<string, GameObject> panelEffectPrefabs;
  private GameObject pvpMatchStartPrefab;
  private Battle01SelectNode uiNode;
  private PVPManager _pvpManager;
  private bool isStageClear;
  private bool isGameOver;
  private bool isTurnOver;

  public void setUiNode(Battle01SelectNode node)
  {
    this.uiNode = node;
  }

  private PVPManager pvpManager
  {
    get
    {
      if ((UnityEngine.Object) this._pvpManager == (UnityEngine.Object) null)
        this._pvpManager = Singleton<PVPManager>.GetInstanceOrNull();
      return this._pvpManager;
    }
  }

  public bool isWaitCurrentAIActionCancel
  {
    get
    {
      return this.waitCurrentAIActionCancel.value;
    }
    private set
    {
      if (this.waitCurrentAIActionCancel.value == value)
        return;
      this.waitCurrentAIActionCancel.value = value;
    }
  }

  public BL.StructValue<bool> instWaitCurrentAIActionCancel
  {
    get
    {
      return this.waitCurrentAIActionCancel;
    }
  }

  protected override IEnumerator Start_Original()
  {
    BattleStateController battleStateController = this;
    ResourceManager rm = Singleton<ResourceManager>.GetInstance();
    Future<GameObject> f = Res.Prefabs.popup.popup_022_8__anim_popup01.Load<GameObject>();
    IEnumerator e = f.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    battleStateController.popupAllDeadPlayerPrefab = f.Result;
    f = rm.Load<GameObject>("BattleEffects/field/ef035_enemy_sporn", 1f);
    e = f.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    battleStateController.spawnEffectPrefab = f.Result;
    HashSet<string> stringSet = new HashSet<string>();
    battleStateController.panelEffectPrefabs = new Dictionary<string, GameObject>();
    for (int row = 0; row < battleStateController.env.core.getFieldHeight(); ++row)
    {
      for (int column = 0; column < battleStateController.env.core.getFieldWidth(); ++column)
      {
        foreach (BattleLandformIncr battleLandformIncr in battleStateController.env.core.getFieldPanel(row, column).landform.GetAllIncr())
        {
          BattleLandformEffectGroup effectGroup = battleLandformIncr.effect_group;
          if (effectGroup != null)
            stringSet.Add(effectGroup.play_prefab_file_name);
        }
      }
    }
    foreach (string str in stringSet)
    {
      string path = str;
      f = rm.LoadOrNull<GameObject>(string.Format("BattleEffects/field/{0}", (object) path));
      e = f.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      if ((UnityEngine.Object) f.Result != (UnityEngine.Object) null)
        battleStateController.panelEffectPrefabs.Add(path, f.Result);
      path = (string) null;
    }
    f = rm.Load<GameObject>("BattleEffects/field/ef009_fixed_heal", 1f);
    e = f.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    battleStateController.healPrefab = f.Result;
    if (battleStateController.battleManager.isPvp || battleStateController.battleManager.isPvnpc)
    {
      f = Singleton<ResourceManager>.GetInstance().Load<GameObject>("Prefabs/battle/dir_PvpMatchStart", 1f);
      e = f.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      battleStateController.pvpMatchStartPrefab = f.Result;
    }
    else if (battleStateController.battleManager.isGvg)
    {
      f = Singleton<ResourceManager>.GetInstance().Load<GameObject>("Prefabs/battle/dir_PvpMatchStart_for_guild", 1f);
      e = f.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      battleStateController.pvpMatchStartPrefab = f.Result;
    }
  }

  protected override IEnumerator Start_Battle()
  {
    // ISSUE: reference to a compiler-generated field
    int num = this.\u003C\u003E1__state;
    BattleStateController battleStateController = this;
    if (num != 0)
      return false;
    // ISSUE: reference to a compiler-generated field
    this.\u003C\u003E1__state = -1;
    battleStateController.phaseStateModified = BL.Observe<BL.PhaseState>(battleStateController.env.core.phaseState);
    battleStateController.playerListModified = BL.Observe<BL.ClassValue<List<BL.UnitPosition>>>(battleStateController.env.core.playerActionUnits);
    battleStateController.neutralListModified = BL.Observe<BL.ClassValue<List<BL.UnitPosition>>>(battleStateController.env.core.neutralActionUnits);
    battleStateController.enemyListModified = BL.Observe<BL.ClassValue<List<BL.UnitPosition>>>(battleStateController.env.core.enemyActionUnits);
    battleStateController.completedListModified = BL.Observe<BL.ClassValue<List<BL.UnitPosition>>>(battleStateController.env.core.completedActionUnits);
    battleStateController.spawnUnitListModified = BL.Observe<BL.ClassValue<List<BL.UnitPosition>>>(battleStateController.env.core.spawnUnits);
    battleStateController.isAutoBattleModified = BL.Observe<BL.StructValue<bool>>(battleStateController.env.core.isAutoBattle);
    battleStateController.aiController = battleStateController.gameObject.AddComponent<BattleAIController>();
    battleStateController.btm = battleStateController.battleManager.getManager<BattleTimeManager>();
    if (battleStateController.battleManager.hasSavedEnvironment())
    {
      BL.Panel panel = battleStateController.env.core.fieldCurrent.value;
      if (panel != null)
        battleStateController.battleManager.getController<BattleCameraController>().setLookAtTarget(panel, false);
    }
    return false;
  }

  private bool startStroyWithNextState(BL.Story story, BL.Phase state)
  {
    if (story != null && story.scriptId >= 0)
    {
      Singleton<NGBattleManager>.GetInstance().startStory(story);
      this.btm.setPhaseState(state, false);
      return true;
    }
    this.btm.setPhaseState(state, false);
    return false;
  }

  private bool startStroyWithNextState(BL.StoryType stype, BL.Phase state)
  {
    return this.startStroyWithNextState(this.env.core.getStory(stype), state);
  }

  private void setStartTarget(BL.UnitPosition up, bool nonCurrent)
  {
    if (nonCurrent)
      this.btm.setTargetPanel(this.env.core.getFieldPanel(up, false), 0.1f, (System.Action) null, (System.Action) null, false);
    else
      this.btm.setCurrentUnit(up, 0.1f, false);
  }

  private void executeSkillEffects(List<BL.UnitPosition> upl)
  {
    bool isWait = false;
    this.btm.setScheduleAction((System.Action) (() =>
    {
      foreach (BL.UnitPosition up in upl)
      {
        foreach (BL.ExecuteSkillEffectResult executeSkillEffect in this.env.core.executeSkillEffects(up))
        {
          if (executeSkillEffect.targets.Count > 0)
          {
            this.doExecuteSkillEffects(up, executeSkillEffect);
            isWait = true;
          }
        }
      }
      float wait = 0.0f;
      Dictionary<BL.UnitPosition, int> prevHp = new Dictionary<BL.UnitPosition, int>();
      List<BattleUnitParts> source = new List<BattleUnitParts>();
      foreach (BL.UnitPosition index in this.env.core.unitPositions.value)
      {
        prevHp[index] = index.unit.hp;
        BattleUnitParts unitParts = this.env.unitResource[index.unit].unitParts_;
        unitParts.SetEffectMode(true, false);
        if (!unitParts.checkEffectCompleted())
          source.Add(unitParts);
      }
      foreach (BL.UnitPosition up in upl)
      {
        foreach (BL.ExecuteSkillEffectResult es in this.env.core.executePhaseSkillEffects(up).ToList<BL.ExecuteSkillEffectResult>())
        {
          if (es.targets.Count > 0)
          {
            this.doExecuteFacilitySkillEffects(up, es);
            this.btm.setScheduleAction((System.Action) null, 0.5f, (System.Action) null, (Func<bool>) null, false);
            wait = 1f;
          }
        }
      }
      Dictionary<BL.UnitPosition, int> afterHp = new Dictionary<BL.UnitPosition, int>();
      foreach (BL.UnitPosition index in this.env.core.unitPositions.value)
      {
        afterHp[index] = index.unit.hp;
        if (index.unit.hp != prevHp[index])
          index.unit.hp = prevHp[index];
      }
      if (source.Any<BattleUnitParts>())
      {
        foreach (BattleUnitParts battleUnitParts in source)
          this.btm.setEnableWait(new Func<bool>(battleUnitParts.checkEffectCompleted));
      }
      this.btm.setScheduleAction((System.Action) null, wait, (System.Action) (() =>
      {
        foreach (BL.UnitPosition index in this.env.core.unitPositions.value)
        {
          if (prevHp[index] != afterHp[index])
            index.unit.hp = afterHp[index];
          this.env.unitResource[index.unit].unitParts_.SetEffectMode(false, false);
        }
      }), (Func<bool>) null, false);
    }), 0.0f, (System.Action) null, (Func<bool>) null, true);
    if (isWait)
      this.btm.setEnableWait(0.1f);
    else
      this.btm.setScheduleAction((System.Action) (() => this.battleManager.isBattleEnable = true), 0.0f, (System.Action) null, (Func<bool>) null, false);
  }

  private void executeTurnInitSkillEffects(List<BL.UnitPosition> upl, int turn)
  {
    bool isWait = false;
    this.btm.setScheduleAction((System.Action) (() =>
    {
      foreach (BL.UnitPosition up in upl)
      {
        foreach (BL.ExecuteSkillEffectResult turnInitSkillEffect in this.env.core.executeTurnInitSkillEffects(up, turn))
        {
          if (turnInitSkillEffect.targets.Count > 0)
          {
            this.doExecuteSkillEffects(up, turnInitSkillEffect);
            isWait = true;
          }
        }
      }
    }), 0.0f, (System.Action) null, (Func<bool>) null, false);
    if (isWait)
      this.btm.setEnableWait(0.1f);
    else
      this.btm.setScheduleAction((System.Action) (() => this.battleManager.isBattleEnable = true), 0.0f, (System.Action) null, (Func<bool>) null, false);
  }

  private void doExecuteSkillEffects(BL.UnitPosition up, BL.ExecuteSkillEffectResult es)
  {
    List<BL.Unit> targets = new List<BL.Unit>();
    foreach (BL.UnitPosition target in es.targets)
      targets.Add(target.unit);
    bool isRebirth = ((IEnumerable<BattleskillEffect>) es.skill.Effects).Any<BattleskillEffect>((Func<BattleskillEffect, bool>) (x => x.effect_logic.Enum == BattleskillEffectLogicEnum.self_rebirth));
    BattleskillFieldEffect passiveEffect = es.skill.passive_effect;
    GameObject effectPrefab = (GameObject) null;
    GameObject targetEffectPrefab = this.healPrefab;
    GameObject invokedEffectPrefab = (GameObject) null;
    if (passiveEffect != null)
    {
      BE.SkillResource skillResource = this.env.skillResource[es.skill.passive_effect.ID];
      effectPrefab = skillResource.effectPrefab;
      invokedEffectPrefab = skillResource.invokedEffectPrefab;
      targetEffectPrefab = skillResource.targetEffectPrefab;
    }
    this.battleManager.battleEffects.skillFieldEffectStartCore(passiveEffect, up.unit, targets, effectPrefab, invokedEffectPrefab, targetEffectPrefab, (System.Action) (() =>
    {
      for (int index = 0; index < es.targets.Count; ++index)
      {
        if (isRebirth)
          es.targets[index].unit.rebirthBE(this.env);
        else
          this.dispHpNumberAnime(es.targets[index].unit, es.target_prev_hps[index], es.target_hps[index]);
      }
    }), (System.Action) null, (List<BL.Unit>) null, (System.Action<BL.Unit>) null, 0, (List<Quaternion?>) null);
  }

  public void doExecuteFacilitySkillEffects(BL.UnitPosition up, BL.ExecuteSkillEffectResult es)
  {
    List<BL.Unit> tl = es.targets.Select<BL.UnitPosition, BL.Unit>((Func<BL.UnitPosition, BL.Unit>) (x => x.unit)).ToList<BL.Unit>();
    BattleskillFieldEffect passiveEffect = es.skill.passive_effect;
    GameObject effectPrefab = (GameObject) null;
    GameObject targetEffectPrefab = this.healPrefab;
    GameObject invokedEffectPrefab = (GameObject) null;
    if (this.battleManager.useGameEngine)
    {
      foreach (BL.Unit attack in tl)
        this.battleManager.gameEngine.applyDeadUnit(attack, (BL.Unit) null);
    }
    if (passiveEffect != null)
    {
      BE.SkillResource skillResource = this.env.skillResource[es.skill.passive_effect.ID];
      effectPrefab = skillResource.effectPrefab;
      invokedEffectPrefab = skillResource.invokedEffectPrefab;
      targetEffectPrefab = skillResource.targetEffectPrefab;
    }
    this.battleManager.battleEffects.skillFieldEffectStartCore(passiveEffect, up.unit, tl, effectPrefab, invokedEffectPrefab, targetEffectPrefab, (System.Action) null, (System.Action) null, (List<BL.Unit>) null, (System.Action<BL.Unit>) (targetUnit =>
    {
      int index = tl.IndexOf(targetUnit);
      if (index < 0 || !es.disp_target_hps[index])
        return;
      BL.Unit unit = es.targets[index].unit;
      if (!unit.isView)
        return;
      BE.UnitResource unitResource = this.env.unitResource[unit];
      unitResource.unitParts_.dispHpNumber(es.target_prev_hps[index], es.target_hps[index]);
      unitResource.unitParts_.setHpGauge(es.target_prev_hps[index], es.target_hps[index]);
    }), 1, (List<Quaternion?>) null);
  }

  private void completedSkillEffects(BL.UnitPosition up)
  {
    List<BL.ExecuteSkillEffectResult> source = this.env.core.completedExecuteSkillEffects(up);
    bool flag = source.Any<BL.ExecuteSkillEffectResult>((Func<BL.ExecuteSkillEffectResult, bool>) (x => x.targets.Count > 0));
    Battle01SelectNode.MaskContinuer mc = (Battle01SelectNode.MaskContinuer) null;
    if (flag)
      mc = this.uiNode.setMaskActive(true, (Battle01SelectNode.MaskContinuer) null, false);
    if (source.Any<BL.ExecuteSkillEffectResult>((Func<BL.ExecuteSkillEffectResult, bool>) (x => x.targets.Count > 0)) && this.battleManager.useGameEngine)
      this.battleManager.gameEngine.applyDeadUnit(up.unit, (BL.Unit) null);
    foreach (BL.ExecuteSkillEffectResult es in source)
    {
      if (es.targets.Count > 0)
        this.doExecuteCompletedSkillEffects(up, es);
    }
    if (!flag)
      return;
    this.uiNode.setMaskActive(false, mc, false);
  }

  private void doExecuteCompletedSkillEffects(BL.UnitPosition up, BL.ExecuteSkillEffectResult es)
  {
    List<BL.Unit> targets = new List<BL.Unit>();
    foreach (BL.UnitPosition target in es.targets)
      targets.Add(target.unit);
    if (es.skill.skill_type == BattleskillSkillType.ailment)
    {
      this.btm.setTargetUnit(up, 0.0f, (GameObject) null, (System.Action) null, (System.Action) null, true);
      this.btm.setTargetUnit(up, 1.3f, (GameObject) null, (System.Action) (() =>
      {
        for (int index = 0; index < es.targets.Count; ++index)
        {
          if (es.targets[index].unit.isView)
          {
            BE.UnitResource unitResource = this.env.unitResource[es.targets[index].unit];
            unitResource.unitParts_.dispHpNumber(es.target_prev_hps[index], es.target_hps[index]);
            unitResource.unitParts_.setHpGauge(es.target_hps[index], es.target_hps[index]);
          }
        }
      }), (System.Action) null, false);
      if (!es.second_targets.Any<BL.Unit>())
        return;
      System.Action<int> dispHpOne = (System.Action<int>) (idx =>
      {
        BL.Unit secondTarget = es.second_targets[idx];
        if (!secondTarget.isView)
          return;
        BE.UnitResource unitResource = this.env.unitResource[secondTarget];
        unitResource.unitParts_.dispHpNumber(es.second_target_prev_hps[idx], es.second_target_hps[idx]);
        unitResource.unitParts_.setHpGauge(es.second_target_hps[idx], es.second_target_hps[idx]);
      });
      System.Action targetAction = (System.Action) null;
      System.Action<BL.Unit> targetEndAction = (System.Action<BL.Unit>) null;
      BattleskillFieldEffect fe = es.skill.passive_effect;
      GameObject targetEffectPrefab = fe != null ? this.env.skillResource[fe.ID].targetEffectPrefab : (GameObject) null;
      if ((UnityEngine.Object) targetEffectPrefab != (UnityEngine.Object) null)
      {
        targetEndAction = (System.Action<BL.Unit>) (targetUnit =>
        {
          int num = es.second_targets.IndexOf(targetUnit);
          if (num < 0)
            return;
          dispHpOne(num);
        });
      }
      else
      {
        fe = (BattleskillFieldEffect) null;
        targetEffectPrefab = this.healPrefab;
        targetAction = (System.Action) (() =>
        {
          for (int index = 0; index < es.second_targets.Count; ++index)
            dispHpOne(index);
        });
      }
      this.battleManager.battleEffects.skillFieldEffectStartCore(fe, (BL.Unit) null, es.second_targets, (GameObject) null, (GameObject) null, targetEffectPrefab, (System.Action) null, targetAction, (List<BL.Unit>) null, targetEndAction, 0, (List<Quaternion?>) null);
    }
    else
    {
      BattleskillFieldEffect passiveEffect = es.skill.passive_effect;
      GameObject effectPrefab = (GameObject) null;
      GameObject targetEffectPrefab = this.healPrefab;
      GameObject invokedEffectPrefab = (GameObject) null;
      if (passiveEffect != null)
      {
        BE.SkillResource skillResource = this.env.skillResource[es.skill.passive_effect.ID];
        effectPrefab = skillResource.effectPrefab;
        targetEffectPrefab = skillResource.targetEffectPrefab;
        invokedEffectPrefab = skillResource.invokedEffectPrefab;
      }
      this.battleManager.battleEffects.skillFieldEffectStartCore(passiveEffect, (BL.Unit) null, targets, effectPrefab, invokedEffectPrefab, targetEffectPrefab, (System.Action) null, (System.Action) (() =>
      {
        for (int index = 0; index < es.targets.Count; ++index)
          this.dispHpNumberAnime(es.targets[index].unit, es.target_prev_hps[index], es.target_hps[index]);
      }), (List<BL.Unit>) null, (System.Action<BL.Unit>) null, 0, (List<Quaternion?>) null);
    }
  }

  private void completedPositionSkillEffects(BL.UnitPosition up)
  {
    float wait = 0.0f;
    bool flag = false;
    List<List<BL.ExecuteSkillEffectResult>> result;
    List<BL.UnitPosition> unitPositionList = this.env.core.completedPositionExecuteSkillEffects(up, out result, (HashSet<BL.ISkillEffectListUnit>) null);
    Battle01SelectNode.MaskContinuer mc = (Battle01SelectNode.MaskContinuer) null;
    if (result != null && result.Any<List<BL.ExecuteSkillEffectResult>>((Func<List<BL.ExecuteSkillEffectResult>, bool>) (x => x.Any<BL.ExecuteSkillEffectResult>((Func<BL.ExecuteSkillEffectResult, bool>) (y => y.targets.Count > 0)))))
    {
      flag = true;
      mc = this.uiNode.setMaskActive(true, (Battle01SelectNode.MaskContinuer) null, false);
      this.btm.setScheduleAction((System.Action) (() =>
      {
        foreach (BL.UnitPosition unitPosition in this.env.core.unitPositions.value)
          this.env.unitResource[unitPosition.unit].unitParts_.SetEffectMode(true, false);
      }), 0.0f, (System.Action) null, (Func<bool>) null, false);
    }
    int num = 0;
    foreach (BL.UnitPosition up1 in unitPositionList)
    {
      foreach (BL.ExecuteSkillEffectResult es in result[num++])
      {
        if (es.targets.Count > 0)
        {
          this.doExecuteFacilitySkillEffects(up1, es);
          this.btm.setScheduleAction((System.Action) null, 0.5f, (System.Action) null, (Func<bool>) null, false);
          wait = 1f;
        }
      }
    }
    if (!flag)
      return;
    foreach (BL.UnitPosition unitPosition in this.env.core.unitPositions.value)
      this.env.unitResource[unitPosition.unit].unitParts_.SetEffectMode(true, false);
    this.btm.setScheduleAction((System.Action) null, wait, (System.Action) (() =>
    {
      foreach (BL.UnitPosition unitPosition in this.env.core.unitPositions.value)
        this.env.unitResource[unitPosition.unit].unitParts_.SetEffectMode(false, false);
    }), (Func<bool>) null, false);
    this.uiNode.setMaskActive(false, mc, false);
  }

  private void setStateWithEffect(
    string effect,
    float time,
    BL.Phase state,
    System.Action endAction,
    bool isBattleEnableControl)
  {
    this.btm.setSchedule((Schedule) new BattleStateController.EffectAIWait(this));
    this.battleManager.battleEffects.startEffect(effect, time, (System.Action) (() =>
    {
      if (endAction != null)
        endAction();
      this.btm.setPhaseState(state, false);
    }), isBattleEnableControl, (GameObject) null, false, false, (System.Action<GameObject>) null, (BattleEffects.CloneEnumlator) null);
  }

  private bool deadCheck()
  {
    if (this.env.core.condition.type == BL.ConditionType.bossdown && this.env.core.getBossUnit().isDead)
    {
      this.btm.setPhaseState(this.battleManager.isWave ? BL.Phase.stageclear_pre : BL.Phase.stageclear, true);
      return true;
    }
    if (this.env.core.getGamevoerType() != BL.GameoverType.alldown)
    {
      if (this.env.core.getGamevoerType() == BL.GameoverType.guestdown)
      {
        if (this.env.core.playerUnits.value.Count<BL.Unit>((Func<BL.Unit, bool>) (x => x.playerUnit.is_gesut && !x.isDead)) == 0)
        {
          this.btm.setPhaseState(BL.Phase.all_dead_player, false);
          return true;
        }
      }
      else if (this.env.core.getGamevoerType() == BL.GameoverType.playerdown && this.env.core.playerUnits.value.Count<BL.Unit>((Func<BL.Unit, bool>) (x => !x.playerUnit.is_gesut && !x.isDead)) == 0)
      {
        this.btm.setPhaseState(BL.Phase.all_dead_player, false);
        return true;
      }
    }
    if (this.env.core.getLoseUnitList().Count > 0 && this.env.core.getLoseUnitList().Any<BL.Unit>((Func<BL.Unit, bool>) (x => x.isDead)))
    {
      this.btm.setPhaseState(BL.Phase.gameover, true);
      return true;
    }
    if (this.env.core.battleInfo.isEarthMode)
    {
      if (!this.env.core.battleInfo.isExtraEarthQuest)
      {
        BL.Unit unit = this.env.core.playerUnits.value.Where<BL.Unit>((Func<BL.Unit, bool>) (x => x.is_leader)).FirstOrDefault<BL.Unit>();
        if (unit != (BL.Unit) null && unit.isDead)
        {
          this.btm.setPhaseState(BL.Phase.gameover, true);
          return true;
        }
      }
      else
      {
        BL.Unit unit = this.env.core.playerUnits.value.Where<BL.Unit>((Func<BL.Unit, bool>) (x => x.unit.same_character_id == 11002)).FirstOrDefault<BL.Unit>();
        if (unit != (BL.Unit) null && unit.isDead)
        {
          this.btm.setPhaseState(BL.Phase.gameover, true);
          return true;
        }
      }
    }
    int num = this.env.core.allDeadUnitsp(BL.ForceID.player) ? 1 : 0;
    bool flag = this.env.core.allDeadUnitsp(BL.ForceID.enemy);
    if (num != 0 && !flag)
    {
      this.btm.setPhaseState(BL.Phase.all_dead_player, false);
      return true;
    }
    if (!flag)
      return false;
    this.btm.setPhaseState(BL.Phase.all_dead_enemy, false);
    return true;
  }

  private bool checkCharmOnly(List<BL.UnitPosition> l)
  {
    foreach (BL.UnitPosition unitPosition in l)
    {
      if (!unitPosition.unit.IsCharm)
        return false;
    }
    return true;
  }

  private void updateNormal()
  {
    if (!this.battleManager.isBattleEnable)
      return;
    switch (this.phaseStateModified.value.state)
    {
      case BL.Phase.player:
        if (this.btm.isRunning || this.deadCheck())
          break;
        bool isCharm1 = this.checkCharmOnly(this.env.core.playerActionUnits.value);
        if (isCharm1)
        {
          if (this.aiController.isCompleted)
          {
            this.aiController.startAIAction(3f, (System.Action) null);
            break;
          }
          this.aiController.startAI(this.env.core.playerActionUnits.value, isCharm1, 0);
          break;
        }
        if (this.isAutoBattleModified.isChangedOnce())
        {
          if (this.isAutoBattleModified.value.value)
          {
            if (this.env.core.unitCurrent.unit != (BL.Unit) null)
              this.env.core.currentUnitPosition.cancelMove(this.env);
            if (this.aiController.startAI(this.env.core.playerActionUnits.value, isCharm1, 0))
              break;
            this.isAutoBattleModified.notifyChanged();
            break;
          }
          this.aiController.stopAIAction();
          if (!this.aiController.isAction)
            break;
          this.startWaitCurrentAIActionCancel();
          break;
        }
        if (!this.env.core.isAutoBattle.value || !this.aiController.isCompleted)
          break;
        this.aiController.startAIAction(3f, (System.Action) null);
        break;
      case BL.Phase.neutral:
        if (this.btm.isRunning || this.deadCheck())
          break;
        bool isCharm2 = this.checkCharmOnly(this.env.core.neutralActionUnits.value);
        if (this.aiController.isCompleted)
        {
          this.aiController.startAIAction(3f, (System.Action) null);
          break;
        }
        this.aiController.startAI(this.env.core.neutralActionUnits.value, isCharm2, 0);
        break;
      case BL.Phase.enemy:
        if (this.btm.isRunning || this.deadCheck())
          break;
        bool isCharm3 = this.checkCharmOnly(this.env.core.enemyActionUnits.value);
        if (this.aiController.isCompleted)
        {
          this.aiController.startAIAction(3f, (System.Action) null);
          break;
        }
        this.aiController.startAI(this.env.core.enemyActionUnits.value, isCharm3, 0);
        break;
    }
  }

  private void startWaitCurrentAIActionCancel()
  {
    string methodName = "doWaitCurrentAIActionCancel";
    this.StopCoroutine(methodName);
    this.StartCoroutine(methodName);
  }

  private IEnumerator doWaitCurrentAIActionCancel()
  {
    this.isWaitCurrentAIActionCancel = true;
    Singleton<CommonRoot>.GetInstance().isActive3DUIMask = true;
    do
    {
      yield return (object) null;
    }
    while (this.aiController.isAction);
    Singleton<CommonRoot>.GetInstance().isActive3DUIMask = false;
    this.isWaitCurrentAIActionCancel = false;
  }

  private void updateOVO()
  {
    if (!this.battleManager.gameEngine.isWaitAction && this.battleManager.isBattleEnable)
      return;
    switch (this.phaseStateModified.value.state)
    {
      case BL.Phase.player:
        if (!this.battleManager.isPvnpc && !this.battleManager.isGvg)
          break;
        bool isCharm = this.checkCharmOnly(this.env.core.playerActionUnits.value);
        if (!isCharm || !this.aiController.startAI(this.env.core.playerActionUnits.value, isCharm, 1))
          break;
        this.btm.setPhaseState(BL.Phase.pvp_move_unit_waiting, false);
        break;
      case BL.Phase.enemy:
        if (this.aiController.isCompleted)
        {
          this.aiController.startAIAction(0.0f, (System.Action) (() => this.battleManager.gameEngine.actionUnitCompleted()));
          break;
        }
        if (!this.battleManager.isPvnpc && !this.battleManager.isGvg)
          break;
        this.aiController.startAI(this.env.core.enemyActionUnits.value, this.checkCharmOnly(this.env.core.enemyActionUnits.value), 1);
        break;
      case BL.Phase.pvp_move_unit_waiting:
        if (!this.aiController.isCompleted)
          break;
        this.aiController.startAIAction(0.0f, (System.Action) (() => this.battleManager.gameEngine.actionUnitCompleted()));
        break;
    }
  }

  protected override void Update_Battle()
  {
    if (this.battleManager.isOvo)
      this.updateOVO();
    else
      this.updateNormal();
  }

  private void spawnUnit(BL.UnitPosition up)
  {
    up.unit.isSpawned = true;
    this.btm.setTargetPanel(this.env.core.getFieldPanel(up, false), 0.3f, (System.Action) (() => this.battleManager.battleEffects.fieldEffectsStart(this.spawnEffectPrefab, up.unit, 0.0f)), (System.Action) (() => up.unit.spawn(this.env, true)), false);
    this.btm.setScheduleAction((System.Action) null, 0.6f, (System.Action) null, (Func<bool>) null, false);
  }

  private void dispHpNumberAnime(BL.Unit unit, int prev_hp, int new_hp)
  {
    if (!unit.isView)
      return;
    this.env.unitResource[unit].unitParts_.dispHpNumber(prev_hp, new_hp);
  }

  private bool execPanelLandformIncr(BL.Panel panel, BL.UnitPosition up)
  {
    BattleLandformIncr incr = panel.landform.GetIncr(up.unit);
    if (!up.unit.isEnable || up.unit.isDead || (up.unit.hp == up.unit.parameter.Hp || (double) incr.hp_healing_ratio <= 0.0))
      return false;
    this.btm.setTargetUnit(up, 0.0f, this.healPrefab, (System.Action) null, (System.Action) (() => up.unit.hp += Mathf.CeilToInt((float) up.unit.parameter.Hp * incr.hp_healing_ratio)), true);
    this.btm.setScheduleAction((System.Action) null, 0.5f, (System.Action) null, (Func<bool>) null, false);
    return true;
  }

  private bool execPanelLandformEffect(
    BL.Panel panel,
    BL.UnitPosition up,
    BattleLandformEffectPhase phase)
  {
    if (up.unit.isFacility)
      return false;
    this.btm.setScheduleAction((System.Action) (() =>
    {
      BattleLandformIncr incr = panel.landform.GetIncr(up.unit);
      IEnumerable<BattleLandformEffect> landformEffects = incr.GetLandformEffects(phase);
      List<BattleFuncs.SkillParam> skillParams1 = new List<BattleFuncs.SkillParam>();
      BattleFuncs.GetLandBlessingSkillAdd(skillParams1, (BL.ISkillEffectListUnit) up.unit, (BL.ISkillEffectListUnit) null, BattleskillEffectLogicEnum.land_blessing_fix_hp_healing, panel.landform, phase);
      BattleFuncs.GetLandBlessingSkillMul(skillParams1, (BL.ISkillEffectListUnit) up.unit, (BL.ISkillEffectListUnit) null, BattleskillEffectLogicEnum.land_blessing_ratio_hp_healing, panel.landform, phase);
      int num1 = BattleFuncs.calcSkillParamAdd(skillParams1);
      if (!landformEffects.Any<BattleLandformEffect>() && num1 == 0 || (!up.unit.isEnable || up.unit.isDead))
        return;
      float num2 = landformEffects.Where<BattleLandformEffect>((Func<BattleLandformEffect, bool>) (x => x.effect_logic.Enum == BattleskillEffectLogicEnum.fix_heal)).Sum<BattleLandformEffect>((Func<BattleLandformEffect, float>) (x => x.GetFloat(BattleskillEffectLogicArgumentEnum.value)));
      float num3 = landformEffects.Where<BattleLandformEffect>((Func<BattleLandformEffect, bool>) (x => x.effect_logic.Enum == BattleskillEffectLogicEnum.ratio_heal)).Sum<BattleLandformEffect>((Func<BattleLandformEffect, float>) (x => x.GetFloat(BattleskillEffectLogicArgumentEnum.percentage)));
      float num4 = landformEffects.Where<BattleLandformEffect>((Func<BattleLandformEffect, bool>) (x => x.effect_logic.Enum == BattleskillEffectLogicEnum.fix_damage)).Sum<BattleLandformEffect>((Func<BattleLandformEffect, float>) (x => x.GetFloat(BattleskillEffectLogicArgumentEnum.value)));
      int damage = Mathf.CeilToInt((float) up.unit.parameter.Hp * landformEffects.Where<BattleLandformEffect>((Func<BattleLandformEffect, bool>) (x => x.effect_logic.Enum == BattleskillEffectLogicEnum.ratio_damage)).Sum<BattleLandformEffect>((Func<BattleLandformEffect, float>) (x => x.GetFloat(BattleskillEffectLogicArgumentEnum.percentage))) + num4);
      int num5 = Mathf.Clamp(up.unit.hp + Mathf.CeilToInt((float) up.unit.parameter.Hp * num3 * BattleFuncs.calcSkillParamMul(skillParams1, 1f) + num2 + (float) num1) - damage, 0, up.unit.parameter.Hp);
      int num6 = BattleFuncs.applyDamageCut(3, damage, (BL.ISkillEffectListUnit) up.unit, (BL.ISkillEffectListUnit) null, (Judgement.BeforeDuelUnitParameter) null, (Judgement.BeforeDuelUnitParameter) null, (AttackStatus) null, (AttackStatus) null, (XorShift) null, 0, 0, new int?(), panel);
      if (!up.unit.CanHeal((BattleskillSkillType) 0))
      {
        num2 = 0.0f;
        num3 = 0.0f;
      }
      List<BattleFuncs.SkillParam> skillParams2 = new List<BattleFuncs.SkillParam>();
      foreach (BattleFuncs.SkillParam skillParam in skillParams1)
      {
        if (up.unit.CanHeal(skillParam.effect.baseSkill.skill_type))
          skillParams2.Add(skillParam);
      }
      int newChangeHp = Mathf.Clamp(up.unit.hp + Mathf.CeilToInt((float) up.unit.parameter.Hp * num3 * BattleFuncs.calcSkillParamMul(skillParams2, 1f) + num2 + (float) BattleFuncs.calcSkillParamAdd(skillParams2)) - num6, 0, up.unit.parameter.Hp);
      if (num5 == up.unit.hp)
        return;
      int prevHp = up.unit.hp;
      this.btm.setTargetUnit(up, 0.0f, (GameObject) null, (System.Action) null, (System.Action) null, true);
      this.btm.setTargetUnit(up, 0.0f, incr.effect_group == null || !this.panelEffectPrefabs.ContainsKey(incr.effect_group.play_prefab_file_name) ? (GameObject) null : this.panelEffectPrefabs[incr.effect_group.play_prefab_file_name], (System.Action) (() =>
      {
        up.unit.hp = newChangeHp;
        this.dispHpNumberAnime(up.unit, prevHp, newChangeHp);
      }), (System.Action) null, false);
      this.btm.setEnableWait(1.3f);
      this.btm.setScheduleAction((System.Action) (() =>
      {
        if (this.battleManager.useGameEngine)
          this.battleManager.gameEngine.applyDeadUnit(up.unit, (BL.Unit) null);
        this.uiNode.hpCheckWithDeadEffects(up.unit, true);
      }), 0.0f, (System.Action) null, (Func<bool>) null, true);
    }), 0.0f, (System.Action) null, (Func<bool>) null, true);
    return false;
  }

  private void startBattleStartEffect()
  {
    BL.Story story = this.env.core.getFirstTurnStart();
    bool isBattleEnableControl = story != null && story.scriptId >= 0;
    ConditionForVictory conditionForVictory = this.battleManager.battleEffects.getConditionForVictory("Condition_forVictory");
    if ((UnityEngine.Object) conditionForVictory != (UnityEngine.Object) null)
    {
      int wave = 0;
      int maxWave = 0;
      if (this.battleManager.isWave)
      {
        wave = this.env.core.currentWave + 1;
        maxWave = this.battleManager.waveLength;
      }
      conditionForVictory.Initialize(this.env.core.condition.condition, wave, maxWave);
    }
    this.battleManager.battleEffects.startEffect("Condition_forVictory", 2.8f, (System.Action) (() => this.startStroyWithNextState(story, BL.Phase.turn_initialize)), isBattleEnableControl, (GameObject) null, false, false, (System.Action<GameObject>) null, (BattleEffects.CloneEnumlator) null);
  }

  private void startBattlePvpStartEffect()
  {
    this.battleManager.battleEffects.startEffect((Transform) null, 5f, (System.Action) (() => this.battleManager.gameEngine.startMain()), true, this.pvpMatchStartPrefab, false, false, (System.Action<GameObject>) null, (BattleEffects.CloneEnumlator) new BattleStateController.PvpMatchEffect(this), false, true);
  }

  private void startBattleFieldEffect(BL.FieldEffectType type)
  {
    this.btm.setScheduleAction((System.Action) (() =>
    {
      foreach (BL.FieldEffect fieldEffect in this.env.core.getFieldEffects(type))
        this.battleManager.battleEffects.startEffect(fieldEffect);
    }), 0.0f, (System.Action) null, (Func<bool>) null, true);
  }

  private void battleStartOvo()
  {
    this.btm.setScheduleAction((System.Action) (() =>
    {
      Singleton<CommonRoot>.GetInstance().isLoading = false;
      this.battleManager.gameEngine.readyComplited();
    }), 0.0f, (System.Action) null, (Func<bool>) null, false);
  }

  private void battleStartNormal()
  {
    this.btm.setScheduleAction((System.Action) (() =>
    {
      BL.Story story = this.env.core.getStory(BL.StoryType.battle_start);
      if (story == null || story.scriptId == 0)
      {
        Singleton<CommonRoot>.GetInstance().isLoading = false;
        Singleton<NGSceneManager>.GetInstance().StopTimer("BattleInit/");
      }
      this.startBattleFieldEffect(BL.FieldEffectType.battle_start);
      this.startStroyWithNextState(BL.StoryType.battle_start, BL.Phase.battle_start_init);
    }), 0.0f, (System.Action) null, (Func<bool>) null, false);
  }

  protected override void LateUpdate_Battle()
  {
    if (!this.battleManager.isPvp && !this.battleManager.isBattleEnable)
      return;
    if (!this.isStageClear && !this.isGameOver)
    {
      if (!this.battleManager.isOvo)
      {
        this.checkScriptUnitInArea();
        if (this.completedListModified.isChanged && this.completedListModified.value.value.Count > 0)
        {
          List<int> scripts = new List<int>();
          foreach (BL.UnitPosition unitPosition in this.completedListModified.value.value)
          {
            List<int> scripts1 = unitPosition.getScripts();
            if (scripts1 != null && scripts1.Count > 0)
            {
              scripts.AddRange((IEnumerable<int>) scripts1);
              unitPosition.resetScript();
            }
          }
          if (scripts.Count > 0)
          {
            this.btm.setScheduleAction((System.Action) (() =>
            {
              scripts.ForEach((System.Action<int>) (i => this.battleManager.startStory(new BL.Story(i, BL.StoryType.unit_in_area, (object[]) null))));
              this.btm.setPhaseState(this.env.core.phaseState.state, false);
            }), 0.0f, (System.Action) null, (Func<bool>) null, false);
            return;
          }
        }
      }
      if (this.completedListModified.isChangedOnce() && this.completedListModified.value.value.Count > 0)
      {
        foreach (BL.UnitPosition up in this.completedListModified.value.value)
        {
          if (!up.unit.isDead && up.unit.hp > 0)
          {
            BL.Phase changePhaseToPanel = this.env.core.getChangePhaseToPanel(up);
            if (changePhaseToPanel == BL.Phase.none)
            {
              if (!up.unit.mIsExecCompletedSkillEffect)
              {
                this.completedSkillEffects(up);
                this.completedPositionSkillEffects(up);
              }
              int[] ids = this.env.core.getReinforcementIDsToPanel(up);
              if (ids != null)
              {
                foreach (BL.UnitPosition unitPosition in this.env.core.unitPositions.value.Where<BL.UnitPosition>((Func<BL.UnitPosition, bool>) (x => x.unit.playerUnit.reinforcement != null && !x.unit.isDead && !x.unit.isEnable && ((IEnumerable<int>) ids).Contains<int>(x.unit.playerUnit.reinforcement.ID))))
                {
                  if (!this.env.core.spawnUnits.value.Contains(unitPosition))
                  {
                    this.env.core.spawnUnits.value.Add(unitPosition);
                    this.env.core.spawnUnits.commit();
                  }
                }
              }
            }
            else
            {
              this.btm.setPhaseState(changePhaseToPanel, true);
              return;
            }
          }
        }
        this.completedListModified.value.value.Clear();
        this.completedListModified.commit();
      }
      if (this.spawnUnitListModified.isChangedOnce() && this.spawnUnitListModified.value.value.Count > 0)
        this.btm.setScheduleAction((System.Action) (() =>
        {
          this.battleManager.getController<BattleInputObserver>().isTouchEnable = false;
          foreach (BL.UnitPosition up in this.spawnUnitListModified.value.value)
            this.spawnUnit(up);
          this.btm.setScheduleAction((System.Action) (() => this.battleManager.getController<BattleInputObserver>().isTouchEnable = true), 0.0f, (System.Action) null, (Func<bool>) null, false);
          this.env.core.spawnUnits.value.Clear();
          this.env.core.spawnUnits.commit();
        }), 0.0f, (System.Action) null, (Func<bool>) null, false);
      if (!this.battleManager.isOvo)
      {
        if (this.playerListModified.isChangedOnce())
        {
          this.btm.setScheduleAction((System.Action) (() =>
          {
            if (this.env.core.phaseState.state != BL.Phase.player)
              return;
            if (this.playerListModified.value.value.Count == 0)
            {
              this.btm.setPhaseState(BL.Phase.player_end, false);
            }
            else
            {
              if (!this.env.core.currentPhaseUnitp(this.env.core.unitCurrent.unit))
                return;
              this.btm.setCurrentUnit((BL.Unit) null, 0.1f, false);
            }
          }), 0.0f, (System.Action) null, (Func<bool>) null, false);
          return;
        }
        if (this.neutralListModified.isChangedOnce())
        {
          this.btm.setScheduleAction((System.Action) (() =>
          {
            if (this.env.core.phaseState.state != BL.Phase.neutral || this.neutralListModified.value.value.Count != 0)
              return;
            this.btm.setPhaseState(BL.Phase.neutral_end, false);
          }), 0.0f, (System.Action) null, (Func<bool>) null, false);
          return;
        }
        if (this.enemyListModified.isChangedOnce())
        {
          this.btm.setScheduleAction((System.Action) (() =>
          {
            if (this.env.core.phaseState.state != BL.Phase.enemy || this.enemyListModified.value.value.Count != 0)
              return;
            this.btm.setPhaseState(BL.Phase.enemy_end, false);
          }), 0.0f, (System.Action) null, (Func<bool>) null, false);
          return;
        }
      }
    }
    if (!this.phaseStateModified.isChangedOnce())
      return;
    switch (this.phaseStateModified.value.state)
    {
      case BL.Phase.player_start:
        this.battleManager.saveEnvironment(false);
        this.aiController.stopAIAction();
        Singleton<NGSoundManager>.GetInstance().PlayBgmFile(this.env.core.stage.stage.field_player_bgm_file, this.env.core.stage.stage.field_player_bgm, 0, 0.0f, 0.5f, 0.5f);
        this.startBattleFieldEffect(BL.FieldEffectType.player_start);
        if (this.env.core.playerActionUnits.value.Count > 0)
          this.setStartTarget(this.env.core.playerActionUnits.value[0], true);
        this.btm.setScheduleAction((System.Action) (() =>
        {
          this.setStateWithEffect("PlayerPhase", 1.5f, BL.Phase.player_start_post, (System.Action) null, true);
          this.isAutoBattleModified.notifyChanged();
        }), 0.0f, (System.Action) null, (Func<bool>) null, false);
        foreach (BL.UnitPosition up in this.env.core.playerActionUnits.value)
          this.execPanelLandformEffect(this.env.core.getFieldPanel(up, false), up, BattleLandformEffectPhase.phase_start);
        this.executeSkillEffects(this.env.core.playerActionUnits.value.Concat<BL.UnitPosition>(this.env.core.facilityUnits.value.Where<BL.Unit>((Func<BL.Unit, bool>) (x => x.facility.thisForce == BL.ForceID.player && x.isEnable && !x.isDead)).Select<BL.Unit, BL.UnitPosition>((Func<BL.Unit, BL.UnitPosition>) (x => this.env.core.getUnitPosition(x)))).ToList<BL.UnitPosition>());
        break;
      case BL.Phase.neutral_start:
        if (this.neutralListModified.value.value.Count == 0)
        {
          this.neutralListModified.notifyChanged();
          break;
        }
        this.aiController.stopAIAction();
        this.env.core.resetActionList(BL.ForceID.neutral);
        this.startBattleFieldEffect(BL.FieldEffectType.neutral_start);
        this.setStartTarget(this.env.core.neutralActionUnits.value[0], false);
        this.btm.setScheduleAction((System.Action) (() =>
        {
          this.battleManager.saveEnvironment(false);
          this.setStateWithEffect("NeutralPhase", 1.5f, BL.Phase.neutral_start_post, (System.Action) null, true);
        }), 0.0f, (System.Action) null, (Func<bool>) null, false);
        foreach (BL.UnitPosition up in this.env.core.neutralActionUnits.value)
          this.execPanelLandformEffect(this.env.core.getFieldPanel(up, false), up, BattleLandformEffectPhase.phase_start);
        this.executeSkillEffects(this.env.core.neutralActionUnits.value.Concat<BL.UnitPosition>(this.env.core.facilityUnits.value.Where<BL.Unit>((Func<BL.Unit, bool>) (x => x.facility.thisForce == BL.ForceID.neutral && x.isEnable && !x.isDead)).Select<BL.Unit, BL.UnitPosition>((Func<BL.Unit, BL.UnitPosition>) (x => this.env.core.getUnitPosition(x)))).ToList<BL.UnitPosition>());
        break;
      case BL.Phase.enemy_start:
        this.aiController.stopAIAction();
        this.env.core.resetActionList(BL.ForceID.enemy);
        Singleton<NGSoundManager>.GetInstance().PlayBgmFile(this.env.core.stage.stage.field_enemy_bgm_file, this.env.core.stage.stage.field_enemy_bgm, 0, 0.0f, 0.5f, 0.5f);
        this.startBattleFieldEffect(BL.FieldEffectType.enemy_start);
        this.setStartTarget(this.env.core.enemyActionUnits.value[0], false);
        this.btm.setScheduleAction((System.Action) (() =>
        {
          this.battleManager.saveEnvironment(false);
          this.setStateWithEffect("EnemyPhase", 1.5f, BL.Phase.enemy_start_post, (System.Action) null, true);
        }), 0.0f, (System.Action) null, (Func<bool>) null, false);
        foreach (BL.UnitPosition up in this.env.core.enemyActionUnits.value)
          this.execPanelLandformEffect(this.env.core.getFieldPanel(up, false), up, BattleLandformEffectPhase.phase_start);
        this.executeSkillEffects(this.env.core.enemyActionUnits.value.Concat<BL.UnitPosition>(this.env.core.facilityUnits.value.Where<BL.Unit>((Func<BL.Unit, bool>) (x => x.facility.thisForce == BL.ForceID.enemy && x.isEnable && !x.isDead)).Select<BL.Unit, BL.UnitPosition>((Func<BL.Unit, BL.UnitPosition>) (x => this.env.core.getUnitPosition(x)))).ToList<BL.UnitPosition>());
        break;
      case BL.Phase.player_end:
        if (this.deadCheck())
          break;
        if (this.env.core.condition.isTurn && this.env.core.phaseState.turnCount + 1 > this.env.core.condition.turn)
        {
          if (this.env.core.battleInfo.quest_type == CommonQuestType.GuildRaid)
            this.isTurnOver = true;
          this.btm.setPhaseState(BL.Phase.gameover, false);
          break;
        }
        if (this.env.core.neutralUnits.value.Any<BL.Unit>((Func<BL.Unit, bool>) (x => !x.isDead && x.isEnable)))
        {
          Resources.UnloadUnusedAssets();
          this.btm.setPhaseState(BL.Phase.neutral_start, false);
          break;
        }
        if (!this.env.core.enemyUnits.value.Any<BL.Unit>((Func<BL.Unit, bool>) (x => !x.isDead && x.isEnable)))
          break;
        Resources.UnloadUnusedAssets();
        this.btm.setPhaseState(BL.Phase.enemy_start, false);
        break;
      case BL.Phase.neutral_end:
        if (this.deadCheck())
          break;
        Resources.UnloadUnusedAssets();
        this.btm.setPhaseState(BL.Phase.enemy_start, false);
        break;
      case BL.Phase.enemy_end:
        if (this.deadCheck())
          break;
        Resources.UnloadUnusedAssets();
        this.btm.setPhaseState(BL.Phase.turn_initialize, false);
        break;
      case BL.Phase.turn_initialize:
        this.aiController.stopAIAction();
        this.btm.setCurrentUnit((BL.Unit) null, 0.1f, false);
        if (this.env.core.condition.isElapsedTurn && this.env.core.phaseState.turnCount > this.env.core.condition.elapsedTurn)
        {
          this.btm.setPhaseState(this.battleManager.isWave ? BL.Phase.stageclear_pre : BL.Phase.stageclear, true);
          break;
        }
        this.btm.setScheduleAction((System.Action) (() =>
        {
          this.env.core.nextRandom();
          this.startBattleFieldEffect(BL.FieldEffectType.turn_start);
          foreach (BL.UnitPosition up2 in this.env.core.unitPositions.value)
          {
            if (up2.unit.spawnTurn == this.env.core.phaseState.turnCount)
              this.spawnUnit(up2);
          }
          this.btm.setScheduleAction((System.Action) (() =>
          {
            this.env.core.resetActionList(BL.ForceID.player);
            this.env.core.resetActionList(BL.ForceID.neutral);
            this.env.core.resetActionList(BL.ForceID.enemy);
            this.env.core.createDangerAria();
          }), 0.0f, (System.Action) null, (Func<bool>) null, false);
        }), 0.0f, (System.Action) null, (Func<bool>) null, true);
        foreach (BL.UnitPosition up in this.env.core.unitPositions.value)
          this.execPanelLandformEffect(this.env.core.getFieldPanel(up, false), up, BattleLandformEffectPhase.turn_start);
        this.executeTurnInitSkillEffects(this.env.core.unitPositions.value.Where<BL.UnitPosition>((Func<BL.UnitPosition, bool>) (x => x.unit.isSpawned)).ToList<BL.UnitPosition>(), this.env.core.phaseState.absoluteTurnCount);
        if (Singleton<NGBattleManager>.GetInstance().isOvo)
          this.executeSkillEffects(this.env.core.unitPositions.value.Where<BL.UnitPosition>((Func<BL.UnitPosition, bool>) (x => !x.unit.isDead && x.unit.isEnable)).ToList<BL.UnitPosition>());
        this.btm.setScheduleAction((System.Action) (() =>
        {
          this.env.core.createDangerAria();
          if (this.battleManager.isOvo)
            this.battleManager.gameEngine.turnInitializeCompleted();
          else
            this.btm.setPhaseState(BL.Phase.player_start, false);
        }), 0.0f, (System.Action) null, (Func<bool>) null, false);
        break;
      case BL.Phase.win_finalize:
        this.btm.setScheduleAction((System.Action) (() => this.startStroyWithNextState(BL.StoryType.battle_win, BL.Phase.finalize)), 0.0f, (System.Action) null, (Func<bool>) null, true);
        break;
      case BL.Phase.finalize:
        Time.timeScale = 1f;
        if ((UnityEngine.Object) this.uiNode != (UnityEngine.Object) null)
          this.uiNode.SavePVPConfig();
        this.btm.setScheduleAction((System.Action) (() =>
        {
          this.battleManager.isSuspend = false;
          if (this.battleManager.isPvp)
            this.gameObject.AddComponent<BattlePvpFinalize>();
          else if (this.battleManager.isPvnpc)
            this.gameObject.AddComponent<BattlePvnpcFinalize>();
          else if (this.battleManager.isGvg)
            this.gameObject.AddComponent<BattleGvgFinalize>();
          else if (this.battleManager.isWave)
            this.gameObject.AddComponent<BattleWaveFinalize>();
          else if (this.battleManager.isRaid)
            this.gameObject.AddComponent<BattleRaidFinalize>();
          else if (this.battleManager.isTower)
            this.gameObject.AddComponent<BattleTowerFinalize>();
          else
            this.gameObject.AddComponent<BattleFinalize>();
          App.SetAutoSleep(true);
        }), 0.0f, (System.Action) null, (Func<bool>) null, false);
        break;
      case BL.Phase.suspend:
        if (!this.battleManager.isEarth)
          break;
        Time.timeScale = 1f;
        this.btm.setScheduleAction((System.Action) (() =>
        {
          this.battleManager.isSuspend = true;
          App.SetAutoSleep(true);
          Singleton<EarthDataManager>.GetInstance().SuspendEarthMode();
        }), 0.0f, (System.Action) null, (Func<bool>) null, false);
        break;
      case BL.Phase.player_start_post:
        List<BL.Story> sl1 = this.env.core.battleInfo.isWave ? this.env.core.getStoryWaveOffense(this.env.core.phaseState.turnCount, this.env.core.nowWaveNo) : this.env.core.getStoryOffense(this.env.core.phaseState.turnCount);
        if (sl1 != null && sl1.Count > 0)
        {
          sl1[0].isRead = true;
          this.btm.setScheduleAction((System.Action) (() => this.startStroyWithNextState(sl1[0], BL.Phase.player)), 0.0f, (System.Action) null, (Func<bool>) null, false);
          break;
        }
        this.btm.setPhaseState(BL.Phase.player, false);
        break;
      case BL.Phase.neutral_start_post:
        this.btm.setPhaseState(BL.Phase.neutral, false);
        break;
      case BL.Phase.enemy_start_post:
        List<BL.Story> sl2 = this.env.core.battleInfo.isWave ? this.env.core.getStoryWaveDefense(this.env.core.phaseState.turnCount, this.env.core.nowWaveNo) : this.env.core.getStoryDefense(this.env.core.phaseState.turnCount);
        if (sl2 != null && sl2.Count > 0)
        {
          sl2[0].isRead = true;
          this.btm.setScheduleAction((System.Action) (() => this.startStroyWithNextState(sl2[0], BL.Phase.enemy)), 0.0f, (System.Action) null, (Func<bool>) null, false);
          break;
        }
        this.btm.setPhaseState(BL.Phase.enemy, false);
        break;
      case BL.Phase.all_dead_player:
        this.aiController.stopAIAction();
        this.btm.setScheduleAction((System.Action) (() =>
        {
          if (this.env.core.battleInfo.isContinueEnable)
          {
            this.battleManager.popupOpen(this.popupAllDeadPlayerPrefab, false, (EventDelegate) null, false, false, false, true, false);
            this.btm.setPhaseState(BL.Phase.none, false);
          }
          else
            this.btm.setPhaseState(BL.Phase.gameover, false);
        }), 0.0f, (System.Action) null, (Func<bool>) null, false);
        break;
      case BL.Phase.all_dead_neutral:
        this.aiController.stopAIAction();
        break;
      case BL.Phase.all_dead_enemy:
        this.aiController.stopAIAction();
        this.btm.setPhaseState(this.battleManager.isWave ? BL.Phase.stageclear_pre : BL.Phase.stageclear, true);
        break;
      case BL.Phase.stageclear_pre:
        this.aiController.stopAIAction();
        List<BL.Story> sl3 = this.battleManager.isWave ? this.env.core.getStoryWaveClear(this.env.core.nowWaveNo) : (List<BL.Story>) null;
        if (sl3 != null && sl3.Count > 0)
        {
          sl3[0].isRead = true;
          this.btm.setScheduleAction((System.Action) (() => this.startStroyWithNextState(sl3[0], BL.Phase.stageclear)), 0.0f, (System.Action) null, (Func<bool>) null, false);
          break;
        }
        this.btm.setPhaseState(BL.Phase.stageclear, false);
        break;
      case BL.Phase.stageclear:
        if (this.isStageClear || this.isGameOver)
          break;
        this.isStageClear = true;
        this.aiController.stopAIAction();
        if (this.battleManager.isWave && this.env.core.nowWaveNo < this.battleManager.waveLength)
        {
          this.startBattleFieldEffect(BL.FieldEffectType.waveclear);
          this.setStateWithEffect("WaveClear", 2.5f, BL.Phase.wave_start, (System.Action) null, false);
          break;
        }
        this.btm.setScheduleAction((System.Action) (() =>
        {
          Singleton<NGSoundManager>.GetInstance().playVoiceByID(this.env.core.playerUnits.value[0].unit.unitVoicePattern, 71, 0, 0.0f);
          this.startBattleFieldEffect(BL.FieldEffectType.stageclear);
          this.setStateWithEffect("StageClear", 5f, BL.Phase.win_finalize, (System.Action) (() => this.env.core.isWin = true), false);
        }), 0.0f, (System.Action) null, (Func<bool>) null, false);
        break;
      case BL.Phase.gameover:
        if (this.isStageClear || this.isGameOver)
          break;
        this.isGameOver = true;
        bool needNabi = !this.battleManager.isOvo && !this.env.core.battleInfo.isEarthMode && (this.env.core.battleInfo.quest_type != CommonQuestType.GuildRaid && this.env.core.battleInfo.isFirstAllDead) && this.env.core.allDeadUnitsp(BL.ForceID.player);
        this.aiController.stopAIAction();
        string effectName = this.isTurnOver ? "dir_TurnOver" : "GameOver";
        this.btm.setScheduleAction((System.Action) (() =>
        {
          if (!this.isTurnOver)
            Singleton<NGSoundManager>.GetInstance().playVoiceByID(this.env.core.playerUnits.value[0].unit.unitVoicePattern, 72, 0, 0.0f);
          this.setStateWithEffect(effectName, 5f, BL.Phase.finalize, (System.Action) (() => this.env.core.isWin = false), needNabi);
        }), 0.0f, (System.Action) null, (Func<bool>) null, false);
        break;
      case BL.Phase.surrender:
        if (this.isStageClear || this.isGameOver)
          break;
        this.isGameOver = true;
        this.aiController.stopAIAction();
        this.btm.setSchedule((Schedule) new BattleStateController.EffectAIWait(this));
        this.env.core.isWin = false;
        this.btm.setPhaseState(BL.Phase.finalize, false);
        break;
      case BL.Phase.pvp_player_start:
        Singleton<NGSoundManager>.GetInstance().PlayBgmFile(this.env.core.stage.stage.field_player_bgm_file, this.env.core.stage.stage.field_player_bgm, 0, 0.0f, 0.5f, 0.5f);
        this.startBattleFieldEffect(BL.FieldEffectType.pvp_change_player);
        BL.UnitPosition unitPosition1 = this.env.core.playerActionUnits.value.FirstOrDefault<BL.UnitPosition>((Func<BL.UnitPosition, bool>) (x => x.cantChangeCurrent));
        BL.UnitPosition up1 = (BL.UnitPosition) null;
        if (unitPosition1 != null)
        {
          up1 = unitPosition1;
          this.env.core.unitCurrent.commit();
        }
        else
        {
          if (this.env.core.playerActionUnits.value.Count > 0)
            up1 = this.env.core.playerActionUnits.value[0];
          this.btm.setCurrentUnit((BL.Unit) null, 0.1f, false);
        }
        if (up1 != null)
          this.setStartTarget(up1, true);
        this.btm.setPhaseState(BL.Phase.player, false);
        break;
      case BL.Phase.pvp_enemy_start:
        Singleton<NGSoundManager>.GetInstance().PlayBgmFile(this.env.core.stage.stage.field_enemy_bgm_file, this.env.core.stage.stage.field_enemy_bgm, 0, 0.0f, 0.5f, 0.5f);
        this.startBattleFieldEffect(BL.FieldEffectType.pvp_change_enemy);
        if (this.env.core.enemyActionUnits.value.Count > 0)
          this.setStartTarget(this.env.core.enemyActionUnits.value[0], false);
        this.btm.setPhaseState(BL.Phase.enemy, false);
        break;
      case BL.Phase.pvp_result:
        if ((UnityEngine.Object) this.pvpManager != (UnityEngine.Object) null)
          this.pvpManager.isResult = true;
        if ((UnityEngine.Object) Singleton<PVNpcManager>.GetInstanceOrNull() != (UnityEngine.Object) null)
          Singleton<PVNpcManager>.GetInstance().isResult = true;
        if ((UnityEngine.Object) Singleton<GVGManager>.GetInstanceOrNull() != (UnityEngine.Object) null)
          Singleton<GVGManager>.GetInstance().isResult = true;
        this.btm.setPhaseState(BL.Phase.finalize, false);
        break;
      case BL.Phase.pvp_start_init:
        this.btm.setScheduleAction((System.Action) (() => this.startBattlePvpStartEffect()), 0.0f, (System.Action) null, (Func<bool>) null, false);
        break;
      case BL.Phase.pvp_exception:
        NGSceneManager sm = Singleton<NGSceneManager>.GetInstance();
        if (sm.sceneName != this.battleManager.topScene)
          this.btm.setScheduleAction((System.Action) (() => sm.backScene()), 0.0f, (System.Action) null, (Func<bool>) null, false);
        ModalWindow.ShowYesNo(Consts.GetInstance().VERSUS_02694POPUP_TITLE, this.pvpManager.getErrorMessage(this.pvpManager.exception), (System.Action) (() => this.pvpManager.errorRecovery()), (System.Action) (() => this.btm.setPhaseState(BL.Phase.finalize, false)));
        this.btm.setPhaseState(BL.Phase.none, false);
        break;
      case BL.Phase.wave_start:
        this.btm.setSchedule((Schedule) new BattleStateController.WaveStart(this, this.env, this.env.core.nowWaveNo));
        this.btm.setPhaseState(BL.Phase.battle_start_init, false);
        break;
      case BL.Phase.battle_start:
        this.battleManager.saveEnvironment(false);
        this.btm.setTargetPanel(this.env.core.getFieldPanel(this.env.core.getUnitPosition(this.env.core.playerUnits.value[0]), false), 0.0f, (System.Action) null, (System.Action) null, true);
        if (this.battleManager.isOvo)
        {
          this.battleStartOvo();
          break;
        }
        this.battleStartNormal();
        break;
      case BL.Phase.battle_start_init:
        this.battleManager.saveEnvironment(false);
        this.isStageClear = false;
        this.isGameOver = false;
        this.isTurnOver = false;
        this.startBattleFieldEffect(BL.FieldEffectType.first_turn_start);
        this.btm.setScheduleAction((System.Action) (() => this.startBattleStartEffect()), 0.0f, (System.Action) null, (Func<bool>) null, false);
        break;
    }
  }

  private void updateCurrentUnitPosition()
  {
    if (this.currentUnitPosition == this.env.core.currentUnitPosition)
      return;
    this.currentUnitPosition = this.env.core.currentUnitPosition;
    this.currentUnitActionCount = this.currentUnitPosition != null ? this.currentUnitPosition.unit.skillEffects.GetCompleteCount() : -1;
  }

  private bool isCurrentUnitActionCountedOnce
  {
    get
    {
      bool flag = false;
      if (this.currentUnitPosition != null)
      {
        flag = this.currentUnitActionCount != this.currentUnitPosition.completedCount;
        this.currentUnitActionCount = this.currentUnitPosition.completedCount;
      }
      return flag;
    }
  }

  private void checkScriptUnitInArea()
  {
    if (this.env.core.unitCurrent.unit == (BL.Unit) null)
      return;
    this.updateCurrentUnitPosition();
    switch (this.phaseStateModified.value.state)
    {
      case BL.Phase.player:
        if (!this.isCurrentUnitActionCountedOnce)
          break;
        BL.UnitPosition currentUnitPosition1 = this.env.core.currentUnitPosition;
        using (List<BL.Story>.Enumerator enumerator = this.env.core.getStoryOffenseInArea(currentUnitPosition1.originalRow, currentUnitPosition1.originalColumn).GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            BL.Story current = enumerator.Current;
            current.isRead = true;
            currentUnitPosition1.setScript(current.scriptId);
          }
          break;
        }
      case BL.Phase.neutral:
        int num = this.isCurrentUnitActionCountedOnce ? 1 : 0;
        break;
      case BL.Phase.enemy:
        if (!this.isCurrentUnitActionCountedOnce)
          break;
        BL.UnitPosition currentUnitPosition2 = this.env.core.currentUnitPosition;
        using (List<BL.Story>.Enumerator enumerator = this.env.core.getStoryDefenseInArea(currentUnitPosition2.originalRow, currentUnitPosition2.originalColumn).GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            BL.Story current = enumerator.Current;
            current.isRead = true;
            currentUnitPosition2.setScript(current.scriptId);
          }
          break;
        }
    }
  }

  private class EffectAIWait : ScheduleEnumerator
  {
    private BattleStateController parent;

    public EffectAIWait(BattleStateController parent)
    {
      this.parent = parent;
    }

    public override IEnumerator doBody()
    {
      BattleStateController.EffectAIWait effectAiWait = this;
      while (effectAiWait.parent.aiController.isAction)
        yield return (object) null;
      effectAiWait.isCompleted = true;
    }
  }

  private class PvpMatchEffect : BattleEffects.CloneEnumlator
  {
    private BattleStateController parent;

    public PvpMatchEffect(BattleStateController parent)
    {
      this.parent = parent;
    }

    public override IEnumerator doBody(GameObject o)
    {
      PopupPvpStart component = o.GetComponent<PopupPvpStart>();
      IGameEngine gameEngine = this.parent.battleManager.gameEngine;
      if ((UnityEngine.Object) component != (UnityEngine.Object) null && gameEngine != null)
      {
        string pGuild = (string) null;
        string eGuild = (string) null;
        if (gameEngine is GVGManager)
        {
          GVGManager gvgManager = gameEngine as GVGManager;
          pGuild = gvgManager.playerGuildName;
          eGuild = gvgManager.enemyGuildName;
        }
        IEnumerator e = component.Initialize(gameEngine.playerName, gameEngine.enemyName, gameEngine.playerEmblem, gameEngine.enemyEmblem, this.parent.env.core.playerUnits.value, this.parent.env.core.enemyUnits.value, pGuild, eGuild);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
      }
    }
  }

  private class WaveStart : ScheduleEnumerator
  {
    private BattleStateController parent;
    private BE env;
    private int wave;

    public WaveStart(BattleStateController parent, BE env, int wave)
    {
      this.parent = parent;
      this.env = env;
      this.wave = wave;
    }

    public override IEnumerator doBody()
    {
      BattleStateController.WaveStart waveStart = this;
      NGBattleManager bm = waveStart.parent.battleManager;
      NGBattle3DObjectManager m3d = bm.getManager<NGBattle3DObjectManager>();
      CommonRoot cr = Singleton<CommonRoot>.GetInstance();
      cr.isLoading = true;
      yield return (object) null;
      waveStart.env.pushWaveStageDatas();
      m3d.setRootActive(false);
      if (waveStart.env.core.battleInfo.isWave)
      {
        Battle3DRoot objectOfType = UnityEngine.Object.FindObjectOfType<Battle3DRoot>();
        if ((UnityEngine.Object) objectOfType != (UnityEngine.Object) null)
          objectOfType.mapPoint.gameObject.SetActive(true);
      }
      yield return (object) null;
      IEnumerator e = new BattleLogicInitializer().initializeWave(waveStart.wave, waveStart.env.core.battleInfo, waveStart.env.core);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      yield return (object) null;
      e = m3d.loadStage(waveStart.env, true);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      yield return (object) null;
      e = m3d.loadUnitResources(waveStart.env);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      yield return (object) null;
      e = m3d.spawns(waveStart.env.core.unitPositions.value, waveStart.env, false);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      yield return (object) null;
      BattleFuncs.createAsterNodeCache(waveStart.env.core);
      waveStart.env.core.clearRouteCache();
      waveStart.parent.aiController.clearCache();
      waveStart.parent.uiNode.initializeStage();
      BattleCameraController controller = bm.getController<BattleCameraController>();
      BL.Panel fieldPanel = waveStart.env.core.getFieldPanel(waveStart.env.core.getUnitPosition(waveStart.env.core.playerUnits.value[0]), false);
      if (fieldPanel != null)
      {
        controller.setLookAtTarget(fieldPanel, true);
        waveStart.env.core.setCurrentField(fieldPanel);
      }
      m3d.setRootActive(true);
      yield return (object) null;
      cr.isLoading = false;
      waveStart.isCompleted = true;
    }
  }
}
