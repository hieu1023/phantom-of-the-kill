﻿// Decompiled with JetBrains decompiler
// Type: Shop00720Prefabs
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;
using UnityEngine;

public class Shop00720Prefabs
{
  private GameObject dirSlotReward;
  private GameObject dirSlotList;
  private GameObject dirSlotPattern;
  private GameObject dirList;

  public GameObject DirSlotReward
  {
    get
    {
      return this.dirSlotReward;
    }
  }

  public GameObject DirSlotList
  {
    get
    {
      return this.dirSlotList;
    }
  }

  public GameObject DirSlotPattern
  {
    get
    {
      return this.dirSlotPattern;
    }
  }

  public GameObject DirList
  {
    get
    {
      return this.dirList;
    }
  }

  public IEnumerator GetPrefabs()
  {
    Future<GameObject> prefabF = Res.Prefabs.shop007_20.dir_slot_reward.Load<GameObject>();
    IEnumerator e = prefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.dirSlotReward = prefabF.Result;
    prefabF = Res.Prefabs.shop007_20.dir_slotList.Load<GameObject>();
    e = prefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.dirSlotList = prefabF.Result;
    prefabF = Res.Prefabs.shop007_20.dir_slot_pattern.Load<GameObject>();
    e = prefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.dirSlotPattern = prefabF.Result;
    prefabF = Res.Prefabs.shop007_20.dir_List.Load<GameObject>();
    e = prefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.dirList = prefabF.Result;
  }
}
