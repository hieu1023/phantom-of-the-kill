﻿// Decompiled with JetBrains decompiler
// Type: Bugu00561Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using System.Collections;
using UnityEngine;

public class Bugu00561Menu : BackButtonMenuBase
{
  [SerializeField]
  protected UILabel TxtDescription;
  [SerializeField]
  protected UILabel SupplyIntroduction;
  [SerializeField]
  protected UILabel GearIntroduction;
  [SerializeField]
  protected UILabel SupplyOwnednumber;
  [SerializeField]
  protected UILabel GearOwnednumber;
  [SerializeField]
  protected UILabel TxtTitle;
  [SerializeField]
  protected UI2DSprite LinkItem;
  [SerializeField]
  protected GameObject SupplyDetail;
  [SerializeField]
  protected GameObject GearDetail;
  [SerializeField]
  protected GameObject TouchBack;
  [SerializeField]
  protected BoxCollider ibtnBackCollider;
  [SerializeField]
  protected TweenAlpha slc_NewIcon;

  public IEnumerator InitDetailedScreen(GearGear gear)
  {
    if ((UnityEngine.Object) this.SupplyDetail != (UnityEngine.Object) null)
      this.SupplyDetail.SetActive(false);
    if ((UnityEngine.Object) this.GearDetail != (UnityEngine.Object) null)
      this.GearDetail.SetActive(true);
    Future<UnityEngine.Sprite> spriteF = gear.LoadSpriteBasic(1f);
    IEnumerator e = spriteF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.LinkItem.sprite2D = spriteF.Result;
    this.LinkItem.width = Mathf.FloorToInt(spriteF.Result.textureRect.width);
    this.LinkItem.height = Mathf.FloorToInt(spriteF.Result.textureRect.height);
    if ((UnityEngine.Object) this.TxtTitle != (UnityEngine.Object) null)
      this.TxtTitle.SetTextLocalize(gear.name);
    this.GearIntroduction.SetTextLocalize(gear.description);
  }

  public IEnumerator InitDetailedScreen(
    GameCore.ItemInfo item,
    bool isNew,
    bool isScreenTouch,
    int counter = 0)
  {
    this.TouchBack.SetActive(false);
    if (isScreenTouch)
      this.ibtnBackCollider.size = new Vector3(2000f, 2000f, 0.0f);
    this.slc_NewIcon.gameObject.SetActive(isNew);
    if (isNew)
      this.slc_NewIcon.PlayForward();
    IEnumerator e;
    if (!item.isSupply)
    {
      e = this.InitDetailedScreen(item.gear);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.GearOwnednumber.SetTextLocalize(item.quantity.ToLocalizeNumberText());
    }
    else
    {
      this.SupplyDetail.SetActive(true);
      this.GearDetail.SetActive(false);
      Future<UnityEngine.Sprite> spriteF = item.supply.LoadSpriteBasic();
      e = spriteF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.LinkItem.sprite2D = spriteF.Result;
      UI2DSprite linkItem1 = this.LinkItem;
      Rect textureRect = spriteF.Result.textureRect;
      int num1 = Mathf.FloorToInt(textureRect.width);
      linkItem1.width = num1;
      UI2DSprite linkItem2 = this.LinkItem;
      textureRect = spriteF.Result.textureRect;
      int num2 = Mathf.FloorToInt(textureRect.height);
      linkItem2.height = num2;
      if ((UnityEngine.Object) this.TxtTitle != (UnityEngine.Object) null)
        this.TxtTitle.SetTextLocalize(item.supply.name);
      this.SupplyOwnednumber.SetTextLocalize(item.quantity.ToLocalizeNumberText());
      this.TxtDescription.SetTextLocalize(item.supply.description);
      this.SupplyIntroduction.SetTextLocalize(item.supply.flavor);
      spriteF = (Future<UnityEngine.Sprite>) null;
    }
    if (counter > 1)
      ModalWindow.Show(Consts.Format(Consts.GetInstance().GACHA_0061MULTIPLE_WEAPONS_TITLE, (IDictionary) null), Consts.Format(Consts.GetInstance().GACHA_0061MULTIPLE_WEAPONS_NUM, (IDictionary) new Hashtable()
      {
        {
          (object) "weapon",
          (object) item.name.ToString()
        },
        {
          (object) "num",
          (object) counter.ToString()
        }
      }), (System.Action) (() => {}));
  }

  public void IbtnBack()
  {
    if (this.IsPushAndSet())
      return;
    this.backScene();
  }

  public override void onBackButton()
  {
    this.IbtnBack();
  }
}
