﻿// Decompiled with JetBrains decompiler
// Type: GuideUnitUnit
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using System;
using UnityEngine;

[Serializable]
public class GuideUnitUnit
{
  [SerializeField]
  private bool isMaterial;
  private UnitUnit unit;
  private GearGear gear;
  private DateTime history;

  public GuideUnitUnit()
  {
  }

  public GuideUnitUnit(UnitUnit unit_)
  {
    this.unit = unit_;
    this.history = new DateTime();
  }

  public GuideUnitUnit(GearGear gear_)
  {
    this.gear = gear_;
    this.history = new DateTime();
  }

  public bool IsMaterial
  {
    get
    {
      return this.isMaterial;
    }
    set
    {
      this.isMaterial = value;
    }
  }

  public UnitUnit Unit
  {
    get
    {
      return this.unit;
    }
    set
    {
      this.unit = value;
      this.isMaterial = this.unit.IsMaterialUnit;
    }
  }

  public GearGear Gear
  {
    get
    {
      return this.gear;
    }
    set
    {
      this.gear = value;
    }
  }

  public DateTime History
  {
    get
    {
      return this.history;
    }
    set
    {
      this.history = value;
    }
  }
}
