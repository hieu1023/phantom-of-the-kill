﻿// Decompiled with JetBrains decompiler
// Type: SheetGachaGetBonus
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using SM;
using System.Collections;
using UnityEngine;

public class SheetGachaGetBonus : MonoBehaviour
{
  public GameObject IconObject;
  public UILabel CompleteText;
  [SerializeField]
  private SheetGachaAnim anim;

  public IEnumerator Init(GachaG007PlayerPanel panel, System.Action endAction)
  {
    CreateIconObject target = this.IconObject.GetOrAddComponent<CreateIconObject>();
    if (panel.reward_type_id.HasValue)
    {
      IEnumerator e = target.CreateThumbnail((MasterDataTable.CommonRewardType) panel.reward_type_id.Value, panel.reward_id.HasValue ? panel.reward_id.Value : 0, panel.reward_quantity.HasValue ? panel.reward_quantity.Value : 0, true, true, new CommonQuestType?(), false);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      if (panel.reward_type_id.Value == 1 || panel.reward_type_id.Value == 24)
      {
        target.GetIcon().GetComponent<UnitIcon>().setLevelText("1");
        target.GetIcon().GetComponent<UnitIcon>().ShowBottomInfo(UnitSortAndFilter.SORT_TYPES.Level);
      }
      else if (panel.reward_type_id.Value != 3 && panel.reward_type_id.Value != 26 && panel.reward_type_id.Value != 35)
      {
        int num = panel.reward_type_id.Value;
      }
      this.CompleteText.SetTextLocalize(CommonRewardType.GetRewardName((MasterDataTable.CommonRewardType) panel.reward_type_id.Value, panel.reward_id.HasValue ? panel.reward_id.Value : 0, panel.reward_quantity.HasValue ? panel.reward_quantity.Value : 0, false));
      NGSoundManager instance = Singleton<NGSoundManager>.GetInstance();
      if ((UnityEngine.Object) instance != (UnityEngine.Object) null)
        instance.playSE("SE_0546", false, 0.0f, -1);
    }
    this.anim.Init(endAction);
  }
}
