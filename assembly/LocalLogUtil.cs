﻿// Decompiled with JetBrains decompiler
// Type: LocalLogUtil
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.IO;
using System.Text;
using UnityEngine;

public class LocalLogUtil
{
  private static string m_logFilePath = StorageUtil.persistentDataPath + "/error.log";

  public static void Init()
  {
    File.Delete(LocalLogUtil.m_logFilePath);
    LocalLogUtil.Append(DateTime.Now.ToString() + ": Initialize log file.\r\n");
  }

  public static void LogFatalError(string logMessage, string stackTrace, LogType logType)
  {
    switch (logType)
    {
      case LogType.Error:
      case LogType.Assert:
      case LogType.Exception:
        LocalLogUtil.Append("\r\n" + logMessage + "\r\n" + stackTrace);
        break;
    }
  }

  private static void Append(string message)
  {
    byte[] bytes = Encoding.UTF8.GetBytes(message);
    using (FileStream fileStream = new FileStream(LocalLogUtil.m_logFilePath, FileMode.Append))
    {
      fileStream.Write(bytes, 0, bytes.Length);
      fileStream.Close();
    }
  }
}
