﻿// Decompiled with JetBrains decompiler
// Type: Guild0281RaidButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Guild0281RaidButton : MonoBehaviour
{
  [SerializeField]
  private GameObject open;
  [SerializeField]
  private UILabel openTime;
  [SerializeField]
  private GameObject close;
  [SerializeField]
  private GameObject outOfTerm;
  [SerializeField]
  private GameObject aggregating;
  [SerializeField]
  private UIGrid dirRPgrid;
  [SerializeField]
  private GameObject[] slcRaidRPIconBase;
  [SerializeField]
  private GameObject[] slcRaidRPIconOff;
  [SerializeField]
  private GameObject[] slcRaidRPIconOn;
  [SerializeField]
  private GameObject dirFrameLight;
  private GameObject frameLightPrefab;

  private void EventClose()
  {
    this.open.SetActive(false);
    this.close.SetActive(true);
    this.outOfTerm.SetActive(true);
    this.aggregating.SetActive(false);
  }

  private void EventAggregating()
  {
    this.open.SetActive(false);
    this.close.SetActive(true);
    this.outOfTerm.SetActive(false);
    this.aggregating.SetActive(true);
  }

  public void Initialize(WebAPI.Response.GuildTop guildTop, DateTime serverTime)
  {
    this.open.SetActive(false);
    this.close.SetActive(false);
    RaidPeriod raidPeriod = guildTop.raid_period;
    if (raidPeriod != null)
    {
      DateTime? endAt = raidPeriod.end_at;
      DateTime dateTime = serverTime;
      this.openTime.SetTextLocalize((endAt.HasValue ? new TimeSpan?(endAt.GetValueOrDefault() - dateTime) : new TimeSpan?()).Value.DisplayStringForGuildHunting());
      this.open.SetActive(true);
      ((IEnumerable<GameObject>) this.slcRaidRPIconBase).ForEach<GameObject>((System.Action<GameObject>) (x => x.gameObject.SetActive(false)));
      for (int index = 0; index < GuildUtil.rp_max && index < this.slcRaidRPIconBase.Length; ++index)
        this.slcRaidRPIconBase[index].gameObject.SetActive(true);
      this.dirRPgrid.Reposition();
      for (int index = 0; index < this.slcRaidRPIconOff.Length && index < this.slcRaidRPIconOn.Length; ++index)
      {
        bool flag = index < GuildUtil.rp;
        this.slcRaidRPIconOff[index].SetActive(!flag);
        this.slcRaidRPIconOn[index].SetActive(flag);
      }
      if (!((UnityEngine.Object) this.frameLightPrefab == (UnityEngine.Object) null))
        return;
      this.StartCoroutine(this.loadFrameLight());
    }
    else if (guildTop.raid_aggregating)
      this.EventAggregating();
    else
      this.EventClose();
  }

  public IEnumerator loadFrameLight()
  {
    Future<GameObject> loader = new ResourceObject("Prefabs/guild/dir_guild_btn_frame_Light").Load<GameObject>();
    IEnumerator e = loader.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.frameLightPrefab = loader.Result;
    yield return (object) this.frameLightPrefab.Clone(this.dirFrameLight.transform).GetComponentInChildren<GuildButtonFrameLight>().Init(new Color(1f, 0.282353f, 0.2588235f));
  }
}
