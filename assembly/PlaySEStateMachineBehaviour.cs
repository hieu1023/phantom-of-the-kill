﻿// Decompiled with JetBrains decompiler
// Type: PlaySEStateMachineBehaviour
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class PlaySEStateMachineBehaviour : StateMachineBehaviour
{
  public string seName;

  public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
  {
    Singleton<NGSoundManager>.GetInstance().playSE(this.seName, false, 0.0f, -1);
  }
}
