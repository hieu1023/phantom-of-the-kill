﻿// Decompiled with JetBrains decompiler
// Type: Story00983Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using System.Collections;
using UnityEngine;

public class Story00983Scene : NGSceneBase
{
  [SerializeField]
  private Story00983Menu menu;
  [SerializeField]
  private UILabel TxtTitle;
  [SerializeField]
  private NGxScroll ScrollContainer;

  public IEnumerator onStartSceneAsync(int ID)
  {
    this.ScrollContainer.Clear();
    bool flag = false;
    QuestExtraS extra = (QuestExtraS) null;
    if (MasterData.QuestExtraS.ContainsKey(ID))
    {
      extra = MasterData.QuestExtraS[ID];
      this.TxtTitle.SetTextLocalize(extra.quest_l.name);
      flag = true;
    }
    if (flag)
    {
      IEnumerator e = this.menu.InitScene(extra);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    this.ScrollContainer.ResolvePosition();
  }
}
