﻿// Decompiled with JetBrains decompiler
// Type: GuildMemberObject
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;
using UnityEngine;

public class GuildMemberObject
{
  private GameObject guildMemberListPopup;
  private GameObject guildMemberInfoPopup;
  private GameObject guildMemberListPrefab;
  private GameObject guildPositionManagementPopup;
  private GameObject guildPositionManagementPopupYesNo;
  private GameObject guildPositionManagementPopupOk;

  public GameObject GuildMemberListPopup
  {
    get
    {
      return this.guildMemberListPopup;
    }
  }

  public GameObject GuildMemberInfoPopup
  {
    get
    {
      return this.guildMemberInfoPopup;
    }
  }

  public GameObject GuildMemberListPrefab
  {
    get
    {
      return this.guildMemberListPrefab;
    }
  }

  public GameObject GuildPositionManagementPopup
  {
    get
    {
      return this.guildPositionManagementPopup;
    }
  }

  public GameObject GuildPositionManagementPopupYesNo
  {
    get
    {
      return this.guildPositionManagementPopupYesNo;
    }
  }

  public GameObject GuildPositionManagementPopupOk
  {
    get
    {
      return this.guildPositionManagementPopupOk;
    }
  }

  public IEnumerator ResourceLoad()
  {
    Future<GameObject> fgObj;
    IEnumerator e;
    if ((Object) this.guildMemberListPopup == (Object) null)
    {
      fgObj = Res.Prefabs.popup.popup_028_guild_member_list__anim_popup01.Load<GameObject>();
      e = fgObj.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.guildMemberListPopup = fgObj.Result;
      fgObj = (Future<GameObject>) null;
    }
    if ((Object) this.guildMemberInfoPopup == (Object) null)
    {
      fgObj = Res.Prefabs.popup.popup_028_guild_member_info__anim_popup01.Load<GameObject>();
      e = fgObj.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.guildMemberInfoPopup = fgObj.Result;
      fgObj = (Future<GameObject>) null;
    }
    if ((Object) this.guildMemberListPrefab == (Object) null)
    {
      fgObj = Res.Prefabs.guild.guild_member_list.Load<GameObject>();
      e = fgObj.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.guildMemberListPrefab = fgObj.Result;
      fgObj = (Future<GameObject>) null;
    }
    if ((Object) this.guildPositionManagementPopup == (Object) null)
    {
      fgObj = Res.Prefabs.popup.popup_028_guild_member_posiotion_management__anim_popup01.Load<GameObject>();
      e = fgObj.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.guildPositionManagementPopup = fgObj.Result;
      fgObj = (Future<GameObject>) null;
    }
    if ((Object) this.guildPositionManagementPopupYesNo == (Object) null)
    {
      fgObj = Res.Prefabs.popup.popup_028_guild_guildmaster_resign_confirmation__anim_popup01.Load<GameObject>();
      e = fgObj.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.guildPositionManagementPopupYesNo = fgObj.Result;
      fgObj = (Future<GameObject>) null;
    }
    if ((Object) this.guildPositionManagementPopupOk == (Object) null)
    {
      fgObj = Res.Prefabs.popup.popup_028_guild_guildmaster_resign_result__anim_popup01.Load<GameObject>();
      e = fgObj.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.guildPositionManagementPopupOk = fgObj.Result;
      fgObj = (Future<GameObject>) null;
    }
  }
}
