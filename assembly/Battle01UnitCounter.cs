﻿// Decompiled with JetBrains decompiler
// Type: Battle01UnitCounter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Battle01UnitCounter : NGBattleMenuBase
{
  [SerializeField]
  private SelectParts selectParts;
  [SerializeField]
  private UILabel count_txt;

  public void setCount(int c)
  {
    if (c <= 0)
    {
      this.selectParts.setValue(1);
    }
    else
    {
      this.selectParts.setValue(0);
      this.setText(this.count_txt, c);
    }
  }
}
