﻿// Decompiled with JetBrains decompiler
// Type: gacha006_effectMenu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System.Collections;
using UnityEngine;

public class gacha006_effectMenu : BackButtonMenuBase
{
  public GameObject backButton;
  public GameObject skipButton;
  [SerializeField]
  private GameObject effectFade;
  private GachaResultData.Result[] resultList;

  public EffectControllerGacha Effect { get; set; }

  public void IbtnSkip()
  {
    this.skipButton.SetActive(false);
    this.Effect.Skip();
  }

  public void IbtnBack()
  {
    this.Effect.Next();
  }

  public override void onBackButton()
  {
    if (this.Effect.State != EffectControllerGacha.STATE.WAIT)
      ToastMessage.showBackKeyToast();
    this.IbtnBack();
  }

  public IEnumerator SetEffectData(GachaResultData.Result[] resultList)
  {
    this.resultList = resultList;
    this.Effect.gameObject.SetActive(true);
    this.skipButton.SetActive(true);
    IEnumerator e = this.Effect.SetNeedData(resultList, this.backButton);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public void ShowResult()
  {
    Singleton<NGSoundManager>.GetInstance().StopBgm(-1, 1f);
    Singleton<NGSoundManager>.GetInstance().StopSe(-1, 1f);
    Singleton<NGSoundManager>.GetInstance().StopVoice(-1, 2f);
    if (this.resultList.Length != 1 || GachaResultData.GetInstance().GetData().is_retry)
    {
      Gacha00613Scene.ChangeScene(false, GachaResultData.GetInstance().GetData().is_retry);
    }
    else
    {
      GachaResultData.Result result = this.resultList[0];
      CommonRewardType commonRewardType = new CommonRewardType(result.reward_type_id, result.reward_result_id, result.reward_result_quantity, result.is_new, result.is_reserves);
      if (commonRewardType.isUnit)
        Singleton<NGSceneManager>.GetInstance().changeScene("gacha006_8", false, (object) commonRewardType.unit, (object) result.is_new);
      else if (commonRewardType.isMaterialUnit)
        Unit00493Scene.changeScene(false, commonRewardType.materialUnit.unit, result.is_new, true);
      else if (commonRewardType.isGear)
      {
        PlayerItem gear = commonRewardType.gear;
        if (commonRewardType.gear.gear.kind.isEquip)
          Gacha00611Scene.changeScene(false, result.is_new, 0, new ItemInfo(gear));
        else
          Bugu00561Scene.changeScene(false, new ItemInfo(gear), result.is_new, true);
      }
      else
        Bugu00561Scene.changeScene(false, new ItemInfo(commonRewardType.materialGear, 0), result.is_new, true);
    }
  }

  public void PlayEffectWhiteFadeIn()
  {
    if ((Object) this.effectFade == (Object) null)
      return;
    this.SetEffectWhiteFadeActive(true);
    this.effectFade.GetComponent<TweenAlpha>().PlayForward();
  }

  public void SetEffectWhiteFadeActive(bool active)
  {
    if ((Object) this.effectFade == (Object) null)
      return;
    this.effectFade.SetActive(active);
    this.effectFade.GetComponent<UI2DSprite>().alpha = 1f;
  }
}
