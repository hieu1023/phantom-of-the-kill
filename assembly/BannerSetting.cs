﻿// Decompiled with JetBrains decompiler
// Type: BannerSetting
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class BannerSetting : BannerBase
{
  [SerializeField]
  private float delay = 3f;
  public int ID;
  [SerializeField]
  public int priority;
  [SerializeField]
  private GameObject togo;
  [SerializeField]
  private FloatButton Button;
  [SerializeField]
  private GameObject BannerEffectSprite;
  [SerializeField]
  private UIUnityMaskRenderer effectRenderer;
  [SerializeField]
  private Animator animator;
  private DateTime serverTime;
  private EventDelegate del;
  private bool DestroyFlag;
  private bool notJump;

  public UIUnityMaskRenderer EffectRenderer
  {
    get
    {
      return this.effectRenderer;
    }
  }

  public bool DestroyButton
  {
    get
    {
      return this.DestroyFlag;
    }
  }

  private bool isUseWebImage(SM.Banner banner)
  {
    return !(banner.transition.scene_name == "quest002_20") && !(banner.transition.scene_name == "quest002_19") && !(banner.transition.scene_name == "quest002_26");
  }

  public IEnumerator Init(
    SM.Banner banner,
    BannersProc parent,
    DateTime serverTime,
    System.Action callback = null,
    bool isStack = true)
  {
    BannerSetting bannerSetting = this;
    bannerSetting.serverTime = serverTime;
    bannerSetting.DestroyFlag = !BannerSetting.judgeTime(banner, serverTime);
    if (!bannerSetting.DestroyFlag)
    {
      IEnumerator e;
      if (!bannerSetting.isUseWebImage(banner))
      {
        e = bannerSetting.LoadAndSetImage(banner);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
      }
      else
      {
        e = bannerSetting.LoadAndSetImage(banner.url);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
      }
      bannerSetting.effectRenderer.SetTexture("_MaskTex", bannerSetting.IdleSprite.mainTexture);
      bannerSetting.setEmphasisEffectVisibility(banner.emphasis);
      e = bannerSetting.SetTransition(banner, parent, callback, isStack);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      if (!bannerSetting.DestroyFlag)
      {
        bannerSetting.delay = (float) banner.duration_seconds;
        bannerSetting.ID = banner.id;
        bannerSetting.priority = banner.priority;
        if (!bannerSetting.notJump)
          EventDelegate.Set(bannerSetting.BtnFormation.onClick, bannerSetting.del);
        // ISSUE: reference to a compiler-generated method
        EventDelegate.Set(bannerSetting.BtnFormation.onOver, new EventDelegate.Callback(bannerSetting.\u003CInit\u003Eb__18_0));
        // ISSUE: reference to a compiler-generated method
        EventDelegate.Set(bannerSetting.BtnFormation.onOut, new EventDelegate.Callback(bannerSetting.\u003CInit\u003Eb__18_1));
        bannerSetting.onOut();
      }
    }
  }

  public static bool judgeTime(SM.Banner banner, DateTime now)
  {
    return !banner.end_at.HasValue || now < banner.end_at.Value;
  }

  public void onOver()
  {
  }

  public void onOut()
  {
  }

  private IEnumerator onBannerEventConnection(
    BannerSetting.BannerType type,
    SM.Banner banner,
    System.Action callback,
    bool isStack)
  {
    IEnumerator e;
    if (!WebAPI.IsResponsedAtRecent("QuestProgressExtra", 60.0))
    {
      Singleton<CommonRoot>.GetInstance().loadingMode = 1;
      Future<WebAPI.Response.QuestProgressExtra> Extra = WebAPI.QuestProgressExtra((System.Action<WebAPI.Response.UserError>) (error =>
      {
        Singleton<CommonRoot>.GetInstance().loadingMode = 0;
        WebAPI.DefaultUserErrorCallback(error);
      }));
      e = Extra.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      Singleton<CommonRoot>.GetInstance().loadingMode = 0;
      if (Extra.Result != null)
        WebAPI.SetLatestResponsedAt("QuestProgressExtra");
      Extra = (Future<WebAPI.Response.QuestProgressExtra>) null;
    }
    PlayerExtraQuestS[] playerExtraQuestSArray = SMManager.Get<PlayerExtraQuestS[]>();
    if (banner.transition.arg1 != 0 && !((IEnumerable<PlayerExtraQuestS>) playerExtraQuestSArray).Any<PlayerExtraQuestS>((Func<PlayerExtraQuestS, bool>) (x => x.quest_extra_s != null && x.quest_extra_s.ID == banner.transition.arg1)))
    {
      Future<GameObject> time_popup = Res.Prefabs.popup.popup_002_23__anim_popup01.Load<GameObject>();
      e = time_popup.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      Singleton<PopupManager>.GetInstance().openAlert(time_popup.Result, false, false, (EventDelegate) null, false, true, false, true);
    }
    else
    {
      switch (type)
      {
        case BannerSetting.BannerType.BANNER_TYPE_L:
          if (callback != null)
            callback();
          Quest00219Scene.ChangeScene(banner.transition.arg1, isStack);
          break;
        case BannerSetting.BannerType.BANNER_TYPE_M:
          if (callback != null)
            callback();
          Quest00220Scene.ChangeScene00220(banner.transition.arg1, false, false);
          break;
        case BannerSetting.BannerType.BANNER_TYPE_RANKING_EVENT:
          if (callback != null)
            callback();
          Quest00226Scene.ChangeScene(banner.transition.arg1, isStack);
          break;
      }
    }
  }

  private void onBannerEvent(
    BannerSetting.BannerType type,
    SM.Banner banner,
    System.Action callback,
    bool isStack)
  {
    this.StopAnime();
    Singleton<CommonRoot>.GetInstance().StartCoroutine(this.onBannerEventConnection(type, banner, callback, isStack));
  }

  public static bool IsExistSpritePath(SM.Banner banner)
  {
    BannerBase.GetSpriteIdlePath(banner.id, BannerBase.Type.mypage, QuestExtra.SeekType.L, true, false);
    string spriteIdlePath;
    if (banner.transition.scene_name == "quest002_20")
    {
      spriteIdlePath = BannerBase.GetSpriteIdlePath(MasterData.QuestExtraS[banner.transition.arg1].quest_m_QuestExtraM, BannerBase.Type.quest, QuestExtra.SeekType.M, true, false);
    }
    else
    {
      if (!(banner.transition.scene_name == "quest002_19") && !(banner.transition.scene_name == "quest002_26"))
        return !string.IsNullOrEmpty(banner.url);
      spriteIdlePath = BannerBase.GetSpriteIdlePath(MasterData.QuestExtraS[banner.transition.arg1].quest_l_QuestExtraL, BannerBase.Type.quest, QuestExtra.SeekType.L, true, false);
    }
    return Singleton<ResourceManager>.GetInstance().Contains(spriteIdlePath);
  }

  private IEnumerator LoadAndSetImage(SM.Banner banner)
  {
    BannerSetting bannerSetting = this;
    string spriteIdlePath = BannerBase.GetSpriteIdlePath(banner.id, BannerBase.Type.mypage, QuestExtra.SeekType.L, true, false);
    if (banner.transition.scene_name == "quest002_20")
      spriteIdlePath = BannerBase.GetSpriteIdlePath(MasterData.QuestExtraS[banner.transition.arg1].quest_m_QuestExtraM, BannerBase.Type.quest, QuestExtra.SeekType.M, true, false);
    else if (banner.transition.scene_name == "quest002_19" || banner.transition.scene_name == "quest002_26")
      spriteIdlePath = BannerBase.GetSpriteIdlePath(MasterData.QuestExtraS[banner.transition.arg1].quest_l_QuestExtraL, BannerBase.Type.quest, QuestExtra.SeekType.L, true, false);
    Future<Texture2D> resource = Singleton<ResourceManager>.GetInstance().LoadOrNull<Texture2D>(spriteIdlePath);
    IEnumerator e = resource.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Texture2D result = resource.Result;
    result.wrapMode = TextureWrapMode.Clamp;
    float width = (float) result.width;
    float height = (float) result.height;
    UnityEngine.Sprite sprite = UnityEngine.Sprite.Create(result, new Rect(0.0f, 0.0f, width, height), new Vector2(0.0f, 0.0f), 1f, 100U, SpriteMeshType.FullRect);
    bannerSetting.IdleSprite.sprite2D = sprite;
    bannerSetting.IdleSprite.SetDimensions((int) width, (int) height);
  }

  public IEnumerator SetTransition(
    SM.Banner banner,
    BannersProc parent,
    System.Action callback,
    bool isStack)
  {
    BannerSetting bannerSetting = this;
    CampaignQuest.RankingEventTerm rankingEventTerm = CampaignQuest.RankingEventTerm.normal;
    bannerSetting.Button.enabled = true;
    bannerSetting.notJump = false;
    if (banner.end_at.HasValue)
      bannerSetting.EndTime = banner.end_at.Value;
    if (banner.transition.scene_name == "mypage001_8_2")
      bannerSetting.del = new EventDelegate((EventDelegate.Callback) (() =>
      {
        parent.StopScroll();
        this.StopAnime();
        this.setEmphasisEffectVisibility(false);
        if (callback != null)
          callback();
        Singleton<NGSceneManager>.GetInstance().changeScene("mypage001_8_2", false, (object) banner.transition.arg1);
      }));
    else if (banner.transition.scene_name == "quest002_20")
      bannerSetting.del = new EventDelegate((EventDelegate.Callback) (() =>
      {
        parent.StopScroll();
        this.StopAnime();
        this.setEmphasisEffectVisibility(false);
        this.onBannerEvent(BannerSetting.BannerType.BANNER_TYPE_M, banner, callback, isStack);
      }));
    else if (banner.transition.scene_name == "quest002_19" || banner.transition.scene_name == "quest002_26")
    {
      int idl = MasterData.QuestExtraS[banner.transition.arg1].quest_l_QuestExtraL;
      if (banner.transition.scene_name == "quest002_26")
      {
        QuestScoreCampaignProgress[] campaignProgressArray = SMManager.Get<QuestScoreCampaignProgress[]>();
        if (campaignProgressArray == null)
        {
          Future<WebAPI.Response.QuestProgressExtra> extra = WebAPI.QuestProgressExtra((System.Action<WebAPI.Response.UserError>) (e => WebAPI.DefaultUserErrorCallback(e)));
          IEnumerator e1 = extra.Wait();
          while (e1.MoveNext())
            yield return e1.Current;
          e1 = (IEnumerator) null;
          if (extra.Result == null)
          {
            yield break;
          }
          else
          {
            campaignProgressArray = SMManager.Get<QuestScoreCampaignProgress[]>();
            extra = (Future<WebAPI.Response.QuestProgressExtra>) null;
          }
        }
        if (campaignProgressArray != null)
        {
          QuestScoreCampaignProgress campaign = ((IEnumerable<QuestScoreCampaignProgress>) campaignProgressArray).FirstOrDefault<QuestScoreCampaignProgress>((Func<QuestScoreCampaignProgress, bool>) (x => x.quest_extra_l == idl));
          if (campaign != null)
          {
            rankingEventTerm = CampaignQuest.GetEvetnTerm(campaign, bannerSetting.serverTime);
            if (rankingEventTerm == CampaignQuest.RankingEventTerm.normal)
              bannerSetting.EndTime = campaign.end_at;
            else if (rankingEventTerm == CampaignQuest.RankingEventTerm.aggregate)
            {
              bannerSetting.EndTime = campaign.final_at;
              bannerSetting.Button.enabled = false;
            }
            else if (rankingEventTerm == CampaignQuest.RankingEventTerm.receive)
              bannerSetting.EndTime = campaign.latest_end_at;
          }
        }
      }
      bannerSetting.del = new EventDelegate((EventDelegate.Callback) (() =>
      {
        parent.StopScroll();
        this.StopAnime();
        this.setEmphasisEffectVisibility(false);
        if (banner.transition.scene_name == "quest002_19")
        {
          this.onBannerEvent(BannerSetting.BannerType.BANNER_TYPE_L, banner, callback, isStack);
        }
        else
        {
          if (rankingEventTerm == CampaignQuest.RankingEventTerm.aggregate)
            return;
          this.onBannerEvent(BannerSetting.BannerType.BANNER_TYPE_RANKING_EVENT, banner, callback, isStack);
        }
      }));
    }
    else if (banner.transition.scene_name == "gacha006_3")
      bannerSetting.del = new EventDelegate((EventDelegate.Callback) (() =>
      {
        parent.StopScroll();
        this.StopAnime();
        this.setEmphasisEffectVisibility(false);
        if (callback != null)
          callback();
        Singleton<NGSceneManager>.GetInstance().changeScene(banner.transition.scene_name, false, (object) banner.transition.arg1);
      }));
    else if (banner.transition.scene_name == "shop007_21")
      bannerSetting.del = new EventDelegate((EventDelegate.Callback) (() =>
      {
        parent.StopScroll();
        this.StopAnime();
        this.setEmphasisEffectVisibility(false);
        if (callback != null)
          callback();
        Shop00721Scene.changeScene(false, true);
      }));
    else if (banner.transition.scene_name == "quest002_30")
      bannerSetting.del = new EventDelegate((EventDelegate.Callback) (() =>
      {
        parent.StopScroll();
        this.StopAnime();
        this.setEmphasisEffectVisibility(false);
        if (callback != null)
          callback();
        Quest00230Scene.ChangeScene(false, banner.transition.arg1);
      }));
    else if (banner.transition.scene_name == "quest002_5")
      bannerSetting.del = new EventDelegate((EventDelegate.Callback) (() =>
      {
        parent.StopScroll();
        this.StopAnime();
        this.setEmphasisEffectVisibility(false);
        if (callback != null)
          callback();
        Quest0025Scene.changeScene0025(false, new Quest0025Scene.Quest0025Param(banner.transition.arg2, banner.transition.arg1, false, false, true));
      }));
    else if (banner.transition.scene_name == "quest002_4")
      bannerSetting.del = new EventDelegate((EventDelegate.Callback) (() =>
      {
        parent.StopScroll();
        this.StopAnime();
        this.setEmphasisEffectVisibility(false);
        if (callback != null)
          callback();
        PlayerStoryQuestS[] playerStoryQuestSArray = SMManager.Get<PlayerStoryQuestS[]>();
        QuestStoryS questStoryS = (QuestStoryS) null;
        Quest00240723Menu.StoryMode storyMode = !MasterData.QuestStoryS.TryGetValue(banner.transition.arg1, out questStoryS) ? Quest00240723Menu.StoryMode.LostRagnarok : (Quest00240723Menu.StoryMode) questStoryS.quest_xl_QuestStoryXL;
        if (questStoryS != null && ((IEnumerable<PlayerStoryQuestS>) playerStoryQuestSArray).Any<PlayerStoryQuestS>((Func<PlayerStoryQuestS, bool>) (x => x._quest_story_s == banner.transition.arg1)))
          Quest00240723Scene.ChangeScene0024(false, MasterData.QuestStoryS[banner.transition.arg1].quest_l_QuestStoryL, true);
        else
          Quest00240723Scene.ChangeScene0024(false, ((IEnumerable<PlayerStoryQuestS>) playerStoryQuestSArray).Where<PlayerStoryQuestS>((Func<PlayerStoryQuestS, bool>) (x => (Quest00240723Menu.StoryMode) x.quest_story_s.quest_xl_QuestStoryXL == storyMode)).Select<PlayerStoryQuestS, int>((Func<PlayerStoryQuestS, int>) (x => x.quest_story_s.quest_l_QuestStoryL)).Max(), true);
      }));
    else if (banner.transition.scene_name == "raid_top")
      bannerSetting.del = new EventDelegate((EventDelegate.Callback) (() =>
      {
        parent.StopScroll();
        this.StopAnime();
        this.setEmphasisEffectVisibility(false);
        if (callback != null)
          callback();
        Singleton<NGGameDataManager>.GetInstance().IsSea = false;
        PlayerAffiliation current = PlayerAffiliation.Current;
        if ((current != null ? (current.isGuildMember() ? 1 : 0) : 0) == 0)
          Guild0281Scene.ChangeSceneGuildTop(false, (Guild0281Menu) null, true);
        else
          RaidTopScene.ChangeSceneBattleFinish(true);
      }));
    else if (banner.transition.scene_name == "")
      bannerSetting.notJump = true;
    else
      bannerSetting.del = new EventDelegate((EventDelegate.Callback) (() =>
      {
        parent.StopScroll();
        this.StopAnime();
        this.setEmphasisEffectVisibility(false);
        if (callback != null)
          callback();
        Singleton<NGSceneManager>.GetInstance().changeScene(banner.transition.scene_name, false, (object[]) Array.Empty<object>());
      }));
    bannerSetting.togo.SetActive(false);
    TimeSpan timeSpan = bannerSetting.EndTime - bannerSetting.serverTime;
    if (banner.show_exp && timeSpan.TotalMilliseconds > 0.0)
    {
      bannerSetting.togo.SetActive(true);
      bannerSetting.SetTime(bannerSetting.serverTime, rankingEventTerm);
    }
  }

  public void setEmphasisEffectVisibility(bool isVisible)
  {
    if (!((UnityEngine.Object) this.BannerEffectSprite != (UnityEngine.Object) null))
      return;
    this.BannerEffectSprite.gameObject.SetActive(isVisible);
  }

  public void StartTween()
  {
    TweenAlpha component = this.GetComponent<TweenAlpha>();
    component.delay = 0.0f;
    EventDelegate.Set(component.onFinished, new EventDelegate((EventDelegate.Callback) (() => this.EndTween()))
    {
      oneShot = true
    });
    component.PlayForward();
  }

  public void EndTween()
  {
    TweenAlpha component = this.GetComponent<TweenAlpha>();
    component.delay = this.delay;
    EventDelegate.Set(component.onFinished, new EventDelegate((EventDelegate.Callback) (() => this.Next()))
    {
      oneShot = true
    });
    component.PlayReverse();
  }

  public void Next()
  {
    this.transform.parent.GetComponent<BannersProc>().LoopBannerNext();
  }

  public void StartAnime()
  {
    if (!((UnityEngine.Object) this.animator != (UnityEngine.Object) null) || !this.animator.gameObject.activeInHierarchy)
      return;
    this.animator.SetBool("isPlay", true);
  }

  public void StopAnime()
  {
    if (!((UnityEngine.Object) this.animator != (UnityEngine.Object) null) || !this.animator.gameObject.activeInHierarchy)
      return;
    this.animator.SetBool("isPlay", false);
  }

  private enum BannerType
  {
    BANNER_TYPE_L,
    BANNER_TYPE_M,
    BANNER_TYPE_OTHER,
    BANNER_TYPE_RANKING_EVENT,
  }
}
