﻿// Decompiled with JetBrains decompiler
// Type: Help0156Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using System;
using System.Collections;
using System.Collections.Generic;

public class Help0156Scene : NGSceneBase
{
  public Help0156Menu menu;

  public static void ChangeScene(bool stack, List<BeginnerNaviTitle> bnTitles)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("help_015_6", (stack ? 1 : 0) != 0, (object) bnTitles);
  }

  public static void ChangeScene(bool stack)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("help_015_6", stack, (object[]) Array.Empty<object>());
  }

  public IEnumerator onStartSceneAsync(List<BeginnerNaviTitle> bnTitles)
  {
    IEnumerator e = this.menu.InitBeginnerNaviTitle(bnTitles);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if (Singleton<CommonRoot>.GetInstance().isLoading)
    {
      yield return (object) null;
      Singleton<CommonRoot>.GetInstance().isLoading = false;
    }
  }
}
