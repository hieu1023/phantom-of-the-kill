﻿// Decompiled with JetBrains decompiler
// Type: Unit004JobChangeMenu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using JobChangeData;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Unit004JobChangeMenu : BackButtonMenuBase, IDetailMenuContainer
{
  [Header("ジョブチェンジ選択カーソル")]
  [SerializeField]
  private Unit004JobChangeMenu.Cursor[] cursors_ = new Unit004JobChangeMenu.Cursor[DefValues.NUM_CHANGETYPE];
  [SerializeField]
  [Tooltip("2ライン表示～3ライン表示をセット")]
  private GameObject[] cursorLines_ = new GameObject[DefValues.NUM_CHANGETYPE - 1];
  private bool isInitialize_ = true;
  private Dictionary<string, UnityEngine.Sprite> dicJobSelector_ = new Dictionary<string, UnityEngine.Sprite>(DefValues.NUM_CHANGETYPE);
  private Dictionary<int, bool> dictJobChangePatternsConditionsLocked = new Dictionary<int, bool>();
  private bool firstChangeJob_ = true;
  private Unit004JobChangeMaterialContainer[] materialSwapBuff_ = new Unit004JobChangeMaterialContainer[2];
  [Header("3Dモデル")]
  [SerializeField]
  private Transform top3DModel_;
  [SerializeField]
  private UI3DModel ui3DModel_;
  private GameObject backup3DModel_;
  [SerializeField]
  private GameObject objLoading3DModel_;
  [Header("Bottom")]
  [SerializeField]
  private Unit004JobChangeMaterialContainer materialContainer_;
  [SerializeField]
  private UILabel txtOwnedZeny_;
  [SerializeField]
  private UILabel txtCostZeny_;
  [SerializeField]
  private ButtonWrapper btnExecute_;
  [SerializeField]
  private GameObject maskReleased_;
  [SerializeField]
  private UILabel MaskReleasedTextAttention;
  [SerializeField]
  private UIButton MaskReleasedTextAttentionButton;
  [SerializeField]
  private Transform lnkDetail_;
  private SceneParam param_;
  private long? wRevision_;
  private PlayerUnit currentUnit_;
  private PlayerUnitJob_abilities currentJobAbility_;
  private int firstCousorIndex_;
  private int focusCousorIndex_;
  private PlayerUnit[] changeUnits_;
  private bool isExistChangePattern_;
  private Dictionary<int, PlayerMaterialUnit[]> dicMaterials_;
  private Dictionary<int, int> dicCost_;
  private Dictionary<int, bool> dicCompletedMaterials_;
  private Dictionary<int, bool> dicUnlocked_;
  private DetailMenuPrefab[] details_;
  private WebAPI.Response.UnitPreviewJob previewJob_;
  private int materialSwapIndex_;
  private Vector3? localTop3DModel_;
  private const int CENTER_FIRST_DETAIL = 2;
  private bool isLoading3DModel_;

  public bool isNeedReset
  {
    get
    {
      bool flag = !this.wRevision_.HasValue;
      long num = SMManager.Revision<PlayerUnit[]>();
      if (!flag)
        flag = this.wRevision_.Value != num;
      return flag;
    }
  }

  public GameObject detailPrefab { get; private set; }

  public GameObject gearKindIconPrefab { get; private set; }

  public GameObject gearIconPrefab { get; private set; }

  public GameObject skillDetailDialogPrefab { get; private set; }

  public GameObject specialPointDetailDialogPrefab { get; private set; }

  public GameObject profIconPrefab { get; private set; }

  public GameObject skillTypeIconPrefab { get; private set; }

  public GameObject skillfullnessIconPrefab { get; private set; }

  public GameObject commonElementIconPrefab { get; private set; }

  public GameObject spAtkTypeIconPrefab { get; private set; }

  public GameObject statusDetailPrefab { get; private set; }

  public GameObject skillListPrefab { get; private set; }

  public GameObject StatusDetailPrefab { get; private set; }

  public GameObject TrainingPrefa { get; private set; }

  public GameObject GroupDetailDialogPrefab { get; private set; }

  public GameObject detailJobAbilityPrefab { get; private set; }

  public GameObject terraiAbilityDialogPrefab { get; private set; }

  public GameObject unityDetailPrefab { get; private set; }

  public GameObject stageItemPrefab { get; private set; }

  public GameObject skillLockIconPrefab { get; private set; }

  private GameObject levelupJobAbilityPrefab { get; set; }

  private GameObject jobChangeConditionsPrefab { get; set; }

  private void Awake()
  {
    if ((UnityEngine.Object) this.top3DModel_ != (UnityEngine.Object) null)
      this.localTop3DModel_ = new Vector3?(new Vector3(this.top3DModel_.localPosition.x, this.top3DModel_.localPosition.y, this.top3DModel_.localPosition.z));
    this.backup3DModel_ = this.ui3DModel_.gameObject;
    this.backup3DModel_.SetActive(false);
    this.ui3DModel_ = (UI3DModel) null;
  }

  public IEnumerator initializeAsync(SceneParam param)
  {
    Unit004JobChangeMenu unit004JobChangeMenu = this;
    unit004JobChangeMenu.isInitialize_ = true;
    unit004JobChangeMenu.firstChangeJob_ = true;
    unit004JobChangeMenu.previewJob_ = (WebAPI.Response.UnitPreviewJob) null;
    unit004JobChangeMenu.param_ = param;
    unit004JobChangeMenu.wRevision_ = new long?(SMManager.Revision<PlayerUnit[]>());
    PlayerUnit playerUnit = Array.Find<PlayerUnit>(SMManager.Get<PlayerUnit[]>(), (Predicate<PlayerUnit>) (u => u.id == param.playerUnitId_));
    if (playerUnit == (PlayerUnit) null)
    {
      unit004JobChangeMenu.currentUnit_ = (PlayerUnit) null;
      unit004JobChangeMenu.isInitialize_ = false;
    }
    else
    {
      if (unit004JobChangeMenu.currentUnit_ != (PlayerUnit) null && unit004JobChangeMenu.currentUnit_.unit.resource_reference_unit_id_UnitUnit != playerUnit.unit.resource_reference_unit_id_UnitUnit)
        unit004JobChangeMenu.dicJobSelector_.Clear();
      unit004JobChangeMenu.currentUnit_ = playerUnit;
      unit004JobChangeMenu.currentJobAbility_ = (PlayerUnitJob_abilities) null;
      unit004JobChangeMenu.changeUnits_ = JobChangeUtil.createJobChangePattern(unit004JobChangeMenu.currentUnit_, out unit004JobChangeMenu.isExistChangePattern_);
      unit004JobChangeMenu.dicMaterials_ = new Dictionary<int, PlayerMaterialUnit[]>(unit004JobChangeMenu.changeUnits_.Length);
      unit004JobChangeMenu.dicCost_ = new Dictionary<int, int>(unit004JobChangeMenu.changeUnits_.Length);
      unit004JobChangeMenu.dicCompletedMaterials_ = new Dictionary<int, bool>(unit004JobChangeMenu.changeUnits_.Length);
      unit004JobChangeMenu.dicUnlocked_ = new Dictionary<int, bool>(unit004JobChangeMenu.changeUnits_.Length);
      PlayerMaterialUnit[] playerMaterials = SMManager.Get<PlayerMaterialUnit[]>();
      if (unit004JobChangeMenu.isExistChangePattern_)
      {
        Future<WebAPI.Response.UnitPreviewJob> dpd = WebAPI.UnitPreviewJob(unit004JobChangeMenu.currentUnit_.id, (System.Action<WebAPI.Response.UserError>) (error =>
        {
          WebAPI.DefaultUserErrorCallback(error);
          Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
        }));
        yield return (object) dpd.Wait();
        unit004JobChangeMenu.previewJob_ = dpd.Result;
        dpd = (Future<WebAPI.Response.UnitPreviewJob>) null;
        JobChangePatterns jobChangePatterns = JobChangeUtil.getJobChangePatterns(unit004JobChangeMenu.currentUnit_);
        unit004JobChangeMenu.resetMaterialsParam(jobChangePatterns.job1_UnitJob, jobChangePatterns.materials1, playerMaterials);
        unit004JobChangeMenu.resetMaterialsParam(jobChangePatterns.job2_UnitJob, jobChangePatterns.materials2, playerMaterials);
        if (jobChangePatterns.job3_UnitJob.HasValue)
        {
          unit004JobChangeMenu.resetMaterialsParam(jobChangePatterns.job3_UnitJob.Value, jobChangePatterns.materials3, playerMaterials);
          if (jobChangePatterns.job4_UnitJob.HasValue)
            unit004JobChangeMenu.resetMaterialsParam(jobChangePatterns.job4_UnitJob.Value, jobChangePatterns.materials4, playerMaterials);
        }
        int patternsConditions = JobChangeUtil.GetJobIdChangePatternsConditions(unit004JobChangeMenu.currentUnit_, unit004JobChangeMenu.previewJob_);
        foreach (KeyValuePair<int, bool> keyValuePair in unit004JobChangeMenu.dicUnlocked_)
          unit004JobChangeMenu.dictJobChangePatternsConditionsLocked[keyValuePair.Key] = !keyValuePair.Value && (patternsConditions != 0 && keyValuePair.Key == patternsConditions);
        yield return (object) OnDemandDownload.WaitLoadUnitResource(unit004JobChangeMenu.dicMaterials_.SelectMany<KeyValuePair<int, PlayerMaterialUnit[]>, UnitUnit>((Func<KeyValuePair<int, PlayerMaterialUnit[]>, IEnumerable<UnitUnit>>) (d => ((IEnumerable<PlayerMaterialUnit>) d.Value).Select<PlayerMaterialUnit, UnitUnit>((Func<PlayerMaterialUnit, UnitUnit>) (x => x.unit)))).Distinct<UnitUnit>(), false, (IEnumerable<string>) null, false);
        if (unit004JobChangeMenu.currentUnit_.job_abilities != null && unit004JobChangeMenu.currentUnit_.job_abilities.Length != 0)
          yield return (object) OnDemandDownload.WaitLoadUnitResource(((IEnumerable<PlayerUnitJob_abilities>) unit004JobChangeMenu.currentUnit_.job_abilities).SelectMany<PlayerUnitJob_abilities, UnitUnit>((Func<PlayerUnitJob_abilities, IEnumerable<UnitUnit>>) (x =>
          {
            List<UnitUnit> unitUnitList = new List<UnitUnit>();
            if (x.master != null)
            {
              for (int level = x.level; level < x.master.levelup_patterns.Length; ++level)
              {
                List<KeyValuePair<UnitUnit, int>> levelupMaterials = x.getLevelupMaterials(this.currentUnit_, level);
                if (levelupMaterials != null)
                  unitUnitList.AddRange(levelupMaterials.Select<KeyValuePair<UnitUnit, int>, UnitUnit>((Func<KeyValuePair<UnitUnit, int>, UnitUnit>) (y => y.Key)));
              }
            }
            return (IEnumerable<UnitUnit>) unitUnitList;
          })).Distinct<UnitUnit>(), false, (IEnumerable<string>) null, false);
      }
      Player player = SMManager.Get<Player>();
      unit004JobChangeMenu.txtOwnedZeny_.SetTextLocalize(player.money);
      PartsContainerJoint component = unit004JobChangeMenu.GetComponent<PartsContainerJoint>();
      if ((UnityEngine.Object) component != (UnityEngine.Object) null && component.enabled)
        yield return (object) component.initializeAsync();
      yield return (object) unit004JobChangeMenu.initCursors();
      yield return (object) unit004JobChangeMenu.initMaterials();
      yield return (object) unit004JobChangeMenu.initDetails();
      yield return (object) unit004JobChangeMenu.init3DModels();
      unit004JobChangeMenu.isInitialize_ = false;
    }
  }

  private void resetMaterialsParam(
    int jobId,
    JobChangeMaterials materials,
    PlayerMaterialUnit[] playerMaterials)
  {
    PlayerMaterialUnit[] materials1 = materials != null ? JobChangeUtil.createPlayerMaterialUnits(materials) : new PlayerMaterialUnit[0];
    this.dicMaterials_[jobId] = materials1;
    this.dicCost_[jobId] = materials != null ? materials.cost : -1;
    this.dicCompletedMaterials_[jobId] = JobChangeUtil.checkCompletedMaterials(playerMaterials, materials1);
    this.dicUnlocked_[jobId] = this.currentUnit_.unit.job_UnitJob == jobId || this.currentUnit_.job_id == jobId || this.previewJob_ != null && ((IEnumerable<int>) this.previewJob_.changed_job_ids).Contains<int>(jobId);
  }

  public IEnumerator repairAsync(bool resetSelect)
  {
    this.reset3DModelAmbientLightColor();
    if (resetSelect)
      yield return (object) this.doChangeFocusJob(this.firstCousorIndex_, true);
    else
      yield return (object) this.doLoad3DModel(this.changeUnits_[this.focusCousorIndex_]);
  }

  private void reset3DModelAmbientLightColor()
  {
    RenderSettings.ambientLight = Consts.GetInstance().UI3DMODEL_AMBIENT_COLOR;
  }

  private IEnumerator initCursors()
  {
    Unit004JobChangeMenu unit004JobChangeMenu = this;
    // ISSUE: reference to a compiler-generated method
    unit004JobChangeMenu.firstCousorIndex_ = Array.FindIndex<PlayerUnit>(unit004JobChangeMenu.changeUnits_, new Predicate<PlayerUnit>(unit004JobChangeMenu.\u003CinitCursors\u003Eb__132_0));
    unit004JobChangeMenu.focusCousorIndex_ = unit004JobChangeMenu.firstCousorIndex_;
    Player player = SMManager.Get<Player>();
    for (int i = 0; i < unit004JobChangeMenu.changeUnits_.Length; ++i)
    {
      bool isCurrent = i == unit004JobChangeMenu.firstCousorIndex_;
      Unit004JobChangeMenu.Cursor cursor = unit004JobChangeMenu.cursors_[i];
      PlayerUnit p = unit004JobChangeMenu.changeUnits_[i];
      UnitUnit unit = p.unit;
      MasterDataTable.UnitJob jobData = p.getJobData();
      cursor.objTop_.SetActive(true);
      cursor.objFocus_.SetActive(isCurrent);
      cursor.cntlBtnCurrent_ = new Unit004JobChangeMenu.ButtonControl(cursor.btnCurrent_);
      cursor.btnCurrent_.gameObject.SetActive(isCurrent);
      cursor.cntlBtnNormal_ = new Unit004JobChangeMenu.ButtonControl(cursor.btnNormal_);
      cursor.btnNormal_.gameObject.SetActive(!isCurrent);
      cursor.txtJobName_.SetTextLocalize(jobData.name);
      UnitJobRankName unitJobRankName;
      cursor.txtRankName_.SetTextLocalize(MasterData.UnitJobRankName.TryGetValue(jobData.job_rank_UnitJobRank, out unitJobRankName) ? unitJobRankName.name : string.Empty);
      yield return (object) unit004JobChangeMenu.loadJobFrameSprite(unit, p.job_id, isCurrent);
      UnityEngine.Sprite jobFrameSprite = unit004JobChangeMenu.getJobFrameSprite(p.job_id, isCurrent);
      if (isCurrent)
      {
        unit004JobChangeMenu.setEventSelectedJob(cursor.cntlBtnCurrent_.button_, i);
        cursor.cntlBtnNormal_.button_.onClick.Clear();
        if ((UnityEngine.Object) jobFrameSprite != (UnityEngine.Object) null)
          cursor.cntlBtnCurrent_.sprite_.sprite2D = jobFrameSprite;
      }
      else
      {
        cursor.cntlBtnCurrent_.button_.onClick.Clear();
        unit004JobChangeMenu.setEventSelectedJob(cursor.cntlBtnNormal_.button_, i);
        if ((UnityEngine.Object) jobFrameSprite != (UnityEngine.Object) null)
          cursor.cntlBtnNormal_.sprite_.sprite2D = jobFrameSprite;
      }
      if (unit004JobChangeMenu.isExistChangePattern_)
      {
        bool flag1 = unit004JobChangeMenu.dicUnlocked_[p.job_id];
        bool flag2 = i != unit004JobChangeMenu.firstCousorIndex_ && (flag1 || unit004JobChangeMenu.dicCompletedMaterials_[p.job_id] && player.CheckZeny(unit004JobChangeMenu.dicCost_[p.job_id]));
        if ((UnityEngine.Object) cursor.objChain_ != (UnityEngine.Object) null)
          cursor.objChain_.SetActive(!flag1 || unit004JobChangeMenu.dictJobChangePatternsConditionsLocked[p.job_id]);
        if ((UnityEngine.Object) cursor.objActive_ != (UnityEngine.Object) null)
          cursor.objActive_.SetActive(flag2 && !unit004JobChangeMenu.dictJobChangePatternsConditionsLocked[p.job_id]);
        if ((UnityEngine.Object) cursor.objUnopened_ != (UnityEngine.Object) null)
          cursor.objUnopened_.SetActive(!flag2 && !flag1 || unit004JobChangeMenu.dictJobChangePatternsConditionsLocked[p.job_id]);
      }
      cursor = (Unit004JobChangeMenu.Cursor) null;
      p = (PlayerUnit) null;
    }
    for (int length = unit004JobChangeMenu.changeUnits_.Length; length < unit004JobChangeMenu.cursors_.Length; ++length)
      unit004JobChangeMenu.cursors_[length].objTop_.SetActive(false);
    ((IEnumerable<GameObject>) unit004JobChangeMenu.cursorLines_).ToggleOnceEx(unit004JobChangeMenu.changeUnits_.Length - 2);
  }

  private IEnumerator loadJobFrameSprite(UnitUnit unit, int jobid, bool bCurrent)
  {
    string key = this.createKeyJobFrameSprite(jobid, bCurrent);
    if (!this.dicJobSelector_.ContainsKey(key))
    {
      Future<UnityEngine.Sprite> ldFrame = unit.LoadSpriteJobFrame(jobid, bCurrent);
      yield return (object) ldFrame.Wait();
      this.dicJobSelector_[key] = ldFrame.Result;
    }
  }

  private UnityEngine.Sprite getJobFrameSprite(int jobid, bool bCurrent)
  {
    return this.dicJobSelector_[this.createKeyJobFrameSprite(jobid, bCurrent)];
  }

  private string createKeyJobFrameSprite(int jobid, bool bCurrent)
  {
    return jobid.ToString() + "_" + (bCurrent ? "1" : "0");
  }

  private void setEventSelectedJob(UIButton button, int index)
  {
    EventDelegate.Set(button.onClick, (EventDelegate.Callback) (() => this.onSelectedJob(index)));
  }

  private void onSelectedJob(int index)
  {
    if (this.isCustomPushAndSet())
      return;
    this.changeFocusJob(index, false);
  }

  private void changeFocusJob(int index, bool isForce = false)
  {
    if (!isForce && this.focusCousorIndex_ == index)
      this.IsPush = false;
    else
      this.StartCoroutine(this.doChangeFocusJob(index, false));
  }

  private IEnumerator doChangeFocusJob(int index, bool bReset = false)
  {
    Unit004JobChangeMenu unit004JobChangeMenu = this;
    unit004JobChangeMenu.cursors_[unit004JobChangeMenu.focusCousorIndex_].objFocus_.SetActive(false);
    unit004JobChangeMenu.cursors_[index].objFocus_.SetActive(true);
    if (unit004JobChangeMenu.isExistChangePattern_ && unit004JobChangeMenu.firstChangeJob_)
    {
      unit004JobChangeMenu.firstChangeJob_ = false;
      yield return (object) unit004JobChangeMenu.doDownloadPreviewData();
    }
    DetailMenuPrefab nowDetail = unit004JobChangeMenu.details_[unit004JobChangeMenu.focusCousorIndex_];
    DetailMenuPrefab nextDetail = unit004JobChangeMenu.details_[index];
    int pos = bReset ? 2 : nowDetail.getCenterItemPositionDiffMode();
    nextDetail.normal.InformationScrollView.GetComponent<UIWidget>().alpha = 0.0f;
    nextDetail.setActiveDiffMode(true);
    nextDetail.resetItemPositionDiffMode(pos);
    yield return (object) null;
    if (unit004JobChangeMenu.focusCousorIndex_ != index)
      nowDetail.setActiveDiffMode(false);
    nextDetail.normal.InformationScrollView.GetComponent<UIWidget>().alpha = 1f;
    PlayerUnit nowUnit = unit004JobChangeMenu.changeUnits_[unit004JobChangeMenu.focusCousorIndex_];
    PlayerUnit nextUnit = unit004JobChangeMenu.changeUnits_[index];
    unit004JobChangeMenu.focusCousorIndex_ = index;
    yield return (object) unit004JobChangeMenu.doUpdateMaterials();
    if (bReset || JobChangeUtil.checkDiff3DModel(nowUnit, nextUnit))
      yield return (object) unit004JobChangeMenu.doLoad3DModel(unit004JobChangeMenu.changeUnits_[index]);
    if (!bReset)
      unit004JobChangeMenu.IsPush = false;
  }

  private IEnumerator doDownloadPreviewData()
  {
    Singleton<CommonRoot>.GetInstance().loadingMode = 1;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    if (this.previewJob_ == null)
    {
      Future<WebAPI.Response.UnitPreviewJob> dpd = WebAPI.UnitPreviewJob(this.currentUnit_.id, (System.Action<WebAPI.Response.UserError>) (error =>
      {
        WebAPI.DefaultUserErrorCallback(error);
        Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
      }));
      yield return (object) dpd.Wait();
      this.previewJob_ = dpd.Result;
      dpd = (Future<WebAPI.Response.UnitPreviewJob>) null;
    }
    if (this.previewJob_ != null)
    {
      for (int index = 0; index < this.changeUnits_.Length; ++index)
      {
        if (index != this.firstCousorIndex_ && this.changeUnits_[index].job_abilities != null)
        {
          foreach (PlayerUnitJob_abilities jobAbility in this.changeUnits_[index].job_abilities)
          {
            PlayerUnitJob_abilities job_ability = jobAbility;
            WebAPI.Response.UnitPreviewJobJob_abilities previewJobJobAbilities = Array.Find<WebAPI.Response.UnitPreviewJobJob_abilities>(this.previewJob_.job_abilities, (Predicate<WebAPI.Response.UnitPreviewJobJob_abilities>) (j => j.job_ability_id == job_ability.job_ability_id));
            if (previewJobJobAbilities != null)
              job_ability.level = previewJobJobAbilities.level;
          }
        }
      }
    }
    yield return (object) this.finalizeDetails();
    Singleton<CommonRoot>.GetInstance().isLoading = false;
    Singleton<CommonRoot>.GetInstance().loadingMode = 0;
  }

  private IEnumerator initMaterials()
  {
    Unit004JobChangeMenu menu = this;
    if ((UnityEngine.Object) menu.materialSwapBuff_[0] == (UnityEngine.Object) null)
    {
      Future<GameObject> ldIcon = Res.Prefabs.UnitIcon.normal.Load<GameObject>();
      yield return (object) ldIcon.Wait();
      menu.materialSwapBuff_[0] = menu.materialContainer_;
      menu.materialSwapBuff_[1] = menu.materialContainer_.gameObject.Clone(menu.materialContainer_.transform.parent).GetComponent<Unit004JobChangeMaterialContainer>();
      menu.materialSwapBuff_[0].initialize(menu, ldIcon.Result);
      menu.materialSwapBuff_[1].initialize(menu, ldIcon.Result);
      menu.materialSwapIndex_ = 0;
      foreach (Unit004JobChangeMaterialContainer materialContainer in menu.materialSwapBuff_)
        materialContainer.isEnabled = true;
      yield return (object) null;
      foreach (Unit004JobChangeMaterialContainer materialContainer in menu.materialSwapBuff_)
        materialContainer.isEnabled = false;
      ldIcon = (Future<GameObject>) null;
    }
    yield return (object) menu.doUpdateMaterials();
  }

  private IEnumerator doUpdateMaterials()
  {
    Unit004JobChangeMenu unit004JobChangeMenu = this;
    if (unit004JobChangeMenu.isExistChangePattern_)
    {
      Unit004JobChangeMaterialContainer nowContainer = unit004JobChangeMenu.materialSwapBuff_[unit004JobChangeMenu.materialSwapIndex_];
      unit004JobChangeMenu.materialSwapIndex_ = unit004JobChangeMenu.materialSwapIndex_ + 1 & 1;
      Unit004JobChangeMaterialContainer nextContainer = unit004JobChangeMenu.materialSwapBuff_[unit004JobChangeMenu.materialSwapIndex_];
      Player player = SMManager.Get<Player>();
      int jobId = unit004JobChangeMenu.changeUnits_[unit004JobChangeMenu.focusCousorIndex_].job_id;
      PlayerMaterialUnit[] dicMaterial = unit004JobChangeMenu.dicMaterials_[jobId];
      nextContainer.setAlpha(0.0f);
      nextContainer.isEnabled = true;
      bool bUnlocked = unit004JobChangeMenu.dicUnlocked_[jobId];
      yield return (object) nextContainer.doUpdateMaterials(dicMaterial, unit004JobChangeMenu.focusCousorIndex_ == 0, bUnlocked, unit004JobChangeMenu.dictJobChangePatternsConditionsLocked[jobId]);
      int num = unit004JobChangeMenu.dicCost_[jobId];
      bool flag = player.CheckZeny(num);
      if (!bUnlocked && num >= 0)
      {
        unit004JobChangeMenu.txtCostZeny_.SetTextLocalize(num);
        unit004JobChangeMenu.txtCostZeny_.color = flag ? Color.white : Color.red;
        unit004JobChangeMenu.txtOwnedZeny_.color = Color.white;
      }
      else
      {
        unit004JobChangeMenu.txtCostZeny_.SetTextLocalize(Consts.GetInstance().JOBCHANGE_NULL_QUANTITY);
        unit004JobChangeMenu.txtCostZeny_.color = unit004JobChangeMenu.txtOwnedZeny_.color = Color.gray;
      }
      nowContainer.isEnabled = false;
      nextContainer.setAlpha(1f);
      unit004JobChangeMenu.btnExecute_.isEnabled = unit004JobChangeMenu.focusCousorIndex_ != unit004JobChangeMenu.firstCousorIndex_ && (bUnlocked || unit004JobChangeMenu.dicCompletedMaterials_[jobId] & flag) && !unit004JobChangeMenu.dictJobChangePatternsConditionsLocked[jobId];
      if ((UnityEngine.Object) unit004JobChangeMenu.maskReleased_ != (UnityEngine.Object) null)
      {
        unit004JobChangeMenu.maskReleased_.SetActive(((unit004JobChangeMenu.focusCousorIndex_ == 0 ? 0 : (unit004JobChangeMenu.focusCousorIndex_ != unit004JobChangeMenu.firstCousorIndex_ ? 1 : 0)) & (bUnlocked ? 1 : 0)) != 0 || unit004JobChangeMenu.dictJobChangePatternsConditionsLocked[jobId]);
        if (unit004JobChangeMenu.maskReleased_.activeSelf)
        {
          if (unit004JobChangeMenu.dictJobChangePatternsConditionsLocked[jobId])
          {
            unit004JobChangeMenu.MaskReleasedTextAttentionButton.gameObject.SetActive(true);
            // ISSUE: reference to a compiler-generated method
            EventDelegate.Set(unit004JobChangeMenu.MaskReleasedTextAttentionButton.onClick, new EventDelegate.Callback(unit004JobChangeMenu.\u003CdoUpdateMaterials\u003Eb__142_0));
            unit004JobChangeMenu.MaskReleasedTextAttention.text = Consts.GetInstance().JOB_CHANGE_NOT_ENOUGH_LV;
          }
          else
          {
            unit004JobChangeMenu.MaskReleasedTextAttentionButton.gameObject.SetActive(false);
            unit004JobChangeMenu.MaskReleasedTextAttention.text = Consts.GetInstance().JOB_CHANGE_NEED_NOT_MATERIAL_MONEY;
          }
        }
      }
    }
  }

  private IEnumerator initDetails()
  {
    Unit004JobChangeMenu unit004JobChangeMenu = this;
    if (!((UnityEngine.Object) unit004JobChangeMenu.lnkDetail_ == (UnityEngine.Object) null))
    {
      // ISSUE: explicit non-virtual call
      if ((UnityEngine.Object) __nonvirtual (unit004JobChangeMenu.detailPrefab) == (UnityEngine.Object) null)
        yield return (object) unit004JobChangeMenu.loadDetailPrefabs();
      if (unit004JobChangeMenu.details_ != null)
      {
        foreach (DetailMenuPrefab detail in unit004JobChangeMenu.details_)
        {
          if ((UnityEngine.Object) detail != (UnityEngine.Object) null)
            UnityEngine.Object.Destroy((UnityEngine.Object) detail.gameObject);
        }
      }
      unit004JobChangeMenu.details_ = new DetailMenuPrefab[unit004JobChangeMenu.changeUnits_.Length];
      for (int n = 0; n < unit004JobChangeMenu.changeUnits_.Length; ++n)
      {
        // ISSUE: explicit non-virtual call
        unit004JobChangeMenu.details_[n] = __nonvirtual (unit004JobChangeMenu.detailPrefab).Clone(unit004JobChangeMenu.lnkDetail_).GetComponent<DetailMenuPrefab>();
        if (n == unit004JobChangeMenu.firstCousorIndex_)
        {
          unit004JobChangeMenu.details_[n].gameObject.SetActive(true);
          yield return (object) unit004JobChangeMenu.details_[n].initAsyncDiffMode(unit004JobChangeMenu.currentUnit_, (PlayerUnit) null, 2, (IDetailMenuContainer) unit004JobChangeMenu);
          unit004JobChangeMenu.setEventsOnClickedJobAbility(unit004JobChangeMenu.currentUnit_, unit004JobChangeMenu.details_[n], true);
          unit004JobChangeMenu.details_[n].setActiveDiffMode(true);
        }
        else
          unit004JobChangeMenu.details_[n].setActiveDiffMode(false);
      }
    }
  }

  private IEnumerator finalizeDetails()
  {
    Unit004JobChangeMenu unit004JobChangeMenu = this;
    int center = unit004JobChangeMenu.details_[unit004JobChangeMenu.firstCousorIndex_].getCenterItemPositionDiffMode();
    for (int n = 0; n < unit004JobChangeMenu.changeUnits_.Length; ++n)
    {
      if (n != unit004JobChangeMenu.firstCousorIndex_)
      {
        unit004JobChangeMenu.details_[n].gameObject.SetActive(true);
        yield return (object) unit004JobChangeMenu.details_[n].initAsyncDiffMode(unit004JobChangeMenu.changeUnits_[n], unit004JobChangeMenu.currentUnit_, center, (IDetailMenuContainer) unit004JobChangeMenu);
        unit004JobChangeMenu.setEventsOnClickedJobAbility(unit004JobChangeMenu.changeUnits_[n], unit004JobChangeMenu.details_[n], false);
        unit004JobChangeMenu.details_[n].gameObject.SetActive(false);
      }
    }
  }

  private void setEventsOnClickedJobAbility(
    PlayerUnit target,
    DetailMenuPrefab detailTop,
    bool bCurrent)
  {
    PlayerUnitJob_abilities[] unitJobAbilitiesArray = target.job_abilities ?? new PlayerUnitJob_abilities[0];
    List<PlayerUnitJob_abilities> source = new List<PlayerUnitJob_abilities>();
    foreach (PlayerUnitJob_abilities unitJobAbilities in unitJobAbilitiesArray)
    {
      if (!unitJobAbilities.skill.IsLand)
        source.Add(unitJobAbilities);
    }
    UIButton[] btnJobAbilities = detailTop.normal.btnJobAbilities;
    for (int index = 0; index < btnJobAbilities.Length; ++index)
    {
      UIButton btn = btnJobAbilities[index];
      btn.onClick.Clear();
      if (index < source.Count<PlayerUnitJob_abilities>())
      {
        if (bCurrent)
          this.setEventOnClickedJobAbility(btn, target, source[index]);
        else
          this.setEventOnClickedJobAbility(btn, source[index]);
      }
    }
  }

  private void setEventOnClickedJobAbility(
    UIButton btn,
    PlayerUnit target,
    PlayerUnitJob_abilities dat)
  {
    if (!MasterData.BattleskillSkill.TryGetValue(dat.skill_id, out BattleskillSkill _))
      return;
    EventDelegate.Set(btn.onClick, (EventDelegate.Callback) (() => this.showDetailJobAbility(target, dat)));
  }

  private void setEventOnClickedJobAbility(UIButton btn, PlayerUnitJob_abilities dat)
  {
    if (!MasterData.BattleskillSkill.TryGetValue(dat.skill_id, out BattleskillSkill _))
      return;
    EventDelegate.Set(btn.onClick, (EventDelegate.Callback) (() => this.showDetailJobAbility(dat)));
  }

  private void showDetailJobAbility(PlayerUnitJob_abilities dat)
  {
    this.currentJobAbility_ = (PlayerUnitJob_abilities) null;
    this.StartCoroutine("doPopupDetailJobAbility", (object) dat);
  }

  private IEnumerator doPopupDetailJobAbility(PlayerUnitJob_abilities dat)
  {
    GameObject go = Singleton<PopupManager>.GetInstance().open(this.detailJobAbilityPrefab, false, false, false, true, false, false, "SE_1006");
    go.SetActive(false);
    yield return (object) go.GetComponent<Unit004JobDialog>().Init(dat);
    go.SetActive(true);
  }

  private void showDetailJobAbility(PlayerUnit target, PlayerUnitJob_abilities dat)
  {
    if (dat.current_levelup_pattern != null)
    {
      if (this.isCustomPushAndSet())
        return;
      this.StartCoroutine(this.doLevelupJobAbility(target, dat));
    }
    else
      this.showDetailJobAbility(dat);
  }

  private IEnumerator doLevelupJobAbility(PlayerUnit target, PlayerUnitJob_abilities dat)
  {
    // ISSUE: reference to a compiler-generated field
    int num = this.\u003C\u003E1__state;
    Unit004JobChangeMenu unit004JobChangeMenu = this;
    GameObject go;
    if (num != 0)
    {
      if (num != 1)
        return false;
      // ISSUE: reference to a compiler-generated field
      this.\u003C\u003E1__state = -1;
      Singleton<PopupManager>.GetInstance().startOpenAnime(go, false);
      return false;
    }
    // ISSUE: reference to a compiler-generated field
    this.\u003C\u003E1__state = -1;
    unit004JobChangeMenu.currentJobAbility_ = dat;
    go = Singleton<PopupManager>.GetInstance().open(unit004JobChangeMenu.levelupJobAbilityPrefab, false, false, false, true, true, true, "SE_1006");
    // ISSUE: reference to a compiler-generated field
    this.\u003C\u003E2__current = (object) go.GetComponent<Unit004JobDialogUp>().Init(target, dat, new System.Action(unit004JobChangeMenu.updatedJobAbility), true);
    // ISSUE: reference to a compiler-generated field
    this.\u003C\u003E1__state = 1;
    return true;
  }

  private void updatedJobAbility()
  {
    this.StopCoroutine("doResetScene");
    this.StartCoroutine("doResetScene");
  }

  private IEnumerator doResetScene()
  {
    while (Singleton<PopupManager>.GetInstance().isOpen || Singleton<PopupManager>.GetInstance().isRunningCoroutine)
      yield return (object) null;
    Singleton<CommonRoot>.GetInstance().loadingMode = 0;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    yield return (object) this.initializeAsync(this.param_);
    Singleton<CommonRoot>.GetInstance().isLoading = false;
  }

  private IEnumerator loadDetailPrefabs()
  {
    Future<GameObject> loader = new ResourceObject("Prefabs/unit004_2/detail").Load<GameObject>();
    yield return (object) loader.Wait();
    this.detailPrefab = loader.Result;
    loader = Res.Prefabs.ItemIcon.prefab.Load<GameObject>();
    yield return (object) loader.Wait();
    this.gearIconPrefab = loader.Result;
    loader = Res.Icons.GearKindIcon.Load<GameObject>();
    yield return (object) loader.Wait();
    this.gearKindIconPrefab = loader.Result;
    loader = PopupSkillDetails.createPrefabLoader(false);
    yield return (object) loader.Wait();
    this.skillDetailDialogPrefab = loader.Result;
    loader = new ResourceObject("Prefabs/unit004_2/SpecialPoint_DetailDialog").Load<GameObject>();
    yield return (object) loader.Wait();
    this.specialPointDetailDialogPrefab = loader.Result;
    loader = new ResourceObject("Prefabs/unit004_2/TerraiAbilityDialog").Load<GameObject>();
    yield return (object) loader.Wait();
    this.terraiAbilityDialogPrefab = loader.Result;
    loader = Res.Icons.GearProfiencyIcon.Load<GameObject>();
    yield return (object) loader.Wait();
    this.profIconPrefab = loader.Result;
    loader = Res.Prefabs.BattleSkillIcon._battleSkillIcon.Load<GameObject>();
    yield return (object) loader.Wait();
    this.skillTypeIconPrefab = loader.Result;
    loader = new ResourceObject("Prefabs/SkillFamily/SkillFamilyIcon").Load<GameObject>();
    yield return (object) loader.Wait();
    this.skillfullnessIconPrefab = loader.Result;
    loader = Res.Icons.CommonElementIcon.Load<GameObject>();
    yield return (object) loader.Wait();
    this.commonElementIconPrefab = loader.Result;
    loader = Res.Icons.SPAtkTypeIcon.Load<GameObject>();
    yield return (object) loader.Wait();
    this.spAtkTypeIconPrefab = loader.Result;
    loader = Res.Prefabs.unit.dir_unit_status_detail.Load<GameObject>();
    yield return (object) loader.Wait();
    this.statusDetailPrefab = loader.Result;
    loader = new ResourceObject("Prefabs/battle017_11_1_1/popup_SkillList").Load<GameObject>();
    yield return (object) loader.Wait();
    this.skillListPrefab = loader.Result;
    loader = Res.Prefabs.unit004_Job.Unit_JobCharacteristic_Dialog.Load<GameObject>();
    yield return (object) loader.Wait();
    this.detailJobAbilityPrefab = loader.Result;
    loader = new ResourceObject("Prefabs/unit004_Job/Unit_JobCharacteristic_UP_Dialog").Load<GameObject>();
    yield return (object) loader.Wait();
    this.levelupJobAbilityPrefab = loader.Result;
    loader = PopupJobChangeConditions.createLoader();
    yield return (object) loader.Wait();
    this.jobChangeConditionsPrefab = loader.Result;
    Future<GameObject>[] loaders = PopupUnityValueDetail.createLoaders(false);
    yield return (object) loaders[0].Wait();
    this.unityDetailPrefab = loaders[0].Result;
    yield return (object) loaders[1].Wait();
    this.stageItemPrefab = loaders[1].Result;
    loaders = (Future<GameObject>[]) null;
    loader = new ResourceObject("Prefabs/BattleSkillIcon/dir_SkillLock").Load<GameObject>();
    yield return (object) loader.Wait();
    this.skillLockIconPrefab = loader.Result;
  }

  private IEnumerator init3DModels()
  {
    this.reset3DModelAmbientLightColor();
    yield return (object) this.doLoad3DModel(this.changeUnits_[this.focusCousorIndex_]);
  }

  private IEnumerator doLoad3DModel(PlayerUnit playerUnit)
  {
    this.isLoading3DModel_ = true;
    if ((UnityEngine.Object) this.objLoading3DModel_ != (UnityEngine.Object) null)
      this.objLoading3DModel_.SetActive(true);
    yield return (object) this.doModelDestroy();
    this.ui3DModel_ = this.backup3DModel_.Clone(this.backup3DModel_.transform.parent).GetComponent<UI3DModel>();
    this.ui3DModel_.gameObject.SetActive(true);
    if ((UnityEngine.Object) this.ui3DModel_.model_creater_ != (UnityEngine.Object) null)
      this.ui3DModel_.model_creater_.BaseModel = (GameObject) null;
    if (this.localTop3DModel_.HasValue)
    {
      Vector3 vector3 = this.localTop3DModel_.Value;
      UnitRenderingPattern renderingPattern = playerUnit.getJobData().rendering_pattern;
      if (renderingPattern != null)
      {
        vector3.x += renderingPattern.texture_x;
        vector3.y += renderingPattern.texture_y;
      }
      this.top3DModel_.localPosition = vector3;
      this.top3DModel_.gameObject.SetActive(false);
      this.top3DModel_.gameObject.SetActive(true);
    }
    if (playerUnit != (PlayerUnit) null)
      yield return (object) this.ui3DModel_.JobChange(playerUnit);
    if ((UnityEngine.Object) this.objLoading3DModel_ != (UnityEngine.Object) null)
      this.objLoading3DModel_.SetActive(false);
    this.isLoading3DModel_ = false;
  }

  private IEnumerator doModelDestroy()
  {
    if ((UnityEngine.Object) this.ui3DModel_ != (UnityEngine.Object) null)
      this.ui3DModel_.Remove();
    if (ModelUnits.Instance.ModelList != null)
    {
      for (int i = 0; i < ModelUnits.Instance.ModelList.Count; ++i)
      {
        if ((UnityEngine.Object) ModelUnits.Instance.ModelList[i] != (UnityEngine.Object) null)
          yield return (object) this.doDestroyPoolableObject(ModelUnits.Instance.ModelList[i].gameObject);
      }
    }
    if ((UnityEngine.Object) this.ui3DModel_ != (UnityEngine.Object) null)
    {
      UnityEngine.Object.Destroy((UnityEngine.Object) this.ui3DModel_.gameObject);
      this.ui3DModel_ = (UI3DModel) null;
    }
    yield return (object) null;
  }

  private IEnumerator doDestroyPoolableObject(GameObject go)
  {
    while ((bool) (UnityEngine.Object) go)
    {
      ObjectPoolController.Destroy(go);
      if (!go.activeSelf)
        break;
      yield return (object) null;
    }
  }

  private void OnDisable()
  {
    this.clear3DModel();
  }

  private void clear3DModel()
  {
    this.isLoading3DModel_ = false;
    if (!((UnityEngine.Object) this.ui3DModel_ != (UnityEngine.Object) null))
      return;
    this.ui3DModel_.Remove();
    ModelUnits.Instance.DestroyModelUnits();
  }

  protected override void Update()
  {
    base.Update();
    int num = this.isInitialize_ ? 1 : 0;
  }

  public override void onBackButton()
  {
    this.onClickedBack();
  }

  public bool isCustomPushAndSet()
  {
    return this.isInitialize_ || this.isBlockTouch || this.IsPushAndSet();
  }

  private bool isBlockTouch
  {
    get
    {
      return this.isLoading3DModel_ || Singleton<CommonRoot>.GetInstance().isLoading;
    }
  }

  public void onClickedBack()
  {
    if (this.isCustomPushAndSet())
      return;
    if (this.param_.onBackScene_ != null)
      this.param_.onBackScene_();
    else
      this.backScene();
  }

  public void onClickedExecute()
  {
    if (this.isCustomPushAndSet())
      return;
    this.StartCoroutine(this.doCheckJobChange());
  }

  private IEnumerator doCheckJobChange()
  {
    Unit004JobChangeMenu unit004JobChangeMenu = this;
    PlayerUnit afterUnit = unit004JobChangeMenu.changeUnits_[unit004JobChangeMenu.focusCousorIndex_];
    Singleton<PopupManager>.GetInstance().open((GameObject) null, false, false, false, false, false, false, "SE_1006");
    Consts instance = Consts.GetInstance();
    Unit004JobChangeMenu.ConfirmPopup[] confirmPopupArray1 = new Unit004JobChangeMenu.ConfirmPopup[4];
    Unit004JobChangeMenu.ConfirmPopup confirmPopup1 = new Unit004JobChangeMenu.ConfirmPopup();
    confirmPopup1.checkOpen_ = (Func<bool>) (() => !this.dicUnlocked_[afterUnit.job_id] && (uint) this.dicMaterials_[afterUnit.job_id].Length > 0U);
    confirmPopup1.title_ = instance.JOB_CHANGE_TITLE;
    confirmPopup1.desc_ = instance.JOB_CHANGE_DESC;
    confirmPopupArray1[0] = confirmPopup1;
    confirmPopup1 = new Unit004JobChangeMenu.ConfirmPopup();
    confirmPopup1.checkOpen_ = (Func<bool>) (() => MasterData.UnitJob[afterUnit.job_id].new_cost > 0);
    confirmPopup1.title_ = instance.JOB_CHANGE_TITLE;
    confirmPopup1.desc_ = instance.JOB_CHANGE_DESC_COST_CHANGE;
    confirmPopupArray1[1] = confirmPopup1;
    confirmPopup1 = new Unit004JobChangeMenu.ConfirmPopup();
    confirmPopup1.checkOpen_ = (Func<bool>) (() =>
    {
      bool flag = false;
      PlayerTransmigrateMemoryPlayerUnitIds current = PlayerTransmigrateMemoryPlayerUnitIds.Current;
      if (current?.transmigrate_memory_player_unit_ids != null && current.transmigrate_memory_player_unit_ids.Length != 0)
      {
        int cur = this.currentUnit_.id;
        flag = ((IEnumerable<int?>) current.transmigrate_memory_player_unit_ids).Any<int?>((Func<int?, bool>) (n => n.HasValue && n.Value == cur));
      }
      return flag;
    });
    confirmPopup1.title_ = instance.JOB_CHANGE_MEMORY_TITLE;
    confirmPopup1.desc_ = instance.JOB_CHANGE_MEMORY_DESC;
    confirmPopupArray1[2] = confirmPopup1;
    confirmPopup1 = new Unit004JobChangeMenu.ConfirmPopup();
    confirmPopup1.checkOpen_ = (Func<bool>) (() => this.currentUnit_.equippedGear != (PlayerItem) null && afterUnit.equippedGear == (PlayerItem) null);
    confirmPopup1.title_ = instance.JOB_CHANGE_GEAR_TITLE;
    confirmPopup1.desc_ = instance.JOB_CHANGE_GEAR_DESC;
    confirmPopupArray1[3] = confirmPopup1;
    Unit004JobChangeMenu.ConfirmPopup[] confirmPopupArray = confirmPopupArray1;
    for (int index = 0; index < confirmPopupArray.Length; ++index)
    {
      Unit004JobChangeMenu.ConfirmPopup confirmPopup2 = confirmPopupArray[index];
      if (confirmPopup2.checkOpen_())
      {
        bool bWait = true;
        bool bCancel = false;
        PopupCommonNoYes.Show(confirmPopup2.title_, confirmPopup2.desc_, (System.Action) (() => bWait = false), (System.Action) (() =>
        {
          bCancel = true;
          bWait = false;
        }), NGUIText.Alignment.Center, (string) null, NGUIText.Alignment.Left, false);
        while (bWait)
          yield return (object) null;
        if (!bCancel)
          ;
        else
        {
          unit004JobChangeMenu.IsPush = false;
          Singleton<PopupManager>.GetInstance().closeAll(false);
          yield break;
        }
      }
    }
    confirmPopupArray = (Unit004JobChangeMenu.ConfirmPopup[]) null;
    Singleton<PopupManager>.GetInstance().closeAll(false);
    Singleton<CommonRoot>.GetInstance().loadingMode = 1;
    Future<WebAPI.Response.UnitJobchange> future = WebAPI.UnitJobchange(unit004JobChangeMenu.currentUnit_.id, unit004JobChangeMenu.dicUnlocked_[afterUnit.job_id] ? new int[0] : ((IEnumerable<PlayerMaterialUnit>) unit004JobChangeMenu.dicMaterials_[afterUnit.job_id]).Select<PlayerMaterialUnit, int>((Func<PlayerMaterialUnit, int>) (m => m.id)).ToArray<int>(), afterUnit.job_id, (System.Action<WebAPI.Response.UserError>) (error =>
    {
      WebAPI.DefaultUserErrorCallback(error);
      Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
    }));
    yield return (object) future.Wait();
    if (future.Result != null)
    {
      afterUnit = Array.Find<PlayerUnit>(future.Result.player_units, (Predicate<PlayerUnit>) (pu => pu.id == this.currentUnit_.id));
      unit004JobChangeMenu.jobChanged(unit004JobChangeMenu.currentUnit_, afterUnit, future.Result.is_new);
    }
  }

  private void jobChanged(PlayerUnit beforeUnit, PlayerUnit afterUnit, bool isNew)
  {
    this.wRevision_ = new long?();
    PrincesEvolutionParam princesEvolutionParam1 = new PrincesEvolutionParam();
    princesEvolutionParam1.mode = Unit00499Scene.Mode.JobChange;
    princesEvolutionParam1.baseUnit = beforeUnit;
    princesEvolutionParam1.resultUnit = afterUnit;
    princesEvolutionParam1.fromEarth = false;
    princesEvolutionParam1.is_new = isNew;
    int count = 1;
    PrincesEvolutionParam princesEvolutionParam2 = princesEvolutionParam1;
    List<PlayerUnit> playerUnitList;
    if (!this.dicUnlocked_[afterUnit.job_id])
    {
      PlayerMaterialUnit[] dicMaterial = this.dicMaterials_[afterUnit.job_id];
      playerUnitList = (dicMaterial != null ? ((IEnumerable<PlayerMaterialUnit>) dicMaterial).Select<PlayerMaterialUnit, PlayerUnit>((Func<PlayerMaterialUnit, PlayerUnit>) (mu => PlayerUnit.CreateByPlayerMaterialUnit(mu, count++))).ToList<PlayerUnit>() : (List<PlayerUnit>) null) ?? new List<PlayerUnit>();
    }
    else
      playerUnitList = new List<PlayerUnit>();
    princesEvolutionParam2.materiaqlUnits = playerUnitList;
    unit00497Scene.ChangeScene(true, princesEvolutionParam1);
  }

  private class ButtonControl
  {
    public UI2DSprite sprite_ { get; private set; }

    public UIButton button_ { get; private set; }

    public ButtonControl(GameObject go)
    {
      this.sprite_ = go.GetComponent<UI2DSprite>();
      this.button_ = go.GetComponent<UIButton>();
    }
  }

  [Serializable]
  private class Cursor
  {
    public GameObject objTop_;
    public GameObject objFocus_;
    public GameObject btnCurrent_;
    [NonSerialized]
    public Unit004JobChangeMenu.ButtonControl cntlBtnCurrent_;
    public GameObject btnNormal_;
    [NonSerialized]
    public Unit004JobChangeMenu.ButtonControl cntlBtnNormal_;
    public UILabel txtJobName_;
    public UILabel txtRankName_;
    public GameObject objActive_;
    public GameObject objChain_;
    public GameObject objUnopened_;
  }

  private struct ConfirmPopup
  {
    public Func<bool> checkOpen_;
    public string title_;
    public string desc_;
  }
}
