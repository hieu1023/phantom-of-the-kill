﻿// Decompiled with JetBrains decompiler
// Type: ClipFieldEffect
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using System.Collections;
using UnityEngine;

public class ClipFieldEffect : BattleMonoBehaviour
{
  private NGSoundManager soundManager;
  private BL.Unit target;
  private GameObject[] prefabs;

  protected override IEnumerator Start_Battle()
  {
    this.soundManager = Singleton<NGSoundManager>.GetInstance();
    yield break;
  }

  public void onPlayEffect(string var)
  {
    int index = int.Parse(var);
    if (this.target == (BL.Unit) null || index >= this.prefabs.Length)
      return;
    this.battleManager.battleEffects.fieldEffectsStart(this.prefabs[index], this.target, 0.0f);
  }

  public void onPlayBGM(string var)
  {
    this.soundManager.playBGM(var, 0.1f);
  }

  public void onPlaySE(string var)
  {
    this.soundManager.playSE(var, false, 0.0f, -1);
  }

  public void onAnimationTrigger(string var)
  {
    if (this.target == (BL.Unit) null)
      return;
    this.env.unitResource[this.target].gameObject.GetComponent<UnitUpdate>().setAnimationTrigger(var);
  }

  public void onEndEffect()
  {
    this.battleManager.battleEffects.onPopupDismiss();
  }

  public void setEffectData(BL.FieldEffect effect)
  {
    if ((Object) this.battleManager == (Object) null)
      this.battleManager = Singleton<NGBattleManager>.GetInstance();
    this.prefabs = this.battleManager.battleEffects.effectResourceAllPrefabs(effect);
    if (effect.fieldEffect.category == BattleFieldEffectCategory.boss)
      this.target = this.env.core.getBossUnit();
    else
      this.target = (BL.Unit) null;
  }
}
