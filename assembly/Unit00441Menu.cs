﻿// Decompiled with JetBrains decompiler
// Type: Unit00441Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using UnityEngine;

public class Unit00441Menu : BackButtonMenuBase
{
  [SerializeField]
  protected UILabel TxtAttackLeft;
  [SerializeField]
  protected UILabel TxtMagicAttackLeft;
  [SerializeField]
  protected UILabel TxtDefenseLeft;
  [SerializeField]
  protected UILabel TxtMagicDefenseLeft;
  [SerializeField]
  protected UILabel TxtDexterityLeft;
  [SerializeField]
  protected UILabel TxtCriticalLeft;
  [SerializeField]
  protected UILabel TxtEvasionLeft;
  [SerializeField]
  protected UILabel TxtWaitLeft;
  [SerializeField]
  protected UILabel TxtRangeMinLeft;
  [SerializeField]
  protected UILabel TxtRangeMaxLeft;
  [SerializeField]
  protected UILabel TxtRangeLeft;
  [SerializeField]
  protected UILabel TxtAttackRight;
  [SerializeField]
  protected UILabel TxtMagicAttackRight;
  [SerializeField]
  protected UILabel TxtDefenseRight;
  [SerializeField]
  protected UILabel TxtMagicDefenseRight;
  [SerializeField]
  protected UILabel TxtDexterityRight;
  [SerializeField]
  protected UILabel TxtCriticalRight;
  [SerializeField]
  protected UILabel TxtEvasionRight;
  [SerializeField]
  protected UILabel TxtWaitRight;
  [SerializeField]
  protected UILabel TxtRangeMinRight;
  [SerializeField]
  protected UILabel TxtRangeMaxRight;
  [SerializeField]
  protected UILabel TxtRangeRight;
  [SerializeField]
  private Popup00441AddEffect StatusGaugeHp;
  [SerializeField]
  private Popup00441AddEffect StatusGaugeAttack;
  [SerializeField]
  private Popup00441AddEffect StatusGaugeMagicAttack;
  [SerializeField]
  private Popup00441AddEffect StatusGaugeDefense;
  [SerializeField]
  private Popup00441AddEffect StatusGaugeMental;
  [SerializeField]
  private Popup00441AddEffect StatusGaugeSpeed;
  [SerializeField]
  private Popup00441AddEffect StatusGaugeTechnique;
  [SerializeField]
  private Popup00441AddEffect StatusGaugeLucky;
  [SerializeField]
  protected GameObject BeforeGear;
  [SerializeField]
  protected GameObject AfterGear;
  [SerializeField]
  protected GameObject BeforeSkill_One_Base;
  [SerializeField]
  protected UIWidget[] BeforeSkill_One;
  [SerializeField]
  protected UIButton[] BeforeSkill_One_Button;
  [SerializeField]
  protected GameObject AfterSkill_One_Base;
  [SerializeField]
  protected UIWidget[] AfterSkill_One;
  [SerializeField]
  protected UIButton[] AfterSkill_One_Button;
  [SerializeField]
  protected GameObject BeforeSkill_Two_Base;
  [SerializeField]
  protected UIWidget[] BeforeSkill_Two;
  [SerializeField]
  protected UIButton[] BeforeSkill_Two_Button;
  [SerializeField]
  protected GameObject AfterSkill_Two_Base;
  [SerializeField]
  protected UIWidget[] AfterSkill_Two;
  [SerializeField]
  protected UIButton[] AfterSkill_Two_Button;
  private PlayerUnit basePlayerUnit;
  private PlayerItem afterGearItem;
  private PlayerUnit changeUnit;
  private int changeGearIndex;
  private bool isEarthMode;
  [SerializeField]
  private GameObject SkillDialog;
  [SerializeField]
  private GameObject floatingSkillDialog;
  private System.Action<GearGearSkill> showSkillDialog;

  private Color GetStrColor(int before, int after)
  {
    Color color = Color.white;
    if (before < after)
      color = new Color(0.0f, 0.8627451f, 0.1176471f);
    else if (before > after)
      color = new Color(0.9803922f, 0.0f, 0.0f);
    return color;
  }

  private IEnumerator setTexture(Future<UnityEngine.Sprite> src, UI2DSprite to)
  {
    return src.Then<UnityEngine.Sprite>((Func<UnityEngine.Sprite, UnityEngine.Sprite>) (sprite => to.sprite2D = sprite)).Wait();
  }

  protected void SetSkillDialogEvent(UIButton button, GearGearSkill skill_data)
  {
    if (this.showSkillDialog == null)
    {
      Battle0171111Event[] componentsInChildren = this.SkillDialog.Clone(this.floatingSkillDialog.transform).GetComponentsInChildren<Battle0171111Event>(true);
      Battle0171111Event dialog = componentsInChildren.Length != 0 ? componentsInChildren[0] : (Battle0171111Event) null;
      if ((UnityEngine.Object) dialog == (UnityEngine.Object) null)
        return;
      dialog.transform.parent.gameObject.SetActive(false);
      this.showSkillDialog = (System.Action<GearGearSkill>) (skill =>
      {
        dialog.setData(skill.skill);
        dialog.setSkillLv(skill.skill_level, skill.skill.upper_level);
        dialog.Show();
      });
    }
    EventDelegate.Set(button.onClick, (EventDelegate.Callback) (() => this.showSkillDialog(skill_data)));
  }

  public IEnumerator SetGear(
    PlayerUnit removeUnit,
    PlayerUnit baseUnit,
    PlayerItem beforeGear,
    PlayerItem afterGear,
    GameObject beforeGearIcon,
    GameObject afterGearIcon,
    GameObject[] beforeSkillIcons,
    GameObject[] afterSkillIcons,
    int index,
    bool isEarth = false)
  {
    this.isEarthMode = isEarth;
    this.changeUnit = removeUnit;
    this.changeGearIndex = index;
    this.basePlayerUnit = baseUnit;
    this.afterGearItem = afterGear;
    if (!(beforeGear == (PlayerItem) null))
    {
      PlayerItem equipReisou = beforeGear.equipReisou;
    }
    PlayerItem afterReisou = afterGear == (PlayerItem) null ? (PlayerItem) null : afterGear.equipReisou;
    if ((UnityEngine.Object) this.SkillDialog == (UnityEngine.Object) null)
    {
      Future<GameObject> loader = Res.Prefabs.battle017_11_1_1.SkillDetailDialog.Load<GameObject>();
      IEnumerator e = loader.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.SkillDialog = loader.Result;
      loader = (Future<GameObject>) null;
    }
    PlayerItem playerItem1 = baseUnit.equippedGear;
    PlayerItem playerItem2 = baseUnit.equippedGear2;
    int after = BattleFuncs.calcEquippedGearWeight((GearGear) null, playerItem1, playerItem2);
    PlayerItem playerReisou = baseUnit.equippedReisou;
    PlayerItem playerReisou2 = baseUnit.equippedReisou2;
    Judgement.NonBattleParameter nonBattleParameter1 = Judgement.NonBattleParameter.FromPlayerUnitWithPlayerGear(baseUnit, true, playerItem1, playerItem2, playerReisou, playerReisou2);
    if (baseUnit.unit.awake_unit_flag)
    {
      if (afterGear.gear.kind.Enum == GearKindEnum.shield || afterGear.gear.kind.Enum == GearKindEnum.accessories)
      {
        playerItem2 = afterGear;
        playerReisou2 = afterReisou;
      }
      else
      {
        playerItem1 = afterGear;
        playerReisou = afterReisou;
      }
    }
    else
    {
      playerItem1 = afterGear;
      playerReisou = afterReisou;
    }
    Judgement.NonBattleParameter nonBattleParameter2 = Judgement.NonBattleParameter.FromPlayerUnitWithPlayerGear(baseUnit, true, playerItem1, playerItem2, playerReisou, playerReisou2);
    int minRange1 = baseUnit.initial_gear.min_range;
    int maxRange1 = baseUnit.initial_gear.max_range;
    int minRange2 = baseUnit.initial_gear.min_range;
    int maxRange2 = baseUnit.initial_gear.max_range;
    int num1 = BattleFuncs.calcEquippedGearWeight((GearGear) null, playerItem1, playerItem2);
    int num2 = 0;
    int num3 = 0;
    int num4 = 0;
    int num5 = 0;
    int num6 = 0;
    int num7 = 0;
    int num8 = 0;
    int num9 = 0;
    beforeGearIcon.gameObject.SetParent(this.BeforeGear);
    beforeGearIcon.SetActive(false);
    beforeGearIcon.SetActive(true);
    if (beforeGear != (PlayerItem) null)
    {
      UIWidget[] uiWidgetArray = this.BeforeSkill_One;
      UIButton[] uiButtonArray = this.BeforeSkill_One_Button;
      this.BeforeSkill_One_Base.SetActive(true);
      this.BeforeSkill_Two_Base.SetActive(false);
      if (beforeSkillIcons != null && beforeSkillIcons.Length != 0)
      {
        if (beforeSkillIcons.Length > 1)
        {
          this.BeforeSkill_One_Base.SetActive(false);
          this.BeforeSkill_Two_Base.SetActive(true);
          uiWidgetArray = this.BeforeSkill_Two;
          uiButtonArray = this.BeforeSkill_Two_Button;
        }
        for (int index1 = 0; index1 < uiWidgetArray.Length; ++index1)
        {
          beforeSkillIcons[index1].GetComponent<BattleSkillIcon>().SetDepth(uiWidgetArray[index1].depth + 1);
          beforeSkillIcons[index1].SetParent(uiWidgetArray[index1].gameObject);
          this.SetSkillDialogEvent(uiButtonArray[index1], beforeGear.skills[index1]);
          beforeSkillIcons[index1].SetActive(false);
          beforeSkillIcons[index1].SetActive(true);
        }
      }
      if (beforeGear.gear.kind.isNonWeapon)
      {
        if (minRange1 < beforeGear.gear.min_range)
          minRange1 = beforeGear.gear.min_range;
        if (maxRange1 < beforeGear.gear.max_range)
          maxRange1 = beforeGear.gear.max_range;
      }
      else
      {
        minRange1 = beforeGear.gear.min_range;
        maxRange1 = beforeGear.gear.max_range;
      }
    }
    this.TxtAttackLeft.SetTextLocalize(nonBattleParameter1.PhysicalAttack.ToString());
    this.TxtMagicAttackLeft.SetTextLocalize(nonBattleParameter1.MagicAttack.ToString());
    this.TxtCriticalLeft.SetTextLocalize(nonBattleParameter1.Critical.ToString());
    this.TxtDefenseLeft.SetTextLocalize(nonBattleParameter1.PhysicalDefense.ToString());
    this.TxtDexterityLeft.SetTextLocalize(nonBattleParameter1.Hit.ToString());
    this.TxtEvasionLeft.SetTextLocalize(nonBattleParameter1.Evasion.ToString());
    this.TxtMagicDefenseLeft.SetTextLocalize(nonBattleParameter1.MagicDefense.ToString());
    this.TxtRangeMinLeft.SetTextLocalize(string.Format("{0}", (object) minRange1));
    this.TxtRangeMaxLeft.SetTextLocalize(string.Format("{0}", (object) maxRange1));
    this.TxtWaitLeft.SetTextLocalize(after.ToString());
    afterGearIcon.gameObject.SetParent(this.AfterGear);
    afterGearIcon.SetActive(false);
    afterGearIcon.SetActive(true);
    if (afterGear != (PlayerItem) null)
    {
      UIWidget[] uiWidgetArray = this.AfterSkill_One;
      UIButton[] uiButtonArray = this.AfterSkill_One_Button;
      this.AfterSkill_One_Base.SetActive(true);
      this.AfterSkill_Two_Base.SetActive(false);
      if (afterSkillIcons != null && afterSkillIcons.Length != 0)
      {
        if (afterSkillIcons.Length > 1)
        {
          this.AfterSkill_One_Base.SetActive(false);
          this.AfterSkill_Two_Base.SetActive(true);
          uiWidgetArray = this.AfterSkill_Two;
          uiButtonArray = this.AfterSkill_Two_Button;
        }
        for (int index1 = 0; index1 < uiWidgetArray.Length; ++index1)
        {
          afterSkillIcons[index1].GetComponent<BattleSkillIcon>().SetDepth(uiWidgetArray[index1].depth + 1);
          afterSkillIcons[index1].gameObject.SetParent(uiWidgetArray[index1].gameObject);
          this.SetSkillDialogEvent(uiButtonArray[index1], afterGear.skills[index1]);
          afterSkillIcons[index1].SetActive(false);
          afterSkillIcons[index1].SetActive(true);
        }
      }
      num2 = afterGear.hp_incremental;
      num3 = afterGear.strength_incremental;
      num4 = afterGear.intelligence_incremental;
      num5 = afterGear.vitality_incremental;
      num6 = afterGear.mind_incremental;
      num7 = afterGear.agility_incremental;
      num8 = afterGear.dexterity_incremental;
      num9 = afterGear.lucky_incremental;
      if (afterGear.gear.kind.isNonWeapon)
      {
        if (minRange2 < afterGear.gear.min_range)
          minRange2 = afterGear.gear.min_range;
        if (maxRange2 < afterGear.gear.max_range)
          maxRange2 = afterGear.gear.max_range;
      }
      else
      {
        minRange2 = afterGear.gear.min_range;
        maxRange2 = afterGear.gear.max_range;
      }
    }
    this.TxtAttackRight.SetTextLocalize(nonBattleParameter2.PhysicalAttack.ToString());
    this.TxtAttackRight.color = this.GetStrColor(nonBattleParameter1.PhysicalAttack, nonBattleParameter2.PhysicalAttack);
    this.TxtMagicAttackRight.SetTextLocalize(nonBattleParameter2.MagicAttack.ToString());
    this.TxtMagicAttackRight.color = this.GetStrColor(nonBattleParameter1.MagicAttack, nonBattleParameter2.MagicAttack);
    this.TxtCriticalRight.SetTextLocalize(nonBattleParameter2.Critical.ToString());
    this.TxtCriticalRight.color = this.GetStrColor(nonBattleParameter1.Critical, nonBattleParameter2.Critical);
    this.TxtDefenseRight.SetTextLocalize(nonBattleParameter2.PhysicalDefense.ToString());
    this.TxtDefenseRight.color = this.GetStrColor(nonBattleParameter1.PhysicalDefense, nonBattleParameter2.PhysicalDefense);
    this.TxtDexterityRight.SetTextLocalize(nonBattleParameter2.Hit.ToString());
    this.TxtDexterityRight.color = this.GetStrColor(nonBattleParameter1.Hit, nonBattleParameter2.Hit);
    this.TxtEvasionRight.SetTextLocalize(nonBattleParameter2.Evasion.ToString());
    this.TxtEvasionRight.color = this.GetStrColor(nonBattleParameter1.Evasion, nonBattleParameter2.Evasion);
    this.TxtMagicDefenseRight.SetTextLocalize(nonBattleParameter2.MagicDefense.ToString());
    this.TxtMagicDefenseRight.color = this.GetStrColor(nonBattleParameter1.MagicDefense, nonBattleParameter2.MagicDefense);
    this.TxtRangeMinRight.SetTextLocalize(string.Format("{0}", (object) minRange2));
    this.TxtRangeMinRight.color = this.GetStrColor(minRange1, minRange2);
    this.TxtRangeMaxRight.SetTextLocalize(string.Format("{0}", (object) maxRange2));
    this.TxtRangeMaxRight.color = this.GetStrColor(maxRange1, maxRange2);
    this.TxtWaitRight.SetTextLocalize(num1);
    this.TxtWaitRight.color = this.GetStrColor(num1, after);
    this.StatusGaugeHp.Init(num2);
    this.StatusGaugeAttack.Init(num3);
    this.StatusGaugeMagicAttack.Init(num4);
    this.StatusGaugeDefense.Init(num5);
    this.StatusGaugeMental.Init(num6);
    this.StatusGaugeSpeed.Init(num7);
    this.StatusGaugeTechnique.Init(num8);
    this.StatusGaugeLucky.Init(num9);
  }

  public void IbtnNo()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<PopupManager>.GetInstance().onDismiss();
  }

  public override void onBackButton()
  {
    this.IbtnNo();
  }

  public void IbtnPopupDecide()
  {
    if (this.IsPushAndSet())
      return;
    this.StartCoroutine(this.Equip());
  }

  private IEnumerator Equip()
  {
    Singleton<PopupManager>.GetInstance().closeAll(true);
    Singleton<CommonRoot>.GetInstance().loadingMode = 1;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    bool isSuccess = true;
    IEnumerator e1;
    if (this.changeUnit != (PlayerUnit) null)
    {
      int changeGearIndex = 1;
      if (this.changeUnit.equippedGear2 != (PlayerItem) null && this.changeUnit.equippedGear2.id == this.afterGearItem.id)
        changeGearIndex = 2;
      e1 = RequestDispatcher.EquipGear(changeGearIndex, new int?(0), this.changeUnit.id, (System.Action<WebAPI.Response.UserError>) (e =>
      {
        Singleton<CommonRoot>.GetInstance().isLoading = false;
        Singleton<CommonRoot>.GetInstance().loadingMode = 0;
        if (e != null)
          WebAPI.DefaultUserErrorCallback(e);
        isSuccess = false;
        Singleton<NGSceneManager>.GetInstance().backScene();
      }), this.isEarthMode);
      while (e1.MoveNext())
        yield return e1.Current;
      e1 = (IEnumerator) null;
      if (!isSuccess)
        yield break;
    }
    int num = 0;
    if (this.afterGearItem != (PlayerItem) null)
      num = this.afterGearItem.id;
    e1 = RequestDispatcher.EquipGear(this.changeGearIndex, new int?(num), this.basePlayerUnit.id, (System.Action<WebAPI.Response.UserError>) (e =>
    {
      Singleton<CommonRoot>.GetInstance().isLoading = false;
      Singleton<CommonRoot>.GetInstance().loadingMode = 0;
      if (e != null)
        WebAPI.DefaultUserErrorCallback(e);
      isSuccess = false;
      Singleton<NGSceneManager>.GetInstance().backScene();
    }), this.isEarthMode);
    while (e1.MoveNext())
      yield return e1.Current;
    e1 = (IEnumerator) null;
    if (isSuccess)
    {
      PlayerUnit playerUnit1 = (PlayerUnit) null;
      foreach (PlayerUnit playerUnit2 in SMManager.Get<PlayerUnit[]>())
      {
        if (playerUnit2.id == this.basePlayerUnit.id)
        {
          playerUnit1 = playerUnit2;
          break;
        }
      }
      this.basePlayerUnit = playerUnit1;
      switch (GuildUtil.gvgPopupState)
      {
        case GuildUtil.GvGPopupState.None:
          this.BackScene();
          break;
        case GuildUtil.GvGPopupState.AtkTeam:
          e1 = GuildUtil.UpdateGuildDeckAttack(PlayerAffiliation.Current.guild_id, Player.Current.id, (System.Action) (() => this.BackScene()));
          while (e1.MoveNext())
            yield return e1.Current;
          e1 = (IEnumerator) null;
          break;
        case GuildUtil.GvGPopupState.DefTeam:
          e1 = GuildUtil.UpdateGuildDeckDefanse(PlayerAffiliation.Current.guild_id, Player.Current.id, (System.Action) (() => this.BackScene()));
          while (e1.MoveNext())
            yield return e1.Current;
          e1 = (IEnumerator) null;
          break;
        default:
          this.BackScene();
          break;
      }
    }
  }

  private void BackScene()
  {
    Singleton<CommonRoot>.GetInstance().isLoading = false;
    Singleton<CommonRoot>.GetInstance().loadingMode = 0;
    Singleton<NGSceneManager>.GetInstance().backScene();
  }
}
