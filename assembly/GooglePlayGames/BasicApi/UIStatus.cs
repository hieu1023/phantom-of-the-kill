﻿// Decompiled with JetBrains decompiler
// Type: GooglePlayGames.BasicApi.UIStatus
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

namespace GooglePlayGames.BasicApi
{
  public enum UIStatus
  {
    LeftRoom = -18, // 0xFFFFFFEE
    UiBusy = -12, // 0xFFFFFFF4
    UserClosedUI = -6, // 0xFFFFFFFA
    Timeout = -5, // 0xFFFFFFFB
    VersionUpdateRequired = -4, // 0xFFFFFFFC
    NotAuthorized = -3, // 0xFFFFFFFD
    InternalError = -2, // 0xFFFFFFFE
    Valid = 1,
  }
}
