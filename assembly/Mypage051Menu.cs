﻿// Decompiled with JetBrains decompiler
// Type: Mypage051Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using Earth;
using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Mypage051Menu : MypageMenuBase
{
  private readonly List<int> baseVoiceIDList = new List<int>()
  {
    52,
    53,
    54
  };

  protected override IEnumerator CreateJogDial()
  {
    Mypage051Menu mypage051Menu = this;
    Future<GameObject> f = Res.Prefabs.mypage.CircleAnimation_Story.Load<GameObject>();
    IEnumerator e = f.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if ((UnityEngine.Object) mypage051Menu.Story_Container != (UnityEngine.Object) null && !Singleton<EarthDataManager>.GetInstance().questProgress.isCleared)
      mypage051Menu.Story_Container.SetEffect(f.Result, "051_story", true);
    if ((UnityEngine.Object) mypage051Menu.Event_Container != (UnityEngine.Object) null)
    {
      if (Singleton<EarthDataManager>.GetInstance().GetEnableEarthExtraQuestList().Count == 0)
      {
        mypage051Menu.Event_Container.ChangeState(false);
      }
      else
      {
        mypage051Menu.Event_Container.SetEffect(f.Result, "event", true);
        mypage051Menu.Event_Container.ChangeState(true);
      }
    }
  }

  public override IEnumerator InitSceneAsync()
  {
    Mypage051Menu mypage051Menu = this;
    IEnumerator e = mypage051Menu.InitOne();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    mypage051Menu.sm = Singleton<NGSoundManager>.GetInstance();
  }

  protected override IEnumerator InitOne()
  {
    Mypage051Menu mypage051Menu = this;
    if (!((UnityEngine.Object) mypage051Menu.CharaAnimObj != (UnityEngine.Object) null))
    {
      IEnumerator e = mypage051Menu.CreateCharcterAnimation();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      e = mypage051Menu.CreateJogDial();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
  }

  protected override IEnumerator CharcterAnimationSetting(bool isCloudAnim, int tweenID)
  {
    Mypage051Menu mypage051Menu = this;
    PlayerUnit[] array = ((IEnumerable<PlayerUnit>) SMManager.Get<PlayerUnit[]>()).Where<PlayerUnit>((Func<PlayerUnit, bool>) (x => !x.unit.IsMaterialUnit)).ToArray<PlayerUnit>();
    if (array.Length == 0)
    {
      MypageMenuBase.LeaderID = 0;
      if (isCloudAnim)
      {
        Singleton<CommonRoot>.GetInstance().StartBGTween(tweenID);
        mypage051Menu.AddStartTweenDelay(0.0f, MypageMenuBase.START_TWEENGROUP_TOP);
        mypage051Menu.AddStartTweenDelay(0.0f, MypageMenuBase.START_TWEEN_GROUP_JOGDIAL);
      }
      else
        mypage051Menu.AddStartTweenDelay(0.0f, MypageMenuBase.START_TWEENGROUP);
    }
    else
    {
      int index = UnityEngine.Random.Range(0, array.Length);
      PlayerUnit unit = array[index];
      int id = unit.unit.ID;
      Future<UnityEngine.Sprite> LeaderF = unit.unit.LoadSpriteLarge(1f);
      IEnumerator e = LeaderF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      MypageMenuBase.playTouchVoiceList = mypage051Menu.baseVoiceIDList.Shuffle<int>().ToList<int>();
      mypage051Menu.nextPlayIndex = 0;
      ((IEnumerable<UI2DSprite>) mypage051Menu.CharaSprites).ForEach<UI2DSprite>((System.Action<UI2DSprite>) (x =>
      {
        UIButton uiButton = (UIButton) null;
        if ((UnityEngine.Object) x != (UnityEngine.Object) null)
        {
          x.sprite2D = LeaderF.Result;
          uiButton = x.gameObject.GetComponent<UIButton>();
        }
        if (!((UnityEngine.Object) uiButton != (UnityEngine.Object) null))
          return;
        EventDelegate.Set(uiButton.onClick, (EventDelegate.Callback) (() => this.CharaTouchVoice()));
      }));
      ((IEnumerable<UILabel>) mypage051Menu.CharaName).ForEach<UILabel>((System.Action<UILabel>) (x => x.SetTextLocalize(unit.unit.name)));
      float addDelayTime = mypage051Menu.SetCharacterAnimControllerWithGetAddDelayTime(false, id);
      if (isCloudAnim)
      {
        Singleton<CommonRoot>.GetInstance().StartBGTween(tweenID);
        mypage051Menu.AddStartTweenDelay(addDelayTime, MypageMenuBase.START_TWEENGROUP_TOP);
        mypage051Menu.AddStartTweenDelay(addDelayTime, MypageMenuBase.START_TWEEN_GROUP_JOGDIAL);
      }
      else
        mypage051Menu.AddStartTweenDelay(addDelayTime, MypageMenuBase.START_TWEENGROUP);
    }
  }

  public override IEnumerator StartSceneAsync(
    bool isCloudAnim,
    bool isReservedEventScript)
  {
    Mypage051Menu mypage051Menu = this;
    mypage051Menu.CharaAnimation = false;
    mypage051Menu.ButtonCount = 0;
    mypage051Menu.ButtonTweenFinishedCount = 0;
    mypage051Menu.TweenInit();
    mypage051Menu.JogDialSetting();
    IEnumerator e = mypage051Menu.CharcterAnimationSetting(isCloudAnim, MypageMenuBase.EARTH_IN_BG_TWEENGROUP);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  protected override void JogDialSetting()
  {
    this.CirculButton.Init(this.Story_Container);
    this.Story_Container.ChangeState(true);
    if (!Singleton<EarthDataManager>.GetInstance().questProgress.isCleared)
      return;
    this.Story_Container.ChangeState(false);
  }

  private IEnumerator ShowBattleRestartPopup()
  {
    Future<GameObject> prefabF = Res.Prefabs.popup.popup_050_20__anim_popup01.Load<GameObject>();
    IEnumerator e = prefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Singleton<PopupManager>.GetInstance().open(prefabF.Result.Clone((Transform) null), false, false, true, true, false, false, "SE_1006");
  }

  protected override void AllCircleTweenFinished()
  {
    base.AllCircleTweenFinished();
    if (!Persist.earthBattleEnvironment.Exists)
      return;
    this.StartCoroutine(this.ShowBattleRestartPopup());
  }

  public override IEnumerator CloudAnimationStart()
  {
    Mypage051Menu mypage051Menu = this;
    Singleton<CommonRoot>.GetInstance().StartBGTween(MypageMenuBase.EARTH_OUT_BG_TWEENGROUP);
    mypage051Menu.PlayTween(MypageMenuBase.END_TWEENGROUP_TOP);
    IEnumerator e = Singleton<CommonRoot>.GetInstance().StartCloudAnim(MypageCloudAnim.EarthOut, MypageCloudAnim.HeavenIn, (System.Action) (() =>
    {
      Singleton<CommonRoot>.GetInstance().isTouchBlock = false;
      Singleton<NGSceneManager>.GetInstance().destroyLoadedScenes();
      Singleton<NGSceneManager>.GetInstance().clearStack();
      EarthDataManager.DestoryInstance();
      SMManager.Swap();
      MasterDataCache.SetGameMode(MasterDataCache.GameMode.HEAVEN);
      MasterDataCache.GetList<int, UnitUnit>("UnitUnit", new Func<MasterDataReader, UnitUnit>(UnitUnit.Parse), (Func<UnitUnit, int>) (x => x.ID));
      MasterDataCache.GetList<int, UnitVoicePattern>("UnitVoicePattern", new Func<MasterDataReader, UnitVoicePattern>(UnitVoicePattern.Parse), (Func<UnitVoicePattern, int>) (x => x.ID));
      MasterDataCache.GetList<int, TipsTips>("TipsTips", new Func<MasterDataReader, TipsTips>(TipsTips.Parse), (Func<TipsTips, int>) (x => x.ID));
      MasterDataCache.GetList<int, UnitCharacter>("UnitCharacter", new Func<MasterDataReader, UnitCharacter>(UnitCharacter.Parse), (Func<UnitCharacter, int>) (x => x.ID));
      MasterDataCache.GetList<int, QuestStoryS>("QuestStoryS", new Func<MasterDataReader, QuestStoryS>(QuestStoryS.Parse), (Func<QuestStoryS, int>) (x => x.ID));
      MasterDataCache.GetList<int, QuestStoryL>("QuestStoryL", new Func<MasterDataReader, QuestStoryL>(QuestStoryL.Parse), (Func<QuestStoryL, int>) (x => x.ID));
      GC.Collect();
      GC.WaitForPendingFinalizers();
      Singleton<NGGameDataManager>.GetInstance().StartSceneAsyncProxy((System.Action<NGGameDataManager.StartSceneProxyResult>) null);
      MypageScene.ChangeScene(false, true, false);
    }));
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public void SetFirstTweenDelegate()
  {
    this.AddStartTweenDelay(0.0f, MypageMenuBase.START_TWEEN_GROUP_JOGDIAL);
  }
}
