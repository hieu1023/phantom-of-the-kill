﻿// Decompiled with JetBrains decompiler
// Type: Popup05021Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

public class Popup05021Menu : BackButtonMenuBase
{
  public override void onBackButton()
  {
    this.IbtnNo();
  }

  public void IbtnNo()
  {
    Singleton<PopupManager>.GetInstance().onDismiss();
  }

  public void IbtnYes()
  {
    Singleton<PopupManager>.GetInstance().closeAll(false);
    Persist.earthBattleEnvironment.Delete();
  }
}
