﻿// Decompiled with JetBrains decompiler
// Type: StatusReady
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class StatusReady
{
  [SerializeField]
  public List<SpriteNumberSelectDirect> slc_Remain_hours;
  [SerializeField]
  public List<SpriteNumberSelectDirect> slc_Remain_minutes;
  [SerializeField]
  private NGTweenGaugeFillAmount slc_enemy_pvp_HP_Gauge;
  [SerializeField]
  private NGTweenGaugeFillAmount slc_player_pvp_HP_Gauge;
  [SerializeField]
  private GuildStatus myStatus;
  [SerializeField]
  private GuildStatus enStatus;

  public GuildStatus MyStatus
  {
    get
    {
      return this.myStatus;
    }
    set
    {
      this.myStatus = value;
    }
  }

  public GuildStatus EnStatus
  {
    get
    {
      return this.enStatus;
    }
    set
    {
      this.enStatus = value;
    }
  }

  public IEnumerator ResourceLoad(GuildRegistration myData, GuildRegistration enData)
  {
    IEnumerator e = this.MyStatus.ResourceLoad(myData);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    e = this.EnStatus.ResourceLoad(enData);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public void SetStatus(GuildRegistration myData, GuildRegistration enData)
  {
    this.MyStatus.SetStatus(myData);
    this.EnStatus.SetStatus(enData);
  }

  public void UpdateStatus(GuildRegistration myData, GuildRegistration enData)
  {
    this.MyStatus.UpdateStatus(myData);
    this.EnStatus.UpdateStatus(enData);
  }
}
