﻿// Decompiled with JetBrains decompiler
// Type: TutorialFakeButtonPositionController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class TutorialFakeButtonPositionController : MonoBehaviour
{
  public string targetName;
  [Tooltip("=0: use the gameobject with targetName as target gameobject\n, >0: use the parent of the gameobject with targetName as target gameobject")]
  public int parentLayer;
  private Transform targetTransform;

  private void Awake()
  {
    this.UpdatePosition();
  }

  private void Start()
  {
    this.UpdatePosition();
  }

  private void Update()
  {
    this.UpdatePosition();
  }

  public void UpdatePosition()
  {
    if ((Object) this.targetTransform == (Object) null)
    {
      GameObject gameObject = GameObject.Find(this.targetName);
      if ((Object) gameObject != (Object) null)
      {
        this.targetTransform = gameObject.transform;
        for (int index = 0; index < this.parentLayer; ++index)
        {
          this.targetTransform = this.targetTransform.parent;
          if ((Object) this.targetTransform == (Object) null)
          {
            Debug.LogError((object) "The specified target gameobject does not exist!");
            return;
          }
        }
      }
      else
      {
        Debug.LogError((object) "The specified target gameobject does not exist!");
        return;
      }
    }
    if (!((Object) this.targetTransform != (Object) null))
      return;
    this.transform.position = this.targetTransform.position;
  }
}
