﻿// Decompiled with JetBrains decompiler
// Type: Startup00062Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Startup00062Menu : MonoBehaviour
{
  [SerializeField]
  protected UILabel TxtDiscription;

  public IEnumerator Start()
  {
    this.TxtDiscription.SetText(Consts.GetInstance().MAINTENANCE_WAIT_DISCRIPTION);
    IEnumerator e = this.Request(true);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public void Init(WebAPI.Response.OfficialinfoMaintenance res)
  {
    this.TxtDiscription.SetText(Consts.Format(Consts.GetInstance().MAINTENANCE_WAIT_DISCRIPTION2, (IDictionary) new Hashtable()
    {
      {
        (object) "title",
        (object) res.message_schedule
      },
      {
        (object) "message",
        (object) res.message_body
      }
    }));
    if (res.is_maintenance)
    {
      this.StartCoroutine(this.Request(false));
    }
    else
    {
      CommonRoot instance1 = Singleton<CommonRoot>.GetInstance();
      if ((UnityEngine.Object) instance1 != (UnityEngine.Object) null)
        UnityEngine.Object.Destroy((UnityEngine.Object) instance1.gameObject);
      NGSceneManager instance2 = Singleton<NGSceneManager>.GetInstance();
      if ((UnityEngine.Object) instance2 != (UnityEngine.Object) null)
        UnityEngine.Object.Destroy((UnityEngine.Object) instance2.gameObject);
      SceneManager.LoadScene("startup000_6");
    }
  }

  public IEnumerator Request(bool second = true)
  {
    if (!second)
      yield return (object) new WaitForSeconds(60f);
    Future<WebAPI.Response.OfficialinfoMaintenance> fRes = WebAPI.OfficialinfoMaintenance((System.Action<WebAPI.Response.UserError>) null);
    IEnumerator e = fRes.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.Init(fRes.Result);
  }
}
