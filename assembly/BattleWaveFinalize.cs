﻿// Decompiled with JetBrains decompiler
// Type: BattleWaveFinalize
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;

public class BattleWaveFinalize : BattleMonoBehaviour
{
  protected override IEnumerator Start_Battle()
  {
    BattleWaveFinalize battleWaveFinalize = this;
    int questType = (int) battleWaveFinalize.env.core.battleInfo.quest_type;
    int win = battleWaveFinalize.env.core.isWin ? 1 : 0;
    bool is_game_over = battleWaveFinalize.env.core.allDeadUnitsp(BL.ForceID.player);
    string battleId = battleWaveFinalize.env.core.battleInfo.battleId;
    int absoluteTurnCount = battleWaveFinalize.env.core.phaseState.absoluteTurnCount;
    int continueCount = battleWaveFinalize.env.core.continueCount;
    List<int> intList1 = new List<int>();
    List<int> intList2 = new List<int>();
    List<int> intList3 = new List<int>();
    List<int> intList4 = new List<int>();
    int weak_element_attack_count = 0;
    int weak_kind_attack_count = 0;
    foreach (BL.Unit unit in battleWaveFinalize.env.core.playerUnits.value)
    {
      if (unit.duelHistory != null)
      {
        foreach (BL.DuelHistory duelHistory in unit.duelHistory)
        {
          intList1.Add(duelHistory.inflictTotalDamage);
          intList2.Add(duelHistory.sufferTotalDamage);
          intList3.Add(duelHistory.criticalCount);
          intList4.Add(duelHistory.inflictMaxDamage);
          weak_element_attack_count += duelHistory.weekElementAttackCount;
          weak_kind_attack_count += duelHistory.weekKindAttackCount;
        }
      }
    }
    List<WebAPI.Request.BattleFinish.UnitResult> source1 = new List<WebAPI.Request.BattleFinish.UnitResult>();
    foreach (BL.Unit unit in battleWaveFinalize.env.core.playerUnits.value)
      source1.Add(new WebAPI.Request.BattleFinish.UnitResult()
      {
        player_unit_id = unit.playerUnit.id,
        total_damage = unit.attackDamage,
        total_damage_count = unit.attackCount,
        total_kill_count = unit.killCount,
        remaining_hp = unit.hp,
        rental = unit.is_helper ? 1 : (unit.index != 5 || !unit.playerUnit.is_gesut ? 0 : 1),
        received_damage = unit.receivedDamage,
        guest = unit.playerUnit.is_gesut ? 1 : 0
      });
    List<WebAPI.Request.BattleFinish.GearResult> source2 = new List<WebAPI.Request.BattleFinish.GearResult>();
    foreach (BL.Unit unit in battleWaveFinalize.env.core.playerUnits.value)
    {
      if (!unit.is_helper && !unit.playerUnit.is_gesut && unit.playerUnit.equippedGear != (PlayerItem) null)
        source2.Add(new WebAPI.Request.BattleFinish.GearResult()
        {
          player_gear_id = unit.playerUnit.equippedGear.id,
          kill_count = unit.killCount,
          damage_count = unit.attackCount
        });
    }
    List<WebAPI.Request.BattleFinish.SupplyResult> source3 = new List<WebAPI.Request.BattleFinish.SupplyResult>();
    foreach (BL.Item obj in battleWaveFinalize.env.core.itemList.value)
    {
      int num = obj.initialAmount - obj.amount;
      if (num > 0)
        source3.Add(new WebAPI.Request.BattleFinish.SupplyResult()
        {
          supply_id = obj.itemId,
          use_quantity = num
        });
    }
    List<WebAPI.Request.BattleFinish.IntimateResult> source4 = new List<WebAPI.Request.BattleFinish.IntimateResult>();
    foreach (Tuple<int, int, int> tuple in battleWaveFinalize.env.core.getPlayerIntimateResult())
      source4.Add(new WebAPI.Request.BattleFinish.IntimateResult()
      {
        character_id = tuple.Item1,
        target_character_id = tuple.Item2,
        exp = tuple.Item3
      });
    List<BattleWaveFinishInfo> battleWaveFinishInfoList = new List<BattleWaveFinishInfo>();
    List<Tuple<BL.DropData, int>> dropDatas = new List<Tuple<BL.DropData, int>>();
    for (int row = 0; row < battleWaveFinalize.env.core.battleInfo.stage.map_height; ++row)
    {
      for (int column = 0; column < battleWaveFinalize.env.core.battleInfo.stage.map_width; ++column)
      {
        BL.Panel fieldPanel = battleWaveFinalize.env.core.getFieldPanel(row, column);
        if (fieldPanel.fieldEvent != null && fieldPanel.fieldEvent.isCompleted)
          dropDatas.Add(new Tuple<BL.DropData, int>(fieldPanel.fieldEvent, fieldPanel.fieldEventId));
      }
    }
    battleWaveFinishInfoList.Add(battleWaveFinalize.CreateBattleFinishInfo(battleWaveFinalize.env.core.stage.id, battleWaveFinalize.env.core.enemyUnits.value, dropDatas));
    if (battleWaveFinalize.env.core.currentWave > 0)
    {
      for (--battleWaveFinalize.env.core.currentWave; battleWaveFinalize.env.core.currentWave >= 0; --battleWaveFinalize.env.core.currentWave)
        battleWaveFinishInfoList.Add(battleWaveFinalize.CreateBattleFinishInfo(battleWaveFinalize.env.core.stage.id, battleWaveFinalize.env.waveEnemiesStack.Pop(), battleWaveFinalize.env.waveDropStack.Pop()));
    }
    Future<WebAPI.Response.BattleWaveFinish> f = WebAPI.BattleWaveFinish(absoluteTurnCount, battleId, continueCount, intList3.ToArray(), intList1.ToArray(), intList2.ToArray(), intList4.ToArray(), source2.Select<WebAPI.Request.BattleFinish.GearResult, int>((Func<WebAPI.Request.BattleFinish.GearResult, int>) (x => x.damage_count)).ToArray<int>(), source2.Select<WebAPI.Request.BattleFinish.GearResult, int>((Func<WebAPI.Request.BattleFinish.GearResult, int>) (x => x.kill_count)).ToArray<int>(), source2.Select<WebAPI.Request.BattleFinish.GearResult, int>((Func<WebAPI.Request.BattleFinish.GearResult, int>) (x => x.player_gear_id)).ToArray<int>(), source4.Select<WebAPI.Request.BattleFinish.IntimateResult, int>((Func<WebAPI.Request.BattleFinish.IntimateResult, int>) (x => x.target_character_id)).ToArray<int>(), source4.Select<WebAPI.Request.BattleFinish.IntimateResult, int>((Func<WebAPI.Request.BattleFinish.IntimateResult, int>) (x => x.exp)).ToArray<int>(), source4.Select<WebAPI.Request.BattleFinish.IntimateResult, int>((Func<WebAPI.Request.BattleFinish.IntimateResult, int>) (x => x.character_id)).ToArray<int>(), is_game_over, source3.Select<WebAPI.Request.BattleFinish.SupplyResult, int>((Func<WebAPI.Request.BattleFinish.SupplyResult, int>) (x => x.supply_id)).ToArray<int>(), source3.Select<WebAPI.Request.BattleFinish.SupplyResult, int>((Func<WebAPI.Request.BattleFinish.SupplyResult, int>) (x => x.use_quantity)).ToArray<int>(), source1.Select<WebAPI.Request.BattleFinish.UnitResult, int>((Func<WebAPI.Request.BattleFinish.UnitResult, int>) (x => x.player_unit_id)).ToArray<int>(), source1.Select<WebAPI.Request.BattleFinish.UnitResult, int>((Func<WebAPI.Request.BattleFinish.UnitResult, int>) (x => x.received_damage)).ToArray<int>(), source1.Select<WebAPI.Request.BattleFinish.UnitResult, int>((Func<WebAPI.Request.BattleFinish.UnitResult, int>) (x => x.remaining_hp)).ToArray<int>(), source1.Select<WebAPI.Request.BattleFinish.UnitResult, int>((Func<WebAPI.Request.BattleFinish.UnitResult, int>) (x => x.rental)).ToArray<int>(), source1.Select<WebAPI.Request.BattleFinish.UnitResult, int>((Func<WebAPI.Request.BattleFinish.UnitResult, int>) (x => x.total_damage)).ToArray<int>(), source1.Select<WebAPI.Request.BattleFinish.UnitResult, int>((Func<WebAPI.Request.BattleFinish.UnitResult, int>) (x => x.total_damage_count)).ToArray<int>(), source1.Select<WebAPI.Request.BattleFinish.UnitResult, int>((Func<WebAPI.Request.BattleFinish.UnitResult, int>) (x => x.total_kill_count)).ToArray<int>(), battleWaveFinishInfoList.ToArray(), weak_element_attack_count, weak_kind_attack_count, win, source1.Select<WebAPI.Request.BattleFinish.UnitResult, int>((Func<WebAPI.Request.BattleFinish.UnitResult, int>) (x => x.guest)).ToArray<int>(), (System.Action<WebAPI.Response.UserError>) null);
    IEnumerator e = f.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if (f.Result != null)
    {
      Singleton<NGGameDataManager>.GetInstance().refreshHomeHome = true;
      BattleUI05Scene.ChangeScene(battleWaveFinalize.env.core.battleInfo, battleWaveFinalize.env.core.isWin, f.Result);
      EventTracker.TrackEvent("BATTLE", "CONTINUE", battleWaveFinalize.env.core.continueCount);
    }
  }

  private BattleWaveFinishInfo CreateBattleFinishInfo(
    int stage_id,
    List<BL.Unit> enemys,
    List<Tuple<BL.DropData, int>> dropDatas)
  {
    BattleWaveFinishInfo battleWaveFinishInfo = new BattleWaveFinishInfo();
    battleWaveFinishInfo.stage_id = stage_id;
    List<int> intList1 = new List<int>();
    List<int> intList2 = new List<int>();
    List<int> intList3 = new List<int>();
    List<int> intList4 = new List<int>();
    List<int> intList5 = new List<int>();
    List<int> intList6 = new List<int>();
    foreach (BL.Unit enemy in enemys)
    {
      if (enemy.hasDrop && enemy.drop.isCompleted)
        intList1.Add(enemy.playerUnit.id);
      intList2.Add(enemy.playerUnit.id);
      intList3.Add(enemy.hp <= 0 ? 1 : 0);
      intList4.Add(enemy.killCount);
      if (enemy.killedBy != (BL.Unit) null)
      {
        intList5.Add(enemy.playerUnit.level - enemy.killedBy.playerUnit.level);
        intList6.Add(enemy.overkillDamage);
      }
      else
      {
        intList5.Add(0);
        intList6.Add(0);
      }
    }
    battleWaveFinishInfo.drop_entity_ids = intList1.ToArray();
    battleWaveFinishInfo.enemy_results_enemy_id = intList2.ToArray();
    battleWaveFinishInfo.enemy_results_dead_count = intList3.ToArray();
    battleWaveFinishInfo.enemy_results_kill_count = intList4.ToArray();
    battleWaveFinishInfo.enemy_results_level_difference = intList5.ToArray();
    battleWaveFinishInfo.enemy_results_overkill_damage = intList6.ToArray();
    List<int> intList7 = new List<int>();
    foreach (Tuple<BL.DropData, int> dropData in dropDatas)
      intList7.Add(dropData.Item2);
    battleWaveFinishInfo.panel_entity_ids = intList7.ToArray();
    return battleWaveFinishInfo;
  }
}
