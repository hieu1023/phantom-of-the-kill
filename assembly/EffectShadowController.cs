﻿// Decompiled with JetBrains decompiler
// Type: EffectShadowController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class EffectShadowController : MonoBehaviour
{
  private Transform mRoot;

  public void SetTransfome(Transform root)
  {
    this.mRoot = root;
  }

  private void Update()
  {
    if (!((Object) this.mRoot != (Object) null))
      return;
    this.transform.localPosition = new Vector3(this.mRoot.localPosition.x, 0.0f, this.mRoot.localPosition.z);
  }
}
