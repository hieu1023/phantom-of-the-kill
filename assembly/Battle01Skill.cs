﻿// Decompiled with JetBrains decompiler
// Type: Battle01Skill
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using System;
using System.Collections;
using UnityEngine;

public class Battle01Skill : NGBattleMenuBase
{
  [SerializeField]
  private Battle01Skill.Container[] containers_;
  private BL.BattleModified<BL.Skill> modified;
  private BL.BattleModified<BL.Unit> modifiedUnit;
  private BL.BattleModified<BL.UnitPosition> unitPositionModified;
  private GameObject typeIconPrefab;
  private GameObject targetIconPrefab;
  private Battle01Skill.Container current_;

  private T clonePrefab<T>(GameObject prefab, UI2DSprite parent) where T : IconPrefabBase
  {
    if ((UnityEngine.Object) parent == (UnityEngine.Object) null)
      return default (T);
    parent.enabled = false;
    T component = prefab.CloneAndGetComponent<T>(parent.transform);
    if ((UnityEngine.Object) component == (UnityEngine.Object) null)
      return default (T);
    NGUITools.AdjustDepth(component.gameObject, parent.depth);
    return component;
  }

  protected override IEnumerator Start_Original()
  {
    Future<GameObject> typeIconPrefabF = Res.Prefabs.BattleSkillIcon._battleSkillIcon.Load<GameObject>();
    IEnumerator e = typeIconPrefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.typeIconPrefab = typeIconPrefabF.Result;
    Future<GameObject> targetIconPrefabF = Res.Icons.SkillGenreIcon.Load<GameObject>();
    e = targetIconPrefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.targetIconPrefab = targetIconPrefabF.Result;
    foreach (Battle01Skill.Container container in this.containers_)
      container.dirTop.SetActive(false);
    this.changeContainer(Battle01Skill.Mode.Normal);
  }

  protected override void LateUpdate_Battle()
  {
    bool flag1 = this.unitPositionModified != null && this.unitPositionModified.isChangedOnce();
    bool flag2 = this.modified != null && this.modifiedUnit != null && this.modified.isChangedOnce() | this.modifiedUnit.isChangedOnce();
    if (!(flag2 | flag1))
      return;
    BL.Skill skill = this.modified.value;
    this.changeContainer(skill.skill.IsJobAbility ? Battle01Skill.Mode.JobAbility : Battle01Skill.Mode.Normal);
    this.initializeContainer(this.current_);
    if (flag2)
    {
      if ((UnityEngine.Object) this.current_.skillIcon != (UnityEngine.Object) null)
        this.StartCoroutine(this.current_.skillIcon.Init(skill.skill));
      this.current_.property01Icon.Init(skill.genre1);
      this.current_.property02Icon.Init(skill.genre2);
      this.setText(this.current_.txt_name, skill.name);
      this.setText(this.current_.txt_remain, "×" + (object) skill.remain);
    }
    bool flag3 = false;
    int? remain = skill.remain;
    if (remain.HasValue)
    {
      remain = skill.remain;
      int num = 0;
      flag3 = remain.GetValueOrDefault() <= num & remain.HasValue;
    }
    if (!flag3 && skill.skill.target_type == BattleskillTargetType.myself)
      flag3 = this.modifiedUnit.value.skillEffects.CanUseSkill(skill.skill, skill.level, (BL.ISkillEffectListUnit) this.modifiedUnit.value, this.env.core, (BL.ISkillEffectListUnit) this.modifiedUnit.value) == 1;
    if (!flag3)
      flag3 = this.modifiedUnit.value.IsDontUseCommand(skill.id);
    this.current_.dir_Disable.SetActive(flag3);
  }

  private void changeContainer(Battle01Skill.Mode mode)
  {
    int index = (int) mode;
    Battle01Skill.Container container = this.containers_.Length > index ? this.containers_[index] : this.containers_[0];
    if (this.current_ == container)
      return;
    if (this.current_ != null)
      this.current_.dirTop.SetActive(false);
    container.dirTop.SetActive(true);
    this.current_ = container;
  }

  private void initializeContainer(Battle01Skill.Container container)
  {
    if (container.isInitialized)
      return;
    container.isInitialized = true;
    container.skillIcon = this.clonePrefab<BattleSkillIcon>(this.typeIconPrefab, container.skill);
    container.property01Icon = this.clonePrefab<SkillGenreIcon>(this.targetIconPrefab, container.property01);
    container.property02Icon = this.clonePrefab<SkillGenreIcon>(this.targetIconPrefab, container.property02);
    this.clonePrefab<SkillGenreIcon>(this.targetIconPrefab, container.property03);
  }

  public void setSkill(BL.Skill skill, BL.Unit unit)
  {
    this.modified = BL.Observe<BL.Skill>(skill);
    this.modifiedUnit = BL.Observe<BL.Unit>(unit);
    this.unitPositionModified = BL.Observe<BL.UnitPosition>(this.env.core.getUnitPosition(unit));
  }

  public BL.Skill getSkill()
  {
    return this.modified.value;
  }

  public BL.Unit getUnit()
  {
    return this.modifiedUnit.value;
  }

  [Serializable]
  private class Container
  {
    public GameObject dirTop;
    public UI2DSprite skill;
    public UI2DSprite property01;
    public UI2DSprite property02;
    public UI2DSprite property03;
    public UILabel txt_name;
    public UILabel txt_remain;
    public GameObject dir_Disable;
    [NonSerialized]
    public bool isInitialized;
    [NonSerialized]
    public BattleSkillIcon skillIcon;
    [NonSerialized]
    public SkillGenreIcon property01Icon;
    [NonSerialized]
    public SkillGenreIcon property02Icon;
  }

  public enum Mode
  {
    Normal,
    JobAbility,
  }
}
