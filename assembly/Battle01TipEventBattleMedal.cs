﻿// Decompiled with JetBrains decompiler
// Type: Battle01TipEventBattleMedal
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Battle01TipEventBattleMedal : Battle01TipEventBase
{
  public override IEnumerator onInitAsync()
  {
    Battle01TipEventBattleMedal eventBattleMedal = this;
    Future<GameObject> f = Res.Icons.UniqueIconPrefab.Load<GameObject>();
    IEnumerator e = f.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    UniqueIcons icon = eventBattleMedal.cloneIcon<UniqueIcons>(f.Result, 0);
    e = icon.SetBattleMedal(0);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    icon.BackGroundActivated = false;
    icon.LabelActivated = false;
    eventBattleMedal.selectIcon(0);
  }

  public override void setData(BL.DropData e, BL.Unit unit)
  {
    if (e.reward.Type != MasterDataTable.CommonRewardType.battle_medal)
      return;
    Dictionary<string, int> dictionary = new Dictionary<string, int>()
    {
      {
        "medal",
        e.reward.Quantity
      }
    };
    if (e.reward.Id != 0)
      this.setText(Consts.Format(Consts.GetInstance().TipEvent_text_limited_battle_medal, (IDictionary) dictionary));
    else
      this.setText(Consts.Format(Consts.GetInstance().TipEvent_text_battle_medal, (IDictionary) dictionary));
  }
}
