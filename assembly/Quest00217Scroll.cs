﻿// Decompiled with JetBrains decompiler
// Type: Quest00217Scroll
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Quest00217Scroll : BannerBase
{
  protected bool inside_ = true;
  [SerializeField]
  public UISprite Clear;
  [SerializeField]
  public UISprite New;
  [SerializeField]
  protected FloatButton Button;
  [SerializeField]
  protected GameObject Highlighting;
  [SerializeField]
  protected UIUnityMaskRenderer EffectRenderer;
  [SerializeField]
  [Tooltip("長押し説明が有る時にON")]
  protected GameObject objHasDescriptions;
  [SerializeField]
  private GameObject LevelLimit;
  [SerializeField]
  private UILabel LevelLimitText;
  [SerializeField]
  private GameObject dummyCollider;
  protected bool initialized;
  protected Quest00217Scroll.Parameter initData;
  protected CampaignQuest.RankingEventTerm rankingEventTerm;
  protected bool enabledCountdown;
  protected bool effect_;
  protected GameObject objEffect_;
  protected DateTime timeGoal;
  protected int lastSeconds;
  private DateTime lastServerTime;
  private float lastLocalTime;
  protected string loadPath;
  protected bool loadEnabledHighlighting;

  public CampaignQuest.RankingEventTerm RankingEventTerm
  {
    get
    {
      return this.rankingEventTerm;
    }
  }

  public virtual IEnumerator InitScroll(
    Quest00217Scroll.Parameter param,
    DateTime serverTime)
  {
    Quest00217Scroll quest00217Scroll = this;
    quest00217Scroll.Setup(param, serverTime);
    IEnumerator e = quest00217Scroll.SetAndCreate_BannerSprite(quest00217Scroll.loadPath, quest00217Scroll.IdleSprite, quest00217Scroll.Highlighting, quest00217Scroll.loadEnabledHighlighting);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public void Setup(Quest00217Scroll.Parameter param, DateTime serverTime)
  {
    this.initialized = false;
    this.initData = param;
    this.inside_ = this.gameObject.activeSelf;
    this.rankingEventTerm = CampaignQuest.RankingEventTerm.normal;
    this.SetEndTime(param.extra.today_day_end_at);
    this.resetLocalTime(serverTime);
    this.Button.enabled = true;
    bool flag1 = param.isNew;
    bool flag2 = param.isClear;
    bool flag3 = param.isHighlighting;
    if (param.isNotice && param.startTime.HasValue && param.startTime.Value > serverTime)
    {
      flag2 = false;
      flag1 = false;
      flag3 = false;
      this.Button.enabled = false;
      this.SetEndTimeAsStart(param.startTime.Value);
      this.startCountdown(param.startTime.Value - serverTime);
    }
    QuestExtra.SeekType seekType = param.seek != QuestExtra.SeekType.None ? param.seek : QuestExtra.toSeekType(param.extra.seek_index);
    QuestExtraS questExtraS = param.extra.quest_extra_s;
    this.loadPath = this.SetSpritePath(questExtraS.quest_l_QuestExtraL, questExtraS.quest_m_QuestExtraM, seekType, questExtraS.quest_ll?.ID);
    this.loadEnabledHighlighting = flag3;
    this.SetScrollButtonCondition(param.extra, serverTime, seekType);
    this.Clear.gameObject.SetActive(flag2);
    QuestScoreCampaignProgress campaignProgress = ((IEnumerable<QuestScoreCampaignProgress>) SMManager.Get<QuestScoreCampaignProgress[]>()).FirstOrDefault<QuestScoreCampaignProgress>((Func<QuestScoreCampaignProgress, bool>) (x => x.quest_extra_l == param.extra.quest_extra_s.quest_l.ID));
    if (campaignProgress != null)
    {
      if (campaignProgress.is_open && serverTime < campaignProgress.end_at)
      {
        this.EndTime = campaignProgress.end_at;
        this.rankingEventTerm = CampaignQuest.RankingEventTerm.normal;
      }
      else if (!campaignProgress.is_open && serverTime < campaignProgress.final_at)
      {
        this.EndTime = campaignProgress.final_at;
        this.rankingEventTerm = CampaignQuest.RankingEventTerm.aggregate;
        this.Button.enabled = false;
      }
      else if (!campaignProgress.is_open && serverTime < campaignProgress.latest_end_at)
      {
        this.EndTime = campaignProgress.latest_end_at;
        this.rankingEventTerm = CampaignQuest.RankingEventTerm.receive;
      }
    }
    this.SetTime(serverTime, this.rankingEventTerm);
    this.LevelLimit.SetActive(false);
    this.dummyCollider.SetActive(false);
    QuestExtraM m = param.extra.quest_extra_s.quest_m;
    QuestExtraReleaseConditionsPlayer conditionsPlayer = ((IEnumerable<QuestExtraReleaseConditionsPlayer>) MasterData.QuestExtraReleaseConditionsPlayerList).FirstOrDefault<QuestExtraReleaseConditionsPlayer>((Func<QuestExtraReleaseConditionsPlayer, bool>) (x => x.quest_m == m));
    if (conditionsPlayer != null && conditionsPlayer.comparison_operator == ">=")
    {
      int level = SMManager.Observe<Player>().Value.level;
      int? playerLevel = conditionsPlayer.player_level;
      int valueOrDefault = playerLevel.GetValueOrDefault();
      if (level < valueOrDefault & playerLevel.HasValue)
      {
        this.LevelLimit.SetActive(true);
        this.LevelLimitText.text = conditionsPlayer.player_level.ToString();
        flag1 = false;
        this.isConditonEffective = false;
        this.BtnFormation.isEnabled = false;
        this.dummyCollider.SetActive(true);
      }
    }
    if ((UnityEngine.Object) this.Highlighting != (UnityEngine.Object) null)
      this.Highlighting.SetActive(false);
    this.New.gameObject.SetActive(flag1);
    this.setActiveHasDescriptions(false);
    this.initialized = true;
  }

  protected string SetSpritePath(int L, int M, QuestExtra.SeekType seek_type, int? LL)
  {
    int id;
    switch (seek_type)
    {
      case QuestExtra.SeekType.M:
        id = M;
        break;
      case QuestExtra.SeekType.LL:
        id = LL.Value;
        break;
      default:
        id = L;
        break;
    }
    int num = (int) seek_type;
    return BannerBase.GetSpriteIdlePath(id, BannerBase.Type.quest, (QuestExtra.SeekType) num, true, false);
  }

  public IEnumerator SetAndCreate_BannerSprite()
  {
    // ISSUE: reference to a compiler-generated field
    int num = this.\u003C\u003E1__state;
    Quest00217Scroll quest00217Scroll = this;
    if (num != 0)
    {
      if (num != 1)
        return false;
      // ISSUE: reference to a compiler-generated field
      this.\u003C\u003E1__state = -1;
      return false;
    }
    // ISSUE: reference to a compiler-generated field
    this.\u003C\u003E1__state = -1;
    // ISSUE: reference to a compiler-generated field
    this.\u003C\u003E2__current = (object) quest00217Scroll.SetAndCreate_BannerSprite(quest00217Scroll.loadPath, quest00217Scroll.IdleSprite, quest00217Scroll.Highlighting, quest00217Scroll.loadEnabledHighlighting);
    // ISSUE: reference to a compiler-generated field
    this.\u003C\u003E1__state = 1;
    return true;
  }

  private IEnumerator SetAndCreate_BannerSprite(
    string path,
    UI2DSprite obj,
    GameObject highlighting,
    bool enabledHighlighting)
  {
    if (!Singleton<ResourceManager>.GetInstance().Contains(path))
      path = string.Format("Prefabs/Banners/ExtraQuest/M/1/Specialquest_idle", (object[]) Array.Empty<object>());
    Future<Texture2D> future = Singleton<ResourceManager>.GetInstance().Load<Texture2D>(path, 1f);
    IEnumerator e = future.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Texture2D result = future.Result;
    if (!((UnityEngine.Object) result == (UnityEngine.Object) null))
    {
      UnityEngine.Sprite sprite = UnityEngine.Sprite.Create(result, new Rect(0.0f, 0.0f, (float) result.width, (float) result.height), new Vector2(0.5f, 0.5f), 1f, 100U, SpriteMeshType.FullRect);
      sprite.name = result.name;
      obj.sprite2D = sprite;
      if ((UnityEngine.Object) highlighting != (UnityEngine.Object) null)
      {
        this.EffectRenderer.SetTexture("_MaskTex", obj.mainTexture);
        this.Highlighting.SetActive(enabledHighlighting);
      }
    }
  }

  protected void SetScrollButtonCondition(
    PlayerExtraQuestS extra,
    DateTime serverTime,
    QuestExtra.SeekType seekType)
  {
    EventDelegate.Set(this.BtnFormation.onClick, (EventDelegate.Callback) (() => this.changeScene(extra, this.gameObject, serverTime, seekType)));
  }

  public void changeScene(
    PlayerExtraQuestS extra,
    GameObject obj,
    DateTime serverTime,
    QuestExtra.SeekType seekType)
  {
    this.StartCoroutine(this.QuestTimeCompare(extra, obj, serverTime, seekType));
  }

  public IEnumerator QuestTimeCompare(
    PlayerExtraQuestS StageData,
    GameObject obj,
    DateTime serverTime,
    QuestExtra.SeekType seekType)
  {
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    bool isEnable = false;
    IEnumerator e = ServerTime.WaitSync();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    serverTime = ServerTime.NowAppTimeAddDelta();
    this.resetLocalTime(serverTime);
    if (serverTime < StageData.today_day_end_at)
      isEnable = true;
    if (isEnable)
    {
      switch (seekType)
      {
        case QuestExtra.SeekType.L:
          if (Array.Find<QuestScoreCampaignProgress>(SMManager.Get<QuestScoreCampaignProgress[]>(), (Predicate<QuestScoreCampaignProgress>) (x => x.quest_extra_l == StageData.quest_extra_s.quest_l.ID)) != null)
          {
            Quest00226Scene.ChangeScene(StageData._quest_extra_s, true);
            break;
          }
          Quest00219Scene.ChangeScene(StageData._quest_extra_s, true);
          break;
        case QuestExtra.SeekType.LL:
          Quest00218Scene.changeScene(StageData.quest_ll.ID, new int?(), true);
          break;
        default:
          QuestExtraS questExtraS = StageData.quest_extra_s;
          Quest00220Scene.ChangeScene00220(false, questExtraS.quest_l_QuestExtraL, questExtraS.quest_m_QuestExtraM, true, false, false);
          break;
      }
    }
    else
    {
      Singleton<CommonRoot>.GetInstance().isLoading = false;
      Future<GameObject> time_popup = Res.Prefabs.popup.popup_002_23__anim_popup01.Load<GameObject>();
      e = time_popup.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      Singleton<PopupManager>.GetInstance().openAlert(time_popup.Result, false, false, (EventDelegate) null, false, true, false, true);
      time_popup = (Future<GameObject>) null;
    }
  }

  protected void startCountdown(TimeSpan tspan)
  {
    this.timeGoal = DateTime.Now.Add(tspan);
    this.lastSeconds = tspan.Seconds - 1;
    this.enabledCountdown = true;
  }

  protected void resetLocalTime(DateTime serverTime)
  {
    this.lastServerTime = serverTime;
    this.lastLocalTime = Time.time;
  }

  protected DateTime nowLocalTime
  {
    get
    {
      return this.lastServerTime.AddSeconds((double) Time.time - (double) this.lastLocalTime);
    }
  }

  private void Update()
  {
    this.updateCountdown(false);
  }

  protected virtual void updateCountdown(bool immediate)
  {
    if (!this.enabledCountdown)
      return;
    TimeSpan tspan = this.timeGoal - DateTime.Now;
    if (tspan.Ticks <= 0L)
    {
      this.enabledCountdown = false;
      if (!immediate)
        this.duplicateEffectFadeOut(this.gameObject, this.gameObject, 0.5f);
      this.Clear.gameObject.SetActive(this.initData.isClear);
      this.New.gameObject.SetActive(this.initData.isNew);
      if ((UnityEngine.Object) this.Highlighting != (UnityEngine.Object) null)
        this.Highlighting.SetActive(this.initData.isHighlighting);
      this.SetEndTime(this.initData.extra.today_day_end_at);
      this.SetTime(this.nowLocalTime, this.rankingEventTerm);
      if (immediate)
        this.Button.enabled = true;
      else
        this.fadeIn(0.5f);
    }
    else
    {
      if (!(this.lastSeconds != tspan.Seconds | immediate))
        return;
      this.lastSeconds = tspan.Seconds;
      this.updateTime(tspan, immediate ? 0.0f : 0.5f);
    }
  }

  protected virtual GameObject duplicateEffectFadeOut(
    GameObject parentObj,
    GameObject originalObj,
    float duration)
  {
    GameObject gameObject = NGUITools.AddChild(parentObj, originalObj);
    this.objEffect_ = gameObject;
    gameObject.SetActive(true);
    UnityEngine.Object.Destroy((UnityEngine.Object) gameObject.GetComponent<Quest002171Scroll>());
    gameObject.AddComponent<Quest00217ScrollFadeOut>().init(duration, 100, new EventDelegate((EventDelegate.Callback) (() =>
    {
      UnityEngine.Object.Destroy((UnityEngine.Object) this.objEffect_);
      this.objEffect_ = (GameObject) null;
    })));
    return gameObject;
  }

  protected virtual void fadeIn(float duration)
  {
    UIWidget uiWidget = Quest00217ScrollFadeOut.setWidget(this.gameObject, 0);
    if (!((UnityEngine.Object) uiWidget != (UnityEngine.Object) null))
      return;
    uiWidget.alpha = 0.0f;
    this.effect_ = true;
    TweenAlpha ta = TweenAlpha.Begin(this.gameObject, duration, 1f);
    ta.SetOnFinished((EventDelegate.Callback) (() =>
    {
      UnityEngine.Object.Destroy((UnityEngine.Object) ta);
      UnityEngine.Object.Destroy((UnityEngine.Object) uiWidget);
      this.Button.enabled = true;
      this.effect_ = false;
    }));
  }

  private void OnEnable()
  {
    if (!this.initialized)
      return;
    if (this.enabledCountdown)
      this.updateCountdown(true);
    else
      this.SetTime(this.nowLocalTime, this.rankingEventTerm);
  }

  private void OnDisable()
  {
    if ((UnityEngine.Object) this.objEffect_ != (UnityEngine.Object) null)
    {
      UnityEngine.Object.Destroy((UnityEngine.Object) this.objEffect_);
      this.objEffect_ = (GameObject) null;
    }
    if (this.effect_)
    {
      UIWidget component1 = this.gameObject.GetComponent<UIWidget>();
      TweenAlpha component2 = this.gameObject.GetComponent<TweenAlpha>();
      component2.value = 1f;
      UnityEngine.Object.Destroy((UnityEngine.Object) component2);
      UnityEngine.Object.Destroy((UnityEngine.Object) component1);
      this.Button.enabled = true;
      this.effect_ = false;
    }
    this.terminateAnimation();
  }

  public void onInside()
  {
    this.inside_ = true;
    this.gameObject.SetActive(true);
  }

  public void onOutside()
  {
    this.inside_ = false;
    this.gameObject.SetActive(false);
  }

  public void setEffectNoDescription()
  {
    if ((UnityEngine.Object) this.Highlighting == (UnityEngine.Object) null)
      return;
    Animator component = this.Highlighting.GetComponent<Animator>();
    if ((UnityEngine.Object) component == (UnityEngine.Object) null)
      return;
    bool activeSelf = this.Highlighting.activeSelf;
    this.Highlighting.SetActive(true);
    component.Play("Banner_Effect_long_tap");
    this.StartCoroutine(this.coWaitEffectNoDescription(component, activeSelf));
  }

  private IEnumerator coWaitEffectNoDescription(Animator animator, bool activeObject)
  {
    yield return (object) new WaitForAnimation(animator, 0, 2f, 1);
    if (!activeObject)
      animator.gameObject.SetActive(false);
  }

  public void setActiveHasDescriptions(bool bActive = true)
  {
    if (!((UnityEngine.Object) this.objHasDescriptions != (UnityEngine.Object) null))
      return;
    this.objHasDescriptions.SetActive(bActive);
  }

  public class Parameter
  {
    public PlayerExtraQuestS extra;
    public EventInfo eventInfo;
    public SM.TowerPeriod towerInfo;
    public QuestExtraDescription[] descriptions;
    public bool isClear;
    public bool isNew;
    public QuestExtra.SeekType seek;
    public bool isNotice;
    public DateTime? startTime;
    public bool isHighlighting;
  }
}
