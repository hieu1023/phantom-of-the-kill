﻿// Decompiled with JetBrains decompiler
// Type: DetailJobTraining
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

public class DetailJobTraining : MonoBehaviour
{
  [SerializeField]
  private SpreadColorButton button;
  [SerializeField]
  private UILabel txtMenuName;
  [SerializeField]
  private GameObject txtUnopended;
  public UIDragScrollView dragScrollView;

  public IEnumerator Init(string menuName, bool isActive, System.Action callFunction)
  {
    this.txtMenuName.text = menuName;
    this.SetActive(isActive);
    if (isActive)
      EventDelegate.Add(this.button.onClick, (EventDelegate.Callback) (() => callFunction()));
    yield return (object) new WaitForEndOfFrame();
  }

  private void SetActive(bool active)
  {
    this.button.isEnabled = active;
    this.txtUnopended.SetActive(!active);
  }
}
