﻿// Decompiled with JetBrains decompiler
// Type: BattlePanelParts
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using UnityEngine;

public class BattlePanelParts : BattleMonoBehaviour
{
  public UnityEngine.Material[] cellMaterials;
  private BL.BattleModified<BL.Panel> modified;
  private FieldButton attackButton;
  private FieldButton healButton;
  private FieldButton loadingButton;
  private GameObject fieldEventEffect;

  private FieldButton cloneButton(GameObject prefab)
  {
    FieldButton component = prefab.CloneAndGetComponent<FieldButton>(this.transform);
    component.transform.localPosition = this.getLocalPosition();
    return component;
  }

  public void setGuardArea(GameObject prefab)
  {
    prefab.Clone(this.transform).transform.localPosition = this.getLocalPosition();
  }

  public void initButtons(GameObject attack, GameObject heal, GameObject loading)
  {
    this.attackButton = this.cloneButton(attack);
    this.attackButton.isActive = false;
    this.healButton = this.cloneButton(heal);
    this.healButton.isActive = false;
    this.loadingButton = this.cloneButton(loading);
    this.loadingButton.isActive = false;
  }

  public void setAttackButton(GameObject prefab)
  {
    if ((Object) this.attackButton == (Object) null)
      this.attackButton = this.cloneButton(prefab);
    this.attackButton.isActive = true;
    if ((Object) this.healButton != (Object) null)
      this.healButton.isActive = false;
    if (!((Object) this.loadingButton != (Object) null))
      return;
    this.loadingButton.isActive = false;
  }

  public void setHealButton(GameObject prefab)
  {
    if ((Object) this.healButton == (Object) null)
      this.healButton = this.cloneButton(prefab);
    this.healButton.isActive = true;
    if ((Object) this.attackButton != (Object) null)
      this.attackButton.isActive = false;
    if (!((Object) this.loadingButton != (Object) null))
      return;
    this.loadingButton.isActive = false;
  }

  public void setLoadingButton(GameObject prefab)
  {
    if ((Object) this.loadingButton == (Object) null)
      this.loadingButton = this.cloneButton(prefab);
    this.loadingButton.isActive = true;
    if ((Object) this.attackButton != (Object) null)
      this.attackButton.isActive = false;
    if (!((Object) this.healButton != (Object) null))
      return;
    this.healButton.isActive = false;
  }

  public void hideButton()
  {
    if ((Object) this.attackButton != (Object) null)
      this.attackButton.isActive = false;
    if ((Object) this.healButton != (Object) null)
      this.healButton.isActive = false;
    if (!((Object) this.loadingButton != (Object) null))
      return;
    this.loadingButton.isActive = false;
  }

  public void buttonDown(bool v)
  {
    if ((Object) this.attackButton != (Object) null && this.attackButton.isActive)
      this.attackButton.isDown = v;
    if ((Object) this.healButton != (Object) null && this.healButton.isActive)
      this.healButton.isDown = v;
    if (!((Object) this.loadingButton != (Object) null) || !this.loadingButton.isActive)
      return;
    this.loadingButton.isDown = v;
  }

  public void setPanel(BL.Panel panel)
  {
    this.modified = BL.Observe<BL.Panel>(panel);
    this.env.panelResource[panel].gameObject = this.gameObject;
    if (!panel.hasEvent || !((Object) this.env.dropDataResource[panel.fieldEvent].prefab != (Object) null))
      return;
    if ((Object) this.fieldEventEffect != (Object) null)
      Object.Destroy((Object) this.fieldEventEffect);
    this.fieldEventEffect = this.env.dropDataResource[panel.fieldEvent].prefab.Clone(this.transform);
    this.fieldEventEffect.transform.localPosition = this.getLocalPosition();
  }

  public Vector3 getLocalPosition()
  {
    return new Vector3(0.0f, this.GetComponent<PanelInit>().panelHeightNonScale + 0.1f, 0.0f);
  }

  public BL.Panel getPanel()
  {
    return this.modified.value;
  }

  public float getHeight()
  {
    return this.GetComponent<PanelInit>().panelHeight;
  }

  private UnityEngine.Material attributeMaterial(BL.Panel panel)
  {
    if (panel.checkAttribute(BL.PanelAttribute.test))
      return this.cellMaterials[4];
    if (panel.checkAttribute(BL.PanelAttribute.target_heal))
      return this.cellMaterials[3];
    if (panel.checkAttribute(BL.PanelAttribute.target_attack))
      return this.cellMaterials[5];
    if (panel.checkAttribute(BL.PanelAttribute.heal_range))
      return this.cellMaterials[3];
    if (panel.checkAttribute(BL.PanelAttribute.attack_range))
      return this.cellMaterials[5];
    if (panel.checkAttribute(BL.PanelAttribute.moving))
      return this.cellMaterials[7];
    if (panel.checkAttribute(BL.PanelAttribute.playermove))
      return this.cellMaterials[1];
    if (panel.checkAttribute(BL.PanelAttribute.neutralmove) || panel.checkAttribute(BL.PanelAttribute.enemymove))
      return this.cellMaterials[2];
    if (panel.checkAttribute(BL.PanelAttribute.danger))
      return this.cellMaterials[4];
    return panel.checkAttribute(BL.PanelAttribute.reserve0) ? this.cellMaterials[6] : this.cellMaterials[0];
  }

  protected override void LateUpdate_Battle()
  {
    if (!this.modified.isChangedOnce())
      return;
    BL.Panel panel = this.modified.value;
    UnityEngine.Material material = this.attributeMaterial(panel);
    Renderer component = this.GetComponent<Renderer>();
    if ((Object) component.material != (Object) material)
    {
      if ((Object) component.material != (Object) null)
        Object.Destroy((Object) component.material);
      component.material = material;
    }
    if (!((Object) this.fieldEventEffect != (Object) null) || panel.hasEvent)
      return;
    Object.Destroy((Object) this.fieldEventEffect);
    this.fieldEventEffect = (GameObject) null;
  }
}
