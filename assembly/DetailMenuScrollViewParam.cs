﻿// Decompiled with JetBrains decompiler
// Type: DetailMenuScrollViewParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnitDetails;
using UnitStatusInformation;
using UnityEngine;

public class DetailMenuScrollViewParam : DetailMenuScrollViewBase
{
  [SerializeField]
  protected GameObject unitySkillObject;
  [SerializeField]
  protected UI2DSprite unitySKill;
  [SerializeField]
  protected UIButton unitySKillBtn;
  [SerializeField]
  protected GameObject extraSkillObject;
  [SerializeField]
  protected UI2DSprite extraSKill;
  [SerializeField]
  protected UIButton extraSKillBtn;
  [SerializeField]
  protected UILabel txtDearDegree;
  [SerializeField]
  protected UILabel txtDearDegreeMax;
  [SerializeField]
  protected UILabel txtDearDegreeTitle;
  [SerializeField]
  protected GameObject slc_DearDegreeTitle;
  [SerializeField]
  protected GameObject floatingSkillDialog;
  [SerializeField]
  protected NGTweenGaugeScale lvGauge;
  [SerializeField]
  protected GameObject[] slc_LBicon_None;
  [SerializeField]
  protected GameObject[] slc_LBicon_Blue;
  private int dispStatusCurrent;
  [SerializeField]
  protected GameObject[] dir_statusNum;
  [SerializeField]
  protected GameObject[] dir_statusListText;
  [SerializeField]
  protected UILabel txt_Agility;
  [SerializeField]
  protected UILabel txt_Luck;
  [SerializeField]
  protected UILabel txt_Magic;
  [SerializeField]
  protected UILabel txt_Power;
  [SerializeField]
  protected UILabel txt_Protct;
  [SerializeField]
  protected UILabel txt_Spirit;
  [SerializeField]
  protected UILabel txt_Technique;
  [SerializeField]
  protected GameObject slc_HpMaxStar;
  [SerializeField]
  protected GameObject slc_AgilityMaxStar;
  [SerializeField]
  protected GameObject slc_LuckMaxStar;
  [SerializeField]
  protected GameObject slc_MagicMaxStar;
  [SerializeField]
  protected GameObject slc_PowerMaxStar;
  [SerializeField]
  protected GameObject slc_ProtctMaxStar;
  [SerializeField]
  protected GameObject slc_SpiritMaxStar;
  [SerializeField]
  protected GameObject slc_TechniqueMaxStar;
  [SerializeField]
  protected UILabel txt_Attack;
  [SerializeField]
  protected UILabel txt_Cost;
  [SerializeField]
  protected UILabel txt_Critical;
  [SerializeField]
  protected UILabel txt_Defense;
  [SerializeField]
  protected UILabel txt_Dexterity;
  [SerializeField]
  protected UILabel txt_Evasion;
  [SerializeField]
  protected UILabel txt_Fighting;
  [SerializeField]
  protected UILabel txt_Matk;
  [SerializeField]
  protected UILabel txt_Mdef;
  [SerializeField]
  protected UILabel txt_Movement;
  [SerializeField]
  protected GameObject dir_Hp;
  [SerializeField]
  protected UILabel txt_Lv;
  [SerializeField]
  protected UILabel txt_Lvmax;
  [SerializeField]
  protected UILabel txt_Hp;
  [SerializeField]
  private GameObject dirStatusPlus;
  [SerializeField]
  private UILabel txt_AgilityPlus;
  [SerializeField]
  private UILabel txt_LuckPlus;
  [SerializeField]
  private UILabel txt_MagicPlus;
  [SerializeField]
  private UILabel txt_PowerPlus;
  [SerializeField]
  private UILabel txt_ProtctPlus;
  [SerializeField]
  private UILabel txt_SpiritPlus;
  [SerializeField]
  private UILabel txt_TechniquePlus;
  [SerializeField]
  private UILabel txt_HpPlus;
  [SerializeField]
  protected UILabel txt_Unity;
  [SerializeField]
  protected UILabel txt_Unity_dec;
  [SerializeField]
  protected GameObject dir_prencessType;
  [SerializeField]
  protected GameObject[] slc_prencessType;
  [SerializeField]
  protected GameObject dir_maxHp;
  [SerializeField]
  protected UILabel txt_HpNow;
  [SerializeField]
  protected UILabel txt_Hpmax;
  private GameObject skillDetailPrefab;
  private PlayerUnit targetUnit;
  [SerializeField]
  protected UI2DSprite[] dyn_Weapon;
  [SerializeField]
  protected UI2DSprite[] dyn_IconRank;
  private GameObject[] weapon;
  private GearProfiencyIcon[] gearProfiencyIcon;
  [SerializeField]
  protected UIButton IbtnStatusDetail;
  [SerializeField]
  protected UIButton IbtnIntimacy;
  [SerializeField]
  protected UIButton IbtnUnitTraining;
  [SerializeField]
  protected UIButton IbtnUnitQuest;
  [SerializeField]
  private UIButton IbtnStatusChange;
  [SerializeField]
  [Tooltip("[装備品|オーバーキラーズ]の順でタブメインオブジェクト配置")]
  private GameObject[] tabObjects;
  private GameObject unityDetailPrefab;
  private GameObject stageItemPrefab;
  private System.Action popupUnityDetail;
  private DetailMenuOverkillersSlots overkillersSlots_;
  private PlayerUnit playerUnit;

  private DetailMenuOverkillersSlots overkillersSlots
  {
    get
    {
      return !((UnityEngine.Object) this.overkillersSlots_ != (UnityEngine.Object) null) ? (this.overkillersSlots_ = this.GetComponent<DetailMenuOverkillersSlots>()) : this.overkillersSlots_;
    }
  }

  private void Awake()
  {
    if (this.weapon == null)
      this.weapon = new GameObject[this.dyn_Weapon.Length];
    if (this.gearProfiencyIcon != null)
      return;
    this.gearProfiencyIcon = new GearProfiencyIcon[this.dyn_IconRank.Length];
  }

  private IEnumerator LoadSkillIcon(
    UI2DSprite sprite,
    BattleskillSkill skill,
    PlayerUnit unit,
    bool isGray)
  {
    Future<UnityEngine.Sprite> spriteF = skill.LoadBattleSkillIcon((BattleFuncs.InvestSkill) null);
    IEnumerator e = spriteF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    sprite.sprite2D = spriteF.Result;
    if (isGray)
      sprite.color = Color.gray;
    else
      sprite.color = Color.white;
  }

  public override IEnumerator initAsync(
    PlayerUnit playerUnit,
    bool limitMode,
    bool isMaterial,
    GameObject[] prefabs)
  {
    DetailMenuScrollViewParam menuScrollViewParam = this;
    menuScrollViewParam.playerUnit = playerUnit;
    menuScrollViewParam.skillDetailPrefab = prefabs[0];
    GameObject gearKindIconPrefab = prefabs[1];
    GameObject profIconPrefab = prefabs[2];
    menuScrollViewParam.unityDetailPrefab = prefabs[3];
    menuScrollViewParam.stageItemPrefab = prefabs[6];
    UnitUnit masterUnit = playerUnit.unit;
    if (menuScrollViewParam.controlFlags.IsOn(Control.OverkillersSlot))
    {
      OverkillersSlotRelease.Conditions[] releaseConditions = !masterUnit.exist_overkillers_slot || playerUnit.over_killers_player_unit_ids == null || playerUnit.over_killers_player_unit_ids.Length == 0 ? (OverkillersSlotRelease.Conditions[]) null : Singleton<NGGameDataManager>.GetInstance().getOverkillersSlotReleaseConditions(masterUnit.same_character_id);
      Control controlFlags = !menuScrollViewParam.isMine(playerUnit) || playerUnit.is_storage ? menuScrollViewParam.controlFlags.Clear(Control.OverkillersEdit) : menuScrollViewParam.controlFlags;
      yield return (object) menuScrollViewParam.overkillersSlots.initialize(prefabs[4], prefabs[5], playerUnit, releaseConditions, controlFlags);
    }
    else
      menuScrollViewParam.overkillersSlots.isHide = true;
    if ((UnityEngine.Object) menuScrollViewParam.unitySkillObject != (UnityEngine.Object) null)
    {
      OverkillersSkillRelease overkillersSkill = menuScrollViewParam.getOverkillersSkill();
      if (overkillersSkill != null)
      {
        menuScrollViewParam.unitySkillObject.SetActive(true);
        yield return (object) menuScrollViewParam.LoadSkillIcon(menuScrollViewParam.unitySKill, overkillersSkill.skill, playerUnit, (double) playerUnit.unityTotal < (double) overkillersSkill.unity_value);
      }
      else
        menuScrollViewParam.unitySkillObject.SetActive(false);
    }
    if ((UnityEngine.Object) menuScrollViewParam.extraSkillObject != (UnityEngine.Object) null)
    {
      if (masterUnit.trust_target_flag)
      {
        menuScrollViewParam.extraSkillObject.SetActive(true);
        menuScrollViewParam.slc_DearDegreeTitle.SetActive(true);
        if (masterUnit.IsSea)
          menuScrollViewParam.txtDearDegreeTitle.SetText(Consts.GetInstance().POPUP_004_LIMIT_EXTENDED_TITLE_DEAR);
        else
          menuScrollViewParam.txtDearDegreeTitle.SetText(Consts.GetInstance().POPUP_004_LIMIT_EXTENDED_TITLE_RELEVANCE);
        double num = Math.Round((double) playerUnit.trust_rate * 100.0) / 100.0;
        menuScrollViewParam.txtDearDegree.SetTextLocalize(num.ToString());
        menuScrollViewParam.txtDearDegreeMax.SetTextLocalize(Consts.Format(Consts.GetInstance().UNIT_TRUST_RATE_PERCENT3, (IDictionary) new Hashtable()
        {
          {
            (object) "trust_rate",
            (object) string.Format("{0}", (object) playerUnit.trust_max_rate)
          }
        }));
        UnitSkillAwake[] awakeSkills = playerUnit.GetAwakeSkills();
        if (awakeSkills == null || awakeSkills.Length == 0)
        {
          menuScrollViewParam.extraSkillObject.SetActive(false);
        }
        else
        {
          UnitSkillAwake unitSkillAwake = awakeSkills[0];
          bool isGray = (double) unitSkillAwake.need_affection > (double) playerUnit.trust_rate;
          IEnumerator e = menuScrollViewParam.LoadSkillIcon(menuScrollViewParam.extraSKill, unitSkillAwake.skill, playerUnit, isGray);
          while (e.MoveNext())
            yield return e.Current;
          e = (IEnumerator) null;
        }
      }
      else
      {
        menuScrollViewParam.extraSkillObject.SetActive(false);
        menuScrollViewParam.slc_DearDegreeTitle.SetActive(false);
        menuScrollViewParam.txtDearDegreeTitle.SetText(string.Empty);
      }
    }
    PlayerItem equippedGear = playerUnit.equippedGear;
    if ((object) equippedGear != null)
    {
      GearGear gear = equippedGear.gear;
    }
    int[] numArray = new int[2]
    {
      masterUnit.kind_GearKind,
      7
    };
    for (int index = 0; index < numArray.Length; ++index)
    {
      int gearId = numArray[index];
      PlayerUnitGearProficiency unitGearProficiency = Array.Find<PlayerUnitGearProficiency>(playerUnit.gear_proficiencies, (Predicate<PlayerUnitGearProficiency>) (x => x.gear_kind_id == gearId));
      if (unitGearProficiency != null && menuScrollViewParam.weapon.Length > index)
      {
        if ((UnityEngine.Object) menuScrollViewParam.weapon[index] == (UnityEngine.Object) null)
        {
          menuScrollViewParam.dyn_Weapon[index].transform.Clear();
          menuScrollViewParam.weapon[index] = gearKindIconPrefab.Clone(menuScrollViewParam.dyn_Weapon[index].transform);
        }
        menuScrollViewParam.weapon[index].GetComponent<GearKindIcon>().Init(unitGearProficiency.gear_kind_id, CommonElement.none);
        menuScrollViewParam.weapon[index].GetComponent<UI2DSprite>().depth = menuScrollViewParam.dyn_Weapon[index].depth + 1;
        if ((UnityEngine.Object) menuScrollViewParam.gearProfiencyIcon[index] == (UnityEngine.Object) null)
        {
          menuScrollViewParam.dyn_IconRank[index].transform.Clear();
          menuScrollViewParam.gearProfiencyIcon[index] = profIconPrefab.Clone(menuScrollViewParam.dyn_IconRank[index].transform).GetComponent<GearProfiencyIcon>();
        }
        menuScrollViewParam.gearProfiencyIcon[index].Init(unitGearProficiency.level);
      }
    }
    bool flag1 = true;
    bool flag2 = true;
    if (((isMaterial ? 1 : (playerUnit.is_storage ? 1 : 0)) | (limitMode ? 1 : 0)) != 0)
      flag2 = false;
    bool flag3;
    if (limitMode)
    {
      flag1 = false;
      flag3 = false;
    }
    else
    {
      List<int> list = ((IEnumerable<UnitUnit>) MasterData.UnitUnitList).Where<UnitUnit>((Func<UnitUnit, bool>) (x => x.same_character_id == masterUnit.same_character_id)).Select<UnitUnit, int>((Func<UnitUnit, int>) (x => x.ID)).ToList<int>();
      flag3 = false;
      foreach (QuestCharacterS questCharacterS in MasterData.QuestCharacterSList)
      {
        if (QuestCharacterS.CheckIsReleased(questCharacterS.start_at) && list.Contains(questCharacterS.unit_UnitUnit))
        {
          flag3 = true;
          break;
        }
      }
    }
    menuScrollViewParam.IbtnIntimacy.isEnabled = flag1;
    menuScrollViewParam.IbtnUnitTraining.isEnabled = flag2;
    menuScrollViewParam.IbtnUnitQuest.isEnabled = flag3;
  }

  public override bool Init(PlayerUnit playerUnit)
  {
    this.gameObject.SetActive(true);
    this.dispStatusCurrent = 0;
    this.targetUnit = playerUnit;
    bool flag = this.controlFlags.IsOn(Control.OverkillersUnit);
    if (flag && (UnityEngine.Object) this.IbtnStatusChange != (UnityEngine.Object) null)
    {
      this.IbtnStatusDetail.isEnabled = false;
      this.IbtnStatusChange.isEnabled = false;
      foreach (Collider component in this.IbtnStatusChange.gameObject.GetComponents<BoxCollider>())
        component.enabled = false;
    }
    int max = playerUnit.exp_next + playerUnit.exp - 1;
    int n = playerUnit.exp;
    if (n > max)
      n = max;
    this.lvGauge.setValue(n, max, false, -1f, -1f);
    Judgement.NonBattleParameter nonBattleParameter;
    if (!this.isMemory)
    {
      nonBattleParameter = Judgement.NonBattleParameter.FromPlayerUnit(playerUnit, this.controlFlags.IsOn(Control.OverkillersUnit | Control.SelfAbility));
      this.setText(this.txt_Lv, playerUnit.level);
    }
    else
    {
      nonBattleParameter = Judgement.NonBattleParameter.FromPlayerUnitMemory(playerUnit);
      this.setText(this.txt_Lv, playerUnit.memory_level);
    }
    this.txt_Lvmax.SetTextLocalize(Consts.Format(Consts.GetInstance().UNIT_DETAIL_MAX, (IDictionary) new Hashtable()
    {
      {
        (object) "max",
        (object) playerUnit.max_level
      }
    }));
    this.SetPrincessType(playerUnit);
    this.setBicon(playerUnit.breakthrough_count, playerUnit.unit.breakthrough_limit);
    if (playerUnit.tower_is_entry && Singleton<CommonRoot>.GetInstance().headerType == CommonRoot.HeaderType.Tower)
    {
      this.dir_Hp.SetActive(false);
      this.dir_maxHp.SetActive(true);
      this.setText(this.txt_HpNow, TowerUtil.GetHp(nonBattleParameter.Hp, playerUnit.TowerHpRate));
      this.txt_Hpmax.SetTextLocalize(Consts.Format(Consts.GetInstance().UNIT_DETAIL_MAX, (IDictionary) new Hashtable()
      {
        {
          (object) "max",
          (object) nonBattleParameter.Hp
        }
      }));
    }
    else
    {
      this.dir_Hp.SetActive(true);
      this.dir_maxHp.SetActive(false);
      this.setText(this.txt_Hp, nonBattleParameter.Hp);
    }
    this.setText(this.txt_Cost, playerUnit.cost);
    this.setText(this.txt_Movement, nonBattleParameter.Move);
    this.setText(this.txt_Power, nonBattleParameter.Strength);
    this.setText(this.txt_Magic, nonBattleParameter.Intelligence);
    this.setText(this.txt_Protct, nonBattleParameter.Vitality);
    this.setText(this.txt_Spirit, nonBattleParameter.Mind);
    this.setText(this.txt_Agility, nonBattleParameter.Agility);
    this.setText(this.txt_Technique, nonBattleParameter.Dexterity);
    this.setText(this.txt_Luck, nonBattleParameter.Luck);
    if (!this.isMemory)
    {
      this.slc_HpMaxStar.SetActive(playerUnit.hp.is_max);
      this.slc_AgilityMaxStar.SetActive(playerUnit.agility.is_max);
      this.slc_LuckMaxStar.SetActive(playerUnit.lucky.is_max);
      this.slc_MagicMaxStar.SetActive(playerUnit.intelligence.is_max);
      this.slc_PowerMaxStar.SetActive(playerUnit.strength.is_max);
      this.slc_ProtctMaxStar.SetActive(playerUnit.vitality.is_max);
      this.slc_SpiritMaxStar.SetActive(playerUnit.mind.is_max);
      this.slc_TechniqueMaxStar.SetActive(playerUnit.dexterity.is_max);
    }
    else
    {
      this.slc_HpMaxStar.SetActive(playerUnit.is_memory_hp_max);
      this.slc_AgilityMaxStar.SetActive(playerUnit.is_memory_agility_max);
      this.slc_LuckMaxStar.SetActive(playerUnit.is_memory_lucky_max);
      this.slc_MagicMaxStar.SetActive(playerUnit.is_memory_intelligence_max);
      this.slc_PowerMaxStar.SetActive(playerUnit.is_memory_strength_max);
      this.slc_ProtctMaxStar.SetActive(playerUnit.is_memory_vitality_max);
      this.slc_SpiritMaxStar.SetActive(playerUnit.is_memory_mind_max);
      this.slc_TechniqueMaxStar.SetActive(playerUnit.is_memory_dexterity_max);
    }
    if (flag)
    {
      OverkillersParameter parameter = OverkillersParameter.getParameter(playerUnit.unit.overkillers_parameter, (int) playerUnit.unityTotal);
      if (parameter != null)
      {
        this.setTextOverkillersParameter(this.txt_PowerPlus, parameter.strength);
        this.setTextOverkillersParameter(this.txt_MagicPlus, parameter.intelligence);
        this.setTextOverkillersParameter(this.txt_ProtctPlus, parameter.vitality);
        this.setTextOverkillersParameter(this.txt_SpiritPlus, parameter.mind);
        this.setTextOverkillersParameter(this.txt_AgilityPlus, parameter.agility);
        this.setTextOverkillersParameter(this.txt_TechniquePlus, parameter.dexterity);
        this.setTextOverkillersParameter(this.txt_LuckPlus, parameter.lucky);
        this.dirStatusPlus.SetActive(true);
        this.setTextOverkillersParameter(this.txt_HpPlus, parameter.hp);
        this.txt_HpPlus.gameObject.SetActive(true);
        goto label_21;
      }
    }
    else if (this.controlFlags.IsOff(Control.SelfAbility) && this.controlFlags.IsOn(Control.OverkillersSlot) && playerUnit.isAnyCacheOverkillersUnits)
    {
      this.setTextPlus(this.txt_PowerPlus, playerUnit.strength.overkillersValue);
      this.setTextPlus(this.txt_MagicPlus, playerUnit.intelligence.overkillersValue);
      this.setTextPlus(this.txt_ProtctPlus, playerUnit.vitality.overkillersValue);
      this.setTextPlus(this.txt_SpiritPlus, playerUnit.mind.overkillersValue);
      this.setTextPlus(this.txt_AgilityPlus, playerUnit.agility.overkillersValue);
      this.setTextPlus(this.txt_TechniquePlus, playerUnit.dexterity.overkillersValue);
      this.setTextPlus(this.txt_LuckPlus, playerUnit.lucky.overkillersValue);
      this.dirStatusPlus.SetActive(true);
      this.setTextPlus(this.txt_HpPlus, playerUnit.hp.overkillersValue);
      this.txt_HpPlus.gameObject.SetActive(true);
      goto label_21;
    }
    this.dirStatusPlus.SetActive(false);
    this.txt_HpPlus.gameObject.SetActive(false);
label_21:
    this.setText(this.txt_Attack, nonBattleParameter.PhysicalAttack);
    this.setText(this.txt_Defense, nonBattleParameter.PhysicalDefense);
    this.setText(this.txt_Matk, nonBattleParameter.MagicAttack);
    this.setText(this.txt_Mdef, nonBattleParameter.MagicDefense);
    this.setText(this.txt_Dexterity, nonBattleParameter.Hit);
    this.setText(this.txt_Critical, nonBattleParameter.Critical);
    this.setText(this.txt_Evasion, nonBattleParameter.Evasion);
    this.setText(this.txt_Fighting, nonBattleParameter.Combat);
    this.setParamUnity(playerUnit);
    return true;
  }

  private void setTextPlus(UILabel label, int value)
  {
    label.text = value.ToString("+#;-#;+0;");
  }

  private void setTextOverkillersParameter(UILabel label, int percentage)
  {
    label.text = "x" + OverkillersParameter.toStringPercentage(percentage) + "%";
  }

  private OverkillersSkillRelease getOverkillersSkill()
  {
    return this.isMine(this.playerUnit) ? this.playerUnit.overkillersSkill : (OverkillersSkillRelease) null;
  }

  private void setBicon(int count, int max)
  {
    for (int index = 0; index < this.slc_LBicon_None.Length; ++index)
    {
      if (index < max)
      {
        this.slc_LBicon_None[index].SetActive(index >= count);
        this.slc_LBicon_Blue[index].SetActive(index < count);
      }
      else
      {
        this.slc_LBicon_None[index].SetActive(false);
        this.slc_LBicon_Blue[index].SetActive(false);
      }
    }
  }

  private void setParamUnity(PlayerUnit playerUnit)
  {
    string[] strArray = ((double) playerUnit.unityTotal < 99.0 ? playerUnit.unityTotal.ToString("f1") : playerUnit.unityTotal.ToString()).Split('.');
    this.txt_Unity.SetTextLocalize(strArray[0]);
    if (strArray.Length > 1)
      this.txt_Unity_dec.SetTextLocalize("." + strArray[1]);
    else
      this.txt_Unity_dec.SetTextLocalize("");
    if (this.controlFlags.IsOff(Control.ToutaPlusNoEnable) && this.isMine(playerUnit) && this.controlFlags.IsOff(Control.OverkillersUnit))
      this.popupUnityDetail = (System.Action) (() =>
      {
        if (!((UnityEngine.Object) this.unityDetailPrefab != (UnityEngine.Object) null))
          return;
        if (!playerUnit.is_enemy && !playerUnit.is_gesut && Player.Current.id == playerUnit.player_id)
        {
          PlayerUnit playerUnit1 = Array.Find<PlayerUnit>(SMManager.Get<PlayerUnit[]>(), (Predicate<PlayerUnit>) (x => x.id == playerUnit.id));
          if ((object) playerUnit1 == null)
            playerUnit1 = playerUnit;
          playerUnit = playerUnit1;
        }
        PopupUnityValueDetail.show(this.unityDetailPrefab, this.stageItemPrefab, (float) playerUnit.unity_value, playerUnit.buildup_unity_value_f, playerUnit.unit, (System.Action) (() =>
        {
          if (Singleton<NGSceneManager>.GetInstance().sceneName == "unit004_2" || Singleton<NGSceneManager>.GetInstance().sceneName == "unit004_2_sea")
          {
            this.ModifySelectedPlayerUnitInChangeSceneParam(playerUnit);
            Singleton<NGGameDataManager>.GetInstance().OpenUnityPopup = this.popupUnityDetail;
          }
          else
          {
            if (!(Singleton<NGSceneManager>.GetInstance().sceneName == "unit004_JobChange"))
              return;
            Singleton<NGGameDataManager>.GetInstance().OpenUnityPopup = this.popupUnityDetail;
          }
        }));
      });
    else
      this.popupUnityDetail = (System.Action) null;
  }

  public void ModifySelectedPlayerUnitInChangeSceneParam(PlayerUnit playerUnit)
  {
    object[] args = Singleton<NGSceneManager>.GetInstance().GetSavedChangeSceneParam().args;
    for (int index = 0; index < args.Length; ++index)
    {
      if (args[index] != null && args[index].GetType() == typeof (PlayerUnit))
      {
        args[index] = (object) playerUnit;
        break;
      }
    }
  }

  private bool isMine(PlayerUnit unit)
  {
    return !unit.is_enemy && !unit.is_gesut && Player.Current.id == unit.player_id;
  }

  private void SetPrincessType(PlayerUnit playerUnit)
  {
    if (playerUnit.unit_type == null || playerUnit.is_enemy)
    {
      this.dir_prencessType.gameObject.SetActive(false);
    }
    else
    {
      this.dir_prencessType.gameObject.SetActive(true);
      if ((UnityEngine.Object) this.slc_prencessType[0].gameObject.GetComponent<UILabel>() != (UnityEngine.Object) null)
        this.slc_prencessType[0].gameObject.GetComponent<UILabel>().SetText("");
      if (!Singleton<NGGameDataManager>.GetInstance().IsSea)
        ((IEnumerable<GameObject>) this.slc_prencessType).ToggleOnceEx((int) playerUnit.unit_type.Enum);
      else
        this.slc_prencessType[0].gameObject.GetComponent<UILabel>().SetText(Consts.GetInstance().GetUnitTypeText(playerUnit.unit_type.Enum));
    }
  }

  public override void MarkAsChanged()
  {
    if ((UnityEngine.Object) this.slc_DearDegreeTitle != (UnityEngine.Object) null)
      this.slc_DearDegreeTitle.GetComponent<UIWidget>().MarkAsChanged();
    foreach (GameObject gameObject in this.slc_LBicon_None)
      gameObject.GetComponent<UIWidget>().MarkAsChanged();
    foreach (GameObject gameObject in this.slc_LBicon_Blue)
      gameObject.GetComponent<UIWidget>().MarkAsChanged();
    foreach (UIWidget componentsInChild in this.unitySkillObject.GetComponentsInChildren<UIWidget>())
      componentsInChild.MarkAsChanged();
    foreach (UIWidget componentsInChild in this.extraSkillObject.GetComponentsInChildren<UIWidget>())
      componentsInChild.MarkAsChanged();
  }

  public void onClickStatusChange()
  {
    ++this.dispStatusCurrent;
    this.dispStatusCurrent %= ((IEnumerable<GameObject>) this.dir_statusNum).Count<GameObject>();
    ((IEnumerable<GameObject>) this.dir_statusNum).ToggleOnceEx(this.dispStatusCurrent);
    ((IEnumerable<GameObject>) this.dir_statusListText).ToggleOnceEx(this.dispStatusCurrent);
  }

  public void onClickOverkillersSKill()
  {
    OverkillersSkillRelease overkillersSkill = this.getOverkillersSkill();
    if (overkillersSkill == null)
      return;
    PopupSkillDetails.show(this.skillDetailPrefab, new PopupSkillDetails.Param(overkillersSkill, new int?((int) this.playerUnit.unityTotal)), false, (System.Action) null, false);
  }

  public void onClickExtraSKill()
  {
    UnitSkillAwake[] awakeSkills = this.playerUnit.GetAwakeSkills();
    if (awakeSkills == null || awakeSkills.Length == 0)
      return;
    PopupSkillDetails.show(this.skillDetailPrefab, PopupSkillDetails.Param.createByUnitView(awakeSkills[0], this.playerUnit, (System.Action) null), false, (System.Action) null, false);
  }

  public void onClicedUnity()
  {
    System.Action popupUnityDetail = this.popupUnityDetail;
    if (popupUnityDetail == null)
      return;
    popupUnityDetail();
  }

  public void onClickedTabEquipment()
  {
    this.onClickedTab(DetailMenuScrollViewParam.TabMode.Equipment);
  }

  public void onClickedTabOverkillers()
  {
    this.onClickedTab(DetailMenuScrollViewParam.TabMode.Overkillers);
  }

  private void onClickedTab(DetailMenuScrollViewParam.TabMode mode)
  {
    Unit0042Menu inParents = NGUITools.FindInParents<Unit0042Menu>(this.gameObject);
    if ((UnityEngine.Object) inParents != (UnityEngine.Object) null)
      inParents.UpdateInfoIndicator(mode);
    else
      this.changeTab(mode);
  }

  public void changeTab(DetailMenuScrollViewParam.TabMode mode)
  {
    for (int index = 0; index < this.tabObjects.Length; ++index)
      this.tabObjects[index].SetActive((DetailMenuScrollViewParam.TabMode) index == mode);
  }

  public override IEnumerator initAsyncDiffMode(
    PlayerUnit playerUnit,
    PlayerUnit prevUnit,
    IDetailMenuContainer menuContainer)
  {
    DetailMenuScrollViewParam menuScrollViewParam = this;
    menuScrollViewParam.playerUnit = playerUnit;
    if (menuScrollViewParam.weapon == null)
      menuScrollViewParam.weapon = new GameObject[menuScrollViewParam.dyn_Weapon.Length];
    if (menuScrollViewParam.gearProfiencyIcon == null)
      menuScrollViewParam.gearProfiencyIcon = new GearProfiencyIcon[menuScrollViewParam.dyn_IconRank.Length];
    menuScrollViewParam.skillDetailPrefab = menuContainer.skillDetailDialogPrefab;
    menuScrollViewParam.unityDetailPrefab = menuContainer.unityDetailPrefab;
    menuScrollViewParam.stageItemPrefab = menuContainer.stageItemPrefab;
    menuScrollViewParam.init(playerUnit, prevUnit);
    menuScrollViewParam.overkillersSlots.isHide = true;
    if ((UnityEngine.Object) menuScrollViewParam.unitySkillObject != (UnityEngine.Object) null)
    {
      OverkillersSkillRelease overkillersSkill = menuScrollViewParam.getOverkillersSkill();
      if (overkillersSkill != null)
      {
        menuScrollViewParam.unitySkillObject.SetActive(true);
        yield return (object) menuScrollViewParam.LoadSkillIcon(menuScrollViewParam.unitySKill, overkillersSkill.skill, playerUnit, (double) playerUnit.unityTotal < (double) overkillersSkill.unity_value);
      }
      else
        menuScrollViewParam.unitySkillObject.SetActive(false);
    }
    if ((UnityEngine.Object) menuScrollViewParam.extraSkillObject != (UnityEngine.Object) null)
    {
      menuScrollViewParam.extraSkillObject.SetActive(false);
      menuScrollViewParam.slc_DearDegreeTitle.SetActive(false);
      menuScrollViewParam.txtDearDegreeTitle.SetText(string.Empty);
      if (playerUnit.unit.trust_target_flag)
      {
        menuScrollViewParam.extraSkillObject.SetActive(true);
        menuScrollViewParam.slc_DearDegreeTitle.SetActive(true);
        if (playerUnit.unit.IsSea)
          menuScrollViewParam.txtDearDegreeTitle.SetText(Consts.GetInstance().POPUP_004_LIMIT_EXTENDED_TITLE_DEAR);
        else
          menuScrollViewParam.txtDearDegreeTitle.SetText(Consts.GetInstance().POPUP_004_LIMIT_EXTENDED_TITLE_RELEVANCE);
        double num = Math.Round((double) playerUnit.trust_rate * 100.0) / 100.0;
        menuScrollViewParam.txtDearDegree.SetTextLocalize(num.ToString());
        menuScrollViewParam.txtDearDegreeMax.SetTextLocalize(Consts.Format(Consts.GetInstance().UNIT_TRUST_RATE_PERCENT3, (IDictionary) new Hashtable()
        {
          {
            (object) "trust_rate",
            (object) string.Format("{0}", (object) playerUnit.trust_max_rate)
          }
        }));
        UnitSkillAwake[] awakeSkills = playerUnit.GetAwakeSkills();
        if (awakeSkills == null || awakeSkills.Length == 0)
        {
          menuScrollViewParam.extraSkillObject.SetActive(false);
        }
        else
        {
          UnitSkillAwake unitSkillAwake = awakeSkills[0];
          bool isGray = (double) unitSkillAwake.need_affection > (double) playerUnit.trust_rate;
          yield return (object) menuScrollViewParam.LoadSkillIcon(menuScrollViewParam.extraSKill, unitSkillAwake.skill, playerUnit, isGray);
        }
      }
    }
    int[] numArray = new int[2]
    {
      playerUnit.unit.kind_GearKind,
      7
    };
    for (int index = 0; index < numArray.Length; ++index)
    {
      int gearId = numArray[index];
      PlayerUnitGearProficiency unitGearProficiency = Array.Find<PlayerUnitGearProficiency>(playerUnit.gear_proficiencies, (Predicate<PlayerUnitGearProficiency>) (x => x.gear_kind_id == gearId));
      if (unitGearProficiency != null && menuScrollViewParam.weapon.Length > index)
      {
        if ((UnityEngine.Object) menuScrollViewParam.weapon[index] == (UnityEngine.Object) null)
        {
          menuScrollViewParam.dyn_Weapon[index].transform.Clear();
          menuScrollViewParam.weapon[index] = menuContainer.gearKindIconPrefab.Clone(menuScrollViewParam.dyn_Weapon[index].transform);
        }
        menuScrollViewParam.weapon[index].GetComponent<GearKindIcon>().Init(unitGearProficiency.gear_kind_id, CommonElement.none);
        menuScrollViewParam.weapon[index].GetComponent<UI2DSprite>().depth = menuScrollViewParam.dyn_Weapon[index].depth + 1;
        if ((UnityEngine.Object) menuScrollViewParam.gearProfiencyIcon[index] == (UnityEngine.Object) null)
        {
          menuScrollViewParam.dyn_IconRank[index].transform.Clear();
          menuScrollViewParam.gearProfiencyIcon[index] = menuContainer.profIconPrefab.Clone(menuScrollViewParam.dyn_IconRank[index].transform).GetComponent<GearProfiencyIcon>();
        }
        menuScrollViewParam.gearProfiencyIcon[index].Init(unitGearProficiency.level);
      }
    }
    menuScrollViewParam.inactivateGameObject<UIButton>(menuScrollViewParam.IbtnStatusDetail);
    menuScrollViewParam.inactivateGameObject<UIButton>(menuScrollViewParam.IbtnUnitTraining);
    menuScrollViewParam.inactivateGameObject<UIButton>(menuScrollViewParam.IbtnIntimacy);
    menuScrollViewParam.inactivateGameObject<UIButton>(menuScrollViewParam.IbtnUnitQuest);
    menuScrollViewParam.inactivateGameObject(menuScrollViewParam.dirStatusPlus);
    menuScrollViewParam.inactivateGameObject<UILabel>(menuScrollViewParam.txt_HpPlus);
  }

  private void init(PlayerUnit playerUnit, PlayerUnit prevUnit)
  {
    this.gameObject.SetActive(true);
    this.dispStatusCurrent = 0;
    this.targetUnit = playerUnit;
    int max = playerUnit.exp_next + playerUnit.exp - 1;
    int n = playerUnit.exp;
    if (n > max)
      n = max;
    this.lvGauge.setValue(n, max, false, -1f, -1f);
    Judgement.NonBattleParameter nonBattleParameter = Judgement.NonBattleParameter.FromPlayerUnit(playerUnit, true);
    Judgement.NonBattleParameter prev = prevUnit != (PlayerUnit) null ? Judgement.NonBattleParameter.FromPlayerUnit(prevUnit, true) : (Judgement.NonBattleParameter) null;
    this.setText(this.txt_Lv, playerUnit.level);
    this.txt_Lvmax.SetTextLocalize(Consts.Format(Consts.GetInstance().UNIT_DETAIL_MAX, (IDictionary) new Hashtable()
    {
      {
        (object) "max",
        (object) playerUnit.max_level
      }
    }));
    this.SetPrincessType(playerUnit);
    this.setBicon(playerUnit.breakthrough_count, playerUnit.unit.breakthrough_limit);
    this.dir_Hp.SetActive(true);
    this.dir_maxHp.SetActive(false);
    this.slc_HpMaxStar.SetActive(playerUnit.hp.is_max);
    this.slc_AgilityMaxStar.SetActive(playerUnit.agility.is_max);
    this.slc_LuckMaxStar.SetActive(playerUnit.lucky.is_max);
    this.slc_MagicMaxStar.SetActive(playerUnit.intelligence.is_max);
    this.slc_PowerMaxStar.SetActive(playerUnit.strength.is_max);
    this.slc_ProtctMaxStar.SetActive(playerUnit.vitality.is_max);
    this.slc_SpiritMaxStar.SetActive(playerUnit.mind.is_max);
    this.slc_TechniqueMaxStar.SetActive(playerUnit.dexterity.is_max);
    this.setParamUnity(playerUnit);
    Dictionary<string, UILabel> dest = new Dictionary<string, UILabel>()
    {
      {
        "Hp",
        this.txt_Hp
      },
      {
        "Move",
        this.txt_Movement
      },
      {
        "Strength",
        this.txt_Power
      },
      {
        "Intelligence",
        this.txt_Magic
      },
      {
        "Vitality",
        this.txt_Protct
      },
      {
        "Mind",
        this.txt_Spirit
      },
      {
        "Agility",
        this.txt_Agility
      },
      {
        "Dexterity",
        this.txt_Technique
      },
      {
        "Luck",
        this.txt_Luck
      },
      {
        "PhysicalAttack",
        this.txt_Attack
      },
      {
        "PhysicalDefense",
        this.txt_Defense
      },
      {
        "MagicAttack",
        this.txt_Matk
      },
      {
        "MagicDefense",
        this.txt_Mdef
      },
      {
        "Hit",
        this.txt_Dexterity
      },
      {
        "Critical",
        this.txt_Critical
      },
      {
        "Evasion",
        this.txt_Evasion
      },
      {
        "Combat",
        this.txt_Fighting
      },
      {
        "Cost",
        this.txt_Cost
      }
    };
    if (prev != null)
      Util.SetTextIntegersWithStateColor<Judgement.NonBattleParameter>(dest, nonBattleParameter, prev);
    else
      Util.SetTextIntegers<Judgement.NonBattleParameter>(dest, nonBattleParameter, Color.white);
  }

  public enum TabMode
  {
    Equipment,
    Overkillers,
  }
}
