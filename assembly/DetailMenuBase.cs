﻿// Decompiled with JetBrains decompiler
// Type: DetailMenuBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;
using System.Collections;

public class DetailMenuBase : NGMenuBase
{
  protected Unit0042Menu menu;
  protected int index;

  public int Index
  {
    get
    {
      return this.index;
    }
  }

  public virtual IEnumerator Init(
    Unit0042Menu menu,
    int index,
    PlayerUnit playerUnit,
    int infoIndex,
    bool isLimit,
    bool isMaterial,
    QuestScoreBonusTimetable[] tables,
    UnitBonus[] unitBonus,
    bool isUpdate = true)
  {
    yield break;
  }

  public virtual IEnumerator SetInformationPanelIndex(int index)
  {
    yield break;
  }
}
