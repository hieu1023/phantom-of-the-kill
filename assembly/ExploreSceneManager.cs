﻿// Decompiled with JetBrains decompiler
// Type: ExploreSceneManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

[RequireComponent(typeof (ExploreTaskManager), typeof (ExploreScreenEffectController), typeof (ExploreModelController))]
[DefaultExecutionOrder(-1)]
public class ExploreSceneManager : Singleton<ExploreSceneManager>
{
  private int mLoopSeChannel = -1;

  public ExploreTaskManager Task { get; private set; }

  public ExploreModelController Model { get; private set; }

  public ExploreScreenEffectController ScreenEffect { get; private set; }

  public Explore033TopMenu TopMenu { get; set; }

  public ExploreFooter Footer { get; set; }

  public bool ReloadDirty { get; private set; }

  public void SetReloadDirty()
  {
    this.ReloadDirty = true;
  }

  public bool IsBackScreen
  {
    get
    {
      return Singleton<PopupManager>.GetInstance().isOpen || !this.IsSceneActive || Singleton<CommonRoot>.GetInstance().DailyMissionController.IsOpened;
    }
  }

  public bool IsSceneActive
  {
    get
    {
      return Singleton<NGSceneManager>.GetInstance().sceneName.Equals("explore033_Top") && Singleton<NGSceneManager>.GetInstance().isSceneInitialized;
    }
  }

  protected override void Initialize()
  {
    this.Task = this.GetComponent<ExploreTaskManager>();
    this.Model = this.GetComponent<ExploreModelController>();
    this.ScreenEffect = this.GetComponent<ExploreScreenEffectController>();
  }

  public static void DestroyInstance()
  {
    ExploreSceneManager instanceOrNull = Singleton<ExploreSceneManager>.GetInstanceOrNull();
    if ((Object) instanceOrNull == (Object) null)
      return;
    Object.Destroy((Object) instanceOrNull.gameObject);
    instanceOrNull.clearInstance();
  }

  public void Pause(bool pause)
  {
    this.Task.Pause(pause);
  }

  public void PlaySe(string clipName, float delay = 0.0f)
  {
    if (!this.IsSceneActive)
      return;
    Singleton<NGSoundManager>.GetInstance().PlaySe(clipName, false, delay, -1);
  }

  public void PlayLoopSe(string clipName)
  {
    if (!this.IsSceneActive)
      return;
    this.mLoopSeChannel = Singleton<NGSoundManager>.GetInstance().PlaySe(clipName, true, 0.0f, -1);
  }

  public void StopLoopSe()
  {
    if (this.mLoopSeChannel == -1)
      return;
    Singleton<NGSoundManager>.GetInstance().StopSe(this.mLoopSeChannel, 0.5f);
    this.mLoopSeChannel = -1;
  }

  public void StartReload()
  {
    this.Pause(true);
    Singleton<CommonRoot>.GetInstance().loadingMode = 0;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    Singleton<NGSceneManager>.GetInstance().StartCoroutine(this.Reload());
  }

  private IEnumerator Reload()
  {
    yield return (object) Singleton<NGSceneManager>.GetInstance().destroyLoadedScenesImmediate();
    Singleton<ExploreDataManager>.GetInstance().CleanLog();
    Explore033TopScene.changeScene(false, false);
  }

  public IEnumerator OnStartExploreSceneAsync()
  {
    Singleton<ExploreDataManager>.GetInstance().UpdateBlUnitsFromPlayerDeck();
    this.ScreenEffect.TransitionFullIn();
    yield return (object) this.Model.CreateAllExploreModel();
    this.Task.Initialize();
    yield return (object) this.ScreenEffect.WaitForTransitionFull();
  }

  public void OnBackExploreScene()
  {
    this.Task.OnBackExploreScene();
  }

  public void OnEndExploreScene()
  {
    Singleton<PopupManager>.GetInstance().dismissWithoutAnim(false, true);
    this.StopLoopSe();
  }

  public void OnDestoryExploreScene()
  {
    this.Pause(true);
    this.Model.SetExploreVisible(false);
  }

  private void OnApplicationPause(bool pause)
  {
    if (!pause)
      return;
    this.SetReloadDirty();
  }
}
