﻿// Decompiled with JetBrains decompiler
// Type: Tower029BattleEntryMenu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using DeckOrganization;
using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Tower029BattleEntryMenu : BackButtonMenuBase
{
  private bool isCompletedOverkillersDeck = true;
  private const int DECK_UNIT_MAX = 6;
  private const float LINK_WIDTH = 92f;
  private const float LINK_DEFWIDTH = 114f;
  private const float scale = 0.8070176f;
  private const int AutoButtonOnOffTween = 1001;
  [SerializeField]
  private UILabel lblTitle;
  [SerializeField]
  private UILabel lblTotalPowerStr;
  [SerializeField]
  private UILabel lblTotalPowerValue;
  [SerializeField]
  private GameObject[] linkCharacters;
  [SerializeField]
  private GameObject[] supplyItems;
  private GameObject unitIconPrefab;
  private GameObject itemIconPrefab;
  [SerializeField]
  private UILabel lblLeaderSkillName;
  [SerializeField]
  private UILabel lblLeaderSkillDesc;
  [SerializeField]
  private GameObject objLeaderSkillZoom;
  [SerializeField]
  private GameObject btnSortie;
  [SerializeField]
  private GameObject btnGearRepair;
  [SerializeField]
  private GameObject btnTeamEdit;
  private List<PlayerUnit> selectedUnits;
  private GameObject goHpGauge;
  private bool init;
  private TowerProgress progress;
  private TowerLevelList floorInfo;
  private GameObject skillDetailPrefab;
  private PlayerUnitLeader_skills leaderSkill;

  private string totalCombat
  {
    get
    {
      if (this.selectedUnits == null)
        return "---";
      int combat = 0;
      this.selectedUnits.ForEach((System.Action<PlayerUnit>) (x =>
      {
        if (!(x != (PlayerUnit) null))
          return;
        combat += Judgement.NonBattleParameter.FromPlayerUnit(x, false).Combat;
      }));
      return combat == 0 ? "---" : string.Format("{0}", (object) combat);
    }
  }

  private void SetButtonVisibility()
  {
    this.btnSortie.SetActive(false);
    this.btnGearRepair.SetActive(false);
    this.btnTeamEdit.SetActive(false);
    if (this.selectedUnits == null || this.selectedUnits.Count <= 0 || (this.selectedUnits.Any<PlayerUnit>((Func<PlayerUnit, bool>) (u => (double) u.TowerHpRate <= 0.0)) || !this.isCompletedOverkillersDeck))
      this.btnTeamEdit.SetActive(true);
    else if (this.selectedUnits.Any<PlayerUnit>((Func<PlayerUnit, bool>) (u =>
    {
      if (u.equippedGear != (PlayerItem) null && u.equippedGear.broken)
        return true;
      return u.equippedGear2 != (PlayerItem) null && u.equippedGear2.broken;
    })))
      this.btnGearRepair.SetActive(true);
    else
      this.btnSortie.SetActive(true);
  }

  private IEnumerator SetSupply()
  {
    PlayerItem[] items = SMManager.Get<PlayerItem[]>();
    items = items.AllTowerSupplies();
    IEnumerator e;
    if ((UnityEngine.Object) this.itemIconPrefab == (UnityEngine.Object) null)
    {
      Future<GameObject> itemIcon = Res.Prefabs.ItemIcon.prefab.Load<GameObject>();
      e = itemIcon.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.itemIconPrefab = itemIcon.Result;
      itemIcon = (Future<GameObject>) null;
    }
    if (!((UnityEngine.Object) this.itemIconPrefab == (UnityEngine.Object) null))
    {
      for (int i = 0; i < this.supplyItems.Length; ++i)
      {
        this.supplyItems[i].transform.Clear();
        ItemIcon component = this.itemIconPrefab.CloneAndGetComponent<ItemIcon>(this.supplyItems[i].transform);
        component.transform.localScale = new Vector3()
        {
          x = 0.8070176f,
          y = 0.8070176f
        };
        if (i < items.Length)
        {
          e = component.InitByPlayerItem(items[i]);
          while (e.MoveNext())
            yield return e.Current;
          e = (IEnumerator) null;
        }
        else
        {
          component.SetModeSupply();
          component.SetEmpty(true);
        }
      }
    }
  }

  private IEnumerator SetTeam()
  {
    this.leaderSkill = (PlayerUnitLeader_skills) null;
    this.lblTotalPowerStr.SetTextLocalize(Consts.GetInstance().TOWER_BATTLE_ENTRY_TOTAL_COMBAT);
    this.lblTotalPowerValue.SetTextLocalize(this.totalCombat);
    int i;
    IEnumerator e;
    if (this.selectedUnits == null || this.selectedUnits.Count <= 0)
    {
      for (i = 0; i < 6; ++i)
      {
        e = this.LoadUnitPrefab(i, (PlayerUnit) null);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
      }
      this.resetLeaderSkill();
    }
    else
    {
      for (i = 0; i < this.selectedUnits.Count; ++i)
      {
        e = this.LoadUnitPrefab(i, this.selectedUnits[i]);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        if (i == 0)
          this.leaderSkill = this.selectedUnits[i].leader_skill;
      }
      this.resetLeaderSkill();
      for (i = this.selectedUnits.Count; i < 6; ++i)
      {
        e = this.LoadUnitPrefab(i, (PlayerUnit) null);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
      }
    }
  }

  private void resetLeaderSkill()
  {
    if (!this.isCompletedOverkillersDeck)
    {
      this.lblLeaderSkillName.SetText("---");
      this.lblLeaderSkillDesc.SetText(Consts.GetInstance().QUEST_0028_INDICATOR_LIMITED_OVERKILLERS);
      this.objLeaderSkillZoom.SetActive(false);
    }
    else if (this.leaderSkill != null)
    {
      BattleskillSkill skill = this.leaderSkill.skill;
      this.lblLeaderSkillName.SetTextLocalize(skill.name);
      this.lblLeaderSkillDesc.SetTextLocalize(skill.description);
      this.objLeaderSkillZoom.SetActive(true);
    }
    else
    {
      this.lblLeaderSkillName.SetText("---");
      this.lblLeaderSkillDesc.SetText("-----");
      this.objLeaderSkillZoom.SetActive(false);
    }
  }

  public IEnumerator LoadUnitPrefab(int index, PlayerUnit unit)
  {
    Future<GameObject> unitIconPrefabF;
    IEnumerator e;
    if ((UnityEngine.Object) this.unitIconPrefab == (UnityEngine.Object) null)
    {
      unitIconPrefabF = Res.Prefabs.UnitIcon.normal.Load<GameObject>();
      e = unitIconPrefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.unitIconPrefab = unitIconPrefabF.Result;
      unitIconPrefabF = (Future<GameObject>) null;
    }
    GameObject unitIconGo = this.unitIconPrefab.Clone(this.linkCharacters[index].transform);
    if ((UnityEngine.Object) this.goHpGauge == (UnityEngine.Object) null)
    {
      unitIconPrefabF = Res.Prefabs.tower.dir_hp_gauge.Load<GameObject>();
      e = unitIconPrefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.goHpGauge = unitIconPrefabF.Result;
      unitIconPrefabF = (Future<GameObject>) null;
    }
    if ((UnityEngine.Object) this.goHpGauge != (UnityEngine.Object) null && unit != (PlayerUnit) null)
    {
      UnitIcon component = unitIconGo.GetComponent<UnitIcon>();
      if ((UnityEngine.Object) component != (UnityEngine.Object) null)
      {
        this.goHpGauge.Clone(unitIconGo.GetComponent<UnitIcon>().hp_gauge.transform);
        component.HpGauge.SetGaugeAndDropoutIcon(unit.TowerHp, unit.total_hp, false);
      }
    }
    unitIconGo.transform.localScale = new Vector3(0.8070176f, 0.8070176f);
    UnitIcon unitScript = unitIconGo.GetComponent<UnitIcon>();
    e = unitScript.setSimpleUnit(unit);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    unitScript.setLevelText(unit);
    unitScript.ShowBottomInfo(UnitSortAndFilter.SORT_TYPES.Level);
    if (unit != (PlayerUnit) null)
    {
      unitScript.onClick = (System.Action<UnitIconBase>) (x => this.StartCoroutine(this.ChangeDetailScene(unit, index)));
      EventDelegate.Set(unitScript.Button.onLongPress, (EventDelegate.Callback) (() => this.StartCoroutine(this.ChangeDetailScene(unit, index))));
      unitScript.BreakWeapon = unit.IsBrokenEquippedGear;
      unitScript.SpecialIcon = false;
      unitScript.Gray = (double) unit.TowerHpRate <= 0.0;
    }
    else
      unitScript.SetEmpty();
    unitScript.Favorite = false;
  }

  private IEnumerator ChangeDetailScene(PlayerUnit unit, int index)
  {
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    yield return (object) null;
    Unit0042Scene.changeScene(true, unit, this.selectedUnits.Where<PlayerUnit>((Func<PlayerUnit, bool>) (x => x != (PlayerUnit) null)).ToArray<PlayerUnit>(), false, false);
    this.DestroyObject();
  }

  private void DestroyObject()
  {
    foreach (GameObject linkCharacter in this.linkCharacters)
    {
      UnitIcon componentInChildren = linkCharacter.GetComponentInChildren<UnitIcon>();
      if ((UnityEngine.Object) componentInChildren != (UnityEngine.Object) null)
        UnityEngine.Object.Destroy((UnityEngine.Object) componentInChildren.gameObject);
    }
    foreach (GameObject supplyItem in this.supplyItems)
    {
      ItemIcon componentInChildren = supplyItem.GetComponentInChildren<ItemIcon>();
      if ((UnityEngine.Object) componentInChildren != (UnityEngine.Object) null)
        UnityEngine.Object.Destroy((UnityEngine.Object) componentInChildren.gameObject);
    }
  }

  private IEnumerator StartSortie()
  {
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    if (this.selectedUnits.Count > 0)
      Singleton<NGSoundManager>.GetInstance().playVoiceByID(this.selectedUnits[0].unit.unitVoicePattern, 70, 0, 0.0f);
    int tower_id = this.progress.tower_id;
    Future<WebAPI.Response.TowerBattleStart> f = WebAPI.TowerBattleStart(tower_id, (System.Action<WebAPI.Response.UserError>) (e => WebAPI.DefaultUserErrorCallback(e)));
    IEnumerator e1 = f.Wait();
    while (e1.MoveNext())
      yield return e1.Current;
    e1 = (IEnumerator) null;
    if (f.Result == null)
    {
      TowerUtil.GotoMypage();
    }
    else
    {
      int stage_id = -1;
      TowerStage towerStage = ((IEnumerable<TowerStage>) MasterData.TowerStageList).FirstOrDefault<TowerStage>((Func<TowerStage, bool>) (x => x.tower_id == tower_id && x.floor == this.floorInfo.floorNum));
      if (towerStage != null)
        stage_id = towerStage.stage_id;
      string battleUuid = f.Result.battle_uuid;
      TowerEnemy[] enemies = f.Result.enemies;
      Tuple<int, int, int, int>[] array = ((IEnumerable<WebAPI.Response.TowerBattleStartEnemy_items>) f.Result.enemy_items).Select<WebAPI.Response.TowerBattleStartEnemy_items, Tuple<int, int, int, int>>((Func<WebAPI.Response.TowerBattleStartEnemy_items, Tuple<int, int, int, int>>) (x => Tuple.Create<int, int, int, int>(x.id, x.reward_type_id, x.reward_id, x.reward_quantity))).ToArray<Tuple<int, int, int, int>>();
      PlayerDeck tower_deck = new PlayerDeck();
      tower_deck.member_limit = 6;
      List<int?> nullableList = new List<int?>();
      for (int index = 0; index < tower_deck.member_limit; ++index)
      {
        if (index < this.selectedUnits.Count)
          nullableList.Add(new int?(this.selectedUnits[index].id));
        else
          nullableList.Add(new int?());
      }
      tower_deck.player_unit_ids = nullableList.ToArray();
      tower_deck.cost_limit = 999;
      tower_deck.deck_number = 0;
      BattleInfo battleInfo = BattleInfo.MakeTowerBattleInfo(battleUuid, f.Result.completed_count, stage_id, enemies, array, tower_deck, new PlayerUnit[0], new PlayerItem[0]);
      NGBattleManager instance = Singleton<NGBattleManager>.GetInstance();
      instance.deleteSavedEnvironment();
      instance.startBattle(battleInfo, 0);
    }
  }

  private void SetTowerDeckUnits()
  {
    if (this.selectedUnits == null)
      this.selectedUnits = new List<PlayerUnit>();
    this.selectedUnits.Clear();
    if (TowerUtil.towerDeckUnits == null)
      return;
    foreach (TowerDeckUnit towerDeckUnit in TowerUtil.towerDeckUnits)
    {
      TowerDeckUnit deckUnit = towerDeckUnit;
      PlayerUnit playerUnit = ((IEnumerable<PlayerUnit>) SMManager.Get<PlayerUnit[]>()).FirstOrDefault<PlayerUnit>((Func<PlayerUnit, bool>) (u => u != (PlayerUnit) null && u.tower_is_entry && u.id == deckUnit.player_unit_id));
      if (playerUnit != (PlayerUnit) null)
        this.selectedUnits.Add(playerUnit);
    }
  }

  private void onClosedSkillZoom()
  {
    this.IsPush = false;
  }

  private IEnumerator UpdateDisplayDeckUnits()
  {
    this.selectedUnits = new List<PlayerUnit>();
    if (TowerUtil.towerDeckUnits != null)
    {
      PlayerUnit[] playerUnitArray = SMManager.Get<PlayerUnit[]>();
      for (int i = 0; i < TowerUtil.towerDeckUnits.Length; i++)
      {
        PlayerUnit playerUnit = ((IEnumerable<PlayerUnit>) playerUnitArray).Where<PlayerUnit>((Func<PlayerUnit, bool>) (u => u.id == TowerUtil.towerDeckUnits[i].player_unit_id)).FirstOrDefault<PlayerUnit>();
        if (playerUnit != (PlayerUnit) null)
          this.selectedUnits.Add(playerUnit);
      }
    }
    OverkillersUtil.checkCompletedDeck(this.selectedUnits.ToArray(), out this.isCompletedOverkillersDeck, (HashSet<int>) null, (bool[]) null);
    this.SetButtonVisibility();
    this.DestroyObject();
    IEnumerator e = this.SetTeam();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    e = this.SetSupply();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public IEnumerator InitializeAsync(TowerProgress progress, TowerLevelList floor)
  {
    this.SetTowerDeckUnits();
    if (!this.init)
    {
      this.init = true;
      this.progress = progress;
      this.floorInfo = floor;
      this.selectedUnits = new List<PlayerUnit>();
      if (TowerUtil.towerDeckUnits != null)
      {
        PlayerUnit[] playerUnitArray = SMManager.Get<PlayerUnit[]>();
        for (int i = 0; i < TowerUtil.towerDeckUnits.Length; i++)
        {
          PlayerUnit playerUnit = ((IEnumerable<PlayerUnit>) playerUnitArray).Where<PlayerUnit>((Func<PlayerUnit, bool>) (u => u.id == TowerUtil.towerDeckUnits[i].player_unit_id)).FirstOrDefault<PlayerUnit>();
          if (playerUnit != (PlayerUnit) null)
            this.selectedUnits.Add(playerUnit);
        }
      }
      this.lblTotalPowerStr.SetTextLocalize(Consts.GetInstance().TOWER_BATTLE_ENTRY_TOTAL_COMBAT);
      this.lblTitle.SetTextLocalize(Consts.GetInstance().TOWER_BATTLE_ENTRY_TITLE);
    }
    if ((UnityEngine.Object) this.skillDetailPrefab == (UnityEngine.Object) null)
    {
      Future<GameObject> loader = PopupSkillDetails.createPrefabLoader(Singleton<NGGameDataManager>.GetInstance().IsSea);
      yield return (object) loader.Wait();
      this.skillDetailPrefab = loader.Result;
      loader = (Future<GameObject>) null;
    }
    OverkillersUtil.checkCompletedDeck(this.selectedUnits.ToArray(), out this.isCompletedOverkillersDeck, (HashSet<int>) null, (bool[]) null);
    this.SetButtonVisibility();
    this.DestroyObject();
    IEnumerator e = this.SetTeam();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    e = this.SetSupply();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public void onEquipButton()
  {
    if (this.IsPushAndSet())
      return;
    Tower029WeaponEditScene.ChangeScene();
  }

  public void onGearRepairButton()
  {
    if (this.IsPushAndSet())
      return;
    Bugu00524Scene.ChangeScene(true);
  }

  public void onSortieButton()
  {
    if (this.IsPushAndSet())
      return;
    this.StartCoroutine(this.StartSortie());
  }

  public void onTeamEditButton()
  {
    if (this.IsPushAndSet())
      return;
    Tower029TeamEditScene.ChangeScene(this.progress);
  }

  private IEnumerator AutoSelectAsync()
  {
    Creator deckCreator_ = new Creator((PlayerUnit[]) null, ((IEnumerable<PlayerUnit>) SMManager.Get<PlayerUnit[]>()).Where<PlayerUnit>((Func<PlayerUnit, bool>) (x => x.tower_is_entry && (double) x.tower_hitpoint_rate > 0.0)).ToArray<PlayerUnit>(), (List<Filter>) null, 1, 6, 0);
    IEnumerator e1 = deckCreator_.Wait();
    while (e1.MoveNext())
      yield return e1.Current;
    e1 = (IEnumerator) null;
    Future<WebAPI.Response.TowerDeckEdit> f = WebAPI.TowerDeckEdit(deckCreator_.result_.Select<PlayerUnit, int>((Func<PlayerUnit, int>) (x => x.id)).ToArray<int>(), this.progress.tower_id, (System.Action<WebAPI.Response.UserError>) (e => WebAPI.DefaultUserErrorCallback(e)));
    e1 = f.Wait();
    while (e1.MoveNext())
      yield return e1.Current;
    e1 = (IEnumerator) null;
    if (f.Result == null)
    {
      TowerUtil.GotoMypage();
    }
    else
    {
      TowerUtil.towerDeckUnits = ((IEnumerable<TowerDeckUnit>) f.Result.tower_deck.tower_deck_units).OrderBy<TowerDeckUnit, int>((Func<TowerDeckUnit, int>) (u => u.position_id)).ToArray<TowerDeckUnit>();
      e1 = this.UpdateDisplayDeckUnits();
      while (e1.MoveNext())
        yield return e1.Current;
      e1 = (IEnumerator) null;
    }
  }

  public void onAutoEditButton()
  {
    if (this.IsPushAndSet())
      return;
    this.IsPush = false;
    this.StartCoroutine(this.AutoSelectAsync());
  }

  public void IbtnBattleSetting()
  {
    if (this.IsPushAndSet())
      return;
    this.StartCoroutine(this.doPopupBattleSetting());
  }

  private IEnumerator doPopupBattleSetting()
  {
    Tower029BattleEntryMenu tower029BattleEntryMenu = this;
    IEnumerator e = Quest0028PopupBattleSetting.show();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    tower029BattleEntryMenu.IsPush = false;
  }

  public override void onBackButton()
  {
    if (this.IsPushAndSet())
      return;
    this.backScene();
  }

  public void onClickedLeaderSkillZoom()
  {
    if (this.leaderSkill == null || (UnityEngine.Object) this.skillDetailPrefab == (UnityEngine.Object) null || this.IsPushAndSet())
      return;
    PopupSkillDetails.show(this.skillDetailPrefab, new PopupSkillDetails.Param(this.leaderSkill), false, new System.Action(this.onClosedSkillZoom), false);
  }
}
