﻿// Decompiled with JetBrains decompiler
// Type: RouletteMenu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class RouletteMenu : NGMenuBase
{
  public float maxSpeed = 1080f;
  public float accelerateTime = 1f;
  public float minSteadyTime = 3f;
  public float decelerateTime = 1f;
  public float blurEffectStartTime = 1f;
  public float blurEffectEndTime = 4f;
  public float cutInStartTime = 1.5f;
  public int awardCount = 8;
  [Range(0.0f, 1f)]
  public float accuracy = 0.75f;
  [Tooltip("「GET」演出のDelay")]
  public float showResultAwardPopupDelay = 0.5f;
  private string[] cutinEffectPrefabPaths = new string[4]
  {
    null,
    "Animations/battle_cutin/battle_cutin_prefab",
    "Animations/battle_cutin/battle_cutin_demon",
    "Animations/battle_cutin/battle_cutin_fairy"
  };
  private int[] cutinEffectSoundClipNames = new int[4]
  {
    0,
    453,
    452,
    451
  };
  [SerializeField]
  private Transform rouletteWheel;
  [SerializeField]
  private TweenAlpha clearRouletteWheelTweenAlpha;
  [SerializeField]
  private TweenAlpha blurredRouletteWheelTweenAlpha;
  [SerializeField]
  private List<GameObject> awardWheelIconContainers;
  [SerializeField]
  private UIButton awardDetailButton;
  [SerializeField]
  private UIButton startButton;
  [SerializeField]
  private UIButton returnButton;
  [SerializeField]
  private GameObject cutInContainer;
  private DuelCutin cutinController;
  [SerializeField]
  private RouletteOttimoAnimationController ottimoAnimationController;
  public RouletteMenu.WheelStatus currentWheelStatus;
  public RouletteMenu.BlurredWheelEffectStatus currentBlurredWheelStatus;
  private bool isCutInPlayed;
  private float acceleration;
  private float deceleration;
  private float accelerateAngle;
  private float minSteadyAngle;
  private float steadyAngle;
  private float decelerateAngle;
  private float steadyTime;
  private float currentPlayTime;
  private float angleIncrement;
  private List<float> awardAngles;
  private float originalWheelAngleZ;
  private float currentWheelAngleZ;
  private int resultIndex;
  private List<RouletteR001FreeDeckEntity> rouletteDeckEntityList;
  private RouletteR001FreeDeckEntity resultDeckEntity;
  private int rouletteID;
  private int rouletteDeckID;
  private int rouletteDeckEntityID;
  private bool shouldShowCampaign;
  private string campaignURL;
  private bool canRoulette;
  public bool isDisplayingAwardDetailPopup;
  private GameObject awardResultPopupPrefab;
  private GameObject campaignPopupPrefab;
  private GameObject awardDetailPopupPrefab;
  private GameObject awardDetailPopupItemPrefab;
  private GameObject awardWheelIconPrefab;
  private GameObject cutInEffectPrefab;
  private bool isNecessaryPrefabsLoaded;
  private bool isResultAwardPrefabsLoaded;
  private NGSoundManager soundManager;

  public IEnumerator Initialize()
  {
    RouletteMenu rouletteMenu = this;
    rouletteMenu.canRoulette = false;
    rouletteMenu.currentWheelStatus = RouletteMenu.WheelStatus.Stopped;
    rouletteMenu.soundManager = Singleton<NGSoundManager>.GetInstance();
    int moduleIndex = 0;
    int rouletteIndex = 0;
    Future<WebAPI.Response.Roulette> rouletteFuture = WebAPI.Roulette((System.Action<WebAPI.Response.UserError>) (e =>
    {
      Singleton<CommonRoot>.GetInstance().isLoading = false;
      Singleton<NGGameDataManager>.GetInstance().isOpenRoulette = false;
      WebAPI.DefaultUserErrorCallback(e);
      Singleton<NGSceneManager>.GetInstance().backScene();
    }));
    IEnumerator e1 = rouletteFuture.Wait();
    while (e1.MoveNext())
      yield return e1.Current;
    e1 = (IEnumerator) null;
    if (rouletteFuture.HasResult && rouletteFuture.Result != null)
    {
      RouletteModuleRoulette[] roulette = rouletteFuture.Result.roulette_modules[moduleIndex].roulette;
      rouletteMenu.rouletteID = roulette[rouletteIndex].id;
      rouletteMenu.rouletteDeckID = roulette[rouletteIndex].deck_id;
      rouletteMenu.shouldShowCampaign = roulette[rouletteIndex].is_campaign;
      rouletteMenu.canRoulette = roulette[rouletteIndex].can_roulette;
      // ISSUE: reference to a compiler-generated method
      rouletteMenu.campaignURL = ((IEnumerable<RouletteR001FreeRoulette>) MasterData.RouletteR001FreeRouletteList).FirstOrDefault<RouletteR001FreeRoulette>(new Func<RouletteR001FreeRoulette, bool>(rouletteMenu.\u003CInitialize\u003Eb__58_1)).url;
      rouletteMenu.StartCoroutine(rouletteMenu.LoadNecessaryPrefabsCoroutine());
      while (!rouletteMenu.isNecessaryPrefabsLoaded)
        yield return (object) null;
      // ISSUE: reference to a compiler-generated method
      rouletteMenu.rouletteDeckEntityList = ((IEnumerable<RouletteR001FreeDeckEntity>) MasterData.RouletteR001FreeDeckEntityList).Where<RouletteR001FreeDeckEntity>(new Func<RouletteR001FreeDeckEntity, bool>(rouletteMenu.\u003CInitialize\u003Eb__58_2)).ToList<RouletteR001FreeDeckEntity>();
      for (int i = 0; i < rouletteMenu.awardWheelIconContainers.Count; ++i)
      {
        foreach (Component component in rouletteMenu.awardWheelIconContainers[i].transform)
          UnityEngine.Object.Destroy((UnityEngine.Object) component.gameObject);
        rouletteMenu.awardWheelIconContainers[i].transform.Clear();
        e1 = rouletteMenu.awardWheelIconPrefab.Clone(rouletteMenu.awardWheelIconContainers[i].transform).GetComponent<RouletteAwardWheelIconController>().Init(rouletteMenu.rouletteDeckEntityList[i].reward_quantity ?? 1, rouletteMenu.rouletteDeckEntityList[i].reward_type_id, rouletteMenu.rouletteDeckEntityList[i].reward_id);
        while (e1.MoveNext())
          yield return e1.Current;
        e1 = (IEnumerator) null;
      }
      rouletteMenu.awardAngles = new List<float>(rouletteMenu.awardCount + 1);
      rouletteMenu.angleIncrement = 360f / (float) rouletteMenu.awardCount;
      for (int index = 0; index < rouletteMenu.awardCount + 1; ++index)
        rouletteMenu.awardAngles.Add((float) index * rouletteMenu.angleIncrement);
      if (!rouletteMenu.canRoulette)
      {
        rouletteMenu.awardDetailButton.isEnabled = true;
        rouletteMenu.returnButton.isEnabled = true;
        rouletteMenu.startButton.isEnabled = false;
        rouletteMenu.ottimoAnimationController.DisableAnimator();
      }
      else
      {
        rouletteMenu.SetButtonsAvailbility(false, 0.0f);
        rouletteMenu.StartCoroutine(rouletteMenu.LoadResultAwardPrefabsCoroutine());
        while (!rouletteMenu.isResultAwardPrefabsLoaded)
          yield return (object) null;
      }
    }
  }

  public void PlayOttimoAnimation()
  {
    if (!this.canRoulette)
      return;
    this.ottimoAnimationController.InitAndPlay();
  }

  private void FixedUpdate()
  {
    if (this.currentWheelStatus == RouletteMenu.WheelStatus.Stopped || this.currentWheelStatus == RouletteMenu.WheelStatus.Preparing)
      return;
    this.UpdateRotationStatus();
  }

  public void SetButtonsAvailbility(bool isEnabled, float delay = 0.0f)
  {
    if ((double) delay > 0.0)
    {
      this.StartCoroutine(this.SetButtonsAvailbilityCoroutine(isEnabled, delay));
    }
    else
    {
      this.awardDetailButton.isEnabled = isEnabled;
      this.startButton.isEnabled = isEnabled;
      this.returnButton.isEnabled = isEnabled;
    }
  }

  private IEnumerator SetButtonsAvailbilityCoroutine(bool isEnabled, float delay = 0.0f)
  {
    yield return (object) new WaitForSeconds(delay);
    this.awardDetailButton.isEnabled = isEnabled;
    this.startButton.isEnabled = isEnabled;
    this.returnButton.isEnabled = isEnabled;
  }

  public void OnClickStartButton()
  {
    if ((double) this.maxSpeed == 0.0)
    {
      Debug.LogError((object) "maxSpeed cannot be 0!");
    }
    else
    {
      if (this.currentWheelStatus != RouletteMenu.WheelStatus.Stopped)
        return;
      this.currentWheelStatus = RouletteMenu.WheelStatus.Preparing;
      this.soundManager.playSE("SE_1059", false, 0.0f, -1);
      this.SetButtonsAvailbility(false, 0.0f);
      this.StartCoroutine(this.PlayRoulette());
    }
  }

  private IEnumerator PlayRoulette()
  {
    RouletteMenu rouletteMenu = this;
    int previousLoadingMode = Singleton<CommonRoot>.GetInstance().loadingMode;
    Singleton<CommonRoot>.GetInstance().loadingMode = 2;
    Future<WebAPI.Response.RouletteR001FreePay> awardFuture = WebAPI.RouletteR001FreePay(rouletteMenu.rouletteID, (System.Action<WebAPI.Response.UserError>) (e =>
    {
      WebAPI.DefaultUserErrorCallback(e);
      Singleton<NGSceneManager>.GetInstance().backScene();
    }));
    IEnumerator e1 = awardFuture.Wait();
    while (e1.MoveNext())
      yield return e1.Current;
    e1 = (IEnumerator) null;
    if (awardFuture.HasResult && awardFuture.Result != null)
    {
      int index = 0;
      rouletteMenu.rouletteDeckEntityID = awardFuture.Result.result[index].deck_entity_id;
      Singleton<CommonRoot>.GetInstance().loadingMode = previousLoadingMode;
      // ISSUE: reference to a compiler-generated method
      rouletteMenu.resultDeckEntity = rouletteMenu.rouletteDeckEntityList.FirstOrDefault<RouletteR001FreeDeckEntity>(new Func<RouletteR001FreeDeckEntity, bool>(rouletteMenu.\u003CPlayRoulette\u003Eb__64_1));
      rouletteMenu.resultIndex = rouletteMenu.rouletteDeckEntityList.IndexOf(rouletteMenu.resultDeckEntity);
      yield return (object) rouletteMenu.StartCoroutine(rouletteMenu.InitializeCutIn(rouletteMenu.resultDeckEntity.action_pattern_id));
      rouletteMenu.RotationStart(rouletteMenu.resultIndex);
    }
  }

  private void RotationStart(int resultIndex)
  {
    this.acceleration = this.maxSpeed / this.accelerateTime;
    this.deceleration = this.maxSpeed / this.decelerateTime;
    this.accelerateAngle = this.maxSpeed * 0.5f * this.accelerateTime;
    this.decelerateAngle = this.maxSpeed * 0.5f * this.decelerateTime;
    this.minSteadyAngle = this.maxSpeed * this.minSteadyTime;
    this.originalWheelAngleZ = this.rouletteWheel.localEulerAngles.z;
    this.currentPlayTime = 0.0f;
    this.isCutInPlayed = false;
    this.currentWheelStatus = RouletteMenu.WheelStatus.Accelerate;
    float num = UnityEngine.Random.Range(this.awardAngles[resultIndex] + (float) ((double) this.angleIncrement * (double) this.accuracy * 0.5), this.awardAngles[resultIndex + 1] - (float) ((double) this.angleIncrement * (double) this.accuracy * 0.5)) - (float) (((double) this.originalWheelAngleZ + (double) this.accelerateAngle + (double) this.minSteadyAngle + (double) this.decelerateAngle) % 360.0);
    if ((double) this.maxSpeed > 0.0 && (double) num < 0.0)
      num += 360f;
    else if ((double) this.maxSpeed < 0.0 && (double) num > 0.0)
      num -= 360f;
    this.steadyTime = this.minSteadyTime + num / this.maxSpeed;
    this.steadyAngle = this.minSteadyAngle + num;
    this.awardDetailButton.gameObject.SetActive(false);
    this.startButton.gameObject.SetActive(false);
    this.soundManager.playSE("SE_1060", false, 0.0f, -1);
  }

  private void UpdateRotationStatus()
  {
    this.currentPlayTime += Time.fixedDeltaTime;
    if ((double) this.currentPlayTime - (double) this.accelerateTime - (double) this.steadyTime - (double) this.decelerateTime >= 0.0)
    {
      this.currentWheelStatus = RouletteMenu.WheelStatus.Stopped;
      this.currentWheelAngleZ = this.originalWheelAngleZ + this.accelerateAngle + this.steadyAngle + this.decelerateAngle;
      this.awardDetailButton.gameObject.SetActive(true);
      this.startButton.gameObject.SetActive(true);
      this.StartCoroutine(this.ShowResultAwardPopup());
    }
    else if ((double) this.currentPlayTime - (double) this.accelerateTime - (double) this.steadyTime >= 0.0)
    {
      this.currentWheelStatus = RouletteMenu.WheelStatus.Decerlerate;
      float num = this.currentPlayTime - this.accelerateTime - this.steadyTime;
      this.currentWheelAngleZ = (float) ((double) this.originalWheelAngleZ + (double) this.accelerateAngle + (double) this.steadyAngle + ((double) this.maxSpeed * (double) num - 0.5 * (double) this.deceleration * (double) num * (double) num));
    }
    else if ((double) this.currentPlayTime - (double) this.accelerateTime >= 0.0)
    {
      this.currentWheelStatus = RouletteMenu.WheelStatus.Steady;
      this.currentWheelAngleZ = (float) ((double) this.originalWheelAngleZ + (double) this.accelerateAngle + (double) (this.currentPlayTime - this.accelerateTime) * (double) this.maxSpeed);
    }
    else if ((double) this.currentPlayTime > 0.0)
    {
      this.currentWheelStatus = RouletteMenu.WheelStatus.Accelerate;
      this.currentWheelAngleZ = this.originalWheelAngleZ + 0.5f * this.acceleration * this.currentPlayTime * this.currentPlayTime;
    }
    this.rouletteWheel.localEulerAngles = new Vector3(0.0f, 0.0f, this.currentWheelAngleZ);
    if (this.currentBlurredWheelStatus == RouletteMenu.BlurredWheelEffectStatus.NotPlaying && (double) this.currentPlayTime >= (double) this.blurEffectStartTime && (double) this.currentPlayTime < (double) this.blurEffectEndTime)
    {
      this.blurredRouletteWheelTweenAlpha.PlayForward();
      this.clearRouletteWheelTweenAlpha.PlayForward();
      this.currentBlurredWheelStatus = RouletteMenu.BlurredWheelEffectStatus.Playing;
    }
    else if (this.currentBlurredWheelStatus == RouletteMenu.BlurredWheelEffectStatus.Playing && (double) this.currentPlayTime >= (double) this.blurEffectEndTime)
    {
      this.blurredRouletteWheelTweenAlpha.PlayReverse();
      this.clearRouletteWheelTweenAlpha.PlayReverse();
      this.currentBlurredWheelStatus = RouletteMenu.BlurredWheelEffectStatus.NotPlaying;
    }
    if (this.isCutInPlayed || (double) this.currentPlayTime < (double) this.cutInStartTime)
      return;
    this.PlayCutIn(this.resultDeckEntity.action_pattern_id);
    this.isCutInPlayed = true;
  }

  private IEnumerator LoadNecessaryPrefabsCoroutine()
  {
    RouletteMenu rouletteMenu = this;
    IEnumerator e = rouletteMenu.LoadNecessaryPrefabs();
    while (true)
    {
      object obj = (object) null;
      try
      {
        if (!e.MoveNext())
          break;
        obj = e.Current;
      }
      catch (Exception ex)
      {
        Debug.Log((object) "Catch exception: LoadNecessaryPrefabs");
        rouletteMenu.StartCoroutine(rouletteMenu.LoadNecessaryPrefabsCoroutine());
      }
      yield return obj;
    }
  }

  private IEnumerator LoadNecessaryPrefabs()
  {
    Future<GameObject> awardWheelIconPrefabF;
    IEnumerator e;
    if ((UnityEngine.Object) this.awardWheelIconPrefab == (UnityEngine.Object) null)
    {
      awardWheelIconPrefabF = new ResourceObject("Prefabs/roulette_5th/AwardContent").Load<GameObject>();
      e = awardWheelIconPrefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.awardWheelIconPrefab = awardWheelIconPrefabF.Result;
      awardWheelIconPrefabF = (Future<GameObject>) null;
    }
    if ((UnityEngine.Object) this.awardDetailPopupPrefab == (UnityEngine.Object) null)
    {
      awardWheelIconPrefabF = new ResourceObject("Prefabs/roulette_5th/popup_Roulette_Reward__anim_popup01").Load<GameObject>();
      e = awardWheelIconPrefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.awardDetailPopupPrefab = awardWheelIconPrefabF.Result;
      awardWheelIconPrefabF = (Future<GameObject>) null;
    }
    if ((UnityEngine.Object) this.awardDetailPopupItemPrefab == (UnityEngine.Object) null)
    {
      awardWheelIconPrefabF = new ResourceObject("Prefabs/roulette_5th/dir_Roulette_RewardList").Load<GameObject>();
      e = awardWheelIconPrefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.awardDetailPopupItemPrefab = awardWheelIconPrefabF.Result;
      awardWheelIconPrefabF = (Future<GameObject>) null;
    }
    this.isNecessaryPrefabsLoaded = true;
  }

  private IEnumerator LoadResultAwardPrefabsCoroutine()
  {
    RouletteMenu rouletteMenu = this;
    IEnumerator e = rouletteMenu.LoadResultAwardPrefabs();
    while (true)
    {
      object obj = (object) null;
      try
      {
        if (!e.MoveNext())
          break;
        obj = e.Current;
      }
      catch (Exception ex)
      {
        Debug.Log((object) "Catch exception: LoadResultAwardPrefabs");
        rouletteMenu.StartCoroutine(rouletteMenu.LoadResultAwardPrefabsCoroutine());
      }
      yield return obj;
    }
  }

  private IEnumerator LoadResultAwardPrefabs()
  {
    Future<GameObject> awardResultPopupPrefabF;
    IEnumerator e;
    if ((UnityEngine.Object) this.awardResultPopupPrefab == (UnityEngine.Object) null)
    {
      awardResultPopupPrefabF = new ResourceObject("Prefabs/roulette_5th/dir_Result_Roulette_Get_reward").Load<GameObject>();
      e = awardResultPopupPrefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.awardResultPopupPrefab = awardResultPopupPrefabF.Result;
      awardResultPopupPrefabF = (Future<GameObject>) null;
    }
    if ((UnityEngine.Object) this.campaignPopupPrefab == (UnityEngine.Object) null && this.shouldShowCampaign)
    {
      awardResultPopupPrefabF = new ResourceObject("Prefabs/roulette_5th/popup_Roulette_Campaign__anim_popup01").Load<GameObject>();
      e = awardResultPopupPrefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.campaignPopupPrefab = awardResultPopupPrefabF.Result;
      awardResultPopupPrefabF = (Future<GameObject>) null;
    }
    this.isResultAwardPrefabsLoaded = true;
  }

  public void ShowAwardDetailPopup()
  {
    if (this.isDisplayingAwardDetailPopup)
      return;
    this.isDisplayingAwardDetailPopup = true;
    this.StartCoroutine(this.ShowAwardDetailPopupCoroutine());
  }

  private IEnumerator ShowResultAwardPopup()
  {
    yield return (object) new WaitForSeconds(this.showResultAwardPopupDelay);
    IEnumerator e = Singleton<PopupManager>.GetInstance().open(this.awardResultPopupPrefab, false, false, false, true, false, false, "SE_1061").GetComponent<RouletteAwardResultPopupController>().Init(this.resultDeckEntity, this.shouldShowCampaign, this.campaignPopupPrefab, this.campaignURL);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  private IEnumerator ShowAwardDetailPopupCoroutine()
  {
    RouletteMenu menu = this;
    IEnumerator e = Singleton<PopupManager>.GetInstance().open(menu.awardDetailPopupPrefab, false, false, false, true, false, false, "SE_1006").GetComponent<RouletteAwardDetailPopupController>().Init(menu, menu.rouletteDeckEntityList, menu.awardDetailPopupItemPrefab);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  private IEnumerator InitializeCutIn(int cutInPattern)
  {
    if (cutInPattern != 0)
    {
      Future<GameObject> cutInEffectBigPrefabF = new ResourceObject(this.cutinEffectPrefabPaths[cutInPattern]).Load<GameObject>();
      IEnumerator e = cutInEffectBigPrefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.cutInEffectPrefab = cutInEffectBigPrefabF.Result;
      foreach (Component component in this.cutInContainer.transform)
        UnityEngine.Object.Destroy((UnityEngine.Object) component.gameObject);
      this.cutInContainer.transform.Clear();
      this.cutInContainer.SetActive(false);
      this.cutinController = this.cutInEffectPrefab.Clone(this.cutInContainer.transform).GetComponent<DuelCutin>();
      e = this.cutinController.InitializeForRoulette(cutInPattern);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
  }

  private void PlayCutIn(int cutInPattern)
  {
    if (cutInPattern == 0)
      return;
    this.cutInContainer.SetActive(true);
    this.cutinController.gameObject.SetActive(true);
    this.cutinController.PlaySkillCutInForRoulette(this.cutinEffectSoundClipNames[cutInPattern]);
    this.cutinController.CameraCutin.transform.localPosition = new Vector3(0.0f, 0.0f, 10f);
    this.cutinController.CameraCutin.GetComponent<Camera>().depth = 0.1f;
  }

  public void onBackButton()
  {
    Singleton<NGSceneManager>.GetInstance().backScene();
  }

  public enum WheelStatus
  {
    Stopped,
    Preparing,
    Accelerate,
    Steady,
    Decerlerate,
  }

  public enum BlurredWheelEffectStatus
  {
    NotPlaying,
    Playing,
  }
}
