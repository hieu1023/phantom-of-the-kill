﻿// Decompiled with JetBrains decompiler
// Type: Setting0101Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System;
using System.Collections;
using UnityEngine;

public class Setting0101Menu : BackButtonMenuBase
{
  [SerializeField]
  protected UILabel TxtTitle;
  [SerializeField]
  protected UISlider sldBgm;
  [SerializeField]
  protected UISlider sldSe;
  [SerializeField]
  protected UISlider sldVoice;
  [SerializeField]
  public GameObject[] btnHpRecoverOn;
  [SerializeField]
  public GameObject[] btnHpRecoverOff;
  [SerializeField]
  private BoxCollider bgmCollider;
  [SerializeField]
  private BoxCollider seCollider;
  [SerializeField]
  private BoxCollider voiceCollider;
  [SerializeField]
  public GameObject btnRecordOpen;
  [SerializeField]
  public GameObject btnRecordPrivate;
  [SerializeField]
  private UIButton btnNormalSound;
  [SerializeField]
  private UIButton btnHighSound;
  private bool initFlg;
  private string btnRecordOpenBaseTexName;
  private string btnRecordPrivateBaseTexName;
  private string btnRecordOffTexName;
  private bool isRecordOpen;
  private bool isRecordOpenBase;

  public float bgmVolume
  {
    get
    {
      return this.sldBgm.value;
    }
  }

  public float seVolume
  {
    get
    {
      return this.sldSe.value;
    }
  }

  public float voiceVolume
  {
    get
    {
      return this.sldVoice.value;
    }
  }

  public void Initialize()
  {
    this.sldBgm.value = Persist.volume.Data.Bgm;
    this.sldSe.value = Persist.volume.Data.Se;
    this.sldVoice.value = Persist.volume.Data.Voice;
    this.btnRecordOpenBaseTexName = this.btnRecordOpen.GetComponent<UIButton>().normalSprite;
    this.btnRecordPrivateBaseTexName = this.btnRecordPrivate.GetComponent<UIButton>().normalSprite;
    this.btnRecordOffTexName = this.btnRecordPrivate.GetComponent<UIButton>().disabledSprite;
    this.isRecordOpen = SMManager.Get<DisplayPvPHistory>().display;
    this.isRecordOpenBase = this.isRecordOpen;
    if (this.isRecordOpen)
      this.IbtnRecordOpen();
    else
      this.IbtnRecordPrivate();
    this.initFlg = true;
    if (Persist.normalDLC.Data.IsSound)
    {
      this.btnNormalSound.normalSprite = this.btnRecordOpenBaseTexName;
      this.btnHighSound.normalSprite = this.btnRecordOffTexName;
    }
    else
    {
      this.btnNormalSound.normalSprite = this.btnRecordOffTexName;
      this.btnHighSound.normalSprite = this.btnRecordOpenBaseTexName;
    }
  }

  public IEnumerator onEndSceneComm()
  {
    Singleton<CommonRoot>.GetInstance().loadingMode = 1;
    Future<WebAPI.Response.PlayerDisplayPvpHistoryEdit> result = WebAPI.PlayerDisplayPvpHistoryEdit(this.isRecordOpen, (System.Action<WebAPI.Response.UserError>) (e =>
    {
      Singleton<CommonRoot>.GetInstance().isLoading = false;
      Singleton<CommonRoot>.GetInstance().loadingMode = 0;
      WebAPI.DefaultUserErrorCallback(e);
    }));
    IEnumerator e1 = result.Wait();
    while (e1.MoveNext())
      yield return e1.Current;
    e1 = (IEnumerator) null;
    if (result.Result != null)
    {
      Singleton<CommonRoot>.GetInstance().isLoading = false;
      Singleton<CommonRoot>.GetInstance().loadingMode = 0;
    }
  }

  public IEnumerator onEndSceneAsync()
  {
    Setting0101Menu setting0101Menu = this;
    if (setting0101Menu.isRecordOpen != setting0101Menu.isRecordOpenBase)
      yield return (object) setting0101Menu.StartCoroutine(setting0101Menu.onEndSceneComm());
  }

  public override void onBackButton()
  {
    this.IbtnBack();
  }

  public virtual void IbtnBack()
  {
    if (this.IsPushAndSet())
      return;
    this.bgmCollider.enabled = false;
    this.seCollider.enabled = false;
    this.voiceCollider.enabled = false;
    Singleton<NGSceneManager>.GetInstance().backScene();
  }

  public virtual void SldBgmValueChange()
  {
    if (!this.initFlg)
      return;
    NGSoundManager instance = Singleton<NGSoundManager>.GetInstance();
    if (!((UnityEngine.Object) instance != (UnityEngine.Object) null))
      return;
    instance.getVolume().bgmVolume = this.sldBgm.value;
    instance.UpdateVolumeLevel();
  }

  public virtual void SldSeValueChange()
  {
    if (!this.initFlg)
      return;
    NGSoundManager instance = Singleton<NGSoundManager>.GetInstance();
    if (!((UnityEngine.Object) instance != (UnityEngine.Object) null))
      return;
    instance.getVolume().seVolume = this.sldSe.value;
    instance.UpdateVolumeLevel();
  }

  public virtual void SldVoiceValueChange()
  {
    if (!this.initFlg)
      return;
    NGSoundManager instance = Singleton<NGSoundManager>.GetInstance();
    if (!((UnityEngine.Object) instance != (UnityEngine.Object) null))
      return;
    instance.getVolume().voiceVolume = this.sldVoice.value;
    instance.UpdateVolumeLevel();
  }

  public virtual void IbtnNotificationChange()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<NGSceneManager>.GetInstance().changeScene("setting010_2", true, (object[]) Array.Empty<object>());
  }

  public virtual void IbtnNameChange()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<NGSceneManager>.GetInstance().changeScene("setting010_1_3", true, (object[]) Array.Empty<object>());
  }

  public virtual void IbtnCommentChange()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<NGSceneManager>.GetInstance().changeScene("setting010_1_4", true, (object[]) Array.Empty<object>());
  }

  public virtual void IbtnRecordOpen()
  {
    if (this.IsPush)
      return;
    this.btnRecordOpen.GetComponent<UIButton>().normalSprite = this.btnRecordOpenBaseTexName;
    this.btnRecordPrivate.GetComponent<UIButton>().normalSprite = this.btnRecordOffTexName;
    this.isRecordOpen = true;
  }

  public virtual void IbtnRecordPrivate()
  {
    if (this.IsPush)
      return;
    this.btnRecordOpen.GetComponent<UIButton>().normalSprite = this.btnRecordOffTexName;
    this.btnRecordPrivate.GetComponent<UIButton>().normalSprite = this.btnRecordPrivateBaseTexName;
    this.isRecordOpen = false;
  }

  public void IbtnOnNormalSound()
  {
    if (this.IsPush || Persist.normalDLC.Data.IsSound)
      return;
    this.StartCoroutine(this.ShowSoundQualityConfirmPopup(true));
  }

  public void IbtnOnHighSound()
  {
    if (this.IsPush || !Persist.normalDLC.Data.IsSound)
      return;
    this.StartCoroutine(this.ShowSoundQualityConfirmPopup(false));
  }

  private IEnumerator ShowSoundQualityConfirmPopup(bool isNormal)
  {
    Future<GameObject> f = Singleton<ResourceManager>.GetInstance().Load<GameObject>("Prefabs/popup/popup_010_AnimQualityChangeConfirm__anim_popup01", 1f);
    IEnumerator e = f.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Singleton<PopupManager>.GetInstance().open(f.Result, false, false, false, true, false, false, "SE_1006").GetComponent<SoundQualityConfirmPopup>().IsNormal = isNormal;
  }
}
