﻿// Decompiled with JetBrains decompiler
// Type: BattleUI05PopupResortie
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class BattleUI05PopupResortie : BackButtonMenuBase
{
  [SerializeField]
  private GameObject topFriend_;
  [SerializeField]
  [Tooltip("助っ人に他人を選択していた時の表示制御")]
  private FriendRequestPopupBase cntlOthers_;
  [SerializeField]
  [Tooltip("助っ人にフレンドを選択していた時の表示制御")]
  private Battle0202502Menu cntlFriend_;
  [SerializeField]
  [Tooltip("助っ人のタイトル部の先頭0:フレンド/1:他人")]
  private GameObject[] topFriendTitles_;
  [SerializeField]
  [Tooltip("助っ人用のオブジェクトの中で初期OFFにしておきたいリスト")]
  private GameObject[] friendFirstDisables_;
  [SerializeField]
  private UIButton btnFriendRequest_;
  [SerializeField]
  private GameObject topSortie_;
  [SerializeField]
  private GameObject topAP_;
  [SerializeField]
  private UILabel txtRealAP_;
  [SerializeField]
  private UILabel txtRequiredAP_;
  [SerializeField]
  private UIButton btnResortie_;
  [SerializeField]
  private UILabel txtPlayerLevel_;
  private System.Action onRequestedFriend_;
  private object quest_;
  private System.Action onNext_;

  private int consumedAP_
  {
    get
    {
      if (this.quest_ is PlayerStoryQuestS)
        return this.storyQuest_.consumed_ap;
      if (this.quest_ is PlayerExtraQuestS)
        return this.extraQuest_.consumed_ap;
      return this.quest_ is PlayerQuestSConverter ? this.charaQuest_.consumed_ap : this.seaQuest_.consumed_ap;
    }
  }

  private int? remainBattleCount_
  {
    get
    {
      if (this.quest_ is PlayerStoryQuestS)
        return this.storyQuest_.remain_battle_count;
      if (this.quest_ is PlayerExtraQuestS)
        return this.extraQuest_.remain_battle_count;
      return this.quest_ is PlayerQuestSConverter ? this.charaQuest_.remain_battle_count : this.seaQuest_.remain_battle_count;
    }
  }

  private CommonQuestType questType_
  {
    get
    {
      if (this.quest_ is PlayerStoryQuestS)
        return CommonQuestType.Story;
      if (this.quest_ is PlayerExtraQuestS)
        return CommonQuestType.Extra;
      if (!(this.quest_ is PlayerQuestSConverter))
        return CommonQuestType.Sea;
      return this.charaQuest_.questS.data_type == QuestSConverter.DataType.Character ? CommonQuestType.Character : CommonQuestType.Harmony;
    }
  }

  private PlayerStoryQuestS storyQuest_
  {
    get
    {
      return this.quest_ as PlayerStoryQuestS;
    }
  }

  private PlayerExtraQuestS extraQuest_
  {
    get
    {
      return this.quest_ as PlayerExtraQuestS;
    }
  }

  private PlayerQuestSConverter charaQuest_
  {
    get
    {
      return this.quest_ as PlayerQuestSConverter;
    }
  }

  private PlayerSeaQuestS seaQuest_
  {
    get
    {
      return this.quest_ as PlayerSeaQuestS;
    }
  }

  private bool isWait_
  {
    get
    {
      return this.IsPush || this.cntlOthers_.IsPush || this.cntlFriend_.IsPush;
    }
    set
    {
      this.IsPush = value;
      this.cntlOthers_.IsPush = value;
      this.cntlFriend_.IsPush = value;
    }
  }

  private bool isWaitAndSet()
  {
    if (this.isWait_)
      return true;
    this.isWait_ = true;
    return false;
  }

  public static IEnumerator show(
    object questS,
    PlayerHelper helper,
    int friendPoint,
    bool isFriend,
    System.Action eventNext)
  {
    switch (questS)
    {
      case null:
        if (eventNext == null)
        {
          yield break;
        }
        else
        {
          eventNext();
          yield break;
        }
      case PlayerCharacterQuestS _:
        questS = (object) new PlayerQuestSConverter(questS as PlayerCharacterQuestS);
        break;
      case PlayerHarmonyQuestS _:
        questS = (object) new PlayerQuestSConverter(questS as PlayerHarmonyQuestS);
        break;
    }
    Future<GameObject> ldPrefab = new ResourceObject("Prefabs/battle/popup_Re_sortie__anim_popup01").Load<GameObject>();
    IEnumerator e = ldPrefab.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if ((UnityEngine.Object) ldPrefab.Result == (UnityEngine.Object) null)
    {
      if (eventNext != null)
        eventNext();
    }
    else
    {
      bool toNext = false;
      bool requestedFriend = false;
      int consumedAP = 0;
      string curScene = Singleton<NGSceneManager>.GetInstance().sceneName;
      do
      {
        while (curScene != Singleton<NGSceneManager>.GetInstance().sceneName)
          yield return (object) null;
        GameObject go = Singleton<PopupManager>.GetInstance().open(ldPrefab.Result, false, false, false, true, true, true, "SE_1006");
        BattleUI05PopupResortie cntl = go.GetComponent<BattleUI05PopupResortie>();
        e = cntl.initialize(questS, helper, friendPoint, isFriend, requestedFriend, (System.Action) (() => requestedFriend = true), (System.Action) (() =>
        {
          toNext = true;
          if (eventNext == null)
            return;
          eventNext();
        }));
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        consumedAP = cntl.consumedAP_;
        Singleton<PopupManager>.GetInstance().startOpenAnime(go, false);
        while (Singleton<PopupManager>.GetInstance().isOpen)
          yield return (object) null;
        if (toNext)
        {
          yield break;
        }
        else
        {
          IEnumerator initPopup = (IEnumerator) null;
          if (!BattleUI05PopupResortie.checkResortie(consumedAP, (System.Action<IEnumerator>) (ie => initPopup = ie), (System.Action) null))
          {
            while (initPopup.MoveNext())
              yield return initPopup.Current;
            while (Singleton<PopupManager>.GetInstance().isOpen)
              yield return (object) null;
            while (Singleton<PopupManager>.GetInstance().isRunningCoroutine)
              yield return (object) null;
            while (Singleton<PopupManager>.GetInstance().isOpen)
              yield return (object) null;
            go = (GameObject) null;
            cntl = (BattleUI05PopupResortie) null;
          }
          else
            break;
        }
      }
      while (!BattleUI05PopupResortie.checkResortie(consumedAP, (System.Action<IEnumerator>) null, (System.Action) null));
      e = PopupMarshalingMenu.show(questS, (System.Action) (() => {}), (System.Action) (() =>
      {
        if (eventNext == null)
          return;
        eventNext();
      }));
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
  }

  private IEnumerator initialize(
    object questS,
    PlayerHelper helper,
    int friendPoint,
    bool isFriend,
    bool isReqestedFriend,
    System.Action eventRequestedFriend,
    System.Action eventNext)
  {
    BattleUI05PopupResortie ui05PopupResortie = this;
    ui05PopupResortie.quest_ = questS;
    ui05PopupResortie.onRequestedFriend_ = eventRequestedFriend;
    ui05PopupResortie.onNext_ = eventNext;
    if (helper != null)
    {
      ui05PopupResortie.topFriend_.SetActive(true);
      ((IEnumerable<GameObject>) ui05PopupResortie.topFriendTitles_).ToggleOnceEx(isFriend ? 0 : 1);
      int index = !helper.is_friend ? (!helper.is_guild_member ? 0 : 2) : 1;
      ((IEnumerable<GameObject>) ui05PopupResortie.friendFirstDisables_).ToggleOnceEx(index);
      ui05PopupResortie.txtPlayerLevel_.SetTextLocalize(helper.level);
      IEnumerator e;
      if (isFriend)
      {
        ui05PopupResortie.btnFriendRequest_.gameObject.SetActive(false);
        ui05PopupResortie.cntlOthers_.enabled = false;
        e = ui05PopupResortie.cntlFriend_.Init(helper, friendPoint);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
      }
      else
      {
        ui05PopupResortie.cntlFriend_.enabled = false;
        ui05PopupResortie.cntlOthers_.SetCallback(new System.Action(ui05PopupResortie.onClickedFriendRequest));
        if (isReqestedFriend)
          ui05PopupResortie.setDisableFriendRequest();
        e = ui05PopupResortie.cntlOthers_.Init(helper, friendPoint);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
      }
    }
    else
      ui05PopupResortie.topFriend_.SetActive(false);
    int? remainBattleCount = ui05PopupResortie.remainBattleCount_;
    bool flag = !remainBattleCount.HasValue || remainBattleCount.Value > 1;
    if (flag)
    {
      switch (ui05PopupResortie.questType_)
      {
        case CommonQuestType.Story:
          PlayerStoryQuestS quest1 = ui05PopupResortie.storyQuest_;
          if (quest1.quest_story_s.story_only)
          {
            flag = false;
            break;
          }
          if (((IEnumerable<PlayerStoryQuestS>) SMManager.Get<PlayerStoryQuestS[]>()).FirstOrDefault<PlayerStoryQuestS>((Func<PlayerStoryQuestS, bool>) (x => x._quest_story_s == quest1._quest_story_s)) == null)
          {
            flag = false;
            break;
          }
          if (quest1.end_at.HasValue && ServerTime.NowAppTimeAddDelta() > quest1.end_at.Value)
          {
            flag = false;
            break;
          }
          break;
        case CommonQuestType.Character:
          PlayerQuestSConverter quest2 = ui05PopupResortie.charaQuest_;
          if (quest2.questS.story_only)
          {
            flag = false;
            break;
          }
          if (((IEnumerable<PlayerCharacterQuestS>) SMManager.Get<PlayerCharacterQuestS[]>()).FirstOrDefault<PlayerCharacterQuestS>((Func<PlayerCharacterQuestS, bool>) (x => x._quest_character_s == quest2._quest_s_id)) == null)
          {
            flag = false;
            break;
          }
          break;
        case CommonQuestType.Extra:
          PlayerExtraQuestS quest3 = ui05PopupResortie.extraQuest_;
          if (quest3.quest_extra_s.story_only)
          {
            flag = false;
            break;
          }
          if (((IEnumerable<PlayerExtraQuestS>) SMManager.Get<PlayerExtraQuestS[]>()).FirstOrDefault<PlayerExtraQuestS>((Func<PlayerExtraQuestS, bool>) (x => x._quest_extra_s == quest3._quest_extra_s)) == null)
          {
            flag = false;
            break;
          }
          if (quest3.extra_quest_area == 2)
          {
            PlayerQuestGate playerQuestGate = quest3.GetPlayerQuestGate();
            if (playerQuestGate != null)
            {
              DateTime dateTime = ServerTime.NowAppTimeAddDelta();
              DateTime? endAt = playerQuestGate.end_at;
              if ((endAt.HasValue ? (dateTime > endAt.GetValueOrDefault() ? 1 : 0) : 0) == 0 && playerQuestGate.in_progress)
                goto label_29;
            }
            flag = false;
            break;
          }
label_29:
          if (ServerTime.NowAppTimeAddDelta() > quest3.today_day_end_at)
          {
            flag = false;
            break;
          }
          break;
        case CommonQuestType.Harmony:
          PlayerQuestSConverter quest4 = ui05PopupResortie.charaQuest_;
          if (quest4.questS.story_only)
          {
            flag = false;
            break;
          }
          if (((IEnumerable<PlayerHarmonyQuestS>) SMManager.Get<PlayerHarmonyQuestS[]>()).FirstOrDefault<PlayerHarmonyQuestS>((Func<PlayerHarmonyQuestS, bool>) (x => x._quest_harmony_s == quest4._quest_s_id)) == null)
          {
            flag = false;
            break;
          }
          break;
        case CommonQuestType.Sea:
          PlayerSeaQuestS quest5 = ui05PopupResortie.seaQuest_;
          if (quest5.quest_sea_s.story_only)
          {
            flag = false;
            break;
          }
          if (((IEnumerable<PlayerSeaQuestS>) SMManager.Get<PlayerSeaQuestS[]>()).FirstOrDefault<PlayerSeaQuestS>((Func<PlayerSeaQuestS, bool>) (x => x._quest_sea_s == quest5._quest_sea_s)) == null)
          {
            flag = false;
            break;
          }
          if (quest5.end_at.HasValue && ServerTime.NowAppTimeAddDelta() > quest5.end_at.Value)
          {
            flag = false;
            break;
          }
          break;
      }
    }
    ui05PopupResortie.topAP_.SetActive(flag);
    Player player = SMManager.Get<Player>();
    int num = player.ap + player.ap_overflow;
    if (flag)
    {
      Hashtable hashtable = new Hashtable()
      {
        {
          (object) "now",
          (object) num
        },
        {
          (object) "max",
          (object) player.ap_max
        }
      };
      Consts instance = Consts.GetInstance();
      ui05PopupResortie.txtRealAP_.SetTextLocalize(Consts.Format(num > player.ap_max ? instance.OVERFLOW_AP_NOW_MAX : instance.VALUE_AP_NOW_MAX, (IDictionary) hashtable));
      ui05PopupResortie.txtRequiredAP_.SetTextLocalize(ui05PopupResortie.consumedAP_);
    }
    ui05PopupResortie.btnResortie_.isEnabled = flag;
  }

  private void onClickedFriendRequest()
  {
    this.setDisableFriendRequest();
    this.cntlOthers_.IsPush = false;
    if (this.onRequestedFriend_ == null)
      return;
    this.onRequestedFriend_();
  }

  private void setDisableFriendRequest()
  {
    this.btnFriendRequest_.gameObject.SetActive(false);
    ((IEnumerable<GameObject>) this.topFriendTitles_).ToggleOnceEx(-1);
  }

  public override void onBackButton()
  {
    this.showBackKeyToast();
  }

  public void onClickedResortie()
  {
    if (this.isWaitAndSet())
      return;
    Singleton<PopupManager>.GetInstance().dismiss(false);
  }

  public void onClickedNext()
  {
    if (this.isWaitAndSet())
      return;
    Singleton<PopupManager>.GetInstance().dismiss(false);
    if (this.onNext_ == null)
      return;
    this.onNext_();
  }

  private static bool checkResortie(
    int consumedAP,
    System.Action<IEnumerator> onStartPopup = null,
    System.Action onPopupAPCompleted = null)
  {
    Player player = SMManager.Get<Player>();
    if (player.CheckMaxUnit())
    {
      if (onStartPopup != null)
        onStartPopup(BattleUI05PopupResortie.popupMaxUnit());
      return false;
    }
    if (player.CheckMaxItem())
    {
      if (onStartPopup != null)
        onStartPopup(BattleUI05PopupResortie.popupMaxGear());
      return false;
    }
    if (player.CheckMaxReisou())
    {
      if (onStartPopup != null)
        onStartPopup(BattleUI05PopupResortie.popupMaxReisou());
      return false;
    }
    if (player.ap >= consumedAP)
      return true;
    if (onStartPopup != null)
      onStartPopup(BattleUI05PopupResortie.popupShortageAP(onPopupAPCompleted));
    return false;
  }

  private static IEnumerator popupMaxUnit()
  {
    IEnumerator e = PopupUtility._999_5_1();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    yield return (object) new WaitForSeconds(0.5f);
  }

  private static IEnumerator popupMaxGear()
  {
    IEnumerator e = PopupUtility._999_6_1(true);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    yield return (object) new WaitForSeconds(0.5f);
  }

  private static IEnumerator popupMaxReisou()
  {
    IEnumerator e = PopupUtility.popupMaxReisou();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    yield return (object) new WaitForSeconds(0.5f);
  }

  private static IEnumerator popupShortageAP(System.Action onCompleted)
  {
    Future<GameObject> ldPrefab = Res.Prefabs.popup.popup_002_7__anim_popup01.Load<GameObject>();
    IEnumerator e = ldPrefab.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Singleton<PopupManager>.GetInstance().open(ldPrefab.Result, false, false, false, true, false, false, "SE_1006").GetComponent<ApPopup0027>().SetBtnAct(onCompleted);
    yield return (object) new WaitForSeconds(0.5f);
  }

  private enum FriendState
  {
    Friend,
    Other,
  }
}
