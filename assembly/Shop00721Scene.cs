﻿// Decompiled with JetBrains decompiler
// Type: Shop00721Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Shop00721Scene : NGSceneBase
{
  private bool isInit = true;
  [SerializeField]
  private Shop00721Menu menu;

  public override IEnumerator onInitSceneAsync()
  {
    Shop00721Scene shop00721Scene = this;
    Future<GameObject> bgF = Res.Prefabs.BackGround.ShopBackground.Load<GameObject>();
    IEnumerator e = bgF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    shop00721Scene.backgroundPrefab = bgF.Result;
  }

  public static void changeScene(bool stack, bool isUpdate)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("shop007_21", (stack ? 1 : 0) != 0, (object) isUpdate);
  }

  public IEnumerator onStartSceneAsync(bool isUpdate)
  {
    Shop00721Scene shop00721Scene = this;
    IEnumerator e1;
    if (!WebAPI.IsResponsedAtRecent("ShopStatus", 60.0) & isUpdate && shop00721Scene.isInit)
    {
      Singleton<CommonRoot>.GetInstance().isLoading = true;
      Future<WebAPI.Response.ShopStatus> shoplistF = WebAPI.ShopStatus((System.Action<WebAPI.Response.UserError>) (e =>
      {
        WebAPI.DefaultUserErrorCallback(e);
        Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
      })).Then<WebAPI.Response.ShopStatus>((Func<WebAPI.Response.ShopStatus, WebAPI.Response.ShopStatus>) (result =>
      {
        Singleton<NGGameDataManager>.GetInstance().Parse(result);
        return result;
      }));
      e1 = shoplistF.Wait();
      while (e1.MoveNext())
        yield return e1.Current;
      e1 = (IEnumerator) null;
      WebAPI.Response.ShopStatus result1 = shoplistF.Result;
      if (result1 == null)
        yield break;
      else if (((IEnumerable<Shop>) ((IEnumerable<Shop>) result1.shops).Where<Shop>((Func<Shop, bool>) (x => x.id == 5000)).ToArray<Shop>()).Count<Shop>() == 0 || result1.limited_shop_banner_url == "")
      {
        Shop0071Scene.changeScene(false, result1, true);
        yield break;
      }
      else
        shoplistF = (Future<WebAPI.Response.ShopStatus>) null;
    }
    e1 = shop00721Scene.menu.Init();
    while (e1.MoveNext())
      yield return e1.Current;
    e1 = (IEnumerator) null;
    shop00721Scene.isInit = false;
    try
    {
      Persist.lastAccessTime.Data.limitedShopLastAccessTime = DateTime.Now;
      Persist.lastAccessTime.Flush();
    }
    catch (Exception ex)
    {
      Persist.lastAccessTime.Delete();
    }
    shop00721Scene.StartCoroutine(Singleton<CommonRoot>.GetInstance().UpdateFooterLimitedShopButton());
    Singleton<CommonRoot>.GetInstance().isLoading = false;
  }
}
