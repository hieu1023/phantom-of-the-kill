﻿// Decompiled with JetBrains decompiler
// Type: Setting01014Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

public class Setting01014Scene : NGSceneBase
{
  [SerializeField]
  private Setting01014Menu menu;

  public override IEnumerator onInitSceneAsync()
  {
    base.onInitSceneAsync();
    this.menu.Initialize();
    yield break;
  }
}
