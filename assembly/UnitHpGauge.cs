﻿// Decompiled with JetBrains decompiler
// Type: UnitHpGauge
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class UnitHpGauge : MonoBehaviour
{
  [SerializeField]
  private NGTweenGaugeScale hpGauge;
  [SerializeField]
  private GameObject slc_dropout;

  public NGTweenGaugeScale TweenHpGauge
  {
    get
    {
      return this.hpGauge;
    }
  }

  public bool Dropout
  {
    set
    {
      this.slc_dropout.SetActive(value);
    }
    get
    {
      return this.slc_dropout.activeSelf;
    }
  }

  public void SetGaugeAndDropoutIcon(int n, int max, bool doTween = true)
  {
    this.hpGauge.setValue(n, max, doTween, -1f, -1f);
    this.Dropout = (double) n <= 0.0;
  }
}
