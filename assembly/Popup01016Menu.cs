﻿// Decompiled with JetBrains decompiler
// Type: Popup01016Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;

public class Popup01016Menu : BackButtonMenuBase
{
  public UILabel TextDescription;
  private Setting01013Menu menu01013;

  public IEnumerator Init(Setting01013Menu menu, string name)
  {
    this.menu01013 = menu;
    this.TextDescription.SetText(Consts.Format(Consts.GetInstance().POPUP_01016_DESCRIPTION, (IDictionary) new Hashtable()
    {
      {
        (object) nameof (name),
        (object) name
      }
    }));
    yield break;
  }

  public virtual void IbtnOk()
  {
    this.menu01013.Initialize();
    Singleton<PopupManager>.GetInstance().onDismiss();
  }

  public void IbtnNo()
  {
    this.IbtnOk();
  }

  public override void onBackButton()
  {
    this.IbtnNo();
  }
}
