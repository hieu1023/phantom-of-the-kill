﻿// Decompiled with JetBrains decompiler
// Type: Bugu0059Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Bugu0059Menu : BackButtonMenuBase
{
  private static readonly string COLOR_TAG_GREEN = "[00ff00]{0}[-]";
  private static readonly string COLOR_TAG_RED = "[ff0000]{0}[-]";
  private static readonly string COLOR_TAG_YELLOW = "[ffff00]{0}[-]";
  private static string[] spriteNameTBL = new string[7]
  {
    "Prefabs/ItemIcon/Materials/s_star1",
    "Prefabs/ItemIcon/Materials/s_star2",
    "Prefabs/ItemIcon/Materials/s_star3",
    "Prefabs/ItemIcon/Materials/s_star4",
    "Prefabs/ItemIcon/Materials/s_star5",
    "Prefabs/ItemIcon/Materials/s_star6",
    "Prefabs/ItemIcon/Materials/s_star7"
  };
  private List<InventoryItem> m_Materials = new List<InventoryItem>();
  [SerializeField]
  private UILabel m_NameLabel;
  [SerializeField]
  private UI2DSprite m_RaritySprite;
  [SerializeField]
  private GearKindIcon m_GearTypeIcon;
  [SerializeField]
  private UI2DSprite m_ThumSprite;
  [SerializeField]
  private UILabel m_RankLabel;
  [SerializeField]
  private UILabel m_NextRankToLabel;
  [SerializeField]
  private GameObject m_GaugeBeforeNextRankToObject;
  [SerializeField]
  private GameObject m_GaugeAfterNextRankToObject;
  [SerializeField]
  private List<UIWidget> m_SkillIconParentList;
  [SerializeField]
  private List<GameObject> m_SkillEvolutionObjectList;
  [SerializeField]
  private List<GameObject> m_SkillAcquireObjectList;
  private List<BattleSkillIcon> m_SkillIconPrefabList;
  [SerializeField]
  private Bugu0059Menu.Status m_AtkStatus;
  [SerializeField]
  private Bugu0059Menu.Status m_MatkStatus;
  [SerializeField]
  private Bugu0059Menu.Status m_DefStatus;
  [SerializeField]
  private Bugu0059Menu.Status m_MdefStatus;
  [SerializeField]
  private Bugu0059Menu.Status m_HitStatus;
  [SerializeField]
  private Bugu0059Menu.Status m_CrtStatus;
  [SerializeField]
  private Bugu0059Menu.Status m_EvaStatus;
  [SerializeField]
  private List<Bugu0059Menu.MaterialIcon> m_MaterialIconList;
  [SerializeField]
  private SpreadColorButton m_DrillingBtn;
  [SerializeField]
  private UILabel m_ZenyLabel;
  [SerializeField]
  protected GameObject DirReisou;
  [SerializeField]
  protected UILabel TxtReisouRank;
  [SerializeField]
  private UILabel TxtReisouNextRank;
  [SerializeField]
  protected GameObject DirReisouExpGauge;
  [SerializeField]
  protected UISprite SlcReisouGauge;
  [SerializeField]
  protected UISprite SlcReisouGaugeAfter;
  [SerializeField]
  protected GameObject DynReisouIcon;
  protected GameCore.ItemInfo reisouInfo;
  protected GameCore.ItemInfo reisouInfoAfter;
  protected ItemIcon reisouIcon;
  protected GameObject reisouPopupPrefab;
  private GameObject m_ItemIconPrefab;
  private GameObject m_SkillIconPrefab;
  private GameObject m_CheckMaterialPopupPrefab;
  private GameObject m_CheckMaterialPopupPrefabMini;
  private const int CheckMaterialPopupMiniIconNum = 5;
  private GameCore.ItemInfo m_Base;
  private GameCore.ItemInfo m_After;
  private Bugu0059Scene m_ParentScene;

  public IEnumerator onInitAsync()
  {
    Future<GameObject> ItemIconF = Res.Prefabs.ItemIcon.prefab.Load<GameObject>();
    IEnumerator e = ItemIconF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.m_ItemIconPrefab = ItemIconF.Result;
    Future<GameObject> SkillIconF = Res.Prefabs.BattleSkillIcon._battleSkillIcon.Load<GameObject>();
    e = SkillIconF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.m_SkillIconPrefab = SkillIconF.Result;
    this.m_SkillIconPrefabList = new List<BattleSkillIcon>();
    for (int index = 0; index < this.m_SkillIconParentList.Count; ++index)
      this.m_SkillIconPrefabList.Add((BattleSkillIcon) null);
    Future<GameObject> popupPrefabF = Res.Prefabs.popup.popup_005_9_1__anim_popup01.Load<GameObject>();
    e = popupPrefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.m_CheckMaterialPopupPrefab = popupPrefabF.Result;
    Future<GameObject> popupPrefabMiniF = Res.Prefabs.popup.popup_005_9_2__anim_popup01.Load<GameObject>();
    e = popupPrefabMiniF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.m_CheckMaterialPopupPrefabMini = popupPrefabMiniF.Result;
  }

  public IEnumerator onStartAsync(
    GameCore.ItemInfo baseGear,
    List<InventoryItem> materials,
    WebAPI.Response.ItemGearDrillingConfirm response,
    Bugu0059Scene scene,
    bool canDriling = true)
  {
    this.m_ParentScene = scene;
    this.m_Base = baseGear;
    Future<UnityEngine.Sprite> basicSpriteF = baseGear.gear.LoadSpriteBasic(1f);
    IEnumerator e = basicSpriteF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.m_ThumSprite.sprite2D = basicSpriteF.Result;
    this.m_NameLabel.SetTextLocalize(baseGear.Name());
    this.SetRarity(baseGear.gear, this.m_RaritySprite);
    float x = baseGear.gearExp + baseGear.gearExpNext > 0 ? (float) baseGear.gearExp / (float) (baseGear.gearExp + baseGear.gearExpNext) : 0.0f;
    if ((double) x != 0.0)
    {
      this.m_GaugeBeforeNextRankToObject.SetActive(true);
      this.m_GaugeBeforeNextRankToObject.transform.localScale = new Vector3(x, 1f, 1f);
    }
    else
      this.m_GaugeBeforeNextRankToObject.SetActive(false);
    this.reisouInfoAfter = (GameCore.ItemInfo) null;
    this.reisouInfo = (GameCore.ItemInfo) null;
    if (response == null)
    {
      e = this.SetNoResponse(baseGear);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    else
    {
      this.m_After = new GameCore.ItemInfo(response.player_item);
      if (response.player_reisou_item != (PlayerItem) null)
        this.reisouInfoAfter = new GameCore.ItemInfo(response.player_reisou_item);
      e = this.SetResponse(baseGear, response);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    if (baseGear.reisou != (PlayerItem) null)
      this.reisouInfo = new GameCore.ItemInfo(baseGear.reisou);
    if (this.reisouInfoAfter != null)
      yield return (object) this.SetReisouInfo(this.reisouInfoAfter);
    else
      yield return (object) this.SetReisouInfo(this.reisouInfo);
    this.m_Materials.Clear();
    if (materials != null)
    {
      this.m_Materials = materials;
      int limitRest = this.m_Base.playerItem.gear_level_limit_max - this.m_Base.playerItem.gear_level_limit;
      for (int i = 0; i < this.m_MaterialIconList.Count; ++i)
      {
        if (i < this.m_Materials.Count)
        {
          InventoryItem material = this.m_Materials[i];
          if (material != null)
          {
            if ((UnityEngine.Object) this.m_MaterialIconList[i].m_ItemIcon == (UnityEngine.Object) null)
              this.m_MaterialIconList[i].m_ItemIcon = this.m_ItemIconPrefab.CloneAndGetComponent<ItemIcon>(this.m_MaterialIconList[i].m_Parent.transform);
            e = this.m_MaterialIconList[i].m_ItemIcon.InitByItemInfo(material.Item);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            this.m_MaterialIconList[i].m_ItemIcon.gameObject.SetActive(true);
            if (limitRest > 0 && GearGear.CanSpecialDrill(this.m_Base.gear, material.Item.gear))
            {
              this.m_MaterialIconList[i].m_ItemIcon.MaxUpMark = true;
              --limitRest;
            }
            else
              this.m_MaterialIconList[i].m_ItemIcon.MaxUpMark = false;
            this.m_MaterialIconList[i].m_AddMaterialObject.SetActive(false);
          }
          material = (InventoryItem) null;
        }
        else
        {
          if ((UnityEngine.Object) this.m_MaterialIconList[i].m_ItemIcon != (UnityEngine.Object) null)
            this.m_MaterialIconList[i].m_ItemIcon.gameObject.SetActive(false);
          this.m_MaterialIconList[i].m_AddMaterialObject.SetActive(true);
        }
      }
    }
    else
    {
      for (int index = 0; index < this.m_MaterialIconList.Count; ++index)
      {
        if ((UnityEngine.Object) this.m_MaterialIconList[index].m_ItemIcon != (UnityEngine.Object) null)
          this.m_MaterialIconList[index].m_ItemIcon.gameObject.SetActive(false);
        this.m_MaterialIconList[index].m_AddMaterialObject.SetActive(true);
      }
    }
    this.m_DrillingBtn.isEnabled = ((materials == null ? 0 : (materials.Count > 0 ? 1 : 0)) & (canDriling ? 1 : 0)) != 0;
  }

  private IEnumerator SetReisouInfo(GameCore.ItemInfo dispReisouInfo)
  {
    Bugu0059Menu bugu0059Menu = this;
    if (Singleton<NGGameDataManager>.GetInstance().IsEarth)
      bugu0059Menu.DirReisou.SetActive(false);
    else if (dispReisouInfo == null)
    {
      bugu0059Menu.TxtReisouRank.gameObject.SetActive(false);
      bugu0059Menu.TxtReisouNextRank.gameObject.SetActive(false);
      bugu0059Menu.DirReisouExpGauge.SetActive(false);
    }
    else
    {
      string str = dispReisouInfo.gearLevel.ToString();
      if (dispReisouInfo.gearLevel > bugu0059Menu.reisouInfo.gearLevel)
        str = Bugu0059Menu.COLOR_TAG_GREEN.F((object) str);
      bugu0059Menu.TxtReisouRank.SetTextLocalize(Consts.GetInstance().UNIT_00443_REISOU_RANK.F((object) str, (object) bugu0059Menu.reisouInfo.gearLevelLimit));
      if (dispReisouInfo.gearLevel > bugu0059Menu.reisouInfo.gearLevel)
        bugu0059Menu.TxtReisouNextRank.SetTextLocalize(Consts.Format(Consts.GetInstance().BUGU_0059_REMAIN, (IDictionary) new Hashtable()
        {
          {
            (object) "remain",
            (object) 0
          }
        }));
      else
        bugu0059Menu.TxtReisouNextRank.SetTextLocalize(Consts.Format(Consts.GetInstance().BUGU_0059_REMAIN, (IDictionary) new Hashtable()
        {
          {
            (object) "remain",
            (object) dispReisouInfo.gearExpNext
          }
        }));
      foreach (Component component in bugu0059Menu.DynReisouIcon.transform)
        UnityEngine.Object.Destroy((UnityEngine.Object) component.gameObject);
      if ((UnityEngine.Object) bugu0059Menu.reisouIcon != (UnityEngine.Object) null)
        UnityEngine.Object.Destroy((UnityEngine.Object) bugu0059Menu.reisouIcon);
      bugu0059Menu.reisouIcon = bugu0059Menu.m_ItemIconPrefab.CloneAndGetComponent<ItemIcon>(bugu0059Menu.DynReisouIcon.transform);
      IEnumerator e = bugu0059Menu.reisouIcon.InitByItemInfo(bugu0059Menu.reisouInfo);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      bugu0059Menu.reisouIcon.setEquipReisouDisp();
      // ISSUE: reference to a compiler-generated method
      bugu0059Menu.reisouIcon.onClick = new System.Action<ItemIcon>(bugu0059Menu.\u003CSetReisouInfo\u003Eb__46_0);
      bugu0059Menu.reisouIcon.EnableLongPressEvent(new System.Action<GameCore.ItemInfo>(bugu0059Menu.SelectReisou));
      int width1 = bugu0059Menu.SlcReisouGauge.width;
      float x1 = (float) bugu0059Menu.reisouInfo.gearExp / (float) (bugu0059Menu.reisouInfo.gearExp + bugu0059Menu.reisouInfo.gearExpNext);
      if ((double) x1 == 0.0 || bugu0059Menu.reisouInfo.gearExp + bugu0059Menu.reisouInfo.gearExpNext == 0)
      {
        bugu0059Menu.SlcReisouGauge.gameObject.SetActive(false);
      }
      else
      {
        bugu0059Menu.SlcReisouGauge.gameObject.SetActive(true);
        if ((double) x1 > 1.0)
          x1 = 1f;
        bugu0059Menu.SlcReisouGauge.transform.localScale = new Vector3(x1, 1f, 1f);
      }
      if (dispReisouInfo.gearExp > bugu0059Menu.reisouInfo.gearExp || dispReisouInfo.gearLevel > bugu0059Menu.reisouInfo.gearLevel)
      {
        int width2 = bugu0059Menu.SlcReisouGaugeAfter.width;
        float x2 = (float) dispReisouInfo.gearExp / (float) (dispReisouInfo.gearExp + dispReisouInfo.gearExpNext);
        if (dispReisouInfo.gearLevel > bugu0059Menu.reisouInfo.gearLevel)
          x2 = 1f;
        if ((double) x2 == 0.0)
        {
          bugu0059Menu.SlcReisouGaugeAfter.gameObject.SetActive(false);
        }
        else
        {
          bugu0059Menu.SlcReisouGaugeAfter.gameObject.SetActive(true);
          bugu0059Menu.SlcReisouGaugeAfter.transform.localScale = new Vector3(x2, 1f, 1f);
        }
      }
      else
        bugu0059Menu.SlcReisouGaugeAfter.gameObject.SetActive(false);
    }
  }

  private IEnumerator SetNoResponse(GameCore.ItemInfo baseGear)
  {
    Consts instance = Consts.GetInstance();
    this.m_GearTypeIcon.Init(baseGear.gear.kind, baseGear.GetElement());
    this.m_RankLabel.SetTextLocalize(Consts.Format(instance.BUGU_0059_RANK, (IDictionary) new Hashtable()
    {
      {
        (object) "now",
        (object) baseGear.gearLevel
      },
      {
        (object) "max",
        (object) baseGear.gearLevelLimit
      }
    }));
    this.m_NextRankToLabel.SetTextLocalize(Consts.Format(instance.BUGU_0059_REMAIN, (IDictionary) new Hashtable()
    {
      {
        (object) "remain",
        (object) baseGear.gearExpNext
      }
    }));
    this.m_GaugeAfterNextRankToObject.SetActive(false);
    this.m_SkillEvolutionObjectList.ForEach((System.Action<GameObject>) (x => x.SetActive(false)));
    this.m_SkillAcquireObjectList.ForEach((System.Action<GameObject>) (x => x.SetActive(false)));
    Judgement.GearParameter gearParameter = Judgement.GearParameter.FromPlayerGear(baseGear);
    if (baseGear.gear.attack_type == GearAttackType.magic)
    {
      this.SetStatus(this.m_AtkStatus, 0, 0);
      this.SetStatus(this.m_MatkStatus, gearParameter.Power, 0);
    }
    else
    {
      this.SetStatus(this.m_AtkStatus, gearParameter.Power, 0);
      this.SetStatus(this.m_MatkStatus, 0, 0);
    }
    this.SetStatus(this.m_DefStatus, gearParameter.PhysicalDefense, 0);
    this.SetStatus(this.m_MdefStatus, gearParameter.MagicDefense, 0);
    this.SetStatus(this.m_HitStatus, gearParameter.Hit, 0);
    this.SetStatus(this.m_CrtStatus, gearParameter.Critical, 0);
    this.SetStatus(this.m_EvaStatus, gearParameter.Evasion, 0);
    this.m_ZenyLabel.SetTextLocalize(0);
    for (int i = 0; i < this.m_SkillIconParentList.Count; ++i)
    {
      if (i < this.m_Base.skills.Length)
      {
        this.m_SkillIconParentList[i].gameObject.SetActive(true);
        if ((UnityEngine.Object) this.m_SkillIconPrefabList[i] == (UnityEngine.Object) null)
        {
          BattleSkillIcon component = this.m_SkillIconPrefab.CloneAndGetComponent<BattleSkillIcon>(this.m_SkillIconParentList[i].transform);
          this.m_SkillIconPrefabList[i] = component;
        }
        IEnumerator e = this.m_SkillIconPrefabList[i].Init(this.m_Base.skills[i].skill);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        this.m_SkillIconPrefabList[i].SetDepth(this.m_SkillIconParentList[i].depth);
      }
      else if ((UnityEngine.Object) this.m_SkillIconPrefabList[i] != (UnityEngine.Object) null)
        this.m_SkillIconPrefabList[i].gameObject.SetActive(false);
    }
  }

  private IEnumerator SetResponse(
    GameCore.ItemInfo baseGear,
    WebAPI.Response.ItemGearDrillingConfirm response)
  {
    Consts instance = Consts.GetInstance();
    PlayerItem drillingGear = response.player_item;
    this.m_GearTypeIcon.Init(drillingGear.gear.kind, drillingGear.GetElement());
    string str1 = drillingGear.gear_level.ToString();
    if (drillingGear.gear_level > baseGear.gearLevel)
      str1 = Bugu0059Menu.COLOR_TAG_GREEN.F((object) str1);
    if (drillingGear.gear_level < baseGear.gearLevel)
      str1 = Bugu0059Menu.COLOR_TAG_RED.F((object) str1);
    string str2 = drillingGear.gear_level_limit.ToString();
    if (drillingGear.gear_level_limit > baseGear.gearLevelLimit)
      str2 = Bugu0059Menu.COLOR_TAG_GREEN.F((object) str2);
    if (drillingGear.gear_level_limit < baseGear.gearLevelLimit)
      str2 = Bugu0059Menu.COLOR_TAG_RED.F((object) str2);
    this.m_RankLabel.SetTextLocalize(Consts.Format(instance.BUGU_0059_RANK, (IDictionary) new Hashtable()
    {
      {
        (object) "now",
        (object) str1
      },
      {
        (object) "max",
        (object) str2
      }
    }));
    this.m_GaugeAfterNextRankToObject.SetActive(true);
    if (drillingGear.gear_level > baseGear.gearLevel)
    {
      this.m_NextRankToLabel.SetTextLocalize(Consts.Format(instance.BUGU_0059_REMAIN, (IDictionary) new Hashtable()
      {
        {
          (object) "remain",
          (object) 0
        }
      }));
      this.m_GaugeAfterNextRankToObject.transform.localScale = Vector3.one;
    }
    else
    {
      this.m_NextRankToLabel.SetTextLocalize(Consts.Format(instance.BUGU_0059_REMAIN, (IDictionary) new Hashtable()
      {
        {
          (object) "remain",
          (object) drillingGear.gear_exp_next
        }
      }));
      this.m_GaugeAfterNextRankToObject.transform.localScale = new Vector3((float) drillingGear.gear_exp / (float) (drillingGear.gear_exp + drillingGear.gear_exp_next), 1f, 1f);
    }
    Judgement.GearParameter gearParameter1 = Judgement.GearParameter.FromPlayerGear(drillingGear);
    Judgement.GearParameter gearParameter2 = Judgement.GearParameter.FromPlayerGear(baseGear);
    if (baseGear.gear.attack_type == GearAttackType.magic)
    {
      this.SetStatus(this.m_AtkStatus, 0, 0);
      this.SetStatus(this.m_MatkStatus, gearParameter1.Power, gearParameter1.Power - gearParameter2.Power);
    }
    else
    {
      this.SetStatus(this.m_AtkStatus, gearParameter1.Power, gearParameter1.Power - gearParameter2.Power);
      this.SetStatus(this.m_MatkStatus, 0, 0);
    }
    this.SetStatus(this.m_DefStatus, gearParameter1.PhysicalDefense, gearParameter1.PhysicalDefense - gearParameter2.PhysicalDefense);
    this.SetStatus(this.m_MdefStatus, gearParameter1.MagicDefense, gearParameter1.MagicDefense - gearParameter2.MagicDefense);
    this.SetStatus(this.m_HitStatus, gearParameter1.Hit, gearParameter1.Hit - gearParameter2.Hit);
    this.SetStatus(this.m_CrtStatus, gearParameter1.Critical, gearParameter1.Critical - gearParameter2.Critical);
    this.SetStatus(this.m_EvaStatus, gearParameter1.Evasion, gearParameter1.Evasion - gearParameter2.Evasion);
    this.m_ZenyLabel.SetTextLocalize(response.consume_money);
    this.m_SkillEvolutionObjectList.ForEach((System.Action<GameObject>) (x => x.SetActive(false)));
    this.m_SkillAcquireObjectList.ForEach((System.Action<GameObject>) (x => x.SetActive(false)));
    for (int i = 0; i < this.m_SkillIconParentList.Count; ++i)
    {
      if (i < drillingGear.skills.Length)
      {
        this.m_SkillIconParentList[i].gameObject.SetActive(true);
        if ((UnityEngine.Object) this.m_SkillIconPrefabList[i] == (UnityEngine.Object) null)
        {
          BattleSkillIcon component = this.m_SkillIconPrefab.CloneAndGetComponent<BattleSkillIcon>(this.m_SkillIconParentList[i].transform);
          this.m_SkillIconPrefabList[i] = component;
        }
        this.m_SkillIconPrefabList[i].gameObject.SetActive(true);
        IEnumerator e = this.m_SkillIconPrefabList[i].Init(drillingGear.skills[i].skill);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        this.m_SkillIconPrefabList[i].SetDepth(this.m_SkillIconParentList[i].depth);
        if (this.m_Base.skills.Length <= i)
          this.m_SkillAcquireObjectList[i].SetActive(true);
        else if (drillingGear.skills[i].ID != this.m_Base.skills[i].ID && this.m_Base.skills[i].release_rank < drillingGear.skills[i].release_rank)
          this.m_SkillEvolutionObjectList[i].SetActive(true);
      }
      else if ((UnityEngine.Object) this.m_SkillIconPrefabList[i] != (UnityEngine.Object) null)
        this.m_SkillIconPrefabList[i].gameObject.SetActive(false);
    }
  }

  private void SetStatus(Bugu0059Menu.Status status, int baseParam, int upParam = 0)
  {
    if (upParam > 0)
      status.m_ParameterLabel.SetTextLocalize(Bugu0059Menu.COLOR_TAG_YELLOW.F((object) baseParam));
    else if (upParam < 0)
      status.m_ParameterLabel.SetTextLocalize(Bugu0059Menu.COLOR_TAG_RED.F((object) baseParam));
    else
      status.m_ParameterLabel.SetTextLocalize(baseParam);
    if (upParam <= 0)
    {
      status.m_ParameterUpObject.SetActive(false);
    }
    else
    {
      status.m_ParameterUpObject.SetActive(true);
      status.m_ParameterUpLabel.SetTextLocalize(upParam);
    }
  }

  protected void SelectReisou(GameCore.ItemInfo item)
  {
    if (item == null)
      return;
    this.StartCoroutine(this.OpenReisouDetailPopupAsync(item));
  }

  protected IEnumerator OpenReisouDetailPopupAsync(GameCore.ItemInfo item)
  {
    if (item != null)
    {
      IEnumerator e;
      if ((UnityEngine.Object) this.reisouPopupPrefab == (UnityEngine.Object) null)
      {
        Future<GameObject> popupPrefabF = new ResourceObject("Prefabs/UnitGUIs/PopupReisouSkillDetails").Load<GameObject>();
        e = popupPrefabF.Wait();
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        this.reisouPopupPrefab = popupPrefabF.Result;
        popupPrefabF = (Future<GameObject>) null;
      }
      GameObject popup = this.reisouPopupPrefab.Clone((Transform) null);
      PopupReisouDetails script = popup.GetComponent<PopupReisouDetails>();
      popup.SetActive(false);
      e = script.Init(item, (PlayerItem) null, true);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      popup.SetActive(true);
      Singleton<PopupManager>.GetInstance().open(popup, false, false, true, true, false, false, "SE_1006");
      yield return (object) null;
      script.scrollResetPosition();
    }
  }

  public void ibtnChangeMaterial()
  {
    if (this.IsPushAndSet())
      return;
    if (this.m_Base.gearLevel == this.m_Base.gearLevelLimit)
      Bugu00527Scene.ChangeScene(false, this.m_Materials, this.m_Base, true);
    else
      Bugu00527Scene.ChangeScene(false, this.m_Materials, this.m_Base, false);
  }

  public void ibtnDrilling()
  {
    if (this.IsPushAndSet())
      return;
    this.StartCoroutine(this.OpenMaterialCheckPopup());
  }

  private IEnumerator OpenMaterialCheckPopup()
  {
    Bugu0059Menu bugu0059Menu = this;
    GameObject obj = bugu0059Menu.m_Materials.Count <= 5 ? bugu0059Menu.m_CheckMaterialPopupPrefabMini.Clone((Transform) null) : bugu0059Menu.m_CheckMaterialPopupPrefab.Clone((Transform) null);
    // ISSUE: reference to a compiler-generated method
    IEnumerator e = obj.GetComponent<Popup00591Menu>().Init(bugu0059Menu.m_Base, bugu0059Menu.m_After, bugu0059Menu.reisouInfo, bugu0059Menu.reisouInfoAfter, bugu0059Menu.m_Materials, new System.Action(bugu0059Menu.\u003COpenMaterialCheckPopup\u003Eb__57_0), bugu0059Menu.m_ItemIconPrefab);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Singleton<PopupManager>.GetInstance().open(obj, false, false, true, true, false, false, "SE_1006");
  }

  private IEnumerator drilling()
  {
    Future<WebAPI.Response.ItemGearDrilling> feature = WebAPI.ItemGearDrilling(this.m_Base.itemID, this.m_Materials.ToGearId().ToArray(), this.m_Materials.ToMaterialId().ToArray(), (System.Action<WebAPI.Response.UserError>) null);
    IEnumerator e = feature.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    WebAPI.Response.ItemGearDrilling result = feature.Result;
    List<GameCore.ItemInfo> numList = new List<GameCore.ItemInfo>(this.m_Materials.Select<InventoryItem, GameCore.ItemInfo>((Func<InventoryItem, GameCore.ItemInfo>) (x => x.Item)));
    numList.Add(new GameCore.ItemInfo(result.player_item));
    bool flag = result.player_item.gear_level_limit_max <= result.player_item.gear_level_limit && result.player_item.gear_level >= result.player_item.gear_level_limit;
    Bugu00539Scene.ChangeScene(true, new Bugu00539ChangeSceneParam(numList, false, result.animation_pattern, this.m_Base, this.reisouInfo, flag ? (System.Action) (() =>
    {
      NGSceneManager instance = Singleton<NGSceneManager>.GetInstance();
      if (instance.backScene("bugu005_drilling_base"))
        return;
      instance.changeScene(Singleton<CommonRoot>.GetInstance().startScene, false, (object[]) Array.Empty<object>());
    }) : (System.Action) null));
    this.m_ParentScene.SetResetFlag();
  }

  public void ibtnBack()
  {
    if (this.IsPushAndSet())
      return;
    this.backScene();
  }

  public override void onBackButton()
  {
    this.ibtnBack();
  }

  public void callBackScene()
  {
    this.backScene();
  }

  private void SetRarity(GearGear gear, UI2DSprite dstSprite)
  {
    if ((UnityEngine.Object) dstSprite == (UnityEngine.Object) null)
      return;
    dstSprite.gameObject.SetActive(false);
    if (gear.rarity.index <= 0)
      return;
    UnityEngine.Sprite sprite = Resources.Load<UnityEngine.Sprite>(Bugu0059Menu.spriteNameTBL[gear.rarity.index - 1]);
    if (!((UnityEngine.Object) sprite != (UnityEngine.Object) null))
      return;
    dstSprite.sprite2D = sprite;
    dstSprite.SetDimensions((int) sprite.textureRect.width, (int) sprite.textureRect.height);
    dstSprite.gameObject.SetActive(true);
  }

  [Serializable]
  public struct Status
  {
    [SerializeField]
    public UILabel m_ParameterLabel;
    [SerializeField]
    public GameObject m_ParameterUpObject;
    [SerializeField]
    public UILabel m_ParameterUpLabel;
  }

  [Serializable]
  public class MaterialIcon
  {
    [SerializeField]
    public GameObject m_Parent;
    [SerializeField]
    public GameObject m_AddMaterialObject;
    [HideInInspector]
    public ItemIcon m_ItemIcon;
  }
}
