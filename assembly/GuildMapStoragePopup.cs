﻿// Decompiled with JetBrains decompiler
// Type: GuildMapStoragePopup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System.Collections;
using UnityEngine;

public class GuildMapStoragePopup : BackButtonMonoBehaiviour
{
  [SerializeField]
  private UIScrollView scrollView;
  [SerializeField]
  private UIGrid grid;
  [SerializeField]
  private GameObject slc_Listbase_None;
  [SerializeField]
  private UILabel lblPossession;
  private Guild0282Menu guildMenu;
  private GameObject mapScrollPrefab;
  private GameObject mapDetailPopupPrefab;
  private bool isPush;

  private IEnumerator ResourceLoad()
  {
    Future<GameObject> f;
    IEnumerator e;
    if ((UnityEngine.Object) this.mapScrollPrefab == (UnityEngine.Object) null)
    {
      f = new ResourceObject("Prefabs/map_edit031/dir_base_map_list").Load<GameObject>();
      e = f.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.mapScrollPrefab = f.Result;
      f = (Future<GameObject>) null;
    }
    if ((UnityEngine.Object) this.mapDetailPopupPrefab == (UnityEngine.Object) null)
    {
      f = new ResourceObject("Prefabs/popup/popup_031_map_detail__anim_popup01").Load<GameObject>();
      e = f.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.mapDetailPopupPrefab = f.Result;
      f = (Future<GameObject>) null;
    }
  }

  private IEnumerator ShowMapDetailPopup(PlayerGuildTown guildTown)
  {
    GuildMapStoragePopup guildMapStoragePopup = this;
    GameObject popup = guildMapStoragePopup.mapDetailPopupPrefab.Clone((Transform) null);
    popup.SetActive(false);
    // ISSUE: reference to a compiler-generated method
    IEnumerator e = popup.GetComponent<PopupMapDetailMenu>().InitializeAsync(guildTown._master, new System.Action(guildMapStoragePopup.\u003CShowMapDetailPopup\u003Eb__9_0));
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    popup.SetActive(true);
    Singleton<PopupManager>.GetInstance().open(popup, false, false, true, true, false, false, "SE_1006");
  }

  private bool isPushAndSet()
  {
    if (this.isPush)
      return true;
    this.isPush = true;
    return false;
  }

  public IEnumerator InitializeAsync(Guild0282Menu menu, PlayerGuildTown[] towns)
  {
    GuildMapStoragePopup guildMapStoragePopup = this;
    guildMapStoragePopup.guildMenu = menu;
    IEnumerator e = guildMapStoragePopup.ResourceLoad();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if (towns == null || towns.Length == 0)
    {
      guildMapStoragePopup.slc_Listbase_None.SetActive(true);
    }
    else
    {
      guildMapStoragePopup.lblPossession.SetTextLocalize(towns.Length);
      guildMapStoragePopup.slc_Listbase_None.SetActive(false);
      for (int i = 0; i < towns.Length; ++i)
      {
        // ISSUE: reference to a compiler-generated method
        e = guildMapStoragePopup.mapScrollPrefab.Clone(guildMapStoragePopup.grid.transform).GetComponent<GuildMapStorageScroll>().InitializeAsync(towns[i], new System.Action<PlayerGuildTown>(guildMapStoragePopup.\u003CInitializeAsync\u003Eb__11_0));
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
      }
      guildMapStoragePopup.grid.Reposition();
    }
  }

  public override void onBackButton()
  {
    if (this.isPushAndSet())
      return;
    this.guildMenu.closePopup();
  }
}
