﻿// Decompiled with JetBrains decompiler
// Type: DateCheck
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

public static class DateCheck
{
  public static bool YearCheck(string y)
  {
    int result;
    return y.Length >= 1 && int.TryParse(y, out result) && result >= 1900 && result < DateTime.Now.Year;
  }

  public static bool MonthCheck(string m)
  {
    int result;
    return m.Length >= 1 && int.TryParse(m, out result) && (result > 0 && result <= 12);
  }
}
