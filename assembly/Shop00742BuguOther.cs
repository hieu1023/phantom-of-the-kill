﻿// Decompiled with JetBrains decompiler
// Type: Shop00742BuguOther
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using System.Collections;
using UnityEngine;

public class Shop00742BuguOther : Shop00742Bugu
{
  [SerializeField]
  protected UILabel TxtFlavor;

  public IEnumerator Initialize(GearGear target)
  {
    Shop00742BuguOther shop00742BuguOther = this;
    shop00742BuguOther.TxtFlavor.SetText(target.description);
    shop00742BuguOther.TxtName.SetText(target.name);
    IEnumerator e = shop00742BuguOther.RarityCreate(target);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    e = shop00742BuguOther.BuguSpriteCreate(target.LoadSpriteBasic(1f));
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  protected override IEnumerator BuguSpriteCreate(Future<UnityEngine.Sprite> spriteF)
  {
    return base.BuguSpriteCreate(spriteF);
  }

  protected override IEnumerator RarityCreate(GearGear target)
  {
    return base.RarityCreate(target);
  }
}
