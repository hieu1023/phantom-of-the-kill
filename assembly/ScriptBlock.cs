﻿// Decompiled with JetBrains decompiler
// Type: ScriptBlock
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore.LispCore;
using System.Collections.Generic;

public class ScriptBlock
{
  public Cons script;

  public void add(string name, Cons args)
  {
    Cons cons = SExp.cons((object) SExp.cons((object) name, (object) args), (object) null);
    if (this.script == null)
      this.script = cons;
    else
      SExp.lastCons((object) this.script).cdr = (object) cons;
  }

  public void add(Cons body)
  {
    if (this.script == null)
      this.script = body;
    else
      SExp.lastCons((object) this.script).cdr = (object) body;
  }

  public void eval(Lisp engine)
  {
    engine.evalBody((object) this.script, (Dictionary<string, object>) null);
  }
}
