﻿// Decompiled with JetBrains decompiler
// Type: EffectSE
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

public class EffectSE : MonoBehaviour
{
  public bool playOnStart = true;
  public string SoundEffectName;
  public bool playOnEnable;
  public float Delay;
  private NGSoundManager sm;

  private void Start()
  {
    this.sm = Singleton<NGSoundManager>.GetInstance();
    if (!this.playOnStart && !this.playOnEnable)
      return;
    this.StartCoroutine(this.PlaySE(this.Delay));
  }

  private void OnEnable()
  {
    if (this.playOnStart || !this.playOnEnable)
      return;
    this.StartCoroutine(this.PlaySE(this.Delay));
  }

  public void playSe()
  {
    this.sm = Singleton<NGSoundManager>.GetInstance();
    if (this.playOnStart)
      return;
    this.StartCoroutine(this.PlaySE(this.Delay));
  }

  public IEnumerator PlaySE(float delayTime = 0.0f)
  {
    if (!((Object) null == (Object) this.sm) && !string.IsNullOrEmpty(this.SoundEffectName))
    {
      if ((double) delayTime > 0.0)
        yield return (object) new WaitForSeconds(delayTime);
      this.sm.playSE(this.SoundEffectName, false, 0.0f, -1);
    }
  }
}
