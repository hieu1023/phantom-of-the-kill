﻿// Decompiled with JetBrains decompiler
// Type: Transfer01289Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;
using UnityEngine;

public class Transfer01289Menu : MonoBehaviour
{
  [SerializeField]
  protected UILabel TxtDescription;

  public void ChangeDescription(string errorCode)
  {
    string text;
    if (!(errorCode == "ASE001"))
    {
      if (!(errorCode == "ASE010"))
      {
        if (errorCode == "unknown")
          text = Consts.GetInstance().POPUP_012_8_9_UNKNOWN_TEXT;
        else
          text = Consts.Format(Consts.GetInstance().POPUP_012_8_9_OTHER_TEXT, (IDictionary) new Hashtable()
          {
            {
              (object) "error_code",
              (object) errorCode
            }
          });
      }
      else
        text = Consts.GetInstance().POPUP_012_8_9_ASE010_TEXT;
    }
    else
      text = Consts.GetInstance().POPUP_012_8_9_ASE001_TEXT;
    this.TxtDescription.SetTextLocalize(text);
  }

  public string TimeIntToString(int time)
  {
    return time >= 0 && time < 10 ? "０" + time.ToLocalizeNumberText() : time.ToLocalizeNumberText();
  }
}
