﻿// Decompiled with JetBrains decompiler
// Type: Colosseum0236Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System;
using System.Collections;
using UnityEngine;

public class Colosseum0236Scene : NGSceneBase
{
  [SerializeField]
  private Colosseum0236Menu menu;

  public static void ChangeScene(ColosseumUtility.Info info)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("colosseum023_6", false, (object) info);
  }

  public override IEnumerator onInitSceneAsync()
  {
    Colosseum0236Scene colosseum0236Scene = this;
    Future<GameObject> bgF = Res.Prefabs.BackGround.ColosseumBackground.Load<GameObject>();
    IEnumerator e = bgF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    colosseum0236Scene.backgroundPrefab = bgF.Result;
  }

  public IEnumerator onStartSceneAsync()
  {
    Future<WebAPI.Response.ColosseumBoot> futureF = WebAPI.ColosseumBoot((System.Action<WebAPI.Response.UserError>) (e =>
    {
      WebAPI.DefaultUserErrorCallback(e);
      Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
    }));
    IEnumerator e1 = futureF.Wait();
    while (e1.MoveNext())
      yield return e1.Current;
    e1 = (IEnumerator) null;
    if (futureF.Result != null)
    {
      e1 = this.onStartSceneAsync(new ColosseumUtility.Info(false, futureF.Result));
      while (e1.MoveNext())
        yield return e1.Current;
      e1 = (IEnumerator) null;
    }
  }

  public IEnumerator onStartSceneAsync(ColosseumUtility.Info info)
  {
    IEnumerator e = this.menu.Initialize(info);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }
}
