﻿// Decompiled with JetBrains decompiler
// Type: Shop007231Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Shop007231Menu : BackButtonMenuBase
{
  [SerializeField]
  private Shop007231Scene scene_;
  [SerializeField]
  private UIScrollView scroll_;
  [SerializeField]
  private UIGrid grid_;
  [SerializeField]
  private GameObject ticketsList_;
  [SerializeField]
  private GameObject noTicketTxt_;

  public IEnumerator coInitialize(
    IEnumerable<SelectTicket> unitTickets,
    List<PlayerSelectTicketSummary> playerUnitTickets)
  {
    Shop007231Menu menu = this;
    if (playerUnitTickets.Count == 0)
    {
      menu.noTicketTxt_.SetActive(true);
      menu.ticketsList_.SetActive(false);
    }
    else
    {
      menu.noTicketTxt_.SetActive(false);
      menu.ticketsList_.SetActive(true);
      Future<GameObject> ldPrefab = Res.Prefabs.shop007_23_1.slc_Ticket_list_Item.Load<GameObject>();
      IEnumerator e = ldPrefab.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      GameObject prefabCoupon = ldPrefab.Result;
      if (!((UnityEngine.Object) prefabCoupon == (UnityEngine.Object) null))
      {
        ldPrefab = (Future<GameObject>) null;
        foreach (SelectTicket unitTicket1 in unitTickets)
        {
          SelectTicket unitTicket = unitTicket1;
          PlayerSelectTicketSummary playerUnitTicket = playerUnitTickets.FirstOrDefault<PlayerSelectTicketSummary>((Func<PlayerSelectTicketSummary, bool>) (x => x.ticket_id == unitTicket.id));
          e = prefabCoupon.Clone(menu.grid_.transform).GetComponent<Shop007231Coupon>().coInitialize(menu, playerUnitTicket, unitTicket);
          while (e.MoveNext())
            yield return e.Current;
          e = (IEnumerator) null;
        }
        menu.grid_.Reposition();
        menu.scroll_.ResetPosition();
      }
    }
  }

  public void onEndMenu()
  {
    foreach (Component child in this.grid_.transform.GetChildren())
      UnityEngine.Object.Destroy((UnityEngine.Object) child.gameObject);
  }

  public void selectedCoupon(PlayerSelectTicketSummary playerUnitTicket)
  {
    if (this.IsPushAndSet())
      return;
    Shop00723Scene.changeScene(playerUnitTicket, true);
  }

  public void onClickedDescription(SelectTicket ticket)
  {
    if (this.IsPushAndSet())
      return;
    this.StartCoroutine("coPopupDescription", (object) ticket);
  }

  private IEnumerator coPopupDescription(SelectTicket ticket)
  {
    Future<GameObject> ldprefab = Res.Prefabs.popup.popup_007_23_1__anim_popup01.Load<GameObject>();
    IEnumerator e = ldprefab.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Singleton<PopupManager>.GetInstance().open(ldprefab.Result, false, false, false, true, false, false, "SE_1006").GetComponent<Shop007231Description>().initialize(ticket);
  }

  public override void onBackButton()
  {
    this.OnIbtnBack();
  }

  public void OnIbtnBack()
  {
    if (this.IsPushAndSet())
      return;
    this.backScene();
  }
}
