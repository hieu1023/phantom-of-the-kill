﻿// Decompiled with JetBrains decompiler
// Type: Quest002152Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

public class Quest002152Menu : NGMenuBase
{
  [SerializeField]
  protected UILabel TxtDescription;
  [SerializeField]
  protected UILabel TxtLiberation;
  public GameObject popupPrefab002152;
  public int Pop;

  public virtual void IbtnPopupClose()
  {
    Debug.Log((object) "click default event IbtnPopupClose");
  }

  public void popupIntimacy()
  {
    Singleton<PopupManager>.GetInstance().openAlert(this.popupPrefab002152, false, false, (EventDelegate) null, false, true, false, true);
  }

  public IEnumerator Init()
  {
    this.Pop = 0;
    yield break;
  }

  private void Update()
  {
    if (this.Pop != 4)
      return;
    this.popupPrefab002152.GetComponent<Quest002152popup>().PopupSetiing();
    this.popupIntimacy();
    this.Pop = 0;
  }

  public void PopPlus()
  {
    ++this.Pop;
  }
}
