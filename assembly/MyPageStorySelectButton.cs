﻿// Decompiled with JetBrains decompiler
// Type: MyPageStorySelectButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class MyPageStorySelectButton : MonoBehaviour
{
  [SerializeField]
  private UIButton button;
  [SerializeField]
  private UISprite sprite;
  private Color defaultSpriteColor;

  private void Start()
  {
    this.defaultSpriteColor = this.sprite.color;
  }

  public void OnPress(bool isDown)
  {
    if (isDown)
      this.sprite.color = this.button.pressed;
    else
      this.sprite.color = this.defaultSpriteColor;
  }

  private void OnDragOut(GameObject draggedObject)
  {
    this.sprite.color = this.defaultSpriteColor;
  }

  private void OnDragEnd()
  {
    this.sprite.color = this.defaultSpriteColor;
  }
}
