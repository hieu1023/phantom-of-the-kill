﻿// Decompiled with JetBrains decompiler
// Type: GuildEstablishmentPopup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class GuildEstablishmentPopup : MonoBehaviour
{
  [SerializeField]
  private Animator anim;
  private const float animNormalSpeed = 1f;
  private const float animSkipSpeed = 162f;

  public void Skip()
  {
    this.anim.speed = 162f;
  }

  public void Stop()
  {
    this.anim.speed = 1f;
  }

  public void ChangeScene()
  {
    Singleton<PopupManager>.GetInstance().dismiss(false);
    Guild0281Scene.ChangeSceneGuildTop(true, (Guild0281Menu) null, true);
  }

  public void PlaySound(string clip)
  {
    Singleton<NGSoundManager>.GetInstance().PlaySe(clip, false, 0.0f, -1);
  }
}
