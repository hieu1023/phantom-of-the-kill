﻿// Decompiled with JetBrains decompiler
// Type: Popup004ReincarnationTypeMenu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;
using UnityEngine;

public class Popup004ReincarnationTypeMenu : BackButtonMenuBase
{
  private Unit004ReincarnationTypeMenu menu;
  [SerializeField]
  private UILabel TxtHimeTypeLeft;
  [SerializeField]
  private UILabel TxtHimeTypeRight;

  public void Init(
    Unit004ReincarnationTypeMenu menu,
    PlayerUnit playerUnitBefore,
    PlayerUnit playerUnitAfter)
  {
    this.menu = menu;
    this.TxtHimeTypeLeft.SetTextLocalize(playerUnitBefore.unit_type.name);
    this.TxtHimeTypeRight.SetTextLocalize(playerUnitAfter.unit_type.name);
  }

  public void IbtnYes()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<PopupManager>.GetInstance().onDismiss();
    this.StartCoroutine(this.menu.ReincarnationType());
  }

  public void IbtnNo()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<PopupManager>.GetInstance().onDismiss();
  }

  public override void onBackButton()
  {
    this.IbtnNo();
  }
}
