﻿// Decompiled with JetBrains decompiler
// Type: DailyMissionList
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MissionData;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class DailyMissionList : MonoBehaviour
{
  private List<Coroutine> loadingCoroutineList = new List<Coroutine>();
  private List<IMissionAchievement> loadMissionList = new List<IMissionAchievement>();
  private List<GameObject> loadScrollList = new List<GameObject>();
  [SerializeField]
  public UIGrid grid;
  [SerializeField]
  private GameObject dirMissionCleared;
  [SerializeField]
  public UIScrollView scrollView;
  [SerializeField]
  public UIScrollBar scrollBar;
  private DailyMissionController controller;
  private GameObject missionPrefab;
  private IMissionAchievement[] missions;
  private int listTypeIndex;
  private DailyMissionWindow targetWindow;
  private const int defaultSetupMissionMax = 5;

  public bool IsCreated { get; private set; }

  private void OnDestroy()
  {
    this.Clear();
  }

  public void Initialize(
    DailyMissionWindow targetWindow,
    int listType,
    GameObject prefab,
    IMissionAchievement[] missions)
  {
    this.targetWindow = targetWindow;
    this.listTypeIndex = listType;
    this.missions = missions;
    this.missionPrefab = prefab;
    this.SetVisible(false);
    this.IsCreated = false;
  }

  public void Clear()
  {
    foreach (Coroutine loadingCoroutine in this.loadingCoroutineList)
    {
      if (loadingCoroutine != null)
        this.StopCoroutine(loadingCoroutine);
    }
    this.loadingCoroutineList.Clear();
    foreach (Component component in this.grid.transform)
      Object.Destroy((Object) component.gameObject);
    this.grid.transform.Clear();
    this.loadMissionList.Clear();
    this.loadScrollList.Clear();
  }

  public void ResetPosition()
  {
    this.scrollView.ResetPosition();
  }

  public void SetVisible(bool isVisible)
  {
    this.scrollBar.gameObject.SetActive(isVisible);
    this.scrollView.gameObject.SetActive(isVisible);
    if (this.missions == null)
      return;
    this.dirMissionCleared.SetActive(isVisible && ((IEnumerable<IMissionAchievement>) this.missions).Count<IMissionAchievement>() == 0);
  }

  public IEnumerator Create(DailyMissionController controller)
  {
    DailyMissionList dailyMissionList = this;
    dailyMissionList.controller = controller;
    dailyMissionList.Clear();
    int count = 0;
    IMissionAchievement[] missionAchievementArray = dailyMissionList.missions;
    for (int index = 0; index < missionAchievementArray.Length; ++index)
    {
      IMissionAchievement pdm = missionAchievementArray[index];
      GameObject gameObject = dailyMissionList.missionPrefab.Clone(dailyMissionList.grid.transform);
      if (count < 5 || !PerformanceConfig.GetInstance().IsTuningMissionInitialize)
      {
        IEnumerator e = gameObject.GetComponent<DailyMission0272Panel>().Init(controller, pdm, dailyMissionList.targetWindow, dailyMissionList.listTypeIndex);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
      }
      else
      {
        dailyMissionList.loadMissionList.Add(pdm);
        dailyMissionList.loadScrollList.Add(gameObject);
      }
      ++count;
    }
    missionAchievementArray = (IMissionAchievement[]) null;
    if (dailyMissionList.loadMissionList.Count > 0)
      dailyMissionList.loadingCoroutineList.Add(dailyMissionList.StartCoroutine(dailyMissionList.CreatePanels()));
    // ISSUE: reference to a compiler-generated method
    dailyMissionList.grid.onReposition = new UIGrid.OnReposition(dailyMissionList.\u003CCreate\u003Eb__22_0);
    dailyMissionList.grid.Reposition();
    dailyMissionList.IsCreated = true;
  }

  private IEnumerator CreatePanels()
  {
    for (int i = 0; i < this.loadMissionList.Count; ++i)
    {
      IEnumerator e = this.loadScrollList[i].GetComponent<DailyMission0272Panel>().Init(this.controller, this.loadMissionList[i], this.targetWindow, this.listTypeIndex);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      yield return (object) null;
    }
  }
}
