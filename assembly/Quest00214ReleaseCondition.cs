﻿// Decompiled with JetBrains decompiler
// Type: Quest00214ReleaseCondition
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quest00214ReleaseCondition : MonoBehaviour
{
  public NGxScroll scroll;

  public IEnumerator InitRelease(
    List<QuestDisplayConditionConverter> list,
    GameObject iconPrefab,
    GameObject conditionPrefab)
  {
    this.scroll.Clear();
    foreach (QuestDisplayConditionConverter data in list)
    {
      GameObject gameObject = conditionPrefab.Clone((Transform) null);
      this.scroll.Add(gameObject, false);
      IEnumerator e = gameObject.GetComponent<Quest00214aScroll>().Init(iconPrefab, data);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    this.scroll.ResolvePosition();
  }

  public void StartTween(bool order)
  {
    foreach (UITweener component in this.GetComponents<UITweener>())
    {
      if (component.tweenGroup == 1)
        component.Play(order);
    }
  }

  public void StartTweenClick(bool order)
  {
    foreach (UITweener component in this.GetComponents<UITweener>())
    {
      if (component.tweenGroup == 2)
        component.Play(order);
    }
  }
}
