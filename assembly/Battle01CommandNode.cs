﻿// Decompiled with JetBrains decompiler
// Type: Battle01CommandNode
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class Battle01CommandNode : MonoBehaviour
{
  private bool isInitializeResetDelegate_ = true;
  public Battle01CommandWait cmdWait_;
  public Battle01CommandSkill cmdSkill_;
  public Battle01CommandOugi cmdOugi_;
  private Battle01CommandNode.CommandFlag? wFlags_;
  private Battle01CommandNode.ResetDelegate onReset_;

  public Battle01CommandNode.CommandFlag ActiveCommands
  {
    get
    {
      return !this.wFlags_.HasValue ? (this.wFlags_ = new Battle01CommandNode.CommandFlag?((Battle01CommandNode.CommandFlag) (((UnityEngine.Object) this.cmdWait_ != (UnityEngine.Object) null ? 1 : 0) | ((UnityEngine.Object) this.cmdSkill_ != (UnityEngine.Object) null ? 2 : 0) | ((UnityEngine.Object) this.cmdOugi_ != (UnityEngine.Object) null ? 4 : 0)))).Value : this.wFlags_.Value;
    }
  }

  public void resetCurrentUnitPosition(bool bClear = false)
  {
    if (this.isInitializeResetDelegate_)
    {
      this.isInitializeResetDelegate_ = false;
      if ((UnityEngine.Object) this.cmdSkill_ != (UnityEngine.Object) null)
        this.onReset_ += new Battle01CommandNode.ResetDelegate(this.cmdSkill_.resetCurrentUnitPosition);
      if ((UnityEngine.Object) this.cmdOugi_ != (UnityEngine.Object) null)
        this.onReset_ += new Battle01CommandNode.ResetDelegate(this.cmdOugi_.resetCurrentUnitPosition);
    }
    Battle01CommandNode.ResetDelegate onReset = this.onReset_;
    if (onReset == null)
      return;
    onReset(bClear);
  }

  public enum CommandNo
  {
    Wait,
    Skill,
    Ougi,
    JobAbility,
  }

  [Flags]
  public enum CommandFlag
  {
    Nil = 0,
    Wait = 1,
    Skill = 2,
    Ougi = 4,
  }

  private delegate void ResetDelegate(bool bClear);
}
