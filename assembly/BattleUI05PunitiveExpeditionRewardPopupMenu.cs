﻿// Decompiled with JetBrains decompiler
// Type: BattleUI05PunitiveExpeditionRewardPopupMenu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System.Collections;
using UnityEngine;

public class BattleUI05PunitiveExpeditionRewardPopupMenu : ResultMenuBase
{
  private GameObject HunitingRewardGetPrefab;

  public override IEnumerator Init(BattleInfo info, BattleEnd result, int index)
  {
    IEnumerator e = this.LoadResources();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  private IEnumerator LoadResources()
  {
    if ((UnityEngine.Object) this.HunitingRewardGetPrefab == (UnityEngine.Object) null)
    {
      Future<GameObject> prefabF = Res.Prefabs.battle.Huniting_RewardGet.Load<GameObject>();
      IEnumerator e = prefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.HunitingRewardGetPrefab = prefabF.Result;
      prefabF = (Future<GameObject>) null;
    }
  }

  public override IEnumerator Run()
  {
    GameObject popup = this.HunitingRewardGetPrefab.Clone((Transform) null);
    BattleUI05PunitiveExpeditionRewardPopup script = popup.GetComponent<BattleUI05PunitiveExpeditionRewardPopup>();
    IEnumerator e = script.Init();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Singleton<PopupManager>.GetInstance().open(popup, false, false, true, true, false, false, "SE_1006");
    bool toNext = false;
    script.SetTapCallBack((System.Action) (() =>
    {
      if (this.IsPush)
        return;
      toNext = true;
    }));
    while (!toNext)
      yield return (object) null;
    Singleton<PopupManager>.GetInstance().onDismiss();
    yield return (object) new WaitForSeconds(0.5f);
  }

  public override IEnumerator OnFinish()
  {
    yield break;
  }
}
