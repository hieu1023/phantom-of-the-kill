﻿// Decompiled with JetBrains decompiler
// Type: TextTipsPrefab
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using UnityEngine;

public class TextTipsPrefab : MonoBehaviour
{
  [SerializeField]
  private UILabel title;
  [SerializeField]
  private UILabel subtitle;
  [SerializeField]
  private UILabel description;
  [SerializeField]
  private UILabel sourceName;

  public void Init(TipsTextTips tips)
  {
    this.title.text = tips.title;
    this.subtitle.text = tips.subtitle;
    this.description.text = tips.description;
    this.sourceName.text = tips.sourcename;
  }
}
