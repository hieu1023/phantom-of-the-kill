﻿// Decompiled with JetBrains decompiler
// Type: Quest002171QuestOpenPopup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Quest002171QuestOpenPopup : BackButtonMenuBase
{
  [SerializeField]
  private UILabel description;
  [SerializeField]
  private UILabel Title;
  [SerializeField]
  private UIButton ibtnYes;
  [SerializeField]
  private UIButton ibtnNo;
  [SerializeField]
  private List<GameObject> Banners;
  private PlayerQuestGate passGate;
  private Quest002171Scroll scrollcomp;
  [SerializeField]
  private UIGrid grid;
  [SerializeField]
  private UIScrollView scrollview;
  [SerializeField]
  private NGxScroll ngxScroll;
  [SerializeField]
  private UI2DSprite keySprite;
  [SerializeField]
  private UILabel txtPossession;

  public IEnumerator Init(PlayerQuestGate[] gates, Quest002171Scroll scrollcomp)
  {
    this.scrollcomp = scrollcomp;
    PlayerQuestKey playerQuestKey = ((IEnumerable<PlayerQuestKey>) SMManager.Get<PlayerQuestKey[]>()).Where<PlayerQuestKey>((Func<PlayerQuestKey, bool>) (x => x.quest_key_id == gates[0].quest_key_id)).FirstOrDefault<PlayerQuestKey>();
    int quantity = playerQuestKey == null ? 0 : playerQuestKey.quantity;
    this.txtPossession.SetTextLocalize(quantity);
    this.Title.SetText(Consts.GetInstance().QUEST_002171_RELEASEPOPUP_TITLE);
    IEnumerator e = this.CreateKeySprite(gates[0].quest_key_id);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    e = this.InitOfEachCount(gates, quantity);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  private IEnumerator InitOfEachCount(PlayerQuestGate[] gates, int quantity)
  {
    IEnumerator e;
    switch (gates.Length)
    {
      case 1:
        this.passGate = gates[0];
        this.description.SetTextLocalize(Consts.Format(Consts.GetInstance().QUEST_002171_RELEASEPOPUP_DESCRIPTION, (IDictionary) new Hashtable()
        {
          {
            (object) "name",
            (object) MasterData.QuestkeyQuestkey[this.passGate.quest_key_id].name
          },
          {
            (object) nameof (quantity),
            (object) this.passGate.consume_quantity.ToLocalizeNumberText()
          },
          {
            (object) "time",
            (object) this.GetReleaseTime(this.passGate.time)
          }
        }));
        if (!((UnityEngine.Object) this.ibtnYes != (UnityEngine.Object) null))
          break;
        this.ibtnYes.isEnabled = quantity >= this.passGate.consume_quantity;
        break;
      case 2:
        this.description.SetText(Consts.Format(Consts.GetInstance().QUEST_002171_RELEASEPOPUP_DESCRIPTION_2, (IDictionary) new Hashtable()
        {
          {
            (object) "name",
            (object) MasterData.QuestkeyQuestkey[gates[0].quest_key_id].name
          }
        }));
        e = this.CreateBannerSprite(gates, false, false);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        break;
      default:
        this.description.SetText(Consts.Format(Consts.GetInstance().QUEST_002171_RELEASEPOPUP_DESCRIPTION_2, (IDictionary) new Hashtable()
        {
          {
            (object) "name",
            (object) MasterData.QuestkeyQuestkey[gates[0].quest_key_id].name
          }
        }));
        e = this.CreateBanner(gates.Length);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        e = this.CreateBannerSprite(gates, false, false);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        break;
    }
  }

  private string GetReleaseTime(int totalSec)
  {
    string str = "";
    TimeSpan timeSpan = new TimeSpan(0, 0, 0, totalSec);
    if (timeSpan.Days > 0)
      str += Consts.Format(Consts.GetInstance().QUEST_002171_RELEASEPOPUP_DESCRIPTION_DAY, (IDictionary) new Hashtable()
      {
        {
          (object) "day",
          (object) timeSpan.Days
        }
      });
    if (timeSpan.Hours > 0)
      str += Consts.Format(Consts.GetInstance().QUEST_002171_RELEASEPOPUP_DESCRIPTION_HOUR, (IDictionary) new Hashtable()
      {
        {
          (object) "hour",
          (object) timeSpan.Hours
        }
      });
    if (timeSpan.Minutes > 0)
      str += Consts.Format(Consts.GetInstance().QUEST_002171_RELEASEPOPUP_DESCRIPTION_MIN, (IDictionary) new Hashtable()
      {
        {
          (object) "min",
          (object) timeSpan.Minutes
        }
      });
    return str;
  }

  public void IbtnYes()
  {
    if (this.IsPushAndSet())
      return;
    this.ibtnYes.isEnabled = false;
    this.ibtnNo.isEnabled = false;
    Singleton<PopupManager>.GetInstance().dismiss(false);
    this.StartAPI_QuestRelease(this.passGate, this.scrollcomp);
  }

  private void StartAPI_QuestRelease(PlayerQuestGate gate, Quest002171Scroll scroll)
  {
    this.StartCoroutine(this.QuestRelease(gate, scroll));
  }

  private IEnumerator QuestRelease(PlayerQuestGate gate, Quest002171Scroll scroll)
  {
    IEnumerator e;
    if (scroll.CanPlay)
    {
      PlayerQuestKey key = ((IEnumerable<PlayerQuestKey>) SMManager.Get<PlayerQuestKey[]>()).Where<PlayerQuestKey>((Func<PlayerQuestKey, bool>) (x => x.quest_key_id == gate.quest_key_id)).First<PlayerQuestKey>();
      Future<GameObject> prefabF = Res.Prefabs.popup.popup_002_17_1__anim_popup01.Load<GameObject>();
      e = prefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      GameObject popup = prefabF.Result.Clone((Transform) null);
      Quest002171CantOpenQuestPopup component = popup.GetComponent<Quest002171CantOpenQuestPopup>();
      popup.SetActive(false);
      e = component.Init(MasterData.QuestkeyQuestkey[gate.quest_key_id].name, gate.quest_key_id, key.quantity, gate.consume_quantity);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      popup.SetActive(true);
      Singleton<PopupManager>.GetInstance().open(popup, false, false, true, true, false, false, "SE_1006");
    }
    else
    {
      Singleton<CommonRoot>.GetInstance().loadingMode = 1;
      Singleton<CommonRoot>.GetInstance().isLoading = true;
      bool isFaildSpend = false;
      e = WebAPI.QuestkeySpend(gate.quest_key_id, gate.consume_quantity, gate.quest_gate_id, (System.Action<WebAPI.Response.UserError>) (error =>
      {
        WebAPI.DefaultUserErrorCallback(error);
        Singleton<CommonRoot>.GetInstance().loadingMode = 0;
        Singleton<CommonRoot>.GetInstance().isLoading = false;
        isFaildSpend = true;
      })).Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      if (!isFaildSpend)
      {
        Future<WebAPI.Response.QuestkeyIndex> keyfuture = WebAPI.QuestkeyIndex((System.Action<WebAPI.Response.UserError>) (error =>
        {
          WebAPI.DefaultUserErrorCallback(error);
          Singleton<CommonRoot>.GetInstance().loadingMode = 0;
          Singleton<CommonRoot>.GetInstance().isLoading = false;
        }));
        e = keyfuture.Wait();
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        Singleton<CommonRoot>.GetInstance().isLoading = false;
        Singleton<CommonRoot>.GetInstance().loadingMode = 0;
        scroll.ChangeScene(((IEnumerable<PlayerQuestGate>) keyfuture.Result.quest_gates).FirstOrDefault<PlayerQuestGate>((Func<PlayerQuestGate, bool>) (x => x.quest_gate_id == gate.quest_gate_id)));
      }
    }
  }

  public void IbtnNo()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<PopupManager>.GetInstance().onDismiss();
  }

  public override void onBackButton()
  {
    this.IbtnNo();
  }

  private IEnumerator CreateKeySprite(int keyID)
  {
    Future<UnityEngine.Sprite> spriteF = MasterData.QuestkeyQuestkey[keyID].LoadSpriteThumbnail();
    IEnumerator e = spriteF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.keySprite.sprite2D = spriteF.Result;
  }

  private IEnumerator CreateBanner(int num)
  {
    this.ngxScroll.Clear();
    this.Banners.Clear();
    Future<GameObject> prefabF = Singleton<ResourceManager>.GetInstance().Load<GameObject>(string.Format("Prefabs/Banners/KeyQuest/popup_banner/banner_base", (object[]) Array.Empty<object>()), 1f);
    IEnumerator e = prefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    GameObject result = prefabF.Result;
    for (int index = 0; index < num; ++index)
    {
      GameObject gameObject = result.Clone(this.grid.transform);
      this.ngxScroll.Add(gameObject, false);
      this.Banners.Add(gameObject);
    }
    this.ngxScroll.scrollView.panel.baseClipRegion = Vector4.zero;
    this.ngxScroll.ResolvePosition();
    yield return (object) null;
  }

  private IEnumerator CreateBannerSprite(
    PlayerQuestGate[] gates,
    bool isScroll,
    bool isAtlas)
  {
    for (int i = 0; i < gates.Length; ++i)
    {
      IEnumerator e = this.Banners[i].GetComponent<Quest002171PopupBanner>().InitScroll(isScroll, isAtlas, gates[i], this.scrollcomp);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
  }
}
