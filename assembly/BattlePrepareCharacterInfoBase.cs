﻿// Decompiled with JetBrains decompiler
// Type: BattlePrepareCharacterInfoBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class BattlePrepareCharacterInfoBase : NGBattleMenuBase
{
  [SerializeField]
  protected UILabel TxtAttack;
  [SerializeField]
  protected UILabel TxtCharaname_element;
  [SerializeField]
  protected GameObject ElementIconParent;
  [SerializeField]
  protected UILabel TxtCritical;
  [SerializeField]
  protected UILabel TxtDexterity;
  [SerializeField]
  protected UILabel TxtHpNumber;
  [SerializeField]
  protected UILabel TxtWeaponName;
  [SerializeField]
  protected UILabel TxtSecondWeaponName;
  [SerializeField]
  protected Transform ImageParent;
  [SerializeField]
  protected int depth;
  [SerializeField]
  protected UI2DSprite iconGear;
  [SerializeField]
  protected UI2DSprite iconSecondGear;
  [SerializeField]
  protected GameObject upCompatibility;
  [SerializeField]
  protected GameObject downCompatibility;
  [SerializeField]
  protected NGTweenGaugeScale hpBar;
  [SerializeField]
  protected UILabel TxtConsume;
  [SerializeField]
  protected GameObject[] attackCount;
  [SerializeField]
  protected TweenAlpha tweenAlpha_first_weapon;
  [SerializeField]
  protected TweenAlpha tweenAlpha_second_weapon;
  [SerializeField]
  private UISprite slcCountry;
  protected AttackStatus[] attacks;
  protected AttackStatus[] originAttacks;
  protected bool hasMagicBullets;
  private BL.UnitPosition currentUnit;
  private AttackStatus currentAttack;
  private GameObject gearIconPrefab;
  private GameObject elementIconPrefab;
  private CommonElementIcon elementIcon;

  public virtual IEnumerator Init(
    BL.UnitPosition up,
    AttackStatus[] attacks_,
    bool isFacility)
  {
    BL.Unit unit = up.unit;
    this.currentUnit = up;
    this.originAttacks = attacks_;
    this.attacks = ((IEnumerable<AttackStatus>) attacks_).OrderBy<AttackStatus, int>((Func<AttackStatus, int>) (x => x.magicBullet == null || !x.magicBullet.isAttack ? 1000 - x.attack : x.magicBullet.cost * 1000 + (1000 - x.attack))).ToArray<AttackStatus>();
    this.hasMagicBullets = ((IEnumerable<AttackStatus>) this.attacks).Any<AttackStatus>((Func<AttackStatus, bool>) (state => state.magicBullet != null && state.magicBullet.isAttack));
    this.hpBar.setValue(unit.hp, unit.parameter.Hp, false, -1f, -1f);
    this.upCompatibility.SetActive(false);
    this.downCompatibility.SetActive(false);
    this.setCurrentAttack(((IEnumerable<AttackStatus>) this.attacks).Any<AttackStatus>() ? this.attacks[0] : (AttackStatus) null);
    this.setHPNumbers(unit.hp.ToString());
    Future<GameObject> loader = Res.Icons.GearKindIcon.Load<GameObject>();
    IEnumerator e = loader.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.gearIconPrefab = loader.Result;
    if ((UnityEngine.Object) null != (UnityEngine.Object) this.gearIconPrefab)
    {
      GameObject gameObject1 = this.gearIconPrefab.Clone(this.iconGear.transform);
      gameObject1.GetComponent<UI2DSprite>().depth = this.iconGear.depth + 1;
      GearKindIcon component1 = gameObject1.GetComponent<GearKindIcon>();
      CommonElement commonElement1 = this.currentUnit.unit.playerUnit.equippedGear != (PlayerItem) null ? this.currentUnit.unit.playerUnit.equippedGear.GetElement() : this.currentUnit.unit.playerUnit.equippedGearOrInitial.GetElement();
      GearKind kind1 = this.currentUnit.unit.playerUnit.equippedGearOrInitial.kind;
      int num1 = (int) commonElement1;
      component1.Init(kind1, (CommonElement) num1);
      if ((UnityEngine.Object) this.tweenAlpha_second_weapon != (UnityEngine.Object) null && this.tweenAlpha_second_weapon.gameObject.activeSelf)
      {
        GameObject gameObject2 = this.gearIconPrefab.Clone(this.iconSecondGear.transform);
        gameObject2.GetComponent<UI2DSprite>().depth = this.iconSecondGear.depth + 1;
        GearKindIcon component2 = gameObject2.GetComponent<GearKindIcon>();
        CommonElement commonElement2 = this.currentUnit.unit.playerUnit.equippedGear2 != (PlayerItem) null ? this.currentUnit.unit.playerUnit.equippedGear2.GetElement() : this.currentUnit.unit.playerUnit.equippedGearOrInitial.GetElement();
        GearKind kind2 = this.currentUnit.unit.playerUnit.equippedGear2OrInitial.kind;
        int num2 = (int) commonElement2;
        component2.Init(kind2, (CommonElement) num2);
      }
    }
    if ((UnityEngine.Object) this.slcCountry != (UnityEngine.Object) null)
    {
      this.slcCountry.gameObject.SetActive(false);
      if (unit.unit.country_attribute.HasValue)
      {
        this.slcCountry.gameObject.SetActive(true);
        unit.unit.SetCuntrySpriteName(ref this.slcCountry);
      }
    }
    e = this.SetElementalIcon(unit, ((IEnumerable<AttackStatus>) this.attacks).Any<AttackStatus>() ? this.attacks[0].elementAttackRate : 1f);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.ImageParent.Clear();
    this.ImageParent.gameObject.SetActive(!isFacility);
    if (!isFacility)
    {
      Future<GameObject> future = unit.unit.LoadStory();
      e = future.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      GameObject go = future.Result.Clone(this.ImageParent);
      SkillMetamorphosis metamorphosis = unit.metamorphosis;
      int jobOrMetamorId = metamorphosis != null ? metamorphosis.metamorphosis_id : unit.playerUnit.job_id;
      unit.unit.SetStoryData(go, "normal", new int?(jobOrMetamorId));
      e = unit.unit.SetLargeSpriteWithMask(jobOrMetamorId, go, this.maskResource().Load<Texture2D>(), this.depth, 0, 0);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      future = (Future<GameObject>) null;
    }
  }

  public IEnumerator SetElementalIcon(BL.Unit unit, float elementAttackRate)
  {
    this.TxtCharaname_element.SetTextLocalize(unit.unit.name);
    if ((UnityEngine.Object) this.gearIconPrefab == (UnityEngine.Object) null)
    {
      Future<GameObject> loader = Res.Icons.GearKindIcon.Load<GameObject>();
      IEnumerator e = loader.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.gearIconPrefab = loader.Result;
      loader = (Future<GameObject>) null;
    }
    if ((UnityEngine.Object) this.gearIconPrefab != (UnityEngine.Object) null)
    {
      GameObject gameObject = this.gearIconPrefab.Clone(this.ElementIconParent.transform);
      this.ElementIconParent.GetComponent<UIWidget>().depth = this.iconGear.depth + 1;
      gameObject.GetComponent<GearKindIcon>().Init(unit.unit.kind, unit.playerUnit.GetElement());
    }
  }

  private void setHPNumbers(string hp)
  {
    this.TxtHpNumber.SetTextLocalize(hp);
  }

  public BL.UnitPosition getCurrent()
  {
    return this.currentUnit;
  }

  public AttackStatus getCurrentAttack()
  {
    return this.currentAttack;
  }

  public int getCurrentAttackOriginIndex()
  {
    return ((IEnumerable<AttackStatus>) this.originAttacks).FirstIndexOrNull<AttackStatus>((Func<AttackStatus, bool>) (x => x == this.currentAttack)).Value;
  }

  public AttackStatus getAttackStatus(int index)
  {
    return this.attacks[index];
  }

  public void setCurrentAttack(AttackStatus attackStatus)
  {
    this.currentAttack = attackStatus;
    this.attackCount[0].SetActive(false);
    this.attackCount[1].SetActive(false);
    this.attackCount[2].SetActive(false);
    this.TxtWeaponName.SetTextLocalize(this.currentUnit.unit.playerUnit.equippedGearName);
    this.iconGear.gameObject.SetActive(true);
    if ((UnityEngine.Object) this.tweenAlpha_first_weapon != (UnityEngine.Object) null && (UnityEngine.Object) this.tweenAlpha_second_weapon != (UnityEngine.Object) null)
    {
      this.tweenAlpha_first_weapon.gameObject.SetActive(true);
      PlayerItem equippedGear = this.currentUnit.unit.playerUnit.equippedGear;
      PlayerItem equippedGear2 = this.currentUnit.unit.playerUnit.equippedGear2;
      this.tweenAlpha_second_weapon.gameObject.SetActive(equippedGear2 != (PlayerItem) null);
      if (equippedGear2 != (PlayerItem) null)
        this.TxtSecondWeaponName.SetTextLocalize(this.currentUnit.unit.playerUnit.equippedGearName2);
      this.tweenAlpha_first_weapon.enabled = false;
      this.tweenAlpha_second_weapon.enabled = false;
      if (equippedGear != (PlayerItem) null && equippedGear2 != (PlayerItem) null)
      {
        this.tweenAlpha_first_weapon.ResetToBeginning();
        this.tweenAlpha_second_weapon.ResetToBeginning();
        this.StartCoroutine(this.StartWeaponNameAnim());
      }
      else if (this.currentUnit.unit.unit.awake_unit_flag)
      {
        if (equippedGear == (PlayerItem) null && equippedGear2 != (PlayerItem) null)
        {
          this.tweenAlpha_first_weapon.gameObject.SetActive(false);
          this.tweenAlpha_second_weapon.GetComponent<UIWidget>().alpha = 1f;
        }
        else if (equippedGear != (PlayerItem) null && equippedGear2 == (PlayerItem) null)
        {
          this.tweenAlpha_second_weapon.gameObject.SetActive(false);
          this.tweenAlpha_first_weapon.GetComponent<UIWidget>().alpha = 1f;
        }
      }
      else
      {
        this.tweenAlpha_first_weapon.GetComponent<UIWidget>().alpha = 1f;
        this.tweenAlpha_second_weapon.GetComponent<UIWidget>().alpha = 0.0f;
      }
    }
    if (this.currentAttack == null)
    {
      this.TxtAttack.SetText("-");
      this.TxtCritical.SetText("-");
      this.TxtDexterity.SetText("-");
    }
    else
    {
      string format = "{0}";
      if ((double) this.currentAttack.elementAttackRate > 1.0)
        format = "[00ff00]{0}[-]";
      else if ((double) this.currentAttack.elementAttackRate < 1.0)
        format = "[ff0000]{0}[-]";
      float damageRate = this.currentAttack.duelParameter.DamageRate;
      this.currentAttack.duelParameter.DamageRate *= this.currentAttack.elementAttackRate * this.currentAttack.attackClassificationRate * this.currentAttack.normalDamageRate;
      this.TxtAttack.SetTextLocalize(format.F((object) Mathf.Max(Mathf.FloorToInt(this.currentAttack.originalAttack), 1)));
      this.currentAttack.duelParameter.DamageRate = damageRate;
      this.TxtCritical.SetTextLocalize(this.currentAttack.criticalDisplay().ToString() + "%");
      this.TxtDexterity.SetTextLocalize(this.currentAttack.dexerityDisplay().ToString() + "%");
      int attackCount = this.currentAttack.attackCount;
      if (this.currentUnit.unit.playerUnit.getDualWieldSkillData() != null)
        attackCount /= this.currentUnit.unit.playerUnit.normalAttackCount;
      this.setAttackCount(attackCount);
    }
  }

  private void setAttackCount(int count)
  {
    int num = count - 2;
    int index = 2 != count ? (3 != count ? (4 != count ? -1 : 2) : 1) : 0;
    if (index < 0)
      return;
    this.attackCount[index].SetActive(true);
  }

  protected virtual ResourceObject maskResource()
  {
    Debug.LogError((object) "no implements");
    return (ResourceObject) null;
  }

  public void SetCompatibility(PlayerUnit opponent)
  {
    Tuple<int, int> gearKindIncr = this.currentUnit.unit.playerUnit.GetGearKindIncr(opponent);
    if (gearKindIncr.Item1 > 0 || gearKindIncr.Item2 > 0)
    {
      this.upCompatibility.SetActive(true);
    }
    else
    {
      if (gearKindIncr.Item1 >= 0 && gearKindIncr.Item2 >= 0)
        return;
      this.downCompatibility.SetActive(true);
    }
  }

  private IEnumerator StartWeaponNameAnim()
  {
    yield return (object) new WaitForSeconds(0.5f);
    this.tweenAlpha_first_weapon.PlayForward();
    this.tweenAlpha_second_weapon.PlayForward();
  }
}
