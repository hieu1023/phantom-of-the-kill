﻿// Decompiled with JetBrains decompiler
// Type: SM_PlayerUnitHistoryExtension
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using SM;
using System;
using System.Collections.Generic;
using UniLinq;

public static class SM_PlayerUnitHistoryExtension
{
  public static PlayerUnitHistory[] AllPlayers(this PlayerUnitHistory[] self)
  {
    return ((IEnumerable<PlayerUnitHistory>) self).Where<PlayerUnitHistory>((Func<PlayerUnitHistory, bool>) (x => MasterData.UnitUnit[x.unit_id].character.category == UnitCategory.player)).ToArray<PlayerUnitHistory>();
  }

  public static PlayerUnitHistory[] AllEnemies(this PlayerUnitHistory[] self)
  {
    return ((IEnumerable<PlayerUnitHistory>) self).Where<PlayerUnitHistory>((Func<PlayerUnitHistory, bool>) (x => MasterData.UnitUnit[x.unit_id].character.category == UnitCategory.enemy)).ToArray<PlayerUnitHistory>();
  }
}
