﻿// Decompiled with JetBrains decompiler
// Type: BattleFinalize
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;

public class BattleFinalize : BattleMonoBehaviour
{
  protected override IEnumerator Start_Battle()
  {
    BattleFinalize battleFinalize = this;
    Singleton<CommonRoot>.GetInstance().loadingMode = 4;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    WebAPI.Request.BattleFinish request = new WebAPI.Request.BattleFinish();
    request.quest_type = battleFinalize.env.core.battleInfo.quest_type;
    request.win = battleFinalize.env.core.isWin;
    request.is_game_over = battleFinalize.env.core.allDeadUnitsp(BL.ForceID.player);
    request.battle_uuid = battleFinalize.env.core.battleInfo.battleId;
    request.player_money = 0;
    request.battle_turn = battleFinalize.env.core.phaseState.turnCount;
    request.continue_count = battleFinalize.env.core.continueCount;
    foreach (BL.Unit unit in battleFinalize.env.core.playerUnits.value)
    {
      if (unit.duelHistory != null)
      {
        foreach (BL.DuelHistory duelHistory in unit.duelHistory)
        {
          request.duels_damage.Add(duelHistory.inflictTotalDamage);
          request.duels_hit_damage.Add(duelHistory.sufferTotalDamage);
          request.duels_critical_count.Add(duelHistory.criticalCount);
          request.duels_max_damage.Add(duelHistory.inflictMaxDamage);
          request.week_element_attack_count += unit.playerUnit.is_gesut ? 0 : duelHistory.weekElementAttackCount;
          request.week_kind_attack_count += unit.playerUnit.is_gesut ? 0 : duelHistory.weekKindAttackCount;
        }
      }
    }
    for (int row = 0; row < battleFinalize.env.core.battleInfo.stage.map_height; ++row)
    {
      for (int column = 0; column < battleFinalize.env.core.battleInfo.stage.map_width; ++column)
      {
        BL.Panel fieldPanel = battleFinalize.env.core.getFieldPanel(row, column);
        if (fieldPanel.fieldEvent != null && fieldPanel.fieldEvent.isCompleted)
        {
          request.panel_reward.Add(fieldPanel.fieldEvent.reward);
          request.panel_entity_ids.Add(fieldPanel.fieldEventId);
        }
      }
    }
    foreach (BL.Unit unit in battleFinalize.env.core.enemyUnits.value)
    {
      if (unit.hasDrop && unit.drop.isCompleted)
      {
        request.panel_reward.Add(unit.drop.reward);
        request.drop_entity_ids.Add(unit.playerUnit.id);
      }
    }
    foreach (BL.Unit unit in battleFinalize.env.core.playerUnits.value)
      request.units.Add(new WebAPI.Request.BattleFinish.UnitResult()
      {
        player_unit_id = unit.playerUnit.id,
        total_damage = unit.attackDamage,
        total_damage_count = unit.attackCount,
        total_kill_count = unit.killCount,
        remaining_hp = unit.hp,
        rental = unit.is_helper ? 1 : (unit.index != 5 || !unit.playerUnit.is_gesut ? 0 : 1),
        received_damage = unit.receivedDamage,
        guest = unit.playerUnit.is_gesut ? 1 : 0
      });
    foreach (BL.Unit unit in battleFinalize.env.core.enemyUnits.value)
    {
      WebAPI.Request.BattleFinish.EnemyResult enemyResult = new WebAPI.Request.BattleFinish.EnemyResult();
      enemyResult.enemy_id = unit.playerUnit.id;
      enemyResult.dead_count = unit.hp <= 0 ? 1 : 0;
      enemyResult.kill_count = unit.killCount;
      if (unit.killedBy != (BL.Unit) null)
      {
        enemyResult.level_difference = unit.playerUnit.level - unit.killedBy.playerUnit.level;
        enemyResult.kill_by_playerunit_id = unit.killedBy.playerUnit.id;
        enemyResult.overkill_damage = unit.overkillDamage;
      }
      else
      {
        enemyResult.level_difference = 0;
        enemyResult.overkill_damage = 0;
        enemyResult.kill_by_playerunit_id = 0;
      }
      request.enemies.Add(enemyResult);
    }
    foreach (BL.Unit unit in battleFinalize.env.core.playerUnits.value)
    {
      if (!unit.is_helper && !unit.playerUnit.is_gesut)
      {
        if (unit.playerUnit.equippedGear != (PlayerItem) null)
          request.gears.Add(new WebAPI.Request.BattleFinish.GearResult()
          {
            player_gear_id = unit.playerUnit.equippedGear.id,
            kill_count = unit.killCount,
            damage_count = unit.attackCount
          });
        if (unit.playerUnit.equippedGear2 != (PlayerItem) null)
          request.gears.Add(new WebAPI.Request.BattleFinish.GearResult()
          {
            player_gear_id = unit.playerUnit.equippedGear2.id,
            kill_count = unit.killCount,
            damage_count = unit.attackCount
          });
      }
    }
    foreach (BL.Item obj in battleFinalize.env.core.itemList.value)
    {
      int num = obj.initialAmount - obj.amount;
      if (num > 0)
        request.supplies.Add(new WebAPI.Request.BattleFinish.SupplyResult()
        {
          supply_id = obj.itemId,
          use_quantity = num
        });
    }
    foreach (Tuple<int, int, int> tuple in battleFinalize.env.core.getPlayerIntimateResult())
      request.intimates.Add(new WebAPI.Request.BattleFinish.IntimateResult()
      {
        character_id = tuple.Item1,
        target_character_id = tuple.Item2,
        exp = tuple.Item3
      });
    Future<BattleEnd> f = WebAPI.BattleFinish(request, battleFinalize.env, new System.Action<WebAPI.Response.UserError>(battleFinalize.errorCallback));
    IEnumerator e = f.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if (f.Result != null)
    {
      if (battleFinalize.env.core.battleInfo.quest_type == CommonQuestType.Extra)
      {
        e = WebAPI.QuestProgressExtra((System.Action<WebAPI.Response.UserError>) (error => WebAPI.DefaultUserErrorCallback(error))).Wait();
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        WebAPI.SetLatestResponsedAt("QuestProgressExtra");
      }
      if (!battleFinalize.env.core.battleInfo.isEarthMode)
      {
        Singleton<NGGameDataManager>.GetInstance().refreshHomeHome = true;
        BattleUI05Scene.ChangeScene(battleFinalize.env.core.battleInfo, battleFinalize.env.core.isWin, f.Result);
        EventTracker.TrackEvent("BATTLE", "CONTINUE", battleFinalize.env.core.continueCount);
      }
      else
      {
        battleFinalize.battleManager.deleteSavedEnvironment();
        BattleUI55Scene.ChangeScene(battleFinalize.env.core.battleInfo, battleFinalize.env.core.isWin, f.Result);
      }
    }
  }

  private void errorCallback(WebAPI.Response.UserError error)
  {
    if (Singleton<NGGameDataManager>.GetInstance().IsSea && string.Equals(error.Code, "SEA000"))
      this.StartCoroutine(PopupUtility.SeaError(error));
    else
      Singleton<NGSceneManager>.GetInstance().StartCoroutine(PopupCommon.Show(error.Code, error.Reason, (System.Action) (() =>
      {
        NGSceneManager instance = Singleton<NGSceneManager>.GetInstance();
        instance.clearStack();
        instance.destroyCurrentScene();
        instance.changeScene(Singleton<CommonRoot>.GetInstance().startScene, false, (object[]) Array.Empty<object>());
      })));
  }
}
