﻿// Decompiled with JetBrains decompiler
// Type: Shop00722Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;
using System.Collections;
using UnityEngine;

public class Shop00722Scene : NGSceneBase
{
  [SerializeField]
  private Shop00722Menu menu;

  public static void changeScene(
    bool stack,
    int shopID,
    PlayerShopArticle article,
    GameObject production)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("shop007_22", (stack ? 1 : 0) != 0, (object) shopID, (object) article, (object) production);
  }

  public IEnumerator onStartSceneAsync(
    int shopID,
    PlayerShopArticle article,
    GameObject production)
  {
    IEnumerator e = ServerTime.WaitSync();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    e = this.menu.Init(shopID, article, production, ServerTime.NowAppTime());
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }
}
