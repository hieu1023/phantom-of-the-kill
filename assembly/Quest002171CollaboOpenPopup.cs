﻿// Decompiled with JetBrains decompiler
// Type: Quest002171CollaboOpenPopup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Quest002171CollaboOpenPopup : Quest002171QuestOpenPopup
{
  [SerializeField]
  private UILabel m_TextCondition;

  public new IEnumerator Init(PlayerQuestGate[] gates, Quest002171Scroll scrollcomp)
  {
    Quest002171CollaboOpenPopup collaboOpenPopup = this;
    KeyValuePair<int, QuestkeyCondition> keyValuePair = MasterData.QuestkeyCondition.FirstOrDefault<KeyValuePair<int, QuestkeyCondition>>((Func<KeyValuePair<int, QuestkeyCondition>, bool>) (fd => fd.Value.gate_id == gates[0].quest_gate_id));
    collaboOpenPopup.m_TextCondition.SetTextLocalize(keyValuePair.Value.contents);
    IEnumerator e = ((Quest002171QuestOpenPopup) collaboOpenPopup).Init(gates, scrollcomp);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }
}
