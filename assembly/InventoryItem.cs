﻿// Decompiled with JetBrains decompiler
// Type: InventoryItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;

public class InventoryItem
{
  public static readonly int GearRankMax = 10;
  private static readonly int BrokenSellValue = 1;
  public ItemInfo Item;
  public ItemIcon icon;
  public bool select;
  public int index;
  public int selectCount;
  public bool Gray;
  public bool removeButton;

  public InventoryItem()
  {
    this.removeButton = true;
  }

  public InventoryItem(ItemInfo item)
  {
    this.Item = item;
  }

  public InventoryItem(PlayerItem playerItem)
  {
    this.Item = new ItemInfo(playerItem);
  }

  public InventoryItem(PlayerMaterialGear playerItem, int sameIndex = 0)
  {
    this.Item = new ItemInfo(playerItem, sameIndex);
  }

  public string GetName()
  {
    return this.Item.Name();
  }

  public string GetDescription()
  {
    return this.Item.Description();
  }

  public long GetSingleSellPrice()
  {
    return this.Item.SellPrice();
  }

  public long GetSellPrice()
  {
    if (!this.select)
      return 0;
    long num1;
    if (this.Item.itemType != ItemInfo.ItemType.Supply)
    {
      if (this.Item.broken)
      {
        num1 = (long) InventoryItem.BrokenSellValue;
      }
      else
      {
        float num2 = this.Item.gearLevel == 0 ? 1f : 1f * (float) this.Item.gearLevel;
        num1 = this.Item.itemType != ItemInfo.ItemType.Exchangable ? (this.Item.itemType != ItemInfo.ItemType.Gear ? (long) ((double) this.Item.SellPrice() * (double) num2) * (long) this.selectCount : (long) ((double) this.Item.SellPrice() * (double) num2)) : this.Item.SellPrice() * (long) this.selectCount;
      }
    }
    else
      num1 = this.Item.SellPrice() * (long) this.selectCount;
    return num1;
  }

  public int GetRepairPrice(int zenyBoostCntCnt = 0)
  {
    int num = this.Item.RepairPrice();
    if (zenyBoostCntCnt > 0)
      num *= zenyBoostCntCnt + 1;
    return num;
  }

  public Future<UnityEngine.Sprite> LoadSpriteThumbnail()
  {
    return this.Item.LoadSpriteThumbnail();
  }
}
