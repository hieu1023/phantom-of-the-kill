﻿// Decompiled with JetBrains decompiler
// Type: Shop0071PopupUnitItemPlus
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

public class Shop0071PopupUnitItemPlus : BackButtonMenuBase
{
  private Shop0071Menu menu_;

  public void initialize(Shop0071Menu menu)
  {
    this.menu_ = menu;
  }

  public void onClickUnitPlus()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<PopupManager>.GetInstance().dismiss(false);
    this.menu_.popupUnitPlus();
  }

  public void onClickItemPlus()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<PopupManager>.GetInstance().dismiss(false);
    this.menu_.popupItemPlus();
  }

  public override void onBackButton()
  {
    this.onClickClose();
  }

  public void onClickClose()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<PopupManager>.GetInstance().dismiss(false);
  }
}
