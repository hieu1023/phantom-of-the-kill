﻿// Decompiled with JetBrains decompiler
// Type: Bugu005WeaponStorageScene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System;
using System.Collections;
using UnityEngine;

public class Bugu005WeaponStorageScene : NGSceneBase
{
  public Bugu005WeaponStorageMenu menu;

  public override IEnumerator onInitSceneAsync()
  {
    yield break;
  }

  public static void ChangeScene(bool stack)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("bugu005_weapon_storage", stack, (object[]) Array.Empty<object>());
  }

  public virtual IEnumerator onStartSceneAsync()
  {
    Bugu005WeaponStorageScene weaponStorageScene = this;
    if (Singleton<NGGameDataManager>.GetInstance().IsColosseum)
      Singleton<CommonRoot>.GetInstance().SetFooterEnable(false);
    Future<GameObject> bgF = Res.Prefabs.BackGround.DefaultBackground_storage.Load<GameObject>();
    IEnumerator e = bgF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    weaponStorageScene.backgroundPrefab = bgF.Result;
    e = weaponStorageScene.menu.Init();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public virtual void onStartScene()
  {
    Singleton<PopupManager>.GetInstance().closeAll(false);
    Singleton<CommonRoot>.GetInstance().isActiveHeader = true;
    Singleton<CommonRoot>.GetInstance().isActiveFooter = true;
  }

  public virtual void onBackScene()
  {
    this.menu.onBackScene();
  }

  public override void onEndScene()
  {
    this.menu.onEndScene();
  }
}
