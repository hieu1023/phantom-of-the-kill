from PIL import Image
from lib.paths import PATH
import os
import json
import shutil

src_path = os.path.join(PATH,*['extracted','AssetBundle','Resources'])
dst_path = os.path.join(PATH,'images')

# get unit names
units = {}
fp = os.path.join(PATH,*['masterdata','UnitUnit.json'])
if os.path.exists(fp):
    units = {
        char['ID'] : (char['name'].replace('?','？'), char['english_name'])
        for char in json.load(open(fp, 'rb'))
    }

# Event Images
ipath = os.path.join(src_path, 'EventImages')
dpath = os.path.join(dst_path,'EventImages')
os.makedirs(dpath, exist_ok=True)
for fp in os.listdir(ipath):
    dst = os.path.join(dpath, fp)
    if os.path.exists(dst):
        continue
    img = Image.open(os.path.join(ipath, fp))
    if img.size == (720, 1136):
        img.crop((40,130,680,950)).save(dst)

# Units
ipath = os.path.join(src_path, 'Units')
dpath = os.path.join(dst_path,'Units')
os.makedirs(dpath, exist_ok=True)
for fid in os.listdir(ipath):
    fp = os.path.join(ipath, *[fid,'2D','unit_hires.png'])
    if os.path.exists(fp):
        name = str(fid)
        fid = int(fid)
        if fid in units:
            name += '_'.join(['',*units[fid]])
        dst = os.path.join(dpath, f"{name}.png")
        shutil.copy2(fp, dst)

# Units Icons
ipath = os.path.join(src_path, 'Units')
dpath = os.path.join(dst_path,'UnitIcons')
os.makedirs(dpath, exist_ok=True)
for fid in os.listdir(ipath):
    fp = os.path.join(ipath, *[fid,'2D','c_thum.png'])
    if os.path.exists(fp):
        name = str(fid)
        fid = int(fid)
        if fid in units:
            name += '_'.join(['',*units[fid]])
        dst = os.path.join(dpath, f"{name}.png")
        shutil.copy2(fp, dst)

# Mob Units
ipath = os.path.join(src_path, 'MobUnits')
dpath = os.path.join(dst_path,'MobUnits')
os.makedirs(dpath, exist_ok=True)
for fid in os.listdir(ipath):
    fp = os.path.join(ipath, *[fid,'unit_large.png'])
    dst = os.path.join(dpath, f"{fid}.png")
    if os.path.exists(fp) and not os.path.exists(dst):
        shutil.copy2(fp, dst)