import json
import os
import shutil
from lib.paths import Paths, PATH
from lib.api import Enviroment

SRC = os.path.join(PATH, *['Data', 'cache'])
DST = os.path.join(PATH, 'extracted')

paths = Paths()
env = Enviroment(True)


os.makedirs(SRC, exist_ok = True)
os.makedirs(DST, exist_ok = True)
# AssetBundle
for fpath, item in paths['AssetBundle'].items():
    sfp = os.path.join(SRC, item['FileName'])
    if 'MasterData' not in fpath or '_' in fpath:
        continue
    if not os.path.exists(sfp) or os.path.getsize(sfp) != item['FileSize']:
        print(fpath, sfp)
        open(sfp, 'wb').write(env.download_asset('ab', item['FileName']))