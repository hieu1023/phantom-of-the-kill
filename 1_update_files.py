import json
import os
import shutil
import unitypack
from lib.paths import Paths, PATH
from lib.api import Enviroment

SRC = os.path.join(PATH, *['Data', 'cache'])
DST = os.path.join(PATH, 'extracted')

paths = Paths()
env = Enviroment(True)


os.makedirs(SRC, exist_ok = True)
os.makedirs(DST, exist_ok = True)
# AssetBundle
for fpath, item in paths['AssetBundle'].items():
    sfp = os.path.join(SRC, item['FileName'])
    if not os.path.exists(sfp) or os.path.getsize(sfp) != item['FileSize']:
        print(fpath, sfp)
        open(sfp, 'wb').write(env.download_asset('ab', item['FileName']))

    try:
        with open(sfp, 'rb') as sf:
            b = unitypack.load(sf)

            ab = None
            for asset in b.assets:
                for obj in asset.objects.values():
                    if obj.type == 'AssetBundle':
                        ab = obj.read()
                        break
                if not ab or 'm_Container' not in ab:
                    continue
                for container in ab['m_Container']:
                    apath = container[0]
                    obj = container[1]['asset'].object
                    #print(apath, container[1]['asset'].object.type)
                    if obj.type == "TextAsset":
                        data = obj.read()
                        dfp = os.path.join(DST, *apath.split('/'))
                        os.makedirs(os.path.dirname(dfp), exist_ok=True)
                        open(dfp, 'wb').write(data.script)
    except:
        pass


# StreamingAssets
for fpath, item in paths['StreamingAssets'].items():
    # cut the crap, no need to save as "raw" file, since no extraction is required
    dfp = os.path.join(DST, *fpath.split('/'))+item['Extension']
    os.makedirs(os.path.dirname(dfp), exist_ok=True)
    if not os.path.exists(dfp) or os.path.getsize(dfp) != item['FileSize']:
        # download file
        print(dfp)
        open(dfp, 'wb').write(env.download_asset('sa', item['FileName']))
